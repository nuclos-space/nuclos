import nuclet.test.rules.TestMultiref
import nuclet.test.rules.TestMultirefDependent

def updateDependent = {
	it.name = it.name + ' - updated'
}

TestMultiref ref = new TestMultiref(name: 'Test')

assert ref.testMultirefDependentByMultirefa.empty

ref.save()

assert ref.testMultirefDependentByMultirefa.empty

assert ref.countmultirefa == 0
assert ref.countmultirefb == 0

assert ref.countmultirefanew == 0
assert ref.countmultirefbnew == 0

assert ref.countmultirefaupdate == 0
assert ref.countmultirefbupdate == 0

assert ref.countmultirefadelete == 0
assert ref.countmultirefbdelete == 0

assert ref.countmultirefanone == 0
assert ref.countmultirefbnone == 0

Insert_new_A:
{
	ref.testMultirefDependentByMultirefa << new TestMultirefDependent(name: new Date().toString())
	ref.save()

	assert ref.countmultirefa == 1
	assert ref.countmultirefb == 0

	assert ref.countmultirefanew == 1
	assert ref.countmultirefbnew == 0

	assert ref.countmultirefaupdate == 0
	assert ref.countmultirefbupdate == 0

	assert ref.countmultirefadelete == 0
	assert ref.countmultirefbdelete == 0

	assert ref.countmultirefanone == 0
	assert ref.countmultirefbnone == 0
}

Insert_new_A_2:
{
	ref.testMultirefDependentByMultirefa << new TestMultirefDependent(name: new Date().toString())
	ref.save()

	assert ref.countmultirefa == 2
	assert ref.countmultirefb == 0

	assert ref.countmultirefanew == 1
	assert ref.countmultirefbnew == 0

	assert ref.countmultirefaupdate == 0
	assert ref.countmultirefbupdate == 0

	assert ref.countmultirefadelete == 0
	assert ref.countmultirefbdelete == 0

	assert ref.countmultirefanone == 1
	assert ref.countmultirefbnone == 0
}

Insert_new_B:
{
	ref.testMultirefDependentByMultirefb << new TestMultirefDependent(name: new Date().toString())
	ref.save()

	assert ref.countmultirefa == 2
	assert ref.countmultirefb == 1

	assert ref.countmultirefanew == 0
	assert ref.countmultirefbnew == 1

	assert ref.countmultirefaupdate == 0
	assert ref.countmultirefbupdate == 0

	assert ref.countmultirefadelete == 0
	assert ref.countmultirefbdelete == 0

	assert ref.countmultirefanone == 2
	assert ref.countmultirefbnone == 0
}

Update_A:
{
	updateDependent(ref.testMultirefDependentByMultirefa.get(0))
	ref.save()

	assert ref.countmultirefa == 2
	assert ref.countmultirefb == 1

	assert ref.countmultirefanew == 0
	assert ref.countmultirefbnew == 0

	assert ref.countmultirefaupdate == 1
	assert ref.countmultirefbupdate == 0

	assert ref.countmultirefadelete == 0
	assert ref.countmultirefbdelete == 0

	assert ref.countmultirefanone == 1
	assert ref.countmultirefbnone == 1
}

Update_B:
{
	updateDependent(ref.testMultirefDependentByMultirefb.get(0))
	ref.save()

	assert ref.countmultirefa == 2
	assert ref.countmultirefb == 1

	assert ref.countmultirefanew == 0
	assert ref.countmultirefbnew == 0

	assert ref.countmultirefaupdate == 0
	assert ref.countmultirefbupdate == 1

	assert ref.countmultirefadelete == 0
	assert ref.countmultirefbdelete == 0

	assert ref.countmultirefanone == 2
	assert ref.countmultirefbnone == 0
}

Remove_A:
{
	ref.testMultirefDependentByMultirefa.remove(ref.testMultirefDependentByMultirefa.get(0))
	ref.save()

	assert ref.countmultirefa == 1
	assert ref.countmultirefb == 1

	assert ref.countmultirefanew == 0
	assert ref.countmultirefbnew == 0

	assert ref.countmultirefaupdate == 0
	assert ref.countmultirefbupdate == 0

	assert ref.countmultirefadelete == 1
	assert ref.countmultirefbdelete == 0

	assert ref.countmultirefanone == 1
	assert ref.countmultirefbnone == 1
}

Remove_A_2:
{
	ref.testMultirefDependentByMultirefa.remove(ref.testMultirefDependentByMultirefa.get(0))
	ref.save()

	assert ref.countmultirefa == 0
	assert ref.countmultirefb == 1

	assert ref.countmultirefanew == 0
	assert ref.countmultirefbnew == 0

	assert ref.countmultirefaupdate == 0
	assert ref.countmultirefbupdate == 0

	assert ref.countmultirefadelete == 1
	assert ref.countmultirefbdelete == 0

	assert ref.countmultirefanone == 0
	assert ref.countmultirefbnone == 1
}

Remove_B:
{
	ref.testMultirefDependentByMultirefb.remove(ref.testMultirefDependentByMultirefb.get(0))
	ref.save()

	assert ref.countmultirefa == 0
	assert ref.countmultirefb == 0

	assert ref.countmultirefanew == 0
	assert ref.countmultirefbnew == 0

	assert ref.countmultirefaupdate == 0
	assert ref.countmultirefbupdate == 0

	assert ref.countmultirefadelete == 0
	assert ref.countmultirefbdelete == 1

	assert ref.countmultirefanone == 0
	assert ref.countmultirefbnone == 0
}

Create_dependents_directly:
{
	new TestMultirefDependent(name: 'Dependent A 1', multirefaId: ref.id).save()
	new TestMultirefDependent(name: 'Dependent A 2', multirefaId: ref.id).save()
	new TestMultirefDependent(name: 'Dependent B 1', multirefbId: ref.id).save()
	new TestMultirefDependent(name: 'Dependent A+B 1', multirefaId: ref.id, multirefbId: ref.id).save()

// Update the counts
	ref.executeMultirefSpeichern()

	assert ref.countmultirefa == 3
	assert ref.countmultirefb == 2

	assert ref.countmultirefanew == 0
	assert ref.countmultirefbnew == 0

	assert ref.countmultirefaupdate == 0
	assert ref.countmultirefbupdate == 0

	assert ref.countmultirefadelete == 0
	assert ref.countmultirefbdelete == 0

	assert ref.countmultirefanone == 3
	assert ref.countmultirefbnone == 2
}