package org.nuclos.test.webclient.dashboard

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.dashboard.Dashboard
import org.nuclos.test.webclient.pageobjects.dashboard.DashboardItem
import org.nuclos.test.webclient.pageobjects.preference.PreferenceType
import org.nuclos.test.webclient.pageobjects.preference.Preferences
import org.nuclos.test.webclient.pageobjects.search.Searchbar

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DashboardTest extends AbstractWebclientTest {
	@Test
	void _00_setup() {
		LocaleChooser.locale = Locale.ENGLISH
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void _05_newDashboard() {
		assert !Dashboard.configMode

		Dashboard.addNew()

		assert Dashboard.configMode

		assert Dashboard.name == 'New dashboard'
		Dashboard.name = 'Dashboard 1'
		assert Dashboard.name == 'Dashboard 1'
		assert Dashboard.tabTitles == ['Dashboard 1']

		assert Dashboard.items.empty
	}

	@Test
	void _10_addTasklist() {
		Dashboard.taskListSelect.open()
		assert Dashboard.taskListSelect.choices.containsAll([
				'Order-Dyn-Tasks',
				'Without Menu Entry',
		])

		Dashboard.addTaskListItem('Order-Dyn-Tasks')

		Dashboard.items.with {
			assert it.size() == 1
			it.first().with {
				assert it.type == 'tasklist'
				assert it.x == 0
				assert it.y == 0
				// rows/cols initially undefined
				assert it.rows == null
				assert it.cols == null
			}
		}

		// TODO: Check column sorting (should already have been loaded from sidebar preferences and applied)

		Dashboard.closeConfig()
	}

	@Test
	void _12_testTaskList() {
		def orderDynTaskList = Dashboard.items.first()
		assert orderDynTaskList.countRows() == 3

		// click task list to open sidebar (here configured to jump into Order BO)
		orderDynTaskList.clickRow(2)
		assert Searchbar.getActiveTaskList() == 'Order-Dyn-Tasks'
		assert Sidebar.listEntryCount == 3
	}

	@Test
	void _15_reloadDashboard() {
		Dashboard.open()
		assert Dashboard.tabTitles == ['Dashboard 1']
		assert Dashboard.items.size() == 1
	}

	@Test
	void _20_shareDashboard() {
		Preferences.open()
		Preferences.shareItem(PreferenceType.DASHBOARD, 'Dashboard 1', 'Example user')
		logout()

		// FIXME: When logging in as nuclos and loading the dashboard, the workspace is accessed and thereby cloned first
		// in response to a GET-request, resulting in the following error:
		// Request failed: GET http://192.168.1.145:44157/nuclos-war/rest/meta/tasklists - 200 - 194 ms
		//org.codehaus.groovy.runtime.powerassert.PowerAssertionError:
		// assert !restResponse.sqlInsertUpdateDeleteCount
		//        ||            |
		//        ||            2
		//        |org.nuclos.test.rest.RestResponse@1ea5e54d
		//        false
//		login('nuclos')
//		assert Dashboard.tabTitles == ['Dashboard 1']
	}

	@Test
	void _25_customizeDashboard() {
		// TODO: Customize the shared dashboard
	}

	@Test
	void _30_addSecondDashboard() {
		// TODO: Add some new dashboard
	}

	@Test
	void _35_deleteDashboard() {
		// TODO: Delete the newly created dashboard
		// TODO: Check that the shared dashboard is not deletable
	}
}
