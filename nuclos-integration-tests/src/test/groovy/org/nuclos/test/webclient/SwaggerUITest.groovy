package org.nuclos.test.webclient

import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.pageobjects.MenuComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SwaggerUITest extends AbstractWebclientTest {

	@AfterClass
	static void cleanup() {
		nuclosSession.setSystemParameters([:])
	}

	@Test()
	void _05_openSwaggerUI() {
		MenuComponent.toggleDevUtilsMenu()
		MenuComponent.clickSwaggerUI()

		// Check for the "login" operation
		waitFor {
			$('#operations-default-login')
		}
	}

	/**
	 * We should be directly able to execute requests that require a valid login.
	 */
	@Test()
	void _10_executeRequestWithAuthorization() {
		// The GET /rest/login operation
		NuclosWebElement operation = $('[id*="operations-default-login"].opblock-get')

		operation.scrollIntoView()

		// Requires a login first (POST /rest/login) - check for authorization button
		assert operation.$('.authorization__btn.unlocked')

		// Expand the operation panel
		operation.click()

		// Click the "Try it out" button
		operation.$('.try-out__btn').click()
		// ...and the "Execute" button
		operation.$('.execute').click()

		NuclosWebElement response = operation.$('.response')
		assert response.$('.response-col_status').text.trim() == '200'
		response.$('.response-col_description').text.trim().with {
			assert it.contains('"sessionId":')
			assert it.contains('"username":')
		}
	}

	@Test()
	void _15_disableSwagger() {
		nuclosSession.setSystemParameters(['SWAGGER_ACTIVE': 'false'])

		getUrlHash('/swagger-ui')
		refresh()

		waitFor {
			$('.swagger-ui')?.text?.contains('No API definition provided.')
		}
	}
}
