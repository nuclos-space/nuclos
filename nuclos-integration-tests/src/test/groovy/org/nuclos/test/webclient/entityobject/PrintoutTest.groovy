package org.nuclos.test.webclient.entityobject

import java.text.SimpleDateFormat

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common2.IOUtils
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.PrintoutComponent
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PrintoutTest extends AbstractWebclientTest {

	static WebElement findElementContainingText(String cssSelector, String text) {
		return $$(cssSelector).find { it.text == text }
	}

	static void selectDropdownValue(String value) {
		$('.modal .ui-autocomplete-dropdown').click()
		findElementContainingText('.modal .ui-autocomplete-list li span', value).click()
	}


	@Test
	void _00_setup() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}


	@Test
	void _15_printXLSX() {
		PrintoutComponent.open()

		PrintoutComponent.selectPrintout(1)

		PrintoutComponent.execute()

		selectDropdownValue('Software')

		PrintoutComponent.execute()

		PrintoutComponent.downloadLink.click()

		// TODO: Download file and check content

		PrintoutComponent.closeModal()
	}

	@Test
	void _10_printPDF() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		PrintoutComponent.open()

		PrintoutComponent.selectPrintout(0)

		PrintoutComponent.execute()

		NuclosWebElement link = PrintoutComponent.getDownloadLink()

		assert link.text.trim() == 'Overview.pdf'
		String downloadUrl = link.getAttribute('href')


		// Download file and check content
		byte[] data = downloadFile(downloadUrl)
		PDDocument document = PDDocument.load(data)
		PDFTextStripper pdfStripper = new PDFTextStripper()
		String text = pdfStripper.getText(document)
		try {
			assert text.contains('Order')
			assert text.contains(eo.getAttribute('orderNumber') as String)
			assert text.contains('Created')
			assert text.contains(new SimpleDateFormat('yyyy-MM-dd').format(new Date()))
		} catch (Throwable t) {
			final File failure = new File('failure.pdf')
			IOUtils.writeToBinaryFile(failure, data)
			throw t
		}

		PrintoutComponent.closeModal()
	}

}
