package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RecordSynchronisationTest extends AbstractWebclientTest {

	@Test
	void _00_setup() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void _05_editSubformEntryInWindow() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		Subform subform = eo.getSubform('example_rest_CustomerAddress_customer')

		Subform.Row row = subform.getRow(0)
		String oldStreet = row.getValue('street')

		assert oldStreet

		String newStreet = 'Another Street'
		assert oldStreet != newStreet

		/**
		 * Edit the row in a new Window.
		 * After saving in the other window, the changes should be synchronized back to this subform.
		 */
		editStreetInOtherWindow:
		{
			row.editInWindow()

			switchToOtherWindow()

			EntityObjectComponent detail = EntityObjectComponent.forDetail()
			detail.setAttribute('street', 'Another Street')
			detail.save()

			switchToOtherWindow()
		}

		assert row.getValue('street') == newStreet

		/**
		 * Edit again directly in subform to check that there are no version conflicts.
		 */
		editSubformEntryInSubform:
		{
			String newStreet2 = 'Another Street 2'
			row.enterValue('street', newStreet2)
			assert row.dirty

			eo.save()

			assert !eo.dirty
			assert eo.getErrorMessages().empty

			assert row.getValue('street') == newStreet2
		}
	}
}
