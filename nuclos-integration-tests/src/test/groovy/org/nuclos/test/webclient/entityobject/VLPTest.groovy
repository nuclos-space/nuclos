package org.nuclos.test.webclient.entityobject


import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class VLPTest extends AbstractWebclientTest {

	private static String name = 'VLP Test'

	@Test
	void _00_setup() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVLP)
		eo.addNew()

		eo.setAttribute('name', name)

		eo.save()
	}

	@Test
	void _05_vlpForMainRecord() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('vlpreference')
		lov.open()

		assert lov.choices == ['', name]
	}

	@Test
	void _10_vlpForSubform() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTVLPSUBFORM, 'parent')

		Subform.Row row = subform.newRow()

		ListOfValues lov = row.getLOV('vlpreference')
		assert lov.open

		assert lov.choices == ['', name]
	}
}
