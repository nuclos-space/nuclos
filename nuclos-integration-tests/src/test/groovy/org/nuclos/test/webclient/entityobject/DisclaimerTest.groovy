package org.nuclos.test.webclient.entityobject

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.DetailButtonsComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DisclaimerTest extends AbstractWebclientTest {

	static String title = 'Privacy Policy'

	static void createPrivacyPolicyViaRest() {
		EntityObject eo = new EntityObject(SystemEntities.NEWS)

		eo.setAttribute('name', 'privacy policy name')
		eo.setAttribute('title', title)
		eo.setAttribute('content', '''Content... <b>Bold</b> <br/> New line....''')
		eo.setAttribute('active', true)
//		eo.setAttribute('confirmationRequired', true)
		eo.setAttribute('privacyPolicy', true)

		nuclosSession.save(eo)
	}

	@BeforeClass
	static void setup() {
		setup(true) {
			createPrivacyPolicyViaRest()
		}

		assert $('#showprivacycontent')
	}

	@Test()
	void _05_cookieWarning() {
		WebElement we = $('#showprivacycontent')
		assert we

		we.click()

		assertMessageModalAndConfirm(title, null)

		we = $('#acceptcookies')
		assert we

		we.click()

		assert !$('#showprivacycontent')
		assert !$('#acceptcookies')

		assert logout()
		EntityObjectComponent.refresh()

		assert $('#showprivacycontent') == null
	}

	@Test()
	void _10_legalDisclaimersMenu() {
		NuclosWebElement we = $('#news')

		assert we
		we.click()

		NuclosWebElement di = $(we, '.dropdown-item')

		assert di
		String text = di.getText()

		assert text.trim() == title
		di.click()

		assertMessageModalAndConfirm(title, null)

		assert login('test', 'test')

		assert $('#news')
		assert !$('#showprivacycontent')
	}

	@Test()
	void _15_routes() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('note', 'Note')
		assert eo.dirty

		// Both #/news/... and #/disclaimer/... routes should work - #/disclaimer/... only for backward compatibility
		['disclaimer', 'news'].each {
			getUrlHash("/$it/$title")
			assertMessageModalAndConfirm(title, null)
		}

		assert currentUrl.contains('/view/' + TestEntities.EXAMPLE_REST_ORDER)
		assert !DetailButtonsComponent.popoverTitle
		assert !DetailButtonsComponent.popoverText

		eo.cancel()
	}

	@Test()
	void _20_routeViaHyperlinkButton() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()
		eo.setAttribute('text', 'Disclaimer Button Test')

		// Both #/news/... and #/disclaimer/... routes should work - #/disclaimer/... only for backward compatibility
		['disclaimer', 'news'].each {
			eo.setAttribute('hyperlink', "#/$it/$title")
			eo.save()

			eo.clickButton('Hyperlink button')
			assertMessageModalAndConfirm(title, null)
		}

		assert currentUrl.contains('/view/nuclet_test_other_TestLayoutComponents')
		assert !DetailButtonsComponent.popoverTitle
		assert !DetailButtonsComponent.popoverText
	}
}
