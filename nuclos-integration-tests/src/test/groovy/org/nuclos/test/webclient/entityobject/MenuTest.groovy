package org.nuclos.test.webclient.entityobject

import org.apache.commons.lang.StringUtils
import org.json.JSONArray
import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.MenuComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class MenuTest extends AbstractWebclientTest {

	@Test
	void _00_openMenuEntry() {
		MenuComponent.openMenu('Example', 'Article')
		assert getCurrentUrl().endsWith('Article')
	}

	@Test
	void _05_openSubMenuEntry() {
		MenuComponent.openMenu('Matrix', 'Unterobjekte', 'X-Achse')
		assert getCurrentUrl().endsWith('XAchse')
	}

	@Test
	void _10_hiddenDynamicTasklist() {
		JSONObject menu = nuclosSession.menuStructure

		checkMenuForNull(menu)
	}

	void checkMenuForNull(JSONObject menu) {
		String label = menu.getString('name')
		assert label != 'null'
		assert StringUtils.isNotBlank(label)

		if (menu.has('entries')){
			JSONArray entries = menu.getJSONArray('entries')
			(0..entries.length()-1).each {
				checkMenuForNull(entries.getJSONObject(it))
			}
		}
	}
}
