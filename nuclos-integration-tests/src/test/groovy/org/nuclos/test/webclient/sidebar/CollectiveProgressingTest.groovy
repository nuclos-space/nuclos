package org.nuclos.test.webclient.sidebar

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.CollectiveProgressingComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CollectiveProgressingTest extends AbstractWebclientTest {

	@BeforeClass
	static void setup() {
		AbstractWebclientTest.setup()
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void _02_checkCollectiveProgressingOpens(){
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		assert CollectiveProgressingComponent.isCollectiveProcessing()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _03_checkWeCannotManipulateSingleEntry() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.showDetails()

		assert EntityObjectComponent.newButton == null
		assert EntityObjectComponent.saveButton == null
		assert EntityObjectComponent.cloneButton == null
		assert EntityObjectComponent.deleteButton == null

		CollectiveProgressingComponent.showCollectiveProcessing()
		CollectiveProgressingComponent.cancelCollectiveProcessing()
		waitForAngularRequestsToFinish()
	}

	@Test
	void _04_checkOpeningViaMultiSelectingElementsInSidebar() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.selectRange(0, 4, true)
		assert Sidebar.countMultiSelected() == 5
		assert Sidebar.isMultiSelecting()

		CollectiveProgressingComponent.cancelCollectiveProcessing()
		waitForAngularRequestsToFinish()
		Sidebar.toggleMultiSelection()

		Sidebar.selectRange(0, 2, false)
		assert Sidebar.countMultiSelected() == 2
		assert Sidebar.isMultiSelecting()

		CollectiveProgressingComponent.cancelCollectiveProcessing()
		waitForAngularRequestsToFinish()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _05_testSelectingViaKeyboardSpace() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.selectEntry(0)

		sendKeysAndWaitForAngular(Keys.SPACE) //Space hit
		assert CollectiveProgressingComponent.isCollectiveProcessing()

		assert Sidebar.countMultiSelected() == 1

		Sidebar.selectEntry(0)

		sendKeysAndWaitForAngular(Keys.ARROW_DOWN)
		sendKeysAndWaitForAngular(Keys.SPACE) //Space hit

		sendKeysAndWaitForAngular(Keys.ARROW_DOWN)
		sendKeysAndWaitForAngular(Keys.SPACE) //Space hit

		assert Sidebar.countMultiSelected() == 3
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _06_checkCANSTATECHANGEDatensatzFreigabe() {
		RESTHelper.createUser('datentest', 'nuclos', ['TesteDatensatzFreigabeStateChange'], nuclosSession)
		waitForAngularRequestsToFinish()

		logout()
		login('datentest', 'nuclos')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()

		assert CollectiveProgressingComponent.stateChangeNames.empty

		waitForAngularRequestsToFinish()
		Sidebar.toggleMultiSelection()

		logout()
		login('test', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()

		assert !CollectiveProgressingComponent.stateChangeNames.empty

		Sidebar.toggleMultiSelection()
	}

	@Test
	void _07_checkStateChangeWorking() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.stateChangeNames.containsAll(['abschließen', 'abbrechen'])

		CollectiveProgressingComponent.doStateChange('abschließen')
		waitForAngularRequestsToFinish()

		assertMessageModalAndConfirm("Soll der Statuswechsel wirklich durchgeführt werden?", "Alles erledigt")

		assert CollectiveProgressingComponent.isProgress()

		CollectiveProgressingComponent.assertProgressSuccess(6)

		CollectiveProgressingComponent.closeProgress()
		waitForAngularRequestsToFinish()


		Sidebar.toggleMultiSelectionAll()
		Sidebar.toggleMultiSelection()
		waitForAngularRequestsToFinish()
	}

	@Test
	void _08_checkObjGenWithoutDialog() {
		logout()
		login('nuclos', '')
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		// first set address for order to make ObjGen working
		Sidebar.selectEntry(0)
		EntityObjectComponent.forDetail().setAttribute('customerAddress', 'Test 1, 12345, Test 1')
		EntityObjectComponent.forDetail().save()

		Sidebar.selectEntry(1)
		EntityObjectComponent.forDetail().setAttribute('customerAddress', 'Test 1, 12345, Test 1')
		EntityObjectComponent.forDetail().save()

		Sidebar.selectEntry(2)
		EntityObjectComponent.forDetail().setAttribute('customerAddress', 'Test 2, 54321, Test 2')
		EntityObjectComponent.forDetail().save()

		Sidebar.toggleMultiSelection()
		Sidebar.selectRange(0, 2, true)
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.funcNames.containsAll(['generate Invoice'])
		CollectiveProgressingComponent.doFunction('generate Invoice')
		waitForAngularRequestsToFinish()

		assert driver.getWindowHandles().size() == 3
		driver.switchTo().window(driver.getWindowHandles()[0])
		closeOtherWindows()

		assert CollectiveProgressingComponent.isProgress()

		CollectiveProgressingComponent.assertProgressSuccess(3)
		def genObjs = CollectiveProgressingComponent.getProgressGenObjectNames(3)
		assert (genObjs.count('1 (Test-Customer)') == 2 || genObjs.count('2 (Test-Customer)') == 2)
		assert (genObjs.count('2 (Test-Customer)') == 1 || genObjs.count('1 (Test-Customer)') == 1)

		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.closeProgress()
		CollectiveProgressingComponent.showCollectiveProcessing()
		CollectiveProgressingComponent.cancelCollectiveProcessing()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _09_checkObjGenWithDialog() {
		logout()
		login('nuclos', '')
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		// first set address for order to make ObjGen working
		Sidebar.selectEntry(4)
		EntityObjectComponent.forDetail().getLOV('customer').selectEntry('VLPText: Test-Customer 2')
		EntityObjectComponent.forDetail().save()

		Sidebar.selectEntry(5)
		EntityObjectComponent.forDetail().getLOV('customer').selectEntry('VLPText: Test-Customer 2')
		EntityObjectComponent.forDetail().save()

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.funcNames.containsAll(['generate Invoice Dialog'])
		CollectiveProgressingComponent.doFunction('generate Invoice Dialog')
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.isProgress()

		// dialog handling
		handleObjectGenDialog('City 4, 44444, Street 4')
		handleObjectGenDialog('Test 2, 54321, Test 2')

		CollectiveProgressingComponent.assertProgressSuccess(6)
		def genObjs = CollectiveProgressingComponent.getProgressGenObjectNames(6)
		assert (genObjs.count('4 (Test-Customer)') == 4 || genObjs.count('3 (Test-Customer)') == 4)
		assert (genObjs.count('4 (Test-Customer 2)') == 2 || genObjs.count('3 (Test-Customer 2)') == 2)

		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.closeProgress()
		CollectiveProgressingComponent.showCollectiveProcessing()
		CollectiveProgressingComponent.cancelCollectiveProcessing()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _10_checkCustomRulesAction() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.funcNames.containsAll(['Diesen Auftrag sperren...', 'SendOrder'])

		CollectiveProgressingComponent.doFunction('Diesen Auftrag sperren...')
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.isProgress()

		CollectiveProgressingComponent.assertProgressSuccess(6)

		CollectiveProgressingComponent.closeProgress()
		waitForAngularRequestsToFinish()
		Sidebar.toggleMultiSelectionAll()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _11_testDeleteAction() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.actionNames.contains('Löschen')

		CollectiveProgressingComponent.doAction('Löschen')

		assertMessageModalAndConfirm("Löschen", "Sollen die Datensätze wirklich gelöscht werden?")
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.isProgress()

		CollectiveProgressingComponent.assertProgressSuccess(6)

		CollectiveProgressingComponent.closeProgress()

		assert Sidebar.listEntryCount == 0
		Sidebar.toggleMultiSelection()
	}

	private static void handleObjectGenDialog(String customerAddress) {
		waitUntilTrue({ isPresent('nuc-detail-dialog') })
		waitForAngularRequestsToFinish()
		new Actions(driver)
				.moveToElement($('div[name="attribute-customerAddress"]').element, 20, 20)
				.click()
				.perform()
		sendKeys(customerAddress)
		new Actions(driver)
				.moveToElement($('nuc-detail-dialog').element, 20, 20)
				.click()
				.perform()
		new Actions(driver)
				.moveToElement($('nuc-datechooser').element, 20, 20)
				.click()
				.perform()
		new Actions(driver)
				.moveToElement($('nuc-datechooser').element, 50, 150)
				.click()
				.perform()
		new Actions(driver)
				.moveToElement($('div.modal-footer #button-ok').element)
				.click()
				.perform()
		waitForAngularRequestsToFinish()
	}
}
