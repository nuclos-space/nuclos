package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CloneTest extends AbstractWebclientTest {

	private EntityObject<Long> parent;

	@Test
	void _00_setup() {
		RESTHelper.createUser('tset', 'test', ['Example user'], nuclosSession)

		parent = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		parent.setAttribute('text', 'parent')
		parent.setAttribute('value', 1)

		List<EntityObject<Long>> subform = parent.getDependents(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
				'parent'
		)

		3.times { subCount ->
			EntityObject subEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM)
			subEo.setAttribute('text', subCount.toString())
			subEo.setAttribute('date', new Date())
			subform << subEo

			List<EntityObject<Long>> subsubform = subEo.getDependents(
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					'subform'
			)
			3.times { subsubCount ->
				EntityObject subsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM)
				subsubEo.setAttribute('text', "$subCount-$subsubCount".toString())
				subsubEo.setAttribute('value', 123.45)
				subsubform << subsubEo

				List<EntityObject<Long>> subsubsubform = subsubEo.getDependents(
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM,
						'subsubform'
				)
				3.times { subsubsubCount ->
					EntityObject subsubsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM)
					subsubsubEo.setAttribute('text', "$subCount-$subsubCount-$subsubsubCount".toString())
					subsubsubEo.setAttribute('value', 12345)
					subsubsubform << subsubsubEo
				}
			}
		}

		nuclosSession.save(parent)

		screenshot('before clone')

		EntityObjectComponent eo = EntityObjectComponent.open(parent);
		eo.clone()

		screenshot('after clone')

		assert eo.isDirty()

		assert eo.getAttribute('text') == 'parent'
		eo.setAttribute('text', 'parent 2')
		assert eo.getAttribute('value') == ''

		Subform subformComponent = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		assert subformComponent.getRowCount() == 3

		for (int i = 0; i < subformComponent.getRowCount(); i++) {
			Subform.Row sfRow = subformComponent.getRow(i)
			assert sfRow.getValue('text', String.class) == i + ''
			assert sfRow.getValue('date', Date.class) != null

			sfRow.setSelected(true)

			Subform subsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')

			for (int j = 0; j < subsubform.getRowCount(); j++) {
				assert subsubform.getRowCount() == 3

				Subform.Row ssfRow = subsubform.getRow(j)
				assert ssfRow.getValue('text', String.class) == i + '-' + j
				assert ssfRow.getValue('value', String.class) == ''

				ssfRow.setSelected(true)

				Subform subsubsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')
				assert subsubsubform.getRowCount() == 0

				ssfRow.setSelected(false)
			}

			sfRow.setSelected(false)
		}

		eo.save()

		eo = EntityObjectComponent.forDetail()
		assert eo.getAttribute('text') == 'parent 2'
	}
}
