package org.nuclos.test.webclient.entityobject


import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class TabindexTest extends AbstractWebclientTest {
	@Test
	void _05_tabAndEnter() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_TABINDEX_TESTTABINDEX)

		eo.addNew()

		// TODO: Initial focus
		$('[name="attribute-a"]').focus()

		sendKeys('input 1')	// write text in first input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 2') // write text in 2. input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 3') // write text in 3. input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 4') // write text in 4. input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 5') // write text in 5. input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 6')
		sendKeys(Keys.TAB)

		sendKeys('input 7')
		sendKeys(Keys.TAB)

		// User LOV - must enter an existing user
		sendKeys('test')
		sendKeys(Keys.TAB)
		waitForAngularRequestsToFinish()
		sendKeys(Keys.TAB)	// Requires a second Tab to confirm the selection

		sendKeys('input 9')
		sendKeys(Keys.TAB)

		sendKeys(' ')	// This is a checkbox - toggle it with space
		sendKeys(Keys.TAB)

		sendKeys('input 11')
		sendKeys(Keys.TAB)

		sendKeys('2018-09-25')
		sendKeys(Keys.TAB)

		sendNumber(38293)	// Integer input
		sendKeys(Keys.TAB)

		sendNumber(8372.38)
		sendKeys(Keys.TAB)

		sendKeys(' + again')	// We are back to the first input now
		sendKeys(Keys.TAB)

		assert eo.getAttribute('a') == 'input 1 + again'
		assert eo.getAttribute('b') == 'input 5'
		assert eo.getAttribute('c') == 'input 2'
		assert eo.getAttribute('d') == 'input 4'
		assert eo.getAttribute('e') == 'input 3'
		assert eo.getAttribute('encryptedtext') == 'input 6'
		assert eo.getAttribute('phone') == 'input 7'

		// FIXME: Works locally, but not on Jenkins
//		assert eo.getAttribute('reference') == 'test'

		assert eo.getAttribute('hyperlink') == 'input 9'
		assert eo.getAttribute('boolean', null, Boolean.class)
		assert eo.getAttribute('email') == 'input 11'
		assert eo.getAttribute('date', null, Date.class) == new Date(118, 8, 25)	// 25.09.2018
		assert eo.getAttribute('integer', null, Integer.class) == 38293
		assert eo.getAttribute('decimal', null, BigDecimal.class) == 8372.38

		eo.cancel()
	}

	/**
	 * Same like before, but tabbing in reverse order using SHIFT+TAB
	 */
	@Test
	void _10_tabAndEnterReverse() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()

		// TODO: Initial focus
		$('[name="attribute-a"]').focus()

		sendKeys('input 1')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendNumber(8372.38)
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendNumber(38293)	// Integer input
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('2018-09-25')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('input 11')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys(' ')	// This is a checkbox - toggle it with space
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('input 9')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		// User LOV - must enter an existing user
		sendKeys('test')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))
		waitForAngularRequestsToFinish()
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('input 7')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('input 6')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('input 5')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('input 4')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('input 3')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys('input 2')
		sendKeys(Keys.chord(Keys.SHIFT, Keys.TAB))

		sendKeys(' + again')	// We are back to the first input now
		sendKeys(Keys.TAB)

		assert eo.getAttribute('a') == 'input 1 + again'
		assert eo.getAttribute('b') == 'input 5'
		assert eo.getAttribute('c') == 'input 2'
		assert eo.getAttribute('d') == 'input 4'
		assert eo.getAttribute('e') == 'input 3'
		assert eo.getAttribute('encryptedtext') == 'input 6'
		assert eo.getAttribute('phone') == 'input 7'

		// FIXME: Works locally, but not on Jenkins
//		assert eo.getAttribute('reference') == 'test'

		assert eo.getAttribute('hyperlink') == 'input 9'
		assert eo.getAttribute('boolean', null, Boolean.class)
		assert eo.getAttribute('email') == 'input 11'
		assert eo.getAttribute('date', null, Date.class) == new Date(118, 8, 25)	// 25.09.2018
		assert eo.getAttribute('integer', null, Integer.class) == 38293
		assert eo.getAttribute('decimal', null, BigDecimal.class) == 8372.38
	}
}
