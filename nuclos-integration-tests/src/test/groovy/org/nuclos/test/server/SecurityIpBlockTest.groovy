package org.nuclos.test.server


import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SecurityIpBlockTest extends AbstractNuclosTest {
	RESTClient unknownClient1 = new RESTClient('unknown', 'unknown')
	RESTClient unknownClient2 = new RESTClient('unknown2', 'unknown2')
	RESTClient validClient = new RESTClient('nuclos', '')
	RESTClient invalidPasswordClient = new RESTClient('nuclos', 'invalid')

	@Test
	void _05_noBlockingByDefault() {
		// Delete all system params -> ip block feature should not be active
		nuclosSession.setSystemParameters([:])

		[unknownClient1, invalidPasswordClient].each { client ->
			5.times {
				expectLoginError(client, 401)
			}
		}
	}

	@Test
	void _10_withBlocking() {
		// Allow 1 attempt per second
		nuclosSession.patchSystemParameters([
				'SECURITY_IP_BLOCK_ATTEMPTS'         : '1',
				'SECURITY_IP_BLOCK_PERIOD_IN_SECONDS': '1',
		])

		// Try wrong credentials 3 times with enough time in between to not be blocked
		3.times {
			expectLoginError(unknownClient1, 401)
			sleep(1000)
		}

		expectLoginError(unknownClient1, 401)

		// Check if we are blocked (unknown user)
		expectLoginError(unknownClient1, 429)

		// Another unknown user
		expectLoginError(unknownClient2, 429)

		// Known user, invalid password
		expectLoginError(invalidPasswordClient, 429)

		// Correct credentials
		expectLoginError(validClient, 429)

		// After the block period we should be able to login again
		sleep(1000)
		validClient.login()
	}

	@Test
	void _15_deleteFailedAttemptsAfterSuccessfulLogin() {
		// Allow 1 attempt per second
		nuclosSession.patchSystemParameters([
				'SECURITY_IP_BLOCK_ATTEMPTS'         : '3',
				'SECURITY_IP_BLOCK_PERIOD_IN_SECONDS': '1',
		])

		3.times {
			expectLoginError(unknownClient1, 401)
		}

		// Now we are blocked for 1 second
		expectLoginError(validClient, 429)

		sleep(1000)

		// Login should work now again
		validClient.login()

		// Increase the period, so the previous attempts would be counted again (if they were not deleted)
		nuclosSession.patchSystemParameters([
				'SECURITY_IP_BLOCK_ATTEMPTS'         : '3',
				'SECURITY_IP_BLOCK_PERIOD_IN_SECONDS': '10',
		])

		// We should have again 3 attempts before being blocked
		3.times {
			expectLoginError(unknownClient1, 401)
		}

		// Now we are blocked for some time
		expectLoginError(validClient, 429)
	}

	@Test
	void _20_proxyIps() {
		// Allow 1 attempt per second
		nuclosSession.patchSystemParameters([
				'SECURITY_IP_BLOCK_ATTEMPTS'         : '1',
				'SECURITY_IP_BLOCK_PERIOD_IN_SECONDS': '1',
				'SECURITY_IP_BLOCK_PROXY_LIST': nuclosSession.ipAddress,
		])

		// Should not be blocked, because our IP is defined as a proxy
		2.times {
			expectLoginError(unknownClient1, 401)
		}

		// Now simulate a forwarded IP that is not on the whitelist
		unknownClient1.withHeaders(['X-Forwarded-For': '1.1.1.1']) {
			expectLoginError(unknownClient1, 401)
			// The forwarded IP should be blocked
			expectLoginError(unknownClient1, 429)
		}

		// Direct requests should still not be blocked
		expectLoginError(unknownClient1, 401)

		// Another forwarded IP should also not be blocked
		unknownClient1.withHeaders(['X-Forwarded-For': '2.2.2.2']) {
			expectLoginError(unknownClient1, 401)
		}

		// Try again the blocked IP, via an additional hop
		unknownClient1.withHeaders(['X-Forwarded-For': '2.2.2.2, 1.1.1.1']) {
			expectLoginError(unknownClient1, 429)
		}
	}

	static void expectLoginError(RESTClient client, int status) {
		expectRestException({
			client.login()
		}) {
			assert it.statusCode == status
		}
	}
}