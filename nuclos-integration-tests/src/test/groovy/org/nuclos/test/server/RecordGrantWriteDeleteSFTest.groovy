package org.nuclos.test.server

import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore
//@CompileStatic
class RecordGrantWriteDeleteSFTest extends AbstractNuclosTest {
	static RESTClient suClient = new RESTClient('nuclos', '')
	static RESTClient client

	static Map<String, Object> customer1 = new HashMap<>(customerNumber: '1', name: 'customer #1', active: true)
	static Map<String, Object> order1 = new HashMap<>(customer: customer1, orderNumber: 1, orderDate: DateTimeFormatter.ofPattern('yyyy-MM-dd').format(LocalDate.now()))
	static Map<String, Object> order2 = new HashMap<>(customer: customer1, orderNumber: 2, orderDate: '1970-01-01')
	static Map<String, Object> cAddress1 = new HashMap<>(customer: customer1, street: 'Street 1', city: 'City 1', zipCode: '12345', default: true)
	static Map<String, Object> cAddress2 = new HashMap<>(customer: customer1, street: 'Street 2', city: 'City 1', zipCode: '12345', default: false)
	static Map<String, Object> article1 = new HashMap<>(articleNumber: '1', name: 'article #1', price: 100, active: true)
	static Map<String, Object> orderPosition1 = new HashMap<>(order: order1, article: article1, price: 100, quantity: 3, shipped: true)
	static Map<String, Object> orderPosition2 = new HashMap<>(order: order1, article: article1, price: 100, quantity: 3, shipped: false)
	static Map<String, Object> invoice1 = new HashMap<>(customer: customer1, customerAddress: cAddress1, order: order1, invoiceDate: DateTimeFormatter.ofPattern('yyyy-MM-dd').format(LocalDate.now()), invoiceNumber: 1, number: '1', enabled: true)
	static Map<String, Object> invoice2 = new HashMap<>(customer: customer1, customerAddress: cAddress1, order: order1, invoiceDate: DateTimeFormatter.ofPattern('yyyy-MM-dd').format(LocalDate.now()), invoiceNumber: 2, number: '2', enabled: false)
	static Long word2Id;

	@Test
	void _00_setup() {
		RESTHelper.createUser('recordGrantSFTest', 'recordGrantSFTest', ['recordGrantSFTest'], nuclosSession)
		client = new RESTClient('recordGrantSFTest', 'recordGrantSFTest')

		client.login()

		//create BOs
		EntityObject<Long> eoCustomer1 = new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMER)
		eoCustomer1.setAttributes(new HashMap<String, Object>(customer1))
		customer1.put('id', client.save(eoCustomer1))

		EntityObject<Long> eoOrder1 = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		eoOrder1.setAttributes(new HashMap<String, Object>(order1))
		eoOrder1.setAttribute('customer', [id: eoCustomer1.getId()])
		order1.put('id', client.save(eoOrder1))

		EntityObject<Long> eoOrder2 = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		eoOrder2.setAttributes(new HashMap<String, Object>(order2))
		eoOrder2.setAttribute('customer', [id: eoCustomer1.getId()])
		order2.put('id', client.save(eoOrder2))

		EntityObject<Long> eoCAddress1 = new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS)
		eoCAddress1.setAttributes(new HashMap<String, Object>(cAddress1))
		eoCAddress1.setAttribute('customer', [id: eoCustomer1.getId()])
		cAddress1.put('id', client.save(eoCAddress1))

		EntityObject<Long> eoCAddress2 = new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS)
		eoCAddress2.setAttributes(new HashMap<String, Object>(cAddress2))
		eoCAddress2.setAttribute('customer', [id: eoCustomer1.getId()])
		cAddress2.put('id', client.save(eoCAddress2))

		EntityObject<Long> eoArticle1 = new EntityObject(TestEntities.EXAMPLE_REST_ARTICLE)
		eoArticle1.setAttributes(new HashMap<String, Object>(article1))
		article1.put('id', client.save(eoArticle1))

		EntityObject<Long> eoOrderPosition1 = new EntityObject(TestEntities.EXAMPLE_REST_ORDERPOSITION)
		eoOrderPosition1.setAttributes(new HashMap<String, Object>(orderPosition1))
		eoOrderPosition1.setAttribute('article', [id: eoArticle1.getId()])
		orderPosition1.put('id', client.save(eoOrderPosition1))

		EntityObject<Long> eoOrderPosition2 = new EntityObject(TestEntities.EXAMPLE_REST_ORDERPOSITION)
		eoOrderPosition2.setAttributes(new HashMap<String, Object>(orderPosition2))
		eoOrderPosition2.setAttribute('article', [id: eoArticle1.getId()])
		orderPosition2.put('id', client.save(eoOrderPosition2))

		EntityObject<Long> eoInvoice1 = new EntityObject(TestEntities.EXAMPLE_REST_INVOICE)
		eoInvoice1.setAttributes(new HashMap<String, Object>(invoice1))
		eoInvoice1.setAttribute('order', [id: eoOrder1.getId()])
		invoice1.put('id', client.save(eoInvoice1))

		EntityObject<Long> eoInvoice2 = new EntityObject(TestEntities.EXAMPLE_REST_INVOICE)
		eoInvoice2.setAttributes(new HashMap<String, Object>(invoice2))
		eoInvoice2.setAttribute('order', [id: eoOrder1.getId()])
		invoice2.put('id', client.save(eoInvoice2))
	}

	@Test
	void _10_testRecordGrantWriteSFSU() {
		suClient.login()

		//customer
		EntityObject<Long> eoCustomer1 = suClient.getEntityObject(TestEntities.EXAMPLE_REST_CUSTOMER, (Long) customer1.get('id'))

		List<EntityObject<Long>> depsOrder = suClient.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_ORDER, TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
		for (EntityObject<Long> eo : depsOrder) {
			eo.setAttribute('note', 'test1')
		}
		eoCustomer1.getDependents().put(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer', depsOrder)

		List<EntityObject<Long>> depsCAddress = suClient.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer')
		for (EntityObject<Long> eo : depsCAddress) {
			eo.setAttribute('city', 'test1')
		}
		eoCustomer1.getDependents().put(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer', depsCAddress)

		eoCustomer1.save()

		for (EntityObject<Long> eo : suClient.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_ORDER, TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')) {
			assert 'test1'.equals(eo.getAttribute('note'))
		}
		for (EntityObject<Long> eo : suClient.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer')) {
			assert 'test1'.equals(eo.getAttribute('city'))
		}


		//order
		EntityObject<Long> eoOrder1 = suClient.getEntityObject(TestEntities.EXAMPLE_REST_ORDER, (Long) order1.get('id'))

		List<EntityObject<Long>> depsOrderPosition = suClient.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_ORDERPOSITION, TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		for (EntityObject<Long> eo : depsOrderPosition) {
			eo.setAttribute('name', 'test1')
		}
		eoOrder1.getDependents().put(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order', depsOrderPosition)

		List<EntityObject<Long>> depsInvoice = suClient.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_INVOICE, TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order')
		for (EntityObject<Long> eo : depsInvoice) {
			eo.setAttribute('number', 'test1')
		}
		eoOrder1.getDependents().put(TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order', depsInvoice)

		eoOrder1.save()

		for (EntityObject<Long> eo : suClient.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_ORDERPOSITION, TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')) {
			assert 'test1'.equals(eo.getAttribute('name'))
		}
		for (EntityObject<Long> eo : suClient.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_INVOICE, TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order')) {
			assert 'test1'.equals(eo.getAttribute('number'))
		}

		suClient.logout()
	}

	@Test
	void _11_testRecordGrantWriteNotAllowedSF() {
		client.login()
		//customer
		EntityObject<Long> eoCustomer1 = client.getEntityObject(TestEntities.EXAMPLE_REST_CUSTOMER, (Long) customer1.get('id'))

		List<EntityObject<Long>> depsOrder = client.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_ORDER, TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
		for (EntityObject<Long> eo : depsOrder) {
			if (Date.parse('yyyy-MM-dd', (String) eo.getAttribute( 'orderDate')).before(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()))) {
				eo.setAttribute('note', 'test2')
			}
		}
		eoCustomer1.getDependents().put(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer', depsOrder)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			eoCustomer1.save()
		}
		eoCustomer1.getDependents().put(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer', new ArrayList<>())

		List<EntityObject<Long>> depsCAddress = client.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer')
		for (EntityObject<Long> eo : depsCAddress) {
			if (eo.getAttribute('default')) {
				eo.setAttribute('city', 'test2')
			}
		}
		eoCustomer1.getDependents().put(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer', depsCAddress)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			eoCustomer1.save()
		}
		eoCustomer1.getDependents().put(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer', new ArrayList<>())

		for (EntityObject<Long> eo : client.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_ORDER, TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')) {
			assert !'test2'.equals(eo.getAttribute('note'))
		}
		for (EntityObject<Long> eo : client.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer')) {
			assert !'test2'.equals(eo.getAttribute('city'))
		}

		//order
		EntityObject<Long> eoOrder1 = client.getEntityObject(TestEntities.EXAMPLE_REST_ORDER, (Long) order1.get('id'))

		List<EntityObject<Long>> depsOrderPosition = client.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_ORDERPOSITION, TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		for (EntityObject<Long> eo : depsOrderPosition) {
			if (eo.getAttribute('shipped')) {
				eo.setAttribute('name', 'test2')
			}
		}
		eoOrder1.getDependents().put(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order', depsOrderPosition)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			eoOrder1.save()
		}
		eoOrder1.getDependents().put(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_customer', new ArrayList<>())

		List<EntityObject<Long>> depsInvoice = client.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_INVOICE, TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order')
		for (EntityObject<Long> eo : depsInvoice) {
			if (!eo.getAttribute('enabled')) {
				eo.setAttribute('number', 'test2')
			}
		}
		eoOrder1.getDependents().put(TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order', depsInvoice)
		expectErrorStatus(Response.Status.FORBIDDEN) {
			eoOrder1.save()
		}
		eoOrder1.getDependents().put(TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order', new ArrayList<>())

		for (EntityObject<Long> eo : client.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_ORDERPOSITION, TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')) {
			assert !'test2'.equals(eo.getAttribute('name'))
		}
		for (EntityObject<Long> eo : client.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_INVOICE, TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order')) {
			assert !'test2'.equals(eo.getAttribute('number'))
		}
	}

	@Test
	void _12_testRecordGrantWriteAllowedSF() {
		//customer
		EntityObject<Long> eoCustomer1 = client.getEntityObject(TestEntities.EXAMPLE_REST_CUSTOMER, (Long) customer1.get('id'))

		List<EntityObject<Long>> depsOrder = client.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_ORDER, TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer').each { EntityObject<Long> eo ->
			if (!Date.parse('yyyy-MM-dd', (String) eo.getAttribute('orderDate')).before(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()))) {
				eo.setAttribute('note', 'test3')
			}
		}
		eoCustomer1.getDependents().put(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer', depsOrder)

		List<EntityObject<Long>> depsCAddress = client.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer').each { EntityObject<Long> eo ->
			if (!eo.getAttribute('default')) {
				eo.setAttribute('city', 'test3')
			}
		}
		eoCustomer1.getDependents().put(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer', depsCAddress)
		eoCustomer1.save()

		client.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_ORDER, TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer').each { EntityObject<Long> eo ->
			if (!Date.parse('yyyy-MM-dd', (String) eo.getAttribute('orderDate')).before(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()))) {
				assert 'test3'.equals(eo.getAttribute('note'))
			}
		}

		client.loadDependents(eoCustomer1, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer').each {eo ->
			if (!eo.getAttribute('default')) {
				assert 'test3'.equals(eo.getAttribute('city'))
			}
		}

		//order
		EntityObject<Long> eoOrder1 = client.getEntityObject(TestEntities.EXAMPLE_REST_ORDER, (Long) order1.get('id'))

		List<EntityObject<Long>> depsOrderPosition = client.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_ORDERPOSITION, TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order');
		depsOrderPosition.each { eo ->
			eo.setFlag(Flag.NONE)
			if (!eo.getAttribute('shipped')) {
				eo.setAttribute('name', 'test3')
			}
		}
		eoOrder1.getDependents().put(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order', depsOrderPosition)

		List<EntityObject<Long>> depsInvoice = client.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_INVOICE, TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order').each {eo ->
			eo.setFlag(Flag.NONE)
			if (eo.getAttribute('enabled')) {
				eo.setAttribute('number', 'test3')
			}
		}
		eoOrder1.getDependents().put(TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order', depsInvoice)
		eoOrder1.save()

		client.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_ORDERPOSITION, TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order').each { EntityObject<Long> eo ->
			if (!eo.getAttribute('shipped')) {
				assert 'test3'.equals(eo.getAttribute('name'))
			}
		}
		client.loadDependents(eoOrder1, TestEntities.EXAMPLE_REST_INVOICE, TestEntities.EXAMPLE_REST_INVOICE.fqn + '_order').each { EntityObject<Long> eo ->
			if (eo.getAttribute('enabled')) {
				assert 'test3'.equals(eo.getAttribute('number'))
			}
		}
	}

	@Ignore
	@Test
	void _20_testRecordGrantDeleteSFSU() {

	}

	@Ignore
	@Test
	void _21_testRecordGrantDeleteSF() {

	}
}
