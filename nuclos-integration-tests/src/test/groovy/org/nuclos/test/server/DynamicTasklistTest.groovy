package org.nuclos.test.server

import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.TestDataHelper

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DynamicTasklistTest extends AbstractNuclosTest {

	@Test
	void _05_queryDynTasklistWithSorting() {
		TestDataHelper.insertTestData(nuclosSession)

		// Make sure we can query a dynamic tasklist with sorting without any exceptions.
		// Especially this must not cause illegal SQL statements on Postgres (NUCLOS-7892).
		List<EntityObject<Long>> eos = nuclosSession.getEntityObjects(
				TestEntities.EXAMPLE_REST_DYNTASKSORDERANDCUSTOMERDTL,
				new QueryOptions(
						orderBy: ['example_rest_DynTasksOrderAndCustomerDTL_Number asc'].toSet()
				)
		)

		assert eos.size() > 5

		List<Object> numbers = eos*.getAttribute('Number')
		assert numbers.first() == 2001

		// Make sure the numbers are already sorted ascending
		assert numbers == numbers.toSorted()

		eos = nuclosSession.getEntityObjects(
				TestEntities.EXAMPLE_REST_DYNTASKSORDERANDCUSTOMERDTL,
				new QueryOptions(
						orderBy: ['example_rest_DynTasksOrderAndCustomerDTL_Number desc'].toSet()
				)
		)

		// Make sure the numbers are already sorted descending
		assert eos*.getAttribute('Number') == numbers.toSorted().reverse()
	}

	@Test
	void _10_queryDynTasklistWithFilter() {
		List<EntityObject<Long>> eos = nuclosSession.getEntityObjects(
				TestEntities.EXAMPLE_REST_DYNTASKSORDERANDCUSTOMERDTL,
				new QueryOptions(
						where: 'example_rest_DynTasksOrderAndCustomerDTL_Number LIKE \'2%\'',
						orderBy: ['example_rest_DynTasksOrderAndCustomerDTL_Number asc'].toSet()
				)
		)

		assert eos.size() == 2

		List<Object> numbers = eos*.getAttribute('Number')
		assert numbers == [2001, 22222]
	}

	@Test
	void _15_dynTasklistWithImage() {
		// Check the entity meta data, make sure the Image attribute has type "Image"
		JSONObject meta = nuclosSession.getEntityMeta(
				TestEntities.EXAMPLE_REST_DYNTASKSORDERANDCUSTOMERDTL
		)
		JSONObject attributes = meta.getJSONObject('attributes')
		assert attributes.getJSONObject('Status').getString('type') == 'Image'

		// Check the EO contents, there must be image links for the image attribute
		List<EntityObject<Long>> eos = nuclosSession.getEntityObjects(
				TestEntities.EXAMPLE_REST_DYNTASKSORDERANDCUSTOMERDTL,
				new QueryOptions(
						where: 'example_rest_DynTasksOrderAndCustomerDTL_Number = 10020140'
				)
		)

		assert eos.size() == 1

		final String href = eos.first().getAttributeImageLink('Status').getString('href')
		assert href

		// Make sure the link really points to an image
		InputStream is = nuclosSession.getInputStream(href)
		BufferedImage image = ImageIO.read(is)
		assert image
	}
}
