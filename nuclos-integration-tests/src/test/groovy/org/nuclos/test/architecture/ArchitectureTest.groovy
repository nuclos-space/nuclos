package org.nuclos.test.architecture

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses
import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices

import org.junit.runner.RunWith

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchIgnore
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.junit.ArchUnitRunner
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.library.GeneralCodingRules
import com.tngtech.archunit.thirdparty.com.google.common.collect.Iterators

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
		locations = NuclosClassesLocationProvider.class,
		importOptions = [
				ImportOption.DontIncludeTests.class,
				ImportOption.DontIncludeJars.class,
				DontIncludeSynthetica.class,
				DontIncludeBuildTmp.class,
		]
)
@CompileStatic
class ArchitectureTest {

	@ArchTest
	static void checkClassCount(JavaClasses classes) {
		// There are ~19k Nuclos classes at the moment
		int classCount = Iterators.size(classes.iterator())
		assert classCount > 15000
		assert classCount < 30000
	}

	@ArchTest
	@ArchIgnore		// TODO: sun.* is currently used by 4 classes - fix and unignore this test
	public static final ArchRule noSun = noClasses().should().accessClassesThat().resideInAPackage("sun.*")

	@ArchTest
	@ArchIgnore 	// TODO: Fix current 14 cycles and unignore this test.
	public static final ArchRule noCycles = slices().matching("org.nuclos.(*)..").should().beFreeOfCycles()

	@ArchTest
	@ArchIgnore		// TODO: Fix current 139 uses of System.out and System.err, and unignore this test.
	public static final ArchRule noStandardStreams = GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS

	@ArchTest
	@ArchIgnore		// TODO: Fix current 106 violations and unignore this test.
	public static final ArchRule noGenericExceptions = GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS

	@ArchTest
	public static final ArchRule noJavaUtilLogging = GeneralCodingRules.NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING
}

