package org.nuclos.test.webclient.pageobjects.dashboard

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.By

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class DashboardItem extends AbstractPageObject {

	final NuclosWebElement item
	final String type
	final int x
	final int y
	final Integer rows
	final Integer cols

	DashboardItem(NuclosWebElement item) {
		this.item = item
		this.type = item.getAttribute('item-type')
		this.x = item.getAttribute('item-x') as Integer
		this.y = item.getAttribute('item-y') as Integer
		this.rows = item.getAttribute('item-rows') as Integer
		this.cols = item.getAttribute('item-cols') as Integer
	}

	NuclosWebElement row(int rowIndex) {
		return item.$('nuc-entity-object-grid ag-grid-angular .ag-body-container [row-index="' + rowIndex + '"]')
	}

	int countRows() {
		return item.$$('nuc-entity-object-grid ag-grid-angular .ag-body-container [role="row"]').size()
	}

	void clickRow(int rowIndex) {
		row(rowIndex).click()
	}

}
