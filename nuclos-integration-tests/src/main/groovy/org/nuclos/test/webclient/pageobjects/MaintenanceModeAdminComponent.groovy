package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.getUrlHash

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

@CompileStatic
class MaintenanceModeAdminComponent extends AbstractPageObject {

	static void open() {
		getUrlHash('#maintenance')
	}

	static void activateMaintenaceMode() {
		$('#activate-maintenace-mode-button').click()
	}

	static void deactivateMaintenaceMode() {
		$('#deactivate-maintenace-mode-button').click()
	}
	static String status() {
		$('#maintenance-mode-status').text
	}
}
