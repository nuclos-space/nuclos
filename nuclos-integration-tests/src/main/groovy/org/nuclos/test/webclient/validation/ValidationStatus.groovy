package org.nuclos.test.webclient.validation

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
enum ValidationStatus {
	VALIDATING,
	VALID,
	INVALID,
	MISSING;

	static ValidationStatus fromCssClasses(String cssClasses) {
		for (ValidationStatus status : ValidationStatus.values()) {
			if (cssClasses.contains('nuc-validation-' + status.name().toLowerCase())) {
				return status
			}
		}

		return null
	}
}
