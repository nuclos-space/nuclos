package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RegistrationComponent extends AbstractPageObject {
	static String containerSelector = '#registration-container'

	static NuclosWebElement findInput(String name) {
		$("$containerSelector input[name=\"$name\"]")
	}

	static void setInputValue(String name, String value) {
		NuclosWebElement input = findInput(name)
		input.clear()
		input.sendKeys(value)
	}

	static void setUsername(String value) {
		setInputValue('username', value)
	}

	static void setPassword(String value) {
		setInputValue('password', value)
	}

	static void setFirstname(String value) {
		setInputValue('firstname', value)
	}

	static void setLastname(String value) {
		setInputValue('lastname', value)
	}

	static void setEmail(String value) {
		setInputValue('email', value)
	}

	static void setEmail2(String value) {
		setInputValue('email2', value)
	}

	static void togglePrivacyConsent() {
		findInput('privacyconsent').click()
	}

	static void submit() {
		$("$containerSelector button[type=\"submit\"]").click()
	}

	static String getMessage() {
		$("$containerSelector .alert").text?.trim()
	}

	static String getErrorMessage() {
		$("$containerSelector .alert.alert-danger")?.text?.trim()
	}

	static String getSuccessMessage() {
		$("$containerSelector .alert.alert-success")?.text?.trim()
	}

	static void submitAndAssertError(List<String> expectedMessage = null) {
		submit()

		String errorMessage = errorMessage
		assert errorMessage

		if (expectedMessage) {
			assert expectedMessage.find { errorMessage.contains(it) }
		}
	}
}
