package org.nuclos.test.webclient.pageobjects.search


import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

@CompileStatic
class StringSearchComponent {
	final NuclosWebElement inputElement

	StringSearchComponent(final NuclosWebElement containerElement) {
		this.inputElement = containerElement
	}

	void setInput(String value) {
		inputElement.click()
		inputElement.value = value
	}

	String getInput() {
		inputElement.value
	}

}
