package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.sleep
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

@CompileStatic
class SearchfilterDropdown {

	void open() {
		if (!open) {
			NuclosWebElement link = dropdownLink
			if (link) {
				link.click()
			}
			/* waitFor {
				if (open) {
					return true
				}
				// FIXME: This is a workaround for NUCLOS-7243 - a second click should not be necessary.
				link.click()
			}*/
		}
	}

	void close() {
		if (open) {
			NuclosWebElement link = dropdownLink
			if (link) {
				link.click()
			}
		}
	}

	void selectEntry(String text) {
		if (!open) {
			open()
		}

		def drpdwnEntries = entries
		for (int i = 0; i < drpdwnEntries.size(); i++) {
			if (drpdwnEntries[i] == text) {
				NuclosWebElement element = dropdownElements[i];
				NuclosWebElement drpdwnMenu = dropdownMenu;

				while (!element.isDisplayed() && drpdwnMenu.canScrollBottom()) {
					drpdwnMenu.scrollBottom()
					sleep(100)
				}

				if (element.isDisplayed()) {
					element.click()
				} else {
					throw new NoSuchElementException("No such visible option in dropdown: $text")
				}
				return
			}
		}

		throw new NoSuchElementException("No such option in dropdown: $text")
	}

	boolean hasEntry(String text) {
		open()
		def drpdwnEntries = entries
		for (int i = 0; i < drpdwnEntries.size(); i++) {
			if (drpdwnEntries[i] == text) {
				return true
			}
		}
		return false
	}

	/**
	 * Returns the current searchfilter name.
	 *
	 * @return
	 */
	String getSelectedEntry() {
		trimText(dropdownLink.getAttribute('innerText'))
	}

	NuclosWebElement[] getDropdownElements() {
		NuclosWebElement[] elements = dropdownMenu.$$('a')
		return elements
	}

	List<String> getEntries() {
		List<String> result = []
		NuclosWebElement[] drpdwnElements = dropdownElements
		for (int i = 0; i < drpdwnElements.size(); i++) {
			NuclosWebElement element = drpdwnElements[i]
			result.add(trimText(element.getAttribute('innerText')))
		}
		result
	}

	boolean isOpen() {
		dropdownMenu?.displayed
	}

	NuclosWebElement getDropdownContainer() {
		$('#searchfilter-selector-dropdown')
	}

	NuclosWebElement getDropdownLink() {
		$('#searchfilter-selector-dropdown-link')
	}

	NuclosWebElement getDropdownMenu() {
		$('#searchfilter-selector-dropdown-menu')
	}

	private String trimText(String text) {
		return text?.replace('\u200B', '')?.trim()
	}

}
