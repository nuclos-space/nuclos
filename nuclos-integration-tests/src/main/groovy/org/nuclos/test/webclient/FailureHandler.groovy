package org.nuclos.test.webclient

import org.nuclos.test.log.Log

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class FailureHandler {
	static String failure
	static Throwable t

	private static boolean isFirstFailure() {
		return !t && !failure
	}

	/**
	 * Handles any failure (e.g. assertion errors) and fails the build immediately.
	 *
	 * Tries to gather some useful information (browser logs, screenshot, last URL) before the exit.
	 * This might cause another error. In this case only the errors are logged and the system is exited.
	 *
	 * @param failure
	 * @param t
	 */
	static void fail(String failure, Throwable t) {
		// Abort if there is another failure after the first one
		if (!isFirstFailure()) {
			Log.error "FAILURE: " + failure, t
			Log.error("ORIGINAL FAILURE: " + FailureHandler.failure, FailureHandler.t)
			shutdown()
		}

		if (!t) {
			t = new Throwable(failure)
		}

		FailureHandler.failure = failure
		FailureHandler.t = t

		AbstractWebclientTest.printBrowserDebugInfos()

		Log.error "FAILURE: " + failure, t

		shutdown()
	}

	/**
	 * Tries to close all connections and exits the system.
	 */
	static void shutdown() {
		Log.warn('Shutting down...')
		AbstractWebclientTest.shutdown()
		System.exit(1)
	}
}
