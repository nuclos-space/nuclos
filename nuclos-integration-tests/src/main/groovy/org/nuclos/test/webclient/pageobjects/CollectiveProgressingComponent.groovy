package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.waitUntilTrue
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * Represents the collective progressing component.
 */
@CompileStatic
class CollectiveProgressingComponent extends AbstractPageObject {

	static boolean isCollectiveProcessing() {
		$("nuc-collective-processing").isDisplayed()
	}

	static void showDetails() {
		$("#backToDetails").click()
	}

	static void showCollectiveProcessing() {
		$("#backToCollectiveProcessing").click()
	}

	static void cancelCollectiveProcessing() {
		$("#cancelCollectiveProcessing").click()
	}

	static List<NuclosWebElement> getStateChangeActions() {
		if (isPresent('.collective-processing-state-change-action')) {
			return $$('.collective-processing-state-change-action')
		} else {
			return new ArrayList<NuclosWebElement>()
		}
	}

	static List<NuclosWebElement> getObjectActions() {
		if (isPresent('.collective-processing-object-action')) {
			return $$('.collective-processing-object-action')
		} else {
			return new ArrayList<NuclosWebElement>()
		}
	}

	static List<NuclosWebElement> getFunctionActions() {
		if (isPresent('.collective-processing-function-action')) {
			return $$('.collective-processing-function-action')
		} else {
			return new ArrayList<NuclosWebElement>()
		}
	}

	static void doStateChange(String stateName) {
		getStateChangeActions().find{
			it.$('a').text?.startsWith(stateName)
		}.$('a').click()
	}

	static void doAction(String actionName) {
		getObjectActions().find{
			it.$('a').text?.startsWith(actionName)
		}.$('a').click()
	}

	static void doFunction(String funcName) {
		getFunctionActions().find{
			it.$('a').text?.startsWith(funcName)
		}.$('a').click()
	}

	static List<String>  getStateChangeNames() {
		def result = []
		getStateChangeActions().each {
			result.add(it.$('a').text)
		}
		return result
	}

	static List<String>  getActionNames() {
		def result = []
		getObjectActions().each {
			result.add(it.$('a').text)
		}
		return result
	}

	static List<String>  getFuncNames() {
		def result = []
		getFunctionActions().each {
			result.add(it.$('a').text)
		}
		return result
	}

	static boolean isProgress() {
		isPresent('nuc-collective-processing-progress')
	}

	static void assertProgressSuccess(int elements) {
		for (int i = 0; i < elements; i++) {
			assert getProgressSuccess(i)
			assert getProgressInfoRowNumber(i) == i+1 + ''
			assert getProgressName(i).length() > 0
			assert getProgressNote(i).length() == 0
		}
	}

	static List<String> getProgressGenObjectNames(int elements) {
		def result = []
		for (int i = 0; i < elements; i++) {
			result.add(getProgressGenObjName(i))
		}
		return result
	}

	static String getProgressInfoRowNumber(int rowIndex) {
		getProgressValue(rowIndex, 'infoRowNumber')
	}

	static String getProgressName(int rowIndex) {
		getProgressValue(rowIndex, 'name')
	}

	static String getProgressGenObjName(int rowIndex) {
		getProgressValue(rowIndex, 'boGenerationResult.bo.title')
	}

	static String getProgressNote(int rowIndex) {
		getProgressValue(rowIndex, 'note')
	}

	static boolean getProgressSuccess(int rowIndex) {
		getProgressValue(rowIndex, 'success')
	}

	private static NuclosWebElement getProgressCell(int rowIndex, String col) {
		$('nuc-collective-processing-progress ag-grid-angular [row-index="' + rowIndex + '"] [col-id="' + col + '"]')
	}

	private static getProgressValue(int rowIndex, String col) {
		NuclosWebElement cell
		waitUntilTrue({ cell = getProgressCell(rowIndex, col); cell != null})
		if (cell == null) {
			throw new NoSuchElementException('Cell with index "' + rowIndex + '" and column "'+ col + '" not found')
		}
		if (col == 'success') {
			cell.$('.fa-check-square-o') != null
		} else {
			cell.text
		}
	}

	static void closeProgress() {
		$("#closeProgress").click()
	}

}
