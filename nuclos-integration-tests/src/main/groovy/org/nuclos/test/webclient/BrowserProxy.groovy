package org.nuclos.test.webclient

import org.nuclos.test.NuclosTestProxy
import org.nuclos.test.log.Log
import org.openqa.selenium.Proxy

import groovy.transform.CompileStatic
import net.lightbody.bmp.client.ClientUtil

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class BrowserProxy extends NuclosTestProxy {

	private final WebclientTestContext context

	BrowserProxy(final WebclientTestContext context) {
		super([context.nuclosWebclientHost])

		this.context = context
	}

	Proxy getSeleniumProxy() {
		Proxy proxy
		if (started) {
			InetAddress proxyHost = InetAddress.getByName(context.nuclosWebclientHost)
			proxy = ClientUtil.createSeleniumProxy(browserMobProxy, proxyHost)
//			proxy.noProxy = 'bs-local.com'
		} else {
			Log.warn('Proxy not started!')
		}
		return proxy
	}
}
