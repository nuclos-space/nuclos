package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import groovy.transform.CompileStatic

/**
 * Represents the (sub-)entity-object component when opened in a detail modal.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObjectDialog extends EntityObjectComponent {
	@Override
	String getSelectorPrefix() {
		'nuc-detail-dialog '
	}

	@Override
	ListOfValues getLOV(final String attributeName) {
		ListOfValues.findByAttributeInModal(attributeName)
	}

	static String getAlertText() {
		$('#errorMessage')?.text?.trim()
	}
}
