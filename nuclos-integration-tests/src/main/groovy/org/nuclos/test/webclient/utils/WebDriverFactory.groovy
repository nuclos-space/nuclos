package org.nuclos.test.webclient.utils


import java.util.logging.Level

import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.WebclientTestContext
import org.openqa.selenium.Proxy
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.logging.LoggingPreferences
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.testcontainers.containers.BrowserWebDriverContainer

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class WebDriverFactory {

	private static final String SELENIUM_CAPABILITIES_PREFIX = 'selenium.capabilities.'

	static RemoteWebDriver remoteWebDriver(
			final DesiredCapabilities caps,
			final WebclientTestContext context,
			final String dockerImage = null
	) {
		final Proxy proxy = context.browserProxy.getSeleniumProxy()

		// Enable browser console logging
		LoggingPreferences logPrefs = new LoggingPreferences()
		logPrefs.enable(LogType.BROWSER, Level.ALL)
		caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs)
		caps.setCapability(CapabilityType.PROXY, proxy)

		setCapabilitiesFromSystemProperties(caps)

		// If no selenium server is defined via system properties, but we know a Docker image,
		// try to instantiate a selenium server via Docker.
		if (!context.seleniumServer && dockerImage) {
			try {
				AbstractWebclientTest.browserContainer = new BrowserWebDriverContainer<>(dockerImage)
						.withDesiredCapabilities(caps)

				// TODO: Recording does not work yet
//					.withRecordingMode(
//					BrowserWebDriverContainer.VncRecordingMode.RECORD_ALL,
//					new File("/tmp/recording/")
//			)
//					.withRecordingFileFactory(new DefaultRecordingFileFactory())

				AbstractWebclientTest.browserContainer.start()
				return AbstractWebclientTest.browserContainer.webDriver
			} catch (Exception e) {
				Log.error 'Failed to start selenium server via Docker', e
			}
		}

		return new RemoteWebDriver(new URL(context.seleniumServer), caps)
	}

	private static void setCapabilitiesFromSystemProperties(DesiredCapabilities caps) {
		System.properties.keySet()
				.collect { "$it" }
				.findAll { it.startsWith(SELENIUM_CAPABILITIES_PREFIX) }
				.each
				{
					String capability = it.substring(SELENIUM_CAPABILITIES_PREFIX.size())
					caps.setCapability(capability, System.getProperty(it))
				}
	}

	static Closure remoteFirefox = { WebclientTestContext context ->
		DesiredCapabilities caps = DesiredCapabilities.firefox()
		remoteWebDriver(caps, context, 'selenium/standalone-firefox:3.141.59-gold')
	}

	static Closure remoteChrome = { WebclientTestContext context ->
		DesiredCapabilities caps = DesiredCapabilities.chrome()
		remoteWebDriver(caps, context, 'selenium/standalone-chrome:3.141.59-gold')
	}

	static Closure remotePhantomJS = { WebclientTestContext context ->
		DesiredCapabilities caps = DesiredCapabilities.phantomjs()
		remoteWebDriver(caps, context)
	}

	static Closure remoteIE = { WebclientTestContext context ->
		DesiredCapabilities caps = DesiredCapabilities.internetExplorer()
		remoteWebDriver(caps, context)
	}

	static Closure remoteSafari = { WebclientTestContext context ->
		DesiredCapabilities caps = DesiredCapabilities.safari()
		remoteWebDriver(caps, context)
	}
}
