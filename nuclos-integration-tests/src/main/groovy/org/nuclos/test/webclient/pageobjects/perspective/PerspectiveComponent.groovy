package org.nuclos.test.webclient.pageobjects.perspective

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class PerspectiveComponent {

	static PerspectiveModal newPerspective() {
		$('#newPerspective').click()
		return new PerspectiveModal()
	}

	/**
	 * Returns all currently visible perspectives.
	 *
	 * @return
	 */
	static List<Perspective> getPerspectives() {
		$$('button.perspective-button:not(.perspective-button-new)').collect {
			String uid = it.getAttribute('uid')
			new Perspective(
					element: it,
					id: uid,
					name: it.text
			)
		}
	}

	/**
	 * Searches for a perspective with the given name in the current page.
	 *
	 * @param name
	 * @return
	 */
	static Perspective getPerspective(String name) {
		perspectives.find {
			it.name == name
		}
	}
}
