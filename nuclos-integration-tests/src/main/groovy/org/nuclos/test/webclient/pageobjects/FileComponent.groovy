package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import java.util.regex.Matcher

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class FileComponent extends AbstractPageObject {
	final NuclosWebElement element

	FileComponent(NuclosWebElement element) {
		this.element = element
	}

	void setFile(File file) {
		NuclosWebElement input = element.$('input')
		if (file) {
			input.sendKeys(file.absolutePath)
		}
	}

	void clearFile() {
		element.mouseover()

		AbstractWebclientTest.waitFor {
			NuclosWebElement reset = element.getParent().$('.reset-document')
			reset?.click()
			return reset
		}
	}

	String getImageUrl() {
		String result

		String value = fileuploadContainer.getCssValue('background')
		Matcher m = value =~ /url\("(.+?)"\)/
		if (m.find()) {
			result = m.group(1)
		}

		return result
	}

	String getText() {
		element.text
	}

	boolean hasImage() {
		element.getCssValue('background') != 'none'
	}

	NuclosWebElement getFileuploadContainer() {
		element
	}

	void downloadFile() {
		element.mouseover()
		$('.icons .fa-download').click()
	}


	void deleteFile() {
		element.mouseover()
		$('.icons .fa-trash').click()
	}
}
