package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.NuclosWebElement

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class MessageModal {
	NuclosWebElement element

	String title
	String message

	void confirm() {
		element.$('div.modal-footer #button-ok').click()
	}

	void decline() {
		element.$('div.modal-header button.close').click()
	}
}
