package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.getUrlHash

import groovy.transform.CompileStatic

/**
 * Represents the locale chooser component, which should be available on all Webclient pages.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class SessionInfo extends AbstractPageObject {
	static void open() {
		getUrlHash('/session-info')
	}

	static String getSessionId() {
		$('dl[title="sessionId"] dd').text.replace('"', '')
	}
}
