package org.nuclos.test

import org.json.JSONObject

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObjectAttributesSerializer extends StdSerializer<Map<String, Object>> {
	protected EntityObjectAttributesSerializer() {
		super(Map.class)
	}

	@Override
	void serialize(
			final Map<String, Object> attributesMap,
			final JsonGenerator gen,
			final SerializerProvider provider
	) throws IOException {
		gen.writeStartObject()

		attributesMap.each { key, value ->
			if (value instanceof JSONObject) {
				value = value.toString()
			}
			gen.writeObjectField(key, value)
		}

		gen.writeEndObject()
	}
}
