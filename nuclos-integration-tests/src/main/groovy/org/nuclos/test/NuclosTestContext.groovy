package org.nuclos.test

import java.text.DateFormat
import java.text.SimpleDateFormat

import org.nuclos.test.log.Log

import groovy.transform.CompileStatic

/**
 * The context should not be changed by the tests!
 * It is therefore marked as @Immutable, although it still has some fields that are not
 * immutable. Those are ignored for now via "knownImmutableClasses".
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@Singleton(lazy = true)
class NuclosTestContext {
	/**
	 * The default timeout used by waitForAngular etc.
	 */
	final int DEFAULT_TMEOUT = 60

	// The following settings can be overriden with System Properties
	final String nuclosServerProtocol = initFromSystemProperty('nuclos.server.protocol', 'http')
	final String nuclosServerHost = initFromSystemProperty('nuclos.server.host', '127.0.0.1')
	final String nuclosServerPort = initFromSystemProperty('nuclos.server.port', '8080')
	final String nuclosServerContext = initFromSystemProperty('nuclos.server.context', 'nuclos-war')

	final Locale locale = initFromSystemProperty('locale', { String name ->
		DateFormat.getAvailableLocales().find {
			it.toString() == name
		}
	}, Locale.GERMANY)

	final DateFormat dateFormat = locale.language == 'en' ? new SimpleDateFormat('MM/dd/yyyy') : new SimpleDateFormat('dd.MM.yyyy')

	final String nuclosServer = "$nuclosServerProtocol://$nuclosServerHost:$nuclosServerPort/$nuclosServerContext".toString()

	static <T> T initFromSystemProperty(String propertyName, T defaultValue) {
		initFromSystemProperty(propertyName, null, defaultValue)
	}

	/**
	 * Tries to read the system property with the given name.
	 *
	 * @param propertyName
	 * @param initClosure Optional, will be applied to the system property value if not null
	 * @param defaultValue
	 * @return
	 */
	static <T> T initFromSystemProperty(String propertyName, Closure initClosure, def defaultValue) {
		String property = System.getProperty(propertyName)
		def result

		if (initClosure) {
			if (property) {
				result = initClosure(property)
			}
		} else {
			result = property
		}

		if (property == null) {
			result = defaultValue
		}

		Log.info "$propertyName = $result"

		return result
	}
}
