package org.nuclos.test

import groovy.transform.CompileStatic
import groovy.transform.Immutable

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Immutable
@CompileStatic
class RestLink {
	final String href
	final Set<String> methods
}
