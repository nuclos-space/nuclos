package nuclet.test.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.RuleContext;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.StateChangeFinalRule;


/** @name
 * @description
 * @usage
 * @change
 */
@Rule(name = "H2 Truncate Tables", description = "H2 Truncate Tables")
public class H2TruncateTables implements StateChangeFinalRule {

	public void changeStateFinal(StateChangeContext context) throws BusinessException {
		doIt(context);
		emptyDocumentDir(context);
	}

	private void emptyDocumentDir(RuleContext context) {
		context.log("Emtying document dir...");
		try {
			java.io.File dir = null;

			Class<?> c = Class.forName("org.nuclos.server.common.NuclosSystemParameters");
			Method getDirectory = c.getDeclaredMethod("getDirectory", String.class);
			getDirectory.setAccessible(true);
			dir = (java.io.File)getDirectory.invoke(null, "nuclos.data.documents.path");

			if (dir.exists()) {
				org.apache.commons.io.FileUtils.cleanDirectory(dir);
			}

		} catch (Exception ex) {
			context.logError("Exception: ", ex);
		}
	}

	private void doIt(RuleContext context) {
		context.log("Truncate tables...");

		Connection conn = null;
		try {
			Object instance = null;

			context.log("Getting SpringDataBaseHelper...");
			Class<?> classSpringDataBaseHelper = Class.forName("org.nuclos.server.database.SpringDataBaseHelper");
			Class<?> classDbAccess = Class.forName("org.nuclos.server.dblayer.DbAccess");
			Method getInstance = classSpringDataBaseHelper.getDeclaredMethod("getInstance");
			Method getDbAccess = classSpringDataBaseHelper.getDeclaredMethod("getDbAccess");
			Method getSchemaName = classDbAccess.getDeclaredMethod("getSchemaName");
			getInstance.setAccessible(true);
			getDbAccess.setAccessible(true);
			instance = getInstance.invoke(null);
			final Object dbAccess = getDbAccess.invoke(instance);
			final String schema = (String)getSchemaName.invoke(dbAccess);

			context.log("Getting datasource...");
			Field dataSourceField = classSpringDataBaseHelper.getDeclaredField("dataSource");
			dataSourceField.setAccessible(true);
			DataSource dataSource = (DataSource) dataSourceField.get(instance);

			context.log("Getting connection...");
			conn = dataSource.getConnection();
			context.log("Creating statement...");
			Statement stmt = conn.createStatement();

			context.log("Deactivating integrity checks...");
			disableConstraints(dbAccess, conn, context);

			context.log("Querying tables to truncate...");
			final String sql = "SELECT TABLE_SCHEMA, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES"
					+ " WHERE TABLE_SCHEMA = '" + schema + "' "
					+ " AND (TABLE_TYPE = 'BASE TABLE' OR TABLE_TYPE = 'TABLE')"
					+ " AND LOWER(TABLE_NAME) NOT LIKE 't_ad_%'"
					+ " AND LOWER(TABLE_NAME) NOT LIKE 't_md_%'"
					+ " AND LOWER(TABLE_NAME) NOT LIKE '%truncatetables%'" // Can't be truncated because it is locked at this moment
					+ " AND LOWER(TABLE_NAME) NOT LIKE 't_ud_%'"
					//+ " OR TABLE_NAME = 'T_MD_PREFERENCE'" Ab 4.18 brauchen wir brauchen die Table-Preferences, also kein truncate mehr, wir loeschen (siehe unten)..
					+ " OR LOWER(TABLE_NAME) LIKE 't_ud_documentfile%'"
					+ " OR LOWER(TABLE_NAME) = 't_md_preference_selected'"
					+ " OR LOWER(TABLE_NAME) = 't_ad_news'";
			ResultSet rs = stmt.executeQuery(sql);

			List<String> tablenames = new ArrayList<>();
			while (rs.next()) {
				tablenames.add(rs.getString("TABLE_NAME"));
			}

			String truncate = "truncate table " + tablenames.stream().collect(Collectors.joining(", "));
			context.log("Truncate: " + truncate);


			if (isH2(dbAccess)) {
				for (String table: tablenames) {
					context.log("Truncate: " + schema + "." + table);
					Statement stmtTruncate = conn.createStatement();
					stmtTruncate.execute("TRUNCATE TABLE " + schema + "." + table);
					stmtTruncate.close();
				}

			} else {

				// TODO something is blocking here - run truncate in new Thread, otherwise db will be locked
				new Thread(() -> {
					try {

						Connection truncateConn = dataSource.getConnection();
						Statement stmtTruncateAll = truncateConn.createStatement();
						stmtTruncateAll.execute(truncate);
						stmtTruncateAll.close();
						truncateConn.close();
					} catch (Exception e) {
						context.logError("Unable to truncate table", e);
					}
				}).start();
			}

			Statement stmtDeletePrefs = conn.createStatement();
			stmtDeletePrefs.execute("DELETE FROM " + schema + ".T_MD_PREFERENCE WHERE STRUID_T_MD_NUCLET IS NULL");
			stmtDeletePrefs.close();

			context.log("Activating integrity checks...");
			enableConstraints(dbAccess, conn, context);

			context.log("Commiting...");
			stmt.execute("COMMIT");

			context.log("Commited.");
		} catch (Exception ex) {
			context.logError("Exception: ", ex);
		} finally {
			if (conn != null) {
				try {
					context.log("Closing connection...");
					conn.close();
				} catch (SQLException e) {
					context.logError("Failed to close connection", e);
				}
			}
		}
		context.log("Truncate tables: done");
	}

	private void disableConstraints(final Object dbAccess, final Connection conn, RuleContext context) throws SQLException {

		final Statement stmt = conn.createStatement();
		if (isH2(dbAccess)) {
			// h2
			stmt.execute("SET REFERENTIAL_INTEGRITY FALSE");

		} else {
			// postgres
			final String sql =
					"SELECT nspname, relname, conname \n" +
							"FROM pg_constraint\n" +
							"INNER JOIN pg_class ON conrelid=pg_class.oid\n" +
							"INNER JOIN pg_namespace ON pg_namespace.oid=pg_class.relnamespace\n" +
							"ORDER BY CASE WHEN contype='f' THEN 0 ELSE 1 END,contype,nspname,relname,conname;";

			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				final String nspname = rs.getString("nspname");
				final String relname = rs.getString("relname");
				final String conname = rs.getString("conname");

				final String dropConstraintSql = "ALTER TABLE \"" + nspname + "." + relname + "\" DROP CONSTRAINT \"" + conname + "\"";
				context.log("Drop constraint: " + dropConstraintSql);

				try {
					final Statement dropstmt = conn.createStatement();
					dropstmt.execute(dropConstraintSql);
				} catch (Exception e) {
					context.log("Unable to drop constraint: " + dropConstraintSql);
				}
			}
		}
	}

	private void enableConstraints(final Object dbAccess, final Connection conn, RuleContext context) throws SQLException {

		final Statement stmt = conn.createStatement();
		if (isH2(dbAccess)) {
			// h2
			stmt.execute("SET REFERENTIAL_INTEGRITY TRUE");

		} else {
			// postgres
			final String sql =
					"SELECT nspname, relname, conname, pg_get_constraintdef(pg_constraint.oid) as constraintdef " +
							"FROM pg_constraint " +
							"INNER JOIN pg_class ON conrelid=pg_class.oid " +
							"INNER JOIN pg_namespace ON pg_namespace.oid=pg_class.relnamespace " +
							"ORDER BY CASE WHEN contype='f' THEN 0 ELSE 1 END DESC,contype DESC,nspname DESC,relname DESC,conname DESC;";

			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				final String nspname = rs.getString("nspname");
				final String relname = rs.getString("relname");
				final String conname = rs.getString("conname");
				final String constraintdef = rs.getString("constraintdef");

				final String addConstraintSql = "ALTER TABLE \"" + nspname + "." + relname + "\" ADD CONSTRAINT \"" + conname + "\" " + constraintdef;
				context.log("Add constraint: " + addConstraintSql);

				try {
					final Statement addstmt = conn.createStatement();
					addstmt.execute(addConstraintSql);
				} catch (Exception e) {
					context.log("Unable to add constraint: " + addConstraintSql);
				}
			}
		}
	}

	private boolean isH2(final Object dbAccess) {
		return dbAccess.getClass().getCanonicalName().equals("org.nuclos.server.dblayer.impl.h2.H2DBAccess");
	}
}
