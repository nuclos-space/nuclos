package org.nuclos.client.i18n.language.data;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import org.nuclos.client.ui.Icons;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.i18n.language.data.DataLanguageVO;

public class DataLanguageSelectionPanel extends JPanel {

	private JPanel pnlAvialables;
	private JPanel pnlSelectionButtons;
	private JPanel pnlSelected;
	private DataLanguageViewListener listener;
	
	private DataLanguageTransferHandler transferHandlerSource;
	private DataLanguageTransferHandler transferHandlerTarget;
	
	private JButton btnAdd;
	private JButton btnRemove;	
	private JButton btnUp;
	private JButton btnDown;
	
	private JTable tblAvailables;
	private JTable tblSelecteds;
			
	public DataLanguageSelectionPanel(DataLanguageViewListener listener) {	
		super(new GridBagLayout());
		this.listener = listener;
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
				
		pnlAvialables = new JPanel(new BorderLayout());
		pnlSelectionButtons = new JPanel(new GridBagLayout());
		pnlSelected = new JPanel(new BorderLayout());
		c.weightx= 1;
		c.weighty=1;
		this.add(pnlAvialables, c);
		c.gridx = 1;
		c.weightx= 0;
		c.anchor = GridBagConstraints.PAGE_START;
		this.add(pnlSelectionButtons, c);
		c.weightx= 1;
		c.gridx = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_END;
		this.add(pnlSelected, c);
		
		initPanels();
	}

	private DataLanguageViewListener getListener() {
		return this.listener;
	}
	
	public void refreshView() {
		((DataLanguageAvailableTableModel) tblAvailables.getModel()).refreshList();
		((DataLanguageSelectionTableModel) tblSelecteds.getModel()).refreshList();
	}
	
	private void initPanels() {
		
		DataLanguageAvailableTableModel tblAvailablesModel = 
				new DataLanguageAvailableTableModel(this.listener);
		DataLanguageSelectionTableModel tblSelectedModel = 
				new DataLanguageSelectionTableModel(this.listener);
		
		tblAvailables = new JTable(tblAvailablesModel);		
		tblSelecteds = new JTable(tblSelectedModel);		
		
		transferHandlerSource = new DataLanguageTransferHandler(tblAvailables);
		transferHandlerTarget = new DataLanguageTransferHandler(tblSelecteds);		
		
	//	tblAvailables.setDragEnabled(true);
	//	tblAvailables.setTransferHandler(this.transferHandlerSource);		
		tblAvailables.setRowSelectionAllowed(true);
		tblAvailables.setDropMode(DropMode.USE_SELECTION);
		tblAvailables.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	
		JScrollPane scrPaneSource = new JScrollPane(tblAvailables);
		pnlAvialables.add(scrPaneSource, BorderLayout.CENTER);		
		
		JPanel pnlButtons = new JPanel(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0;
		c.weighty = 0;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(0,10,0,10);
		btnAdd = new JButton(new AbstractAction() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (getTblAvailables().getSelectedRowCount() > 0) {
					DataLanguageAvailableTableModel model = (DataLanguageAvailableTableModel) getTblAvailables().getModel();
					List<DataLanguageVO> selectedLangs = new ArrayList<DataLanguageVO>();
					for (int idx : getTblAvailables().getSelectedRows()) {
						selectedLangs.add(model.getLanguage(idx));
					}
					getListener().addDataLanguages(selectedLangs);
				}
			}
		});
		
		btnRemove = new JButton(new AbstractAction() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getTblSelecteds().getSelectedRowCount() > 0) {
					DataLanguageSelectionTableModel model = (DataLanguageSelectionTableModel) getTblSelecteds().getModel();
					List<DataLanguageVO> selectedLangs = new ArrayList<DataLanguageVO>();
					for (int idx : getTblSelecteds().getSelectedRows()) {
						selectedLangs.add(model.getLanguage(idx));
					}
					getListener().removeDataLanguages(selectedLangs);
				}			
			}
		});
		
		this.btnAdd.setIcon(Icons.getInstance().getIconRight16());
		this.btnRemove.setIcon(Icons.getInstance().getIconLeft16());
		
		pnlButtons.add(btnAdd, c);
		c.gridy = 1;
		pnlButtons.add(btnRemove, c);
		c.gridy = 0;
		pnlSelectionButtons.add(pnlButtons, c);
		
		tblSelecteds.setRowSelectionAllowed(true);
	//	tblSelecteds.setTransferHandler(this.transferHandlerTarget);
		tblSelecteds.setDropMode(DropMode.USE_SELECTION);
		//tblSelecteds.setDragEnabled(true);
		tblSelecteds.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblSelecteds.setFillsViewportHeight(true);
		
		JScrollPane scrPane = new JScrollPane(tblSelecteds);
		pnlSelected.add(scrPane, BorderLayout.CENTER);		
		
		JPanel pnlOrder = new JPanel(new GridBagLayout());
		
		c.weightx = 0;
		c.weighty = 0;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(0,10,0,10);
		
		btnUp = new JButton(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getTblSelecteds().getSelectedRowCount() == 1) {
					DataLanguageSelectionTableModel model = (DataLanguageSelectionTableModel) getTblSelecteds().getModel();
					int selectedRow = getTblSelecteds().getSelectedRow();
					if (selectedRow > 0) {
						getListener().moveUpDataLanguages(model.getLanguage(getTblSelecteds().getSelectedRow()));
						getTblSelecteds().setRowSelectionInterval(selectedRow - 1, selectedRow-1);						
					}
				}		
			}
		});
		this.btnUp.setIcon(Icons.getInstance().getIconUp16());
		
		btnDown = new JButton(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getTblSelecteds().getSelectedRowCount() == 1) {
					DataLanguageSelectionTableModel model = (DataLanguageSelectionTableModel) getTblSelecteds().getModel();
					int selectedRow = getTblSelecteds().getSelectedRow();
					if (selectedRow < getTblSelecteds().getRowCount() -1) {
						getListener().moveDownDataLanguages(model.getLanguage(getTblSelecteds().getSelectedRow()));
						getTblSelecteds().setRowSelectionInterval(selectedRow + 1, selectedRow + 1);						
					}
				}	
			}
		});
		this.btnDown.setIcon(Icons.getInstance().getIconDown16());
		
		pnlOrder.add(btnUp, c);
		c.gridy=1;
		pnlOrder.add(btnDown, c);
		
		pnlSelected.add(pnlOrder, BorderLayout.EAST);
		
	}
	
	public JTable getTblAvailables() {
		return tblAvailables;
	}

	public JTable getTblSelecteds() {
		return tblSelecteds;
	}
	
	public class DataLanguageAvailableTableModel extends DefaultTableModel {

		private DataLanguageViewListener listener;
		List<DataLanguageVO> items;
		
		public DataLanguageAvailableTableModel(DataLanguageViewListener listener) {
			this.listener = listener;
			this.items = listener.getAvailables();
		}
		
		public DataLanguageVO getLanguage(int row) {
			return items.get(row);
		}
		
		@Override
		public int getColumnCount() {
			return 1;
		}
		
		@Override
		public int getRowCount() {
			return items != null ? items.size() : 0;
		}
		
		@Override
		public String getColumnName(int column) {
			return SpringLocaleDelegate.getInstance().getMessage(
					"DefaultSelectObjectsPanel.6","Verfügbar");
		}
		
		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (rowIndex >= 0 && columnIndex >= 0) {	
				DataLanguageVO dataLanguageVO = this.items.get(rowIndex);
				return dataLanguageVO.toString();
			} else {
				return null;
			}
		}
		
		@Override
		public boolean isCellEditable(int row, int column){  
	          return false;  
		}
		
		public void refreshList() {
			this.items = listener.getAvailables();
			fireTableDataChanged();
		}
	}
	
	public class DataLanguageSelectionTableModel extends AbstractTableModel {

		private DataLanguageViewListener listener;
		List<DataLanguageVO> items;
		
		public DataLanguageSelectionTableModel(DataLanguageViewListener listener) {
			this.listener = listener;
			this.items = listener.getSelected();
		}
		
		public DataLanguageVO getLanguage(int row) {
			return items.get(row);
		}
		
		@Override
		public int getColumnCount() {
			return 2;
		}
		
		@Override
		public int getRowCount() {
			return items != null ? items.size() : 0;
		}
		
		@Override
		public String getColumnName(int column) {
			if (column == 0) {
				return SpringLocaleDelegate.getInstance().getMessage(
						"DefaultSelectObjectsPanel.1","Ausgewählt");
			} else {
				return SpringLocaleDelegate.getInstance().getMessage(
						"PrimaryLanguage","Primärsprache");
			}		
		}
		
		@Override
		public void setValueAt(Object value, int row, int column) {
			if (value == Boolean.TRUE) {
				for (int idx=0; idx < items.size(); idx++) {
					DataLanguageVO dl = items.get(idx);
					if (row == idx) {
						dl.setIsPrimary(true);
					} else {
						dl.setIsPrimary(false);
					}
				}
				fireTableRowsUpdated(0, items.size()-1);
				this.listener.switchSave();
			}
		}
		
		
		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if (rowIndex >= 0 && columnIndex >= 0) {				
				DataLanguageVO dataLanguageVO = this.items.get(rowIndex);
				
				if (columnIndex == 0) {
					return dataLanguageVO.toString();
							
				} else {
					return dataLanguageVO.isPrimary();
				}
			} else {
				return null;
			}
		}
		
		@Override
		public boolean isCellEditable(int row, int column) {			
			if (column == 1) {
				return true;
			}
			return false;  		
		}
		
		public void refreshList() {
			this.items = listener.getSelected();
			fireTableDataChanged();
		}
	
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
	    }
	}
}
