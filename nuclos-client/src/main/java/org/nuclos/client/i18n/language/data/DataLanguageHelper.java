package org.nuclos.client.i18n.language.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;


public class DataLanguageHelper {

	public static List<String> getDataLanguageReferencePath(List<Attribute> entityAttributes) {
		Map<String, List<String[]>> pathes = new HashMap<String, List<String[]>>();
		
		for (Attribute a : entityAttributes) {
			if (a.getDatatyp().isRefenceTyp()) {
				List<UID> path = new ArrayList<UID>();
				path.add(a.getMetaVO().getUID());
				
				if (a.getMetaVO() != null) {
					ArrayList<UID> arrayList = new ArrayList<UID>(path);
					arrayList.add(a.getMetaVO().getUID());
					
					if (E.DATA_LANGUAGE.getUID().equals(a.getMetaVO().getUID())) {
						if (pathes.get(a.getUID().toString()) == null) {
							pathes.put(a.getUID().toString(), new ArrayList<String[]>());
						} 
						List<String> foundPath = new ArrayList<String>();
						pathes.get(a.getUID().toString()).add(foundPath.toArray(new String[0]));
					} else {
						List<List<String>> foundPathes = getDataLanguageReferencePath(arrayList, a.getUID(), a.getMetaVO().getUID());
						if (!foundPathes.isEmpty()) {
							if (pathes.get(a.getUID().toString()) == null) {
								pathes.put(a.getUID().toString(), new ArrayList<String[]>());
							} 
							for(List<String> p : foundPathes) {
								pathes.get(a.getUID().toString()).add(p.toArray(new String[p.size()]));
							}							
						}						
					}
				}				
			}
		}
		
		List<String> retVal = new ArrayList<String>();
		
		for (String s : pathes.keySet()) {
			List<String[]> list = pathes.get(s);
			for (String[] parts : list) {
				StringBuilder sb = new StringBuilder();
				sb.append(s + ";");
				for (int idx=0; idx < parts.length; idx++) {
					String part = parts[idx];
					sb.append(part);
					if (idx < parts.length - 1)
						sb.append(";");
				}
				if (!retVal.contains(sb.toString()))
					retVal.add(sb.toString());
			}
		}
		return retVal;
	}
	
	private static List<List<String>> getDataLanguageReferencePath(List<UID> currentPath , UID refField, UID foreignEntity) {
		
		List<List<String>> retVal = new ArrayList<List<String>>();
		
		Map<UID, FieldMeta<?>>  valueMP = 
				MetaProvider.getInstance().getAllEntityFieldsByEntity(foreignEntity);
				
		for (FieldMeta fm : valueMP.values()) {
			if (fm.getForeignEntity() != null) {
				if (E.DATA_LANGUAGE.getUID().equals(fm.getForeignEntity())) {
					List<String> foundPath = new ArrayList<String>();
					foundPath.add(fm.getUID().toString());
					retVal.add(foundPath);
				} else {
					if (!MetaProvider.getInstance().getEntity(fm.getForeignEntity()).isUidEntity()) {
						if (!currentPath.contains(fm.getForeignEntity())) {
							ArrayList<UID> arrayList = new ArrayList<UID>(currentPath);
							arrayList.add(fm.getForeignEntity());
							List<List<String>> dataLanguageReferencePath = 
									getDataLanguageReferencePath(arrayList, fm.getUID(), fm.getForeignEntity());
							
							if (!dataLanguageReferencePath.isEmpty()) {
								for (List<String> path : dataLanguageReferencePath) {
									path.add(0, fm.getUID().toString());
								}
								retVal.addAll(dataLanguageReferencePath);								
							}
							dataLanguageReferencePath = null;
							arrayList = null;
						}
					}
				}
			}
		}
		valueMP = null;
		return retVal;
	}
}
