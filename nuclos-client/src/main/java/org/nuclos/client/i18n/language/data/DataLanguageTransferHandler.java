package org.nuclos.client.i18n.language.data;

import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.TransferHandler;

import org.nuclos.client.i18n.language.data.DataLanguageSelectionPanel.DataLanguageAvailableTableModel;
import org.nuclos.client.i18n.language.data.DataLanguageSelectionPanel.DataLanguageSelectionTableModel;
import org.nuclos.server.i18n.language.data.DataLanguageVO;

public class DataLanguageTransferHandler extends TransferHandler{

	private DataLanguageViewListener listener;
	private JTable source;
	
	public DataLanguageTransferHandler(JTable source) {
		this.source = source;
	}
	
	public int getSourceActions(JComponent c) {
		 return DnDConstants.ACTION_COPY_OR_MOVE;
	}
	 
	public DataLanguageViewListener getListener() {
		return listener;
	}

	public void setListener(DataLanguageViewListener listener) {
		this.listener = listener;
	}

	@Override
	 protected Transferable createTransferable(JComponent c) {		 
		 
		 DataLanguageTransferable retVal = null;
		 
		 if (c != null && c instanceof JTable) {
			
			retVal = new DataLanguageTransferable();
			
			// add or remove languages
			JTable avails = (JTable) c;
			
			if (avails.getModel() instanceof DataLanguageAvailableTableModel) {
				DataLanguageAvailableTableModel model = (DataLanguageAvailableTableModel) avails.getModel();
				for (int row : avails.getSelectedRows()) {
					retVal.addDataLanguage(model.getLanguage(row));
				}				
			} else {
				DataLanguageSelectionTableModel model = (DataLanguageSelectionTableModel) avails.getModel();
				for (int row : avails.getSelectedRows()) {
					retVal.addDataLanguage(model.getLanguage(row));
				}
			}
		 }
		 
		 return retVal;
	 }
	 
	 @Override
	 public boolean canImport(final TransferSupport support) {
		 boolean retVal = false;
		 
		 if (support.isDataFlavorSupported(DataLanguageTransferable.FLAVOR) && 
				 support.getTransferable() != null) {
				if (!support.getComponent().equals(this.source))
					retVal = true;
		 }
		 
		 return retVal;
	 }
	 
	 @Override
	 public boolean importData(TransferSupport support) {
		 boolean retVal = false;
		 
		 try {
			Object o = support.getTransferable().getTransferData(DataLanguageTransferable.FLAVOR);
			
			if (o != null && this.listener != null) {
				List<DataLanguageVO> lstDataLanguageVOTranfer = (List<DataLanguageVO>) o;
				
				if (support.getComponent() != null && support.getComponent() instanceof JTable) {					
					JTable tbl = (JTable) support.getComponent();
					if (tbl.getModel() instanceof DataLanguageAvailableTableModel) {
						this.listener.removeDataLanguages(lstDataLanguageVOTranfer);						
					} else {
						this.listener.addDataLanguages(lstDataLanguageVOTranfer);						
					}
					retVal = true;
				}
			}
		} catch (Exception e) {
			// ignore
		}
		 
		return retVal;
	 }
}
