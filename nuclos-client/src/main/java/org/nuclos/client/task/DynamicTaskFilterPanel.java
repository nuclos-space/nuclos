//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.task;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.io.Closeable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.HorizontalLayout;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;

public class DynamicTaskFilterPanel extends JPanel implements Closeable {
	
	private static final Logger LOG = Logger.getLogger(DynamicTaskFilterPanel.class);

	//
	
	private JPanel filterPanel = new JPanel(new HorizontalLayout());
	private JPanel resetFilterButtonPanel = new JPanel(new BorderLayout());
	private final JButton resetFilterButton = new JButton();
	
	private TableColumnModel columnModel;
	private TableColumnModelListener columnModelListener;
	private Map<String, FilterComponent> column2component = new HashMap<String, FilterComponent>();
	
	private boolean closed = false;
	
	public DynamicTaskFilterPanel(TableColumnModel columnModel, Map<String, FilterComponent> column2component) {
		setLayout(new BorderLayout());
		this.columnModel = columnModel;
		this.column2component = column2component;

		add(filterPanel, BorderLayout.CENTER);
		init();
	}
	
	private void init() {
		resetFilterButton.setIcon(Icons.getInstance().getIconClearSearch16());
		resetFilterButton.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("subform.filter.tooltip", "Filterkriterien l\u00f6schen"));
		resetFilterButtonPanel.add(resetFilterButton, BorderLayout.CENTER);
		arrangeFilterComponents();
        setVisible(false);
        addListener();
	}

	@Override
	public void close() {
		// Close is needed for avoiding memory leaks
		// If you want to change something here, please consult me (tp).  
		if (!closed) {
			LOG.debug("close(): " + this);
			filterPanel = null;
			if(columnModelListener!=null){
				if(columnModel!=null) columnModel.removeColumnModelListener(columnModelListener);
				columnModelListener=null;
			}
			columnModel = null;
			column2component.clear();
			column2component = null;
			
			// JXCollapsiblePane
			removeAll();
			// setContentPane(null);
			
			closed = true;
		}
	}
	
	/**
	 * adds a listener on the columnmodel to get informed, when e.g. a column margin was changed
	 * and to arrange the filter components again
	 */
	public void addListener() {
		columnModelListener = new TableColumnModelListener() {

			@Override
			public void columnAdded(TableColumnModelEvent e) {
				// do nothing
			}

			@Override
			public void columnMarginChanged(ChangeEvent e) {
				arrangeFilterComponents();
			}

			@Override
			public void columnMoved(TableColumnModelEvent e) {
				if (e.getFromIndex() != e.getToIndex()) {
					arrangeFilterComponents();
				}
			}

			@Override
			public void columnRemoved(TableColumnModelEvent e) {
				// do nothing
			}

			@Override
			public void columnSelectionChanged(ListSelectionEvent e) {
				// do nothing
			}
		};
		
		columnModel.addColumnModelListener(columnModelListener);
	}
	
	boolean flip=false;
	/**
	 * arranges all filter components, because e.g a column was moved,
	 * so that the filter components stay directly above the corresponding columns
	 */
	final void arrangeFilterComponents() {
		int index = 0;
		filterPanel.removeAll();
		
		index++;
	
		for(TableColumn tc : CollectionUtils.iterableEnum(columnModel.getColumns())) {
			if(tc.getIdentifier() == null)
				continue;
			FilterComponent clctcomp = (tc.getIdentifier() instanceof String) ? getCollectableComponentByName((String)tc.getIdentifier()) : null;
			JComponent comp = (clctcomp == null) ? null : clctcomp.getControlComponent();
			if(comp != null) {
				// place checkboxes in the center
				if (comp instanceof JCheckBox) {
					JPanel p = new JPanel(new GridBagLayout());
					p.add(comp);
					comp = p;
				}

				comp.setPreferredSize(new Dimension(tc.getWidth(), 20));
				filterPanel.add(comp);
				index++;
			}
		}
		filterPanel.add(this.resetFilterButtonPanel);
		filterPanel.revalidate();
		filterPanel.repaint();
		repaint();
	}
	
	FilterComponent getCollectableComponentByName(String name) {
		if (name == null) {
			return null;
		}
		
		for (FilterComponent comp : column2component.values()) {
			if (comp.getId().equals(name)) {
				return comp;
			}
		}
		
		return null;
	}
	
	public JButton getResetFilterButton() {
		return this.resetFilterButton;
	}
	
	/**
	 * @return the active filter components
	 */
	public Map<UID, FilterComponent> getActiveFilterComponents() {
		Map<UID, FilterComponent> activeActiveFilterComponents = new HashMap<UID, FilterComponent>();
		
		for (int index=0 ; index<columnModel.getColumnCount() ; index++) {
			TableColumn column = columnModel.getColumn(index);
			if(column.getIdentifier() == null)
				continue;
			Object ident = column.getIdentifier();
			if (ident instanceof UID) {
				UID header = (UID)ident;
				FilterComponent comp = column2component.get(header);
				
				if (!"".equals(header)) {
					activeActiveFilterComponents.put(header, comp);
				}
			}
		}
		
		return activeActiveFilterComponents;
	}

	public Map<String, String> getFilterValues() {
		HashMap<String, String> result = new HashMap<String, String>();
		Collection<FilterComponent> c = column2component.values();
		for (FilterComponent filterComponent : c) {
			result.put(filterComponent.getId(), filterComponent.getValue());
		}
		return result;
	}

	public void resetFilter() {
		Collection<FilterComponent> c = column2component.values();
		for (FilterComponent filterComponent : c) {
			filterComponent.resetFilter();
		}
	}
	
}
