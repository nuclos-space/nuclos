package org.nuclos.client.genericobject.controller;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Locale;

import javax.swing.AbstractAction;

import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.genericobject.IStatusChanger;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.statemodel.StateWrapper;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class StateChangeAction extends AbstractAction {

	private MainFrameTab tab;
	private final IStatusChanger statusChanger;

	private final StateVO statevoSource;
	private final StateVO statevoTarget;

	public StateChangeAction(IStatusChanger statusChanger,
							 StateVO statevoSource, StateVO statevoTarget) {
		super(StringUtils.looksEmpty(statevoTarget.getButtonLabel(LocaleDelegate.getInstance().getLocale())) ?
						statevoTarget.getStatename(LocaleDelegate.getInstance().getLocale()) :
						statevoTarget.getButtonLabel(LocaleDelegate.getInstance().getLocale()),
				statevoTarget.getButtonIcon() == null ? null
						: ResourceCache.getInstance().getIconResource(
						statevoTarget.getButtonIcon().getId()));
		this.statusChanger = statusChanger;
		this.tab = statusChanger.getTab();

		this.statevoSource = statevoSource;
		this.statevoTarget = statevoTarget;

		putValue("Color", statevoTarget.getColor());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		StateWrapper swSource = createStateWrapper(statevoSource);
		StateWrapper swTarget = createStateWrapper(statevoTarget);
		statusChanger.cmdChangeStates(tab, swSource, swTarget, swTarget.getStatesBefore());
	}

	private static StateWrapper createStateWrapper(StateVO stateVO) {
		if (stateVO == null) {
			return null;
		}

		final Locale locale = LocaleDelegate.getInstance().getLocale();
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();

		String sLabel = localeDelegate.getResource(stateVO.getResourceIdForLabel(), stateVO.getStatename(locale));
		String sDescription = localeDelegate.getResource(stateVO.getResourceIdForDescription(), stateVO.getDescription(locale));

		return new StateWrapper(stateVO.getId(), stateVO.getNumeral(), sLabel, stateVO.getIcon(),
				sDescription, stateVO.getColor(), stateVO.getButtonIcon(), stateVO.isFromAutomatic(),
				stateVO.getNonStopSourceStates(), Collections.singletonList(stateVO.getId()));
	}

	public void setMainFrameTab(MainFrameTab tab) {
		this.tab = tab;
	}

	public StateVO getStateSource() {
		return statevoSource;
	}

	public StateVO getStateTarget() {
		return statevoTarget;
	}

}
