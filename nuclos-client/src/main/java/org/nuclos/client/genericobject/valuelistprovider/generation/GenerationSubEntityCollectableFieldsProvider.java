//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider.generation;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.GenericObjectLayoutCache;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonBusinessException;
/**
 * Value list provider to get subentities of a module.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:uwe.allner@novabit.de">uwe.allner</a>
 * @version 00.01.000
 */
public class GenerationSubEntityCollectableFieldsProvider implements CollectableFieldsProvider {
	
	private static final Logger LOG = Logger.getLogger(GenerationSubEntityCollectableFieldsProvider.class);
	
	public static final String ENTITY_UID = "module";
	
	public static final String PARAMETER_ENTITY_UID = "parameterEntity";
	
	public static final String SOURCE_TYPE = "sourceType";
	
	public static final String VALUE_SOURCE_TYPE_PARAMETER = "parameter";
	
	//

	private UID moduleId;
	private UID parameterEntityId;
	private String sourceType;
	
	/**
	 * @deprecated
	 */
	public GenerationSubEntityCollectableFieldsProvider() {
	}
	
	public GenerationSubEntityCollectableFieldsProvider(UID entityUid, UID parameterEntityUid, String valueSourceType) {
		setParameter(ENTITY_UID, entityUid);
		setParameter(PARAMETER_ENTITY_UID, parameterEntityUid);
		setParameter(SOURCE_TYPE, valueSourceType);
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		// LOG.debug("setParameter - sName = " + sName + " - oValue = " + oValue);
		if (sName.equals(ENTITY_UID)) {
			this.moduleId = (UID) oValue;
		} else if (sName.equals(PARAMETER_ENTITY_UID)) {
			this.parameterEntityId = (UID) oValue;
		} else if (sName.equals(SOURCE_TYPE)) {
			this.sourceType = (String) oValue;
		} else {
			// ignore
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	protected UID getEntityId() {
		return VALUE_SOURCE_TYPE_PARAMETER.equals(sourceType) ? parameterEntityId : moduleId;
	}
	
	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		final UID entityId = getEntityId();
		if (entityId == null) {
			return Collections.<CollectableField>emptyList();
		} else {
			List<CollectableField> result = MasterDataDelegate.getInstance().getSubEntities(entityId);
			if(Modules.getInstance().isModule(entityId)) {
				for (EntityAndField entityAndField : GenericObjectLayoutCache.getInstance().getSubFormEntities(entityId)) {
					EntityMeta<?> metaDataVO = MetaProvider.getInstance().getEntity(entityAndField.getEntity());
					CollectableValueIdField cIdField = new CollectableValueIdField(metaDataVO.getUID(), metaDataVO.getEntityName());		
					if (!result.contains(cIdField)) {
						result.add(cIdField);
					}
				}
			} else {
				for (UID entityField : MasterDataLayoutHelper.newLayoutRoot(entityId, false, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY)).getMapOfSubForms().keySet()) {
					EntityMeta<?> metaDataVO = MetaProvider.getInstance().getEntity(entityField);
					CollectableValueIdField cIdField = new CollectableValueIdField(metaDataVO.getUID(), metaDataVO.getEntityName());		
					if (!result.contains(cIdField)) {
						result.add(cIdField);
					}
				}				
			}
			Collections.sort(result);
			return result;
		}
	}

}	// class ModuleSubEntityCollectableFieldsProvider
