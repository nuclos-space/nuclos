//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.startup.AbstractLocalUserCache;
import org.nuclos.common.AttributeProvider;
import org.nuclos.common.CacheableListener;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.GenericObjectMetaDataProvider;
import org.nuclos.common.GenericObjectMetaDataVO;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosAttributeNotFoundException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

/**
 * Client side leased object meta data cache (singleton).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class GenericObjectMetaDataCache extends AbstractLocalUserCache implements GenericObjectMetaDataProvider {

	private static final Logger LOG = Logger.getLogger(GenericObjectMetaDataCache.class);
	
	private static GenericObjectMetaDataCache INSTANCE;

	//
	
	private transient GenericObjectDelegate genericObjectDelegate;

	private GenericObjectMetaDataVO lometacvo;

	private transient List<CacheableListener> lstCacheableListeners;
	
	private transient TopicNotificationReceiver tnr;
	private transient MessageListener messageListener; 

	private GenericObjectMetaDataCache() {
		INSTANCE = this;
	}

	public void afterPropertiesSet() throws Exception {
		// Constructor might not be called - as this instance might be deserialized (tp)
		if (INSTANCE == null) {
			INSTANCE = this;
		}
		if (!wasDeserialized() || !isValid())
			setup();
	}

	public final void initMessageListener() {
		if (messageListener != null)
			return;
		
		messageListener = new MessageListener() {
			@Override
			public void onMessage(Message msg) {
				LOG.info("onMessage: Received notification from server: meta data changed, revalidate...");
				GenericObjectMetaDataCache.this.revalidate();

				GenericObjectLayoutCache.getInstance().invalidate();
				MasterDataDelegate.getInstance().invalidateLayoutCache();
			}
		};
		tnr.subscribe(getCachingTopic(), messageListener);
	}

	public final void setTopicNotificationReceiver(TopicNotificationReceiver tnr) {
		this.tnr = tnr;
	}
	
	public final void setGenericObjectDelegate(GenericObjectDelegate genericObjectDelegate) {
		this.genericObjectDelegate = genericObjectDelegate;
	}

	public static GenericObjectMetaDataCache getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	@Override
	public String getCachingTopic() {
		return JMSConstants.TOPICNAME_METADATACACHE;
	}

	private GenericObjectMetaDataVO getMetaDataCVO() {
		return lometacvo;
	}

	public Set<UID> getAttributeUids(UID uid, Boolean searchField) {
		return this.getMetaDataCVO().getAttributesByModule(uid, searchField);
	}
	
	public AttributeProvider getAttributeProvider() {
		return AttributeCache.getInstance();
	}

	@Override
	public AttributeCVO getAttribute(UID attributeUid) {
		return this.getAttributeProvider().getAttribute(attributeUid);
	}
	
	@Override
	public Collection<AttributeCVO> getAttributes() {
		return this.getAttributeProvider().getAttributes();
	}

	@Override
	public Collection<AttributeCVO> getAttributeCVOsByModule(UID moduleUid, Boolean bSearchable) {
		return this.getMetaDataCVO().getAttributeCVOsByModule(this.getAttributeProvider(), moduleUid, bSearchable);
	}

	@Override
	public Set<UID> getSubFormEntityByLayout(UID layoutUid) {
		return this.getMetaDataCVO().getSubFormEntityByLayout(layoutUid, MetaProvider.getInstance());
	}

	@Override
	public Set<UID> getAllSubFormEntitiesByEntity(UID entityUid) {
		return this.getMetaDataCVO().getSubFormEntityNamesByModuleId(entityUid, MetaProvider.getInstance());
	}

	@Override
	public Collection<EntityAndField> getSubFormEntityAndForeignKeyFieldsByLayout(UID layoutUid) {
		return this.getMetaDataCVO().getSubFormEntityAndForeignKeyFieldsByLayout(layoutUid, MetaProvider.getInstance());
	}

	/**
	 * @param moduleUid may be <code>null</code>.
	 * @return the names of subform entities used in the module (details only) with the given id. If module id is <code>null</code>,
	 * the names of all subform entities used in any module (details only).
	 */
	public Set<UID> getSubFormEntityNamesByModuleId(UID moduleUid) {
		return this.getMetaDataCVO().getSubFormEntityNamesByModuleId(moduleUid, MetaProvider.getInstance());
	}

	@Override
	public Set<UID> getBestMatchingLayoutAttributes(UsageCriteria usagecriteria) throws CommonFinderException {
		return this.getMetaDataCVO().getBestMatchingLayoutAttributes(usagecriteria, MetaProvider.getInstance());
	}

	@Override
	public UID getBestMatchingLayout(UsageCriteria usagecriteria, boolean bSearchScreen) throws CommonFinderException {
		return this.getMetaDataCVO().getBestMatchingLayout(usagecriteria, bSearchScreen);
	}

	@Override
	public Set<UID> getLayoutsByModule(UID moduleUid, boolean bSearchScreen) {
		return this.getMetaDataCVO().getLyoutsByModule(moduleUid, bSearchScreen);
	}
	
	@Override
	public String getLayoutML(UID moduleUid) {
		return this.getMetaDataCVO().getLayoutML(moduleUid);
	}

	public void revalidate() {
		AttributeCache.getInstance().revalidate();
		Modules.getInstance().invalidate();
		this.setup();
	}

	private void setup() {
		this.lometacvo = genericObjectDelegate.getMetaDataCVO();
		this.fireCacheableChanged();
	}

	private synchronized List<CacheableListener> getCacheableListeners() {
		if (this.lstCacheableListeners == null) {
			this.lstCacheableListeners = new LinkedList<CacheableListener>();
		}
		return this.lstCacheableListeners;
	}

	public synchronized void addCacheableListener(CacheableListener cacheablelistener) {
		this.getCacheableListeners().add(cacheablelistener);
	}

	public synchronized void removeCacheableListener(CacheableListener cacheablelistener) {
		this.getCacheableListeners().remove(cacheablelistener);
	}

	private synchronized void fireCacheableChanged() {
		for (CacheableListener listener : this.getCacheableListeners()) {
			listener.cacheableChanged();
		}
	}

	@Override
	public FieldMeta getEntityField(UID field)
			throws NuclosAttributeNotFoundException {
		return MetaProvider.getInstance().getEntityField(field);
	}

}	// class GenericObjectMetaDataCache
