package org.nuclos.client.genericobject.controller;

import javax.swing.JComponent;

import org.nuclos.client.ui.LayoutComponentUtils;
import org.nuclos.client.ui.collect.detail.DetailsPanel;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.DetailsPresentation;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class GenericObjectDetailsPanel extends DetailsPanel {

	private final WorkspaceDescription2.EntityPreferences entityPreferences;

	public GenericObjectDetailsPanel(UID entityId,
									 WorkspaceDescription2.EntityPreferences entityPreferences, DetailsPresentation detailPresentation, ControllerPresentation controllerPresentation) {
		super(entityId, detailPresentation, controllerPresentation);
		this.entityPreferences = entityPreferences;
	}

	/**
	 * §todo pull down to SearchOrDetailsPanel and/or change signature into
	 * EditView newEditView()
	 *
	 * @param compRoot the edit component according to the LayoutML
	 * @return the edit component to be used in the Details panel. Default
	 * is <code>compRoot</code> itself. Successors may build their
	 * own component/panel out of compRoot.
	 */
	public JComponent newEditComponent(JComponent compRoot) {
		return LayoutComponentUtils.setPreferences(entityPreferences,
				compRoot);
	}

} // inner class GenericObjectDetailsPanel
