//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.entityobject.EntityFacadeDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.masterdata.valuelistprovider.CacheableCollectableFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProvider;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.valuelistprovider.cache.ManagedCollectableFieldsProvider;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.ejb3.CollectableFieldsByNameParams;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Provides <code>CollectableField</code>s from the attribute values of a <code>CollectableGenericObjectEntityField</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 * @deprecated Does not use EO mechanism
 */
@Deprecated
public class GenericObjectCollectableFieldsProvider extends ManagedCollectableFieldsProvider implements CacheableCollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(GenericObjectCollectableFieldsProvider.class);
	
	//
	
	private final CollectableEntityField clctef;
	private final Map<String, Object> parameters = new HashMap<String, Object>();

	public GenericObjectCollectableFieldsProvider(CollectableEntityField clctef) {
		this.clctef = clctef;
	}

	@Override
	public void setParameter(String sName, Object oValue) {
		if (NuclosConstants.VLP_MANDATOR_PARAMETER.equals(sName)) {
			setMandator((UID) oValue);
		} else if (sName.equals(NuclosConstants.VLP_ADDITIONAL_CONDITION_FOR_RESULTFILTER)) {
			additionalCondition = RigidUtils.uncheckedCast(oValue);
		} else {
			parameters.put(sName, oValue);
		}
	}

	private Pair<UID, CollectableSearchCondition> additionalCondition;

	@Override
	public Object getCacheKey() {
		// the cache key is field name + name of referenced entity/field (if any)
		return Arrays.<Object>asList(
			clctef.getUID(),
			clctef.isReferencing() ? clctef.getReferencedEntityUID() : null,
			clctef.isReferencing() ? clctef.getReferencedEntityFieldName() : null,
			this.getIgnoreValidity(),
			getMandator());
	}
	
	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result;

		if (this.clctef.isReferencing()) {
			if (Modules.getInstance().isModule(clctef.getReferencedEntityUID())) {
				result = getCollectableFieldsByName(clctef.getReferencedEntityFieldName(), true);
			}
			else {
				final String sFieldName = this.clctef.getReferencedEntityFieldName();	
				// NUCLOSINT-4
				if (SF.STATE.getMetaData(this.clctef.getEntityUID()).getUID().equals(this.clctef.getUID()) || 
					SF.STATENUMBER.getMetaData(this.clctef.getEntityUID()).getUID().equals(this.clctef.getUID()) || 
					SF.STATEICON.getMetaData(this.clctef.getEntityUID()).getUID().equals(this.clctef.getUID())) {

					boolean bShowName = SF.STATE.getMetaData(this.clctef.getEntityUID()).getUID().equals(this.clctef.getUID());
					Collection<StateVO> states = 
							StateDelegate.getInstance().getStatesByModule(this.clctef.getCollectableEntity().getUID());

					result = new ArrayList<CollectableField>();
					for (StateVO state : states) {
						CollectableField clctf = new CollectableValueField(bShowName?state.getStatename(
								LocaleDelegate.getInstance().getLocale()):state.getNumeral());
						if (!result.contains(clctf)) {
							result.add(clctf);
						}
					}
				} else {
					final MasterDataCollectableFieldsProvider clctfprovider = new MasterDataCollectableFieldsProvider(clctef.getReferencedEntityUID());
					clctfprovider.setParameter(NuclosConstants.VLP_STRINGIFIED_FIELD_DEFINITION_PARAMETER, clctef.getReferencedEntityFieldName());
					clctfprovider.setIgnoreValidity(this.getIgnoreValidity());
					for (Entry<String, Object> param : parameters.entrySet()) {
						clctfprovider.setParameter(param.getKey(), param.getValue());
					}
					clctfprovider.setMandator(getMandator());
					result = StringUtils.isNullOrEmpty(sFieldName) ? clctfprovider.getCollectableFields() : clctfprovider.getCollectableFieldsByName(sFieldName, true && !this.getIgnoreValidity());
				}
			}
		}
		else if (clctef instanceof CollectableGenericObjectEntityField){
			final CollectableGenericObjectEntityField goEntityField = (CollectableGenericObjectEntityField) clctef;
			result = new ArrayList<CollectableField>();
		} else {
			throw new NuclosFatalException("Field " + clctef.getUID() + " of entity " + clctef.getCollectableEntity().getUID() + " is neither a generic object field nor a reference field");
		}

		Collections.sort(result);
		return result;
	}

	/**
	 * returns a list of collectable fields for an attribute name.
	 * 
	 * @param attribute
	 * @param bValid
	 * @return List&lt;CollectableField&gt;
	 */
	public List<CollectableField> getCollectableFieldsByName(String attribute, boolean bValid) throws CommonBusinessException {
		return getCollectableFieldsByName(clctef.getReferencedEntityUID(), attribute, bValid);
	}

	private synchronized List<CollectableField> getCollectableFieldsByName(UID entity, String attribute, boolean bValid) throws CommonBusinessException {
		
		UID datalanguage = DataLanguageContext.getDataUserLanguage() != null ?
				DataLanguageContext.getDataUserLanguage() : DataLanguageContext.getDataSystemLanguage();

		return EntityFacadeDelegate.getInstance().getCollectableFieldsByName(
				CollectableFieldsByNameParams.builder()
						.entityUid(entity)
						.stringifiedFieldDefinition(attribute)
						.checkValidity((!getIgnoreValidity() && bValid))
						.mandator(getMandator())
						.dataLanguage(datalanguage)
						.additionalCondition(additionalCondition)
						.build()
		);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GenericObjectCollectableFieldsProvider) {
			GenericObjectCollectableFieldsProvider that = (GenericObjectCollectableFieldsProvider)obj;
			return LangUtils.equal(getCacheKey(), that.getCacheKey());
			
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(getCacheKey());
	}
	
	@Override
	public String toString() {
		return "GOFieldsProvider[" + getCacheKey() + "] (" + hashCode() + ")";
	}
	
}	// class GenericObjectCollectableFieldsProvider
