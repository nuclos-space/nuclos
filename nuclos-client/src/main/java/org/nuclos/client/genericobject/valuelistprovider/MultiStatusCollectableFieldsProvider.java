//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.statemodel.StatemodelClosure;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Provides status-model related fields.
 * If the fields belong to different status models, the field values are prefixed with the corresponding status model name.
 */
public abstract class MultiStatusCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(MultiStatusCollectableFieldsProvider.class);

	static final String MODULE_UID = "module";
	public static final String PROCESS_UID = "process";

	protected UID moduleId;
	private UID processId;

	private StatemodelClosure statemodelClosure;	/**

	 * @deprecated
	 */
	public MultiStatusCollectableFieldsProvider() {
	}

	MultiStatusCollectableFieldsProvider(UID entityWithStatemodelUid, UID processUid) {
		setParameter(MODULE_UID, entityWithStatemodelUid);
		setParameter(PROCESS_UID, processUid);
	}

	/**
	 * Must return a List of CollectableFields where the ValueId is the UID of a status.
	 */
	abstract List<CollectableField> getCollectableStatusFields();

	/**
	 * valid parameters:
	 * <ul>
	 *   <li>"module" = name of module entity</li>
	 *   <li>"process" = process id</li>
	 * </ul>
	 * @param sName parameter name
	 * @param oValue parameter value
	 *
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		switch (sName) {
			case MODULE_UID:
				try {
					if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
						moduleId = UID.parseUID((String) oValue);
					} else if (oValue instanceof UID) {
						moduleId = (UID) oValue;
					}
					if (moduleId == null) {
						throw new IllegalArgumentException("oValue");
					}
				} catch (Exception ex) {
					throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
							"StatusCollectableFieldsProvider.1", "Der Parameter \"module\" muss den Namen einer Modul-Entit\u00e4t enthalten.\n\"{0}\" ist keine g\u00fcltige Modul-Entit\u00e4t.", oValue), ex);
				}
				break;
			case PROCESS_UID:
				if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
					processId = UID.parseUID((String) oValue);
				} else if (oValue instanceof UID) {
					processId = (UID) oValue;
				} else {
					processId = null;
				}
				break;
			default:
				// ignore
				LOG.info("Unknown parameter " + sName + " with value " + oValue);
				break;
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> fields = getCollectableStatusFields();

		if (hasMultipleStatusModels()) {
			return fields.stream()
					.map(this::prefixWithStatusModelName)
					.sorted()
					.collect(Collectors.toList());
		}

		return fields;
	}

	private boolean hasMultipleStatusModels() {
		return getStatemodelClosure().getStatusModelNameById().size() > 1;
	}

	private CollectableField prefixWithStatusModelName(CollectableField field) {
		StateVO state = getStatemodelClosure().getState((UID) field.getValueId());
		String statusModelName = getStatemodelClosure().getStatusModelNameById().get(state.getModelUID());

		if (statusModelName == null) {
			LOG.warn("Unknown status model: " + field.getValueId());
			statusModelName = "UNKNOWN";
		}

		return new CollectableValueIdField(
				field.getValueId(),
				"[" + statusModelName + "] " + field.getValue()
		);

	}

	private StatemodelClosure getStatemodelClosure() {
		if (statemodelClosure == null) {
			statemodelClosure = StateDelegate.getInstance().getStatemodelClosure(moduleId);
		}

		return statemodelClosure;
	}

	Map<UID, StateVO> getStatusById() {
		final Map<UID, StateVO> result = new HashMap<>();

		if (moduleId == null) {
			LOG.info("status numeral value list provider: module uid not set (parameter" + MODULE_UID + ")");
		}

		if (processId != null) {
			final UID iStateModelId = StateDelegate.getInstance().getStateModelId(new UsageCriteria(moduleId, processId, null, null));
			for (StateVO statevo : StateDelegate.getInstance().getStatesByModel(iStateModelId)) {
				result.put(statevo.getId(), statevo);
			}
		} else {
			for (StateVO statevo : StateDelegate.getInstance().getStatesByModule(moduleId)) {
				if (!result.containsKey(statevo.getId())) {
					result.put(statevo.getId(), statevo);
				}
			}
		}

		return result;
	}
}
