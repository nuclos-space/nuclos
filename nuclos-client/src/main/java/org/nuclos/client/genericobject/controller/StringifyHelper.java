package org.nuclos.client.genericobject.controller;

import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 *
 * Help stringify in a standardized f≈ormat
 *
 * @author Moritz Neuhäuser<moritz.neuhaeuser@nuclos.de>
 *
 */
public class StringifyHelper {
	/**
	 * Stringify {@link StateVO}
	 *
	 * @param stateVO	state vo
	 * @return
	 */
	public static String toString(final StateVO stateVO) {
		return stateVO.getStatename(LocaleDelegate.getInstance().getLocale()) + "(" + stateVO.getNumeral() + ")";
	}
}
