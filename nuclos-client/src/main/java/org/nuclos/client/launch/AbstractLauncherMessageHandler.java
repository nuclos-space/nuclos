package org.nuclos.client.launch;

import org.nuclos.schema.launcher.LauncherLaunchMessage;
import org.nuclos.schema.launcher.LauncherMessage;
import org.nuclos.schema.launcher.LauncherPidMessage;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
abstract class AbstractLauncherMessageHandler {
	protected abstract void handleLaunchMessage(LauncherLaunchMessage message);
	protected abstract void handlePidMessage(LauncherPidMessage message);

	void handleMessage(LauncherMessage message) {
		if (message instanceof LauncherLaunchMessage) {
			handleLaunchMessage((LauncherLaunchMessage) message);
		} else if (message instanceof LauncherPidMessage) {
			handlePidMessage((LauncherPidMessage) message);
		} else {
			throw new IllegalArgumentException("Coult not handle unknown launcher message: " + message);
		}
	}
}
