package org.nuclos.client.ui.collect.component;

import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;
import org.nuclos.common.UID;

public interface CollectableLocalizedComponent<PK> {

    CollectableLocalizedComponentModel<PK> getDetailsComponentModel();
	
    UID getFieldUID();
    
    void setModel(CollectableComponentModel clctcompmodel); 
 
    public boolean isButtonDisabled();
    
    public Integer getType();
    
}
