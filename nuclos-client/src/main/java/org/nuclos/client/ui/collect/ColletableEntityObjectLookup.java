package org.nuclos.client.ui.collect;

import org.nuclos.client.ui.collect.component.CollectableLocalizedComponent;

public interface ColletableEntityObjectLookup<PK> {

	void setCollectableLocalizedComponent(
			CollectableLocalizedComponent<PK> lclFieldComponent, int row);
	
}
