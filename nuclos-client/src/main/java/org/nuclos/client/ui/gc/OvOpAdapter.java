package org.nuclos.client.ui.gc;

import org.nuclos.client.ui.OvOpListener;

class OvOpAdapter extends EventAdapter implements OvOpListener {
	
	OvOpAdapter(OvOpListener wrapped) {
		super(wrapped);
	}

	@Override
	public void done(int result) {
		final OvOpListener l = (OvOpListener) wrapped.get();
		if (l != null) {
			l.done(result);
		}
	}

}
