//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.profile;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JCheckBoxMenuItem;

import org.apache.log4j.Logger;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.common.profile.ProfileItem;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

public class ProfileMenuItem extends JCheckBoxMenuItem {
	
	private static final Logger LOG	= Logger.getLogger(ProfileMenuItem.class);
	
	private final AbstractProfilesController pctl;
	private final NuclosCollectControllerCommonState state;
	private final ProfileItem profile;
	
	public ProfileMenuItem(final AbstractProfilesController pctl, final NuclosCollectControllerCommonState state, final ProfileItem profile) {
		super(profile.toString(), profile.isActive());
		this.pctl = pctl;
		this.state = state;
		this.profile = profile;
		
		this.setAction(new AbstractAction(toString()) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// try {
					if (state != null) {
						final EntitySearchFilter esf = state.getCurrentSearchFilter();
						if (esf != null) {
							final SearchFilterVO sf = esf.getSearchFilterVO();
							if (sf != null) {
								sf.setResultProfile(null);
							}
						}
					}
					pctl.switchToProfile(getProfile());
				/* } catch (CommonBusinessException e1) {
					LOG.warn("switchToProfile(" + getPreferences() + ") failed: " + e1, e1);
				} */
			}
		});
	}
	
	public ProfileItem getProfile() {
		return profile;
	}
	
	@Override
	public String toString() {
		return profile.toString();
	}
	
}
