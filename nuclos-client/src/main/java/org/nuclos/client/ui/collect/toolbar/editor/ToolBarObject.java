//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar.editor;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common2.SpringLocaleDelegate;

class ToolBarObject {
	
	private final Logger LOG = Logger.getLogger(ToolBarObject.class);
	
	private ToolBarItem item;
	private String label;
	private Icon icon;
	
	private IToolBarComponent tc;
	private IToolBarComponent mc;
	
	private boolean bToolbarOnly = false;
	private boolean bMenuOnly = false;
	
	private JLabel dummy = new JLabel();
	
	public ToolBarObject(ToolBarItem item, String label, String tooltip, Icon icon, 
			boolean toolLabel, boolean menuLabel, boolean toolIcon, boolean menuIcon, 
			ToolBarConfigurationEditor editor) {
		this.item = item;
		this.label = label;
		this.icon = icon;
		
		if (NuclosToolBarItems.COLLECT_INDICATOR.equals(item)) {
			bToolbarOnly = true;
			this.tc = new ToolBarCollectIndicator(this);
		} else if (NuclosToolBarItems.EXTRAS.equals(item)) {
			bToolbarOnly = true;
			this.tc = new ToolBarExtraButton(this);
		} else if (NuclosToolBarItems.LIST_PROFILE.equals(item)) {
			bToolbarOnly = true;
			this.tc = new ToolBarPopupButton(label, this);
		} else if (NuclosToolBarItems.MENU_SEPARATOR.equals(item)) {
			bMenuOnly = true;
			this.mc = new MenuSeparator(this);
		} else {
			switch (item.getType()) {
			case ACTION: {
				ToolBarButton button = new ToolBarButton(label, this);
				if (!toolLabel) button.putClientProperty("hideActionText", Boolean.TRUE);
				if (toolIcon) button.setIcon(icon);
				addContextMenu(this, button);
				this.tc = button;
				MenuItem menuitem = new MenuItem(label, this);
				if (!menuLabel) menuitem.putClientProperty("hideActionText", Boolean.TRUE);
				if (menuIcon) menuitem.setIcon(icon);
				menuitem.addMenuDragMouseListener(editor);
				this.mc = menuitem;
				break;
			}
			case TOGGLE_ACTION: {
				ToolBarToggleButton button = new ToolBarToggleButton(label, this);
				if (!toolLabel) button.putClientProperty("hideActionText", Boolean.TRUE);
				if (toolIcon) button.setIcon(icon);
				addContextMenu(this, button);
				this.tc = button;
				MenuItem menuitem = new MenuItem(label, this);
				if (!menuLabel) menuitem.putClientProperty("hideActionText", Boolean.TRUE);
				if (menuIcon) menuitem.setIcon(icon);
				menuitem.addMenuDragMouseListener(editor);
				this.mc = menuitem;
				break;
			}
			case PR_ACTION: {
				ToolBarComboBox combobox = new ToolBarComboBox(label, this);
				this.tc = combobox;
				MenuRadioButton radio = new MenuRadioButton(label, this);
				radio.addMenuDragMouseListener(editor);
				this.mc = radio;
				break;
			}
			case PSD_ACTION:
			case PSR_ACTION: {
				ToolBarComboBox combobox = new ToolBarComboBox(label, this);
				this.tc = combobox;
				MenuItem menuitem = new MenuItem(label, this);
				if (menuIcon) menuitem.setIcon(icon);
				menuitem.addMenuDragMouseListener(editor);
				this.mc = menuitem;
				break;
			}
			case DROPDOWN_ACTION: {
				bToolbarOnly = true;
				ToolBarComboBox combobox = new ToolBarComboBox(label, this);
				this.tc = combobox;
				break;
			}
			case LOV:
				bToolbarOnly = true;
				this.tc = new ToolBarComboBox(label, this);
				break;
//			case DATECHOOSER:
//				break;
			case CHECKBOX:
				this.tc = new ToolBarCheckBox(label, this);
				MenuCheckBox check = new MenuCheckBox(label, this);
				check.addMenuDragMouseListener(editor);
				this.mc = check;
				break;
			case RADIOBUTTON:
				this.tc = new ToolBarRadioButton(label, this);
				MenuRadioButton radio = new MenuRadioButton(label, this);
				radio.addMenuDragMouseListener(editor);
				this.mc = radio;
				break;
			case TEXTFIELD:
				bToolbarOnly = true;
				this.tc = new ToolBarTextField(label, this);
				break;
			default:
				LOG.warn(String.format("ToolBarObject of Type %s not implemented", item.getType()));
			}
		}
		
		if (tc != null) {
			this.tc.getJComponent().setToolTipText(tooltip);
			this.tc.getJComponent().setName(item.getKey());
			this.tc.getJComponent().addMouseMotionListener(editor);
			this.tc.getJComponent().addMouseListener(editor);
			this.tc.getJComponent().addComponentListener(editor);
		}
		
		if (mc != null) {
			this.mc.getJComponent().setName(item.getKey());
			this.mc.getJComponent().addMouseMotionListener(editor);
			this.mc.getJComponent().addMouseListener(editor);
			this.mc.getJComponent().addComponentListener(editor);
		}
	}
	
	public ToolBarItem getToolBarItem() {
		return this.item;
	}
	
	public JComponent getToolBarComponent() {
		if (this.tc == null) {
			return this.dummy;
		}
		return this.tc.getJComponent();
	}
	
	public JComponent getMenuComponent() {
		if (this.mc == null) {
			return this.dummy;
		}
		return this.mc.getJComponent();
	}

	public boolean isToolbarOnly() {
		return this.bToolbarOnly;
	}

	public boolean isMenuOnly() {
		return this.bMenuOnly;
	}
	
	public void resetComponents() {
		if (this.tc != null)
			this.tc.reset();
		if (this.mc != null)
			this.mc.reset();
	}
	
	private static void addContextMenu(final ToolBarObject tbo, final JComponent c) {
		c.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(c, SpringLocaleDelegate.getInstance().getText("ToolBarObject.1"), "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)) {
						tbo.getToolBarItem().setProperty(ToolBarItem.PROPERTY_SHOW_TEXT, Boolean.TRUE.toString());
						c.putClientProperty("hideActionText", Boolean.FALSE);
					} else {
						tbo.getToolBarItem().setProperty(ToolBarItem.PROPERTY_SHOW_TEXT, null);
						c.putClientProperty("hideActionText", Boolean.TRUE);
					}
				}
			}
		});
	}
	
}