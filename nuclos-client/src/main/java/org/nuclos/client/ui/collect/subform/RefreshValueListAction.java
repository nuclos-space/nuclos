package org.nuclos.client.ui.collect.subform;

import org.nuclos.common.UID;

/**
 * Created by Oliver Brausch on 17.07.17.
 */
public class RefreshValueListAction {
	private final UID targetComponentUid;
	private final UID parentComponentEntityUid;
	private final UID parentComponentUid;
	private final String sParameterNameForSourceComponent;

	/**
	 * @param targetComponentUid
	 * @param parentComponentEntityUid the entity name of the subform (shared by target component and parent component)
	 *   or the entity name of the form (in which case the parent component lies in the form, outside the subform).
	 * @param parentComponentUid
	 * @param sParameterNameForSourceComponent the name of the parameter in the valuelistprovider for the source component.
	 */
	public RefreshValueListAction(UID targetComponentUid, UID parentComponentEntityUid,
			UID parentComponentUid, String sParameterNameForSourceComponent) {
		this.targetComponentUid = targetComponentUid;
		this.parentComponentEntityUid = parentComponentEntityUid;
		this.parentComponentUid = parentComponentUid;
		this.sParameterNameForSourceComponent = sParameterNameForSourceComponent;
	}

	public UID getTargetComponentUID() {
		return this.targetComponentUid;
	}

	public UID getParentComponentEntityUID() {
		return this.parentComponentEntityUid;
	}

	public UID getParentComponentUID() {
		return this.parentComponentUid;
	}

	public String getParameterNameForSourceComponent() {
		return this.sParameterNameForSourceComponent;
	}
	// former inner class RefreshValueListAction
}
