package org.nuclos.client.ui;

import javax.swing.JComponent;

public interface EditorContainer {
	
	void initLayoutNavigationSupport(LayoutNavigationCollectable lnc);
	
	JComponent getInnerEditor();
}
