package org.nuclos.client.ui.layer;

import java.util.concurrent.Future;

import org.nuclos.client.command.ResultListener;

/**
 * Normally, a ResultListener is waiting for a result of a computation.
 * <p>
 * As part of a GUI, a part of the GUI is locked while waiting. In Nuclos
 * the part that is locked used to be a MainFrameTab. For unlocking it,
 * a the associated LayerLock is needed. 
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.15.3
 */
public interface LockingResultListener<R> extends ResultListener<R> {
	
	Future<LayerLock> getLayerLock();

}
