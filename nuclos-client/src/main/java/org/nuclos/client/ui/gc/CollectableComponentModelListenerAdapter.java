package org.nuclos.client.ui.gc;

import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;

public class CollectableComponentModelListenerAdapter extends EventAdapter implements CollectableComponentModelListener {
	
	CollectableComponentModelListenerAdapter(CollectableComponentModelListener wrapped) {
		super(wrapped);
	}

	@Override
	public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
		final CollectableComponentModelListener l = (CollectableComponentModelListener) wrapped.get();
		if (l != null) {
			l.collectableFieldChangedInModel(ev);
		}
	}

	@Override
	public void searchConditionChangedInModel(SearchComponentModelEvent ev) {
		final CollectableComponentModelListener l = (CollectableComponentModelListener) wrapped.get();
		if (l != null) {
			l.searchConditionChangedInModel(ev);
		}
	}

	@Override
	public void valueToBeChanged(DetailsComponentModelEvent ev) {
		final CollectableComponentModelListener l = (CollectableComponentModelListener) wrapped.get();
		if (l != null) {
			l.valueToBeChanged(ev);
		}
	}

}
