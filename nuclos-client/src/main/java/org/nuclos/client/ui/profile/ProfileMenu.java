//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.profile;

import java.awt.IllegalComponentStateException;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.event.PopupMenuEvent;

import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.BubbleUtils;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.PopupButton;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.profile.ProfileItem;
import org.nuclos.common.profile.PublishType;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.ProfileException;
import org.nuclos.common2.exception.WorkspaceException;
import org.springframework.util.StringUtils;

public class ProfileMenu {
	
	private ProfileMenu() {
		// Never invoked.
	}

	/**
	 * PopupButton with lazy loading
	 * 
	 */
	public static abstract class PopupButtonProfiles extends PopupButton {

		public PopupButtonProfiles(String text) {
			super(text);
			putClientProperty(ToolBarItem.COMPONENT_PROPERTY_KEY, NuclosToolBarItems.LIST_PROFILE);
		}

		public JPopupMenu getPopupMenu() {
			return this.popupMenu;
		}	

		@Override
		public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
			super.popupMenuWillBecomeInvisible(e);
		}

		@Override
		public void popupMenuCanceled(PopupMenuEvent e) {
			super.popupMenuCanceled(e);
		}

		@Override
		public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
			this.popupMenu.removeAll();
			for (final JComponent component : loadEntries()) {
				this.popupMenu.add(component);
			}
			super.popupMenuWillBecomeVisible(e);
		}

		public abstract List<JComponent> loadEntries();
	}

	public static PopupButtonProfiles createPopupButton(final AbstractProfilesController pctl, final NuclosCollectControllerCommonState state) {
		final PopupButtonProfiles popupButton = new PopupButtonProfiles(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.10", "Ansichten")) {

			@Override
			public List<JComponent> loadEntries() {
				return ProfileMenu.createMenuItems(pctl, state, this);
			}
		};

		popupButton.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.11", "Listenansichten verwalten."));
		return popupButton;
	}

	public static JMenu createMenu(final AbstractProfilesController pctl, final NuclosCollectControllerCommonState state) {
		final JMenu menu = new JMenu(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.10", "Ansichten"));

		menu.addMenuListener(new MenuListener() {

			@Override
			public void menuSelected(MenuEvent e) {
				menu.removeAll();
				for (final JComponent component : ProfileMenu.createMenuItems(pctl, state, menu)) {
					menu.add(component);
				}
			}

			@Override
			public void menuDeselected(MenuEvent e) {}

			@Override
			public void menuCanceled(MenuEvent e) {}
		});
		return menu;
	}

	private static List<JComponent> createMenuItems(final AbstractProfilesController pctl, final NuclosCollectControllerCommonState state, final JComponent parentComponent) {
		final List<JComponent> lstProfileMenu = new ArrayList<JComponent>();
		if (!pctl.getModel().getPersonalProfiles().isEmpty()) {
			lstProfileMenu.add(new JLabel("<html><b>" + SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.6", "Eigene Listenansichten") + "</b></html>"));
			for (final ProfileItem item: pctl.getModel().getPersonalProfiles()){
				lstProfileMenu.add(new ProfileMenuItem(pctl, state, item));
			}
		}

		if (!pctl.getModel().getPublicProfiles().isEmpty()) {
			lstProfileMenu.add(new JLabel("<html><b>" + SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.7", "Publizierte Listenansichten") + "</b></html>"));
			for (final ProfileItem item: pctl.getModel().getPublicProfiles()){
				lstProfileMenu.add(new ProfileMenuItem(pctl, state, item));
			}
		}

		// New Profile
		final ProfileActionMenuItem paNew = new ProfileActionMenuItem(pctl);
		paNew.setAction(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.1", "Neue Listenansicht"), Icons.getInstance().getIconNew16()) {

			@Override	
			public void actionPerformed(ActionEvent e) {
				try {
					final String msg = SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.5", "Name der neuen Listenansicht:");
					final String newProfile = JOptionPane.showInputDialog(parentComponent, msg, null,JOptionPane.QUESTION_MESSAGE);
					if (null == newProfile) {
						return;
					} else if (StringUtils.isEmpty(newProfile)) {
						throw new ProfileException(
								SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.12", "Bitte tragen Sie einen Namen ein."));
					}
					pctl.createNewProfile(newProfile);

				} catch (final ProfileException ex) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								(new Bubble(parentComponent, ex.getMessage(), 8, BubbleUtils.Position.SW)).setVisible(true);
							} catch (IllegalComponentStateException e) {
								JOptionPane.showMessageDialog(parentComponent, ex.getMessage());
							}
						}
					});
				}
			}
		});
			
		// Rename Profile
		final ProfileActionMenuItem paRename = new ProfileActionMenuItem(pctl);
		paRename.setAction(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.9", "aktuelle Listenansicht umbenennen."), Icons.getInstance().getIconEdit16()) {

			@Override	
			public void actionPerformed(ActionEvent e) {
				try {
					String[] initialValue = {pctl.getModel().getSelectedProfile().getName()};
					final String msg = SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.13", "Neuer Name:");
			
					final String newProfile = (String) JOptionPane.showInputDialog(parentComponent, msg, null,JOptionPane.QUESTION_MESSAGE, null, null, initialValue[0]);
					
					if (null == newProfile) {
						return;
					}else if (StringUtils.isEmpty(newProfile)) {
						throw new ProfileException(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.12", "Bitte tragen Sie einen Namen ein."));
					}
					pctl.renameSelectedProfile(newProfile);
				} catch (final WorkspaceException ex) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								(new Bubble(parentComponent, ex.getMessage(), 8, BubbleUtils.Position.SW)).setVisible(true);
							} catch (IllegalComponentStateException e) {
								JOptionPane.showMessageDialog(parentComponent, ex.getMessage());
							}
						}
					});
				} catch (final CommonPermissionException ex) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								(new Bubble(parentComponent, ex.getMessage(), 8, BubbleUtils.Position.SW)).setVisible(true);
							} catch (IllegalComponentStateException e) {
								JOptionPane.showMessageDialog(parentComponent, ex.getMessage());
							}
						}
					});
				}
			}
		});
		
		// Publish Profile
		final ProfileActionMenuItem paPublish= new ProfileActionMenuItem(pctl);
		paPublish.setAction(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage(
				"Workspace.Profile.ProfileMenu.2", "aktuelle Listenansicht publizieren."), 
				Icons.getInstance().getIconRedo16()) {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					pctl.publishProfile(pctl.getModel().getSelectedProfile(), PublishType.NEW);
					ProfileMenu.showMessage(SpringLocaleDelegate.getInstance().getMessage(
							"WorkspaceChooserController.16","Änderungen erfolgreich publiziert."), parentComponent);
				} catch (CommonBusinessException ex) {
					JOptionPane.showMessageDialog(parentComponent, ex.getMessage());
				}
			}
		});

		// Reset Profile
		
		final ProfileActionMenuItem paReset = new ProfileActionMenuItem(pctl);
		paReset.setAction(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage(
				"Workspace.Profile.ProfileMenu.3", "aktuelle Listenansicht auf Vorlage zurücksetzen."), 
				Icons.getInstance().getIconUndo16()) {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					pctl.resetSelectedProfile();
				} catch (CommonBusinessException ex) {
					JOptionPane.showMessageDialog(parentComponent, ex.getMessage());
				}
			}
		});

		// Remove Profile 
		
		final ProfileActionMenuItem paRemove = new ProfileActionMenuItem(pctl);
		paRemove.setAction(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileMenu.4", "aktuelle Listenansicht löschen."), Icons.getInstance().getIconRealDelete16()) {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					pctl.removeSelectedProfile();
				} catch (CommonBusinessException ex) {
					JOptionPane.showMessageDialog(parentComponent, ex.getMessage());
				}

			}
		});
		paRemove.getAction().setEnabled(pctl.isRemoveAllowed(pctl.getModel().getSelectedProfile()));	

		final Collection<JComponent> lstFunctions = new ArrayList<JComponent>();
		if (pctl.isCreatePersonalProfileAllowed()) {
			lstFunctions.add(paNew);
		}
		if (pctl.isRenameAllowed(pctl.getModel().getSelectedProfile())) {
			lstFunctions.add(paRename);
		}
		
		if (pctl.isPublishAllowed(pctl.getModel().getSelectedProfile())) {
			lstFunctions.add(paPublish);
		}
		if (pctl.isResetPossible(pctl.getModel().getSelectedProfile())) {
			lstFunctions.add(paReset);
		}
		if (pctl.isRemoveAllowed(pctl.getModel().getSelectedProfile())) {
			lstFunctions.add(paRemove);
		}
		
		if (!lstFunctions.isEmpty()) {
			lstProfileMenu.add(new JSeparator());
			for (final JComponent component : lstFunctions) {
				lstProfileMenu.add(component);
			}
		}
		return lstProfileMenu;
	}

	private final static void showMessage(final String message, final JComponent parentComponent) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					(new Bubble(parentComponent, message, 8, BubbleUtils.Position.SW)).setVisible(true);
				} catch (IllegalComponentStateException e) {
					JOptionPane.showMessageDialog(parentComponent, message);
				}
			}
		});
	}

}


