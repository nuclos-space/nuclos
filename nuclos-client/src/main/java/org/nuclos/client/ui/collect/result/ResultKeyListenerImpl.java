package org.nuclos.client.ui.collect.result;

import java.awt.event.KeyEvent;

import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class ResultKeyListenerImpl implements ResultKeyListener {
	
	private final JTable resultTable;
	
	private final CommonResultState state;
	
	ResultKeyListenerImpl(JTable resultTable, CommonResultState state) {
		this.resultTable = resultTable;
		this.state = state;
	}

	@Override
	public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
		if (e.getKeyCode() == ResultController.ESC) {
			if (!pressed) {
				resultTable.getSelectionModel().clearSelection();
				return true;
			}
		} else if (e.getKeyCode() == ResultController.UP || e.getKeyCode() == ResultController.DOWN) {
			if (pressed) {
				/* Warum wird dies benötigt?
				 * BasicTableUI.actionPerformed(...) sendet ein ungewolltes changeSelection mit toggle=false bei PFEIL-NACH-OBEN/-UNTEN (siehe unten).
				 * Der Standard in der Ergebnisansicht soll aber ein umgedrehtes Verhalten für Mausklicks sein. (siehe Implementierung 
				 * 		ResultPanel: super.changeSelection(rowIndex, columnIndex, alternateSelectionToggle? !toggle: toggle, extend);)  
				 * 
				 * else if (!inSelection) {
            	 * 		moveWithinTableRange(table, dx, dy);
            	 * 		table.changeSelection(leadRow, leadColumn, false, extend);
        		 * }
				 */
				state.setAlternateSelectionToggle(false);
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						state.setAlternateSelectionToggle(true);
					}
				});
			}
		}
		return false;
	}
}
