//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.releasenote;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.E;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;

class ReleaseNoteDetailsPanel extends JPanel {
	
	private static final Logger LOG = Logger.getLogger(ReleaseNoteDetailsPanel.class);
	
	private final SpringLocaleDelegate sld;
	
	private final ReleaseNoteOverviewTableModel model;
	
	private final ListSelectionModel selection;
	
	private final RowSorter sorter;
	
	private final JEditorPane editor;
	
	private final JLabel title;
	
	private final JButton previous;
	
	private final JButton ok;
	
	private final JButton next;
	
	private int viewRow;
	
	ReleaseNoteDetailsPanel(final ReleaseNoteOverviewTableModel model, final ListSelectionModel selection, 
			final RowSorter sorter, int viewRow) {
		super(new BorderLayout());
		if (model == null || selection == null || sorter == null) {
			throw new NullPointerException();
		}
		if (viewRow < 0 || viewRow >= model.getRowCount()) {
			throw new IllegalArgumentException();
		}
		this.model = model;
		this.selection = selection;
		this.sorter = sorter;
		this.viewRow = viewRow;
		this.sld = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
		
		this.title = new JLabel();
		add(title, BorderLayout.NORTH);
		
		editor = new ReleaseNoteEditorPane();
		add(new JScrollPane(editor), BorderLayout.CENTER);
		
		final JPanel south = new JPanel();
		south.setLayout(new FlowLayout());
		previous = new JButton("<");
		previous.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final int row = getViewRow();
				if (row > 0) {
					hasRead();
					setViewRow(row - 1);
				}
			}
		});
		south.add(previous);
		ok = new JButton("Ok");
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				hasRead();
				final Window window = UIUtils.getWindowForComponent(ReleaseNoteDetailsPanel.this);
				window.dispose();
			}
		});
		south.add(ok);
		next = new JButton(">");
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final int row = getViewRow();
				if (row < sorter.getViewRowCount() - 1) {
					hasRead();
					setViewRow(row + 1);
				}
			}
		});
		south.add(next);
		add(south, BorderLayout.SOUTH);
		
		setMinimumSize(new Dimension(300, 300));
				
		// this is needed to enable/disable buttons
		setViewRow(viewRow);
	}
	
	private int getViewRow() {
		return viewRow;
	}
	
	private void setViewRow(int viewRow) {
		boolean enabled = viewRow > 0;
		previous.setEnabled(enabled);
		enabled = viewRow < sorter.getViewRowCount() - 1;
		next.setEnabled(enabled);
		this.viewRow = viewRow;
		
		// change title
		final int modelRow = sorter.convertRowIndexToModel(viewRow);
		final EntityObjectVO<UID> eo = model.getRow(modelRow);
		final String label = sld.getMessage(
				"nuclet.release.note.details.label", "nuclet.release.note.details.label",
				eo.getFieldValue(E.NUCLETRELEASENOTE.nuclet.getUID()),
				eo.getFieldValue(E.NUCLETRELEASENOTE.nucletVersion.getUID()));
		title.setText(label);
		// change text
		final String content = eo.getFieldValue(E.NUCLETRELEASENOTE.content.getUID(), String.class);
		editor.setText(content);
		// select row
		selection.setSelectionInterval(viewRow, viewRow);
	}
	
	private void hasRead() {
		final int modelRow = sorter.convertRowIndexToModel(viewRow);
		final EntityObjectVO<UID> current = model.getRow(modelRow);
		final Boolean old = current.getFieldValue(ReleaseNoteOverviewTableModel.NEW_RN, Boolean.class);
		current.setFieldValue(ReleaseNoteOverviewTableModel.NEW_RN, false);
		// only fire if changed
		if (old == null || old.booleanValue() != false) {
			final UID nuclet = current.getFieldUid(E.NUCLETRELEASENOTE.nuclet.getUID());
			final Integer nucletVersion = current.getFieldValue(E.NUCLETRELEASENOTE.nucletVersion.getUID(), Integer.class);
			
			// fire table change event
			model.fireTableCellUpdated(viewRow, 2);
			model.storePrefs(model.getNewVersionsAtStart(), Collections.<UID>emptySet());
		}
	}
	
	private static class ReleaseNoteEditorPane extends JEditorPane implements HyperlinkListener {

		public ReleaseNoteEditorPane() {
			setContentType("text/html");
			setEditable(false);
			addHyperlinkListener(this);
		}
		
		@Override
		public void hyperlinkUpdate(HyperlinkEvent uriEvent) {
			try {
				if (uriEvent.getEventType() == EventType.ACTIVATED) {
					Desktop.getDesktop().browse(uriEvent.getURL().toURI());
				}
			} catch (Exception e) {
				LOG.warn(e.getMessage(), e);
			}
		}
		
	}

}
