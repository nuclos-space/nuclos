package org.nuclos.client.ui.collect.subform;

import java.util.Collection;

import org.nuclos.client.common.Utils;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Created by Oliver Brausch on 17.07.17.
 */
class SubformModelListener implements CollectableComponentModelListener {

	private final SubFormTableModel subformtblmdl;

	private final SubFormTable subformtbl;

	private final boolean bSearchable;

	private final Collection<TransferLookedUpValueAction> collTransferValueActions;

	private final CollectableComponent clctcomp;

	private final Collection<ClearAction> collClearActions;

	private final CollectableComponentTableCellEditor result;

	private final UID entityUID;

	SubformModelListener(SubFormTableModel subformtblmdl, boolean bSearchable,
						 SubFormTable subformtbl, Collection<TransferLookedUpValueAction> collTransferValueActions,
						 CollectableComponent clctcomp, Collection<ClearAction> collClearActions,
						 CollectableComponentTableCellEditor result, UID entityUID) {

		this.subformtblmdl = subformtblmdl;
		this.bSearchable = bSearchable;
		this.subformtbl = subformtbl;
		this.collTransferValueActions = collTransferValueActions;
		this.clctcomp = clctcomp;
		this.collClearActions = collClearActions;
		this.result = result;
		this.entityUID = entityUID;
	}

	@Override
	public void valueToBeChanged(DetailsComponentModelEvent ev) {
	}

	@Override
	public void searchConditionChangedInModel(SearchComponentModelEvent ev) {
	}

	@Override
	public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
		if (ev.getCollectableComponentModel().isInitializing())
			return; // @see RSWORGA-58

		if (!collClearActions.isEmpty()) {
			int i1 = subformtbl.getSelectedRow();
			SubForm.clearValues(subformtbl, subformtblmdl, i1 > -1 ? i1 : result.getLastEditingRow(), collClearActions);
		}
		if (!collTransferValueActions.isEmpty()) {
			Object id = null;
			try {
				CollectableField value = clctcomp.getField();
				if (value.getFieldType() == CollectableField.TYPE_VALUEIDFIELD) {
					id = value.getValueId();
				}
			} catch (CollectableFieldFormatException e1) {
				SubForm.LOG.warn("collectableFieldChangedInModel failed: " + e1, e1);
			}
			Collectable<?> clct = null;
			UID referencedEntity = clctcomp.getEntityField().getReferencedEntityUID();
			if (referencedEntity != null) {
				try {
					clct = Utils.getReferencedCollectable(entityUID, clctcomp.getFieldUID(), id);
				} catch (CommonBusinessException ex) {
					SubForm.LOG.error(ex);
				}
			}
			SubForm.transferLookedUpValues(clct, subformtbl, bSearchable, result.getLastEditingRow(), collTransferValueActions, true);
		}
	}
}
