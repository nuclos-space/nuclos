package org.nuclos.client.ui.collect;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.client.genericobject.CollectableGenericObjectAttributeField;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.CollectableMasterDataField;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

public abstract class AbstractUpdateAction<PK, Clct extends Collectable<PK>, S> implements MultiCollectablesActionController.Action<S, Object> {

	protected final CollectController<PK, Clct> ctl;

	protected final Map<UID, CollectableField> changedFields;

	protected AbstractUpdateAction(CollectController<PK, Clct> ctl) throws CommonBusinessException {
		this.ctl = ctl;
		this.changedFields = new HashMap<UID, CollectableField>();
		if (!ctl.getCollectState().isResultMode()) {
			for (CollectableComponent clctcomp : ctl.getEditView(false).getCollectableComponents()) {
				if (clctcomp.getDetailsModel().isValueToBeChanged()) {
					changedFields.put(clctcomp.getFieldUID(), clctcomp.getField());
				}
			}
		}
	}

	protected CollectableField prepareFieldForMultiUpdate(Map.Entry<UID, CollectableField> e) {
		CollectableField field = e.getValue();
		if (field.getValue() instanceof GenericObjectDocumentFile) {
			NuclosFile fileForAllObjects = ((GenericObjectDocumentFile) field.getValue()).getNuclosFile();
			// set new UID (NUCLOS-7314)
			UID docUid = DocumentFileBase.newFileUID();
			GenericObjectDocumentFile docFile = new GenericObjectDocumentFile(
					fileForAllObjects.getName(), docUid, fileForAllObjects.getContent());
			if (field instanceof CollectableValueIdField) {
				return new CollectableValueIdField(docUid, docFile);
			} else if (field instanceof CollectableGenericObjectAttributeField) {
				return new CollectableGenericObjectAttributeField(
						new DynamicAttributeVO(e.getKey(), null, docUid, docFile),
						field.getFieldType());
			} else if (field instanceof CollectableMasterDataField) {
				final CollectableMasterData<?> clctmd = ((CollectableMasterDataField) field).getCollectableMasterData();
				clctmd.setField(e.getKey(), new CollectableValueIdField(docUid, docFile));
				return clctmd.getField(e.getKey());
			}
		}
		return e.getValue();
	}

}
