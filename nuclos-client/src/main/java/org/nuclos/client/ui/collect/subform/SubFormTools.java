//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.ui.collect.subform;

import java.util.Collection;

import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.LookupEvent;
import org.nuclos.client.ui.collect.component.LookupListener;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;

public class SubFormTools {
	
	/**
	 * Clears the given values.
	 */
	public static void clearValues(SubFormTableModel subformtblmdl, int iRow, Collection<ClearAction> collClearActions) {
		// transfer the looked up values:
		for (ClearAction act : collClearActions) {
			final int iTargetColumn = subformtblmdl.findColumnByFieldUid(act.getTargetComponentName());
			if (iTargetColumn == -1) {
				return;
			}
			final CollectableEntityField clctefTarget = subformtblmdl.getCollectableEntityField(iTargetColumn);
			Object oValue = subformtblmdl.getNullValue(clctefTarget);

			// NUCLOS-6210 b)
			if (!clctefTarget.isNullable() && clctefTarget.getJavaClass() == Boolean.class) {
				CollectableField cf = clctefTarget.getDefault();
				if (cf != null && cf.getValue() != null) {
					oValue = cf;
				} else {
					oValue = new CollectableValueField(Boolean.FALSE);
				}
			}
			if (iRow >= 0) {
				subformtblmdl.setValueAt(oValue, iRow, iTargetColumn);
			}
		}
	}
	
	public static class LookupClearListener implements LookupListener {

		private final SubFormTableModel subformtblmdl;


		final Collection<ClearAction> collClearActions;
		private int iRow = -1;

		public LookupClearListener(SubFormTableModel subformtblmdl, int iRow, Collection<ClearAction> collClearActions) {

			this.subformtblmdl = subformtblmdl;
			this.collClearActions = collClearActions;
			this.iRow = iRow;
		}

		@Override
		public void lookupSuccessful(LookupEvent ev) {
			SubFormTools.clearValues(subformtblmdl, iRow, collClearActions);
		}

		@Override
        public int getPriority() {
            return 1;
        }
	}

	public static class LookupValuesListener implements LookupListener {

		private final SubFormTable subformtbl;

		private final boolean searchable;

		private final int row;

		private final Collection<TransferLookedUpValueAction> valueActions;

		public LookupValuesListener(SubFormTable subformtbl, boolean searchable, int row,
									Collection<TransferLookedUpValueAction> valueActions) {

			this.subformtbl = subformtbl;
			this.searchable = searchable;
			this.row = row;
			this.valueActions = valueActions;
		}

		@Override
		public void lookupSuccessful(LookupEvent ev) {
			if (ev.getSource() instanceof CollectableListOfValues) {
				final int iTargetColumn = subformtbl.getSubFormModel().findColumnByFieldUid(((CollectableListOfValues)ev.getSource()).getFieldUID());
				subformtbl.getSubFormModel().setValueAt(((CollectableListOfValues)ev.getSource()).getModel().getField(), subformtbl.getSelectedRow(), iTargetColumn);
			}
			SubForm.transferLookedUpValues(ev.getSelectedCollectable(), subformtbl, searchable,
					row, valueActions, true);
		}

		@Override
        public int getPriority() {
            return 1;
        }
	}
}
