package org.nuclos.client.ui.util;

import java.util.Comparator;

import javax.swing.AbstractButton;

import org.nuclos.common2.LangUtils;

public class ButtonComparator implements Comparator<AbstractButton> {

	public ButtonComparator() {
	}

	@Override
	public int compare(AbstractButton o1, AbstractButton o2) {
		final String t1 = o1.getText();
		final String t2 = o2.getText();
		return LangUtils.compare(t1, t2);
	}
	
}
