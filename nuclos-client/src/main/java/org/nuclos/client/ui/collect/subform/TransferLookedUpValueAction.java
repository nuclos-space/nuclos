package org.nuclos.client.ui.collect.subform;

import org.nuclos.common.UID;

/**
 * inner class TransferLookedUpValueAction.
 *
 * §todo it might be better to define all of these actions in collect.action.
 */
public class TransferLookedUpValueAction {
	private final UID sTargetComponentName;
	private final UID sSourceFieldName;

	public TransferLookedUpValueAction(UID sTargetComponentName, UID sSourceFieldName) {
		this.sTargetComponentName = sTargetComponentName;
		this.sSourceFieldName = sSourceFieldName;
	}

	public UID getTargetComponentName() {
		return this.sTargetComponentName;
	}

	public UID getSourceFieldName() {
		return this.sSourceFieldName;
	}
}	// inner class TransferLookedUpValueAction
