//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.labeled;

import java.awt.Dimension;

import javax.swing.JComponent;

import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.StrictSizeComponent;
import org.nuclos.client.ui.UIUtils;

/**
 * <code>LabeldComponent</code> that presents a value in a <code>ListOfValues</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public class LabeledListOfValues extends LabeledComponent implements StrictSizeComponent, IMultiSelectable {

	private final ListOfValues lov;
	private Dimension strictSize = null;

	public LabeledListOfValues(LabeledComponentSupport support) {
		super(support);
		lov = new ListOfValues(support);
		this.addControl(this.lov);
		this.getJLabel().setLabelFor(this.lov);
	}

	public ListOfValues getListOfValues() {
		return this.lov;
	}
	
	public java.awt.Font getFont() {
		return lov == null ? super.getFont() : lov.getFont();
	};
	
	public void setFont(java.awt.Font font) {
		if (lov == null) 
			super.setFont(font);
		else
			lov.setFont(font);
	};

	@Override
	public JComponent getControlComponent() {
		return this.lov;
	}

	@Override
	public void setName(String sName) {
		super.setName(sName);
		UIUtils.setCombinedName(this.lov, sName, "lov");
	}

	@Override
	public void setMultiSelect(Boolean multiSelect) {
		this.lov.setMultiSelect(multiSelect);
	}
	
	@Override	
	public void setMinimumSize(Dimension minimumSize) {
		if (strictSize == null)
			super.setMinimumSize(minimumSize);
	}

	@Override
	public void setSize(Dimension d) {
		if (strictSize == null)
			super.setSize(d);
	}

	@Override
	public void setSize(int width, int height) {
		if (strictSize == null)
			super.setSize(width, height);
	}

	@Override
	public void setPreferredSize(Dimension size) {
		if (strictSize == null) {
			super.setPreferredSize(size);
			lov.getJTextField().setPreferredSize(size);
		}
	}

	@Override
	public void setStrictSize(Dimension size) {
		strictSize = size;
		super.setMinimumSize(size);
		super.setSize(size);
		super.setPreferredSize(size);
		lov.getJTextField().setMinimumSize(size);
		lov.getJTextField().setSize(size);
		lov.getJTextField().setPreferredSize(size);
	}

	@Override
	public Dimension getStrictSize() {
		return strictSize;
	}
	
	@Override
	public Boolean isMultiSelect() {
		return this.lov.isMultiSelect();
	}
	
	public void setLovBtn(Boolean bLovBtn) {
		this.lov.setLovBtn(bLovBtn);
	}
	
	public void setLovSearch(Boolean bLovSearch) {
		this.lov.setLovSearch(bLovSearch);
	}

	public void setDropdownBtn(Boolean bDropdownBtn) {
		this.lov.setDropdownBtn(bDropdownBtn);
	}

	public void setCustomUsageSearch(String customUsageSearch) {
		this.lov.setCustomUsageSearch(customUsageSearch);
	}

}  // class LabeledListOfValues
