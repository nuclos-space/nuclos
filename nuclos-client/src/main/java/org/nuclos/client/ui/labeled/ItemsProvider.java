package org.nuclos.client.ui.labeled;

public interface ItemsProvider {
	
	int getItemCount();
	
	Object getItemAt(int i);
	
}
