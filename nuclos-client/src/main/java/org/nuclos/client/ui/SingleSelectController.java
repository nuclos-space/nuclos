//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * 
 * Autor: Oliver Brausch
 * Controller a Single Seclection Dialog
 * 
 * 
 */
public class SingleSelectController<T> extends SelectObjectsController<T>  implements ListSelectionListener {

	@SuppressWarnings("serial")
	private static class SingleSelectPanel<T> extends DefaultSelectObjectsPanel<T> {
		SingleSelectPanel() {
			this.pnlMiddleButtons.setVisible(false);
			this.pnlRightButtons.setVisible(false);
			this.pnlTitleSelectedColumns.setVisible(false);
			this.pnlSelectedColumns.setVisible(false);
			this.labSelectedColumns.setVisible(false);
			this.labAvailableColumns.setText("");			
			this.scrlpnAvailableColumns.setPreferredSize(new Dimension(200, 200));
		}
		
		void addRememberCheckBox(JCheckBox remember) {
			this.pnlTitleAvailableObjects.add(remember);
		}
	} 

	public SingleSelectController(Component parent, String rememberTxt, String tooltiptxt) {
		super(parent, new SingleSelectPanel<T>());
		final SingleSelectPanel<T> pnl = (SingleSelectPanel<T>) getPanel();
		final JList list = pnl.getJListAvailableObjects();
		if (rememberTxt != null) pnl.addRememberCheckBox(remember = new JCheckBox(rememberTxt));

		list.setToolTipText(tooltiptxt);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(this);
	}
	
	private T selectedItem = null;
	public T getSelectedObject() {
		return selectedItem;
	}
	
	private JCheckBox remember = null;
	public boolean rememberDecision() {
		return remember != null && remember.isSelected();
	}
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		Object obj = e.getSource();
		if (obj instanceof JList) {
			Object sel = ((JList)obj).getSelectedValue();
			selectedItem = (T)sel;
			enableOptionPaneOkButton(true);
		}
	}
	
	@Override
	protected void setupListeners() {		
	}
}
