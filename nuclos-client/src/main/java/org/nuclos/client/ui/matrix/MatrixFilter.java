//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.result.FilterableTable;
import org.nuclos.client.ui.collect.result.ResultFilter;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.FixedColumnRowHeader.FixedRowIndicatorTableModel;
import org.nuclos.client.ui.collect.subform.SubFormFilterPanel;
import org.nuclos.client.ui.collect.subform.SubFormParameterProvider;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.LangUtils;

/**
 * SubFormFilter that handles the collapsible filter panels for the
 * fixed and external tables of a SubForm and filters the corresponding table data.
 * <p>
 * containing the {@link SubFormFilterPanel}s.
 * 
 * @author	<a href="mailto:oliver.brausch@nuclos.de">Oliver Brausch</a>
 * @version 01.00.00
 */
public class MatrixFilter extends ResultFilter {

	private static final Logger LOG = Logger.getLogger(MatrixFilter.class);

	private SubFormParameterProvider parameterProvider;
	public JMatrixComponent matrix;

	MatrixFilter(JMatrixComponent matrix) {
		super(matrix);
		this.matrix = matrix;

		CollectableFieldsProviderCache valueListProviderCache = new CollectableFieldsProviderCache();
		CollectableFieldsProviderFactory collectableFieldsProviderFactory
				= MasterDataCollectableFieldsProviderFactory.newFactory(matrix.getEntityY(), valueListProviderCache);
		setupFilter(null, collectableFieldsProviderFactory);
	}

	private void setupFilter(SubFormParameterProvider parameterProvider,
					 CollectableFieldsProviderFactory collectableFieldsProviderFactory) {
		super.setupFilter(collectableFieldsProviderFactory);
		this.parameterProvider = parameterProvider;
	}

	public void invalidate() {
		created = false;
	}

	@Override
	public void createFilter(final boolean nonEmptyFilterLoaded) {
		if (created) {
			doFiltering();
			return;
		}

		created = true;

		_createFilter(matrix.getHeaderFixed().getColumnModel());
        setSubFormParameterProvider(parameterProvider);
		doFiltering();
	}

	@Override
	protected CollectableComponent newCollectableComponent(CollectableEntityField cef) {
		CollectableComponent clctcomp = super.newCollectableComponent(cef);
		if (clctcomp instanceof CollectableListOfValues) {
			clctcomp = new CollectableTextField(cef, true);
		}
		return clctcomp;
	}

	@Override
	protected JTable getFixedTable() {
		if (!(resultComponent instanceof JMatrixComponent)) {
			return null;
		}

		JMatrixComponent matrix = (JMatrixComponent) resultComponent;
		return matrix.getTableFixed();
	}

	private List<CollectableEntityField> getCollectableFields() {
		List<CollectableEntityField> lstFields = new ArrayList<>();

		for (UID fieldUid : matrix.lstSelectedFixed) {
			if (fieldUid != null && !UID.UID_NULL.equals(fieldUid)) {
				FieldMeta<?> fieldMeta = MetaProvider.getInstance().getEntityField(fieldUid);
				CollectableEntityField cef = new CollectableEOEntityField(fieldMeta);
				lstFields.add(cef);
			}
		}
		return lstFields;
	}

	/**
	 * create collectablecomponents as search components and assign them to the corresponding column
	 */
	@Override
	protected Map<UID, CollectableComponent> getSearchFilterComponents() {
		return getSearchFilterComponents(getCollectableFields());
	}

	@Override
	protected FilterableTable getExternalTable() {
		if (!(resultComponent instanceof JMatrixComponent)) {
			return null;
		}

		JMatrixComponent matrix = (JMatrixComponent) resultComponent;
		return matrix.getTable();
	}

	public void toolbarAction(String actionCommand) {
		if (ToolbarFunction.fromCommandString(actionCommand) == ToolbarFunction.FILTER) {
			createFilter(false);
			// collapse removes the filter, expanding filters with the (maybe set) entered filters
			if (getFixedResultFilter() != null && (!getFixedResultFilter().isCollapsed() || !getExternalResultFilter().isCollapsed())) {
				//NUCLEUSINT-789 f
				clearFilter();				filterButton.setSelected(false);
			} else {
				filter();
				filterButton.setSelected(true);
			}

			getFixedResultFilter().setCollapsed(!getFixedResultFilter().isCollapsed());
			getExternalResultFilter().setCollapsed(!getExternalResultFilter().isCollapsed());
		}
	}

	/**
	 * actionlistener to collapse or expand the searchfilter panels
	 */
	protected void addActionListener() {

	}

	private void setSubFormParameterProvider(SubFormParameterProvider parameterProvider) {
		this.parameterProvider = parameterProvider;
	}

	/**
	 * filters the tablemodel using the entered data in the filter components located above the columns
	 */
	@Override
	protected void filter() {
		// matrixFilterPanel is invisible by default. @see NUCLOSINT-1630
		if (getFixedResultFilter() == null || getExternalResultFilter() == null)
			return;

		int[] selRows = getFixedTable().getSelectedRows();
		List<Integer> lstSelRowsModel = Arrays.stream(selRows).map(k -> getFixedTable().convertRowIndexToModel(k)).boxed().collect(Collectors.toList());

		getFixedResultFilter().setVisible(true);
		getExternalResultFilter().setVisible(true);

		Map<UID, CollectableComponent> columnFilters = getAllFilterComponents();
		if (this.matrix.getMaxEntries() == null || this.matrix.getMaxEntries() == 0) {
			ArrayList<RowFilter<TableModel, Integer>> filters = new ArrayList<>();
			List<CollectableEntityField> lstCef = getCollectableFields();

			for (CollectableEntityField cef : lstCef) {
				CollectableComponent clctcomp = columnFilters.get(cef.getUID());
				if (setFieldCondition(clctcomp, cef)) {
					filters.add(new MatrixRowFilter(cef, clctcomp, matrix));
				}
			}

			RowFilter<TableModel, Integer> filter = null;
			filters.addAll(fixedRowFilters);
			if (filters.size() == 1) {
				filter = filters.get(0);
			} else if (filters.size() > 1) {
				filter = RowFilter.andFilter(filters);
			}

			setRowFilter(filter);

			filteringActive = (filter != null);

		} else {
			createConditionFromFilter(columnFilters);
		}

		List<Integer> lstSelTableModel = lstSelRowsModel.stream().map(k -> getFixedTable().convertRowIndexToView(k)).collect(Collectors.toList());

		for (Integer row : lstSelTableModel) {
			if (row >= 0) {
				getFixedTable().getSelectionModel().addSelectionInterval(row, row);
			}
		}

	}

	/**
	 * Removes filtering
	 * NUCLEUSINT-789 f
	 */
	public void clearFilter() {
		Icon icon = Icons.getInstance().getIconFilter16();
		this.filterButton.setIcon(icon);

		setRowFilter(null);

		filteringActive = false;
	}

	private void setRowFilter(RowFilter<TableModel, Integer> filter) {
		final RowSorter<?> rowSorter = getFixedTable().getRowSorter();
		if (rowSorter instanceof TableRowSorter) {
			((TableRowSorter<?>) rowSorter).setRowFilter(filter);
		}
		final RowSorter<?> rowSorter2 = getExternalTable().getRowSorter();
		if (rowSorter2 instanceof TableRowSorter) {
			((TableRowSorter<?>) rowSorter2).setRowFilter(filter);
		}
	}

	@Override
	protected String getSearchStringForLike(final String s) {
		if (matrix.getSearchStringAdaption() == null) {
			return super.getSearchStringForLike(s);
		}

		// e.g: "*" + s.replaceAll("\\+", "\\\\\\+") + "*";
		return matrix.getSearchStringAdaption().apply(s);
	}

	public class MatrixRowFilter extends AbstractRowFilter {
		private JMatrixComponent matrix;

		public MatrixRowFilter(CollectableEntityField cef, CollectableComponent clctcomp, JMatrixComponent mtx) {
			super(cef, clctcomp);
			this.matrix = mtx;
			if (clctcomp instanceof CollectableTextField) {
				CollectableTextField collectableTextField = (CollectableTextField) clctcomp;
				if (collectableTextField.getComparisonOperator() == ComparisonOperator.EQUAL) {
					collectableTextField.setComparisonOperator(ComparisonOperator.LIKE);
				}
			}
		}

		@Override
		protected Column getColumnNew() {
			return (Column)matrix.getColumn(cef.getUID());
		}

		@Override
		public boolean include(Entry<? extends TableModel, ? extends Integer> entry) {
			TableModel model = entry.getModel();			
			if (model instanceof FixedRowIndicatorTableModel) {
				model = ((FixedRowIndicatorTableModel) model).getExternalModel();
			}
			
			int colIndex = -1;
			if (model instanceof MatrixTableModel) {
				TableColumnModel columnModel = matrix.getHeaderFixed().getColumnModel();
				for (int i = 0; i < columnModel.getColumnCount(); i++) {
					TableColumn tc = columnModel.getColumn(i);
					if (cef.getUID().equals(tc.getIdentifier())) {
						colIndex = i;
						break;
					}
				}
			}

			if (colIndex == -1) {
				return true;
			}

			try {
				/**
				 * NUCLOS-1897
				 * Valuelist Provider is executed by rendering, the model only contains id's. This results in an invalid string comparison.
				 * Solution: References get compared by id (maybe later by BusinessObject comparables?)
				 */
				boolean comparisonByReference = false;
				if (!MatrixFilter.this.filterByString && clctcomp.getField() instanceof CollectableValueIdField) {
					if (clctcomp.getField().getValueId() != null)
						comparisonByReference = true;
				}

				Object v = model.getValueAt(entry.getIdentifier(), colIndex);

				if (!(v instanceof MatrixCollectable))
					return true;

				MatrixCollectable f = (MatrixCollectable) v;

				Object o1;

				if (comparisonByReference) {
					if (f.getValueId(f.getField()) instanceof UID) {
						o1 = f.getValueId(f.getField());
					} else {
						o1 = IdUtils.toLongId(f.getValueId(f.getField()));
					}
				} else {
					o1 = f.getValue(f.getField());

					if (f.getYgroupkey() != null && o1 instanceof String) {
						// NUCLOS-8381 combined strings within groups
						String s1 = (String) o1;
						for (int y = 0; y < model.getRowCount(); y++) {
							if (!entry.getIdentifier().equals(y)) {
								MatrixCollectable mcOther = (MatrixCollectable)model.getValueAt(y, colIndex);
								if (mcOther.getYgroupkey() != null && f.getYgroupkey().getGroup() == mcOther.getYgroupkey().getGroup()) {
									Object oOther = mcOther.getValue(mcOther.getField());
									if (!LangUtils.equal(oOther, o1)) {
										s1 += "|" + oOther;
									}
								}
							}
						}
						o1 = s1;
					}

				}
		
				return includeImpl(o1, comparisonByReference);

			} catch (CollectableFieldFormatException e) {
				LOG.warn("include failed: " + e, e);
				ResultFilter.handleFilterException(cef.getLabel());
				return true;
			}
		}

	}

	@Override
	protected void loadTableFilter() {
	}

	@Override
	public JToggleButton getToggleButton() {
		if (!(resultComponent instanceof JMatrixComponent)) {
			return null;
		}
		return ((JMatrixComponent)resultComponent).getFilterButton();
	}

	@Override
	protected JCheckBoxMenuItem getMenuItem() {
		return null;
	}

}
