//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.model;

import java.awt.Color;
import java.util.List;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.tasklist.TasklistDefinition;

/**
 * A read-only table model wrapping a {@link ResultVO} as internal 
 * representation.
 * <p>
 * However, it hides the INID column from being seen with the normal (i.e. 
 * {@link TableModel}) methods. Because of this there some additional methods for 
 * seeing the internal/real representation.
 * </p><p>
 * This class was formerly known as org.nuclos.client.task.DynamicTaskTableModel.
 * </p>
 * @author Thomas Pasch (javadoc, refactoring)
 */
public class ResultVOTableModel extends DefaultTableModel {

	private ResultVO resultvo;
	int rowColorColumnIndex=-1;
	
	public Color getRowColor(int row) {
		if(rowColorColumnIndex==-1)
			return null;
		else
		{			
			Object res=resultvo.getRows().get(row)[rowColorColumnIndex];		
			if(res==null)
				return null;			

			return Color.decode((String) res);			
		}
	}
	public ResultVOTableModel(TasklistDefinition def, ResultVO resultvo, List<UID> columnOrder) {
		this.resultvo = resultvo;
		for (UID column : columnOrder) {
			if (isMetaColumn(def, column) || !containsInternColumn(column)) {
				continue;
			}
			else {
				columnIdentifiers.add(column);
			}
		}
		for (int i = 0; i < resultvo.getColumnCount(); i++) {
			ResultColumnVO col = resultvo.getColumns().get(i);
			final UID columnUid = new UID(col.getColumnLabel());
			
			
			if(col.getColumnLabel().toUpperCase().endsWith("NUCLOSROWCOLOR"))
			{
				rowColorColumnIndex=i;
				continue;
			}
			if (columnIdentifiers.contains(columnUid) || isMetaColumn(def, columnUid) || !containsInternColumn(columnUid)) {
				continue;
			}
			else {
				columnIdentifiers.add(columnUid);
			}
		}
	}
	
	public void setData(ResultVO r) {
		this.resultvo = r;
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return resultvo != null ? resultvo.getRows().size() : 0;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		String columnName = getColumnName(columnIndex);
		final UID column = UID.isStringifiedUID(columnName) ? UID.parseUID(columnName) : new UID(columnName);
		final int index = getInternColumnIndex(column);
		return resultvo.getColumns().get(index).getColumnClass();
	}

	@Override
	public Object getValueAt(int iRow, int iColumn) {
		String columnName = getColumnName(iColumn);
		final UID column = UID.isStringifiedUID(columnName) ? UID.parseUID(columnName) : new UID(columnName);
		final int index = getInternColumnIndex(column);
		return getInternValueAt(iRow, index);
	}
	
	public Object getInternValueAt(int iRow, int iColumn) {
		return resultvo.getRows().get(iRow)[iColumn];
	}

	@Override
	public void setValueAt(Object oValue, int iRow, int iColumn) {
		// do nothing because this is a read only model
	}

	private boolean containsInternColumn(UID column) {
		return getInternColumnIndex(column) > -1;
	}
	
	public int getInternColumnIndex(UID column) {
		int i = 0;
		for (ResultColumnVO col : resultvo.getColumns()) {
			if (col.getColumnLabel().equals(column.getString())) {
				return i;
			}
			i++;
		}
		return -1;
	}
	
	private boolean isMetaColumn(TasklistDefinition def, UID column) {
		if (column.equals(def.getDynamicTasklistIdFieldUid())) {
			return true;
		}
		else if (column.equals(def.getDynamicTasklistEntityFieldUid())) {
			return true;
		}
		else if (column.equals(def.getCustomRuleIdFieldUid())) {
			return true;
		}
		else if (column.equals(def.getCustomRuleEntityFieldUid())) {
			return true;
		}
		return false;
	}
}
