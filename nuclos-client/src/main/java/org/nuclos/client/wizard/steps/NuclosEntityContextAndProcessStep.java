//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import static org.nuclos.client.masterdata.valuelistprovider.EntityFieldsCollectableFieldsProvider.ENTITY_UID;
import static org.nuclos.client.masterdata.valuelistprovider.EntityFieldsCollectableFieldsProvider.RESTRICT_REFERENCE_ENTITY;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.prefs.Preferences;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.masterdata.valuelistprovider.EntityFieldsCollectableFieldsProvider;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.component.CollectableComboBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModel;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubFormParameterProvider;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*/
public class NuclosEntityContextAndProcessStep extends NuclosEntityAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityContextAndProcessStep.class);

	private static final long serialVersionUID = 2900241917334839766L;

	public static final String[] labels = TranslationVO.LABELS_ENTITY;

	protected SubForm subformProcess;
	protected SubForm subformContext;
	protected MasterDataSubFormController subFormControllerContext;
	protected MasterDataSubFormController subFormControllerProcess;

	private CollectableComponentModelProvider provider;

	private JTabbedPane tabbedPane;
	
	public NuclosEntityContextAndProcessStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	protected void initComponents() {
		tabbedPane = new JTabbedPane();
		TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.FILL);
		tbllay.getTableLayout().setVGap(3);
		tbllay.getTableLayout().setHGap(5);
		tbllay.newRow(TableLayout.FILL).add(tabbedPane);
	}

	@Override
	public void prepare() {
		super.prepare();
		tabbedPane.removeAll();

		subformContext = new SubForm(E.ENTITYCONTEXT.getUID(), false, JToolBar.VERTICAL, UID.UID_NULL, E.ENTITYCONTEXT.mainEntity.getUID()) {
			@Override
			public TableCellEditor getTableCellEditor(final JTable tbl, final int iRow,
													  final CollectableEntity clcte, final CollectableEntityField clctefTarget,
													  final SubFormTableModel subformtblmdl, final boolean bSearchable, final Preferences prefs,
													  final CollectableFieldsProviderFactory getCollectableFieldsProviderFactory,
													  final SubFormParameterProvider parameterProvider,
													  final UID mandator, final Long intidForVLP) {
				TableCellEditor editor = super.getTableCellEditor(tbl, iRow, clcte, clctefTarget, subformtblmdl, bSearchable, prefs,
						getCollectableFieldsProviderFactory, parameterProvider, mandator, intidForVLP);
				if (editor instanceof CollectableComponentTableCellEditor) {
					CollectableComponentTableCellEditor ccEditor = (CollectableComponentTableCellEditor) editor;
					if (clctefTarget.getUID().equals(E.ENTITYCONTEXT.dependentEntityField.getUID())) {
						CollectableComponent clctcomp = ccEditor.getCollectableComponent();
						if (clctcomp instanceof CollectableComboBox) {
							CollectableComboBox clctComboBox = (CollectableComboBox) clctcomp;
							CollectableEntityField depEntityField = clcte.getEntityField(E.ENTITYCONTEXT.dependentEntity.getUID());
							int iColDepEntityField = subformtblmdl.getColumn(depEntityField);
							Object selectedEntity = subformtblmdl.getValueAt(iRow, iColDepEntityField);
							UID depEntityUID = null;
							if (selectedEntity != null && selectedEntity instanceof CollectableValueIdField) {
								depEntityUID = (UID) ((CollectableValueIdField) selectedEntity).getValueId();
							}
							EntityFieldsCollectableFieldsProvider fieldsProvider = (EntityFieldsCollectableFieldsProvider) clctComboBox.getValueListProvider();
							fieldsProvider.setParameter(ENTITY_UID, depEntityUID);
							clctComboBox.refreshValueList(false);
						}
					}
				}
				return editor;
			}
		};
		Column refFieldColumn = new Column(E.ENTITYCONTEXT.dependentEntityField.getUID());
		EntityFieldsCollectableFieldsProvider fieldsProvider = new EntityFieldsCollectableFieldsProvider(null, true);
		fieldsProvider.setParameter(RESTRICT_REFERENCE_ENTITY, model.getUID());
		refFieldColumn.setValueListProvider(fieldsProvider);
		subformContext.addColumn(refFieldColumn);
		Column depEntityColumn = new Column(E.ENTITYCONTEXT.dependentEntity.getUID());
		depEntityColumn.setValueListProvider(new DependentEntityCollectableFieldsProvider(model.getUID()));
		subformContext.addColumn(depEntityColumn);
		subformProcess = new SubForm(E.PROCESS.getUID(), false, JToolBar.VERTICAL, UID.UID_NULL, E.PROCESS.module.getUID());
		Column column = new Column(E.PROCESS.nuclet.getUID(), "Nuclet", new CollectableComponentType(CollectableUtils.getCollectableComponentTypeForClass(String.class), null), false, false,false, false, false, false, 0, 0, null);
		subformProcess.addColumn(column);

		tabbedPane.addTab(SpringLocaleDelegate.getInstance().getMsg("wizard.step.processes.label.context"), subformContext);
		if (model.isStateModel()) {
			tabbedPane.addTab(SpringLocaleDelegate.getInstance().getMsg("wizard.step.processes.label.processes"), subformProcess);
		}
		MainFrameTab tab = getModel().getParentFrame();

		try {
			CollectableComponentModelProvider provider = getCollectableComponentModelProvider();

			Preferences prefsContext = java.util.prefs.Preferences.userRoot().node("org/nuclos/client/entitywizard/steps/context");
			subFormControllerContext = new ContextSubformController(tab, provider,E.ENTITYCONTEXT.getUID(), subformContext, prefsContext,
					getEntityPreferences(), null);
			Preferences prefsProcess = java.util.prefs.Preferences.userRoot().node("org/nuclos/client/entitywizard/steps/process");
			subFormControllerProcess = new ProcessSubformController(tab, provider,E.PROCESS.getUID(), subformProcess, prefsProcess,
					getEntityPreferences(), null);

			fillSubform(subFormControllerContext, model.getContexts());
			fillSubform(subFormControllerProcess, model.getProcesses());

			subFormControllerContext.getCollectableTableModel().addTableModelListener(new TableModelListener() {
				@Override
				public void tableChanged(final TableModelEvent e) {
					if (e.getType() == TableModelEvent.UPDATE) {
						subFormControllerContext.getCollectableTableModel().getCollectables().stream().forEach(clct -> {
							CollectableMasterData clctmd = (CollectableMasterData) clct;
							CollectableField depEntityField = clctmd.getField(E.ENTITYCONTEXT.dependentEntity.getUID());
							if (depEntityField.isNull()) {
								// Entity is null -> also set the field to null
								clctmd.setField(E.ENTITYCONTEXT.dependentEntityField.getUID(), CollectableValueIdField.NULL);
							} else {
								// Entity is NOT null
								UID depEntityId = (UID) depEntityField.getValueId();
								boolean bFindDepEntityField = false;
								if (clctmd.getField(E.ENTITYCONTEXT.dependentEntityField.getUID()).isNull()) {
									bFindDepEntityField = true;
								} else {
									// validate depEntity <-> depEntityField
									UID depEntityFieldId = (UID) clctmd.getField(E.ENTITYCONTEXT.dependentEntityField.getUID()).getValueId();
									FieldMeta<?> efMeta = MetaProvider.getInstance().getEntityField(depEntityFieldId);
									if (!efMeta.getEntity().equals(depEntityId)) {
										// depEntity changed!
										bFindDepEntityField = true;
									}
								}

								if (bFindDepEntityField) {
									EntityFieldsCollectableFieldsProvider fieldsProvider = new EntityFieldsCollectableFieldsProvider(depEntityId, true);
									fieldsProvider.setParameter(RESTRICT_REFERENCE_ENTITY, model.getUID());
									try {
										CollectableValueIdField clctVid = (CollectableValueIdField) fieldsProvider.getCollectableFields().stream()
												.findFirst().orElse(CollectableValueIdField.NULL);
										clctmd.setField(E.ENTITYCONTEXT.dependentEntityField.getUID(), clctVid);
									} catch (CommonBusinessException ex) {
										LOG.error(ex);
									}
								}
							}
						});
					}
				}
			});

			setComplete(true);
		} catch (Exception e) {
			Errors.getInstance().showExceptionDialog(tab, e);
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				subformContext.requestFocusInWindow();
			}
		});

	}

	private static class DependentEntityCollectableFieldsProvider implements CollectableFieldsProvider {

		private static final String MAIN_ENTITY_ID = "mainEntityId";

		private UID mainEntityUID;

		public DependentEntityCollectableFieldsProvider(final UID mainEntityUID) {
			this.mainEntityUID = mainEntityUID;
		}

		@Override
		public void setParameter(final String sName, final Object oValue) {
			if (MAIN_ENTITY_ID.equals(sName)) {
				mainEntityUID = (UID) oValue;
			}
		}

		@Override
		public List<CollectableField> getCollectableFields() throws CommonBusinessException {
			List<CollectableField> result = new ArrayList<>();
			if (mainEntityUID != null) {
				final MetaProvider metaProvider = MetaProvider.getInstance();
				Collection<FieldMeta<?>> allReferencingFields = metaProvider.getAllReferencingFields(mainEntityUID);
				allReferencingFields.stream().forEach(fieldMeta -> result.add(
						new CollectableValueIdField(fieldMeta.getEntity(), metaProvider.getEntity(fieldMeta.getEntity()).getEntityName())));
			}
			Collections.sort(result);
			return result;
		}
	}

	public static <R> Predicate<R> not(Predicate<R> predicate) {
		return predicate.negate();
	}

	@Override
	public void applyState() throws InvalidStateException {

		Collection<EntityObjectVO<UID>> contexts = getSubformData(subFormControllerContext);
		Collection<EntityObjectVO<UID>> processes = getSubformData(subFormControllerProcess);

		// validate contexts
		if (contexts.stream()
				.filter(not(EntityObjectVO::isFlagRemoved)).anyMatch(context ->
					context.getFieldUid(E.ENTITYCONTEXT.dependentEntity) == null ||
					context.getFieldUid(E.ENTITYCONTEXT.dependentEntityField) == null)) {
			JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMsg(
					"wizard.step.processes.error.context.mandatory"),
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
			throw new InvalidStateException();
		}
		boolean areAllUnique = contexts.stream()
				.filter(not(EntityObjectVO::isFlagRemoved))
				.map(context -> context.getFieldUid(E.ENTITYCONTEXT.dependentEntity)).allMatch(new HashSet<>()::add);
		if (!areAllUnique) {
			JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMsg(
					"wizard.step.processes.error.context.unique"),
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
			throw new InvalidStateException();
		}

		// validate processes
		Set<String> names = new HashSet<String>();
		for (EntityObjectVO<UID> process : processes) {
			String name = process.getFieldValue(E.PROCESS.name);
			if (StringUtils.isNullOrEmpty(name)) {
				JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.processes.error.name.mandatory", "Bitte definieren Sie für jede Aktion einen Namen."),
		    			SpringLocaleDelegate.getInstance().getMessage("wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
	 	        throw new InvalidStateException();
			}
			if (!names.add(name)) {
				JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.processes.error.name.unique", "Der Name einer Aktion muss eindeutig sein ({0}).", name),
		    			SpringLocaleDelegate.getInstance().getMessage("wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
	 	        throw new InvalidStateException();
			}
		}
		model.setContexts(contexts);
		model.setProcesses(processes);

		subFormControllerContext.close();
		subFormControllerProcess.close();
		super.applyState();
		
		// close Subform support
		subformContext.close();
		subformProcess.close();

		super.applyState();
	}

	private Collection<EntityObjectVO<UID>> getSubformData(MasterDataSubFormController subFormController) throws InvalidStateException {
		List<CollectableEntityObject> subformdata;
		try {
			subformdata = subFormController.getCollectables(true, true, true);
		} catch (CommonValidationException e1) {
			JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMessageFromResource(e1.getMessage()),
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
			throw new InvalidStateException();
		}
		Collection<EntityObjectVO<UID>> eoData = CollectionUtils.transform(subformdata, new Transformer<CollectableEntityObject, EntityObjectVO<UID>>() {
			@Override
			public EntityObjectVO<UID> transform(CollectableEntityObject i) {
				final EntityObjectVO<UID> eo = i.getEntityObjectVO();
				if (eo.getPrimaryKey() == null)
					eo.setPrimaryKey(new UID());
				return eo;
			}
		});
		return eoData;
	}

	private class ContextSubformController extends MasterDataSubFormController<UID> {

		public ContextSubformController(MainFrameTab tab, CollectableComponentModelProvider clctcompmodelproviderParent, UID parentEntity, SubForm subform, Preferences prefsUserParent,
										EntityPreferences entityPrefs, CollectableFieldsProviderCache valueListProviderCache) {
			super(tab, clctcompmodelproviderParent, parentEntity, subform, prefsUserParent, entityPrefs, valueListProviderCache, null, null);
		}

	}

	private class ProcessSubformController extends MasterDataSubFormController<UID> {

		public ProcessSubformController(MainFrameTab tab, CollectableComponentModelProvider clctcompmodelproviderParent, UID parentEntity, SubForm subform, Preferences prefsUserParent, 
				EntityPreferences entityPrefs, CollectableFieldsProviderCache valueListProviderCache) {
			super(tab, clctcompmodelproviderParent, parentEntity, subform, prefsUserParent, entityPrefs, valueListProviderCache, null, null);
		}

		@Override
		protected void removeSelectedRows() {
			for (CollectableEntityObject<UID> process : this.getSelectedCollectables()) {
				if (process.getId() != null) {
					try {
						MetaDataDelegate.getInstance().tryRemoveProcess(process.getEntityObjectVO());
					}
					catch (NuclosBusinessException e) {
						JOptionPane.showMessageDialog(NuclosEntityContextAndProcessStep.this,
								getSpringLocaleDelegate().getMessage(
										"wizard.step.processes.error.removeprocess", "Aktion {0} ist bereits in Verwendung und kann nicht entfernt werden.", process.getField(E.PROCESS.name.getUID())),
								getSpringLocaleDelegate().getMessage(
										"wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
						return;
					}
				}
			}
			super.removeSelectedRows();
		}
	}

	protected static void fillSubform(MasterDataSubFormController subFormController, Collection<EntityObjectVO<UID>> data) {
		if (data != null) {
			try {
				subFormController.clear();
				subFormController.fillSubForm(null, data);
			} catch (NuclosBusinessException e) {
				throw new NuclosFatalException(e);
			}
		}
	}

	protected final CollectableComponentModelProvider getCollectableComponentModelProvider() {
		if (provider == null) {
			provider = new CollectableComponentModelProvider() {
				private CollectableComponentModel entityModel = new DetailsComponentModel(DefaultCollectableEntityProvider.getInstance().getCollectableEntity(E.ENTITY.getUID()).getEntityField(E.ENTITY.entity.getUID()));

				@Override
				public Collection<UID> getFieldUids() {
					return Collections.singleton(E.ENTITY.entity.getUID());
				}

				@Override
				public Collection<? extends CollectableComponentModel> getCollectableComponentModels() {
					return Collections.singleton(entityModel);
				}

				@Override
				public CollectableComponentModel getCollectableComponentModelFor(UID fieldUID) {
					if (E.ENTITY.entity.getUID().equals(fieldUID)) {
						return entityModel;
					}
					return null;
				}
			};
		}
		return provider;
	}

	@Override
	public void close() {
		tabbedPane = null;

		if (subFormControllerContext != null) {
			subFormControllerContext.close();
		}
		subFormControllerContext = null;
		if (subformContext != null) {
			subformContext.close();
		}
		subformContext = null;
		if (subFormControllerProcess != null) {
			subFormControllerProcess.close();
		}
		subFormControllerProcess = null;
		if (subformProcess != null) {
			subformProcess.close();
		}
		subformProcess = null;

		super.close();
	}
}
