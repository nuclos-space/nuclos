//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

/**
 * ConfigurationTreeModel
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class ConfigurationTreeModel extends DefaultTreeModel{

	private boolean isIgnoreListeners = false;


	public ConfigurationTreeModel(ConfigurationTreeNode<?> root) {
		super(root);
		this.root = root;
	}

	public boolean ignoreListeners() {
		return this.isIgnoreListeners;
	}
	public boolean isMeOrAnyParentInactive(ConfigurationTreeNode<?> node) {
		boolean result = false;
		if (!node.isActive()) {
			result = true;
		} else {

			ConfigurationTreeNode<?> parentNode = node;
			do {
				parentNode = (ConfigurationTreeNode<?>)parentNode.getParent();
				if(null != parentNode && !parentNode.isActive()) {
					result = true;
					break;
				}
			}while(null != parentNode);
		}
		return result;
	}

	@Override
	public void removeNodeFromParent(MutableTreeNode node) {
		this.isIgnoreListeners = false;
		super.removeNodeFromParent(node);
	}

	public void removeNodeFromParent(MutableTreeNode node, boolean fireEvent) {
		this.isIgnoreListeners = !fireEvent;
		super.removeNodeFromParent(node);
	}

}
