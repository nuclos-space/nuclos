//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer;

import java.lang.reflect.Constructor;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.explorer.node.DatasourceDirectoryExplorerNode;
import org.nuclos.client.explorer.node.DatasourceExplorerNode;
import org.nuclos.client.explorer.node.DatasourceReportExplorerNode;
import org.nuclos.client.explorer.node.EventSupportExplorerNode;
import org.nuclos.client.explorer.node.EventSupportTargetExplorerNode;
import org.nuclos.client.explorer.node.GenericObjectExplorerNode;
import org.nuclos.client.explorer.node.GenericObjectSearchResultExplorerNode;
import org.nuclos.client.explorer.node.MasterDataExplorerNode;
import org.nuclos.client.explorer.node.NucletContentCustomComponentExplorerNode;
import org.nuclos.client.explorer.node.NucletContentEntityExplorerNode;
import org.nuclos.client.explorer.node.NucletContentEntryExplorerNode;
import org.nuclos.client.explorer.node.NucletContentExplorerNode;
import org.nuclos.client.explorer.node.NucletExplorerNode;
import org.nuclos.client.explorer.node.PersonalSearchFiltersByEntityExplorerNode;
import org.nuclos.client.explorer.node.SearchFilterExplorerNode;
import org.nuclos.client.explorer.node.SubFormEntryExplorerNode;
import org.nuclos.client.explorer.node.SubFormExplorerNode;
import org.nuclos.client.explorer.node.datasource.AllDatasourceNode;
import org.nuclos.client.explorer.node.datasource.DatasourceNode;
import org.nuclos.client.explorer.node.datasource.DatasourceReportFormularNode;
import org.nuclos.client.explorer.node.datasource.DatasourceUsageNode;
import org.nuclos.client.explorer.node.datasource.DirectoryDatasourceNode;
import org.nuclos.client.explorer.node.datasource.OwnDatasourceNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetTreeNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTreeNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportUsageExplorerNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportUsageNode;
import org.nuclos.client.rule.client.explorer.ClientRuleEntityExplorerNode;
import org.nuclos.client.rule.client.explorer.ClientRuleEntityNode;
import org.nuclos.client.rule.client.explorer.ClientRuleFieldExplorerNode;
import org.nuclos.client.rule.client.explorer.ClientRuleFieldNode;
import org.nuclos.client.rule.client.explorer.ClientRuleFieldRuleExplorerNode;
import org.nuclos.client.rule.client.explorer.ClientRuleFieldRuleNode;
import org.nuclos.client.rule.client.explorer.ClientRuleNucletExplorerNode;
import org.nuclos.client.rule.client.explorer.ClientRuleNucletNode;
import org.nuclos.client.rule.client.explorer.ClientRuleRootExplorerNode;
import org.nuclos.client.rule.client.explorer.ClientRuleRootNode;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.SearchFilterCache;
import org.nuclos.client.searchfilter.SearchFilterTreeNode;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.preferences.ExplorerRestorePreferences;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNode;
import org.nuclos.server.navigation.treenode.EntitySearchResultTreeNode;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNodeParameters;
import org.nuclos.server.navigation.treenode.LabelTreeNode;
import org.nuclos.server.navigation.treenode.MasterDataSearchResultTreeNode;
import org.nuclos.server.navigation.treenode.RelationTreeNode;
import org.nuclos.server.navigation.treenode.SubFormEntryTreeNode;
import org.nuclos.server.navigation.treenode.SubFormTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.navigation.treenode.nuclet.NucletTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.DefaultNucletContentEntryTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentCustomComponentTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentEntityTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentPreferenceTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentProcessTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentSearchfilterNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.ReportNucletContentTreeNode;

/**
 * Factory for creating <code>ExplorerNode</code>s out of <code>TreeNode</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class ExplorerNodeFactory {

	private static final Logger LOG = Logger.getLogger(ExplorerNodeFactory.class);

	private static ExplorerNodeFactory singleton;

	/**
	 * maps TreeNodes (classes) to ExplorerNode constructors
	 */
	private final Map<Class<? extends TreeNode>, Constructor<? extends ExplorerNode>> mp = CollectionUtils.newHashMap();

	public static synchronized ExplorerNodeFactory getInstance() {
		if (singleton == null) {
			singleton = newFactory();
		}
		return singleton;
	}

	private static ExplorerNodeFactory newFactory() {
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getExplorerNodeFactoryClassName(),
					ExplorerNodeFactory.class.getName());

			return (ExplorerNodeFactory) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName).newInstance();
		}
		catch (Exception ex) {
			throw new CommonFatalException("ExplorerNodeFactory cannot be created.", ex);
		}
	}

	protected ExplorerNodeFactory() {
		this.putConstructor(GenericObjectTreeNode.class, GenericObjectExplorerNode.class);
		this.putConstructor(EntitySearchResultTreeNode.class, GenericObjectSearchResultExplorerNode.class);
		this.putConstructor(PersonalSearchFiltersByEntityTreeNode.class, PersonalSearchFiltersByEntityExplorerNode.class);
		this.putConstructor(SearchFilterTreeNode.class, SearchFilterExplorerNode.class);
		this.putConstructor(RelationTreeNode.class, RelationExplorerNode.class);
		this.putConstructor(SubFormTreeNode.class, SubFormExplorerNode.class);
		this.putConstructor(SubFormEntryTreeNode.class, SubFormEntryExplorerNode.class);
		this.putConstructor(EventSupportUsageNode.class, EventSupportUsageExplorerNode.class);
		this.putConstructor(DefaultMasterDataTreeNode.class, MasterDataExplorerNode.class);
		
		this.putConstructor(ClientRuleNucletNode.class, ClientRuleNucletExplorerNode.class);
		this.putConstructor(ClientRuleEntityNode.class, ClientRuleEntityExplorerNode.class);
		this.putConstructor(ClientRuleFieldNode.class, ClientRuleFieldExplorerNode.class);
		this.putConstructor(ClientRuleFieldRuleNode.class, ClientRuleFieldRuleExplorerNode.class);
		this.putConstructor(ClientRuleRootNode.class, ClientRuleRootExplorerNode.class);
		
		this.putConstructor(EventSupportTreeNode.class, EventSupportExplorerNode.class);
		this.putConstructor(EventSupportTargetTreeNode.class, EventSupportTargetExplorerNode.class);
		this.putConstructor(DatasourceNode.class, DatasourceExplorerNode.class);
		this.putConstructor(DatasourceReportFormularNode.class, DatasourceReportExplorerNode.class);
		this.putConstructor(DirectoryDatasourceNode.class, DatasourceDirectoryExplorerNode.class);
		this.putConstructor(AllDatasourceNode.class, DatasourceDirectoryExplorerNode.class);
		this.putConstructor(DatasourceUsageNode.class, DatasourceDirectoryExplorerNode.class);
		this.putConstructor(OwnDatasourceNode.class, DatasourceDirectoryExplorerNode.class);

		this.putConstructor(NucletTreeNode.class, NucletExplorerNode.class);
		
		this.putConstructor(NucletContentTreeNode.class, NucletContentExplorerNode.class);
		this.putConstructor(ReportNucletContentTreeNode.class, NucletContentExplorerNode.class);
		
		this.putConstructor(DefaultNucletContentEntryTreeNode.class, NucletContentEntryExplorerNode.class);
		this.putConstructor(NucletContentProcessTreeNode.class, NucletContentEntryExplorerNode.class);
		this.putConstructor(NucletContentSearchfilterNode.class, NucletContentEntryExplorerNode.class);
		this.putConstructor(NucletContentPreferenceTreeNode.class, NucletContentEntryExplorerNode.class);
		
		this.putConstructor(NucletContentEntityTreeNode.class, NucletContentEntityExplorerNode.class);
		this.putConstructor(NucletContentCustomComponentTreeNode.class, NucletContentCustomComponentExplorerNode.class);

		
		this.putConstructor(LabelTreeNode.class, ExplorerNode.class);
	}

	protected void putConstructor(Class<? extends TreeNode> clsTreeNode, Class<? extends ExplorerNode> clsExplorerNode) {
		try {
			this.mp.put(clsTreeNode, clsExplorerNode.getDeclaredConstructor(TreeNode.class));
		}
		catch (NoSuchMethodException ex) {
			throw new CommonFatalException("ExplorerNode must define ctor(TreeNode).", ex);
		}
	}

	/**
	 * @param treenode
	 * @param doRefresh TODO
	 * @return a new <code>ExplorerNode</code> that is appropriate to present the given <code>TreeNode</code>.
	 */
	public ExplorerNode<?> newExplorerNode(TreeNode treenode, boolean doRefresh) {
		final ExplorerNode<?> result;

		// refresh content of treenode
		if(doRefresh) {
			try {
				treenode = treenode.refreshed();
			} catch (CommonFinderException | CommonPermissionException e) {
				throw new ExplorerNodeRefreshException("Der Knoten "+treenode.getLabel()+" in der Baumansicht konnte nicht wiederhergestellt werden.\n" + e.getMessage());
			}
		}

		final Class<? extends TreeNode> clsTreeNode = treenode.getClass();
		final Constructor<? extends ExplorerNode> ctorExplorerNode = this.mp.get(clsTreeNode);
		if (ctorExplorerNode == null) {
			// default: create plain ExplorerNode for unregistered TreeNodes:
			result = new ExplorerNode<TreeNode>(treenode);
			LOG.warn("Unspecific ExplorerNode created for class " + clsTreeNode.getName());
		}
		else {
			try {
				result = ctorExplorerNode.newInstance(treenode);
			}
			catch (Exception ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return result;
	}
	
	public static TreeNode createTreeNodeFromPreferences(ExplorerRestorePreferences rp) {
		if (rp.getSearchFilterId() != null) {
			EntitySearchFilter esf = SearchFilterCache.getInstance().getEntitySearchFilterById(rp.getSearchFilterId());
			if (esf != null) {
				return new SearchFilterTreeNode(esf);				
			}
		}
		
		if (rp.getTreeNodeClassName() != null) try {
			Class<?> clazz = Class.forName(rp.getTreeNodeClassName());
			
			if (rp.getGenericObjectTreeNodeParameters() != null) {
				Constructor<?> constructor = clazz.getConstructor(GenericObjectTreeNodeParameters.class);
				return (TreeNode)constructor.newInstance(rp.getGenericObjectTreeNodeParameters());
				
			} else {
				return (TreeNode)clazz.newInstance();
				
			}
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		if (rp.getGenericObjectTreeNodeParameters() != null) {
			return new GenericObjectTreeNode(rp.getGenericObjectTreeNodeParameters());
		}
		
		if (rp.getMasterDataSearchResultNodeParameters() != null) {
			return new MasterDataSearchResultTreeNode(rp.getMasterDataSearchResultNodeParameters());
		}
		
		if (rp.getNucletTreeNodeParameters() != null) {
			return new NucletTreeNode(rp.getNucletTreeNodeParameters());
		}

		if (rp.getDefaultMasterDataTreeNodeParameters() != null) {
			return new DefaultMasterDataTreeNode(rp.getDefaultMasterDataTreeNodeParameters());
		}

		if (rp.getId() != null) {
			//TODO: IMPLEMENT WHAT TO DO
		}
		
		return rp.getTreeNodeRoot();
	}



}	// class ExplorerNodeFactory
