package org.nuclos.client.explorer.node.eventsupport;

import java.awt.Point;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;
import org.nuclos.api.rule.CommunicationRule;
import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.api.rule.JobRule;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.client.explorer.node.EventSupportExplorerNode;
import org.nuclos.client.explorer.node.EventSupportTargetExplorerNode;
import org.nuclos.client.rule.server.EventSupportDataFlavor;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;

public class EventSupportDropListener implements DropTargetListener {

	private static final Logger LOG = Logger.getLogger(EventSupportDropListener.class);
	
	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		try {
			dropTargetDrag(dtde);
		}
		catch (UnsupportedFlavorException e) {
			LOG.error(e.getMessage(), e);
			dtde.rejectDrag();
		}
		catch (IOException e) {
			LOG.error(e.getMessage(), e);
			dtde.rejectDrag();
		}
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		try {
			dropTargetDrag(dtde);
		}
		catch (UnsupportedFlavorException e) {
			LOG.error(e.getMessage(), e);
			dtde.rejectDrag();
		}
		catch (IOException e) {
			LOG.error(e.getMessage(), e);
			dtde.rejectDrag();
		}
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
		try {
			dropTargetDrag(dtde);
		}
		catch (UnsupportedFlavorException e) {
			LOG.error(e.getMessage(), e);
			dtde.rejectDrag();
		}
		catch (IOException e) {
			LOG.error(e.getMessage(), e);
			dtde.rejectDrag();
		}
	}

	@Override
	public void dragExit(DropTargetEvent dte) {		
	}

	void dropTargetDrag(DropTargetDragEvent ev) throws UnsupportedFlavorException, IOException {
		// Target Tree
		JTree tree = (JTree) ((DropTarget)ev.getSource()).getComponent();
		// Transfer Source
		EventSupportExplorerNode esenSource = (EventSupportExplorerNode) 
				ev.getTransferable().getTransferData(EventSupportDataFlavor.FLAVOR);
		
		Point p = ev.getLocation();		
		int row = tree.getClosestRowForLocation(p.x, p.y);
		tree.setSelectionRow(row);
		
		EventSupportTargetExplorerNode node = (EventSupportTargetExplorerNode) getSelectedTreeNode(tree);
		 
		if (node != null && node.getTreeNode() != null && node.getTreeNode().getTreeNodeType() != null)
		{
			EventSupportTreeNode treeNode = esenSource.getTreeNode();
			
			switch (node.getTreeNode().getTreeNodeType()) {
			case ENTITY:
			case ENTITY_INTEGRATION_POINT:
				if (EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType()) && 
					!StateChangeRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName()) &&
					!StateChangeFinalRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName()) && 
					!GenerateRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName()) &&
					!GenerateFinalRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName()) &&
					!JobRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName()))
				{	
					ev.acceptDrag(ev.getDropAction());
				}
				else
				{
					ev.rejectDrag();
				}
				break;
			case STATEMODEL:
				if (EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType()) && (
					StateChangeRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName()) || 
					StateChangeFinalRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName())))
					{	
						ev.acceptDrag(ev.getDropAction());
					}
					else
					{
						ev.rejectDrag();
					}
				break;
			case JOB:
				if (EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType()) && 
						 JobRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName()))
							{	
								ev.acceptDrag(ev.getDropAction());
							}
							else
							{
								ev.rejectDrag();
							}
				break;
			case GENERATION:
				if (EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType()) && (
						 GenerateRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName()) || 
						 GenerateFinalRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName())))
							{	
								ev.acceptDrag(ev.getDropAction());
							}
							else
							{
								ev.rejectDrag();
							}
				break;
			case COMMUNICATION_PORT:
				if (EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType()) && (
						 CommunicationRule.class.getCanonicalName().equals(treeNode.getParentNode().getNodeName())))
							{	
								ev.acceptDrag(ev.getDropAction());
							}
							else
							{
								ev.rejectDrag();
							}
				break;
			default:
				ev.rejectDrag();
				break;
			}
		}
		else {
			ev.rejectDrag();
		}
    }
	
	@Override
	public void drop(DropTargetDropEvent dtde) {	
		boolean success = false;
		dtde.acceptDrop(dtde.getDropAction());
		
		try {	
			DropTarget targetNode = (DropTarget) dtde.getSource();
			
			EventSupportTargetExplorerNode esenTarget = (EventSupportTargetExplorerNode) getSelectedTreeNode((JTree) targetNode.getComponent());
						
			// Get dragged Node
			EventSupportExplorerNode esenSource = (EventSupportExplorerNode) 
					dtde.getTransferable().getTransferData(EventSupportDataFlavor.FLAVOR);
			
			if (esenTarget.getTreeNode().getTreeNodeType().equals(EventSupportTargetType.ENTITY) ||
					esenTarget.getTreeNode().getTreeNodeType().equals(EventSupportTargetType.ENTITY_INTEGRATION_POINT)) {
				// and attach it to the target entity
				EventSupportEventVO addedEseVO = esenSource.getTreeNode().getController().addEventSupportToEntity(esenSource.getTreeNode(), esenTarget.getTreeNode());	
				EventSupportRepository.getInstance().invalidate(addedEseVO);
				success = addedEseVO != null;
			}
			else if(esenTarget.getTreeNode().getTreeNodeType().equals(EventSupportTargetType.STATEMODEL)) {
				// and attach it to the selected statetransition
				EventSupportTransitionVO addedEstVO = esenSource.getTreeNode().getController().addEventSupportToStateTransition(esenSource.getTreeNode(), esenTarget.getTreeNode());
				EventSupportRepository.getInstance().invalidate(addedEstVO);
				success = addedEstVO != null;
			}
			else if(esenTarget.getTreeNode().getTreeNodeType().equals(EventSupportTargetType.JOB)) {
				// and attach it to the selected jobcontroller
				EventSupportJobVO addedEstVO = esenSource.getTreeNode().getController().addEventSupportToJob(esenSource.getTreeNode(), esenTarget.getTreeNode());
				EventSupportRepository.getInstance().invalidate(addedEstVO);
				success = addedEstVO != null;
			}
			else if(esenTarget.getTreeNode().getTreeNodeType().equals(EventSupportTargetType.GENERATION)) {
				// and attach it to the selected jobcontroller
				EventSupportGenerationVO addedEstVO = esenSource.getTreeNode().getController().addEventSupportToGeneration(esenSource.getTreeNode(), esenTarget.getTreeNode());
				EventSupportRepository.getInstance().invalidate(addedEstVO);
				success = addedEstVO != null;
			}
			else if(esenTarget.getTreeNode().getTreeNodeType().equals(EventSupportTargetType.COMMUNICATION_PORT)) {
				// and attach it to the selected communication port
				EventSupportCommunicationPortVO addedEstVO = esenSource.getTreeNode().getController().addEventSupportToCommunicationPort(esenSource.getTreeNode(), esenTarget.getTreeNode());
				EventSupportRepository.getInstance().invalidate(addedEstVO);
				success = addedEstVO != null;
			}
			
			if (success)
			{
				
				//NUCLOS-3013 complete invalidate is too much
				//EventSupportDelegate.getInstance().invalidateCaches();
				
				EventSupportTargetTreeNode treeNode = (EventSupportTargetTreeNode) esenTarget.getTreeNode();
				treeNode.setLstSubNodes(treeNode.getController().createTargetSubNodesByType(treeNode, null));
				esenTarget.refresh((JTree) targetNode.getComponent(), true);
				((JTree) targetNode.getComponent()).setSelectionPath(new TreePath(esenTarget.getPath()));
				esenTarget.getTreeNodeActionOnMouseClick((JTree) targetNode.getComponent()).actionPerformed(new ActionEvent(this, -1, "Dropped"));
			}
			dtde.dropComplete(success);
		}
		catch (UnsupportedFlavorException e) {
			LOG.fatal(e.getMessage(), e);
		}
		catch (IOException e) {
			LOG.fatal(e.getMessage(), e);
		}
		catch (CommonFinderException e) {
			LOG.fatal(e.getMessage(), e);
		}
		catch (CommonPermissionException e) {
			LOG.fatal(e.getMessage(), e);
		}		
	}

	private ExplorerNode<?> getSelectedTreeNode(final JTree tree) {
		TreePath treePath = tree.getSelectionPath();
		return (treePath == null) ? null : (ExplorerNode<?>) treePath.getLastPathComponent();
	}
	
}
