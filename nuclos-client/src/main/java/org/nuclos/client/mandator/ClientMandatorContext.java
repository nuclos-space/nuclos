//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.mandator;

import org.apache.log4j.Logger;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.UID;

public class ClientMandatorContext {

	private static final Logger LOG = Logger.getLogger(ClientMandatorContext.class);
	
	private static MandatorVO mandator;
	
	public static void setMandator(MandatorVO mandator) {
		ClientMandatorContext.mandator = mandator;
		LOG.info("Set mandator " + mandator);
	}
	
	public static MandatorVO getMandator() {
		return ClientMandatorContext.mandator;
	}
	
	public static UID getMandatorUID() {
		if (ClientMandatorContext.mandator != null) {
			return ClientMandatorContext.mandator.getUID();
		}
		return null;
	}
	
	public static String getMandatorName() {
		if (ClientMandatorContext.mandator != null) {
			return ClientMandatorContext.mandator.getName();
		}
		return null;
	}
	
	public static String getMandatorPath() {
		if (ClientMandatorContext.mandator != null) {
			return ClientMandatorContext.mandator.getPath();
		}
		return null;
	}
	
}
