package org.nuclos.client.startup;

import java.io.Serializable;

import org.springframework.beans.factory.InitializingBean;

public interface LocalUserCache extends Serializable, InitializingBean {
	
	String getCachingTopic();
	
	boolean wasDeserialized();
	
	void setDeserialized(boolean blnDeserialized);
	
	boolean isValid();
	
}
