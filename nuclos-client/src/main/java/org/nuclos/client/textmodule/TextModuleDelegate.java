package org.nuclos.client.textmodule;

import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.textmodule.TextModule;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.server.textmodule.TextModuleFacadeRemote;
import org.springframework.beans.factory.annotation.Autowired;

public class TextModuleDelegate {
	private static final Logger LOG = Logger.getLogger(TextModuleDelegate.class);
	private static TextModuleDelegate INSTANCE = null;
	
	@Autowired
	TextModuleFacadeRemote tmFacade;
	
	public static TextModuleDelegate getInstance() {
		if (INSTANCE == null) {
			// lazy support
			INSTANCE = SpringApplicationContextHolder.getBean(TextModuleDelegate.class);
		}
		return INSTANCE;
	}
	
	
	public List<TextModule> findTextModules(final TextModuleSettings tms) {
		return tmFacade.findTextModules(tms);
	}
	
	
	
}
