//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.dbtransfer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.dbtransfer.TransferNuclet;
import org.nuclos.common.dbtransfer.ZipInput;
import org.nuclos.common.dbtransfer.ZipOutput;
import org.nuclos.server.dbtransfer.TransferFacadeRemote;

import info.clearthought.layout.TableLayout;

public class DBTransferUtils {

	private static final Logger LOG = Logger.getLogger(DBTransferUtils.class);
	
	//
	
	// former Spring injection
	
	private TransferFacadeRemote transferFacadeRemote;
	
	// end of former Spring injection
	
	public static final int TABLE_LAYOUT_V_GAP = 3;
	public static final int TABLE_LAYOUT_H_GAP = 5;
	
	public DBTransferUtils() {
		setTransferFacadeRemote(SpringApplicationContextHolder.getBean(TransferFacadeRemote.class));
	}
	
	final void setTransferFacadeRemote(TransferFacadeRemote transferFacadeRemote) {
		this.transferFacadeRemote = transferFacadeRemote;
	}
	
	final TransferFacadeRemote getTransferFacadeRemote() {
		return transferFacadeRemote;
	}
	
	protected void initJPanel(JPanel panel, double[] cols, double[] rows) {	
		final double size [][] = {cols, rows};
		final TableLayout layout = new TableLayout(size);
		
		layout.setVGap(TABLE_LAYOUT_V_GAP);
		layout.setHGap(TABLE_LAYOUT_H_GAP);
		
		panel.setLayout(layout);
	}
	
	public static JFileChooser getFileChooser(final String fileDescription, final String fileExtension, final boolean dirsAllowed){
		JFileChooser filechooser = new JFileChooser();
		FileFilter filefilter = new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isDirectory() || file.getName().toLowerCase().endsWith(fileExtension);
			}
			@Override
			public String getDescription() {
				return fileDescription + " (*" + fileExtension + ")";
			}
		};
		if (dirsAllowed) {
			filechooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		}
		filechooser.setFileFilter(filefilter);
		
		return filechooser;
	}
	
	protected JFileChooser getDirChooser(){
		JFileChooser filechooser = new JFileChooser();
		filechooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		return filechooser;
	}
	
	protected byte[] getBytes(InputStream in, int minBufferSize)	throws IOException {
		LinkedList<byte[]> bufs = new LinkedList<byte[]>();
		byte[] buf = new byte[Math.max(in.available(), minBufferSize)];
		int offset = 0, size = 0;
		for (int n; (n = in.read(buf, offset, buf.length - offset)) > 0;) {
			offset += n;
			if (offset == buf.length) {
				bufs.add(buf);
				size += offset;
				buf = new byte[Math.max(in.available(), minBufferSize)];
				offset = 0;
			}
		}
		if (offset == 0 && bufs.size() == 1)
			return bufs.get(0);
		else {
			bufs.add(buf);
			size += offset;
			buf = new byte[size];
			for (byte[] b : bufs) {
				System.arraycopy(
					b, 0, buf, buf.length - size, Math.min(b.length, size));
				size -= b.length;
			}
			return buf;
		}
	}
	
	protected static void writeToZip(ZipOutput zout, File dir, File rootDir) throws IOException {
		if (dir.exists()) {
			if (isHidden(dir)) {
				return;
			}
			
			if (dir.isDirectory()) {
				for (File child : dir.listFiles()) {
					writeToZip(zout, child, rootDir);
				}
			} else {
				InputStream is = new FileInputStream(dir);
			    long length = dir.length();
			    byte[] bytes = new byte[(int)length];
			    int offset = 0;
			    int numRead = 0;
			    try {
				    while (offset < bytes.length
				           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
				        offset += numRead;
				    }
				    if (offset < bytes.length) {
				        throw new IOException("Could not completely read file "+dir.getName());
				    }
			    }
			    finally {
			    	is.close();
			    }
			    String root = rootDir.getPath() + File.separator;
			    String filepath = dir.getPath();
			    String zippath = filepath.substring(root.length());
			    zout.addEntry(zippath.replace(File.separatorChar, '/'), bytes);
			}
		}
	}
	
	protected static void writeToDir(File dir, byte[] nuclet) throws IOException {
		if (dir.exists()) {
			deleteNucletFiles(dir);
		}
		dir.mkdirs();
		ZipInput zin = new ZipInput(new ByteArrayInputStream(nuclet));
		try {
			ZipEntry ze;
			
			while ((ze = zin.getNextEntry()) != null) {
				String name = ze.getName();
				File part = new File(dir, name);
				part.mkdirs();
				part.delete();
				FileOutputStream fos = new FileOutputStream(part);
				fos.write(zin.readEntry());
				fos.close();
			}
		} finally {
			zin.close();
		}
	}
	
	private static void deleteNucletFiles(File dir) {
		if (dir.exists()) {
			//Remove System files like ".DS_Store"
			//if (isHidden(dir)) { 
			if (".svn".equals(dir.getName()) || ".git".equals(dir.getName())) {
				return;
			}
			
			if (dir.isDirectory()) {
				for (File child : dir.listFiles()) {
					deleteNucletFiles(child);
				}
				try {
					// try to remove dir if it does not contain .svn or .git
					File svnFile = new File(dir.getCanonicalPath() + File.separatorChar + ".svn");
					File gitFile = new File(dir.getCanonicalPath() + File.separatorChar + ".git");
					if (!svnFile.exists() && !gitFile.exists() && 
							dir.listFiles().length == 0) {
						dir.delete();
					}
				} catch (Exception ex) {
					// ignore
				}
			} else {
				dir.delete();
			}
		}
	}
	
	private static boolean isHidden(File file) {
		return file.exists() && file.isHidden() || file.getName().startsWith(".");
	}
	
	protected void startProgressThread(final JProgressBar progressBar, final int runUntil, final int sleepTime) {
		new Thread("DBTransferUtils.progressThread") {
			@Override
			public void run() {
				try {
					while(progressBar.getValue()<runUntil){
						progressBar.setValue(progressBar.getValue()+1);
						sleep(sleepTime);
					}
				}
				catch(Exception e) {
					LOG.warn("startProgressThread failed: " + e);
				}
			}
		}.start();
	}

	protected TransferNuclet[] getAvailableNuclets() {
		List<TransferNuclet> result = getTransferFacadeRemote().getAvailableNuclets();
		return result.toArray(new TransferNuclet[0]);
	}

}
