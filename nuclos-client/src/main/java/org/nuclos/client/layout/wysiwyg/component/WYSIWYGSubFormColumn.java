//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.nuclos.client.layout.wysiwyg.WYSIWYGEditorModes;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.common.collect.collectable.CollectableEntityField;

/**
 * 
 * 
 * 
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class WYSIWYGSubFormColumn extends AbstractWYSIWYGTableColumn implements WYSIWYGComponent, Serializable, WYSIWYGEditorModes {
	
	private static final Logger LOG = Logger.getLogger(WYSIWYGSubFormColumn.class);
	

	private WYSIWYGSubForm subform;
	

	public WYSIWYGSubFormColumn(WYSIWYGSubForm subform, CollectableEntityField field) {
		super(field);
		this.subform = subform;
	}
	
	public void setLabel(String label) {
		this.subform.setSubFormFromProperties();
	}
	
	public WYSIWYGSubForm getSubForm(){
		return subform;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getParentEditor()
	 */
	@Override
	public WYSIWYGLayoutEditorPanel getParentEditor(){
		return getSubForm().getParentEditor();
	}
	

}
