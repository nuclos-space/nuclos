//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General License for more details.
//
//You should have received a copy of the GNU Affero General License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.gef;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;

/**
 * §todo naming?! A ShapeModel seems to be a container holding Shapes, not the model of a Shape.
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */

public interface ShapeModel {
	/**
	 *
	 * @param shape
	 */
	void addSelection(Shape shape);

	/**
	 *
	 * @param shapes
	 */
	void addSelections(Collection<Shape> shapes);

	/**
	 *
	 * @return
	 */
	Collection<Shape> getSelection();

	/**
	 *
	 * @return
	 */
	boolean isMultiSelected();

	/**
	 *
	 */
	void selectAll();

	/**
	 *
	 */
	void deselectAll();

	/**
	 *
	 * @param p
	 * @return
	 */
	Shape findSelectionAt(Point2D p);

	/**
	 *
	 */
	void clear();

	/**
	 *
	 * @param shape
	 */
	void removeSelection(Shape shape);

	/**
	 *
	 * @param shapes
	 */
	void removeSelectionList(Collection<Shape> shapes);

	/**
	 *
	 * @param shape
	 */
	void addShape(Shape shape);

	/**
	 *
	 * @param shapes
	 */
	void addShapes(Collection<Shape> shapes);

	/**
	 *
	 * @param shape
	 */
	void removeShape(Shape shape);

	/**
	 *
	 * @param shapes
	 */
	void removeShapes(Collection<Shape> shapes);

	/**
	 *
	 * @param p
	 * @return
	 */
	Shape findShapeAt(Point2D p);

	/**
	 *
	 * @param p
	 * @return
	 */
	Shape findConnectableShapeAt(Point2D p);

	/**
	 *
	 * @param r
	 * @return
	 */
	Collection<Shape> findShapesIn(Rectangle2D r);

	/**
	 *
	 * @param listener
	 */
	void addShapeModelListener(ShapeModelListener listener);

	/**
	 *
	 * @param listener
	 */
	void removeShapeModelListener(ShapeModelListener listener);

	/**
	 *
	 */
	void beginUpdate();

	/**
	 *
	 */
	void endUpdate();

	/**
	 *
	 * @param sName
	 * @param bUserLayer
	 * @return
	 */
	AbstractShapeModel.Layer addLayer(String sName, boolean bUserLayer, int iOrder);

	/**
	 *
	 * @return
	 */
	ArrayList<AbstractShapeModel.Layer> getVisibleLayers();

	/**
	 *
	 * @return
	 */
	AbstractShapeModel.Layer getActiveLayer();

	/**
	 *
	 * @param layer
	 */
	void setActiveLayer(AbstractShapeModel.Layer layer);

	/**
	 *
	 * @param sName
	 * @throws ShapeControllerException
	 */
	void setActiveLayer(String sName) throws ShapeControllerException;

	/**
	 *
	 * @param viewer
	 */
	void setView(ShapeViewer viewer);

	/**
	 *
	 * @return
	 */
	ShapeViewer getView();

	/**
	 *
	 */
	void fireModelChanged();

	/**
	 *
	 * @return
	 */
	Rectangle2D getShapeDimension();

	/**
	 *
	 * @param iId
	 * @return
	 */
	Shape getShape(Integer iId);
}
