package org.nuclos.client.common;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.geom.AffineTransform;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.textmodule.TextModuleDelegate;
import org.nuclos.client.textmodule.TextModulePopupListener;
import org.nuclos.client.ui.collect.DynamicRowHeightChangeListener;
import org.nuclos.client.ui.collect.DynamicRowHeightSupport;
import org.nuclos.client.ui.collect.component.CollectableLocalizedComponent;
import org.nuclos.client.ui.collect.component.CollectableTextArea;
import org.nuclos.client.ui.collect.component.CollectableTextComponent;
import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledTextAreaWithButton;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.textmodule.TextModule;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common2.StringUtils;

public class NuclosCollectableLocalizedTextArea extends CollectableTextComponent implements CollectableLocalizedComponent<Long>{

	private static final Logger LOG = Logger.getLogger(CollectableTextArea.class);
	private static final FocusForward FOCUS_FORWARD = new FocusForward();
	private static final FocusBackward FOCUS_BACKWORD = new FocusBackward();

	/**
	 * §postcondition this.isDetailsComponent()
	 */
	public NuclosCollectableLocalizedTextArea(CollectableEntityField clctef) {
		this(clctef, false);
	}

	public NuclosCollectableLocalizedTextArea(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, new LabeledTextAreaWithButton(new LabeledComponentSupport(), 
				clctef.isNullable(), clctef.getJavaClass(), clctef.getFormatInput(), bSearchable, clctef), bSearchable);
		
		( (LabeledTextAreaWithButton) getJComponent()).setLocalizedTextArea(this);
		
		setFillControlHorizontally(true);
		setupTextArea(clctef, getLabeledTextArea(), bSearchable); 
		
		overrideActionMap();
		getJTextArea().addComponentListener(new ComponentListener() {

			@Override
			public void componentShown(ComponentEvent e) {
			}

			@Override
			public void componentResized(ComponentEvent e) {
				fireHeightChanged(getJTextArea().getPreferredSize().height);
			}

			@Override
			public void componentMoved(ComponentEvent e) {
			}

			@Override
			public void componentHidden(ComponentEvent e) {
			}
		});
		
		 if(MetaProvider.getInstance().getEntityField(clctef.getUID()).getCalcFunction() != null)
	    	  ((LabeledTextAreaWithButton)getJComponent()).setButtonVisibility(false);
	}
	
   /**
	* sets up a contained textarea according to the type of this component.
	* @param ta
	*/
	private static void setupTextArea(CollectableEntityField clctef, LabeledTextAreaWithButton ta, boolean bSearchable) {
		// set the preferred width for numbers and dates:
		final Class<?> cls = clctef.getJavaClass();
		if(String.class.isAssignableFrom(cls)) {
			ta.setFormat(clctef);
		}
	}
	   
	   public boolean isButtonDisabled() {
		   return ((LabeledTextAreaWithButton)getJComponent()).isButtonsDisabled();
	   }
	   
	protected void updateView(CollectableField clctfValue) {
		   super.updateView(clctfValue);
		   ((LabeledTextAreaWithButton)getJComponent()).setDataLanguageMapAsDirty(
				   getDetailsComponentModel().isDataLanguageMapAsDirty());
	   }
	
	public LabeledTextAreaWithButton getLabeledTextArea() {
		return (LabeledTextAreaWithButton) this.getJComponent();
	}

	public JTextArea getJTextArea() {
		return (JTextArea) this.getJTextComponent();
	}

	@Override
	public JComponent getFocusableComponent() {
		return this.getJTextArea();
	}

	@Override
	public void setComparisonOperator(ComparisonOperator compop) {
		super.setComparisonOperator(compop);

		if (compop.getOperandCount() < 2) {
			this.runLocked(new Runnable() {
				@Override
                public void run() {
					try {
						getJTextComponent().setText(null);
					}
					catch (Exception e) {
						LOG.error("CollectableTextArea.setComparisionOperator: " + e, e);
					}						
				}
			});
		}
	}

	@Override
	public void setInsertable(boolean bInsertable) {
		// do nothing here
//		this.getJTextArea().setEditable(bInsertable);
	}

	@Override
	public void setRows(int iRows) {
		this.getJTextArea().setRows(iRows);
	}

	@Override
	public void setColumns(int iColumns) {
		this.getJTextArea().setColumns(iColumns);
	}

	@Override
	protected boolean selectAllOnGainFocus() {
		return true;
	}

	@Override
	public CollectableLocalizedComponentModel<Long> getDetailsComponentModel() {
		return (CollectableLocalizedComponentModel<Long>) getModel();
	}

	@Override
	public JPopupMenu newJPopupMenu() {
		JPopupMenu popupmenu = super.newJPopupMenu();
		if (null == popupmenu) {
			popupmenu = new JPopupMenu();
		}
		final JPopupMenu pm = popupmenu;
		if (!isSearchComponent() && 
			null != getLabeledTextArea().getTextModuleSettings()) {
			popupmenu.addPopupMenuListener(new TextModulePopupListener(popupmenu, getLabeledTextArea()));
		}
		return popupmenu;
	}
	
	protected <PK> List<TextModule> loadTextModules(final TextModuleSettings tms) {
		return TextModuleDelegate.getInstance().findTextModules(tms);
	}
	
	// For behaviour as table cell renderer
	 @Override
	   protected void setEnabledState(boolean flag) {
		   super.setEnabledState(flag);
		   this.getJTextArea().setEditable(flag);
		   if (!flag) {
			   ((LabeledTextAreaWithButton)getJComponent()).setButtonsDisabled();
		   }
	   }
	   
	 
	// Override the tab key
	private void overrideActionMap() {
		overrideActionMap(FOCUS_FORWARD, FOCUS_BACKWORD);
	}
	// Override the tab key from Subforms
	public final void overrideActionMap(Action forwardAction, Action backwardAction) {
		JTextArea component = getJTextArea();
		
		if (forwardAction == null)
			forwardAction = FOCUS_FORWARD;
		if (backwardAction == null)
			backwardAction = FOCUS_BACKWORD;
		
		// Add actions
		component.getActionMap().put(FOCUS_FORWARD.getValue(Action.NAME), forwardAction);
		component.getKeymap().addActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, KeyEvent.SHIFT_MASK), backwardAction);
	}


	/**
	 * This cell renderer is limited to 3 lines of text, when it is enabled.
	 * @return special cell renderer
	 */
	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		return new TextAreaCellRenderer(subform);
	}

	/**
	 *
	 *
	 */
	private class TextAreaCellRenderer implements TableCellRenderer, DynamicRowHeightSupport {

		final boolean subform;

		public TextAreaCellRenderer(boolean subform) {
			super();
			this.subform = subform;
		}

		@Override
        public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {

			NuclosCollectableLocalizedTextArea.this.setObjectValue(oValue);

			final JTextArea ta = NuclosCollectableLocalizedTextArea.this.getJTextArea();
			ta.setCaretPosition(0);

			final JScrollPane sp = (JScrollPane) NuclosCollectableLocalizedTextArea.this.getControlComponent();
			sp.setBorder(BorderFactory.createEmptyBorder());
			sp.setViewportBorder(BorderFactory.createEmptyBorder());

			// Calculate the correct line count (as JTextArea only counts \n characters)
			final int columnWidth = tbl.getColumnModel().getColumn(iColumn).getWidth()-tbl.getColumnModel().getColumnMargin();
			final int scrollBarWidth = sp.getVerticalScrollBar().getWidth();
			final boolean scrollBarVisible = sp.getVerticalScrollBar().isVisible();
			final int lineCount = NuclosCollectableLocalizedTextArea.this.getLineCount(columnWidth-(scrollBarVisible?scrollBarWidth:0));

			ta.setRows(lineCount);

			setBackgroundColor(ta, tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
			return NuclosCollectableLocalizedTextArea.this.getControlComponent();
		}

		@Override
		public int getHeight(Component cellRendererComponent) {
			return ((JScrollPane) cellRendererComponent).getViewport().getComponent(0).getPreferredSize().height+2;
		}
	}

	/**
	 * Calculate the real number of lines in the text area after it has wrapped the lines.
	 * The TextArea.getLineCount() just returns the number of '\n' characters in the text.
	 * Here we emulate the original word wrap algorithm of the text area.
	 *
	 * @return the line count
	 */
	private int getLineCount(int iWidth) {
		int result = 0;

		final JTextArea ta = this.getJTextArea();
		final String text = ta.getText();

		if (!StringUtils.looksEmpty(text)) {
			final Font font = ta.getFont();
			final FontRenderContext frc = new FontRenderContext(new AffineTransform(), true, false);

			final LineBreakMeasurer measurer = new LineBreakMeasurer(new AttributedString(text, font.getAttributes()).getIterator(), frc);

			while (measurer.getPosition() < text.length()) {
		         measurer.nextLayout(iWidth);
		         result++;
		    }
		}
		return Math.max(1, result);
	}

	@Override
	public void setProperty(String sName, Object oValue) {
		super.setProperty(sName, oValue);
		if ("font-family".equals(sName)) {
			Map<TextAttribute, Object> fontAttributes = new HashMap<TextAttribute, Object>(getJTextArea().getFont().getAttributes());
			fontAttributes.put(TextAttribute.FAMILY, oValue);
			final Font newFont = new Font(fontAttributes);
			getJTextArea().setFont(newFont);
		}
	}

	public void fireHeightChanged(int height) {
		for (DynamicRowHeightChangeListener drhcl : dynamicRowHeightChangeListener) {
			drhcl.heightChanged(height);
		}
	}

	public void addDynamicRowHeightChangeListener(DynamicRowHeightChangeListener drhcl) {
		dynamicRowHeightChangeListener.add(drhcl);
	}

	
	private final Collection<DynamicRowHeightChangeListener> dynamicRowHeightChangeListener = new ArrayList<DynamicRowHeightChangeListener>();


	private static class FocusForward extends AbstractAction {
		
		private FocusForward() {
			super("insert-tab");
		}
	
		@Override
	    public void actionPerformed(ActionEvent evt) {
			((Component) evt.getSource()).transferFocus();
		}
	}

	public static class FocusBackward extends AbstractAction {
	
		private FocusBackward() {
			super("Move Focus Backwards");
		}
	
		@Override
	    public void actionPerformed(ActionEvent evt) {
			Component c = (Component)((Component) evt.getSource()).getParent();
			if (c != null) //@see NUCLOS-1652
				c.dispatchEvent(new KeyEvent(c, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), KeyEvent.SHIFT_MASK, KeyEvent.VK_TAB));//c.transferFocusBackward();
			else
				((Component) evt.getSource()).transferFocusBackward();
		}
	}

	@Override
	public Integer getType() {
		return CollectableComponentTypes.TYPE_TEXTAREA;
	}
	
}
