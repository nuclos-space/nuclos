package org.nuclos.client.common;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * Created by Sebastian Debring on 9/24/2019.
 */
public class NuclosFileChooser extends JFileChooser {

	private final SpringLocaleDelegate localeDelegate = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);

	public NuclosFileChooser(final String sLastDir) {
		super(sLastDir);
	}

	@Override
	public void approveSelection() {
		File selected = getSelectedFile();
		boolean exists;

		try {
			exists = selected.exists();
		} catch (SecurityException e) {
			exists = false;
		}

		if (exists) {
			int val;
			val = JOptionPane.showConfirmDialog(this,
					localeDelegate.getMessage("general.overwrite.file", "general.overwrite.file", selected.getName()),
					localeDelegate.getMessage("general.overwrite.file.title", "general.overwrite.file.title"),
					JOptionPane.YES_NO_OPTION);
			if (val != JOptionPane.YES_OPTION) {
				return;
			}
		}

		try {
			if (selected.createNewFile()) {
				selected.delete();
			}
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(this,
					localeDelegate.getMessage("general.notwritable.file", "general.notwritable.file", selected.getName()),
					localeDelegate.getMessage("general.overwrite.file.title", "general.overwrite.file.title"),
					JOptionPane.WARNING_MESSAGE);
			return;
		} catch (SecurityException se) {
			//There is already file read/write access so at this point
			// only delete access is denied.  Just ignore it because in
			// most cases the file created in createNewFile gets
			// overwritten anyway.
		}
		File pFile = selected.getParentFile();
		if ((selected.exists() &&
				(!selected.isFile() || !selected.canWrite())) ||
				((pFile != null) &&
						(!pFile.exists() || (pFile.exists() && !pFile.canWrite())))) {
			JOptionPane.showMessageDialog(this,
					localeDelegate.getMessage("general.notwritable.file", "general.notwritable.file", selected.getName()),
					localeDelegate.getMessage("general.overwrite.file.title", "general.overwrite.file.title"),
					JOptionPane.WARNING_MESSAGE);
			return;
		}

		super.approveSelection();
	}
}
