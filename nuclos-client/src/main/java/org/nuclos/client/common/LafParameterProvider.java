//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.text.ParseException;
import java.util.Collection;

import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.mainframe.workspace.WorkspaceChooserController;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterMap;
import org.nuclos.common.LafParameterStorage;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;

//@Component("lafParameterProvider")
public class LafParameterProvider {
	
	private static final Logger LOG = Logger.getLogger(LafParameterProvider.class);

	private static LafParameterProvider INSTANCE;
	
	// Spring injection
	
	@Autowired
	private MetaProvider metaDataProvider;
	
	@Autowired
	private ParameterProvider parameterProvider;
	
	@Autowired
	private SecurityCache securityCache;
	
	// end of Spring injection
	
	private LafParameterProvider() {
		INSTANCE = this;
	}
	
	public static LafParameterProvider getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("Too early");
		}
		return INSTANCE;
	}
	
	public boolean isStorageAllowed(LafParameter<?,?> parameter, LafParameterStorage storage, UID entityUid) {
		switch (storage) {
		case SYSTEMPARAMETER:
			if (securityCache.isSuperUser()) {
				return parameter.isStoragePossible(storage);
			} else {
				return false;
			}
		case ENTITY:
			if (entityUid != null && securityCache.isSuperUser() && 
					!E.isNuclosEntity(entityUid)) {
				return parameter.isStoragePossible(storage);
			} else {
				return false;
			}
		case WORKSPACE:
			if ((WorkspaceChooserController.getInstance().getSelectedWorkspace().isAssigned()
					&& SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN))
				|| !WorkspaceChooserController.getInstance().getSelectedWorkspace().isAssigned()) {
				return parameter.isStoragePossible(storage);
			} else {
				return false;
			}
		default:
			return false;
		}
	}
	
	public <T,S> T getValue(LafParameter<T,S> parameter) {
		return getValue(parameter, null);
	}
	
	public <T,S> T getValue(LafParameter<T,S> parameter, UID entityUid) {
		return getValue(parameter, entityUid, false);
	}
	
	public <T,S> T getValue(LafParameter<T,S> parameter, UID entityUid, boolean noCache) {
		return getValue(parameter, entityUid, null, null, noCache);
	}
	
	<T,S> T getValue(LafParameter<T,S> parameter, UID entityUid, LafParameterStorage newValueStorage, T newValue) {
		return getValue(parameter, entityUid, newValueStorage, newValue, false);
	}
	
	<T,S> T getValue(LafParameter<T,S> parameter, UID entityUid, LafParameterStorage newValueStorage, T newValue, boolean noCache) {
		T result = null;
		
		// 1. Entity
		if (result == null && entityUid != null) {
			if (newValueStorage == LafParameterStorage.ENTITY) {
				result = newValue;
			} else {
				result = getValue(parameter, entityUid, LafParameterStorage.ENTITY, noCache);
			}
		}
		
		// 2. Workspace
		if (result == null) {
			if (newValueStorage == LafParameterStorage.WORKSPACE) {
				result = newValue;
			} else {
				final String p = WorkspaceChooserController.getInstance().getSelectedWorkspace().getWoDesc().getParameter(parameter.getName());
				result = convertFromStore(parameter, entityUid, p);
			}
		}
		
		// 3. Systemparameter
		if (result == null) {
			if (newValueStorage == LafParameterStorage.SYSTEMPARAMETER) {
				result = newValue;
			} else {
				result = getValue(parameter, entityUid, LafParameterStorage.SYSTEMPARAMETER, noCache);
			}
		}
		
		// 4. Nuclos default
		if (result == null) {
			result = parameter.getDefault();
		}
		
		assert result != null;
		return result;
	}
	
	private <T,S> T convertFromStore(LafParameter<T,S> parameter, UID entityUid, String p) {
		T result;
		try {
			result = parameter.convertFromStore((S) p);
		} catch (ParseException e) {
			LOG.warn(String.format("Can't convert LafParameter %s (entityUid=%s) value %s: %s",
					parameter.getName(), entityUid, p, e.toString()), e);
			result = parameter.getDefault();
		} catch (Exception e) {
			LOG.warn(String.format("Can't read LafParameter %s (entityUid=%s) value %s: %s",
					parameter.getName(), entityUid, p, e.toString()), e);
			result = parameter.getDefault();
		}
		return result;
	}
	
	public <T,S> T getValue(LafParameter<T,S> parameter, UID entityUid, LafParameterStorage storage) {
		return getValue(parameter, entityUid, storage, false);
	}
	
	public <T,S> T getValue(LafParameter<T,S> parameter, UID entityUid, LafParameterStorage storage, boolean noCache) {
		switch (storage) {
		case WORKSPACE:
			final String p1 = WorkspaceChooserController.getInstance().getSelectedWorkspace().getWoDesc().getParameter(parameter.getName());
			return convertFromStore(parameter, entityUid, p1);
		case ENTITY:
			if (entityUid == null) {
				throw new IllegalArgumentException("entityUid must not be null");
			}
			if (noCache) {
				UID entity = E.ENTITYLAFPARAMETER.getUID();
				Collection<MasterDataVO<UID>> colParamter = MasterDataDelegate.getInstance().getMasterData(entity, 
						SearchConditionUtils.and(
								SearchConditionUtils.newComparison(E.ENTITYLAFPARAMETER.parameter.getUID(), ComparisonOperator.EQUAL, parameter.getName()),
								SearchConditionUtils.newComparison(E.ENTITYLAFPARAMETER.entity.getUID(), ComparisonOperator.EQUAL, entityUid)));
				switch (colParamter.size()) {
				case 1:
					MasterDataVO<UID> mdParameter = colParamter.iterator().next();
					return convertFromStore(parameter, entityUid, mdParameter.getFieldValue(E.ENTITYLAFPARAMETER.value));
				default:
					return null;
				}
			}
			LafParameterMap<T,S> entityParameters = metaDataProvider.getLafParameters(entityUid);
			if (entityParameters != null) {
				return entityParameters.getValue(parameter);
			} else {
				return null;
			}
		case SYSTEMPARAMETER:
			if (noCache) {
				UID entity = E.PARAMETER.getUID();
				Collection<MasterDataVO<UID>> colParamter = MasterDataDelegate.getInstance().getMasterData(entity, SearchConditionUtils.newComparison(E.PARAMETER.name.getUID(),
						ComparisonOperator.EQUAL, parameter.getName()));
				switch (colParamter.size()) {
				case 1:
					MasterDataVO<UID> mdParameter = colParamter.iterator().next();
					return convertFromStore(parameter, entityUid, mdParameter.getFieldValue(E.PARAMETER.value));
				default:
					return null;
				}
			}
			final String p2 = parameterProvider.getValue(parameter.getName());
			return convertFromStore(parameter, entityUid, p2);
		default:
			return null;
		}
	}
	
	public <T,S> void setValue(LafParameter<T,S> parameter, UID entityUid, LafParameterStorage storage, S value) {
		if (!parameter.isStoragePossible(storage)) {
			throw new IllegalArgumentException(String.format("Storage %s for parameter %s is not possible", parameter, storage));
		}
		try {
			switch (storage) {
			case WORKSPACE:
				final WorkspaceDescription2 desc = WorkspaceChooserController.getInstance().getSelectedWorkspace().getWoDesc();
				desc.setParameter(parameter.getName(), (String) value);
				break;
			case ENTITY:
				if (entityUid == null) {
					throw new IllegalArgumentException("entityUid must not be null");
				}
				UID entity = E.ENTITYLAFPARAMETER.getUID();
				Collection<MasterDataVO<UID>> colParamter = MasterDataDelegate.getInstance().getMasterData(entity, 
						SearchConditionUtils.and(
								SearchConditionUtils.newComparison(E.ENTITYLAFPARAMETER.parameter.getUID(), ComparisonOperator.EQUAL, parameter.getName()),
								SearchConditionUtils.newComparison(E.ENTITYLAFPARAMETER.entity.getUID(), ComparisonOperator.EQUAL, entityUid)));
				switch (colParamter.size()) {
				case 1:
					MasterDataVO<UID> mdParameter = colParamter.iterator().next();
					if (value == null) {
						MasterDataDelegate.getInstance().remove(entity, mdParameter, null);
					} else {
						mdParameter.setFieldValue(E.ENTITYLAFPARAMETER.value, (String) value);
						MasterDataDelegate.getInstance().update(entity, mdParameter, null, null, false);
					}
					break;
				default:
					if (value != null) {
						EntityObjectVO<UID> eoParameter = new EntityObjectVO<UID>(E.ENTITYLAFPARAMETER);
						eoParameter.flagNew();
						eoParameter.setFieldValue(E.ENTITYLAFPARAMETER.parameter.getUID(), parameter.getName());
						eoParameter.setFieldValue(E.ENTITYLAFPARAMETER.value.getUID(), value);
						eoParameter.setFieldUid(E.ENTITYLAFPARAMETER.entity.getUID(), entityUid);
						mdParameter = DalSupportForMD.wrapEntityObjectVO(eoParameter);						
						MasterDataDelegate.getInstance().create(entity, mdParameter, null, null);
					}
				}
				break;
			case SYSTEMPARAMETER:
				entity = E.PARAMETER.getUID();
				colParamter = MasterDataDelegate.getInstance().getMasterData(entity, SearchConditionUtils.newComparison(E.PARAMETER.name.getUID(),
						ComparisonOperator.EQUAL, parameter.getName()));
				switch (colParamter.size()) {
				case 1:
					MasterDataVO<UID> mdParameter = colParamter.iterator().next();
					if (value == null) {
						MasterDataDelegate.getInstance().remove(entity, mdParameter, null);
					} else {
						mdParameter.setFieldValue(E.PARAMETER.value.getUID(), value);
						MasterDataDelegate.getInstance().update(entity, mdParameter, null, null, false);
					}
					break;
				default:
					if (value != null) {
						EntityObjectVO<UID> eoParameter = new EntityObjectVO<UID>(E.PARAMETER);
						eoParameter.flagNew();
						eoParameter.setFieldValue(E.PARAMETER.name.getUID(), parameter.getName());
						eoParameter.setFieldValue(E.PARAMETER.description.getUID(), parameter.getName());
						eoParameter.setFieldValue(E.PARAMETER.value.getUID(), value);
						mdParameter = DalSupportForMD.wrapEntityObjectVO(eoParameter);
						MasterDataDelegate.getInstance().create(entity, mdParameter, null, null);
					}
				}
				break;
			default:
				throw new NotImplementedException("setLafParameterValue for storage " + storage);
			}
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			Errors.getInstance().showExceptionDialog(null, ex);
		}
	}
	
}
