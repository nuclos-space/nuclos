//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.LafParameterMap;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class SystemMetaProvider implements IMetaProvider {

	private static SystemMetaProvider INSTANCE = new SystemMetaProvider();
	
	private final Map<UID, EntityMeta<?>> entities = new HashMap<UID, EntityMeta<?>>();
	private final Map<UID, FieldMeta<?>> fields = new HashMap<UID, FieldMeta<?>>();

	SystemMetaProvider(){
		for (EntityMeta meta : MetaDataDelegate.getInstance().getSystemMetaData()) {
			registerEntity(meta);
		}
		INSTANCE = this;
	}
	
	public static SystemMetaProvider getInstance() {
		return INSTANCE;
	}
	
	protected void registerEntity(EntityMeta<?> entityMeta) {
		entities.put(entityMeta.getUID(), entityMeta);
		for (FieldMeta<?> field : entityMeta.getFields()) {
			fields.put(field.getUID(), field);
		}
	}

	@Override
	public Collection<EntityMeta<?>> getAllEntities() {
		Collection<EntityMeta<?>> result = new ArrayList<EntityMeta<?>>(entities.values());
		return result;
	}
	
	/*@Override
	public EntityMeta<?> getEntityUnsafe(String entity) {
		for (EntityMeta<?> metaDataVO : entities.values()) {
			if (entity.equals(metaDataVO.getUID()))
				return metaDataVO;
		}
		return null;
	}*/

	@Override
	public boolean checkEntityField(final UID fieldUID) {
		return MetaProvider.getInstance().checkEntityField(fieldUID);
	}

	@Override
	public FieldMeta<?> getEntityField(UID fieldUID) {
		return fields.get(fieldUID);
	}

	@Override
	public boolean checkEntity(final UID entityUID) {
		return MetaProvider.getInstance().checkEntity(entityUID);
	}

	@Override
	public EntityMeta<?> getEntity(UID entity) {
		return MetaProvider.getInstance().getEntity(entity);
	}

	@Override
	public EntityMeta<?> getByTablename(String sTableName) {
		return MetaProvider.getInstance().getByTablename(sTableName);
	}
	
	@Override
	public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entityUID) {
		return MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUID);
	}
	
	@Override
	public boolean isNuclosEntity(UID entityUID) {
		return MetaProvider.getInstance().isNuclosEntity(entityUID);
	}
	
	@Override
	public List<UID> getEntities(UID nucletUID) {
		return CollectionUtils.transform(entities.values(), new Transformer<EntityMeta, UID>() {
			@Override
			public UID transform(EntityMeta i) {
				return i.getUID();
			}
		});
	}

	@Override
	public LafParameterMap getLafParameters(UID entityUID) {
		return null;
	}

	@Override
	public Map<UID, LafParameterMap> getAllLafParameters() {
		return null;
	}

	@Override
	public Collection<FieldMeta<?>> getAllReferencingFields(UID masterUID) {
		return MetaProvider.getInstance().getAllReferencingFields(masterUID);
	}

	@Override
	public FieldMeta<?> getCalcAttributeCustomization(UID fieldUID,
			String paramValues) {
		return null;
	}

	@Override
	public Collection<EntityMeta<?>> getAllLanguageEntities() {
		return null;
	}

	@Override
	public List<EntityObjectVO<UID>> getNuclets() {
		return new ArrayList<>();
	}

	@Override
	public Set<UID> getImplementingEntities(final UID genericEntityUID) {
		return Collections.emptySet();
	}


}
