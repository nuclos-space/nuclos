package org.nuclos.client.common;

import org.nuclos.common.collect.collectable.CollectableEntityField;

public class NuclosDropDown extends NuclosCollectableListOfValues {

	public NuclosDropDown(CollectableEntityField clctef, Boolean bSearchable) {
		super(clctef, bSearchable, Boolean.TRUE);
	}

}
