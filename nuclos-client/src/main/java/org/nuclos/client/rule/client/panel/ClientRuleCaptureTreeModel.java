package org.nuclos.client.rule.client.panel;

import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.nuclos.client.rule.client.explorer.ClientRuleSelectionNode;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleCaptureTreeModel implements TreeModel {

	private ClientRuleSelectionNode root;
	protected EventListenerList listenerList = new EventListenerList();

	public ClientRuleCaptureTreeModel(ClientRuleSelectionNode fieldToAddRule) {
		this.root = fieldToAddRule;
	}
	
	@Override
	public Object getRoot() {
		return this.root;
	}

	@Override
	public Object getChild(Object parent, int index) {
		TreeNode parentNode = (TreeNode) parent;
		return parentNode.getSubNodes() != null ? parentNode.getSubNodes().get(index): null;
	}

	@Override
	public int getChildCount(Object parent) {
		TreeNode parentNode = (TreeNode) parent;
		return parentNode.getSubNodes() != null ? parentNode.getSubNodes().size() : 0;
	}

	@Override
	public boolean isLeaf(Object node) {
		return getChildCount(node) == 0;
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		for (int i = 0; i < getChildCount(parent); i++) {
			if (getChild(parent, i).equals(child)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		listenerList.add(TreeModelListener.class, l);
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		listenerList.remove(TreeModelListener.class, l);
	}
	
}
