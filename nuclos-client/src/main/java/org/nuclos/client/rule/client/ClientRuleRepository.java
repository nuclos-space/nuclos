package org.nuclos.client.rule.client;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.rule.client.ClientRuleCacheRemote;
import org.springframework.beans.factory.InitializingBean;

public class ClientRuleRepository implements InitializingBean{

	private static ClientRuleRepository INSTANCE;

	private ClientRuleCacheRemote serverCache;
	
	// --- internal cache
	Map<UID, List<EntityObjectVO<UID>>> entitiesByNuclet;
	Map<UID, List<FieldMeta<?>>> fieldsByBo;
	Map<UID, Map<Pair<UID, UID>, List<FieldMeta<?>>>> referencesByBo;
	Map<UID, Map<UID, List<FieldMeta<?>>>> dependentsByBo;

	// --- internal cache
	
	private ClientRuleRepository() {}
	
	public static ClientRuleRepository getInstance() {
		if(INSTANCE == null) {
			INSTANCE = SpringApplicationContextHolder.getBean(ClientRuleRepository.class);
		}
		
		return INSTANCE;
	}

	public void setClientRuleCacheRemote(ClientRuleCacheRemote serverCache) {
		this.serverCache = serverCache;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		revalidateClient();
	}

	private void revalidateClient() {
		entitiesByNuclet = new HashMap<UID, List<EntityObjectVO<UID>>>();
		fieldsByBo = new HashMap<UID, List<FieldMeta<?>>>();
		referencesByBo = new HashMap<UID, Map<Pair<UID, UID>,List<FieldMeta<?>>>>();
		dependentsByBo = new HashMap<UID, Map<UID,List<FieldMeta<?>>>>();
	}
	
	public List<EntityObjectVO<UID>> getEntitiesByNuclet(UID nucletUid) {
		if (!entitiesByNuclet.containsKey(nucletUid)) {
			List<EntityObjectVO<UID>> results = serverCache.getBOsByNuclet(nucletUid);
			Collections.sort(results, new Comparator<EntityObjectVO<UID>>() {
				@Override
				public int compare(EntityObjectVO<UID> o1,
						EntityObjectVO<UID> o2) {
					return o1.getFieldValue(E.ENTITY.entity).compareTo(o2.getFieldValue(E.ENTITY.entity));
				}				
			});
			
			entitiesByNuclet.put(nucletUid, results);
		}
		
		return entitiesByNuclet.get(nucletUid);
	}
	
	public List<FieldMeta<?>> getFieldsByBO(UID boUid) {
		
		if (!fieldsByBo.containsKey(boUid)) {
			// all fields of entity
			fieldsByBo.put(boUid, serverCache.getFieldsByBO(boUid));
		}
		
		return fieldsByBo.get(boUid);
	}
	
	public Map<Pair<UID, UID>, List<FieldMeta<?>>> getReferencesByBO(UID boUid) {
		
		if (!referencesByBo.containsKey(boUid)) {
			// all fields of entity
			referencesByBo.put(boUid, serverCache.getReferencesByBO(boUid));
		}
		
		return referencesByBo.get(boUid);
	}

	public Map<UID, List<FieldMeta<?>>> getDependentsByBO(UID boUid) {
	
	if (!dependentsByBo.containsKey(boUid)) {
		// all fields of entity
		dependentsByBo.put(boUid, serverCache.getDependentsByBO(boUid));
	}
	
	return dependentsByBo.get(boUid);
}
}
