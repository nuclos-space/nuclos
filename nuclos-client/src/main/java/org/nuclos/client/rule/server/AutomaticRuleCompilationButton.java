package org.nuclos.client.rule.server;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;

import org.nuclos.common2.SpringLocaleDelegate;

class AutomaticRuleCompilationButton extends JButton {
	
	private final AutomaticRuleCompilationButtonFactory factory;
	
	AutomaticRuleCompilationButton(AutomaticRuleCompilationButtonFactory factory) {
		super();
		this.factory = factory;
		
		init();
	}
	
	private final void init() {
		final SpringLocaleDelegate sld = factory.getLocaleDelegate();
		
		setAction(new AbstractAction(getText(), getIcon()) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				toggle();
			}
		});
		setToolTipText(sld.getMessage("RuleCollectController.7", "RuleCollectController.7"));
	}
	
	@Override
	public Icon getIcon() {
		return factory.isAutomaticRuleCompilation() ? factory.getEnabledIcon() : factory.getDisabledIcon();
	}
	
	@Override
	public String getText() {
		if (factory == null) return "<not_initialized>";
		return factory.isAutomaticRuleCompilation() ? factory.getCompileString() : factory.getNocompileString();
	}
	
	public void toggle() {
		toggle(!factory.isAutomaticRuleCompilation());
	}
	
	public void toggle(boolean ruleCompilation) {
		factory.setAutomaticRuleCompilation(ruleCompilation);
	}
	
}
