package org.nuclos.client.rule.server.panel;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;

import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.rule.server.model.EventSupportEntityPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.common.UID;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.ProcessVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

public class EventSupportEntityPropertyPanel extends AbstractEventSupportPropertyPanel implements IReferenceHolder {
	
	private final List<EventListener> refs = new LinkedList<EventListener>();
		
	private final EventSupportEntityPropertiesTableModel model;
	private final Map<EventSupportActions, AbstractAction> actionMapping;
	
	private JComboBox cmbStatus;
	private JComboBox cmbProcess;
	
	private UID module;
	
	public EventSupportEntityPropertyPanel(Map<EventSupportActions, AbstractAction> pActionMapping) {
		this.model = new EventSupportEntityPropertiesTableModel(this);
		this.actionMapping = pActionMapping;
		
		setLayout(new BorderLayout());
		
		createPropertiesTable();
		
		final JTable tbl = getPropertyTable();
		
		final TableColumnModel colModel = tbl.getColumnModel();
	
		cmbProcess = new JComboBox();
		cmbStatus = new JComboBox();
		
		colModel.getColumn(1).setCellEditor(new DefaultCellEditor(cmbStatus));
		colModel.getColumn(2).setCellEditor(new DefaultCellEditor(cmbProcess));
		
		cmbProcess.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					EventSupportEventVO entryByRowIndex = (EventSupportEventVO)
							EventSupportEntityPropertyPanel.this.model.getEntryByRowIndex(tbl.getSelectedRow());
					
					String process = e.getItem() == null ? null : (String) e.getItem();
					
					List<String> vals = new ArrayList<String>();

					if (entryByRowIndex.getEntityUID() != null) {
						for (ProcessVO p : EventSupportRepository.getInstance().getProcessesByModuleUid(entryByRowIndex.getEntityUID())) {
							if (process == null) {
								for (StateVO state : EventSupportEntityPropertyPanel.this.model.getStatus(entryByRowIndex.getEntityUID(), null)) {
									vals.add(state.toString());
								}
								break;
							} else {
								if (p.getName().equals(process)) {
									for (StateVO state : EventSupportEntityPropertyPanel.this.model.getStatus(entryByRowIndex.getEntityUID(), p.getId())) {
										vals.add(state.toString());
									}
									break;
								}
							}
						}
					}
					vals.add(0,null);
					cmbStatus.setModel(new DefaultComboBoxModel(vals.toArray(new String[vals.size()])));
				}				
					
				}
			
			
		});
		
		tbl.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				 if (e.getValueIsAdjusting()) {
					EventSupportEventVO entryByRowIndex = (EventSupportEventVO)
							 EventSupportEntityPropertyPanel.this.model.getEntryByRowIndex(tbl.getSelectedRow());
					
					if (entryByRowIndex != null && entryByRowIndex.getEntityUID() != null) {
						List<String> vals = new ArrayList<String>();
						vals.add(null);
						for (StateVO state : 
							EventSupportEntityPropertyPanel.this.model.getStatus(entryByRowIndex.getEntityUID(), entryByRowIndex.getProcessUID())) {
							vals.add(state.toString());
						}
						cmbStatus.setModel(new DefaultComboBoxModel(vals.toArray(new String[vals.size()])));
						
						List<String> valsProcess = new ArrayList<String>();
						valsProcess.add(null);
						for(ProcessVO p : EventSupportRepository.getInstance().getProcessesByModuleUid(entryByRowIndex.getEntityUID())) {
							valsProcess.add(p.getName());
						}
						cmbProcess.setModel(new DefaultComboBoxModel(valsProcess.toArray(new String[valsProcess.size()])));						
					}
				 }				
			}
		});
	}
	
	@Override
	public void addRef(EventListener e) {
		refs.add(e);
	}
	
	public void setSelectedModule(UID mod) {
		this.module = mod;
	}
	
	public UID getModule() {
		return this.module;
	}
	
	@Override
	protected EventSupportPropertiesTableModel getPropertyModel() {
		return this.model;
	}

	@Override
	public Map<EventSupportActions, AbstractAction> getActionMapping() {
		return this.actionMapping;
	}

	@Override
	public ActionToolBar[] getActionToolbarMapping() {
		return new ActionToolBar[] {
				new ActionToolBar(EventSupportActions.ACTION_DELETE_EVENT, true),
				new ActionToolBar(EventSupportActions.ACTION_MOVE_UP_EVENT, true),
				new ActionToolBar(EventSupportActions.ACTION_MOVE_DOWN_EVENT, true) };
	}
	
	@Override
	protected String getPreferenceNodeName() {
		return EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_ENTITIES;
	}
}
