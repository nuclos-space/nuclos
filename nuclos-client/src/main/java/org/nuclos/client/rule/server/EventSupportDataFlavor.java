package org.nuclos.client.rule.server;

import java.awt.datatransfer.DataFlavor;

public class EventSupportDataFlavor extends DataFlavor {

	public static final EventSupportDataFlavor FLAVOR = new EventSupportDataFlavor();
	
	private static final String DATAFLAVOR = "EventSupportDataFlavor";
	
	private EventSupportDataFlavor() {
		super(EventSupportExplorerNodeTransferable.class, DATAFLAVOR);
	}

}
