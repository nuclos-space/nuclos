package org.nuclos.client.rule.client.explorer;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleRootNode implements TreeNode {

	@Override
	public Object getId() {
		return null;
	}

	@Override
	public Object getRootId() {
		return null;
	}

	@Override
	public UID getNodeId() {
		return null;
	}

	@Override
	public UID getEntityUID() {
		return null;
	}

	@Override
	public String getLabel() {
		return "Regelzuweisung";
	}

	@Override
	public String getDescription() {
		return "Regelzuweisung";
	}

	@Override
	public String getIdentifier() {
		return null;
	}

	@Override
	public List<ClientRuleNucletNode> getSubNodes() {
		List<ClientRuleNucletNode> retVal = new ArrayList<ClientRuleNucletNode>();
		
		// Default Nuclet (not assigned entities)
		EntityObjectVO<UID> entityObject = new EntityObjectVO<UID>(E.NUCLET);
		
		entityObject.setPrimaryKey(UID.UID_NULL);
		entityObject.setFieldValue(E.NUCLET.name, "Default");
		entityObject.setFieldValue(E.NUCLET.description, "Default");
		
		retVal.add(new ClientRuleNucletNode(entityObject));
		
		for (MasterDataVO mdvo : MasterDataCache.getInstance().get(E.NUCLET.getUID())) {
			EntityObjectVO<UID> eoVO = ((MasterDataVO<UID>) mdvo).getEntityObject();
			retVal.add(new ClientRuleNucletNode(eoVO));
		}
		
		return retVal;
	}

	@Override
	public Boolean hasSubNodes() {
		return Boolean.TRUE;
	}

	@Override
	public void removeSubNodes() {	}

	@Override
	@Deprecated
	public void refresh() {}

	@Override
	public boolean implementsNewRefreshMethod() {
		return true;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		return this;
	}

	@Override
	public boolean needsParent() {
		return false;
	}

}
