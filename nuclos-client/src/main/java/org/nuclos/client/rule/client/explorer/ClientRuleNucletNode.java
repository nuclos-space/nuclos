package org.nuclos.client.rule.client.explorer;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.client.rule.client.ClientRuleRepository;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleNucletNode implements TreeNode {

	private List<ClientRuleEntityNode> subnodes;
	private EntityObjectVO<UID> nuclet;
	
	public ClientRuleNucletNode(EntityObjectVO<UID> nuclet) {
		this.nuclet = nuclet;
	}
	
	@Override
	public Object getId() {
		return nuclet.getPrimaryKey();
	}

	@Override
	public Object getRootId() {
		return null;
	}

	@Override
	public UID getNodeId() {
		return nuclet.getPrimaryKey();
	}

	@Override
	public UID getEntityUID() {
		return E.NUCLET.getUID();
	}

	@Override
	public String getLabel() {
		return nuclet.getFieldValue(E.NUCLET.name);
	}

	@Override
	public String getDescription() {
		return nuclet.getFieldValue(E.NUCLET.description);
	}

	@Override
	public String getIdentifier() {
		return nuclet.getPrimaryKey().getString();
	}

	@Override
	public List<ClientRuleEntityNode> getSubNodes() {		
		if (this.subnodes == null) {
			this.subnodes = getAllEntities();
		}
		
		return this.subnodes;
	}
	
	private List<ClientRuleEntityNode> getAllEntities() {
		List<ClientRuleEntityNode> retVal =
				 new ArrayList<ClientRuleEntityNode>();
		
		for (EntityObjectVO<UID> eoVo : ClientRuleRepository.getInstance().getEntitiesByNuclet(nuclet.getPrimaryKey())) {
			retVal.add(new ClientRuleEntityNode(eoVo));
		}
		
		return retVal;
	}

	@Override
	public Boolean hasSubNodes() {
		List<ClientRuleEntityNode> subNodes2 = getSubNodes();
		return subNodes2 != null ? subNodes2.size() > 0 : Boolean.FALSE;
	}

	@Override
	public void removeSubNodes() {
		if (this.subnodes != null)
			this.subnodes.clear();
	}

	@Override
	@Deprecated
	public void refresh() {}

	
	@Override
	public boolean implementsNewRefreshMethod() {
		return true;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		return this;
	}

	@Override
	public boolean needsParent() {
		return false;
	}

}
