//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.maintenance;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeRemote;

public class MaintenanceUtils {

	public static boolean isNucletIntegationPossible() {
		return isImportPossible();
	}

	public static boolean isNucletAssignmentPossible() {
		return isImportPossible();
	}

	public static boolean isImportPossible() {
		final MaintenanceFacadeRemote maintenanceFacadeRemote = SpringApplicationContextHolder.getBean(MaintenanceFacadeRemote.class);
		final String maintenanceMode = maintenanceFacadeRemote.getMaintenanceMode();
		final boolean productionEnvironment = SpringApplicationContextHolder.getBean(MaintenanceFacadeRemote.class).isProductionEnvironment();
		if (!MaintenanceConstants.MAINTENANCE_MODE_ON.equals(maintenanceMode) &&
				productionEnvironment) {
			return false;
		} else {
			return true;
		}
	}

}
