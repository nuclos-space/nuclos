//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.customcomp.resplan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.nuclos.common.customcomp.resplan.PlanElementLocaleVO;
import org.nuclos.common.customcomp.resplan.ResPlanResourceVO;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;

//Version
@SuppressWarnings("serial")
public class EntryTranslationTableModel extends AbstractTranslationTableModel {
	protected List<PlanElementLocaleVO> lstRows = new ArrayList<PlanElementLocaleVO>();

	protected static String[] columns = {ResPlanResourceVO.LOCALE,
		ResPlanResourceVO.BOOKING_L,
		ResPlanResourceVO.BOOKING_TT};

	public EntryTranslationTableModel(Collection<LocaleInfo> locales) {
		super(locales);
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	public void setRows(List<PlanElementLocaleVO> rows) {
		lstRows = rows;
		this.fireTableDataChanged();
	}

	public List<PlanElementLocaleVO> getRows() {
		return lstRows;
	}

	@Override
	public int getRowCount() {
		return lstRows.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return localeLabels.get(lstRows.get(rowIndex).getLocaleId());
			case 1:
				return lstRows.get(rowIndex).getBookingLabel();
			case 2:
				return lstRows.get(rowIndex).getBookingTooltip();
			default:
				break;
		}
		return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (aValue != null && !(aValue instanceof String)) {
			throw new IllegalArgumentException("aValue is not a java.lang.String");
		}
		String value = (String) aValue;

		switch(columnIndex) {
		case 1:
			lstRows.get(rowIndex).setBookingLabel(value);
			break;
		case 2:
			lstRows.get(rowIndex).setBookingTooltip(value);
			break;
		default:
				break;
		}
	}
	
	@Override
	public String getColumnName(int column) {
		return SpringLocaleDelegate.getInstance().getText(
				"nuclos.resplan.l10n.labels." + columns[column]);
	}
}
