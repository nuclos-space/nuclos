package org.nuclos.client.command;

public abstract class ExecutionResultAdapter implements ExecutionResultListener {
	
	public void done() {}
	
	public void error(Exception ex) {}
	
}

