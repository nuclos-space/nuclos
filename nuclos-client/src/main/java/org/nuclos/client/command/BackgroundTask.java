package org.nuclos.client.command;

import org.nuclos.common2.exception.CommonBusinessException;

/**
 * A background task is similar to a CommonClientWorker but is only responsible for performing task-related actions.
 * In contrast to the CommonClientWorker(Adapter), the BackgroundTask should not perform any GUI locking or error
 * handling. These will be performed by the particular executor in a uniform way.
 */
public abstract class BackgroundTask {

	public void init() throws CommonBusinessException {
	}

	public abstract void doInBackground() throws CommonBusinessException;

	public void done() throws CommonBusinessException {
	}
}

