//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.autonumber;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.ListSelectionModel;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.TableModel;

import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.dnd.IReorderable;
import org.nuclos.common.DefaultComponentTypes;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;

/**
 * AutonumberUiUtils
 * 
 * static autonumber helpers
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class AutonumberUiUtils {
	
	private AutonumberUiUtils() {
		// Never invoked.
	}

	/**
	 * Is sorted by autonumber field
	 * 
	 * @param detailsSubFormController	{@link MasterDataSubFormController}
	 * @return true if sorted by autonumber field
	 */
	public static boolean isSortedByAutonumber(final DetailsSubFormController<?,?> detailsSubFormController) { 
		final List<? extends SortKey> sortKeys = detailsSubFormController.getCollectableTableModel().getSortKeys();
		boolean result = false;
		// check for any sorting keys
		if (sortKeys.iterator().hasNext()) {

			SubFormTableModel tblModel = (SubFormTableModel) detailsSubFormController.getJTable().getModel();
			final SortKey sortKey = sortKeys.iterator().next();
			final UID fieldUid = tblModel.getColumnFieldUid(sortKey.getColumn());

			// evalute field type
			final FieldMeta<?> metaField = MetaProvider.getInstance().getEntityField(fieldUid);
			if (isAutonumber(metaField.getDefaultComponentType())) {
				// validate sorting order
				if (SortOrder.DESCENDING.equals(sortKey.getSortOrder()) || SortOrder.ASCENDING.equals(sortKey.getSortOrder())) {
					result = true;
				}
			}
		}
		return result;
	}
	
	/**
	 * Is autonumber
	 * 
	 * @param defaultComponentType
	 * @return
	 */
	public static boolean isAutonumber(final String defaultComponentType) {
		return DefaultComponentTypes.AUTONUMBER.equals(defaultComponentType);
		
	}
	
	/**
	 * Find autonumbercolumn in subform
	 * 
	 * @param sfTable	subform table
	 * @return
	 */
	public static <PK> int findColumnAutoNumber(final SubFormTable sfTable) {
		int iColumnAutonumber = -1;
		final SortableCollectableTableModel<PK,CollectableEntityObject<PK>> cltTblModel = 
				(SortableCollectableTableModel<PK,CollectableEntityObject<PK>>) sfTable.getModel();
		for (int iColumn = 0; iColumn < cltTblModel.getColumnCount(); ++iColumn) {
			CollectableEntityField clctField = cltTblModel.getCollectableEntityField(iColumn);
			if (AutonumberUiUtils.isAutonumber(clctField.getDefaultComponentType())) {
				iColumnAutonumber = iColumn;
				break;
			}
		}
		return iColumnAutonumber;
		
	}
	
	/**
	 * Find autonumberfield in entity
	 * 
	 * @param sEntity	entity name
	 * @return
	 */
	public static UID findAutoNumberField(final UID sEntity) {
		final Map<UID, FieldMeta<?>> mpEoFieldMetaVO = MetaProvider.getInstance().getAllEntityFieldsByEntity(sEntity);
		
		UID field = null;
		for (final Entry<UID, FieldMeta<?>> entry: mpEoFieldMetaVO.entrySet()) {
			if (DefaultComponentTypes.AUTONUMBER.equals(entry.getValue().getDefaultComponentType())) {
				field = entry.getKey();
				break;
			}

		}
		return field;
	}
	
	/**
	 * Fix Subform numbers
	 * @param sfTable	subform table
	 */
	public static <PK> void fixSubFormOrdering(final SubFormTable sfTable) {
		final SortableCollectableTableModel<PK,CollectableEntityObject<PK>> cltTblModel = 
				(SortableCollectableTableModel<PK,CollectableEntityObject<PK>>) sfTable.getModel();
		int iColumnAutonumber = findColumnAutoNumber(sfTable);
		int iRowCount = cltTblModel.getRowCount();
		final List<? extends SortKey> sortKeys = cltTblModel.getSortKeys();
		if (iColumnAutonumber > -1 
		    && iRowCount > 0 
		    && sortKeys.iterator().hasNext()) {
			
			if (SortOrder.DESCENDING.equals(cltTblModel.getSortKeys().get(0).getSortOrder())) {
				// FIXME improve this dummy algorithm
				for (int iRow = 0; iRow < iRowCount; ++iRow) {
					sfTable.getModel().setValueAt(new CollectableValueField(iRowCount - iRow), iRow, iColumnAutonumber);
				}
			} else {
				// FIXME improve this dummy algorithm
				for (int iRow = 0; iRow < iRowCount; ++iRow) {
					sfTable.getModel().setValueAt(new CollectableValueField(iRow + 1), iRow, iColumnAutonumber);
				}
			}
		}
	}
	
	/**
	 * move rows
	 * 
	 * @param iRowSrc				source row
	 * @param iRowTgt				target row
	 * @param arrRowsSelected		selected rows
	 * @param sfTable				subform table
	 * @return
	 */
	public static boolean moveRows(final int iRowSrc, final int iRowTgt, final int[] arrRowsSelected, final SubFormTable sfTable) {
		assert sfTable != null;
		if (iRowSrc < 0 || iRowTgt < 0 || arrRowsSelected.length < 1) {
			return false;
		}
		
		int iRowDropped = iRowTgt;
		boolean moveForward = (iRowDropped > iRowSrc);
		final List<Integer> lstMovedRows = new ArrayList<Integer>();
		int distance = iRowDropped - iRowSrc;
		if (moveForward) {
			// top down
			for (int i = arrRowsSelected.length ; i > 0; i--) {
				int iRowCurrentPosition = arrRowsSelected[i - 1];
				int iRowMovedPosition = iRowCurrentPosition + distance;

				// check outgoing indexes > list size
				if(iRowMovedPosition < sfTable.getRowCount()) {
					final TableModel model = sfTable.getModel();
					((IReorderable) model).reorder(sfTable.convertRowIndexToModel(iRowCurrentPosition), sfTable.convertRowIndexToModel(iRowMovedPosition));
					lstMovedRows.add(new Integer(iRowMovedPosition));
				} else {
					return false;
				}
			}	
		} else {
			// bottom up
			for (int i = 0 ; i < arrRowsSelected.length ; i++) {
				int iRowCurrentPosition = arrRowsSelected[i];
				int iRowMovedPosition = iRowCurrentPosition + distance;

				// check outgoing indexes > list size
				if(iRowMovedPosition >= 0) {
					final TableModel model = sfTable.getModel();
					((IReorderable) model).reorder(sfTable.convertRowIndexToModel(iRowCurrentPosition), sfTable.convertRowIndexToModel(iRowMovedPosition));
					lstMovedRows.add(new Integer(iRowMovedPosition));
				} else {
					return false;
				}
			}
		}
		// select moved rows
		final ListSelectionModel tsm = sfTable.getSelectionModel();
		tsm.clearSelection();
		for (final Integer iRow: lstMovedRows) {
			tsm.addSelectionInterval(iRow.intValue(), iRow.intValue());
			
		}
		return true;
	}
}
