//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.MainFrameTabController;
import org.nuclos.client.ui.SwingAppender;

public class ScriptLoggerController extends MainFrameTabController {

	private final static Logger LOG = (Logger) LogManager.getLogger(ScriptEvaluator.class);

	private final ScriptLoggerView view = new ScriptLoggerView();

	private final Appender appender = new SwingAppender(getView());
			
	public ScriptLoggerController(MainFrameTab parent) {
		super(parent);

		parent.addMainFrameTabListener(new MainFrameTabAdapter() {
			@Override
			public void tabClosing(MainFrameTab tab, ResultListener<Boolean> rl) {
				LOG.removeAppender(appender);
				rl.done(true);
			}
		});

		LOG.addAppender(appender);
	}

	public ScriptLoggerView getView() {
		return view;
	}
}
