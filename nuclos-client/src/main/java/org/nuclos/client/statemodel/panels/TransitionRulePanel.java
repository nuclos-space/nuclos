package org.nuclos.client.statemodel.panels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.rule.server.panel.EventSupportSelectionExceptionListener;
import org.nuclos.client.rule.server.panel.EventSupportSelectionPanel;
import org.nuclos.client.rule.server.panel.EventSupportTypeConverter;
import org.nuclos.client.statemodel.StateModelEditor;
import org.nuclos.client.statemodel.models.NameAndDescriptionModel;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.E;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;

public class TransitionRulePanel extends JPanel {

	private EventSupportSelectionPanel<EventSupportTransitionVO> pnlTransitions;
	private EventSupportSelectionPanel<EventSupportTransitionVO> pnlTransitionsFinal;
	
	private JCheckBox jckAutoStateChangeTrans;
	private JCheckBox jckDefaultStateChangeTrans;
	private JCheckBox jckNonstopStateChangeTrans;
	private StateModelEditor parent;
	private List<TransitionDataChangeListener> listener = new ArrayList<TransitionDataChangeListener>();

	private final NameAndDescriptionModel model = new NameAndDescriptionModel();

	public NameAndDescriptionModel getModel() {
		return model;
	}

	public TransitionRulePanel(StateModelEditor parent) {
		super(new BorderLayout());
		
		this.parent = parent;
		
		// Common properties
		this.add(createCommonProperties(), BorderLayout.NORTH);
		
		// Rules
		this.add(createRulesPanel(), BorderLayout.CENTER);
	}

	private Component createCommonProperties() {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		JPanel commonProperiesPanel = new JPanel(new BorderLayout());
		
		JPanel pnlAutomaticChange = new JPanel(new BorderLayout());
		pnlAutomaticChange.setBorder(BorderFactory.createEmptyBorder(10,5,10,0));
		
		jckAutoStateChangeTrans = new JCheckBox(localeDelegate.getMessage("nuclos.entityfield.statetransition.automatic.label", "Automatischer Statuswechsel", null));
		jckAutoStateChangeTrans.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e != null && e.getSource() != null && e.getSource() instanceof JCheckBox) {
					JCheckBox jbx = (JCheckBox) e.getSource();
					// Only commit real changes
					fireTransitionAutomaticDataChange(jbx.isSelected());
				}
			}
		});
		pnlAutomaticChange.add(jckAutoStateChangeTrans, BorderLayout.NORTH);

		jckDefaultStateChangeTrans = new JCheckBox(localeDelegate.getMessage("nuclos.entityfield.statetransition.default.label", "Standard Statuswechsel", null));
		jckDefaultStateChangeTrans.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e != null && e.getSource() != null && e.getSource() instanceof JCheckBox) {
					JCheckBox jbx = (JCheckBox) e.getSource();
					fireTransitionDefaultDataChange(jbx.isSelected());
				}
			}
		});
		pnlAutomaticChange.add(jckDefaultStateChangeTrans, BorderLayout.CENTER);
		
		jckNonstopStateChangeTrans = new JCheckBox(localeDelegate.getMessage("nuclos.entityfield.statetransition.nonstop.label", "Durchgehender Statuswechsel", null));
		jckNonstopStateChangeTrans.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e != null && e.getSource() != null && e.getSource() instanceof JCheckBox) {
					JCheckBox jbx = (JCheckBox) e.getSource();
					fireTransitionNonstopDataChange(jbx.isSelected());
				}
			}
		});

		pnlAutomaticChange.add(jckNonstopStateChangeTrans, BorderLayout.SOUTH);

		commonProperiesPanel.add(pnlAutomaticChange, BorderLayout.NORTH);

		// The following block is more or less copied from StatePropertiesPanel. This is done deliberantly as very soon
		// it's refactored anyway with multi-language support.

		final JPanel pnlStateProperties = new JPanel(new GridBagLayout());
		pnlStateProperties.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		final JLabel labNameDE = new JLabel(localeDelegate.getMessage("StatePropertiesPanel.7","Name") + " (" +
				localeDelegate.getMessage("StatePropertiesPanel.21","DE") + ")");
		final JTextField tfNameDE = new JTextField();
		labNameDE.setAlignmentY((float) 0.0);
		labNameDE.setHorizontalAlignment(SwingConstants.LEADING);
		labNameDE.setHorizontalTextPosition(SwingConstants.TRAILING);
		labNameDE.setLabelFor(tfNameDE);
		labNameDE.setVerticalAlignment(SwingConstants.CENTER);
		labNameDE.setVerticalTextPosition(SwingConstants.CENTER);
		tfNameDE.setAlignmentX((float) 0.0);
		tfNameDE.setAlignmentY((float) 0.0);
		tfNameDE.setPreferredSize(new Dimension(100, 21));
		tfNameDE.setDocument(model.docNameDE);
		tfNameDE.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));

		final JLabel labNameEN = new JLabel(localeDelegate.getMessage("StatePropertiesPanel.7","Name") + " (EN)");
		final JTextField tfNameEN = new JTextField();
		labNameEN.setAlignmentY((float) 0.0);
		labNameEN.setHorizontalAlignment(SwingConstants.LEADING);
		labNameEN.setHorizontalTextPosition(SwingConstants.TRAILING);
		labNameEN.setLabelFor(tfNameEN);
		labNameEN.setVerticalAlignment(SwingConstants.CENTER);
		labNameEN.setVerticalTextPosition(SwingConstants.CENTER);

		tfNameEN.setAlignmentX((float) 0.0);
		tfNameEN.setAlignmentY((float) 0.0);
		tfNameEN.setPreferredSize(new Dimension(100, 21));
		tfNameEN.setDocument(model.docNameEN);
		tfNameEN.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));


		final JLabel labDescriptionDE = new JLabel(localeDelegate.getMessage("StatePropertiesPanel.5","Hinweistext") +
				" (" +
				localeDelegate.getMessage("StatePropertiesPanel.21","DE") + ")");
		final JTextArea taDescriptionDE = new JTextArea();
		labDescriptionDE.setAlignmentY((float) 0.0);
		labDescriptionDE.setHorizontalAlignment(SwingConstants.LEADING);
		labDescriptionDE.setHorizontalTextPosition(SwingConstants.TRAILING);
		labDescriptionDE.setIconTextGap(4);
		labDescriptionDE.setLabelFor(taDescriptionDE);
		labDescriptionDE.setVerticalAlignment(SwingConstants.TOP);
		labDescriptionDE.setVerticalTextPosition(SwingConstants.TOP);

		taDescriptionDE.setAlignmentX((float) 0.0);
		taDescriptionDE.setAlignmentY((float) 0.0);
		taDescriptionDE.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 4));
		taDescriptionDE.setText("");
		taDescriptionDE.setDocument(model.docDescriptionDE);
		taDescriptionDE.setFont(tfNameDE.getFont());
		taDescriptionDE.setLineWrap(true);
		taDescriptionDE.setEditable(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));

		final JLabel labDescriptionEN = new JLabel(localeDelegate.getMessage("StatePropertiesPanel.5","Hinweistext")+ " (EN)");
		final JTextArea taDescriptionEN = new JTextArea();
		labDescriptionEN.setAlignmentY((float) 0.0);
		labDescriptionEN.setHorizontalAlignment(SwingConstants.LEADING);
		labDescriptionEN.setHorizontalTextPosition(SwingConstants.TRAILING);
		labDescriptionEN.setIconTextGap(4);
		labDescriptionEN.setLabelFor(taDescriptionEN);
		labDescriptionEN.setVerticalAlignment(SwingConstants.TOP);
		labDescriptionEN.setVerticalTextPosition(SwingConstants.TOP);

		taDescriptionEN.setAlignmentX((float) 0.0);
		taDescriptionEN.setAlignmentY((float) 0.0);
		taDescriptionEN.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 4));
		taDescriptionEN.setText("");
		taDescriptionEN.setDocument(model.docDescriptionEN);
		taDescriptionEN.setFont(tfNameDE.getFont());
		taDescriptionEN.setLineWrap(true);
		taDescriptionEN.setEditable(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));

		final JScrollPane scrlpnDE = new JScrollPane(taDescriptionDE);
		scrlpnDE.setAutoscrolls(true);
		scrlpnDE.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		final JScrollPane scrlpnEN = new JScrollPane(taDescriptionEN);
		scrlpnEN.setAutoscrolls(true);
		scrlpnEN.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);


		pnlStateProperties.add(labNameDE,
				new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(2, 0, 0, 5), 0, 0));
		pnlStateProperties.add(tfNameDE,
				new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
						new Insets(2, 5, 0, 0), 0, 0));

		pnlStateProperties.add(labNameEN,
				new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(2, 0, 0, 5), 0, 0));
		pnlStateProperties.add(tfNameEN,
				new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
						new Insets(2, 5, 0, 0), 0, 0));

		pnlStateProperties.add(labDescriptionDE,
				new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
						new Insets(2, 0, 0, 5), 0, 0));
		pnlStateProperties.add(scrlpnDE,
				new GridBagConstraints(1, 3, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						new Insets(2, 5, 0, 0), 0, 0));
		pnlStateProperties.add(labDescriptionEN,
				new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
						new Insets(2, 0, 0, 5), 0, 0));
		pnlStateProperties.add(scrlpnEN,
				new GridBagConstraints(1, 4, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						new Insets(2, 5, 0, 0), 0, 0));

		commonProperiesPanel.add(pnlStateProperties, BorderLayout.SOUTH);

		return commonProperiesPanel;
	}

	public void setAutomatic(boolean bAutomatic) {
		jckAutoStateChangeTrans.setSelected(bAutomatic);
	}

	public void setDefault(boolean bDefault) {
		this.jckDefaultStateChangeTrans.setSelected(bDefault);
	}
	
	public void setNonstop(boolean bNonstop) {
		this.jckNonstopStateChangeTrans.setSelected(bNonstop);
	}

	private JComponent createRulesPanel() {
		
		// Exception Listener
		EventSupportSelectionExceptionListener listener = new EventSupportSelectionExceptionListener() {
				@Override
				public void fireException(Exception firedException) {
					Errors.getInstance().showExceptionDialog(TransitionRulePanel.this, firedException.getMessage(), firedException);
				}
		};
		
		// StateChangeRules
		pnlTransitions = new EventSupportSelectionPanel<EventSupportTransitionVO>(
				parent.getStateModelCollectController(), "Regeln vor dem Statuswechsel");
		pnlTransitions.addEventSupportSelectionExceptionListener(listener);
		pnlTransitions.setEventSupportTypeConverter(new EventSupportTypeConverter<EventSupportTransitionVO>() {
			@Override
			public EventSupportTransitionVO convertData(EventSupportSourceVO esVO) {				
				return new EventSupportTransitionVO(esVO.getClassname(), StateChangeRule.class.getCanonicalName(),
					parent.getSelectedTransition(), pnlTransitions.getModel().getRowCount() + 1);
			}

			@Override
			public String getRuleTypeAsString() {
				return StateChangeRule.class.getCanonicalName();
			}
		});
		
		// StateChangeFinalRules		
		pnlTransitionsFinal = new EventSupportSelectionPanel<EventSupportTransitionVO>(
				parent.getStateModelCollectController(), "Regeln nach dem Statuswechsel");
		pnlTransitionsFinal.addEventSupportSelectionExceptionListener(listener);
		pnlTransitionsFinal.setEventSupportTypeConverter(new EventSupportTypeConverter<EventSupportTransitionVO>() {

			@Override
			public String getRuleTypeAsString() {
				return StateChangeFinalRule.class.getCanonicalName();
			}
			
			@Override
			public EventSupportTransitionVO convertData(EventSupportSourceVO esVO) {				
				return new EventSupportTransitionVO(esVO.getClassname(), StateChangeFinalRule.class.getCanonicalName(),
					parent.getSelectedTransition(), pnlTransitionsFinal.getModel().getRowCount() + 1);
			}
		});
		
		JPanel p = new JPanel(new GridLayout(2,1));
		p.add(pnlTransitions);
		p.add(pnlTransitionsFinal);
		return p;
	}

	public EventSupportSelectionPanel<EventSupportTransitionVO> getStateChangeRulePanel() {
		return this.pnlTransitions;
	}
	public EventSupportSelectionPanel<EventSupportTransitionVO> getStateChangeFinalRulePanel() {
		return this.pnlTransitionsFinal;
	}

	public void addTransitionRuleDataChangeListener(TransitionDataChangeListener listener) {
		// Listener for Automatic and default transition checkboxes
		this.listener.add(listener);
		// Listener for tables
		pnlTransitions.addEventSupportDataChangeListener(listener);
		pnlTransitionsFinal.addEventSupportDataChangeListener(listener);
	}
	
	public void fireTransitionAutomaticDataChange(boolean isAutomatic) {
		for (TransitionDataChangeListener tdcl : this.listener) {
			try {
				tdcl.transitionAutomaticDataChange(isAutomatic);				
			} catch (Exception e) {}
		}
	}
	
	public void fireTransitionDefaultDataChange(boolean isDefault) {
		for (TransitionDataChangeListener tdcl : this.listener) {
			try {
				tdcl.transitionDefaultDataChange(isDefault);
			} catch (Exception e) {}
		}
	}
	
	public void fireTransitionNonstopDataChange(boolean isNonstop) {
		for (TransitionDataChangeListener tdcl : this.listener) {
			try {
				tdcl.transitionNonstopDataChange(isNonstop);
			} catch (Exception e) {}
		}
	}
	
	public void setButtonWritable(boolean isWriteAllowed) {
		this.pnlTransitions.setButtonWritable(isWriteAllowed);
		this.pnlTransitionsFinal.setButtonWritable(isWriteAllowed);
	}
}
