package org.nuclos.client.statemodel.models;

import java.io.Serializable;
import java.util.Locale;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;

/**
 * Created by Oliver Brausch on 30.01.19.
 */
public class NameAndDescriptionModel implements Serializable {

	private static final Logger LOG = Logger.getLogger(NameAndDescriptionModel.class);

	public Document docNameDE = new PlainDocument();
	public Document docNameEN = new PlainDocument();
	public Document docDescriptionDE = new PlainDocument();
	public Document docDescriptionEN = new PlainDocument();

	public String getName(Locale locale) {
		String sResult = "";
		try {
			if (Locale.GERMAN.equals(locale)) {
				sResult = docNameDE.getText(0, docNameDE.getLength());
			} else {
				sResult = docNameEN.getText(0, docNameEN.getLength());
			}
		}
		catch (BadLocationException e) {
			// this should never happens
			LOG.warn("getName (" + locale + ") failed: " + e, e);
		}
		return sResult;
	}

	public void setName(Locale locale, String sName) {
		try {

			if (Locale.GERMAN.equals(locale)) {
				this.docNameDE.remove(0, docNameDE.getLength());
				this.docNameDE.insertString(0, sName, null);
			} else {
				this.docNameEN.remove(0, docNameEN.getLength());
				this.docNameEN.insertString(0, sName, null);
			}
		}
		catch (BadLocationException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	public String getDescription(Locale locale) {
		final String result;
		try {
			if (Locale.GERMAN.equals(locale)) {
				result = docDescriptionDE.getText(0, docDescriptionDE.getLength());
			} else {
				result = docDescriptionEN.getText(0, docDescriptionEN.getLength());
			}
		}
		catch (BadLocationException ex) {
			throw new NuclosFatalException(ex);
		}
		return result;
	}

	public void setDescription(Locale locale, String sDescription) {
		try {
			if (Locale.GERMAN.equals(locale)) {
				this.docDescriptionDE.remove(0, docDescriptionDE.getLength());
				if (sDescription != null) {
					this.docDescriptionDE.insertString(0, sDescription, null);
				}
			} else {
				this.docDescriptionEN.remove(0, docDescriptionEN.getLength());
				if (sDescription != null) {
					this.docDescriptionEN.insertString(0, sDescription, null);
				}
			}
		}
		catch (BadLocationException ex) {
			throw new NuclosFatalException(ex);
		}
	}

}
