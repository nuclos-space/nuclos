//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.admin;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableBean;
import org.nuclos.common.collect.collectable.AbstractCollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.collection.Transformer;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateModelVO;

/**
 * Makes a StateModelVO look like a Collectable.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableStateModel extends AbstractCollectableBean<StateModelVO,UID> {

	private final StateGraphVO stategraphvo;
	private final boolean bComplete;
	
	public static final UID entity = E.STATEMODEL.getUID();

	public static class Entity extends AbstractCollectableEntity {

		private Entity() {			
			super(entity, "Statusmodell");
			
			DefaultCollectableEntityField fieldName = new DefaultCollectableEntityField(E.STATEMODEL.name.getUID(), String.class, 
					getSpringLocaleDelegate().getMessage("CollectableStateModel.3","Name"),
					getSpringLocaleDelegate().getMessage("CollectableStateModel.4","Name des Statusmodells"), 
					255, null, false, CollectableField.TYPE_VALUEFIELD, null, null, entity, null);
			
			DefaultCollectableEntityField fieldDesc = new DefaultCollectableEntityField(E.STATEMODEL.description.getUID(), String.class, 
					getSpringLocaleDelegate().getMessage("CollectableStateModel.1","Beschreibung"),
					getSpringLocaleDelegate().getMessage("CollectableStateModel.2","Name des Beschreibung des Statusmodells"), 
					4000, null, true, CollectableField.TYPE_VALUEFIELD, null, null, entity, null);
			
			this.addCollectableEntityField(fieldName);
			this.addCollectableEntityField(fieldDesc);
		}

	}	// inner class Entity

	public static final CollectableEntity clcte = new Entity();

	/**
	 * §postcondition !this.isComplete()
	 */
	public CollectableStateModel(StateModelVO statemodelvo) {
		this(new StateGraphVO(statemodelvo), false);
		assert !this.isComplete();
	}

	/**
	 * §postcondition this.isComplete()
	 */
	public CollectableStateModel(StateGraphVO stategraphvo) {
		this(stategraphvo, true);
		assert this.isComplete();
	}

	private CollectableStateModel(StateGraphVO stategraphvo, boolean bComplete) {
		super(stategraphvo.getStateModel());
		this.stategraphvo = stategraphvo;
		this.bComplete = bComplete;
	}

	@Override
	public boolean isComplete() {
		return this.bComplete;
	}

	public StateGraphVO getStateGraphVO() {
		return this.stategraphvo;
	}

	public StateModelVO getStateModelVO() {
		return this.getBean();
	}

	@Override
	public UID getId() {
		return this.getStateModelVO().getId();
	}

	@Override
	public void removeId() {
		this.getStateModelVO().setPrimaryKey(null);
	}

	@Override
	protected CollectableEntity getCollectableEntity() {
		return clcte;
	}

	@Override
	public int getVersion() {
		return this.getStateModelVO().getVersion();
	}

	@Override
	public Object getValue(UID fieldUid) {
		try {
			String sFieldName = MetaProvider.getInstance().getEntityField(fieldUid).getFieldName();
			return PropertyUtils.getSimpleProperty(this.getStateModelVO(), sFieldName);
		}
		catch (IllegalAccessException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (InvocationTargetException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (NoSuchMethodException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	@Override
	public UID getEntityUID() {
		return clcte.getUID();
	}

	@Override
	public UID getPrimaryKey() {
		return getBean().getPrimaryKey();
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getCollectableEntity());
		result.append(",vo=").append(getBean());
		result.append(",stateVo=").append(getStateModelVO());
		result.append(",stateGraphVo=").append(getStateGraphVO());
		result.append(",id=").append(getId());
		//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
		//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//				result.append(",label=").append(getIdentifierLabel());
		result.append(",complete=").append(isComplete());
		result.append("]");
		return result.toString();
	}
	
	public static class MakeCollectable implements Transformer<StateModelVO, CollectableStateModel> {
		@Override
		public CollectableStateModel transform(StateModelVO statemodelvo) {
			return new CollectableStateModel(statemodelvo);
		}
	}

}	// class CollectableStateModel
