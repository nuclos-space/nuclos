//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.report;

import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.NuclosReportPrintJob;
import org.nuclos.common.report.NuclosReportRemotePrintService;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Report delegate.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class ReportDelegate {

	private static final Logger LOG = Logger.getLogger(ReportDelegate.class);
	private static ReportDelegate INSTANCE;

	// Spring injection

	private ReportFacadeRemote reportFacadeRemote;

	// end of Spring injection
	
	private Map<UsageCriteria, Collection<DefaultReportVO>> reportsByUsageCache = new HashMap<UsageCriteria, Collection<DefaultReportVO>>();

	ReportDelegate() {
		INSTANCE = this;
	}

	public static ReportDelegate getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	public final void setReportFacadeRemote(ReportFacadeRemote reportFacadeRemote) {
		this.reportFacadeRemote = reportFacadeRemote;
	}

	public Collection<UID> getReadableReportIdsForCurrentUser() throws NuclosFatalException {
		return reportFacadeRemote.getReadableReportUIDsForCurrentUser();
	}

	private CollectableSearchCondition getUserReadableReportsSubCondition(CollectableMasterDataEntity clmde)
			throws NuclosFatalException {
		Collection<UID> readableReportIdsForCurrentUser = getReadableReportIdsForCurrentUser();
		CollectableSearchCondition res = SearchConditionUtils
				.getCollectableSearchConditionForIds(readableReportIdsForCurrentUser);
		if (!SearchConditionUtils.isAlwaysFalseCondition(res)) {
			res.setConditionName("user readable");
		}
		return res;
	}

	public CollectableSearchCondition getCollectableSearchCondition(CollectableMasterDataEntity clctmde,
			CollectableSearchCondition superclctcond) {
		final CollectableSearchCondition result;
		final CollectableSearchCondition userReadableReportsCond = getUserReadableReportsSubCondition(clctmde);
		final CollectableSearchCondition cond = superclctcond;
		if (cond != null) {
			final Collection<CollectableSearchCondition> collOperands = Arrays.asList(new CollectableSearchCondition[] {
					cond, userReadableReportsCond });
			result = new CompositeCollectableSearchCondition(LogicalOperator.AND, collOperands);
		}
		else
			result = userReadableReportsCond;
		return result;
	}

	public MasterDataVO<UID> create(MasterDataVO<UID> mdvoInserted, IDependentDataMap mpDependants)
			throws CommonBusinessException {
		return reportFacadeRemote.create(mdvoInserted, mpDependants);
	}

	/**
	 * @return collection of all reports the current user has access to
	 * @throws CommonPermissionException
	 */
	public Collection<DefaultReportVO> getReports() throws CommonPermissionException {
		return reportFacadeRemote.getReports();
	}

	/**
	 * @return collection of all reports the datasource with the specified id is used in.
	 * @throws CommonPermissionException
	 */
	public Collection<DefaultReportVO> getReportsForDatasourceId(UID datasourceUID) throws CommonPermissionException {
		return reportFacadeRemote.getReportsForDatasourceId(datasourceUID, ReportType.REPORT);
	}

	/**
	 * @return collection of all reports the datasource with the specified id is used in.
	 */
	public Collection<DefaultReportVO> getFormularsForDatasourceId(UID datasourceUID) throws CommonPermissionException {
		return reportFacadeRemote.getReportsForDatasourceId(datasourceUID, ReportType.FORM);
	}

	public UID modify(MasterDataVO<UID> mdvo, IDependentDataMap mpDependants) throws CommonBusinessException {
		return reportFacadeRemote.modify(mdvo, mpDependants);
	}

	public void removeReport(MasterDataVO<UID> mdvo) throws CommonBusinessException {
		reportFacadeRemote.remove(mdvo);
	}

	public NuclosFile testReport(UID reportUID) throws NuclosReportException {
		return reportFacadeRemote.testReport(reportUID);
	}

	public NuclosFile prepareReport(UID id, Map<String, Object> params, Integer iMaxRowCount, UID language)
			throws CommonBusinessException {
		return reportFacadeRemote.prepareReport(id, params, iMaxRowCount, language);
	}

	/**
	 * TODO: Don't serialize CollectableEntityField and/or CollectableEntity! (tp)
	 * Refer to {@link org.nuclos.common.CollectableEntityFieldWithEntity#readObject(ObjectInputStream)} for details.
	 */
	public NuclosFile prepareSearchResult(CollectableSearchExpression clctexpr,
			final List<? extends CollectableEntityField> lstclctefweSelected, List<Integer> selectedFieldWidth,
			UID entityUID, ReportOutputVO.Format format, String customUsage,
			ReportOutputVO.PageOrientation orientation, boolean columnScaled) throws NuclosReportException {
		return reportFacadeRemote.prepareSearchResult(clctexpr, lstclctefweSelected, selectedFieldWidth, entityUID,
				format, customUsage, orientation, columnScaled);
	}

	public NuclosFile prepareExport(ResultVO resultvo, ReportOutputVO.Format format) throws NuclosReportException {
		return reportFacadeRemote.prepareExport(resultvo, format);
	}

	public NuclosFile prepareExport(String searchCondition, ResultVO resultvo, ReportOutputVO.Format format,
			ReportOutputVO.PageOrientation orientation, boolean columnScaled) throws NuclosReportException {
		return reportFacadeRemote.prepareExport(searchCondition, resultvo, format, orientation, columnScaled);
	}

	/**
	 * @param reportUID report/form id
	 * @return Is save allowed for the report/form with the given id?
	 */
	public boolean isSaveAllowed(UID reportUID) {
		return reportFacadeRemote.isSaveAllowed(reportUID);
	}

	/**
	 * finds reports (forms) by usage criteria
	 * @param usagecriteria
	 * @return collection of reports (forms)
	 */
	public Collection<DefaultReportVO> findReportsByUsage(UsageCriteria usagecriteria) {
		synchronized (reportsByUsageCache) {
			Collection<DefaultReportVO> result = reportsByUsageCache.get(usagecriteria);
			if (result != null) {
				return result;
			} else {
				result = reportFacadeRemote.findReportsByUsage(usagecriteria);
				if (result == null) {
					result = Collections.emptyList();
				}
				reportsByUsageCache.put(usagecriteria, result);
				return result;
			}
		}
	}
	
	/**
	 * 
	 * @param usagecriteria
	 * @return
	 */
	public boolean hasReportsAssigned(UsageCriteria usagecriteria){
		return (findReportsByUsage(usagecriteria).size() > 0);
	}

	/**
	 * get output formats for report
	 * 
	 * @param eportUID id of report
	 * @return collection of output formats
	 */
	public Collection<DefaultReportOutputVO> getReportOutputs(UID eportUID) {
		return reportFacadeRemote.getReportOutputs(eportUID);
	}

	public PrintService lookupDefaultPrintService() throws NuclosReportException {
		return reportFacadeRemote.lookupDefaultPrintService();
	}

	public PrintService[] lookupPrintServices(DocFlavor flavor, AttributeSet as) throws NuclosReportException {
		return reportFacadeRemote.lookupPrintServices(flavor, as);
	}

	public void printViaPrintService(NuclosReportRemotePrintService ps, NuclosReportPrintJob pj,
			PrintRequestAttributeSet aset, byte[] data) throws NuclosReportException {
		reportFacadeRemote.printViaPrintService(ps, pj, aset, data);
	}
	
	public void invalidateCaches() {
		synchronized (reportsByUsageCache) {
			LOG.info("Invalidating reportsByUsage cache.");
			reportsByUsageCache.clear();
		}
	}

} // class ReportDelegate
