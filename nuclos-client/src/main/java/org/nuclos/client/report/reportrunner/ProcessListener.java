package org.nuclos.client.report.reportrunner;

import java.util.EventListener;

public interface ProcessListener extends EventListener {
	
	void onDone(ProcessEvent event);
	
	void onCancel(ProcessEvent event);
	
	void onError(ProcessEvent event);

}
