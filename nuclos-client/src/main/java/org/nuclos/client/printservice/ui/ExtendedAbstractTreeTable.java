//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice.ui;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.TreeTableCellEditor;
import org.jdesktop.swingx.treetable.TreeTableModel;

/**
 * {@link JXTreeTable} lacks support of 
 * 
 * {@link TreeTableCellEditor}
 * 
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class ExtendedAbstractTreeTable extends JXTreeTable {

	private TableCellEditor editorForHierarchicalColumn;
	private TableCellRenderer rendererForHierarchicalColumn;
	
	public ExtendedAbstractTreeTable() {
		super();
			
	}

	public ExtendedAbstractTreeTable(TreeTableModel treeModel) {
		super(treeModel);
	}
	
	
	/**
	 * Custom render for TreeNode
	 * 
	 * 
	 * 
	 * set {@link TableCellEditor} applied on hierarchical column(s)
	 * 
	 * @param editor {@link TableCellEditor}
	 
	public void setCellEditorForTree(final TableCellEditor editor) {
		this.editorForHierarchicalColumn = editor;
	}
	
	public void setCellRendererForTree(final TableCellRenderer renderer) {
		this.rendererForHierarchicalColumn = renderer;
	}
	
	public TableCellEditor getStandardCellEditor(int row, int column) {
		return super.getCellEditor(row, column);
	}
	public TableCellRenderer getStandardCellRenderer(int row, int column) {
		return super.getCellRenderer(row, column);
	}
	
	@Override
	public TableCellEditor getCellEditor(int row, int column) {
		if (null != editorForHierarchicalColumn && isHierarchical(column)) {
			return editorForHierarchicalColumn;
		} else {
			return super.getCellEditor(row, column);
		}
	}
	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {
		if (null != rendererForHierarchicalColumn && isHierarchical(column)) {
			return rendererForHierarchicalColumn;
		} else {
			return super.getCellRenderer(row, column);
		}
	}
	
*/
}
