//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice.ui;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.nuclos.client.ui.treetable.TreeTableModel;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.Mutable;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.Destination;
import org.nuclos.common.report.valueobject.ReportVO.OutputType;
import org.nuclos.common2.SpringLocaleDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link NuclosPrintoutModel}
 * default implementation for {@link PrintoutTreeNodeModel}
 * 
 * @see PrintoutTreeNodeModel
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class NuclosPrintoutModel extends AbstractPrintoutModel {

	private final static Logger LOG = LoggerFactory.getLogger(NuclosPrintoutModel.class);
	public final static UID COLUMN_CHOOSE = new UID("column-choose");
	
	private final List<PrintoutTreeNode> model;
	 
	private final List<PrintoutColumn> columnModel;
	private PrintoutColumn printserviceColumn;
	private PrintoutColumn trayColumn;
	private final Map<UID, PrintoutNodeUpdateVisitor> updateVisitor;
	
	private final Map<UID, PrintServiceTO> printServices = new HashMap<UID, PrintServiceTO>();
	private final Map<UID, PrintServiceTO.Tray> trays = new HashMap<UID, PrintServiceTO.Tray>();
	private final Map<String, PrintServiceTO> printServicesClient = new HashMap<String, PrintServiceTO>();
	private final Map<Pair<String, Integer>, PrintServiceTO.Tray> traysClient = new HashMap<Pair<String, Integer>, PrintServiceTO.Tray>();

	private final SpringLocaleDelegate localeDelegate;
	
	public static final UID COLUMN_TREE = new UID("column-tree");
	public static final Class<TreeTableModel> COLUMNCLASS_TREE = TreeTableModel.class;
	
	public NuclosPrintoutModel(final List<PrintoutTreeNode> printouts) {
		super(new DefaultMutableTreeNode()); // create anonymous root
		this.localeDelegate = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
		this.columnModel = new ArrayList<PrintoutColumn>();
		prepareColumns();
		this.updateVisitor = new HashMap<UID, PrintoutNodeUpdateVisitor>();
		this.model = new ArrayList<PrintoutTreeNode>();
		for (final PrintoutTreeNode printout : printouts) {
			this.model.add(printout);
		}
	}
	
	protected void prepareColumns() {
		this.columnModel.add(createDetachedColumn(
				COLUMN_TREE, 
				"DefaultPrintoutModel.column.tree.label", 
				"DefaultPrintoutModel.column.tree.description", 
				String.class, 
				false));
		this.columnModel.add(createDetachedColumn(
				COLUMN_CHOOSE, 
				null, 
				"DefaultPrintoutModel.column.choose.description", 
				Boolean.class, 
				true));
		this.printserviceColumn = createColumn(E.REPORTOUTPUT.printservice,true);
		this.columnModel.add(printserviceColumn);
		this.trayColumn = createColumn(E.REPORTOUTPUT.tray, true);
		this.columnModel.add(trayColumn);
		this.columnModel.add(createColumn(E.REPORTOUTPUT.copies,true));
		this.columnModel.add(createColumn(E.REPORTOUTPUT.isDuplex,true));
	}
	
	private final PrintoutColumn createDetachedColumn(final UID id, 
			final String name, 
			final String description, 
			final Class<?> javaclass, 
			final boolean isEditable) {
		return new PrintoutColumn() {
			
			@Override
			public boolean isEditable() {
				return isEditable;
			}
			
			@Override
			public String getName() {
				return name;
			}
			
			@Override
			public Class<?> getJavaClass() {
				return javaclass;
			}
			
			@Override
			public UID getId() {
				return id;
			}
			
			@Override
			public String getDescription() {
				return description;
			}
		};
	}
	private final NuclosFieldColumn createColumn(final FieldMeta<?> field, final boolean isEditable) {
		return new NuclosFieldColumn(field, isEditable);
	}
	
	public int getIndexOfColumn(final UID id) {
		int result = -1;
		int i = 0;
		for (final PrintoutColumn column : columnModel) {
			if (column.getId().equals(id)) {
				result = i; 
				break;
			}
			++i;
		}
		return result;
	}

	@Override
	public String getColumnName(int idxColumn) {
		String resId = this.columnModel.get(idxColumn).getName();
		if (resId == null) {
			return "";
		}
				return localeDelegate.getMessage(
						this.columnModel.get(idxColumn).getName(),
						this.columnModel.get(idxColumn).getName());
	}

	@Override
	public Class<?> getColumnClass(int idxColumn) {
		return this.columnModel.get(idxColumn).getJavaClass();
	}
	
	@Override
	public Object getValueAt(Object node, int idxColumn) {
		if (node instanceof PrintoutTreeNode) {
			final PrintoutTreeNode printoutVO = (PrintoutTreeNode)node;
			if (idxColumn == getIndexOfColumn(COLUMN_TREE)) {
				return printoutVO.getName();
			}
			return getValueForColumn(idxColumn, printoutVO);
		} else if (node instanceof OutputFormatTreeNode) {
			final OutputFormatTreeNode outputVO = (OutputFormatTreeNode)node;
			if (idxColumn == getIndexOfColumn(COLUMN_TREE)) {
				return outputVO.getDescription();
			}
			return getValueForColumn(idxColumn, outputVO);
		}
		return null;
	}

	@Override
	public int getColumnCount() {
		return columnModel.size();
	}

	@Override
	public Object getChild(Object parentNode, int idxOfParentNode) {
		Object result = null;
		if (parentNode instanceof PrintoutTreeNode) {
			final PrintoutTreeNode printoutNode = (PrintoutTreeNode) parentNode;
			result = printoutNode.getChildAt(idxOfParentNode);
			//result = printoutNode.getOutputFormats().get(idxOfParentNode);
		} else {
			result = model.get(idxOfParentNode);
		}
		
		return result;
	}

	@Override
	public int getChildCount(Object parentNode) {
		int result = -1;
		if (parentNode instanceof PrintoutTreeNode) {
			final PrintoutTreeNode printoutNode = (PrintoutTreeNode) parentNode;
			//result = printoutNode.getOutputFormats().size();
			result = printoutNode.getChildCount();
		} else {
			result = model.size();
		}
		return result;
		
	}

	@Override
	public int getIndexOfChild(final Object parentNode, final Object childNode) {
		int result = -1;
		if (
				(parentNode instanceof PrintoutTreeNode) 
				&& (childNode instanceof OutputFormatTreeNode)) {
			final PrintoutTreeNode printoutVO = (PrintoutTreeNode)parentNode;
			final OutputFormatTreeNode outputVO = (OutputFormatTreeNode)childNode;
			//result = printoutVO.getOutputFormats().indexOf(outputVO);
			result = printoutVO.getIndex(outputVO);
			
		} else {
			LOG.warn("illegal parent({}) to child({})", parentNode, childNode);
		}
		return result;
	}
	
	@Override
	public boolean isLeaf(Object node) {
		return (node != null && node instanceof OutputFormatTreeNode);
	}
	
	@Override
	public boolean isCellEditable(Object node, int idxColumn) {
		//(idxColumn == COLUMN_TREE) ? false
		boolean result = false;
		final PrintoutColumn column = this.columnModel.get(idxColumn);
		if (column.isEditable()) {
			if (node instanceof OutputFormatTreeNode) {
				final OutputFormatTreeNode ofNode = (OutputFormatTreeNode)node;
				final PrintoutTreeNode poNode = (PrintoutTreeNode) ofNode.getParent();
				if (COLUMN_CHOOSE.equals(column.getId())) {
					if (poNode.getPrintout().getOutputType() == OutputType.SINGLE) {
						result = !ofNode.isMandatory();
					}
				} else {
					if (ofNode.getOutputFormat().getDestination() != Destination.FILE &&
						ofNode.getOutputFormat().getDestination() != Destination.SCREEN) {
						result = true;	
					}
				}					
			} else if (node instanceof PrintoutTreeNode) {
				final PrintoutTreeNode poNode = (PrintoutTreeNode)node;
				if (poNode.getOutputType() != OutputType.SINGLE && COLUMN_CHOOSE.equals(column.getId())) {
					result = true;
				}
			}
		}
		return result;
	}
	
	
	protected boolean isValidColumnIndex(int idxColumn) {
		if (idxColumn > -1 && idxColumn < getColumnCount()) {
			return true;
		}
		return false;
	}

	protected PrintoutColumn getColumn(int columnIndex) {
		if (!isValidColumnIndex(columnIndex)) {
			throw new IllegalArgumentException("try to access illegal column " + columnIndex);
		}
		// locate column
		return columnModel.get(columnIndex);
	}

	protected Object getValueForColumn(int columnIndex, final PrintoutTreeNode printout) {
		final PrintoutColumn column = getColumn(columnIndex);
		 if (COLUMN_CHOOSE.equals(column.getId())) {
				return printout.isChoose();
		 }
		 return null;
		 
	}
	
	protected Object getValueForColumn(int columnIndex, final OutputFormatTreeNode outputVO) {
		final PrintoutColumn column = getColumn(columnIndex);

		// avoid reflection at this point, just resolve methods manually
		if (E.REPORTOUTPUT.description.getUID().equals(column.getId())) {
			return outputVO.getDescription();
		} else if ( E.REPORTOUTPUT.copies.getUID().equals(column.getId())) {
			return outputVO.getProperties().getCopies();
		} else if(E.REPORTOUTPUT.isDuplex.getUID().equals(column.getId())) {
			return outputVO.getProperties().isDuplex();
		} else if (E.REPORTOUTPUT.tray.getUID().equals(column.getId())) {
			if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_SERVER ||
				outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_SERVER) {
				UID trayId = (UID) outputVO.getProperties().getTrayId();
				if (trayId != null) {
					return trays.get(trayId);
				}
			} else if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_CLIENT ||
					   outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_CLIENT) {
				String printerNameOnClient = outputVO.getProperties().getPrinterNameOnClient();
				Integer trayNumberOnClient = outputVO.getProperties().getTrayNumberOnClient();
				if (printerNameOnClient != null && trayNumberOnClient != null) {
					return traysClient.get(new Pair<String, Integer>(printerNameOnClient, trayNumberOnClient));
				}
			}  
			return null;
		} else if (E.REPORTOUTPUT.printservice.getUID().equals(column.getId())) {
			if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_SERVER ||
				outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_SERVER) {
				UID printServiceId = (UID) outputVO.getProperties().getPrintServiceId();
				if (printServiceId != null) {
					return printServices.get(printServiceId);
				}
			} else if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_CLIENT ||
					   outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_CLIENT) {
				String printerNameOnClient = outputVO.getProperties().getPrinterNameOnClient();
				if (printerNameOnClient != null) {
					return printServicesClient.get(printerNameOnClient);
				}
			}
			return null;
		} else if (COLUMN_CHOOSE.equals(column.getId())) {
			return outputVO.isChoose();
		} else {
			LOG.error("unknown column {}", column.getId());
			throw new IllegalArgumentException("unknown column " + column);
		}
	}
	@Override
	public void setValueAt(final Object value, final Object node, final int idxColumn) {
		final PrintoutColumn column = getColumn(idxColumn);
		Object oldValue = null;
		if (node instanceof OutputFormatTreeNode) {
			final OutputFormatTreeNode outputVO = (OutputFormatTreeNode)node;
			
			// avoid reflection at this point, just resolve methods manually
			if ( E.REPORTOUTPUT.copies.getUID().equals(column.getId())) {
				oldValue = outputVO.getProperties().getCopies(); 
				outputVO.getProperties().setCopies((Integer) value);
				if (!RigidUtils.equal(oldValue, value) && !outputVO.isChoose()) {
					outputVO.setChoose(true);
					outputVO.setEdited(true);
					fireUpdate(COLUMN_CHOOSE, (TreeNode)node, false, true);
				}
				
			} else if(E.REPORTOUTPUT.isDuplex.getUID().equals(column.getId())) {
				oldValue = outputVO.getProperties().isDuplex();
				outputVO.getProperties().setDuplex((Boolean) value);
				if (!RigidUtils.equal(oldValue, value) && !outputVO.isChoose()) {
					outputVO.setChoose(true);
					outputVO.setEdited(true);
					fireUpdate(COLUMN_CHOOSE, (TreeNode)node, false, true);
				}
				
			} else if (E.REPORTOUTPUT.tray.getUID().equals(column.getId())) {
				oldValue = null;
				if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_SERVER ||
					outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_SERVER) {
					if (outputVO.getProperties().getTrayId() != null) {
						oldValue = trays.get(outputVO.getProperties().getTrayId());
					}
				} else if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_CLIENT ||
						   outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_CLIENT) {
					String printerNameOnClient = outputVO.getProperties().getPrinterNameOnClient();
					Integer trayNumberOnClient = outputVO.getProperties().getTrayNumberOnClient();
					if (printerNameOnClient != null && trayNumberOnClient != null) {
						oldValue = traysClient.get(new Pair<String, Integer>(printerNameOnClient, trayNumberOnClient));
					}
				}
				
				PrintServiceTO.Tray tray = (PrintServiceTO.Tray)value;
				
				if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_SERVER ||
					outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_SERVER) {
					outputVO.getProperties().setTrayId(tray==null ? null: tray.getId());
				} else if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_CLIENT ||
						   outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_CLIENT) {
					outputVO.getProperties().setTrayNumberOnClient(tray==null ? null: tray.getNumber());
					//outputVO.getProperties().setPrinterNameOnClient(tray==null ? null: tray.getPrintServiceName());
				}
				
				if (!RigidUtils.equal(oldValue, value) && !outputVO.isChoose()) {
					outputVO.setChoose(true);
					outputVO.setEdited(true);
					fireUpdate(COLUMN_CHOOSE, (TreeNode)node, false, true);
				}

			} else if (E.REPORTOUTPUT.printservice.getUID().equals(column.getId())) {
				oldValue = null;
				if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_SERVER ||
					outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_SERVER) {
					if (outputVO.getProperties().getPrintServiceId() != null) {
						oldValue = printServices.get(outputVO.getProperties().getPrintServiceId());
					}
				} else if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_CLIENT ||
						   outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_CLIENT) {
					if (outputVO.getProperties().getPrinterNameOnClient() != null) {
						oldValue = printServicesClient.get(outputVO.getProperties().getPrinterNameOnClient());
					}
				}
				
				Object oldTray = null;
				if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_SERVER ||
					outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_SERVER) {
					if (outputVO.getProperties().getTrayId() != null) {
						oldTray = trays.get(outputVO.getProperties().getTrayId());
					}
				} else if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_CLIENT ||
						   outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_CLIENT) {
					String printerNameOnClient = outputVO.getProperties().getPrinterNameOnClient();
					Integer trayNumberOnClient = outputVO.getProperties().getTrayNumberOnClient();
					if (printerNameOnClient != null && trayNumberOnClient != null) {
						oldTray = traysClient.get(new Pair<String, Integer>(printerNameOnClient, trayNumberOnClient));
					}
				}
				
				PrintServiceTO ps = (PrintServiceTO)value;
				if (null != ps) {
					if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_SERVER ||
						outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_SERVER) {
						outputVO.getProperties().setPrintServiceId(ps.getId());
						Object newTray = ps.getDefaultTray();
						outputVO.getProperties().setTrayId(newTray==null?null:((PrintServiceTO.Tray)newTray).getId());
						fireUpdate(trayColumn.getId(), (TreeNode)node, oldTray, newTray);
					} else if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_CLIENT ||
							   outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_CLIENT) {
						outputVO.getProperties().setPrinterNameOnClient(ps.getName());
						Object newTray = null;
						outputVO.getProperties().setTrayNumberOnClient(null);
						fireUpdate(trayColumn.getId(), (TreeNode)node, oldTray, newTray);
					}
				} else {
					if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_SERVER ||
						outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_SERVER) {
						outputVO.getProperties().setPrintServiceId(null);
						outputVO.getProperties().setTrayId(null);
						fireUpdate(trayColumn.getId(), (TreeNode)node, oldTray, null);
					} else if (outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.DEFAULT_PRINTER_CLIENT ||
							   outputVO.getOutputFormat().getDestination() == ReportOutputVO.Destination.PRINTER_CLIENT) {
						outputVO.getProperties().setPrinterNameOnClient(null);
						outputVO.getProperties().setTrayNumberOnClient(null);
						fireUpdate(trayColumn.getId(), (TreeNode)node, oldTray, null);
					}
				}
				if (!RigidUtils.equal(oldValue, value) && !outputVO.isChoose()) {
					outputVO.setChoose(true);
					outputVO.setEdited(true);
					fireUpdate(COLUMN_CHOOSE, (TreeNode)node, false, true);
				}

			} else if (COLUMN_CHOOSE.equals(column.getId())) {
				oldValue = outputVO.isChoose();
				outputVO.setChoose((Boolean)value);
				if (Boolean.FALSE.equals(value)) {
					outputVO.setEdited(false);
				}
			} else {
				LOG.error("unknown column {}", column.getId());
				throw new IllegalArgumentException("unknown column " + column);
			}
			
			if (LOG.isDebugEnabled()) {
				LOG.debug("set field {} with value {}", column.getId(), value);
			}
		} else if (node instanceof PrintoutTreeNode) {
			final PrintoutTreeNode printoutNode = (PrintoutTreeNode)node;
			 if (COLUMN_CHOOSE.equals(column.getId())) {
				 oldValue = printoutNode.isChoose();
				 printoutNode.setChoose((Boolean)value);
				 if (Boolean.FALSE.equals(value)) {
					for (OutputFormatTreeNode outputVO : printoutNode.getOutputFormats()) {
						outputVO.setEdited(false);
					}
				 }
			 }
		} else {
			LOG.warn("unknown node type {}", node.getClass());
		}
		fireUpdate(column.getId(), (TreeNode)node, oldValue, value);
		
	}

	private void fireUpdate(UID idColumn, TreeNode node, Object oldValue, Object newValue) {
		if (this.updateVisitor.containsKey(idColumn)) {
			this.updateVisitor.get(idColumn).visit(idColumn, node, oldValue, newValue);
		}
	}

	@Override
	public void addPrintout(PrintoutTO printout) {
		final PrintoutTreeNode poNode = new PrintoutTreeNode(printout);
		final Enumeration<OutputFormatTreeNode> outputformats = poNode.children();
		synchronized(outputformats) {
			while (outputformats.hasMoreElements()) {
				final OutputFormatTreeNode nextElement = outputformats.nextElement();
				if (nextElement.isMandatory()) {
					setValueAt(true, nextElement, getIndexOfColumn(COLUMN_CHOOSE));
				}
			}
			this.model.add(poNode);
		}

	}
	
	public void fireTreeStructureChanged(final TreePath path) {
		this.modelSupport.fireTreeStructureChanged(path);
	}

	@Override
	public void clear() {
		this.model.clear();
	}
	
	public void setNodeVisitor(final UID idColumn, final PrintoutNodeUpdateVisitor visitor) {
		this.updateVisitor.put(idColumn, visitor);
	}
	
	public PrintoutTreeNode getPrintoutTreeNodeById(UID id) {
		for (final PrintoutTreeNode node : this.model) {
			if (node.getId().equals(id)) {
				return node;
			}
		}
		return null;
	}
	
	public OutputFormatTreeNode getOutputFormatTreeNodeById(UID id) {
		for (final PrintoutTreeNode node : this.model) {
			for (final OutputFormatTreeNode formatnode : node.getOutputFormats()) {
				if (formatnode.getId().equals(id)) {
					return formatnode;
				}
			}
		}
		return null;
	}
	
	public TreeNode getSelectedSingleNonMandatoryTreeNode(final Mutable<Boolean> isSelectionEmpty) {
		TreeNode result = null;
		isSelectionEmpty.setValue(true);
		for (final PrintoutTreeNode node : this.model) {
			if (node.getOutputType() == OutputType.SINGLE) {
				Enumeration<OutputFormatTreeNode> tmp = node.children();
				while (tmp.hasMoreElements()) {
					final OutputFormatTreeNode outputFormat = tmp.nextElement();
					if (outputFormat.isChoose() && !outputFormat.isMandatory()) {
						isSelectionEmpty.setValue(false);
						if (result == null) {
							result = outputFormat;
						} else {
							return null;
						}
					}
				}
			} else {
				if (node.isChoose()) {
					isSelectionEmpty.setValue(false);
					if (result == null) {
						result = node;
					} else {
						return null;
					}
				}
			}
		}
		return result;
	}
	
	public List<PrintoutTO> getSelectedPrintouts() {
		final List<PrintoutTO> result = new ArrayList<PrintoutTO>();
		for (final PrintoutTreeNode node : this.model) {
			final List<OutputFormatTO> outputFormats = new ArrayList<OutputFormatTO>();
			Enumeration<OutputFormatTreeNode> tmp = node.children();
			while (tmp.hasMoreElements()) {
				final OutputFormatTreeNode outputFormat = tmp.nextElement();
				if (outputFormat.isChoose()) {
					outputFormats.add(outputFormat.getOutputFormat());
				}
			}
			
			if (outputFormats.isEmpty()) {
				// continue if no output format is selected
				continue;
			}
			
			// at least 1 output format is selected, add to result (wrapped by the assigned printout)
			final PrintoutTO copyPrintout = new PrintoutTO(node.getId(), 
					node.getName(), node.getOutputType(), node.getPrintout().getDatasourceId());
			copyPrintout.getOutputFormats().addAll(outputFormats);
			copyPrintout.setBusinessObjectId(node.getBusinessObjectId());

			result.add(copyPrintout);
		}
		return result;
	}
	
	public void setPrintServices(List<PrintServiceTO> printServicesLst) {
		for (PrintServiceTO printService : printServicesLst) {
			this.printServices.put(printService.getId(), printService);
			for (PrintServiceTO.Tray tray : printService.getTrays()) {
				this.trays.put(tray.getId(), tray);
			}
		}
	}
	
	public void setPrintServicesClient(List<PrintServiceTO> modelPrintServicesClient) {
		for (PrintServiceTO printService : modelPrintServicesClient) {
			this.printServicesClient.put(printService.getName(), printService);
			for (PrintServiceTO.Tray tray : printService.getTrays()) {
				this.traysClient.put(new Pair<String, Integer>(printService.getName(), tray.getNumber()), tray);
			}
		}
	}

}
