package org.nuclos.client.printservice.ui;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import org.nuclos.common.report.valueobject.PrintoutTO;

public abstract class AbstractPrintoutModel extends AbstractTreeTableModel {
	
	/**
	 * create {@link AbstractPrintoutModel}
	 * 
	 * @param root root node
	 */
	public AbstractPrintoutModel(Object root) {
		super(root);
	}
	
	/**
	 * add {@link PrintoutTO}
	 * 
	 * @param printout {@link PrintoutTO}
	 */
	public abstract void addPrintout(final PrintoutTO printout);
	
	/**
	 * clear model
	 * 
	 */
	public abstract void clear();


}
