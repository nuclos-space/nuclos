//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.util.Collection;

import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;

/**
 * A <code>Collectable</code> with dependants.
 * The dependants are <code>CollectableMasterData</code>, so note that this class is Nucleus specific.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public interface CollectableWithDependants<PK> extends Collectable<PK> {

	/**
	 * §postcondition result != null
	 * 
	 * @param referencingField
	 * @return a copy of the contained dependants for the subentity with the given referencingField.
	 */
	Collection<CollectableEntityObject<PK>> getDependants(FieldMeta<?> referencingField);
	
	/**
	 * §postcondition result != null
	 * 
	 * @param dependentKey
	 * @return a copy of the contained dependants for the subentity with the given name.
	 */
	Collection<? extends AbstractCollectable<PK>> getDependants(IDependentKey dependentKey);

	/**
	 * inner class GetDependants
	 */
	class GetDependants<PK> implements Transformer<CollectableWithDependants<PK>, Collection<? extends AbstractCollectable<PK>>> {
		
		private final IDependentKey dependentKey;

		public GetDependants(IDependentKey dependentKey) {
			this.dependentKey = dependentKey;
		}

		@Override
		public Collection<? extends AbstractCollectable<PK>> transform(CollectableWithDependants<PK> clctlowd) {
			return clctlowd.getDependants(this.dependentKey);
		}
	}
	
	IDependentDataMap getDepenentDataMap();

	void setDependents(IDependentDataMap mpDependents);

}	// interface CollectableWithDependants
