package org.nuclos.client.masterdata.datatransfer;

import java.util.Collections;
import java.util.List;

import org.nuclos.common.UID;

public class MasterDataVORow<T> {

	public static class Row<T> {
		final List<T> lstFields;

		public Row(final List<T> lstFields) {
			this.lstFields = lstFields;
		}
		
		public List<T> getFields() {
			return Collections.unmodifiableList(lstFields);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((lstFields == null) ? 0 : lstFields.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Row<T> other = (Row<T>) obj;
			if (lstFields == null) {
				if (other.lstFields != null)
					return false;
			} else if (!lstFields.equals(other.lstFields))
				return false;
			return true;
		}
		
	}
	
	private final UID entity;
	private final Row<T> row;
	
	public MasterDataVORow(UID entity, Row<T> row) {
		super();
		this.entity = entity;
		this.row = row;
	}

	public UID getEntity() {
		return entity;
	}
	
	public Row<T> getRow() {
		return row;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entity == null) ? 0 : entity.hashCode());
		result = prime * result + ((row == null) ? 0 : row.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MasterDataVORow<T> other = (MasterDataVORow<T>) obj;
		if (entity == null) {
			if (other.entity != null)
				return false;
		} else if (!entity.equals(other.entity))
			return false;
		if (row == null) {
			if (other.row != null)
				return false;
		} else if (!row.equals(other.row))
			return false;
		return true;
	}
	
}
