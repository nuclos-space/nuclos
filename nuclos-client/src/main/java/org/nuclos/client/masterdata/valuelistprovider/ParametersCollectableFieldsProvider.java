//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.fileimport.CommonParseException;

public class ParametersCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(ParametersCollectableFieldsProvider.class);

	public static final String CLASSNAME = "showClass";

	public static final String VALUENAME = "showValue";

	private static final String DEFAULT_CLASSNAME = "java.lang.String";

	private final List<String> params = new ArrayList<String>();

	// 

	private String showClass = DEFAULT_CLASSNAME;
	
	/**
	 * @deprecated
	 */
	public ParametersCollectableFieldsProvider() {
	}

	public ParametersCollectableFieldsProvider(Class<?> showClass, List<String> values) {
		setParameter(CLASSNAME, showClass.getName());
		for (String v : values) {
			setParameter(VALUENAME, v);
		}
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (CLASSNAME.equalsIgnoreCase(sName)) {
			showClass = (String) oValue;
		} else if (VALUENAME.equalsIgnoreCase(sName)) {
			params.add((String) oValue);
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonParseException {
		// LOG.debug("getCollectableFields");
		final List<CollectableField> result = new ArrayList<CollectableField>();
		try {
			Class<?> clazz = LangUtils.getClassLoaderThatWorksForWebStart().loadClass(this.showClass);
			final Class<?>[] constructorAttrs = new Class[1];
			constructorAttrs[0] = java.lang.String.class;
			for (String i: params) {
				result.add(new CollectableValueField(clazz.getConstructor(constructorAttrs).newInstance(i)));
			}
		} catch (ClassNotFoundException e) {
			LOG.info("getCollectableFields failed: " + e, e);
			throw new CommonParseException("Invalid parameter: " + e.getMessage());
		} catch (IllegalArgumentException e) {
			LOG.info("getCollectableFields failed: " + e, e);
			throw new CommonParseException("Invalid parameter: " + e.getMessage());
		} catch (SecurityException e) {
			LOG.info("getCollectableFields failed: " + e, e);
			throw new CommonParseException("Invalid parameter: " + e.getMessage());
		} catch (InstantiationException e) {
			LOG.info("getCollectableFields failed: " + e, e);
			throw new CommonParseException("Invalid parameter: " + e.getMessage());
		} catch (IllegalAccessException e) {
			LOG.info("getCollectableFields failed: " + e, e);
			throw new CommonParseException("Invalid parameter: " + e.getMessage());
		} catch (InvocationTargetException e) {
			LOG.info("getCollectableFields failed: " + e, e);
			throw new CommonParseException("Invalid parameter: " + e.getMessage());
		} catch (NoSuchMethodException e) {
			LOG.info("getCollectableFields failed: " + e, e);
			throw new CommonParseException("Invalid parameter: " + e.getMessage());
		}
		Collections.sort(result);
		return result;
	}

}
