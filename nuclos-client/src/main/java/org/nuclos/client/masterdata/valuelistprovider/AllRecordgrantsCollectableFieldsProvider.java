//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.Utils;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * <code>ValueListProvider</code> for "all reports". This is used in the role administration dialog where all reports and forms
 * must be displayed, regardless of user rights.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class AllRecordgrantsCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(AllRecordgrantsCollectableFieldsProvider.class);
	
	//
	
	private UID entityUID;
	
	/**
	 * @deprecated
	 */
	AllRecordgrantsCollectableFieldsProvider() {
	}
	
	public AllRecordgrantsCollectableFieldsProvider(UID entityUID) {
		setParameter(NuclosConstants.VLP_ENTITY_UID_PARAMETER, entityUID);
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (NuclosConstants.VLP_ENTITY_UID_PARAMETER.equals(sName)) {
			if (oValue instanceof UID) {
				entityUID = (UID) oValue;
			} else if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
				entityUID = UID.parseUID((String) oValue);
			}
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		if (entityUID == null) {
			throw new NullPointerException();
		}

		final List<CollectableField> result = Utils.getCollectableFieldsByName(
				entityUID, MasterDataDelegate.getInstance().getAllRecordgrants(), Utils.getNameIdentifierField(entityUID).getStringifiedDefinition(), true);
		Collections.sort(result);
		return result;
	}

}	// class AllReportsCollectableFieldsProvider
