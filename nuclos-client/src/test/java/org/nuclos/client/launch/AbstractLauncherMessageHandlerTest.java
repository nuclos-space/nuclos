package org.nuclos.client.launch;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.junit.Test;
import org.nuclos.schema.launcher.LauncherLaunchMessage;
import org.nuclos.schema.launcher.LauncherMessage;
import org.nuclos.schema.launcher.LauncherPidMessage;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class AbstractLauncherMessageHandlerTest {
	AbstractLauncherMessageHandler testHandler = new AbstractLauncherMessageHandler() {
		@Override
		protected void handleLaunchMessage(final LauncherLaunchMessage message) {
		}

		@Override
		protected void handlePidMessage(final LauncherPidMessage message) {
		}
	};

	@Test
	public void allMessageTypesMustBeHandled() throws IllegalAccessException, InstantiationException {
		Class[] messageTypes = LauncherMessage.class.getAnnotation(XmlSeeAlso.class).value();
		for (Class<?> cls: messageTypes) {
			LauncherMessage o = (LauncherMessage) cls.newInstance();
			testHandler.handleMessage(o);
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void unknownMessageShouldCauseException() throws IllegalAccessException, InstantiationException {
		// Anonymous class is unknown to the handler
		LauncherMessage m = new LauncherMessage(){};
		testHandler.handleMessage(m);
	}

}