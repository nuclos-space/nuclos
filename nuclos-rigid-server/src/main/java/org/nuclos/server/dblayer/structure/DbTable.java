//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.structure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Predicate;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.structure.DbConstraint.DbPrimaryKeyConstraint;

public class DbTable extends DbArtifact {

	private final List<DbTableArtifact> tableArtifacts;
	private final boolean virtual;

	public DbTable(UID uid, String tableName, DbTableArtifact...tableArtifacts) {
		this(uid, tableName, Arrays.asList(tableArtifacts));
	}

	public DbTable(UID uid, String tableName, Collection<? extends DbTableArtifact> tableArtifacts) {
		this(uid, tableName, tableArtifacts, false);
	}

	public DbTable(UID uid, String tableName, Collection<? extends DbTableArtifact> tableArtifacts, boolean virtual) {
		super(uid, tableName);
		for (DbTableArtifact tableArtifact : tableArtifacts) {
			if (tableArtifact.getTable() == null) {
				tableArtifact.setTable(getNamedObject());
			} else if (!getNamedObject().equals(tableArtifact.getTable()) && !virtual) {
				throw new IllegalArgumentException("Table artifact " + tableArtifact + " belongs to different table");
			}
		}
		this.tableArtifacts = new ArrayList<DbTableArtifact>(tableArtifacts);
		this.virtual = virtual;
		Collections.sort(this.tableArtifacts, DbArtifact.COMPARATOR);
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("table=").append(getTableName());
		result.append(", artefacts=").append(tableArtifacts);
		result.append("]");
		return result.toString();
	}

	public String getTableName() {
		return getSimpleName();
	}

	public List<DbTableArtifact> getTableArtifacts() {
		return tableArtifacts;
	}

	public <T extends DbTableArtifact> List<T> getTableArtifacts(Class<T> clazz) {
		return RigidUtils.selectInstancesOf(tableArtifacts, clazz);
	}

	public List<DbColumn> getTableColumns() {
		return getTableArtifacts(DbColumn.class);
	}

	public Pair<DbTable, List<DbTableArtifact>> flatten() {
		return flatten(false);
	}

	public Pair<DbTable, List<DbTableArtifact>> flatten(boolean flattenColumns) {
		Predicate<DbTableArtifact> predicate;
		if (flattenColumns) {
			predicate = RigidUtils.alwaysFalse();
		} else {
			predicate = RigidUtils.<DbTableArtifact>isInstanceOf(DbColumn.class);
		}
		Pair<List<DbTableArtifact>,List<DbTableArtifact>> p = RigidUtils.split(tableArtifacts, predicate);
		return Pair.makePair(new DbTable(getUID(), getTableName(), p.x, virtual), p.y);
	}

	@Override
	protected boolean isUnchanged(DbArtifact a, boolean forceNames) {
		DbTable other = (DbTable) a;
		Map<String, DbTableArtifact> map = DbArtifact.makeNameMap(getTableArtifacts(), forceNames);
		Map<String, DbTableArtifact> otherMap = DbArtifact.makeNameMap(other.getTableArtifacts(), forceNames);
		return getNamedObject().getName().equals(other.getNamedObject().getName()) && map.equals(otherMap);
	}

	@Override
	public <T> T accept(DbArtifactVisitor<T> visitor) throws DbException {
		return visitor.visitTable(this);
	}

	public boolean isVirtual() {
		return virtual;
	}
	
	public String getPkColumnName() {
		List<DbPrimaryKeyConstraint> lstPkConstrs = getTableArtifacts(DbPrimaryKeyConstraint.class);
		if (lstPkConstrs != null && !lstPkConstrs.isEmpty()) {
			return lstPkConstrs.get(0).getColumnNames().get(0);
		}
		return null;
	}

}
