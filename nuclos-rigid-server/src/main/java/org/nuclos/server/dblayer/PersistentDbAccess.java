//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.nuclos.common.collection.Transformer;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.structure.DbTableType;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Configurable
public class PersistentDbAccess {

	private final DbAccess dbAccess;
	
	public PersistentDbAccess(DbAccess dbAccess) {
		this.dbAccess = dbAccess;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public int execute(List<? extends DbBuildableStatement> statements) throws DbException {
		return this.dbAccess.execute(statements);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public int execute(DbBuildableStatement statement) throws DbException {
		return this.dbAccess.execute(statement);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public int executeDelete(DbDelete delete) throws DbException {
		return this.dbAccess.executeDelete(delete);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public int executeUpdate(DbUpdate update) throws DbException {
		return this.dbAccess.executeUpdate(update);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public <T> List<T> executeQuery(DbQuery<? extends T> query) throws DbException {
		return this.dbAccess.executeQuery(query);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public ResultVO executePlainQuery(String sql, int maxRows, final boolean DEPRECATED_2017_ALLOWED) throws DbException {
		return this.dbAccess.executePlainQueryAsResultVO(sql, maxRows, DEPRECATED_2017_ALLOWED);
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public int executePlainUpdate(String sql) throws DbException {
		return this.dbAccess.executePlainUpdate(sql);
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public <T, R> List<R> executeQuery(DbQuery<T> query, Transformer<? super T, R> transformer) throws DbException {
		return this.dbAccess.executeQuery(query, transformer);
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public <T> T executeQuerySingleResult(DbQuery<? extends T> query) throws DbException {
		return this.dbAccess.executeQuerySingleResult(query);
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public <T, R> R executeQuerySingleResult(DbQuery<T> query, Transformer<? super T, R> transformer) throws DbException {
		return this.dbAccess.executeQuerySingleResult(query, transformer);
	}
	
	public DbQueryBuilder getQueryBuilder() {
		return this.dbAccess.getQueryBuilder();
	}
	
	public DbType getDbType() {
		return this.dbAccess.getDbType();
	}

	public String getSchemaName() {
		return this.dbAccess.getSchemaName();
	}

	public Collection<? extends String> getTableNames(DbTableType table) {
		return this.dbAccess.getTableNames(table);
	}

	public String generateName(String prefix, String base, String...affixes) {
		return this.dbAccess.generateName(prefix, base, affixes);
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append(dbAccess.toString());
		result.append("]");
		return result.toString();
	}
	
	public void commit() throws SQLException {
		dbAccess.getDbExecutor().commit();
	}
}
