//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidE;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbPlainStatement;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.statements.DbStructureChange.Type;
import org.nuclos.server.dblayer.structure.DbCallable;
import org.nuclos.server.dblayer.structure.DbCallableType;
import org.nuclos.server.dblayer.structure.DbSimpleView;
import org.nuclos.server.dblayer.structure.DbSimpleView.DbSimpleViewColumn;

public class DbObjectHelper {
	
	private static final Logger LOG = Logger.getLogger(DbObjectHelper.class);
	
	private static int runningThreads = 0;

	public static class DbObject{
		private String name;
		private DbObjectType type;
		private Integer order;
		public DbObject(String name, DbObjectType type, Integer order) {
	        super();
	        this.name = name;
	        this.type = type;
	        this.order = order;
        }
		public String getName() {
        	return name;
        }
		public DbObjectType getType() {
        	return type;
        }
		public Integer getOrder() {
			return RigidUtils.defaultIfNull(order, Integer.MAX_VALUE);
		}
		@Override
		public String toString() {
			final StringBuilder result = new StringBuilder();
			result.append(getClass().getName()).append("[");
			result.append("name=").append(name);
			result.append(", type=").append(type);
			result.append(", order=").append(order);
			result.append("]");
			return result.toString();
		}
		@Override
		public boolean equals(Object that) {
			if (this == that) {
				return true;
			}
			if (that instanceof DbObject) {
				return RigidUtils.equal(this.name, ((DbObject) that).name) &&
						RigidUtils.equal(this.type, ((DbObject) that).type);
			}
			return super.equals(that);
		}
		@Override
		public int hashCode() {
			return RigidUtils.hashCode(type + "." + name);
		}
	}

	public static enum DbObjectType{
		VIEW("view", 5),
		FUNCTION("function", 3),
		PROCEDURE("procedure", 4),
		PACKAGE("package", 1),
		PACKAGE_BODY("package body", 2),
		INDEX("index", 6);

		private String name;
		private int order;

		private DbObjectType(String name, int order) {
			this.name = name;
			this.order = order;
		}

		public String getName() {
			return this.name;
		}
		
		public int getOrder() {
			return this.order;
		}

		public boolean checkName(String name) {
			return this.name.equals(name);
		}

		public static DbObjectType getByName(String name) {
			for (DbObjectType type : DbObjectType.values()) {
				if (type.checkName(name))
					return type;
			}
			return null;
		}
		
	}

	private final DbAccess dbAccess;

	public DbObjectHelper(DbAccess dbAccess) {
		this.dbAccess = dbAccess;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("access=").append(dbAccess);
		result.append("]");
		return result.toString();
	}
	
	public static class DbObjectComparator implements Comparator<DbObject> {
		
		private boolean asc = true;
		
		public DbObjectComparator(boolean asc) {
			this.asc = asc;
		}
		
		@Override
		public int compare(DbObject o1, DbObject o2) {
			int result = RigidUtils.compare(o1.getOrder(), o2.getOrder());
			if (result == 0) {
				result = RigidUtils.compare(o1.getType().getOrder(), o2.getType().getOrder());
				if (result == 0) {
					result = RigidUtils.compare(o1.getName(), o2.getName());
				}
			}
			return asc?result:-result;
		}
	}

	/**
	 *
	 * @param type (could be null)
	 * @return
	 */
	public Map<DbObject, Pair<DbPlainStatement, DbStatement>> getAllDbObjects(DbObjectType type) {
		Map<DbObject, Pair<DbPlainStatement, DbStatement>> result = new HashMap<DbObject, Pair<DbPlainStatement, DbStatement>>();

		DbQuery<DbTuple> queryObject = dbAccess.getQueryBuilder().createTupleQuery();
		DbFrom<UID> fromObject = queryObject.from(RigidE.DBOBJECT, "dbo");
		queryObject.multiselect(
			fromObject.baseColumn(RigidE.DBOBJECT.getPk()).alias("PK"),
			fromObject.baseColumn(RigidE.DBOBJECT.name).alias("STRDBOBJECT"),
			fromObject.baseColumn(RigidE.DBOBJECT.dbobjecttype).alias("STRDBOBJECTTYPE"),
			fromObject.baseColumn(RigidE.DBOBJECT.order).alias("INTORDER"));

		for (DbTuple object : dbAccess.executeQuery(queryObject)) {
			UID pk = (UID)object.get("PK");
			String name = object.get(RigidE.DBOBJECT.name);
			DbObjectType currentType = DbObjectType.getByName(object.get(RigidE.DBOBJECT.dbobjecttype));
			Integer order = object.get(RigidE.DBOBJECT.order);
			Pair<DbPlainStatement, DbStatement> statement = this.getDbObject(pk, type);
			if (statement != null)
				result.put(new DbObject(name, currentType, order), statement);
		}
		return result;
	}
		
	/**
	 * @param name Name of DbObject
	 * @return
	 */
	public DbPlainStatement getDbObjectStatementByName(String name) {

		DbQuery<DbTuple> queryObject = dbAccess.getQueryBuilder().createTupleQuery();
		DbFrom<UID> fromObject = queryObject.from(RigidE.DBOBJECT, "dbo");
		queryObject.multiselect(
			fromObject.baseColumn(RigidE.DBOBJECT.getPk()).alias("PK"),
			fromObject.baseColumn(RigidE.DBOBJECT.dbobjecttype).alias("STRDBOBJECTTYPE"));
		
		queryObject.addToWhereAsAnd(dbAccess.getQueryBuilder().equalValue(fromObject.baseColumn(RigidE.DBOBJECT.name), name.toUpperCase()));

		for (DbTuple object : dbAccess.executeQuery(queryObject)) {
			UID pk = (UID)object.get("PK");
			DbObjectType currentType = DbObjectType.getByName(object.get(RigidE.DBOBJECT.dbobjecttype));
			
			Pair<DbPlainStatement, DbStatement> statement = this.getDbObject(pk, currentType);
			
			if (statement != null) {
				return statement.x;
			}
		}
		return null;
	}
	
	public static String getSourceTableForSimpleViewIfAny(DbPlainStatement st) {
		if (st != null && StringUtils.isNotEmpty(st.getSql())) {
			int n = st.getSql().toUpperCase().lastIndexOf(" FROM ");
			if (n > -1) {
				String fromTable = st.getSql().substring(n + " FROM ".length()).trim();
				String fromTableWODotsAndUnderscores = fromTable.replaceAll("[\\._]", "");
				
				//Check if this is really just one table the select is "from"
				if (StringUtils.isAlphanumeric(fromTableWODotsAndUnderscores)) {
					return fromTable;
				}	
			}
		}
		
		return null;
	}
	
	private static Map<String, String> cacheSourceTableForSimpleView = new HashMap<String, String>();
	
	public String getSourceTableForSimpleViewIfAny(String nameOfView) {
		if (!cacheSourceTableForSimpleView.containsKey(nameOfView)) {
			DbPlainStatement st = getDbObjectStatementByName(nameOfView);
			cacheSourceTableForSimpleView.put(nameOfView, st != null ? getSourceTableForSimpleViewIfAny(st) : null);			
		}
		
		return cacheSourceTableForSimpleView.get(nameOfView);		
	}

	/**
	 *
	 * @param entityMeta
	 * @return
	 */
	public Pair<DbPlainStatement, DbStatement> getUserdefinedEntityView(MetaDbEntityWrapper entityMeta, MetaDbProvider provider) {
		if (provider.isNuclosEntity(entityMeta.getUID())){
			// is system entitiy
			return null;
		}
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<UID> queryObject = builder.createQuery(UID.class);
		DbFrom<UID> fromObject = queryObject.from(RigidE.DBOBJECT, "dbo");
		queryObject.select(fromObject.baseColumn(SF.PK_UID));
		queryObject.where(builder.and(
			builder.equalValue(fromObject.baseColumn(RigidE.DBOBJECT.name), entityMeta.getDbTable()),
			builder.equalValue(fromObject.baseColumn(RigidE.DBOBJECT.dbobjecttype), DbObjectType.VIEW.getName())));
		final List<UID> dbObjectIds = dbAccess.executeQuery(queryObject);

		if (dbObjectIds.size() > 1)
			throw new NuclosFatalException("DbObject is not unique.");

		if (dbObjectIds.size() != 0)
			return this.getDbObject(dbObjectIds.get(0), DbObjectType.VIEW);

		return null;
	}

	/**
	 *
	 * @param entityMeta
	 * @return
	 */
	public boolean hasUserdefinedEntityView(MetaDbEntityWrapper entityMeta, MetaDbProvider provider) {
		return getUserdefinedEntityView(entityMeta, provider)!=null;
	}

	/**
	 *
	 * @param name
	 * @param type
	 * @param source
	 * @param drop
	 * @return
	 */
	public static Pair<DbPlainStatement, DbStatement> getStatements(String name, String type, String source, String drop) {
		DbStatement dropStatement;
		if (RigidUtils.looksEmpty(drop)) {
			switch (DbObjectType.getByName(type)) {
			case VIEW:
				dropStatement = new DbStructureChange(Type.DROP, new DbSimpleView(null, null, name, new ArrayList<DbSimpleViewColumn>()));
				break;
			case FUNCTION:
				dropStatement = new DbStructureChange(Type.DROP, new DbCallable(DbCallableType.FUNCTION, null, name, null));
				break;
			case PROCEDURE:
				dropStatement = new DbStructureChange(Type.DROP, new DbCallable(DbCallableType.PROCEDURE, null, name, null));
				break;
			default:
				throw new UnsupportedOperationException("Could not determine drop statement, please define it yourself.");
			}

		} else {
			dropStatement = new DbPlainStatement(drop);
		}
		return new Pair<DbPlainStatement, DbStatement>(new DbPlainStatement(source), dropStatement);
	}

	public Pair<DbPlainStatement, DbStatement> getDbObject(UID uid, DbObjectType type) {
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> querySource = builder.createTupleQuery();
		DbFrom<UID> fromSource = querySource.from(RigidE.DBSOURCE, "dbsource");
		
		// TODO MULTINUCLET:
		// different type DBOBJECT_name<->DBSOURCE_dbobject
		// DbFrom fromObject = fromSource.join(E.DBOBJECT, JoinType.RIGHT).alias("dbobject").on("STRDBOBJECT", "STRDBOBJECT", String.class);
		DbFrom fromObject = fromSource.join(RigidE.DBOBJECT, JoinType.RIGHT, "dbobject").on(RigidE.DBSOURCE.dbobject, RigidE.DBOBJECT.getPk());
		querySource.multiselect(
			//fromSource.baseColumn("CLBSOURCE", String.class).alias("CLBSOURCE"),
			fromSource.baseColumn(RigidE.DBSOURCE.source),
			//fromSource.baseColumn("CLBDROPSTATEMENT", String.class).alias("CLBDROPSTATEMENT"),
			fromSource.baseColumn(RigidE.DBSOURCE.dropstatement),
			//fromSource.baseColumn("BLNACTIVE", Boolean.class).alias("BLNACTIVE"),
			fromSource.baseColumn(RigidE.DBSOURCE.active),
			//fromObject.baseColumn("STRDBOBJECTTYPE", String.class).alias("STRDBOBJECTTYPE"));
			fromObject.baseColumn(RigidE.DBOBJECT.dbobjecttype),
			fromObject.baseColumn(RigidE.DBOBJECT.name));
		querySource.where(builder.and(
			//builder.equal(fromSource.baseColumn("STRDBTYPE", String.class), this.dbAccess.getDbType().toString()),
			builder.equalValue(fromSource.baseColumn(RigidE.DBSOURCE.dbtype), this.dbAccess.getDbType().toString()),
			//builder.equal(fromSource.baseColumn("STRDBOBJECT", String.class), name)));
			builder.equalValue(fromSource.baseColumn(RigidE.DBSOURCE.dbobject), uid)));
		List<DbTuple> dbSources = dbAccess.executeQuery(querySource);

		if (dbSources.size() > 1)
			throw new NuclosFatalException("DbSource is not unique.");

		if (dbSources.size() != 0) {
			DbTuple dbSource = dbSources.get(0);
			if (dbSource.get(RigidE.DBSOURCE.active)) {
				// String sSource = dbSource.get("CLBSOURCE", String.class);
				String sSource = dbSource.get(RigidE.DBSOURCE.source);
				// String sDrop = dbSource.get("CLBDROPSTATEMENT", String.class);
				String sDrop = dbSource.get(RigidE.DBSOURCE.dropstatement);
				// String sType = dbSource.get("STRDBOBJECTTYPE", String.class);
				String sType = dbSource.get(RigidE.DBOBJECT.dbobjecttype);
				String name = dbSource.get(RigidE.DBOBJECT.name);
				if (type != null && !DbObjectType.getByName(sType).equals(type)) {
					return null;
				}

				return getStatements(name, sType, sSource, sDrop);
			}
		}

		return null;
	}

	public EntityMeta<?> getEntityMetaForView(String dbobjectname, IRigidMetaProvider provider) {
		for (EntityMeta<?> meta : provider.getAllEntities()) {
			if (dbobjectname.equals(meta.getDbTable())) {
				return meta;
			}
		}
		return null;
	}
	
	public static boolean isUsedAsCalculatedAttribute(String name, IRigidMetaProvider metaProvider) {
		for (EntityMeta<?> eMeta : metaProvider.getAllEntities())
			for (final FieldMeta<?> efMeta : metaProvider.getAllEntityFieldsByEntity(eMeta.getUID()).values())
				if (name.equalsIgnoreCase(efMeta.getCalcFunction()))
					return true;
		return false;
	}
	
	private static void logScript(List<String> script, List<?> statements) {
		for (Object sql : statements)
			if (sql != null && !RigidUtils.looksEmpty(sql.toString()))
				script.add(sql.toString());
	}
	
	public static synchronized void updateDbObjects(DbAccess dbAccess,
		Map<DbObject, Pair<DbPlainStatement, DbStatement>> dbObjects,
		Map<DbObject, Pair<DbPlainStatement, DbStatement>> compareDbObjects,
		DbStructureChange.Type type,
		boolean bExecuteDDL, List<String> script, StringBuffer sbResultMessage) throws NuclosBusinessException {
		
		if (DbObjectHelper.countRunningThreads() > 0) {
			throw new NuclosBusinessException("There are still running DbObject creation threads (" + DbObjectHelper.countRunningThreads() + "). Please try again later.");
		}
		
		cacheSourceTableForSimpleView.clear();
		
		final PersistentDbAccess pdbAccess = new PersistentDbAccess(dbAccess);
		
		for (DbObject dbObject : RigidUtils.sorted(dbObjects.keySet(), new DbObjectComparator(type==DbStructureChange.Type.CREATE))) {
			DbStatement stmt = null;
			boolean runThreaded = false;
			switch (type) {
				case CREATE:
					if (dbObject.type == DbObjectType.INDEX) {
						// only if new or changed...
						DbPlainStatement otherCreate = !compareDbObjects.containsKey(dbObject) ? null : compareDbObjects.get(dbObject).x;
						DbPlainStatement curCreate = dbObjects.get(dbObject).x;
						if (!RigidUtils.equal(otherCreate==null?null:otherCreate.getSql(), curCreate.getSql())) {
							runThreaded = true;
							stmt = dbObjects.get(dbObject).x;
						}
					} else {
						stmt = dbObjects.get(dbObject).x;
					}
					break;
				case DROP:
					if (dbObject.type == DbObjectType.INDEX) {
						// only if changed...
						DbPlainStatement otherCreate = !compareDbObjects.containsKey(dbObject) ? null : compareDbObjects.get(dbObject).x;
						DbPlainStatement curCreate = dbObjects.get(dbObject).x;
						if (!RigidUtils.equal(otherCreate==null?null:otherCreate.getSql(), curCreate.getSql())) {
							stmt = dbObjects.get(dbObject).y;
						}
					} else {
						stmt = dbObjects.get(dbObject).y;
					}
					break;
				default: break;
			}
			if (stmt != null) {
				if (bExecuteDDL && runThreaded) {
					logScript(script, Collections.singletonList(stmt+"... running in background. Please check log for errors!"));
				} else {
					logScript(script, Collections.singletonList(stmt));
				}
				if (bExecuteDDL) {
					if (runThreaded) {
						final String sName = dbObject.getName();
						final DbStatement tStmt = stmt;
						Thread t = new Thread(new Runnable() {
							@Override
							public void run() {
								try {
									increaseRunningThreads();
									pdbAccess.execute(tStmt);
								} catch (Exception ex) {
									LOG.error("DbObjectCreation of " + sName + " failed: " + ex.getMessage(), ex);
								} finally {
									decreaseRunningThreads();
								}
							}
						}, "DbOjectCreation_"+dbObject.getName());
						t.start();
					} else {
						try {
							pdbAccess.execute(stmt);
						} catch (Exception ex) {
							DbUtils.logDDLError(sbResultMessage, ex.getMessage(), Collections.singletonList(stmt));
						}
					}
				}
			}
		}
	}
	
	public static synchronized void increaseRunningThreads() {
		runningThreads++;
	}
	
	public static synchronized void decreaseRunningThreads() {
		runningThreads--;
	}
	
	public static synchronized int countRunningThreads() {
		return runningThreads;
	}

}
