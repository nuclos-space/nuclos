package org.nuclos.server.dblayer;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.nuclos.server.autosync.SchemaHelper;

public class SchemaDiff {
	public static final int SYSTEMTABLE = 1;
	public static final int DATATABLE = 2;
	public static final int ANYTABLE = SYSTEMTABLE | DATATABLE;
	
	private final Collection<String> dbTables;
	private final Collection<String> metaTables;
	private final Collection<String> allTables;
	
	public SchemaDiff(Collection<String> dbTables, Collection<String> metaTables) {
		this.dbTables = dbTables;
		this.metaTables = metaTables;
		this.allTables = new HashSet<String>(dbTables);
		this.allTables.addAll(metaTables);
	}
	
	private static final int REMOVED = 1;
	private static final int KEPT = 2;
	private static final int ADDED = 4;
	private static final int DB = 8;
	
	private Collection<String> getTables(int filter, int type) {
		Set<String> retRemoved = new HashSet<String>();
		Set<String> retKept = new HashSet<String>();
		Set<String> retAdded = new HashSet<String>();
		Set<String> retDb = new HashSet<String>();
		
		for (String table : allTables) {
			if (!isTableOfType(table, type)) {
				continue;
			}
			
			if (dbTables.contains(table)) {
				retDb.add(table);
			}
			
			if (metaTables.contains(table)) {
				
				if (dbTables.contains(table)) {
					retKept.add(table);
					
				} else {
					retAdded.add(table);
					
				}
				
			} else {
				retRemoved.add(table);
				
			}
		}
		
		switch (filter) {
			case REMOVED:
				return retRemoved;
			case KEPT:
				return retKept;
			case ADDED:
				return retAdded;
			case DB:
				return retDb;
		}
		
		return null;
	}
	
	public Collection<String> getRemovedTables(int type) {
		return getTables(REMOVED, type);
	}
	
	public Collection<String> getKeptTables(int type) {
		return getTables(KEPT, type);
	}
	
	public Collection<String> getAddedTables(int type) {
		return getTables(ADDED, type);
	}

	public Collection<String> getDbTables(int type) {
		return getTables(DB, type);
	}

	public static boolean isTableOfType(String table, int type) {
		boolean system = SchemaHelper.isSystemTable(table);
		
		if ((type & SYSTEMTABLE) != 0 && system) {
			return true;
		}

		if ((type & DATATABLE) != 0 && !system) {
			return true;
		}

		return false;
	}
	
}
