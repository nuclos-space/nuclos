//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.structure;

import org.nuclos.common.UID;
import org.nuclos.server.dblayer.DbException;

/**
 * Sequence.
 */
public class DbSequence extends DbArtifact {

	private final long startWith;
	private final boolean longSequence;;
	
	public DbSequence(UID uid, String name, Long startWith) {
		super(uid, name);
		this.startWith = startWith;
		this.longSequence = false;
	}
	public DbSequence(UID uid, String name, Long startWith,boolean longSeq) {
		super(uid, name);
		this.startWith = startWith;
		this.longSequence = longSeq;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("start=").append(startWith);
		result.append(", name=").append(getSimpleName());
		result.append("]");
		return result.toString();
	}

	public String getSequenceName() {
		return getSimpleName();
	}
	
	public long getStartWith() {
		return startWith;
	}
	
	@Override
	protected boolean isUnchanged(DbArtifact a, boolean forceNames) {
		DbSequence other = (DbSequence) a;
		return getStartWith() == other.getStartWith()&&isLongSequence()==other.isLongSequence();
	}
	
	@Override
	public <T> T accept(DbArtifactVisitor<T> visitor) throws DbException {
		return visitor.visitSequence(this);
	}

	@Override
	public boolean isVirtual() {
		return false;
	}
	public boolean isLongSequence() {
		return longSequence;
	}

	
}
