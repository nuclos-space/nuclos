package org.nuclos.server.autosync.migration.m_04_19_00;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.nuclos.common.E_04_19_0016_source;
import org.nuclos.common.E_04_19_0017_target;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.server.autosync.IMigrationHelper;
import org.nuclos.server.autosync.RigidEO;
import org.nuclos.server.autosync.migration.AbstractMigration;

public class Migration_04_19_0017 extends AbstractMigration {

	private Logger LOG;

	private IMigrationHelper helper;

	public class AlreadyMigratedException extends Exception {

		private static final long serialVersionUID = 1L;

		public AlreadyMigratedException() {
			super("RoleSubform migration done already");
		}

	}

	@Override
	public void migrate() {
		if (getContext().isNucletMigration()) {
			return; // no nuclet migration here
		}
		long start = System.currentTimeMillis();
		LOG = getLogger(Migration_04_19_0017.class);
		LOG.info("Migration started");

		helper = getContext().getHelper();

		try {
			migrateRoleSubforms();
		} catch (AlreadyMigratedException e) {
			LOG.info("Done by previous version already. Skipping...");
			return;
		}
		
		LOG.info("Migration finished in " + new DecimalFormat("0.###").format((System.currentTimeMillis()-start)/1000d/60d) + " minutes");
	}

	@Override
	public SysEntities getSourceMeta() {
		return E_04_19_0016_source.getThis();
	}

	@Override
	public String getSourceStatics() {
		return "2.8";
	}

	@Override
	public SysEntities getTargetMeta() {
		return E_04_19_0017_target.getThis();
	}

	@Override
	public String getTargetStatics() {
		return "2.8";
	}

	@Override
	public String getNecessaryUntilSchemaVersion() {
		return "4.19.0016";
	}

	@Override
	public String[] getIgnoringSourceSchemaVersions() {
		return new String[]{};
	}

	private void migrateRoleSubforms() throws AlreadyMigratedException {

		Collection<RigidEO> entityFields = helper.getAll(E_04_19_0016_source.ENTITYFIELD);
		Collection<RigidEO> roleSubformsAll = helper.getAll(E_04_19_0016_source.ROLESUBFORM);
		Collection<RigidEO> roleSubformGroups = helper.getAll(E_04_19_0016_source.ROLESUBFORMGROUP);

		// This filter is not necessary while DB migration, but could be important within a nuclet migration
		Collection<RigidEO> roleSubforms = getRoleSubformsReadWriteNotNull(roleSubformsAll);

		// First step: Migrate ROLESUBFORM from "READWRITE" to "CREATE" and "DELETE"
		for (RigidEO roleSubForm : roleSubformsAll) {
			Boolean readWrite = roleSubForm.getValue(E_04_19_0016_source.ROLESUBFORM.readwrite);
			if (readWrite == null) {
				readWrite = Boolean.FALSE;
			}
			roleSubForm.setValue(E_04_19_0017_target.ROLESUBFORM.create, readWrite);
			roleSubForm.setValue(E_04_19_0017_target.ROLESUBFORM.delete, readWrite);
		}

		helper.updateAll(E_04_19_0017_target.ROLESUBFORM, roleSubforms);

		// Second step: Migrate ROLESUBFORM from "READWRITE" into ROLESUBFORMGROUP for each suform column attribute group
		Map<UID, Collection<UID>>  mpGroups = getGroupsMap(entityFields);
		Collection<Pair<UID, UID>> exstGroupKeys = getSubformGroupKeys(roleSubformGroups);

		for (RigidEO roleSubForm : roleSubforms) {
			UID roleSFUid = (UID)roleSubForm.getPrimaryKey();
			UID sfEntity = roleSubForm.getForeignUID(E_04_19_0016_source.ROLESUBFORM.entity);
			Boolean readWrite = roleSubForm.getValue(E_04_19_0016_source.ROLESUBFORM.readwrite);

			Collection<UID> groups = mpGroups.get(sfEntity);
			if (groups == null) {
				groups = Collections.singleton(null); // This only happens with system subforms like "9nLM" GENERALSEARCHDOCUMENT
			}
			for (UID group : groups) {
				if (exstGroupKeys.contains(new Pair<UID, UID>(roleSFUid, group))) {
					continue; // This already has got explicit rights
				}

				RigidEO rigidEO = new RigidEO(E_04_19_0017_target.ROLESUBFORMGROUP.getUID(), new UID());
				rigidEO.setValue(E_04_19_0017_target.ROLESUBFORMGROUP.readwrite, readWrite);
				rigidEO.setForeignUID(E_04_19_0017_target.ROLESUBFORMGROUP.group, group);
				rigidEO.setForeignUID(E_04_19_0017_target.ROLESUBFORMGROUP.rolesubform, roleSFUid);

				helper.insert(E_04_19_0017_target.ROLESUBFORMGROUP, rigidEO);
			}
		}

	}

	private Collection<RigidEO> getRoleSubformsReadWriteNotNull(Collection<RigidEO> roleSubformsAll) {
		Collection<RigidEO> ret = new ArrayList<>();
		for (RigidEO rsf : roleSubformsAll) {
			if (rsf.getValue(E_04_19_0016_source.ROLESUBFORM.readwrite) != null) {
				ret.add(rsf);
			}
		}
		return ret;
	}

	private Collection<Pair<UID, UID>> getSubformGroupKeys(Collection<RigidEO> roleSubformGroups) {
		Collection<Pair<UID, UID>> ret = new HashSet<>();
		for (RigidEO rsg : roleSubformGroups) {
			UID roleSubForm = rsg.getForeignUID(E_04_19_0016_source.ROLESUBFORMGROUP.rolesubform);
			UID group = rsg.getForeignUID(E_04_19_0016_source.ROLESUBFORMGROUP.group);
			ret.add(new Pair<UID, UID>(roleSubForm, group));
		}
		return ret;
	}

	private Map<UID, Collection<UID>> getGroupsMap(Collection<RigidEO> entityFields) {
		Map<UID, Collection<UID>> ret = new HashMap<>();
		for (RigidEO f : entityFields) {
			UID entity = f.getForeignUID(E_04_19_0016_source.ENTITYFIELD.entity);
			UID group = f.getForeignUID(E_04_19_0016_source.ENTITYFIELD.entityfieldgroup);

			if (!ret.containsKey(entity)) {
				ret.put(entity, new HashSet<UID>());
			}

			ret.get(entity).add(group);
		}
		return ret;
	}

}
