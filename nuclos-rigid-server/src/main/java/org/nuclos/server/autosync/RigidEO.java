//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.DbField;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SFValueable;
import org.nuclos.common.UID;

/**
 * TODO: Make generic, use correct type for primary key.
 */
public class RigidEO {

	private final UID entity;

	private final Object primaryKey;

	private final Map<DbField<Long>, Long> foreignIDs = new HashMap<>();

	private final Map<DbField<UID>, UID> foreignUIDs = new HashMap<>();

	private final Map<DbField<?>, Object> values = new HashMap<>();

	public RigidEO(UID entity, Object primaryKey) {
		super();
		if (entity == null) {
			throw new NuclosFatalException("entity must not be null");
		}
		if (primaryKey == null) {
			throw new NuclosFatalException("primaryKey must not be null");
		}
		this.entity = entity;
		this.primaryKey = primaryKey;
	}

	public UID getEntity() {
		return entity;
	}

	public Object getPrimaryKey() {
		return primaryKey;
	}

	public void setForeignUID(DbField<UID> field, UID value) {
		foreignUIDs.put(field, value);
	}

	public void setForeignID(DbField<Long> field, Long value) {
		foreignIDs.put(field, value);
	}

	public <T> void setValue(FieldMeta.Valueable<T> valueable, T value) {
		values.put(valueable, value);
	}

	public <T> void setValue(SFValueable<T> valueable, T value) {
		values.put(valueable, value);
	}

	public void setValueUNSAFE(DbField<?> valueable, Object value) {
		values.put(valueable, value);
	}

	public UID getForeignUID(DbField<UID> field) {
		return foreignUIDs.get(field);
	}

	public Set<DbField<UID>> getForeignUIDKeys() {
		return foreignUIDs.keySet();
	}

	public Long getForeignID(DbField<Long> field) {
		return foreignIDs.get(field);
	}

	public Set<DbField<Long>> getForeignIDKeys() {
		return foreignIDs.keySet();
	}

	public <T> T getValue(FieldMeta.Valueable<T> valueable) {
		return (T) values.get(valueable);
	}

	public <T> T getValue(SFValueable<T> valueable) {
		return (T) values.get(valueable);
	}

	public Object getValue(DbField<?> valueable) {
		return values.get(valueable);
	}

	public Set<DbField<?>> getValueKeys() {
		return values.keySet();
	}

	@Override
	public int hashCode() {
		return primaryKey.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof RigidEO) {
			return RigidUtils.equal(((RigidEO) obj).getPrimaryKey(), getPrimaryKey());
		}
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return String.format("RigidEO[pk=%s, entity=%s]", primaryKey, entity.getString());
	}
}
