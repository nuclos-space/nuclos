//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SysEntities;
import org.nuclos.common.VersionNumber;
import org.nuclos.common.collection.Pair;
import org.nuclos.server.autosync.migration.AbstractMigration;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.TypeFilter;

public class MigrationUtils {
	
	static final String VERSION_MIG_SUFFIX = "(mig)";
	static final String VERSION_VAL_SUFFIX = "(val)";
	
	public static List<AbstractMigration> getMigrations(String installedVersion) throws Exception {
		BeanDefinitionRegistry bdr = new SimpleBeanDefinitionRegistry();
		ClassPathBeanDefinitionScanner s = new ClassPathBeanDefinitionScanner(bdr, false);
		TypeFilter tf = new AssignableTypeFilter(AbstractMigration.class);
		s.addIncludeFilter(tf);
		s.setIncludeAnnotationConfig(false);
		s.scan("org.nuclos.server.autosync.migration");
		
		List<AbstractMigration> migrations = new ArrayList<AbstractMigration>();
		String[] beans = bdr.getBeanDefinitionNames();
		
		for (String bean : beans) {
			BeanDefinition bd = bdr.getBeanDefinition(bean);
			AbstractMigration migration = (AbstractMigration) Class.forName(bd.getBeanClassName()).newInstance();
			migrations.add(migration);
		}
		
		return selectAndOrder(migrations, getPureVersion(installedVersion));
	}
	
	private static List<AbstractMigration> selectAndOrder(List<AbstractMigration> migrations, String installedVersion) throws Exception {
		List<AbstractMigration> result = new ArrayList<AbstractMigration>();
		if (migrations != null) {
			for (AbstractMigration migration : migrations) {
				final String sourceSchema = migration.getSourceMeta()._getSchemaVersion();
				final String targetSchema = migration.getTargetMeta()._getSchemaVersion();
				if (VersionNumber.compare(installedVersion, migration.getNecessaryUntilSchemaVersion()) <= 0) {
					// check if version has to be ignored
					boolean ignore = false;
					for (String ignoreSourceSchema : migration.getIgnoringSourceSchemaVersions()) {
						if (installedVersion.equals(ignoreSourceSchema)) {
							ignore = true;
							break;
						}
					}
					if (!ignore) {
						// needs to run
						result.add(migration);
					}
				} else if (VersionNumber.compare(installedVersion, sourceSchema) > 0 &&
						   VersionNumber.compare(installedVersion, migration.getNecessaryUntilSchemaVersion()) < 0) {
					// necessary until version <= installed > source version
					throw new NuclosFatalException(String.format("Class %s: Version \"%s\" needs migration to \"%s\", but only accessible until \"%s\".", 
							migration.getClass().getSimpleName(), installedVersion, targetSchema, migration.getNecessaryUntilSchemaVersion()));
				}
			}
		}
		Collections.sort(result, new Comparator<AbstractMigration>() {
			@Override
			public int compare(AbstractMigration m1, AbstractMigration m2) {
				try {
					Pair<String, String> schemaVersions1 = getSchemaVersions(getSysEntities(m1));
					final Pair<String, String> schemaVersions2 = getSchemaVersions(getSysEntities(m2));
					return VersionNumber.compare(schemaVersions1.y, schemaVersions2.y);
				} catch (Exception e) {
					throw new NuclosFatalException(e);
				}
			}
		});
		return result;
	}

	public static Pair<String, String> getSchemaVersions(Pair<SysEntities, SysEntities> sysEntities) throws Exception {
		return new Pair<String, String>(
				sysEntities.x._getSchemaVersion(),
				sysEntities.y._getSchemaVersion());
	}
	
	public static Pair<SysEntities, SysEntities> getSysEntities(AbstractMigration migration) throws Exception {
		return new Pair<SysEntities, SysEntities>(
				migration.getSourceMeta(), 
				migration.getTargetMeta());
	}
	
	public static Pair<String, String> getStaticsVersions(AbstractMigration migration) throws Exception {
		return new Pair<String, String>(migration.getSourceStatics(), migration.getTargetStatics());
	}
	
	public static String getPureVersion(String version) {
		if (version.endsWith(VERSION_MIG_SUFFIX)) {
			version = version.substring(0, version.length()-5);
		}		
		return version;
	}
}
