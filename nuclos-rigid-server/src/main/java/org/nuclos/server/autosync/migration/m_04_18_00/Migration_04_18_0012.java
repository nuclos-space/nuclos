//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.autosync.migration.m_04_18_00;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.logging.log4j.Logger;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.server.autosync.IMigrationHelper;
import org.nuclos.server.autosync.RigidEO;
import org.nuclos.server.autosync.migration.AbstractMigration;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbMap;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.xml.transform.StringSource;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * Mainly:
 * Workspace.TablePreferences -> Preferences (Web) Migration
 * NUCLOS-5884
 *
 * Other:
 * NUCLOS-3244
 */
public class Migration_04_18_0012 extends AbstractMigration {

	private Logger LOG;

	private IMigrationHelper helper;

	private Map<UID, String> mapUserNameByUID = new HashMap<>();

	private Map<UID, String> mapEntityNameByUID = new HashMap<>();

	private Map<UID, String> mapFieldNameByUID = new HashMap<>();

	private Map<UID, String> mapLayoutNameByUID = new HashMap<>();

	private Map<UID, String> mapNucletNameByUID = new HashMap<>();

	private Map<UID, Set<UID>> mapAllLayoutsByEntity = new HashMap<>();

	private Map<UID, DatasourceMetaVO> mapDynamicEntityMeta;

	private Jaxb2Marshaller dsMetaMarshaller = new Jaxb2Marshaller();

	private DbQueryBuilder builder;

	@Override
	public void migrate() {
		if (getContext().isNucletMigration()) {
			return; // no nuclet migration here
		}
		long start = System.currentTimeMillis();
		LOG = getLogger(Migration_04_18_0012.class);
		LOG.info("Migration started");

		helper = getContext().getHelper();
		dsMetaMarshaller.setClassesToBeBound(DatasourceMetaVO.class);

		renamePreferencesType();
		migrateWorkspaces();

		removeSystemColumnCopies();

		LOG.info("Migration finished in " + new DecimalFormat("0.###").format((System.currentTimeMillis()-start)/1000d/60d) + " minutes");
	}

	@Override
	public SysEntities getSourceMeta() {
		return E_04_18_0012.getThis();
	}

	@Override
	public String getSourceStatics() {
		return "2.8";
	}

	@Override
	public SysEntities getTargetMeta() {
		return E_04_18_0012.getThis();
	}

	@Override
	public String getTargetStatics() {
		return "2.8";
	}

	/**
	 * rename "sideviewmenu" prefernces type to "table"
	 */
	private void renamePreferencesType() {
		final DbQueryBuilder builder = helper.getDbAccess().getQueryBuilder();
		final DbMap values = new DbMap();
		values.put(E_04_18_0012.PREFERENCE.type, "table");
		final DbUpdate update = builder.createUpdate(E_04_18_0012.PREFERENCE, values);
		update.where(builder.equalValue(update.baseColumn(E_04_18_0012.PREFERENCE.type), "sideviewmenu"));
		final int countUpdated = helper.getDbAccess().executeUpdate(update);
		LOG.info(countUpdated + " \"sideviewmenu\"-preferences renamed to \"table\" preferences");
	}

	private void migrateWorkspaces() {
		final XStream xstream = new XStream(new StaxDriver());

		xstream.alias("org.nuclos.common.WorkspaceDescription2", WO_WorkspaceDescription2.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Action", WO_WorkspaceDescription2.Action.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$ApiDesktopItem", WO_WorkspaceDescription2.ApiDesktopItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Color", WO_WorkspaceDescription2.Color.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$ColumnPreferences", WO_WorkspaceDescription2.ColumnPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$ColumnSorting", WO_WorkspaceDescription2.ColumnSorting.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Desktop", WO_WorkspaceDescription2.Desktop.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$DesktopItem", WO_WorkspaceDescription2.DesktopItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$EntityPreferences", WO_WorkspaceDescription2.EntityPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Frame", WO_WorkspaceDescription2.Frame.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$LayoutPreferences", WO_WorkspaceDescription2.LayoutPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$MatrixPreferences", WO_WorkspaceDescription2.MatrixPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$MenuButton", WO_WorkspaceDescription2.MenuButton.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$MenuItem", WO_WorkspaceDescription2.MenuItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$MutableContent", WO_WorkspaceDescription2.MutableContent.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$NestedContent", WO_WorkspaceDescription2.NestedContent.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$ProfileManager", WO_WorkspaceDescription2.ProfileManager.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Split", WO_WorkspaceDescription2.Split.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$SubFormPreferences", WO_WorkspaceDescription2.SubFormPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Tab", WO_WorkspaceDescription2.Tab.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Tabbed", WO_WorkspaceDescription2.Tabbed.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$TablePreferences", WO_WorkspaceDescription2.TablePreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$TasklistPreferences", WO_WorkspaceDescription2.TasklistPreferences.class);

		xstream.alias("org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator", WO_ComparisonOperator.class);

		builder = helper.getDbAccess().getQueryBuilder();

		final DbQuery<UID> qAssignableWorkspaces = builder.createQuery(UID.class);
		final DbFrom<UID> fromAssignableWorkspaces = qAssignableWorkspaces.from(E_04_18_0012.WORKSPACE);
		qAssignableWorkspaces.select(fromAssignableWorkspaces.basePk());
		qAssignableWorkspaces.where(builder.isNull(E_04_18_0012.WORKSPACE.user));
		qAssignableWorkspaces.orderBy(builder.desc(fromAssignableWorkspaces.baseColumn(SF.CHANGEDAT)));
		final List<UID> allAssignableWorkspaces = helper.getDbAccess().executeQuery(qAssignableWorkspaces);
		for (UID wsUID : allAssignableWorkspaces) {
			final RigidEO eo = helper.getByPrimaryKey(E_04_18_0012.WORKSPACE, wsUID);
			final UID nucletUID = eo.getForeignUID(E_04_18_0012.WORKSPACE.nuclet);
			LOG.info(String.format("Migrate shared workspace \"%s\"", eo.getValue(E_04_18_0012.WORKSPACE.name)));
			try {
				final String wd2String = eo.getValue(E_04_18_0012.WORKSPACE.clbworkspace);
				final Object wd2Object = xstream.fromXML(wd2String);
				WO_WorkspaceDescription2 wd2 = (WO_WorkspaceDescription2) wd2Object;
				final Set<UID> prefsCreated = new HashSet<UID>();
				migrateWorkspaceTablePreferences(wd2, true, nucletUID, null,
						false, prefsCreated);
				eo.setValue(E_04_18_0012.WORKSPACE.clbworkspace, xstream.toXML(wd2));
				helper.update(E_04_18_0012.WORKSPACE, eo);

				// copy sharing with roles
				final DbQuery<UID> qRoles = builder.createQuery(UID.class);
				final DbFrom<UID> fromRoles = qRoles.from(E_04_18_0012.ROLEWORKSPACE);
				qRoles.select(fromRoles.baseColumn(E_04_18_0012.ROLEWORKSPACE.role));
				qRoles.where(builder.equalValue(fromRoles.baseColumn(E_04_18_0012.ROLEWORKSPACE.workspace), wsUID));
				final List<UID> roleUIDlst = helper.getDbAccess().executeQuery(qRoles);
				for (UID roleUID : roleUIDlst) {
					for (UID prefUID : prefsCreated) {
						final RigidEO rolePrefEO = new RigidEO(E_04_18_0012.ROLEPREFERENCE.getUID(), new UID());
						rolePrefEO.setForeignUID(E_04_18_0012.ROLEPREFERENCE.role, roleUID);
						rolePrefEO.setForeignUID(E_04_18_0012.ROLEPREFERENCE.preference, prefUID);
						helper.insert(E_04_18_0012.ROLEPREFERENCE, rolePrefEO);
					}
				}
			} catch (Exception ex) {
				LOG.error(String.format("Error during migration of shared workspace \"%s\": %s", eo.getValue(E_04_18_0012.WORKSPACE.name), ex.getMessage()), ex);
				LOG.warn(String.format("Removing shared workspace..."));
				removeWorkspace(wsUID);
			}
		}

		final DbQuery<UID> qUserWorkspaces = builder.createQuery(UID.class);
		final DbFrom<UID> fromUserWorkspaces = qUserWorkspaces.from(E_04_18_0012.WORKSPACE);
		qUserWorkspaces.select(fromUserWorkspaces.basePk());
		qUserWorkspaces.where(builder.isNotNull(E_04_18_0012.WORKSPACE.user));
		qUserWorkspaces.orderBy(builder.desc(fromAssignableWorkspaces.baseColumn(SF.CHANGEDAT)));
		final List<UID> allUserWorkspaces = helper.getDbAccess().executeQuery(qUserWorkspaces);
		for (UID wsUID : allUserWorkspaces) {
			final RigidEO eo = helper.getByPrimaryKey(E_04_18_0012.WORKSPACE, wsUID);
			final UID assignedWorkspace = eo.getForeignUID(E_04_18_0012.WORKSPACE.assignedWorkspace);
			final boolean dropOnlyTablePreferences = assignedWorkspace != null;
			final UID userUID = eo.getForeignUID(E_04_18_0012.WORKSPACE.user);
			final String username = getUserNameByUID(userUID);
			LOG.info(String.format("Migrate user workspace \"%s\" for user %s", eo.getValue(E_04_18_0012.WORKSPACE.name), username));
			try {
				final String wd2String = eo.getValue(E_04_18_0012.WORKSPACE.clbworkspace);
				final Object wd2Object = xstream.fromXML(wd2String);
				WO_WorkspaceDescription2 wd2 = (WO_WorkspaceDescription2) wd2Object;
				migrateWorkspaceTablePreferences(wd2, false, null, userUID,
						dropOnlyTablePreferences, new HashSet<UID>());
				eo.setValue(E_04_18_0012.WORKSPACE.clbworkspace, xstream.toXML(wd2));
				helper.update(E_04_18_0012.WORKSPACE, eo);
			} catch (Exception ex) {
				LOG.error(String.format("Error during migration of user workspace \"%s\": %s", eo.getValue(E_04_18_0012.WORKSPACE.name), ex.getMessage()), ex);
				LOG.warn(String.format("Removing user workspace..."));
				removeWorkspace(wsUID);
			}
		}
	}

	private void migrateWorkspaceTablePreferences(final WO_WorkspaceDescription2 wd2, boolean shared, final UID nucletUID,
												  final UID userUID, final boolean dropOnly, final Set<UID> prefsCreated) {
		final List<WO_WorkspaceDescription2.EntityPreferences> ePrefLst = wd2.getEntityPreferences();
		for (WO_WorkspaceDescription2.EntityPreferences ePref : ePrefLst) {
			if (ePref.getEntity() == null) {
				// no entity ?! what is it?
				ePref.dropProfiles();
				ePref.dropSubformPrefs();
				continue;
			}
			if (!dropOnly) {
				migrateWorkspaceTablePreferenceList(shared ?
								ePref.getProfiles() : ePref.getPersonalProfiles(),
						"table", ePref.getEntity(), nucletUID, new HashSet<UID>(), userUID, prefsCreated);
			}
			ePref.dropProfiles();

			final Set<UID> allLayouts = getAllLayoutsByEntity(ePref.getEntity());
			final List<WO_WorkspaceDescription2.SubFormPreferences> subPrefLst = ePref.getSubFormPreferences();
			for (WO_WorkspaceDescription2.SubFormPreferences subPref : subPrefLst) {
				if (!dropOnly) {
					migrateWorkspaceTablePreferenceList(shared ? subPref.getProfiles() : subPref.getPersonalProfiles(),
							"subform-table", subPref.getEntity(), nucletUID, allLayouts, userUID, prefsCreated);
				}
				subPref.dropProfiles();
			}
			ePref.dropSubformPrefs();
		}

		// no migration of tasklists cause of missing UIDs
		wd2.dropTasklistPreferences();
	}

	private void migrateWorkspaceTablePreferenceList(final List<WO_WorkspaceDescription2.TablePreferences> tpLst, final String prefType,
													 final UID entityUID, final UID nucletUID, final Set<UID> allLayouts,
													 final UID userUID, final Set<UID> prefsCreated) {
		for (WO_WorkspaceDescription2.TablePreferences tp : tpLst) {
			migrateWorkspaceTablePreference(tp, prefType, entityUID, nucletUID, allLayouts, userUID, null, null, prefsCreated);
		}
	}

	private void migrateWorkspaceTablePreference(final WO_WorkspaceDescription2.TablePreferences tp, final String prefType,
												 final UID entityUID, final UID nucletUID, final Set<UID> allLayouts,
												 final UID userUID, final UID taskListUID, final UID searchFilterUID, final Set<UID> prefsCreated) {
		if (allLayouts.isEmpty()) {
			allLayouts.add(UID.UID_NULL);
		}

		for (UID layoutUID : allLayouts) {
			layoutUID = layoutUID.equals(UID.UID_NULL) ? null : layoutUID;
			final RigidEO vo = new RigidEO(E_04_18_0012.PREFERENCE.getUID(), new UID());
			final String name = RigidUtils.defaultIfNull(tp.getName(), "Default");

			vo.setValue(E_04_18_0012.PREFERENCE.app, "nuclos");
			vo.setValue(E_04_18_0012.PREFERENCE.type, prefType);
			vo.setValue(E_04_18_0012.PREFERENCE.name, name);
			vo.setForeignUID(E_04_18_0012.PREFERENCE.entity, entityUID);
			vo.setForeignUID(E_04_18_0012.PREFERENCE.layout, layoutUID);
			vo.setForeignUID(E_04_18_0012.PREFERENCE.nuclet, nucletUID);
			vo.setForeignUID(E_04_18_0012.PREFERENCE.user, userUID);

			LOG.info(String.format("Creating %s-preference with name \"%s\" for user \"%s\": entity=\"%s\", layout=\"%s\", nuclet=\"%s\"...",
					prefType, name,
					logUIDUnknownIfResultNull(userUID, getUserNameByUID(userUID)),
					logUIDUnknownIfResultNull(entityUID, getEntityNameByUID(entityUID)),
					logUIDUnknownIfResultNull(layoutUID, getLayoutNameByUID(layoutUID)),
					logUIDUnknownIfResultNull(nucletUID, getNucletNameByUID(nucletUID))));

			final JsonObjectBuilder json = Json.createObjectBuilder();
			json.add("userdefinedName", true);
			if (taskListUID != null) {
				json.add("taskListId", taskListUID.getStringifiedDefinitionWithEntity(E_04_18_0012.TASKLIST));
			}
			if (searchFilterUID != null) {
				json.add("searchFilterId", searchFilterUID.getStringifiedDefinitionWithEntity(E_04_18_0012.SEARCHFILTER));
			}

			final JsonArrayBuilder jsonColumns = Json.createArrayBuilder();

			final List<WO_WorkspaceDescription2.ColumnSorting> columnSortings = tp.getColumnSortings();
			final List<WO_WorkspaceDescription2.ColumnPreferences> selectedColumnPreferences = tp.getSelectedColumnPreferences();

			for (int i = 0; i < selectedColumnPreferences.size(); i++) {
				final WO_WorkspaceDescription2.ColumnPreferences cp = selectedColumnPreferences.get(i);
				final JsonObjectBuilder jsonColumn = Json.createObjectBuilder();
				final UID entityOfColumnUID = cp.getEntity()!=null ? cp.getEntity() : entityUID;
				String fieldName = null;
				if (entityUID != null) {
					fieldName = getFieldNameByUID(entityOfColumnUID, cp.getColumn());
					if (fieldName == null) {
						// ignore old / unknown attributes
						continue;
					}
				}
				jsonColumn.add("name", fieldName != null ? fieldName : cp.getColumn().getString());
				if (fieldName != null) {
					jsonColumn.add("boAttrId", cp.getColumn().getStringifiedDefinitionWithEntity(E_04_18_0012.ENTITYFIELD));
				} else {
					jsonColumn.add("column", cp.getColumn().getString());
				}
				if (cp.getEntity() != null && !cp.getEntity().equals(entityUID)) {
					jsonColumn.add("boMetaId", cp.getEntity().getStringifiedDefinitionWithEntity(E_04_18_0012.ENTITY));
				}
				jsonColumn.add("width", cp.getWidth());
				if (cp.isFixed()) {
					jsonColumn.add("fixed", true);
				}
				jsonColumn.add("position", i);
				readColumnSortFromPreferences(jsonColumn, cp.getColumn(), columnSortings);
				readFilterFromPreferences(jsonColumn, cp);
				jsonColumns.add(jsonColumn);
			}
			json.add("columns", jsonColumns);

			final JsonObject jsonObject = json.build();

			vo.setValue(E_04_18_0012.PREFERENCE.json, jsonObject);
			helper.insert(E_04_18_0012.PREFERENCE, vo);
			prefsCreated.add((UID) vo.getPrimaryKey());

			if (tp.isActive() && userUID != null) {
				// add selection, but only if no selection is present

				final DbQuery<Long> qSelected = builder.createQuery(Long.class);
				final DbFrom<UID> fromSelected = qSelected.from(E_04_18_0012.PREFERENCE_SELECTED);
				qSelected.select(builder.countRows());
				qSelected.where(builder.equalValue(fromSelected.baseColumn(E_04_18_0012.PREFERENCE_SELECTED.app), "nuclos"));
				qSelected.addToWhereAsAnd(builder.equalValue(fromSelected.baseColumn(E_04_18_0012.PREFERENCE_SELECTED.type), prefType));
				qSelected.addToWhereAsAnd(builder.equalValue(fromSelected.baseColumn(E_04_18_0012.PREFERENCE_SELECTED.user), userUID));
				qSelected.addToWhereAsAnd(entityUID != null ?
						builder.equalValue(fromSelected.baseColumn(E_04_18_0012.PREFERENCE_SELECTED.entity), entityUID) :
						builder.isNull(fromSelected.baseColumn(E_04_18_0012.PREFERENCE_SELECTED.entity)));
				qSelected.addToWhereAsAnd(layoutUID != null ?
						builder.equalValue(fromSelected.baseColumn(E_04_18_0012.PREFERENCE_SELECTED.layout), layoutUID) :
						builder.isNull(fromSelected.baseColumn(E_04_18_0012.PREFERENCE_SELECTED.layout)));
				final Long count = helper.getDbAccess().executeQuerySingleResult(qSelected);

				if (count != null && count.longValue() == 0l) {
					LOG.info(String.format("Selecting %s-preference with name \"%s\" for user \"%s\": entity=\"%s\", layout=\"%s\"...",
							prefType, name,
							logUIDUnknownIfResultNull(userUID, getUserNameByUID(userUID)),
							logUIDUnknownIfResultNull(entityUID, getEntityNameByUID(entityUID)),
							logUIDUnknownIfResultNull(layoutUID, getLayoutNameByUID(layoutUID))));

					final RigidEO voSelected = new RigidEO(E_04_18_0012.PREFERENCE_SELECTED.getUID(), new UID());
					voSelected.setValue(E_04_18_0012.PREFERENCE_SELECTED.app, "nuclos");
					voSelected.setValue(E_04_18_0012.PREFERENCE_SELECTED.type, prefType);
					voSelected.setForeignUID(E_04_18_0012.PREFERENCE_SELECTED.user, userUID);
					voSelected.setForeignUID(E_04_18_0012.PREFERENCE_SELECTED.entity, entityUID);
					voSelected.setForeignUID(E_04_18_0012.PREFERENCE_SELECTED.layout, layoutUID);
					voSelected.setForeignUID(E_04_18_0012.PREFERENCE_SELECTED.selectedPreference, (UID) vo.getPrimaryKey());
					helper.insert(E_04_18_0012.PREFERENCE_SELECTED, voSelected);
				}
			}
		}
	}

	private void readFilterFromPreferences(final JsonObjectBuilder jsonColumn, final WO_WorkspaceDescription2.ColumnPreferences cp) {
		final Object value = cp.getColumnFilter();
		try {
			if (value != null) {
				if (value instanceof Integer) {
					jsonColumn.add("filterValueInteger", String.valueOf(value));
				} else if (value instanceof Long) {
					jsonColumn.add("filterValueLong", String.valueOf(value));
				} else if (value instanceof Double) {
					jsonColumn.add("filterValueDouble", String.valueOf(value));
				} else if (value instanceof String) {
					jsonColumn.add("filterValueString", String.valueOf(value));
				} else if (value instanceof Boolean) {
					jsonColumn.add("filterValueBoolean", String.valueOf(value));
				} else if (value instanceof Date) {
					jsonColumn.add("filterValueDate", new SimpleDateFormat("yyyy-MM-dd").format(value));
				} else {
					LOG.warn("Filter \"" + value + "\" for column " + cp.getColumn() + " not stored in preferences. Class " + value.getClass().getCanonicalName() + " is not supported!");
					return;
				}
				jsonColumn.add("filterOp", cp.getColumnFilterOp().name());
			}
		} catch (Exception ex) {
			LOG.error("Error writing column filter value [" + value + "] to prefs: " + ex.getMessage(), ex);
		}
	}

	private void readColumnSortFromPreferences(final JsonObjectBuilder jsonColumn, UID columnUID, List<WO_WorkspaceDescription2.ColumnSorting> columnSortings) {
		for (int i = 0; i< columnSortings.size(); i++) {
			WO_WorkspaceDescription2.ColumnSorting sort = columnSortings.get(i);
			if (sort.getColumn().equals(columnUID)) {
				final JsonObjectBuilder jsonSort = Json.createObjectBuilder();
				jsonSort.add("direction", sort.isAsc() ? "asc" : "desc");
				jsonSort.add("prio", i + 1);
				jsonColumn.add("sort", jsonSort);
			}
		}
	}

	private void removeWorkspace(UID workspaceUID) {
		final DbQueryBuilder builder = helper.getDbAccess().getQueryBuilder();

		final DbDelete deleteAssigned = builder.createDelete(E_04_18_0012.WORKSPACE);
		deleteAssigned.where(builder.equalValue(deleteAssigned.baseColumn(E_04_18_0012.WORKSPACE.assignedWorkspace), workspaceUID));
		helper.getDbAccess().executeDelete(deleteAssigned);

		final DbDelete deleteRoleWorkspace = builder.createDelete(E_04_18_0012.ROLEWORKSPACE);
		deleteRoleWorkspace.where(builder.equalValue(deleteRoleWorkspace.baseColumn(E_04_18_0012.ROLEWORKSPACE.workspace), workspaceUID));
		helper.getDbAccess().executeDelete(deleteRoleWorkspace);

		final DbDelete delete = builder.createDelete(E_04_18_0012.WORKSPACE);
		delete.where(builder.equalValue(delete.baseColumn(SF.PK_UID), workspaceUID));
		helper.getDbAccess().executeDelete(delete);
	}

	private Collection<DatasourceMetaVO> getAllDynamicDatasourceMeta() {
		Map<UID, DatasourceMetaVO> result = mapDynamicEntityMeta;
		if (result == null) {
			final Collection<RigidEO> allDynEoLst = helper.getAll(E_04_18_0012.DYNAMICENTITY);
			result = new HashMap<>();
			for (RigidEO dynEo : allDynEoLst) {
				final String sMeta = dynEo.getValue(E_04_18_0012.DYNAMICENTITY.meta);
				final DatasourceMetaVO metaVO = (DatasourceMetaVO) dsMetaMarshaller.unmarshal(new StringSource(sMeta));
				result.put((UID) dynEo.getPrimaryKey(), metaVO);
			}
			mapDynamicEntityMeta = result;
		}
		return mapDynamicEntityMeta.values();
	}

	private Set<UID> getAllLayoutsByEntity(UID entityUID) {
		Set<UID> result = new HashSet<>();
		if (!mapAllLayoutsByEntity.containsKey(entityUID)) {
			if (entityUID != null) {
				final DbQueryBuilder builder = helper.getDbAccess().getQueryBuilder();
				final DbQuery<UID> q = builder.createQuery(UID.class);
				final DbFrom<UID> from = q.from(E_04_18_0012.LAYOUTUSAGE);
				q.select(from.baseColumn(E_04_18_0012.LAYOUTUSAGE.layout)).distinct(true);
				q.where(builder.equalValue(from.baseColumn(E_04_18_0012.LAYOUTUSAGE.entity), entityUID));
				final List<UID> uids = helper.getDbAccess().executeQuery(q);
				result.addAll(uids);
			}
			mapAllLayoutsByEntity.put(entityUID, result);
		} else {
			result.addAll(mapAllLayoutsByEntity.get(entityUID));
		}
		// new set (changed from caller)
		return new HashSet<>(result);
	}

	private String logUIDUnknownIfResultNull(UID uid, String result) {
		if (uid == null || uid.equals(UID.UID_NULL)) {
			return null;
		} else {
			if (result == null) {
				return "Unkown-" + uid.getString();
			} else {
				return result;
			}
		}
	}

	private String getEntityNameByUID(UID entityUID) {
		if (entityUID == null || entityUID.equals(UID.UID_NULL)) {
			return null;
		}
		String result = null;
		if (!mapEntityNameByUID.containsKey(entityUID)) {
			if (E_04_18_0012.isNuclosEntity(entityUID)) {
				result = E_04_18_0012.getByUID(entityUID).getEntityName();
			} else if (entityUID.getString().startsWith("YxQw-")) {
				// dynamic entity
				try {
					UID dynamicDatasourceUID = new UID (entityUID.getString().substring(5));
					RigidEO eo = helper.getByPrimaryKey(E_04_18_0012.DYNAMICENTITY, dynamicDatasourceUID);
					result = eo == null ? null : (eo.getValue(E_04_18_0012.DYNAMICENTITY.name)+"DYN");
				} catch (DbInvalidResultSizeException ex) {
					// does not exist any more... ok
				}
			} else {
				try {
					RigidEO eo = helper.getByPrimaryKey(E_04_18_0012.ENTITY, entityUID);
					result = eo == null ? null : eo.getValue(E_04_18_0012.ENTITY.entity);
				} catch (DbInvalidResultSizeException ex) {
					// does not exist any more... ok
				}
			}
			mapEntityNameByUID.put(entityUID, result);
		} else {
			result = mapEntityNameByUID.get(entityUID);
		}
		return result;
	}

	private String getFieldNameByUID(UID entityUID, UID fieldUID) {
		if (fieldUID == null || fieldUID.equals(UID.UID_NULL)) {
			return null;
		}
		String result = null;
		if (!mapFieldNameByUID.containsKey(fieldUID)) {
			if (entityUID != null && SF.isEOField(entityUID, fieldUID)) {
				result = SF.getEOField(entityUID, fieldUID).getFieldName();
			} else if (fieldUID.getString().startsWith("YxQwf-")) {
				// dynamic entity field. Search over dsmeta
				for (DatasourceMetaVO dynMeta : getAllDynamicDatasourceMeta()) {
					if (result != null) {
						break;
					}
					for (DatasourceMetaVO.ColumnMeta colMeta : dynMeta.getColumns()) {
						if (colMeta.getFieldUID().equals(fieldUID)) {
							result = colMeta.getColumnName();
							break;
						}
					}
				}
			} else {
				try {
					RigidEO eo = helper.getByPrimaryKey(E_04_18_0012.ENTITYFIELD, fieldUID);
					result = eo == null ? null : eo.getValue(E_04_18_0012.ENTITYFIELD.field);
				} catch (DbInvalidResultSizeException ex) {
					// does not exist any more ... ok
				}
			}
			mapFieldNameByUID.put(fieldUID, result);
		} else {
			result = mapFieldNameByUID.get(fieldUID);
		}
		return result;
	}

	private String getUserNameByUID(UID userUID) {
		if (userUID == null || userUID.equals(UID.UID_NULL)) {
			return null;
		}
		final String result;
		if (!mapUserNameByUID.containsKey(userUID)) {
			RigidEO eo = helper.getByPrimaryKey(E_04_18_0012.USER, userUID);
			result = eo==null?null: eo.getValue(E_04_18_0012.USER.name);
			mapUserNameByUID.put(userUID, result);
		} else {
			result = mapUserNameByUID.get(userUID);
		}
		return result;
	}

	private String getLayoutNameByUID(UID layoutUID) {
		if (layoutUID == null || layoutUID.equals(UID.UID_NULL)) {
			return null;
		}
		final String result;
		if (!mapLayoutNameByUID.containsKey(layoutUID)) {
			RigidEO eo = helper.getByPrimaryKey(E_04_18_0012.LAYOUT, layoutUID);
			result = eo==null?null: eo.getValue(E_04_18_0012.LAYOUT.name);
			mapLayoutNameByUID.put(layoutUID, result);
		} else {
			result = mapLayoutNameByUID.get(layoutUID);
		}
		return result;
	}

	private String getNucletNameByUID(UID nucletUID) {
		if (nucletUID == null || nucletUID.equals(UID.UID_NULL)) {
			return null;
		}
		final String result;
		if (!mapNucletNameByUID.containsKey(nucletUID)) {
			RigidEO eo = helper.getByPrimaryKey(E_04_18_0012.NUCLET, nucletUID);
			result = eo==null?null: eo.getValue(E_04_18_0012.NUCLET.name);
			mapNucletNameByUID.put(nucletUID, result);
		} else {
			result = mapNucletNameByUID.get(nucletUID);
		}
		return result;
	}

	/**
	 * NUCLOS-3244
	 */
	private void removeSystemColumnCopies() {
		final Collection<RigidEO> allFields = helper.getAll(E_04_18_0012.ENTITYFIELD);
		for (RigidEO eo : allFields) {
			final Object pk = eo.getPrimaryKey();
			final String value = eo.getValue(E_04_18_0012.ENTITYFIELD.field);
			final UID entityUID = eo.getForeignUID(E_04_18_0012.ENTITYFIELD.entity);
			if (SF.isEOField(entityUID, (UID) pk)) {
				// system field in Nuclet...
				LOG.info("Removing system field " + value + " ( " + pk + " ) from Nuclet");
				helper.delete(E_04_18_0012.ENTITYFIELD, eo);
			}
		}
	}
}
