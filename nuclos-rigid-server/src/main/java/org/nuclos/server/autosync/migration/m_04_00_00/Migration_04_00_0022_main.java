package org.nuclos.server.autosync.migration.m_04_00_00;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.internal.util.Base64;
import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NucletConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dblayer.IFieldRef;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common2.ForeignEntityFieldParser;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.autosync.ConstraintHelper;
import org.nuclos.server.autosync.IMigrationHelper;
import org.nuclos.server.autosync.IndexHelper;
import org.nuclos.server.autosync.NotSupportedException;
import org.nuclos.server.autosync.RigidEO;
import org.nuclos.server.autosync.migration.AbstractMigration;
import org.nuclos.server.autosync.migration.AbstractMigration.Migration;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.CustomComponent3;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.CustomComponent4;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.GOCollect3;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.GOCollect4;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.MDCollect3;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.MDCollect4;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.NuclosCollect3;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.NuclosCollect4;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.Task3;
import org.nuclos.server.autosync.migration.m_04_00_00.WO_RestorePreferences.Task4;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SystemMetaProvider;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dblayer.DbObjectHelper;
import org.nuclos.server.dblayer.MetaDbEntityWrapper;
import org.nuclos.server.dblayer.MetaDbFieldWrapper;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbPlainStatement;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbNamedObject;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbSimpleView;
import org.nuclos.server.dblayer.structure.DbTableType;
import org.nuclos.server.dblayer.util.DbObjectUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.io.xml.StaxDriver;

@Migration
public class Migration_04_00_0022_main extends AbstractMigration {

	@Override
	public SysEntities getSourceMeta() {
		return E_04_00_0022_pre.getThis();
	}

	@Override
	public String getSourceStatics() {
		return "1.9";
	}

	@Override
	public SysEntities getTargetMeta() {
		return E_04_00_0022_main.getThis();
	}

	@Override
	public String getTargetStatics() {
		return "2.0";
	}
	
	@Override
	public String getNecessaryUntilSchemaVersion() {
		return "3.99.9999";
	}

	private Logger LOG;
	
	/**
	 * the system identifier (URI) for the LayoutML DTD.
	 */
	public static final String LAYOUTML_DTD_SYSTEMIDENTIFIER = "http://www.novabit.de/technologies/layoutml/layoutml.dtd";

	/**
	 * the resource path of the LayoutML DTD
	 */
	public static final String LAYOUTML_DTD_RESSOURCEPATH = "resources/mig_04_00_00/mig_layoutml.dtd";
	
	/**
	 * the system identifier (URI) for the LayoutML DTD.
	 */
	public static final String QUERYBUILDERMODEL_DTD_SYSTEMIDENTIFIER = "http://www.novabit.de/technologies/querybuilder/querybuildermodel.dtd";

	/**
	 * the resource path of the LayoutML DTD
	 */
	public static final String QUERYBUILDERMODEL_DTD_RESSOURCEPATH = "resources/mig_04_00_00/mig_querybuildermodel.dtd";

	private static final byte[] EMPTY_IMAGE = Base64.decode("R0lGODlhAQABAPAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==".getBytes());
	
	private static final String NUCLET_PREFERRED_LOCAL_IDENTIFIER = "migration_04_00_00.nuclet.preferred.local.identifier=";
	
	private static final int THREAD_POOL_SIZE = NuclosSystemParameters.getInteger(NuclosSystemParameters.MIGRATION_THREAD_POOL_SIZE, 20);
	
	private IMigrationHelper helper;
	
	private ConcurrentMap<Pair<String, Long>, Pair<UID, Integer>> nucletContentMap;
	private Map<Long, RigidEO> entitiesById;
	private Map<String, RigidEO> entitiesByName;
	private Map<String, Long> entityIdsByName;
	private Map<UID, RigidEO> entitiesByUID;
	private Map<Long, UID> fieldsById;
	
	private Map<Pair<String,String>, RigidEO> _fields;
	private ConcurrentMap<Pair<FieldMeta<?>, Object>, UID> _refValueCache = new ConcurrentHashMap<Pair<FieldMeta<?>, Object>, UID>();
	
	/**
	 * local identifier -> old namespace (if exist)
	 */
	private Map<String, String> namespaceByLocalIdent = new HashMap<String, String>();
	private Map<UID, String> localIdentifier = new HashMap<UID, String>();
	
	@Override
	public void migrate() {
		if (getContext().isNucletMigration()) {
			return; // no nuclet migration here
		}
		long start = System.currentTimeMillis();
		LOG = getLogger(Migration_04_00_0022_main.class);
		LOG.info("Migration started");
		
		helper = getContext().getHelper();
		
		preValidation();
		
		createNucletContentMap();
		migrateMetaData();
		dropViews();
		
		manageDbObjects(0);
		removeUserDefinedConstraintsAndIndices();
		
		migratePrimaryKeys();
		migrateForeignKeys();
		migrateForeignFields();
		migrateImportAttributes();
		migrateLocaleResourceTreeViews();
		migrateEntitySubNodes();
		migrateLogBook();
		
		migrateLocalIdentifiers();
		manageDbObjects(1);
		migrateDbStatics();
		migrateUserDefinedForeignKeys();
		
		// XML content migration
		migrateLayouts();
		migrateDatasources();
		migrateSearchfilters();
		migrateWorkspaces();
		migrateResPlans();
		
		migrateResources();
		
		resetUserPreferences();
		resetStateModelLayouts();
		
		LOG.info("Migration finished in " + new DecimalFormat("0.###").format((System.currentTimeMillis()-start)/1000d/60d) + " minutes");
	}
	
	private String releaseNote(String key) {
		return (String.format("(See also http://wiki.nuclos.de/display/Installationsanleitung/Hinweise+zu+Nuclos+4.0.0 : %s)", key));
	}
	
	private void logTitle(String title) {
		LOG.info(String.format("<<<<<<<<   %-30s   >>>>>>>>", title));
	}
	
	private void preValidation() {
		logTitle(String.format("pre validation"));
		final Set<String> uniquePackage = new HashSet<String>();
		final Set<String> uniqueName = new HashSet<String>();
		for (RigidEO nuclet : helper.getAll(E_03_15_0004.NUCLET)) {
			String sPackage = nuclet.getValue(E_03_15_0004.NUCLET.packagefield);
			String sName = nuclet.getValue(E_03_15_0004.NUCLET.name);
			if (RigidUtils.looksEmpty(sPackage)) {
				throw new IllegalArgumentException(
						String.format("nuclet %s: Package is NULL! " + releaseNote("MIG-06"), nuclet.getPrimaryKey()));
			}
			if (RigidUtils.looksEmpty(sName)) {
				throw new IllegalArgumentException(
						String.format("nuclet %s: Name is NULL! " + releaseNote("MIG-06"), nuclet.getPrimaryKey()));
			}
			if (uniquePackage.contains(sPackage)) {
				throw new IllegalArgumentException(
						String.format("nuclet \"%s\": Package \"%s\" is not unique! " + releaseNote("MIG-06"), sName, sPackage));
			}
			if (uniqueName.contains(sName)) {
				throw new IllegalArgumentException(
						String.format("nuclet \"%s\": Name \"%s\" is not unique! " + releaseNote("MIG-06"), sPackage, sName));
			}
			uniquePackage.add(sPackage);
			uniqueName.add(sName);
		}
		for (RigidEO eoEntity : helper.getAll(E_03_15_0004.ENTITY)) {
			String entity = eoEntity.getValue(E_03_15_0004.ENTITY.entity);
			String virtualentity = eoEntity.getValue(E_03_15_0004.ENTITY.virtualentity);
			if (RigidUtils.looksEmpty(virtualentity)) {
				String tablename = MetaDbHelper.getTableName(
						virtualentity,
						eoEntity.getValue(E_03_15_0004.ENTITY.dbentity)).toUpperCase();
				EntityMeta<?> systemEntity = E_03_15_0004.getAllEntities().stream().filter(em ->
						tablename.equals(MetaDbHelper.getTableName(
								em.getVirtualEntity(),
								em.getDbTable()).toUpperCase())).findFirst().orElse(null);
				if (systemEntity != null) {
					throw new IllegalArgumentException(String.format("entity \"%s\" uses a system table \"%s\", please change to virtual entity.", entity, tablename));
				}
			}
		}
		if (!helper.getAllPks(E_03_15_0004.RULE).isEmpty()) {
			LOG.warn(String.format("At least one old rule found and removed! All old rules were replaced by new API rules? " + releaseNote("Entfall alter Regeln")));
		}
		if (!helper.getAllPks(E_03_15_0004.TIMELIMITRULE).isEmpty()) {
			LOG.warn(String.format("At least one old time limit rule found and removed! All old rules were replaced by new API rules? " + releaseNote("Entfall alter Regeln")));
		}
		if (!helper.getAllPks(E_03_15_0004.CODE).isEmpty()) {
			LOG.warn(String.format("At least one old library rule found and removed! All old rules were replaced by new API rules? " + releaseNote("Entfall alter Regeln")));
		}
	}

	private void createNucletContentMap() {
		logTitle(String.format("create nuclet content map"));
		nucletContentMap = new ConcurrentHashMap<Pair<String,Long>, Pair<UID, Integer>>();
		Collection<RigidEO> nucletContentUIDs = helper.getAll(E_03_15_0004.NUCLETCONTENTUID);
		for (RigidEO ncuid : nucletContentUIDs) {
			final String entity = ncuid.getValue(E_03_15_0004.NUCLETCONTENTUID.nuclosentity);
			final Long id = ncuid.getValue(E_03_15_0004.NUCLETCONTENTUID.objectid);
			final UID uid = new UID(ncuid.getValue(E_03_15_0004.NUCLETCONTENTUID.uid));
			final Integer importVersion = ncuid.getValue(E_03_15_0004.NUCLETCONTENTUID.objectversion);
			nucletContentMap.put(new Pair<String, Long>(entity, id), new Pair<UID, Integer>(uid, importVersion));
		}
	}
	
	private <T> void migrateMetaData() {
		logTitle(String.format("migrate meta data"));
		// Entities first
		entitiesById = new HashMap<Long, RigidEO>();
		entitiesByName = new HashMap<String, RigidEO>();
		entityIdsByName = new HashMap<String, Long>();
		entitiesByUID = new HashMap<UID, RigidEO>();
		for (RigidEO old : helper.getAll(E_03_15_0004.ENTITY)) {
			final Long oldPk = (Long) old.getPrimaryKey();
			final UID pk = getUID(E_03_15_0004.ENTITY.getEntityName(), oldPk);
			final RigidEO entityNew = new RigidEO(E_03_15_0004.ENTITY.getUID(), pk);
			final String entityName = old.getValue(E_04_00_0022_finish.ENTITY.entity);
			for (DbField<Long> key : old.getForeignIDKeys()) {
				entityNew.setForeignID(key, old.getForeignID(key));
			}
			for (DbField<?> key : old.getValueKeys()) {
				final Object value = old.getValue(key);
				if (value != null) {
					if (E_03_15_0004.ENTITY.dbentity.equals(key)) {
						entityNew.setValue(E_04_00_0022_finish.ENTITY.dbtable, (String)value);
					}
				}
				
				entityNew.setValueUNSAFE(key, value);
			}
			entitiesById.put(oldPk, entityNew);
			entitiesByName.put(entityName, entityNew);
			entityIdsByName.put(entityName, oldPk);
			entitiesByUID.put(pk, entityNew);
			
			if (old.getValue(E_03_15_0004.ENTITY.showsearch) == null) {
				old.setValue(E_03_15_0004.ENTITY.showsearch, true);
			}
			
			helper.update(E_03_15_0004.ENTITY, old);
		}
		
		// Fields
		fieldsById = new HashMap<Long, UID>();
		_fields = new HashMap<Pair<String, String>, RigidEO>();
		for (RigidEO old : helper.getAll(E_03_15_0004.ENTITYFIELD)) {
			final UID pk = getUID(E_03_15_0004.ENTITYFIELD.getEntityName(), (Long) old.getPrimaryKey());
			final RigidEO fieldNew = new RigidEO(E_03_15_0004.ENTITYFIELD.getUID(), pk);
			final String entityName = entitiesById.get(old.getForeignID(E_03_15_0004.ENTITYFIELD.entity)).getValue(E_03_15_0004.ENTITY.entity);
			final String fieldName = old.getValue(E_03_15_0004.ENTITYFIELD.field);
			
			for (DbField<Long> key : old.getForeignIDKeys()) {
				final Long foreignId = old.getForeignID(key);
				if (E_03_15_0004.ENTITYFIELD.entity.equals(key)) {
					final UID foreignUID = getUID(E_03_15_0004.ENTITY.getEntityName(), foreignId);
					fieldNew.setForeignUID(E_04_00_0022_finish.ENTITYFIELD.entity, foreignUID);
				}
				fieldNew.setForeignID(key, foreignId);
			}
			for (DbField<?> key : old.getValueKeys()) {
				final Object value = old.getValue(key);
				if (value != null) {
					if (E_03_15_0004.ENTITYFIELD.foreignentity.equals(key)) {
						final UID foreignUID = getEntityUIDByName((String)value);
						if (foreignUID==null) {
							LOG.warn(String.format("entity with name \"%s\" not found, but was set in %s", value, key));
							continue;
						}
						fieldNew.setForeignUID(E_04_00_0022_finish.ENTITYFIELD.foreignentity, foreignUID);
					} else if (E_03_15_0004.ENTITYFIELD.lookupentity.equals(key)) {
						final UID foreignUID = getEntityUIDByName((String)value);
						if (!entitiesByName.containsKey(value)) {
							LOG.warn(String.format("entity with name \"%s\" not found, but was set in %s", value, key));
							continue;
						}
						fieldNew.setForeignUID(E_04_00_0022_finish.ENTITYFIELD.lookupentity, foreignUID);
					}
				}
				
				fieldNew.setValueUNSAFE(key, value);
			}
			fieldsById.put((Long) old.getPrimaryKey(), pk);
			_fields.put(new Pair<String, String>(entityName, fieldName), fieldNew);
		}
	}
	
	private void migrateLocaleResourceTreeViews() {
		logTitle(String.format("migrate local resource tree views"));
		for (RigidEO eoEntity : helper.getAll(E_03_15_0004.ENTITY)) {
			String entity = eoEntity.getValue(E_03_15_0004.ENTITY.entity);
			migrateLocaleResourceTreeView(entity, eoEntity.getValue(E_03_15_0004.ENTITY.localeresourcett));
			migrateLocaleResourceTreeView(entity, eoEntity.getValue(E_03_15_0004.ENTITY.localeresourcetw));
		}
	}
	
	private void migrateLocaleResourceTreeView(String entity, String res) {
		if (res == null) {
			return;
		}
		
		Collection<RigidEO> eoLocaleResources = helper.getByField(E_03_15_0004.LOCALERESOURCE, E_03_15_0004.LOCALERESOURCE.resourceID, res);
		if (eoLocaleResources.isEmpty()) {
			LOG.warn(String.format("locale resource %s does not exist", res));
		}
		
		for (RigidEO eoRes : eoLocaleResources) {
			String eoResValueOld = eoRes.getValue(E_03_15_0004.LOCALERESOURCE.text);		
			String eoResValueNew = migrateRefString(entity, eoResValueOld, 
					String.format("locale resource value \"%s\" of entity %s contains invalid value: ", 
							eoResValueOld, entity));
			if (eoResValueNew.isEmpty()) {
				eoResValueNew = eoResValueOld;
			}
			LOG.debug(String.format("entity \"%s\": locale resource value %s --> %s", entity, eoResValueOld, eoResValueNew));
			eoRes.setValue(E_03_15_0004.LOCALERESOURCE.text, eoResValueNew);
			helper.update(E_03_15_0004.LOCALERESOURCE, eoRes);
		}
	}
	
	private void migrateEntitySubNodes() {
		logTitle(String.format("migrate entity subnodes"));
		for (RigidEO eoSubNode : helper.getAll(E_03_15_0004.ENTITYSUBNODES)) {
			boolean update = false;
			Long entityRootId = eoSubNode.getForeignID(E_03_15_0004.ENTITYSUBNODES.originentityid);
			String entityRoot = entitiesById.get(entityRootId).getValue(E_03_15_0004.ENTITY.entity);
			String entity = eoSubNode.getValue(E_03_15_0004.ENTITYSUBNODES.entity);
			
			// fieldroot and field are "combined" unreferenced foreign entity fields
			String fieldRoot = eoSubNode.getValue(E_03_15_0004.ENTITYSUBNODES.fieldroot);
			String field = eoSubNode.getValue(E_03_15_0004.ENTITYSUBNODES.field);
			
			if (fieldRoot != null) {
				UID fieldUID = getFieldUIDByNames(entity, fieldRoot);
				if (fieldUID != null) {
					update = true;
					eoSubNode.setValue(E_03_15_0004.ENTITYSUBNODES.fieldrootuid, fieldUID.getString());
				} else {
					LOG.warn(String.format("Subnode for %s: Root field \"%s\" in entity \"%s\" does not exist", entityRoot, fieldRoot, entity));
				}
			}
			if (field != null) {
				UID fieldUID = getFieldUIDByNames(entity, field);
				if (fieldUID != null) {
					update = true;
					eoSubNode.setValue(E_03_15_0004.ENTITYSUBNODES.fielduid, fieldUID.getString());
				} else {
					LOG.warn(String.format("Subnode for %s: Field \"%s\" in entity \"%s\" does not exist", entityRoot, field, entity));
				}
			}
			
			// node/nodetooltip definition
			String refNode = eoSubNode.getValue(E_03_15_0004.ENTITYSUBNODES.node);
			String refNodeTooltip = eoSubNode.getValue(E_03_15_0004.ENTITYSUBNODES.nodetooltip);
			
			if (refNode != null) {
				update = true;
				String eoRefNode = migrateRefString(entity, refNode, 
						String.format("ref definition \"%s\" of entity subnode %s contains invalid value: ", 
								refNode, entity));
				LOG.debug(String.format("\"%s\": entity subnode %s --> %s", entity, refNode, eoRefNode.toString()));
				eoSubNode.setValue(E_03_15_0004.ENTITYSUBNODES.node, eoRefNode.toString());
			}
			if (refNodeTooltip != null) {
				update = true;
				String eoRefNodeTooltip = migrateRefString(entity, refNodeTooltip, 
						String.format("ref definition \"%s\" of entity subnodetooltip %s contains invalid value: ", 
								refNodeTooltip, entity));
				LOG.debug(String.format("\"%s\": entity subnodetooltip %s --> %s", entity, refNodeTooltip, eoRefNodeTooltip.toString()));
				eoSubNode.setValue(E_03_15_0004.ENTITYSUBNODES.nodetooltip, eoRefNodeTooltip.toString());
			}
			if (update)
				helper.update(E_03_15_0004.ENTITYSUBNODES, eoSubNode);
		}
	}
	
	private void migrateLogBook() {
		logTitle(String.format("migrate log book"));
		final ConcurrentMap<Long, UID> goToModule = new ConcurrentHashMap<Long, UID>();
		
		ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		for (final Object pkLogBook : helper.getAllPks(E_03_15_0004.GENERICOBJECTLOGBOOK)) {
		Runnable worker = new Runnable() {@Override	public void run() {

			RigidEO eoLogBook = helper.getByPrimaryKey(E_03_15_0004.GENERICOBJECTLOGBOOK, pkLogBook);
			UID attributeUID = null;
			UID masterdataUID = null;
			UID entityFieldUID = null;
			final Long goId = eoLogBook.getForeignID(E_03_15_0004.GENERICOBJECTLOGBOOK.genericObject);
			if (!goToModule.containsKey(goId)) {
				final RigidEO go = helper.getByPrimaryKey(E_03_15_0004.GENERICOBJECT, goId);
				final UID moduleUID = new UID(go.getValue(E_03_15_0004.GENERICOBJECT.moduleuid));
				goToModule.put(goId, moduleUID);
			}
			final UID moduleUID = goToModule.get(goId);
			final Integer oldAttributeId = eoLogBook.getValue(E_03_15_0004.GENERICOBJECTLOGBOOK.attributeId);
			if (oldAttributeId != null) {
				if (oldAttributeId < 0) {
					switch (oldAttributeId) {
						case -10010: attributeUID = SF.STATE.getUID(moduleUID); break;
						case -10011: attributeUID = SF.STATENUMBER.getUID(moduleUID); break;
						case -10012: attributeUID = SF.SYSTEMIDENTIFIER.getUID(moduleUID); break;
						case -10013: attributeUID = SF.PROCESS.getUID(moduleUID); break;
						case -10014: attributeUID = SF.CREATEDAT.getUID(moduleUID); break;
						case -10015: attributeUID = SF.CREATEDBY.getUID(moduleUID); break;
						case -10016: attributeUID = SF.CHANGEDAT.getUID(moduleUID); break;
						case -10017: attributeUID = SF.CHANGEDBY.getUID(moduleUID); break;
						case -10018: attributeUID = SF.ORIGIN.getUID(moduleUID); break;
						case -10019: attributeUID = SF.LOGICALDELETED.getUID(moduleUID); break;
						case -10020: attributeUID = SF.STATEICON.getUID(moduleUID); break;
						case -10021: attributeUID = SF.VERSION.getUID(moduleUID); break;
						default: LOG.warn("Unknown system attribute " + oldAttributeId);
					}
				} else {
					attributeUID = fieldsById.get(oldAttributeId.longValue());
				}
			}
			final Integer oldMasterdataId = eoLogBook.getValue(E_03_15_0004.GENERICOBJECTLOGBOOK.masterdataId);
			if (oldMasterdataId != null) {
				RigidEO eoEntity = entitiesById.get(oldMasterdataId.longValue());
				if (eoEntity != null) {
					masterdataUID = (UID) eoEntity.getPrimaryKey();
				}
			}
			final Integer oldEntityFieldId = eoLogBook.getValue(E_03_15_0004.GENERICOBJECTLOGBOOK.entityfieldId);
			if (oldEntityFieldId != null) {
				if (oldEntityFieldId < 0 && masterdataUID != null) {
					switch (oldEntityFieldId) {
						case -10010: entityFieldUID = SF.STATE.getUID(masterdataUID); break;
						case -10011: entityFieldUID = SF.STATENUMBER.getUID(masterdataUID); break;
						case -10012: entityFieldUID = SF.SYSTEMIDENTIFIER.getUID(masterdataUID); break;
						case -10013: entityFieldUID = SF.PROCESS.getUID(masterdataUID); break;
						case -10014: entityFieldUID = SF.CREATEDAT.getUID(masterdataUID); break;
						case -10015: entityFieldUID = SF.CREATEDBY.getUID(masterdataUID); break;
						case -10016: entityFieldUID = SF.CHANGEDAT.getUID(masterdataUID); break;
						case -10017: entityFieldUID = SF.CHANGEDBY.getUID(masterdataUID); break;
						case -10018: entityFieldUID = SF.ORIGIN.getUID(masterdataUID); break;
						case -10019: entityFieldUID = SF.LOGICALDELETED.getUID(masterdataUID); break;
						case -10020: entityFieldUID = SF.STATEICON.getUID(masterdataUID); break;
						case -10021: entityFieldUID = SF.VERSION.getUID(masterdataUID); break;
						default: LOG.warn("Unknown system attribute " + oldAttributeId);
					}
				} else {
					entityFieldUID = fieldsById.get(oldEntityFieldId.longValue());
				}
			}
			
			LOG.debug(String.format("log book entry %s (object=%s, module=%s): attribute %s --> %s, masterdata %s --> %s, entity field %s --> %s", 
					eoLogBook.getPrimaryKey(), goId, moduleUID,
					oldAttributeId, attributeUID, oldMasterdataId, masterdataUID, oldEntityFieldId, entityFieldUID));
			if (attributeUID!=null) eoLogBook.setValue(E_03_15_0004.GENERICOBJECTLOGBOOK.attributeIduid, attributeUID.getString());
			if (masterdataUID!=null) eoLogBook.setValue(E_03_15_0004.GENERICOBJECTLOGBOOK.masterdataIduid, masterdataUID.getString());
			if (entityFieldUID!=null) eoLogBook.setValue(E_03_15_0004.GENERICOBJECTLOGBOOK.entityfieldIduid, entityFieldUID.getString());
			helper.update(E_03_15_0004.GENERICOBJECTLOGBOOK, eoLogBook);

		}};	executor.execute(worker);}

	    executor.shutdown();
	    try {
			if (!executor.awaitTermination(1, TimeUnit.DAYS)) {
				throw new CommonFatalException("Timeout!");
			}
		} catch (InterruptedException e) {
			throw new CommonFatalException(e);
		}
	}
	
	private void migrateForeignFields() {
		logTitle(String.format("migrate foreign fields"));
		// Foreign/lookup/search entity field definition
		// (entity_field.unreferencedForeignEntityField migrated already from foreignKeys)
		for (RigidEO eoField : helper.getAll(E_03_15_0004.ENTITYFIELD)) {
			String foreignField = eoField.getValue(E_03_15_0004.ENTITYFIELD.foreignentityfield);
			String lookupField = eoField.getValue(E_03_15_0004.ENTITYFIELD.lookupentityfield);
			String refSearchField = eoField.getValue(E_03_15_0004.ENTITYFIELD.searchfield);
			String refField = RigidUtils.defaultIfNull(foreignField, lookupField);
			String refEntity = RigidUtils.defaultIfNull(eoField.getValue(E_03_15_0004.ENTITYFIELD.foreignentity), eoField.getValue(E_03_15_0004.ENTITYFIELD.lookupentity));
			if (refField != null) {
				String entity = entitiesById.get(eoField.getForeignID(E_03_15_0004.ENTITYFIELD.entity)).getValue(E_03_15_0004.ENTITY.entity);
				String field = eoField.getValue(E_03_15_0004.ENTITYFIELD.field);
				String eoRefValue = migrateRefString(refEntity, refField, 
						String.format("ref definition \"%s\" of field %s.%s contains invalid value: ", 
						refField, entity, field));
				LOG.debug(String.format("\"%s.%s\": foreign field %s --> %s", entity, field, refField, eoRefValue.toString()));
				if (foreignField != null) {
					eoField.setValue(E_03_15_0004.ENTITYFIELD.foreignentityfield, eoRefValue.toString());
				} else {
					eoField.setValue(E_03_15_0004.ENTITYFIELD.lookupentityfield, eoRefValue.toString());
				}
				if (refSearchField != null) {
					String eoRefSearchValue = migrateRefString(refEntity, refSearchField, 
							String.format("ref definition \"%s\" of field %s.%s contains invalid value: ", 
							refField, entity, field));
					LOG.debug(String.format("\"%s.%s\": search field %s --> %s", entity, field, refSearchField, eoRefSearchValue.toString()));
					eoField.setValue(E_03_15_0004.ENTITYFIELD.searchfield, eoRefSearchValue.toString());
				}
				helper.update(E_03_15_0004.ENTITYFIELD, eoField);
			}
		}
	}
	
	private void migrateImportAttributes() {
		logTitle(String.format("migrate import attributes"));
		Map<Long, Long> importEntities = new HashMap<Long, Long>();
		for (RigidEO eoImport : helper.getAll(E_04_00_0022_pre.IMPORT)) {
			Long entityId = eoImport.getForeignID(E_04_00_0022_pre.IMPORT.entity);
			importEntities.put((Long) eoImport.getPrimaryKey(), entityId);
		}
		
		Map<Long, MetaDbFieldWrapper> importAttributeFields = new HashMap<Long, MetaDbFieldWrapper>();
		for (RigidEO eoAttribute : helper.getAll(E_04_00_0022_pre.IMPORTATTRIBUTE)) {
			try {
				String field = eoAttribute.getValue(E_04_00_0022_pre.IMPORTATTRIBUTE.attribute);
				Long importId = eoAttribute.getForeignID(E_04_00_0022_pre.IMPORTATTRIBUTE.importfield);
				Long entityId = importEntities.get(importId);
				String entity = entitiesById.get(entityId).getValue(E_03_15_0004.ENTITY.entity);
				MetaDbFieldWrapper fieldMeta = getFieldMetaByNames(entity, field);
				if (fieldMeta != null) {
					LOG.debug(String.format("import %s attribute old \"%s\" --> new %s", importId, field, fieldMeta.getUID().getString()));
					eoAttribute.setValue(E_04_00_0022_pre.IMPORTATTRIBUTE.attributeuid, fieldMeta.getUID().getString());
					helper.update(E_04_00_0022_pre.IMPORTATTRIBUTE, eoAttribute);
					importAttributeFields.put((Long) eoAttribute.getPrimaryKey(), fieldMeta);
				} else {
					LOG.warn(String.format("import %s attribute \"%s\" in entity \"%s\" does not exist", importId, field, entity));
				}
			} catch (Exception ex) {
				LOG.warn("unable to migrate import attribute with pk " + eoAttribute.getPrimaryKey(), ex);
			}
		}
		
		for (RigidEO eoIdentifier : helper.getAll(E_04_00_0022_pre.IMPORTIDENTIFIER)) {
			try {
				String field = eoIdentifier.getValue(E_04_00_0022_pre.IMPORTIDENTIFIER.attribute);
				Long importId = eoIdentifier.getForeignID(E_04_00_0022_pre.IMPORTIDENTIFIER.importfield);
				Long entityId = importEntities.get(importId);
				String entity = entitiesById.get(entityId).getValue(E_03_15_0004.ENTITY.entity);
				UID fieldUID = getFieldUIDByNames(entity, field);
				if (fieldUID != null) {
					LOG.debug(String.format("import %s identifier old \"%s\" --> new %s", importId, field, fieldUID.getString()));
					eoIdentifier.setValue(E_04_00_0022_pre.IMPORTIDENTIFIER.attributeuid, fieldUID.getString());
					helper.update(E_04_00_0022_pre.IMPORTIDENTIFIER, eoIdentifier);
				} else {
					LOG.warn(String.format("import %s identifier \"%s\" in entity \"%s\" does not exist", importId, field, entity));
				}
			} catch (Exception ex) {
				LOG.warn("unable to migrate import identifier with pk " + eoIdentifier.getPrimaryKey(), ex);
			}
		}
		
		for (RigidEO eoFeIdentifier : helper.getAll(E_04_00_0022_pre.IMPORTFEIDENTIFIER)) {
			try {
				String field = eoFeIdentifier.getValue(E_04_00_0022_pre.IMPORTFEIDENTIFIER.attribute);
				Long impAttributeId = eoFeIdentifier.getForeignID(E_04_00_0022_pre.IMPORTFEIDENTIFIER.importattribute);
				MetaDbFieldWrapper fieldMeta = importAttributeFields.get(impAttributeId);
				if (fieldMeta != null && fieldMeta.getForeignEntity() != null) {
					String entity = getEntityNameByUID(fieldMeta.getForeignEntity());
					UID fieldUID = getFieldUIDByNames(entity, field);
					if (fieldUID != null) {
						LOG.debug(String.format("import attribute %s fe identifier old \"%s\" --> new %s", impAttributeId, field, fieldUID.getString()));
						eoFeIdentifier.setValue(E_04_00_0022_pre.IMPORTFEIDENTIFIER.attributeuid, fieldUID.getString());
						helper.update(E_04_00_0022_pre.IMPORTFEIDENTIFIER, eoFeIdentifier);
					} else {
						LOG.warn(String.format("import attribute %s fe identifier \"%s\" in entity \"%s\" does not exist", impAttributeId, field, entity));
					}
				}
			} catch (Exception ex) {
				LOG.warn("unable to migrate import fe identifier with pk " + eoFeIdentifier.getPrimaryKey(), ex);
			}
		}
	}
	
	private String migrateRefString(String refEntity, String refString, String warnIfNecessary) {
		if (refEntity == null || refString == null) {
			return null;
		}
		ForeignEntityFieldParser parser = new ForeignEntityFieldParser(refString);
		Iterator<IFieldRef> it = parser.iterator();
		StringBuffer eoRefValue = new StringBuffer();
		while (it.hasNext()) {
			IFieldRef ref = it.next();
			if (ref.isConstant()) {
				eoRefValue.append(ref.getContent());
			} else {
				EntityMeta<?> refNuclosEntityMeta = E_03_15_0004.getByName(refEntity);
				boolean warn = false;
				if (refNuclosEntityMeta != null) {
					FieldMeta<?> refNuclosFieldMeta = null;
					for (FieldMeta<?> fMeta : refNuclosEntityMeta.getFields()) {
						if (fMeta.getFieldName().equals(ref.getContent())) {
							refNuclosFieldMeta = fMeta;
							break;
						}
					}
					if (refNuclosFieldMeta != null) {
						eoRefValue.append(refNuclosFieldMeta.getUID().getStringifiedDefinition());
					} else {
						warn = true;
					}
				} else {
					UID eoRefFieldUID = getFieldUIDByNames(refEntity, ref.getContent());
					if (eoRefFieldUID != null) {
						eoRefValue.append(eoRefFieldUID.getStringifiedDefinition());
					} else {
						warn = true;
					}
				}
				if (warn && warnIfNecessary != null) {
					LOG.warn(warnIfNecessary + ref.getContent());
				}
			}
		}
		return eoRefValue.toString();
	}
	
	private UID getEntityUIDByName(String entityName) {
		if (entityName == null) {
			return null;
		}
		final EntityMeta<?> entityMeta = E_03_15_0004.getByName(entityName);
		if (entityMeta != null) {
			return entityMeta.getUID();
		}
		final RigidEO rEntity = entitiesByName.get(entityName);
		if (rEntity != null) {
			return (UID) rEntity.getPrimaryKey();
		}
		return null;
	}
	
	private String getEntityNameByUID(UID entityUID) {
		if (entityUID == null) {
			return null;
		}
		final EntityMeta<?> entityMeta = E_03_15_0004.getByUID(entityUID);
		if (entityMeta != null) {
			return entityMeta.getEntityName();
		}
		final RigidEO rEntity = entitiesByUID.get(entityUID);
		if (rEntity != null) {
			return rEntity.getValue(E_03_15_0004.ENTITY.entity);
		}
		return null;
	}
	
	private UID getFieldUIDByNames(String entityName, String fieldName) {
		MetaDbFieldWrapper fieldMeta = getFieldMetaByNames(entityName, fieldName);
		if (fieldMeta != null) {
			return fieldMeta.getUID();
		}
		return null;
	}
	
	private MetaDbFieldWrapper getFieldMetaByNames(String entityName, String fieldName) {
		final UID entityUID = getEntityUIDByName(entityName);
		if (entityUID != null) {
			final SF<?> sysField = SF.getByField(fieldName);
			if (sysField != null) {
				return new MetaDbFieldWrapper(sysField.getMetaData(entityUID));
			}
			if (E_03_15_0004.isNuclosEntity(entityUID)) {
				EntityMeta<?> entityMeta = E_03_15_0004.getByName(entityName);
				for (FieldMeta<?> fieldMeta : entityMeta.getFields()) {
					if (fieldMeta.getFieldName().equals(fieldName)) {
						return new MetaDbFieldWrapper(fieldMeta);
					}
				}
			} else {
				RigidEO eoField = _fields.get(new Pair<String, String>(entityName, fieldName));
				if (eoField != null) {
					return new MetaDbFieldWrapper(eoField);
				}
			}
		}
		return null;
	}
	
	private UID getUID(EntityMeta<?> entity, Long id) {
		return getNucletContentUID(entity.getEntityName(), id).x;
	}
	
	private UID getUID(String entity, Long id) {
		return getNucletContentUID(entity, id).x;
	}
	
	private Pair<UID, Integer> getNucletContentUID(String entity, Long id) {
		final Pair<String, Long> key = new Pair<String, Long>(entity, id);
		Pair<UID, Integer> result = nucletContentMap.get(key);
		if (result == null) {
			result = new Pair<UID, Integer>();
			result.x = new UID(); //create new UID
			LOG.debug(String.format("new UID generated: entity=%s, id=%s, uid=%s", entity, id, result.x.getString()));
			nucletContentMap.put(key, result);
		}
		return result;
	}
	
	private void removeUserDefinedConstraintsAndIndices() {
		logTitle(String.format("remove user defined constraints and indices"));
		Collection<MetaDbEntityWrapper> allEntities = new ArrayList<MetaDbEntityWrapper>();
		Collection<MetaDbFieldWrapper> allFields = new ArrayList<MetaDbFieldWrapper>();
		Collection<UID> involvedFields = new ArrayList<UID>();
		
		for (RigidEO entity : entitiesById.values()) {
			allEntities.add(new MetaDbEntityWrapper(entity));
			final UID pkFieldUID = SF.PK_ID.getUID((UID)entity.getPrimaryKey());
			final MetaDbFieldWrapper pkFieldWrapper = new MetaDbFieldWrapper(SF.PK_ID.getMetaData((UID)entity.getPrimaryKey()));
			allFields.add(pkFieldWrapper);
			involvedFields.add(pkFieldUID);
			if (Boolean.TRUE.equals(entity.getValue(E_03_15_0004.ENTITY.usessatemodel))) {
				final UID stateFieldUID = SF.STATE_UID.getUID((UID)entity.getPrimaryKey());
				final UID processFieldUID = SF.PROCESS_UID.getUID((UID)entity.getPrimaryKey());
				final MetaDbFieldWrapper stateFieldWrapper = new MetaDbFieldWrapper(SF.STATE_UID.getMetaData((UID)entity.getPrimaryKey()));
				final MetaDbFieldWrapper processFieldWrapper = new MetaDbFieldWrapper(SF.PROCESS_UID.getMetaData((UID)entity.getPrimaryKey()));
				// change to INTID_ here...
				stateFieldWrapper.overwriteDbColumn(MetaDbHelper.getDbRefColumn(stateFieldWrapper.getDbColumn(), false));
				processFieldWrapper.overwriteDbColumn(MetaDbHelper.getDbRefColumn(processFieldWrapper.getDbColumn(), false));
				allFields.add(stateFieldWrapper);
				allFields.add(processFieldWrapper);
				involvedFields.add(stateFieldUID);
				involvedFields.add(processFieldUID);
			}
		}
		
		for (RigidEO field : _fields.values()) {
			allFields.add(new MetaDbFieldWrapper(field));
			involvedFields.add((UID) field.getPrimaryKey());
		}
		
		ConstraintHelper.removeConstraints(true, true, true, involvedFields, allEntities, allFields, E_03_15_0004.getThis(), LOG);
		IndexHelper.removeIndices(involvedFields, allEntities, allFields, E_03_15_0004.getThis(), LOG);
	}
	
	private void migratePrimaryKeys() {
		logTitle(String.format("migrate primary keys"));
		for (EntityMeta<?> newEntity : E_04_00_0022_finish.getAllEntities()) {
			EntityMeta<?> oldEntity = E_03_15_0004.getByUID(newEntity.getUID());
			if (oldEntity == null) {
				// new 4.0 entity...
				continue;
			}
			if (E_03_15_0004.FORM == oldEntity || E_03_15_0004.FORMOUTPUT == oldEntity || E_03_15_0004.FORMUSAGE == oldEntity ||
					E_03_15_0004.REPORTEXECUTION == oldEntity) {
				// REPORT and FORM are equivalent
				continue;
			}
			if (newEntity.isUidEntity()) {
				LOG.debug(String.format("migrate primary keys for %s", oldEntity.getEntityName()));
				
				for (Object pk : helper.getAllPks(oldEntity)) {
					RigidEO eo = helper.getByPrimaryKey(oldEntity, pk);
					if (pk instanceof Long) {
						Pair<UID, Integer> ncuid = getNucletContentUID(oldEntity.getEntityName(), (Long) pk);
						LOG.debug(String.format("primary key old %s --> new %s", pk, ncuid.x.getString()));
						try {
							eo.setForeignUID(getUIDPkField(oldEntity), ncuid.x);
							eo.setValue(SF.IMPORTVERSION, ncuid.y);
							helper.update(oldEntity, eo);
						} catch (Exception ex) {
							LOG.error(String.format("pk field not accessible: %s", oldEntity), ex);
						}
					} else {
						LOG.error(String.format("pk not instance of long: %s", pk));
					}
				}
				
			}
		}
	}
	
	private DbField<UID> getUIDPkField(EntityMeta<?> entitymeta) {
		try {
			return (DbField<UID>) entitymeta.getClass().getField("pkuid").get(entitymeta);
		} catch (Exception e) {
			return null;
		}
	}
	
	private void migrateForeignKeys() {
		logTitle(String.format("migrate foreign keys"));
		// system keys
		final IRigidMetaProvider prov = new SystemMetaProvider(E_03_15_0004.getThis());
		for (final EntityMeta<?> entity : E_03_15_0004.getAllEntities()) {
			if (E_03_15_0004.FORM == entity || E_03_15_0004.FORMOUTPUT == entity || E_03_15_0004.FORMUSAGE == entity ||
					E_03_15_0004.REPORTEXECUTION == entity) {
				// REPORT and FORM are equivalent
				continue;
			}
			LOG.debug(String.format("migrate foreign keys of %s", entity.getEntityName()));
			
			final List<FieldMeta<?>> fieldMetas = new ArrayList<FieldMeta<?>>(entity.getFields());
			Collections.sort(fieldMetas, new Comparator<FieldMeta<?>>() {
				@Override
				public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
					return o1.getFieldName().compareTo(o2.getFieldName());
				}
			});
			
			ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
			for (final Object pk : helper.getAllPks(entity)) {
			Runnable worker = new Runnable() {@Override	public void run() {
			    
				RigidEO eo = helper.getByPrimaryKey(entity, pk);
				boolean printData = false;
				
				for (FieldMeta<?> fieldMeta : fieldMetas) {
					UID foreignEntityUID = RigidUtils.defaultIfNull(fieldMeta.getForeignEntity(), fieldMeta.getUnreferencedForeignEntity());
					
					Object value = eo.getValue(fieldMeta);
					if (foreignEntityUID != null) {
						EntityMeta<?> foreignEntity = E_04_00_0022_finish.getByUID(foreignEntityUID);
						if (foreignEntity == null) {
							// entity does not exist any more
							continue;
						}
						if (!foreignEntity.isUidEntity()) {
							// nothing to do
							continue;
						}
						
						UID foreignUID = null;
						Long foreignId = eo.getForeignID((DbField<Long>) fieldMeta);
						if (foreignId == null && fieldMeta.getUnreferencedForeignEntity() != null) {
							if (value == null) {
								continue;
							}
							if (E_03_15_0004.ENTITY.checkEntityUID(foreignEntityUID)) {
								String sForeignEntityName = (String) value;
								foreignUID = getEntityUIDByName(sForeignEntityName);
								if (foreignUID == null) {
									if (sForeignEntityName.startsWith("dyn_") || sForeignEntityName.startsWith("crt_")) {
				            			LOG.warn(String.format("foreign dynamic or chart entity %s not found: Please open and save the underlaying datasource to generate meta data. Then add dynamic or chart entity again manually. " + releaseNote("MIG-02"), sForeignEntityName));
				            			if (!fieldMeta.isNullable()) {
				            				printData = true;
				            			}
				            		} else {
				            			LOG.warn(String.format("foreign entity %s not found", sForeignEntityName));
				            		}
								}
							} else if (E_03_15_0004.ENTITYSUBNODES.field == fieldMeta) {
								if (value != null) {
									String sSubnodeEntity = eo.getValue(E_03_15_0004.ENTITYSUBNODES.entity);
									UID fieldUID = getFieldUIDByNames(sSubnodeEntity, (String) value);
									if (fieldUID != null) {
										eo.setValue(E_03_15_0004.ENTITYSUBNODES.fielduid, fieldUID.getString());
									} else {
										LOG.warn(String.format("subnode foreign key %s.%s not found (field)", sSubnodeEntity, value));
									}
								}
							} else if (E_03_15_0004.ENTITYSUBNODES.fieldroot == fieldMeta) {
								if (value != null) {
									String sSubnodeEntity = eo.getValue(E_03_15_0004.ENTITYSUBNODES.entity);
									UID fieldUID = getFieldUIDByNames(sSubnodeEntity, (String) value);
									if (fieldUID != null) {
										eo.setValue(E_03_15_0004.ENTITYSUBNODES.fieldrootuid, fieldUID.getString());
									} else {
										LOG.warn(String.format("subnode foreign key %s.%s not found (fieldroot)", sSubnodeEntity, value));
									}
								}
							} else {
								ForeignEntityFieldUIDParser parser = new ForeignEntityFieldUIDParser(fieldMeta, prov);
								// check if only one single ref field, so we can search for that value, instead of check all stringified values
								FieldMeta<?> singleRefField = null;
								Iterator<IFieldUIDRef> it2 = parser.iterator();
								while (it2.hasNext()) {
									IFieldUIDRef ref = it2.next();
									if (ref.isConstant()) {
										// constants not supported... break
										FieldMeta<?> refField = null;
										break;
									}
									if (ref.isUID()) {
										if (singleRefField == null) {
											singleRefField = prov.getEntityField(ref.getUID());
										} else {
											// more then one field
											singleRefField = null;
											break;
										}
									}
								}
								if (singleRefField != null) {
									try {
										foreignUID = getRefUID(foreignEntity, singleRefField, value);
									} catch (Exception ex) {
										if ("NotUnique".equals(ex.getMessage())) {
											LOG.warn(String.format("foreign value \"%s\" in field %s.%s is not a unique value in %s",
												value, entity.getEntityName(), fieldMeta.getFieldName(), foreignEntity.getEntityName()));
										} else {
											throw new NuclosFatalException(ex);
										}
									}
								} else {
									for (Object pkRef : helper.getAllPks(foreignEntity)) { // foreignEntity here uses UID as PK
										RigidEO eoRef = helper.getByPrimaryKey(foreignEntity, pkRef);
										StringBuffer eoRefValue = new StringBuffer();
										Iterator<IFieldUIDRef> it = parser.iterator();
										while (it.hasNext()) {
											IFieldUIDRef ref = it.next();
											if (ref.isConstant()) {
												eoRefValue.append(ref.getConstant());
											}
											if (ref.isUID()) {
												FieldMeta<?> refField = prov.getEntityField(ref.getUID());
												eoRefValue.append(eoRef.getValue(refField));
											}
										}
										if (eoRefValue.toString().equals(value)) {
											if (foreignUID == null) {
												foreignUID = (UID) eoRef.getPrimaryKey();
											} else {
												LOG.warn(String.format("foreign value \"%s\" in field %s.%s is not a unique value in %s",
														value, entity.getEntityName(), fieldMeta.getFieldName(), foreignEntity.getEntityName()));
												break;
											}
										} 
									}
								}
								if (foreignUID == null) {
									LOG.warn(String.format("foreign value \"%s\" in field %s.%s not found in %s",
											value, entity.getEntityName(), fieldMeta.getFieldName(), foreignEntity.getEntityName()));
								}
							}
						}
						
						if (foreignId != null) {
							foreignUID = getUID(foreignEntity.getEntityName(), foreignId);
						}
						if (foreignUID != null) {	
							FieldMeta<?> newForeignFieldMeta = null;
							for (FieldMeta<?> fieldMeta2 : entity.getFields()) {
								if (fieldMeta2.getFieldName().equals(fieldMeta.getFieldName()+"uid")) {
									newForeignFieldMeta = fieldMeta2;
									break;
								}
							}
							if (newForeignFieldMeta == null) {
								LOG.error(String.format("new foreign field %suid does not exist for entity \"%s\"", fieldMeta.getFieldName(), entity.getEntityName()));
							} else {
								LOG.debug(String.format("foreign %s key old %s --> new %s", fieldMeta.getFieldName(), RigidUtils.firstNonNull(foreignId, value), foreignUID.getString()));
								eo.setValueUNSAFE(newForeignFieldMeta, foreignUID.getString());
								helper.update(entity, eo);
							}
							
						}
					} else if (E_03_15_0004.GENERATIONUSAGE.state == fieldMeta) {
						if (value != null) {
							try {
								String statemnemonic = (String) value;
								Integer statenum;
								if (statemnemonic.indexOf(" ") == -1) {
									statenum = Integer.parseInt(statemnemonic);
								} else {
									String[] cut = statemnemonic.split(" ");
									statenum = Integer.parseInt(cut[0]);
								}
								Long generationId = eo.getForeignID(E_03_15_0004.GENERATIONUSAGE.generation);
								Long processId = eo.getForeignID(E_03_15_0004.GENERATIONUSAGE.process);
								for (RigidEO eoGeneration : helper.getAll(E_03_15_0004.GENERATION)) {
									if (generationId.equals(eoGeneration.getPrimaryKey())) {
										Long entityId = eoGeneration.getForeignID(E_03_15_0004.GENERATION.sourceModule);
										Long stateModelWithProcessId = null;
										Long stateModelWithoutId = null;
										for (RigidEO eoStatemodelUsage : helper.getAll(E_03_15_0004.STATEMODELUSAGE)) {
											if (entityId.equals(eoStatemodelUsage.getForeignID(E_03_15_0004.STATEMODELUSAGE.nuclos_module))) {
												if (eoStatemodelUsage.getForeignID(E_03_15_0004.STATEMODELUSAGE.process) == null) {
													stateModelWithoutId = eoStatemodelUsage.getForeignID(E_03_15_0004.STATEMODELUSAGE.statemodel);
												} else {
													if (processId != null && processId.equals(eoStatemodelUsage.getForeignID(E_03_15_0004.STATEMODELUSAGE.process))) {
														stateModelWithProcessId = eoStatemodelUsage.getForeignID(E_03_15_0004.STATEMODELUSAGE.statemodel);
													}
												}
											}
										}
										for (RigidEO eoState : helper.getAll(E_03_15_0004.STATE)) {
											if (RigidUtils.equal(eoState.getForeignID(E_03_15_0004.STATE.model), RigidUtils.defaultIfNull(stateModelWithProcessId, stateModelWithoutId))) {
												if (RigidUtils.equal(statenum, eoState.getValue(E_03_15_0004.STATE.numeral))) {
													UID stateUID = getUID(E_03_15_0004.STATE.getEntityName(), (Long) eoState.getPrimaryKey());
													LOG.debug(String.format("generation usage state mnemonic %s --> %s", value, stateUID.getString()));
													eo.setValue(E_03_15_0004.GENERATIONUSAGE.stateuid, stateUID.getString());
													helper.update(entity, eo);
													break;
												}
											}
										}
										break;
									}
								}
							} catch (Exception ex) {
								LOG.warn("unable to migrate generation usage with pk " + eo.getPrimaryKey(), ex);
							}
						}
					}
				}
				
				if (printData) {
					StringBuffer s = new StringBuffer();
					s.append("Data record");
					s.append(String.format("\n%-30s = %s", "TABLE", entity.getDbTable()));
					for (DbField<UID> value : eo.getForeignUIDKeys()) {
						s.append(String.format("\n%-30s = %s", value.getDbColumn(), eo.getForeignUID(value)));
					}
					for (DbField<?> value : eo.getValueKeys()) {
						s.append(String.format("\n%-30s = %s", value.getDbColumn(), eo.getValue(value)));
					}
					LOG.debug(s.toString());
				}
				
				
			}};	executor.execute(worker);}

		    executor.shutdown();
		    try {
				if (!executor.awaitTermination(1, TimeUnit.DAYS)) {
					throw new CommonFatalException("Timeout!");
				}
			} catch (InterruptedException e) {
				throw new CommonFatalException(e);
			}
		}
	}
	
	/**
	 * 
	 * @param foreignEntity from E_04 only
	 * @param singleRefField
	 * @param value
	 * @return
	 * @throws Exception
	 */
	private synchronized UID getRefUID(EntityMeta<?> foreignEntity, FieldMeta<?> singleRefField, Object value) throws Exception {
		Pair<FieldMeta<?>, Object> cachekey = new Pair<FieldMeta<?>, Object>(singleRefField, value);
		if (_refValueCache.containsKey(cachekey)) {
			return _refValueCache.get(cachekey);
		} else {
			UID foreignUID = null;
			Collection<RigidEO> eoRefs = helper.getByField(foreignEntity, singleRefField, value); // foreignEntity here uses UID as PK
			switch (eoRefs.size()) {
			case 0:
				_refValueCache.remove(cachekey);
				break;
			case 1:
				foreignUID = (UID) eoRefs.iterator().next().getPrimaryKey();
				_refValueCache.put(cachekey, foreignUID);
				break;
			default:
				throw new Exception("NotUnique");
			}
			return foreignUID;
		}
	}
	
	/**
	 * migrate after local identifiers!
	 */
	private void migrateUserDefinedForeignKeys() {
		logTitle(String.format("migrate user defined foreign keys"));
		
		// custom keys
		for (RigidEO eoEntity : entitiesById.values()) {
//			String entity = eoEntity.getValue(E_03_13_0001.ENTITY.entity);
			UID entityUID = (UID) eoEntity.getPrimaryKey();
			String virtualentity = eoEntity.getValue(E_03_15_0004.ENTITY.virtualentity);
			
			if (virtualentity == null) {
				String tablename = MetaDbHelper.getTableName(
							virtualentity,
							eoEntity.getValue(E_03_15_0004.ENTITY.dbentity)).toUpperCase();
				
				if (Boolean.TRUE.equals(eoEntity.getValue(E_03_15_0004.ENTITY.usessatemodel))) {
					try {
						LOG.debug(String.format("migrate table \"%s\" process and state column to uid", tablename));
						// create new column
						DbColumn statecolumn = new DbColumn(null, new DbNamedObject(null, tablename), SF.STATE_UID.getDbColumn(), MetaDbHelper.UID_COLUMN_TYPE, DbNullable.NULL, null, SF.STATE_UID.getOrder());
						DbColumn processcolumn = new DbColumn(null, new DbNamedObject(null, tablename), SF.PROCESS_UID.getDbColumn(), MetaDbHelper.UID_COLUMN_TYPE, DbNullable.NULL, null, SF.PROCESS_UID.getOrder());
						helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.CREATE, statecolumn));
						helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.CREATE, processcolumn));
						
						// update new columns
						for (RigidEO eoState : helper.getAll(E_03_15_0004.STATE)) {
							DbMap values = new DbMap();
							DbMap conditions = new DbMap();
							values.put(SF.STATE_UID, eoState.getForeignUID(E_03_15_0004.STATE.pkuid));
							conditions.putUnsafe(SimpleDbField.create("INTID_NUCLOSSTATE", Long.class), eoState.getPrimaryKey());
							DbUpdateStatement<Long> updstmt = new DbUpdateStatement<Long>(tablename, values, conditions);
							helper.getDbAccess().execute(updstmt);
						}
						
						for (RigidEO eoProcess : helper.getAll(E_03_15_0004.PROCESS)) {
							DbMap values = new DbMap();
							DbMap conditions = new DbMap();
							values.put(SF.PROCESS_UID, eoProcess.getForeignUID(E_03_15_0004.PROCESS.pkuid));
							conditions.putUnsafe(SimpleDbField.create("INTID_NUCLOSPROCESS", Long.class), eoProcess.getPrimaryKey());
							DbUpdateStatement<Long> updstmt = new DbUpdateStatement<Long>(tablename, values, conditions);
							helper.getDbAccess().execute(updstmt);
						}
						
						// drop old columns
						statecolumn = new DbColumn(null, new DbNamedObject(null, tablename), "INTID_NUCLOSSTATE", MetaDbHelper.ID_COLUMN_TYPE, DbNullable.NULL, null, SF.STATE.getOrder());
						processcolumn = new DbColumn(null, new DbNamedObject(null, tablename), "INTID_NUCLOSPROCESS", MetaDbHelper.ID_COLUMN_TYPE, DbNullable.NULL, null, SF.PROCESS.getOrder());
						helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.DROP, statecolumn));
						helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.DROP, processcolumn));
						
						// create foreign keys with "custom" migration
						
					} catch (Exception ex) {
						LOG.error(String.format("migrate table \"%s\" process and state column to uid failed: %s ", tablename, ex.getMessage()), ex);
					}
				}
				
				for (RigidEO eoField : _fields.values()) {
					String columnname = eoField.getValue(E_03_15_0004.ENTITYFIELD.dbfield).toUpperCase();
					Integer columnorder = eoField.getValue(E_03_15_0004.ENTITYFIELD.order);
					if (columnname.startsWith("STRVALUE_")) {
						columnname = "INTID_" + columnname.substring(9);
					}
					if (entityUID.equals(eoField.getForeignUID(E_04_00_0022_finish.ENTITYFIELD.entity))) {
						UID foreignentityUID = eoField.getForeignUID(E_04_00_0022_finish.ENTITYFIELD.foreignentity);
						if (foreignentityUID != null) {
							EntityMeta<?> sysentity4 = E_04_00_0022_finish.getByUID(foreignentityUID);
							EntityMeta<?> sysentity3 = E_03_15_0004.getByUID(foreignentityUID);
							if (sysentity4 != null && sysentity4.isUidEntity() && sysentity3 != null) {
								try {
									LOG.debug(String.format("migrate table column \"%s.%s\" to uid", tablename, columnname));
									String newcolumname;
									if (columnname.startsWith("INTID_")) {
										newcolumname = "STRUID_" + columnname.substring(6);
									} else {
										newcolumname = "STRUID_" + columnname;
									}
									// create new column
									DbColumn newcolum = new DbColumn(null, new DbNamedObject(null, tablename), newcolumname, MetaDbHelper.UID_COLUMN_TYPE, DbNullable.NULL, null, columnorder);
									helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.CREATE, newcolum));
									
									// update new column
									for (Object pk : helper.getAllPks(sysentity3)) {
										RigidEO eo = helper.getByPrimaryKey(sysentity3, pk);
										DbMap values = new DbMap();
										DbMap conditions = new DbMap();
										values.put(SimpleDbField.create(newcolumname, UID.class), eo.getForeignUID(getUIDPkField(sysentity3)));
										conditions.putUnsafe(SimpleDbField.create(columnname, Long.class), eo.getPrimaryKey());
										DbUpdateStatement<Long> updstmt = new DbUpdateStatement<Long>(tablename, values, conditions);
										helper.getDbAccess().execute(updstmt);
									}
									
									// drop old columns
									DbColumn dropcolum = new DbColumn(null, new DbNamedObject(null, tablename), columnname, MetaDbHelper.ID_COLUMN_TYPE, DbNullable.NULL, null, columnorder);
									helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.DROP, dropcolum));
									
									// alter not null
									if (Boolean.FALSE.equals(eoField.getValue(E_03_15_0004.ENTITYFIELD.nullable))) {
										DbColumn newcolumNotnull = new DbColumn(null, new DbNamedObject(null, tablename), newcolumname, MetaDbHelper.UID_COLUMN_TYPE, DbNullable.NOT_NULL, null, columnorder);
										helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.MODIFY, newcolum, newcolumNotnull));
									}
									
								} catch (Exception ex) {
									LOG.error(String.format("migrate table column \"%s.%s\" to uid failed: %s ", tablename, columnname, ex.getMessage()), ex);
								}
							}
						}
					}
				}
			}
			
		}
	}
	
	final XStream xstreamTabRestoreOld = new XStream(new StaxDriver());
	final XStream xstreamTabRestoreNew = new XStream(new StaxDriver());
	
	private void migrateWorkspaces() {
		logTitle(String.format("migrate workspaces"));
		xstreamTabRestoreOld.alias("org.nuclos.client.customcomp.CustomComponentController$RestorePreferences", WO_RestorePreferences.CustomComponent3.class);
		xstreamTabRestoreNew.alias("org.nuclos.client.customcomp.CustomComponentController$RestorePreferences", WO_RestorePreferences.CustomComponent4.class);
		xstreamTabRestoreOld.alias("org.nuclos.client.customcomp.resplan.ResPlanController$RestorePreferences", WO_RestorePreferences.ResPlan.class);
		xstreamTabRestoreNew.alias("org.nuclos.client.customcomp.resplan.ResPlanController$RestorePreferences", WO_RestorePreferences.ResPlan.class);
		xstreamTabRestoreOld.alias("org.nuclos.client.common.NuclosCollectController$RestorePreferences", WO_RestorePreferences.NuclosCollect3.class);
		xstreamTabRestoreNew.alias("org.nuclos.client.common.NuclosCollectController$RestorePreferences", WO_RestorePreferences.NuclosCollect4.class);
		xstreamTabRestoreOld.alias("org.nuclos.client.masterdata.MasterDataCollectController$RestorePreferences", WO_RestorePreferences.MDCollect3.class);
		xstreamTabRestoreNew.alias("org.nuclos.client.masterdata.MasterDataCollectController$RestorePreferences", WO_RestorePreferences.MDCollect4.class);
		xstreamTabRestoreOld.alias("org.nuclos.client.genericobject.GenericObjectCollectController$RestorePreferences", WO_RestorePreferences.GOCollect3.class);
		xstreamTabRestoreNew.alias("org.nuclos.client.genericobject.GenericObjectCollectController$RestorePreferences", WO_RestorePreferences.GOCollect4.class);
		xstreamTabRestoreOld.alias("org.nuclos.client.task.TaskController$RestorePreferences", WO_RestorePreferences.Task3.class);
		xstreamTabRestoreNew.alias("org.nuclos.client.task.TaskController$RestorePreferences", WO_RestorePreferences.Task4.class);
		
		xstreamTabRestoreOld.setMarshallingStrategy(new WO_RestorePreferences.IgnoreExplorerStrategy());
		
		final XStream xstream = new XStream(new StaxDriver());
		xstream.alias("org.nuclos.common.WorkspaceDescription", WO_WorkspaceDescriptionOld.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$Action", WO_WorkspaceDescriptionOld.Action.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$ApiDesktopItem", WO_WorkspaceDescriptionOld.ApiDesktopItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$Color", WO_WorkspaceDescriptionOld.Color.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$ColumnPreferences", WO_WorkspaceDescriptionOld.ColumnPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$ColumnSorting", WO_WorkspaceDescriptionOld.ColumnSorting.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$Desktop", WO_WorkspaceDescriptionOld.Desktop.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$DesktopItem", WO_WorkspaceDescriptionOld.DesktopItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$EntityPreferences", WO_WorkspaceDescriptionOld.EntityPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$Frame", WO_WorkspaceDescriptionOld.Frame.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$MenuButton", WO_WorkspaceDescriptionOld.MenuButton.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$MenuItem", WO_WorkspaceDescriptionOld.MenuItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$MutableContent", WO_WorkspaceDescriptionOld.MutableContent.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$NestedContent", WO_WorkspaceDescriptionOld.NestedContent.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$ProfileManager", WO_WorkspaceDescriptionOld.ProfileManager.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$Split", WO_WorkspaceDescriptionOld.Split.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$SubFormPreferences", WO_WorkspaceDescriptionOld.SubFormPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$Tab", WO_WorkspaceDescriptionOld.Tab.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$Tabbed", WO_WorkspaceDescriptionOld.Tabbed.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$TablePreferences", WO_WorkspaceDescriptionOld.TablePreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$TasklistPreferences", WO_WorkspaceDescriptionOld.TasklistPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription$MatrixPreferences", WO_WorkspaceDescriptionOld.MatrixPreferences.class);
		
		xstream.alias("org.nuclos.common.WorkspaceDescription2", WO_WorkspaceDescriptionNew.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Action", WO_WorkspaceDescriptionNew.Action.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$ApiDesktopItem", WO_WorkspaceDescriptionNew.ApiDesktopItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Color", WO_WorkspaceDescriptionNew.Color.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$ColumnPreferences", WO_WorkspaceDescriptionNew.ColumnPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$ColumnSorting", WO_WorkspaceDescriptionNew.ColumnSorting.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Desktop", WO_WorkspaceDescriptionNew.Desktop.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$DesktopItem", WO_WorkspaceDescriptionNew.DesktopItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$EntityPreferences", WO_WorkspaceDescriptionNew.EntityPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Frame", WO_WorkspaceDescriptionNew.Frame.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$MenuButton", WO_WorkspaceDescriptionNew.MenuButton.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$MenuItem", WO_WorkspaceDescriptionNew.MenuItem.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$MutableContent", WO_WorkspaceDescriptionNew.MutableContent.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$NestedContent", WO_WorkspaceDescriptionNew.NestedContent.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$ProfileManager", WO_WorkspaceDescriptionNew.ProfileManager.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Split", WO_WorkspaceDescriptionNew.Split.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$SubFormPreferences", WO_WorkspaceDescriptionNew.SubFormPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Tab", WO_WorkspaceDescriptionNew.Tab.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$Tabbed", WO_WorkspaceDescriptionNew.Tabbed.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$TablePreferences", WO_WorkspaceDescriptionNew.TablePreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$TasklistPreferences", WO_WorkspaceDescriptionNew.TasklistPreferences.class);
		xstream.alias("org.nuclos.common.WorkspaceDescription2$MatrixPreferences", WO_WorkspaceDescriptionNew.MatrixPreferences.class);
		
		
		for (Object pk : helper.getAllPks(E_03_15_0004.WORKSPACE)) {
			RigidEO eo = helper.getByPrimaryKey(E_03_15_0004.WORKSPACE, pk);
			LOG.debug(String.format("migrate workspace \"%s\" from user %s", eo.getValue(E_03_15_0004.WORKSPACE.name), eo.getValue(E_03_15_0004.WORKSPACE.useruid)));
			boolean isAssignable = eo.getValue(E_03_15_0004.WORKSPACE.useruid) == null;
			try {
				String xmlOld = eo.getValue(E_03_15_0004.WORKSPACE.clbworkspace);
				if (xstream.fromXML(xmlOld) instanceof WO_WorkspaceDescriptionOld) {
					WO_WorkspaceDescriptionOld wd = (WO_WorkspaceDescriptionOld) xstream.fromXML(xmlOld);
					WO_WorkspaceDescriptionNew wd2 = new WO_WorkspaceDescriptionNew();
					wd2.setName(wd.getName());
					wd2.setHide(wd.isHide());
					wd2.setHideName(wd.isHideName());
					wd2.setHideMenuBar(wd.isHideMenuBar());
					wd2.setAlwaysOpenAtLogin(wd.isAlwaysOpenAtLogin());
					wd2.setUseLastFrameSettings(wd.isUseLastFrameSettings());
					wd2.setAlwaysReset(wd.isAlwaysReset());
					wd2.setNuclosResource(wd.getNuclosResource());
					for (WO_WorkspaceDescriptionOld.Frame f : wd.getFrames()) {
						WO_WorkspaceDescriptionNew.Frame f2 = new WO_WorkspaceDescriptionNew.Frame();
						f2.setMainFrame(f.isMainFrame());
						f2.setNumber(f.getNumber());
						f2.setExtendedState(f.getExtendedState());
						f2.setNormalBounds(f.getNormalBounds());
						f2.getContent().setContent(migrateWorkspaceContent(f.getContent(), isAssignable));
						wd2.addFrame(f2);
					}
					for (WO_WorkspaceDescriptionOld.EntityPreferences ep : wd.getEntityPreferences()) {
						WO_WorkspaceDescriptionNew.EntityPreferences ep2 = new WO_WorkspaceDescriptionNew.EntityPreferences();
						UID entityUID = getEntityUIDByName(ep.getEntity());
						if (entityUID != null) {
							ep2.setEntity(entityUID);
							if (ep.hasResultPreferences()) {
								if (isAssignable) LOG.warn(String.format("unable to migrate workspace entity preferences: Format for entity \"%s\" is not 4.0 compatible. " + releaseNote("MIG-03"), ep.getEntity()));
							}
							for (WO_WorkspaceDescriptionOld.TablePreferences tp : ep.getProfiles()) {
								ep2.addProfile(migrateWorkspaceTablePreferences(ep.getEntity(), tp, isAssignable));
							}
							for (WO_WorkspaceDescriptionOld.TablePreferences tp : ep.getPersonalProfiles()) {
								ep2.addPersonalProfile(migrateWorkspaceTablePreferences(ep.getEntity(), tp, isAssignable));
							}
							for (WO_WorkspaceDescriptionOld.SubFormPreferences sp : ep.getSubFormPreferences()) {
								WO_WorkspaceDescriptionNew.SubFormPreferences sp2 = new WO_WorkspaceDescriptionNew.SubFormPreferences();
								if (sp.hasTablePreferences()) {
									if (isAssignable) LOG.warn(String.format("unable to migrate workspace subform preferences: Format for subform \"%s\" is not 4.0 compatible. " + releaseNote("MIG-03"), sp.getEntity()));
								}
								UID subUID = getEntityUIDByName(sp.getEntity());
								if (subUID != null) {
									sp2.setEntity(subUID);
									for (WO_WorkspaceDescriptionOld.TablePreferences tp : sp.getProfiles()) {
										sp2.addProfile(migrateWorkspaceTablePreferences(sp.getEntity(), tp, isAssignable));
									}
									for (WO_WorkspaceDescriptionOld.TablePreferences tp : sp.getPersonalProfiles()) {
										sp2.addPersonalProfile(migrateWorkspaceTablePreferences(sp.getEntity(), tp, isAssignable));
									}
									ep2.addSubFormPreferences(sp2);
								} else {
									if (sp.getEntity().startsWith("dyn_")) {
										if (isAssignable) LOG.warn(String.format("unable to migrate workspace subform preferences: Dynamic subform \"%s\" removed. Please open and save the underlaying datasource to generate meta data. " + releaseNote("MIG-02"), sp.getEntity()));
				            		} else {
				            			if (isAssignable) LOG.warn("unable to migrate workspace subform preferences: Subform \"" + sp.getEntity() + "\" does not exist");
				            		}
								}
							}
							for (WO_WorkspaceDescriptionOld.MatrixPreferences mp : ep.getMatrixPreferences()) {
								ep2.addMatrixPreferences(migrateWorkspaceMatrixPreferences(mp, isAssignable));
							}
							ep2.addAllLayoutComponentPreferences(ep.getAllLayoutComponentPreferences());
							wd2.addEntityPreferences(ep2);
						} else {
							if (isAssignable) LOG.warn("unable to migrate workspace entity preferences: Entity \"" + ep.getEntity() + "\" does not exist");
						}
					}
					for (WO_WorkspaceDescriptionOld.TasklistPreferences tp : wd.getTasklistPreferences()) {
						WO_WorkspaceDescriptionNew.TasklistPreferences tp2 = new WO_WorkspaceDescriptionNew.TasklistPreferences();
						tp2.setType(tp.getType());
						tp2.setName(tp.getName());
						tp2.setTablePreferences(migrateWorkspaceTablePreferences(null, tp.getTablePreferences(), isAssignable));
						wd2.addTasklistPreferences(tp2);
					}
					for (Map.Entry<String, String> param : wd.getParameters().entrySet()) {
						wd2.setParameter(param.getKey(), param.getValue());
					}
					String xmlNew = xstream.toXML(wd2);
					eo.setValue(E_03_15_0004.WORKSPACE.clbworkspace, xmlNew);
					helper.update(E_03_15_0004.WORKSPACE, eo);
				} else {
					LOG.warn("E_03_14_0003.WORKSPACE.clbworkspace not instance of WO_WorkspaceDescriptionOld.");
				}
			} catch (Exception ex) {
				LOG.warn("unable to migrate workspace with pk " + eo.getPrimaryKey(), ex);
			} catch (java.lang.LinkageError ex) {
				LOG.warn("unable to migrate workspace with pk " + eo.getPrimaryKey(), ex);
			}
		}

	}
	
	private WO_WorkspaceDescriptionNew.NestedContent migrateWorkspaceContent(WO_WorkspaceDescriptionOld.NestedContent nc, boolean isAssignable) {
		if (nc instanceof WO_WorkspaceDescriptionOld.MutableContent) {
			return migrateWorkspaceContent(((WO_WorkspaceDescriptionOld.MutableContent) nc).getContent(), isAssignable);
		} else if (nc instanceof WO_WorkspaceDescriptionOld.Split) {
			WO_WorkspaceDescriptionOld.Split sp = (WO_WorkspaceDescriptionOld.Split) nc;
			WO_WorkspaceDescriptionNew.Split sp2 = new WO_WorkspaceDescriptionNew.Split();
			sp2.setHorizontal(sp.isHorizontal());
			sp2.setPosition(sp.getPosition());
			sp2.setFixedState(sp.getFixedState());
			sp2.getContentA().setContent(migrateWorkspaceContent(sp.getContentA(), isAssignable));
			sp2.getContentB().setContent(migrateWorkspaceContent(sp.getContentB(), isAssignable));
			return sp2;
		} else if (nc instanceof WO_WorkspaceDescriptionOld.Tabbed) {
			WO_WorkspaceDescriptionOld.Tabbed ta = (WO_WorkspaceDescriptionOld.Tabbed) nc;
			WO_WorkspaceDescriptionNew.Tabbed ta2 = new WO_WorkspaceDescriptionNew.Tabbed();
			ta2.setHome(ta.isHome());
			ta2.setHomeTree(ta.isHomeTree());
			ta2.setShowEntity(ta.isShowEntity());
			ta2.setShowAdministration(ta.isShowAdministration());
			ta2.setShowConfiguration(ta.isShowConfiguration());
			ta2.setNeverHideStartmenu(ta.isNeverHideStartmenu());
			ta2.setNeverHideHistory(ta.isNeverHideHistory());
			ta2.setNeverHideBookmark(ta.isNeverHideBookmark());
			ta2.setAlwaysHideStartmenu(ta.isAlwaysHideStartmenu());
			ta2.setAlwaysHideHistory(ta.isAlwaysHideHistory());
			ta2.setAlwaysHideBookmark(ta.isAlwaysHideBookmark());
			ta2.setDesktopActive(ta.isDesktopActive());
			ta2.setHideStartTab(ta.isHideStartTab());
			ta2.setSelected(ta.getSelected());
			ta2.addAllReducedStartmenus(ta.getReducedStartmenus());
			ta2.addAllReducedHistoryEntities(ta.getReducedHistoryEntities());
			ta2.addAllReducedBookmarkEntities(ta.getReducedBookmarkEntities());
			for(String entity : ta.getPredefinedEntityOpenLocations()) {
				UID entityUID = getEntityUIDByName(entity);
				if (entityUID != null) {
					ta2.addPredefinedEntityOpenLocation(getEntityUIDByName(entity));
				}
			}
			for(WO_WorkspaceDescriptionOld.Tab tab : ta.getTabs()) {
				WO_WorkspaceDescriptionNew.Tab tab2 = new WO_WorkspaceDescriptionNew.Tab();
				tab2.setLabel(tab.getLabel());
				tab2.setIconResolver(tab.getIconResolver());
				tab2.setIcon(tab.getIcon());
				tab2.setNeverClose(tab.isNeverClose());
				tab2.setFromAssigned(tab.isFromAssigned());
				tab2.setRestoreController(tab.getRestoreController());
				if (!RigidUtils.looksEmpty(tab.getPreferencesXML())) {
					try {
						Object objRestore = xstreamTabRestoreOld.fromXML(tab.getPreferencesXML());
						if (objRestore == null) {
							// restore of explorer nodes not implemented. to complex
							continue;
						} else if (objRestore instanceof CustomComponent3) {
							CustomComponent3 cc3 = (CustomComponent3) objRestore;
							
							Collection<RigidEO> ccs = helper.getByField(E_03_15_0004.CUSTOMCOMPONENT, E_03_15_0004.CUSTOMCOMPONENT.name, cc3.customComponentName);
							if (ccs.isEmpty()) {
								LOG.debug(String.format("ignore tab restore: custom component \"%s\" does not exist", cc3.customComponentName));
								continue;
							}
							Long id = (Long) ccs.iterator().next().getPrimaryKey();
							CustomComponent4 cc4 = new CustomComponent4();
							cc4.customComponentUID = getUID(E_03_15_0004.CUSTOMCOMPONENT.getEntityName(), id);
							cc4.customComponentClass = cc3.customComponentClass;
							cc4.customComponentName = cc3.customComponentName;
							cc4.instanceStateXML = cc3.instanceStateXML;
							if (!RigidUtils.looksEmpty(cc3.instanceStateXML)) {
								Object objInstanceRestore = xstreamTabRestoreOld.fromXML(tab.getPreferencesXML());
								if (objInstanceRestore != null) {
									// remove SearchCondition from ResPlan
									cc4.instanceStateXML = xstreamTabRestoreNew.toXML(objInstanceRestore);
								}
							}
							tab2.setPreferencesXML(xstreamTabRestoreNew.toXML(cc4));
						} else if (objRestore instanceof NuclosCollect3) {
							NuclosCollect3 nc3 = (NuclosCollect3) objRestore;
							
							NuclosCollect4 nc4 = new NuclosCollect4();
							nc4.customUsage = nc3.customUsage;
							nc4.iCollectState = nc3.iCollectState;
							
							nc4.objectId = nc3.objectId;
							UID entityUID = getEntityUIDByName(nc3.entity);
							if (entityUID != null && 
									E_04_00_0022_main.getByUID(entityUID) != null && 
									E_04_00_0022_main.getByUID(entityUID).isUidEntity()) {
								LOG.debug(String.format("ignore tab restore: entity \"%s\" is uid entity", nc3.entity));
								continue;
							}
							if (entityUID == null) {
								LOG.debug(String.format("ignore tab restore: entity \"%s\" does not exist", nc3.entity));
								continue;
							}
							
							nc4.inheritControllerPreferences = nc3.inheritControllerPreferences;
							if (nc4.inheritControllerPreferences != null) {
								for (String key : nc4.inheritControllerPreferences.keySet()) {
									String value = nc4.inheritControllerPreferences.get(key);
									if (value != null) {
										Object objInstanceRestore = xstreamTabRestoreOld.fromXML(value);
										if (objInstanceRestore instanceof MDCollect3) {
											MDCollect3 mdc3 = (MDCollect3) objInstanceRestore;
											MDCollect4 mdc4 = new MDCollect4();
											
											if (mdc3.searchFilterName != null) {
												Collection<RigidEO> sfs = helper.getByField(E_03_15_0004.SEARCHFILTER, E_03_15_0004.SEARCHFILTER.name, mdc3.searchFilterName);
												if (sfs.isEmpty()) {
													LOG.debug(String.format("ignore tab restore: search filter \"%s\" does not exist", mdc3.searchFilterName));
													continue;
												}
												if (sfs.size() > 1) {
													LOG.debug(String.format("ignore tab restore: search filter \"%s\" is not unique", mdc3.searchFilterName));
													continue;
												}
												mdc4.searchFilterUID = getUID(E_03_15_0004.SEARCHFILTER.getEntityName(), (Long) sfs.iterator().next().getPrimaryKey());
											}
											
											nc4.inheritControllerPreferences.put(key, xstreamTabRestoreNew.toXML(mdc4));
										} else if (objInstanceRestore instanceof GOCollect3) {
											GOCollect3 goc3 = (GOCollect3) objInstanceRestore;
											GOCollect4 goc4 = new GOCollect4();
											
											if (goc3.processId != null) {
												UID processUID = getUID(E_03_15_0004.PROCESS.getEntityName(), goc3.processId.longValue());
												goc4.processUID = processUID;
											}
											
											if (goc3.searchFilterName != null) {
												Collection<RigidEO> sfs = helper.getByField(E_03_15_0004.SEARCHFILTER, E_03_15_0004.SEARCHFILTER.name, goc3.searchFilterName);
												if (sfs.isEmpty()) {
													LOG.debug(String.format("ignore tab restore: search filter \"%s\" does not exist", goc3.searchFilterName));
													continue;
												}
												if (sfs.size() > 1) {
													LOG.debug(String.format("ignore tab restore: search filter \"%s\" is not unique", goc3.searchFilterName));
													continue;
												}
												goc4.searchFilterUID = getUID(E_03_15_0004.SEARCHFILTER.getEntityName(), (Long) sfs.iterator().next().getPrimaryKey());
											}
											
											nc4.inheritControllerPreferences.put(key, xstreamTabRestoreNew.toXML(goc4));
										}
									}
								}
							}
							
							nc4.entity = entityUID;
							tab2.setPreferencesXML(xstreamTabRestoreNew.toXML(nc4));
						} else if (objRestore instanceof Task3) {
							Task3 ta3 = (Task3) objRestore;
							Task4 ta4 = new Task4();
							ta4.type = ta3.type;
							ta4.tasklistName = ta3.tasklistName;
							ta4.refreshInterval = ta3.refreshInterval;
							
							if (ta3.searchFilterId != null) {
								UID searchFilterUID = getUID(E_03_15_0004.SEARCHFILTER.getEntityName(), ta3.searchFilterId.longValue());
								ta4.searchFilterId = searchFilterUID;
							}
							if (ta3.tasklistId != null) {
								UID tasklistUID = getUID(E_03_15_0004.DYNAMICTASKLIST.getEntityName(), ta3.tasklistId.longValue());
								ta4.tasklistId = tasklistUID;
							}
							
							tab2.setPreferencesXML(xstreamTabRestoreNew.toXML(ta4));
						}
					} catch (Exception ex) {
						LOG.debug(String.format("ignore tab restore: " + ex.getMessage()));
						LOG.debug(tab.getPreferencesXML());
						tab.setPreferencesXML(null);
					} catch (java.lang.LinkageError ex) {
						LOG.debug(String.format("ignore tab restore: " + ex.getMessage()));
						LOG.debug(tab.getPreferencesXML());
						tab.setPreferencesXML(null);
					}
				}
				ta2.addTab(tab2);
			}
			WO_WorkspaceDescriptionOld.Desktop de = ta.getDesktop();
			if (de != null) {
				WO_WorkspaceDescriptionNew.Desktop de2 = new WO_WorkspaceDescriptionNew.Desktop();
				de2.setHorizontalGap(de.getHorizontalGap());
				de2.setVerticalGap(de.getVerticalGap());
				de2.setMenuItemTextSize(de.getMenuItemTextSize());
				de2.setLayout(de.getLayout());
				de2.setMenuItemTextHorizontalPadding(de.getMenuItemTextHorizontalPadding());
				de2.setMenuItemTextHorizontalAlignment(de.getMenuItemTextHorizontalAlignment());
				de2.setMenuItemTextColor(migrateWorkspaceColor(de.getMenuItemTextColor()));
				de2.setMenuItemTextHoverColor(migrateWorkspaceColor(de.getMenuItemTextHoverColor()));
				de2.setResourceMenuBackground(getResourceUID(de.getResourceMenuBackground()));
				de2.setResourceMenuBackgroundHover(getResourceUID(de.getResourceMenuBackgroundHover()));
				de2.setResourceBackground(getResourceUID(de.getResourceBackground()));
				de2.setNuclosResourceBackground(de.getNuclosResourceBackground());
				de2.setResourceBackgroundNorthWest(getResourceUID(de.getResourceBackgroundNorthWest()));
				de2.setResourceBackgroundNorth(getResourceUID(de.getResourceBackgroundNorth()));
				de2.setResourceBackgroundNorthEast(getResourceUID(de.getResourceBackgroundNorthEast()));
				de2.setResourceBackgroundWest(getResourceUID(de.getResourceBackgroundWest()));
				de2.setResourceBackgroundCenter(getResourceUID(de.getResourceBackgroundCenter()));
				de2.setResourceBackgroundEast(getResourceUID(de.getResourceBackgroundEast()));
				de2.setResourceBackgroundSouthWest(getResourceUID(de.getResourceBackgroundSouthWest()));
				de2.setResourceBackgroundSouth(getResourceUID(de.getResourceBackgroundSouth()));
				de2.setResourceBackgroundSouthEast(getResourceUID(de.getResourceBackgroundSouthEast()));
				de2.setHideToolBar(de.isHideToolBar());
				de2.setHideTabBar(de.isHideTabBar());
				de2.setRootpaneBackgroundColor(de.isRootpaneBackgroundColor());
				de2.setStaticMenu(de.isStaticMenu());
				for (WO_WorkspaceDescriptionOld.DesktopItem di : de.getDesktopItems()) {
					if (di instanceof WO_WorkspaceDescriptionOld.MenuButton) {
						WO_WorkspaceDescriptionOld.MenuButton mb = (WO_WorkspaceDescriptionOld.MenuButton) di;
						WO_WorkspaceDescriptionNew.MenuButton mb2 = new WO_WorkspaceDescriptionNew.MenuButton();
						mb2.setMenuAction(migrateWorkspaceAction(mb.getMenuAction(), isAssignable));
						mb2.setResourceIcon(getResourceUID(mb.getResourceIcon()));
						mb2.setResourceIconHover(getResourceUID(mb.getResourceIconHover()));
						mb2.setNuclosResource(mb.getNuclosResource());
						mb2.setNuclosResourceHover(mb.getNuclosResourceHover());
						for (WO_WorkspaceDescriptionOld.MenuItem mi : mb.getMenuItems()) {
							WO_WorkspaceDescriptionNew.MenuItem mi2 = new WO_WorkspaceDescriptionNew.MenuItem();
							mi2.setMenuAction(migrateWorkspaceAction(mi.getMenuAction(), isAssignable));
							mb2.addMenuItem(mi2);
						}
						de2.addDesktopItem(mb2);
					} else if (di instanceof WO_WorkspaceDescriptionOld.ApiDesktopItem) {
						WO_WorkspaceDescriptionOld.ApiDesktopItem api = new WO_WorkspaceDescriptionOld.ApiDesktopItem();
						WO_WorkspaceDescriptionNew.ApiDesktopItem api2 = new WO_WorkspaceDescriptionNew.ApiDesktopItem();
						api2.setId(api.getId());
						de2.addDesktopItem(api2);
					} else {
						if (isAssignable) LOG.warn("unable to migrate workspace desktop item " + di);
					}
				}
				ta2.setDesktop(de2);
			}
			return ta2;
		} else {
			throw new IllegalArgumentException("Unsupported workspace content type: " + nc.getClass().getName());
		}
	}
	
	private WO_WorkspaceDescriptionNew.TablePreferences migrateWorkspaceTablePreferences(String entity, WO_WorkspaceDescriptionOld.TablePreferences tp, boolean isAssignable) {
		WO_WorkspaceDescriptionNew.TablePreferences tp2 = new WO_WorkspaceDescriptionNew.TablePreferences();
		tp2.setName(tp.getName());
		tp2.setActive(tp.getIsActive());
		tp2.setDynamicRowHeight(tp.getIsDynamicRowHeight());
		for (WO_WorkspaceDescriptionOld.ColumnPreferences cp : tp.getSelectedColumnPreferences()) {
			WO_WorkspaceDescriptionNew.ColumnPreferences cp2 = new WO_WorkspaceDescriptionNew.ColumnPreferences();
			String e = RigidUtils.defaultIfNull(cp.getEntity(), entity);
			UID entityUID = getEntityUIDByName(e);
			UID columnUID = getFieldUIDByNames(entity, cp.getColumn());
			if (columnUID != null && entityUID != null) {
				cp2.setColumn(columnUID);
				cp2.setEntity(entityUID);
				cp2.setWidth(cp.getWidth());
				cp2.setFixed(cp.isFixed());
				cp2.setType(cp.getType());
				if (cp.getPivotSubForm() != null) {
					// ignore
					continue;
				}
				tp2.addSelectedColumnPreferences(cp2);
			} else {
				if (isAssignable) LOG.warn("unable to migrate workspace column: \"" + e + "." + cp.getColumn() + "\" does not exist");
			}
		}
		for (String column : tp.getHiddenColumns()) {
			UID columnUID = getFieldUIDByNames(entity, column);
			if (columnUID != null) {
				tp2.addHiddenColumn(columnUID);
			} else {
				if (isAssignable) LOG.warn("unable to migrate workspace column: \"" + entity + "." + entity + "\" does not exist");
			}
		}
		for (WO_WorkspaceDescriptionOld.ColumnSorting cs : tp.getColumnSortings()) {
			WO_WorkspaceDescriptionNew.ColumnSorting cs2 = new WO_WorkspaceDescriptionNew.ColumnSorting();
			UID columnUID = getFieldUIDByNames(entity, cs.getColumn());
			if (columnUID != null) {
				cs2.setColumn(columnUID);
				cs2.setAsc(cs.isAsc());
				tp2.addColumnSorting(cs2);
			} else {
				if (isAssignable) LOG.warn("unable to migrate workspace column: \"" + entity + "." + cs.getColumn() + "\" does not exist");
			}
		}
		return tp2;
	}
	
	private WO_WorkspaceDescriptionNew.Action migrateWorkspaceAction(WO_WorkspaceDescriptionOld.Action ac, boolean isAssignable) {
		if (ac == null) {
			return null;
		}
		WO_WorkspaceDescriptionNew.Action ac2 = new WO_WorkspaceDescriptionNew.Action();
		ac2.setAction(ac.getAction());
		for (String key : ac.getStringParams().keySet()) {
			if ("entity".equals(key)) {
				UID uid = getEntityUIDByName(ac.getStringParameter(key));
				if (uid != null) {
					ac2.putStringParameter(key, uid.getString());
				} else {
					if (isAssignable) LOG.warn("unable to migrate workspace action parameter entity=\"" + ac.getStringParameter(key) + "\" does not exist");
				}
			} else if ("report".equals(key)) {
				Collection<RigidEO> reports = helper.getByField(E_03_15_0004.REPORT, E_03_15_0004.REPORT.name, ac.getStringParameter(key));
				if (reports.size()==1) {
					ac2.putStringParameter(key, getUID(E_03_15_0004.REPORT, (Long) reports.iterator().next().getPrimaryKey()).getString());
				} else {
					if (isAssignable) LOG.warn("unable to migrate workspace action parameter report=\"" + ac.getStringParameter(key) + "\" does not exist or is not unique");
				}
			} else if ("searchfilter".equals(key)) {
				Collection<RigidEO> searchfilters = helper.getByField(E_03_15_0004.SEARCHFILTER, E_03_15_0004.SEARCHFILTER.name, ac.getStringParameter(key));
				if (searchfilters.size()==1) {
					ac2.putStringParameter(key, getUID(E_03_15_0004.SEARCHFILTER, (Long) searchfilters.iterator().next().getPrimaryKey()).getString());
				} else {
					if (isAssignable) LOG.warn("unable to migrate workspace action parameter searchfilter=\"" + ac.getStringParameter(key) + "\" does not exist or is not unique");
				}
			} else if ("process".equals(key)) {
				Collection<RigidEO> processes = helper.getByField(E_03_15_0004.PROCESS, E_03_15_0004.PROCESS.name, ac.getStringParameter(key));
				if (processes.size()==1) {
					ac2.putStringParameter(key, getUID(E_03_15_0004.PROCESS, (Long) processes.iterator().next().getPrimaryKey()).getString());
				} else {
					if (isAssignable) LOG.warn("unable to migrate workspace action parameter process=\"" + ac.getStringParameter(key) + "\" does not exist or is not unique");
				}
			} else if ("name".equals(key)) {
				Collection<RigidEO> tasklists = helper.getByField(E_03_15_0004.TASKLIST, E_03_15_0004.TASKLIST.name, ac.getStringParameter(key));
				if (tasklists.size()==1) {
					ac2.putStringParameter("tasklist", getUID(E_03_15_0004.TASKLIST, (Long) tasklists.iterator().next().getPrimaryKey()).getString());
				} else {
					if (isAssignable) LOG.warn("unable to migrate workspace action parameter tasklist=\"" + ac.getStringParameter(key) + "\" does not exist or is not unique");
				}
			} else {
				ac2.putStringParameter(key, ac.getStringParameter(key));
			}
		}
		for (String key : ac.getLongParams().keySet()) {
			ac2.putLongParameter(key, ac.getLongParameter(key));
		}
		for (String key : ac.getBooleanParams().keySet()) {
			ac2.putBooleanParameter(key, ac.getBooleanParameter(key));
		}
		return ac2;
	}
	
	private WO_WorkspaceDescriptionNew.Color migrateWorkspaceColor(WO_WorkspaceDescriptionOld.Color co) {
		if (co == null) {
			return null;
		}
		return new WO_WorkspaceDescriptionNew.Color(co.getRed(), co.getGreen(), co.getBlue());
	}
	
	private WO_WorkspaceDescriptionNew.MatrixPreferences migrateWorkspaceMatrixPreferences(WO_WorkspaceDescriptionOld.MatrixPreferences mp, boolean isAssignable) {
		WO_WorkspaceDescriptionNew.MatrixPreferences mp2 = new WO_WorkspaceDescriptionNew.MatrixPreferences();
		if (mp.getName()!=null) mp2.setName(new UID(mp.getName()));
		mp2.setAvailable(mp.getAvailable());
		mp2.setAvailableGroupElements(mp.getAvailableGroupElements());
		mp2.setSelected(mp.getSelected());
		mp2.setSelectedGroupElements(mp.getSelectedGroupElements());
		mp2.setShowOnly(mp.getShowOnly());
		mp2.setSortKeys(mp.getSortKeys());
		String fixedEntity = mp.getFixedEntity();
		if (fixedEntity!=null) {
			if (mp.getAvailablefixed() != null) {
				SortedSet<UID> newAvailablefixed = new TreeSet<UID>();
				for (String availablefixed : mp.getAvailablefixed()) {
					UID fieldUID = getFieldUIDByNames(fixedEntity, availablefixed);
					if (fieldUID != null) {
						newAvailablefixed.add(fieldUID);
					} else {
						if (isAssignable) LOG.warn("unable to migrate matrix available fixed: \"" + fixedEntity + "." + availablefixed + "\" does not exist");
					}
				}
				mp2.setAvailablefixed(newAvailablefixed);
			}
			if (mp.getFixedFieldWidths() != null) {
				Map<UID, Integer> newFixedFieldWidths = new HashMap<UID, Integer>();
				for (Entry<String, Integer> fixedFieldWidth : mp.getFixedFieldWidths().entrySet()) {
					UID fieldUID = getFieldUIDByNames(fixedEntity, fixedFieldWidth.getKey());
					if (fieldUID != null) {
						newFixedFieldWidths.put(fieldUID, fixedFieldWidth.getValue());
					} else {
						if (isAssignable) LOG.warn("unable to migrate matrix fixed field width: \"" + fixedEntity + "." + fixedFieldWidth.getKey() + "\" does not exist");
					}
				}
				mp2.setFixedFieldWidths(newFixedFieldWidths);
			}
			if (mp.getSelectedfixed() != null) {
				List<UID> newSelectedFixed = new ArrayList<UID>();
				for (String selectedfixed : mp.getSelectedfixed()) {
					UID fieldUID = getFieldUIDByNames(fixedEntity, selectedfixed);
					if (fieldUID != null) {
						newSelectedFixed.add(fieldUID);
					} else {
						if (isAssignable) LOG.warn("unable to migrate matrix selected fixed: \"" + fixedEntity + "." + selectedfixed + "\" does not exist");
					}
				}
				mp2.setSelectedfixed(newSelectedFixed);
			}
		} else {
			if (isAssignable) LOG.warn(String.format("unable to migrate workspace matrix preferences: Format for matrix \"%s\" is not 4.0 compatible. " + releaseNote("MIG-03"), mp.getName()));
		}
		return mp2;
	}
	
	private UID getResourceUID(String sResource) {
		if (sResource == null) {
			return null;
		}
		for (RigidEO eoResource : helper.getAll(E_03_15_0004.RESOURCE)) {
			if (sResource.equals(eoResource.getValue(E_03_15_0004.RESOURCE.name))) {
				return eoResource.getForeignUID(E_03_15_0004.RESOURCE.pkuid);
			}
		}
		return null;
	}
	
	private void resetUserPreferences() {
		logTitle(String.format("reset user preferences"));
		for (Object pk : helper.getAllPks(E_03_15_0004.USER)) {
			RigidEO eo = helper.getByPrimaryKey(E_03_15_0004.USER, pk);
			eo.setValue(E_03_15_0004.USER.preferences, null);
			helper.update(E_03_15_0004.USER, eo);
		}
	}
	
	private void resetStateModelLayouts() {
		logTitle(String.format("reset state model layouts"));
		for (RigidEO eo : helper.getAll(E_03_15_0004.STATEMODEL)) {
			eo.setValue(E_03_15_0004.STATEMODEL.layout, null);
			helper.update(E_03_15_0004.STATEMODEL, eo);
		}
	}
	
	private void migrateLayouts() {		
		logTitle(String.format("migrate layouts"));
		for (RigidEO eo : helper.getAll(E_03_15_0004.LAYOUT)) {
			String layoutname = eo.getValue(E_03_15_0004.LAYOUT.name);
			LOG.debug(String.format("migrate layout \"%s\"", layoutname));
			try {
				String entity = null;
				for (RigidEO usage : helper.getAll(E_03_15_0004.LAYOUTUSAGE)) {
					if (eo.getPrimaryKey().equals(usage.getForeignID(E_03_15_0004.LAYOUTUSAGE.layout))) {
						if (entity != null && !entity.equals(usage.getValue(E_03_15_0004.LAYOUTUSAGE.entity))) {
							entity = null;
							break;
						} else {
							entity = usage.getValue(E_03_15_0004.LAYOUTUSAGE.entity);
						}
					}
				}
				Long entityId = entityIdsByName.get(entity);
				if (entity == null || entityId == null) {
					LOG.warn(String.format("entity for layout \"%s\" not found or not unique (entity=%s, ID=%s)", layoutname, entity, entityId));
					continue;
				}
				
				String layoutML = eo.getValue(E_03_15_0004.LAYOUT.layoutML);				
				
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	            DocumentBuilder builder = factory.newDocumentBuilder();
	            builder.setEntityResolver(new LayoutDTDResolver());
	            Document document = builder.parse(new InputSource(new StringReader(layoutML)));
	            
	            List<Node> nodesToRemove = new ArrayList<Node>();
	            
	            // collectable-component
	            NodeList collectableFields = document.getElementsByTagName("collectable-component");
	            for (int i = 0; i < collectableFields.getLength(); i++) {
	            	Node collectableComponent = collectableFields.item(i);
	            	NamedNodeMap compAttributes = collectableComponent.getAttributes();
	            	Node name = compAttributes.getNamedItem("name");
	            	String fieldName = name.getTextContent();
	            	UID fieldUID = getFieldUIDByNames(entity, fieldName);
	            	if (fieldUID != null) {
	            		name.setTextContent(fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
	            	} else {
	            		LOG.warn(String.format("layout \"%s\": Field \"%s\" in entity \"%s\" does not exist", layoutname, fieldName, entity));
	            		name.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
	            	}
	            	migrateLayoutNextFocus(compAttributes, document, layoutname, entity);
	            }
	            
	            // initial-focus-component
	            NodeList initialFocusComponents = document.getElementsByTagName("initial-focus-component");
	            for (int i = 0; i < initialFocusComponents.getLength(); i++) {
	            	Node initialFocusComponent = initialFocusComponents.item(i);
	            	NamedNodeMap compAttributes = initialFocusComponent.getAttributes();
	            	Node name = compAttributes.getNamedItem("name");
	            	Node entityNode = compAttributes.getNamedItem("entity");
	            	String fieldName = name.getTextContent();
	            	String focusentityName = entity;
	            	if (entityNode != null && entityNode.getTextContent() != null) {
	            		focusentityName = entityNode.getTextContent();
	            		UID focusentityUID = getEntityUIDByName(focusentityName);
	            		if (focusentityUID != null) {
	            			entityNode.setTextContent(focusentityUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
		            	} else {
		            		LOG.warn(String.format("layout \"%s\" initialFocus: Entity \"%s\" does not exist", layoutname, focusentityName));
		            		nodesToRemove.add(initialFocusComponent);
		            		continue;
		            	}
	            	}
	            	UID fieldUID = getFieldUIDByNames(focusentityName, fieldName);
	            	if (fieldUID != null) {
	            		name.setTextContent(fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
	            	} else {
	            		LOG.warn(String.format("layout \"%s\" initialFocus: Field \"%s\" in entity \"%s\" does not exist", layoutname, fieldName, focusentityName));
	            		nodesToRemove.add(initialFocusComponent);
	            	}
	            }
	            
	            // subform
	            NodeList subforms = document.getElementsByTagName("subform");
	            for (int i = 0; i < subforms.getLength(); i++) {
	            	Node subform = subforms.item(i);
	            	NamedNodeMap subformAttributes = subform.getAttributes();
	            	Node entityNode = subformAttributes.getNamedItem("entity");
	            	String subformEntityName = entityNode.getTextContent();
	            	UID subformUID = getEntityUIDByName(subformEntityName);
	            	if (subformUID != null) {
	            		entityNode.setTextContent(subformUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
	            		
	            		Node uniquemastercolumnNode = subformAttributes.getNamedItem("unique-mastercolumn");
	            		if (uniquemastercolumnNode != null) {
	            			String uniquemastercolumnName = uniquemastercolumnNode.getTextContent();
	            			UID fieldUID = getFieldUIDByNames(subformEntityName, uniquemastercolumnName);
    		            	if (fieldUID != null) {
    		            		uniquemastercolumnNode.setTextContent(fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
    		            	} else {
    		            		LOG.warn(String.format("layout \"%s\": unique master column \"%s\" in entity \"%s\" does not exist", layoutname, uniquemastercolumnName, subformEntityName));
    		            		subformAttributes.removeNamedItem("unique-mastercolumn");
    		            	}
	            		}
	            		Node foreignkeyfieldtoparentNode = subformAttributes.getNamedItem("foreignkeyfield-to-parent");
	            		if (foreignkeyfieldtoparentNode != null) {
	            			String foreignkeyfieldtoparentName = foreignkeyfieldtoparentNode.getTextContent();
	            			UID fieldUID = getFieldUIDByNames(subformEntityName, foreignkeyfieldtoparentName);
    		            	if (fieldUID != null) {
    		            		foreignkeyfieldtoparentNode.setTextContent(fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
    		            	} else {
    		            		LOG.warn(String.format("layout \"%s\": foreign keyfield to parent \"%s\" in entity \"%s\" does not exist", layoutname, foreignkeyfieldtoparentName, subformEntityName));
    		            		subformAttributes.removeNamedItem("foreignkeyfield-to-parent");
    		            	}
	            		}
	            		Node parentsubformNode = subformAttributes.getNamedItem("parent-subform");
	            		if (parentsubformNode != null) {
	            			String parentsubformName = parentsubformNode.getTextContent();
	            			UID entityUID = getEntityUIDByName(parentsubformName);
    		            	if (entityUID != null) {
    		            		parentsubformNode.setTextContent(entityUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
    		            	} else {
    		            		LOG.warn(String.format("layout \"%s\": parent subform \"%s\" does not exist", layoutname, parentsubformName));
    		            		subformAttributes.removeNamedItem("parent-subform");
    		            	}
	            		}
	            		
	            		NodeList subformChildren = subform.getChildNodes();
	            		for (int j = 0; j < subformChildren.getLength(); j++) {
	            			Node child = subformChildren.item(j);
	            			if ("subform-column".equals(child.getNodeName())) {
	            				NamedNodeMap colAttributes = child.getAttributes();
	        	            	Node name = colAttributes.getNamedItem("name");
	        	            	String fieldName = name.getTextContent();
	        	            	UID fieldUID = getFieldUIDByNames(subformEntityName, fieldName);
	    		            	if (fieldUID != null) {
	    		            		name.setTextContent(fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
	    		            	} else {
	    		            		LOG.warn(String.format("layout \"%s\": Subform column \"%s\" in entity \"%s\" does not exist", layoutname, fieldName, subformEntityName));
	    		            		nodesToRemove.add(child);
	    		            	}
	    		            	Node nextfocuscomponent = colAttributes.getNamedItem("nextfocuscomponent");
	    		            	if (nextfocuscomponent != null) {
	    		            		String nfc = nextfocuscomponent.getTextContent();
		    		            	UID nfcUID = getFieldUIDByNames(subformEntityName, nfc);
	    		            		if (nfcUID != null) {
	    			            		Attr nextfocusfield = document.createAttribute("nextfocusfield");
	    		            			nextfocusfield.setTextContent(nfcUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
	    		            			colAttributes.setNamedItem(nextfocusfield);
	    		            		} else {
	    		            			LOG.warn(String.format("layout \"%s\": Subform next focus column \"%s\" in entity \"%s\" does not exist", layoutname, nfc, subformEntityName));
	    		            		}
	    		            		colAttributes.removeNamedItem("nextfocuscomponent");
	    		            	}
	            			}
	            		}
	            	} else {
	            		if (subformEntityName.startsWith("dyn_")) {
	            			LOG.warn(String.format("layout \"%s\": Dynamic subform for entity \"%s\" removed. Please open and save the underlaying datasource to generate meta data. Then the dynamic entity subform could be add again. " + releaseNote("MIG-02"), layoutname, subformEntityName));
	            		} else {
	            			LOG.warn(String.format("layout \"%s\": Subform entity \"%s\" does not exist", layoutname, subformEntityName));
	            		}
	            		
	            		nodesToRemove.add(subform);
	            	}
	            }
	            
	            // chart
	            NodeList charts = document.getElementsByTagName("chart");
	            for (int i = 0; i < charts.getLength(); i++) {
	            	Node chart = charts.item(i);
	            	NamedNodeMap chartAttributes = chart.getAttributes();
	            	Node entityNode = chartAttributes.getNamedItem("entity");
	            	String chartEntityName = entityNode.getTextContent();
	            	LOG.warn(String.format("layout \"%s\": Removing chart for entity \"%s\". Please open and save the underlaying datasource to generate meta data. Then the chart could be add again. " + releaseNote("MIG-02"), layoutname, chartEntityName));
	            	nodesToRemove.add(chart);
	            }
	            
	            // matrix
	            NodeList matrixes = document.getElementsByTagName("matrix");
	            for (int i = 0; i < matrixes.getLength(); i++) {
//	            	<!ATTLIST matrix
//	            	name CDATA #IMPLIED
//	            	matrix_preferences_field CDATA #IMPLIED
//	            	entity_x CDATA #IMPLIED
//	            	entity_y CDATA #IMPLIED
//	            	entity_matrix CDATA #IMPLIED
//	            	entity_field_matrix_parent CDATA #IMPLIED
//	            	entity_field_matrix_x_ref_field CDATA #IMPLIED
//	            	entity_matrix_value_field CDATA #IMPLIED
//	            	entity_field_categorie CDATA #IMPLIED
//	            	entity_field_x CDATA #IMPLIED
//	            	entity_y_parent_field CDATA #IMPLIED
//	            	entity_field_y CDATA #IMPLIED
	            	Node matrix = matrixes.item(i);
	            	NamedNodeMap matrixAttributes = matrix.getAttributes();
	            	
	            	for (String[] attrEntityField : new String[][]{
	            			new String[]{"entity_matrix","matrix_preferences_field"},
	            			new String[]{"entity_matrix","entity_field_matrix_parent"},
	            			new String[]{"entity_matrix","entity_field_matrix_x_ref_field"},
	            			new String[]{"entity_matrix","entity_matrix_value_field"},
	            			new String[]{"entity_x","entity_field_categorie"},
	            			new String[]{"entity_x","entity_field_x"},
	            			new String[]{"entity_y","entity_y_parent_field"},
	            			new String[]{"entity_y","entity_field_y"}}) {
	            		Node entityNode = matrixAttributes.getNamedItem(attrEntityField[0]);
	            		Node fieldNode = matrixAttributes.getNamedItem(attrEntityField[1]);
	            		if (fieldNode == null) {
	            			continue;
	            		}
	            		String fieldName = fieldNode.getTextContent();
	            		if (entityNode != null) {
	            			String entityName = entityNode.getTextContent();
	            			UID fieldUID = getFieldUIDByNames(entityName, fieldName);
			            	if (fieldUID != null) {
			            		fieldNode.setTextContent(fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
			            	} 
	            		} else {
		            		LOG.warn(String.format("layout \"%s\": matrix %s \"%s\" does not exist", layoutname, attrEntityField[1], fieldName));
		            		matrixAttributes.removeNamedItem(attrEntityField[1]);
		            	}
	            	}
	            	for (String attrEntity : new String[]{"entity_x","entity_y","entity_matrix"}) {
	            		Node entityNode = matrixAttributes.getNamedItem(attrEntity);
	            		if (entityNode == null) {
	            			continue;
	            		}
	            		String entityName = entityNode.getTextContent();
	            		UID entityUID = getEntityUIDByName(entityName);
		            	if (entityUID != null) {
		            		entityNode.setTextContent(entityUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
		            	} else {
		            		LOG.warn(String.format("layout \"%s\": matrix %s \"%s\" does not exist", layoutname, attrEntity, entityName));
		            		matrixAttributes.removeNamedItem(attrEntity);
		            	}
	            	}
	            }
	            
	            // rule
	            NodeList rules = document.getElementsByTagName("rule");
	            for (int i = 0; i < rules.getLength(); i++) {
	            	Node rule = rules.item(i);
	            	NodeList ruleChildren = rule.getChildNodes();
	            	String ruleEntityName = null;
	            	MetaDbFieldWrapper sourcecomponentField = null;
            		for (int j = 0; j < ruleChildren.getLength(); j++) {
            			Node child = ruleChildren.item(j);
            			if ("event".equals(child.getNodeName())) {
//            				type (lookup|value-changed) #REQUIRED
//            				entity CDATA #IMPLIED
//            				sourcecomponent CDATA #REQUIRED
            				NamedNodeMap eventAttributes = child.getAttributes();
        	            	Node entityNode = eventAttributes.getNamedItem("entity");
        	            	Node sourcecomponentNode = eventAttributes.getNamedItem("sourcecomponent");
        	            	ruleEntityName = entity;
        	            	if (entityNode != null) {
        	            		ruleEntityName = entityNode.getTextContent();
        	            		UID ruleEntityUID = getEntityUIDByName(ruleEntityName);
        	            		if (ruleEntityUID != null) {
        	            			entityNode.setTextContent(ruleEntityUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
        	            		} else {
        	            			LOG.warn(String.format("layout \"%s\": Rule entity \"%s\" does not exist", layoutname, ruleEntityName));
        	            			entityNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
        	            		}
        	            	}
        	            	ruleEntityName = RigidUtils.defaultIfNull(ruleEntityName, entity);
        	            	String sourcecomponentName = sourcecomponentNode.getTextContent();
        	            	getFieldUIDByNames(ruleEntityName, sourcecomponentName);
        	            	sourcecomponentField = getFieldMetaByNames(ruleEntityName, sourcecomponentName);
    		            	if (sourcecomponentField != null) {
    		            		sourcecomponentNode.setTextContent(sourcecomponentField.getUID().getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
    		            	} else {
    		            		LOG.warn(String.format("layout \"%s\": Rule source component \"%s\" in entity \"%s\" does not exist", layoutname, sourcecomponentName, ruleEntityName));
    		            		sourcecomponentNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
    		            	}
            			} else if ("actions".equals(child.getNodeName())) {
            				if (ruleEntityName == null) {
            					LOG.warn(String.format("layout \"%s\": Rule entity name not found", layoutname));
    		            		continue;
            				}
            				NodeList actionsChildren = child.getChildNodes();
            				for (int k = 0; k < actionsChildren.getLength(); k++) {
            					Node action = actionsChildren.item(k);
            					if ("transfer-lookedup-value".equals(action.getNodeName())) {
//                					sourcefield CDATA #REQUIRED
//                					targetcomponent CDATA #REQUIRED<>
                					NamedNodeMap lookupAttributes = action.getAttributes();
                					Node sourcefieldNode = lookupAttributes.getNamedItem("sourcefield");
                	            	Node targetcomponentNode = lookupAttributes.getNamedItem("targetcomponent");
                	            	UID sourceentityUID = RigidUtils.defaultIfNull(sourcecomponentField.getForeignEntity(), 
                	            			sourcecomponentField.getLookupEntity());
                	            	String sourcefieldName = sourcefieldNode.getTextContent();
                	            	String targetcomponentName = targetcomponentNode.getTextContent();
                	            	UID sourcefieldUID = getFieldUIDByNames(getEntityNameByUID(sourceentityUID), sourcefieldName);
                	            	if (sourcefieldUID != null) {
                	            		sourcefieldNode.setTextContent(sourcefieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	} else {
            		            		LOG.warn(String.format("layout \"%s\": Rule action transfer lookup value source field \"%s\" in entity \"%s\" does not exist", layoutname, sourcefieldName, sourceentityUID.getString()));
            		            		sourcefieldNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	}
                	            	UID targetcomponentUID = getFieldUIDByNames(ruleEntityName, targetcomponentName);
                	            	if (targetcomponentUID != null) {
                	            		targetcomponentNode.setTextContent(targetcomponentUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	} else {
            		            		LOG.warn(String.format("layout \"%s\": Rule action transfer lookup value target component \"%s\" in entity \"%s\" does not exist", layoutname, targetcomponentName, ruleEntityName));
            		            		targetcomponentNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	}
                				} else if ("clear".equals(action.getNodeName())) {
//                					entity CDATA #IMPLIED
//                					targetcomponent CDATA #REQUIRED
                					NamedNodeMap clearAttributes = action.getAttributes();
                					Node entityNode = clearAttributes.getNamedItem("entity");
                	            	Node targetcomponentNode = clearAttributes.getNamedItem("targetcomponent");
                	            	String clearEntityName = ruleEntityName;
                	            	if (entityNode != null) {
                	            		clearEntityName = entityNode.getTextContent();
                	            		UID clearEntityUID = getEntityUIDByName(clearEntityName);
                	            		if (clearEntityUID != null) {
                	            			entityNode.setTextContent(clearEntityUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
                	            		} else {
                	            			LOG.warn(String.format("layout \"%s\": Rule action clear entity \"%s\" does not exist", layoutname, clearEntityName));
                	            			entityNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
                	            		}
                	            	}
                	            	clearEntityName = RigidUtils.defaultIfNull(clearEntityName, ruleEntityName);
                	            	String targetcomponentName = targetcomponentNode.getTextContent();
                	            	UID targetcomponentUID = getFieldUIDByNames(clearEntityName, targetcomponentName);
                	            	if (targetcomponentUID != null) {
                	            		targetcomponentNode.setTextContent(targetcomponentUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	} else {
            		            		LOG.warn(String.format("layout \"%s\": Rule action clear target component \"%s\" in entity \"%s\" does not exist", layoutname, targetcomponentName, clearEntityName));
            		            		targetcomponentNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	}
                				} else if ("enable".equals(action.getNodeName())) {
//                					targetcomponent CDATA #REQUIRED
//                					invertable (%boolean;) #IMPLIED
                					NamedNodeMap enableAttributes = action.getAttributes();
                					Node targetcomponentNode = enableAttributes.getNamedItem("targetcomponent");
                					String targetcomponentName = targetcomponentNode.getTextContent();
                					UID targetcomponentUID = getFieldUIDByNames(ruleEntityName, targetcomponentName);
                	            	if (targetcomponentUID != null) {
                	            		targetcomponentNode.setTextContent(targetcomponentUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	} else {
            		            		LOG.warn(String.format("layout \"%s\": Rule action enable target component \"%s\" in entity \"%s\" does not exist", layoutname, targetcomponentName, ruleEntityName));
            		            		targetcomponentNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	}
                				} else if ("refresh-valuelist".equals(action.getNodeName())) {
//                					entity CDATA #IMPLIED
//                					targetcomponent CDATA #REQUIRED
//                					parameter-for-sourcecomponent CDATA #IMPLIED // Parameter aus ValueListProvider, muss nicht migriert werden
                					NamedNodeMap refreshAttributes = action.getAttributes();
                					Node entityNode = refreshAttributes.getNamedItem("entity");
                	            	Node targetcomponentNode = refreshAttributes.getNamedItem("targetcomponent");
                	            	String refreshEntityName = ruleEntityName;
                	            	if (entityNode != null) {
                	            		refreshEntityName = entityNode.getTextContent();
                	            		UID clearEntityUID = getEntityUIDByName(refreshEntityName);
                	            		if (clearEntityUID != null) {
                	            			entityNode.setTextContent(clearEntityUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
                	            		} else {
                	            			LOG.warn(String.format("layout \"%s\": Rule action refresh valuelist entity \"%s\" does not exist", layoutname, refreshEntityName));
                	            			entityNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
                	            		}
                	            	}
                	            	refreshEntityName = RigidUtils.defaultIfNull(refreshEntityName, ruleEntityName);
                	            	String targetcomponentName = targetcomponentNode.getTextContent();
                	            	UID targetcomponentUID = getFieldUIDByNames(refreshEntityName, targetcomponentName);
                	            	if (targetcomponentUID != null) {
                	            		targetcomponentNode.setTextContent(targetcomponentUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	} else {
            		            		LOG.warn(String.format("layout \"%s\": Rule action refresh valuelist target component \"%s\" in entity \"%s\" does not exist", layoutname, targetcomponentName, ruleEntityName));
            		            		targetcomponentNode.setTextContent(UID.UID_NULL.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            		            	}
                				} 
            				}
            			}
            		}
	            }
	            
	            // valuelist-provider
	            NodeList vlps = document.getElementsByTagName("valuelist-provider");
	            for (int i = 0; i < vlps.getLength(); i++) {
	            	Node vlp = vlps.item(i);
	            	NamedNodeMap vlpAttributes = vlp.getAttributes();
	            	Node typeNode = vlpAttributes.getNamedItem("type");
	            	String type = typeNode.getTextContent();
	            	final String name = type;
	            	final String value;
	            	if (RigidUtils.looksEmpty(type)) {
	            		value = null;
	            	} else if ("default".equals(type)) {
	            		value = type;
	            	} else if ("dependant".equals(type)) {
	            		value = type;
	            	} else if (type.endsWith(".ds")) {
	            		final String dsName = type.substring(0, type.length()-3);
	            		type = "ds";
	            		UID dsUID = null;
	            		// search for datasource with name
	            		for (RigidEO ds : helper.getAll(E_03_15_0004.VALUELISTPROVIDER)) {
	            			if (dsName.equalsIgnoreCase(ds.getValue(E_03_15_0004.VALUELISTPROVIDER.name))) {
	            				dsUID = getUID(E_03_15_0004.VALUELISTPROVIDER.getEntityName(), (Long) ds.getPrimaryKey());
	            				break;
	            			}
	            		}
	            		if (dsUID != null) {
	            			value = dsUID.getStringifiedDefinitionWithEntity(E_03_15_0004.VALUELISTPROVIDER);
	            		} else {
	            			value = null;
	            		}
	            	} else {
	            		value = type;
	            		type = "named";
	            	}
	            	if (value != null) {
	            		typeNode.setTextContent(type);
	            		Attr nameNode = document.createAttribute("name");
	            		Attr valueNode = document.createAttribute("value");
	            		nameNode.setTextContent(name);
	            		valueNode.setTextContent(value);
	            		vlpAttributes.setNamedItem(nameNode);
	            		vlpAttributes.setNamedItem(valueNode);
	            		NodeList vlpChildren = vlp.getChildNodes();
	            		for (int j = 0; j < vlpChildren.getLength(); j++) {
	            			Node child = vlpChildren.item(j);
	            			if ("parameter".equals(child.getNodeName())) {
	            				if ("named".equals(type) && "process".equals(value)) {
	            					nodesToRemove.add(child);
	            				}
	            			}
	            		}
	            	} else {
	            		// remove
	            		LOG.warn(String.format("layout \"%s\": vlp \"%s\" does not exist", layoutname, type));
	            		nodesToRemove.add(vlp);
	            	}
	            }
	            
	            // textfield
	            NodeList textfields = document.getElementsByTagName("textfield");
	            for (int i = 0; i < textfields.getLength(); i++) {
	            	Node textfield = textfields.item(i);
	            	migrateLayoutNextFocus(textfield.getAttributes(), document, layoutname, entity);
	            }
	            
	            // textarea
	            NodeList textareas = document.getElementsByTagName("textarea");
	            for (int i = 0; i < textareas.getLength(); i++) {
	            	Node textarea = textareas.item(i);
	            	migrateLayoutNextFocus(textarea.getAttributes(), document, layoutname, entity);
	            }
	            
	            // combobox
	            NodeList comboboxes = document.getElementsByTagName("combobox");
	            for (int i = 0; i < comboboxes.getLength(); i++) {
	            	Node combobox = comboboxes.item(i);
	            	migrateLayoutNextFocus(combobox.getAttributes(), document, layoutname, entity);
	            }
	            
	            // button
	            NodeList buttons = document.getElementsByTagName("button");
	            for (int i = 0; i < buttons.getLength(); i++) {
	            	Node button = buttons.item(i);
	            	NamedNodeMap buttonAttributes = button.getAttributes();
	            	migrateLayoutNextFocus(buttonAttributes, document, layoutname, entity);
	            	Node iconNode = buttonAttributes.getNamedItem("icon");
	            	if (iconNode != null) {
	            		String sResourceName = iconNode.getTextContent();
	            		UID resourceUID = getResourceUID(sResourceName);
	            		if (resourceUID != null) {
	            			iconNode.setTextContent(resourceUID.getStringifiedDefinitionWithEntity(E_03_15_0004.RESOURCE));
	            		} else {
	            			LOG.warn(String.format("layout \"%s\": button icon %s does not exist", layoutname, sResourceName));
	            		}
	            	}
	            	NodeList buttonChildren = button.getChildNodes();
            		for (int j = 0; j < buttonChildren.getLength(); j++) {
            			Node child = buttonChildren.item(j);
            			if ("property".equals(child.getNodeName())) {
//            				name CDATA #REQUIRED
//            				value CDATA #REQUIRED
            				NamedNodeMap propertyAttributes = child.getAttributes();
        	            	Node nameNode = propertyAttributes.getNamedItem("name");
        	            	Node valueNode = propertyAttributes.getNamedItem("value");
        	            	if ("targetState".equals(nameNode.getTextContent())) {
        	            		String sTargetState = valueNode.getTextContent();
        	            		UID targetStateUID = null;
        	    				try {
        	    					Integer targetState = Integer.parseInt(sTargetState);
        	    					// search for state with given state number
        	    					for (RigidEO statemodelusage : helper.getByField(E_03_15_0004.STATEMODELUSAGE, E_03_15_0004.STATEMODELUSAGE.nuclos_module, entityId)) {
        	    						Long statemodelId = statemodelusage.getForeignID(E_03_15_0004.STATEMODELUSAGE.statemodel);
        	    						for (RigidEO state : helper.getByField(E_03_15_0004.STATE, E_03_15_0004.STATE.model, statemodelId)) {
        	    							Integer iStatenumeral = state.getValue(E_03_15_0004.STATE.numeral);
        	    							if (RigidUtils.equal(iStatenumeral, targetState)) {
        	    								if (targetStateUID != null) {
        	    									LOG.warn(String.format("layout \"%s\": button target state, numeral %s is not a unique state for entity \"%s\", selecting first \"%s\"", layoutname, sTargetState, entity, targetStateUID));
        	    								} else {
        	    									targetStateUID = state.getForeignUID(E_03_15_0004.STATE.pkuid);
        	    								}
        	    							}
        	    						}
        	    					}
        	    					valueNode.setTextContent(targetStateUID.getStringifiedDefinitionWithEntity(E_03_15_0004.STATE));
        	    				} catch (NumberFormatException e) {
        	    					LOG.warn(String.format("layout \"%s\": button target state is not an NUMERAL \"%s\"", layoutname, sTargetState));
        	    					break;
        	    				}
        	            	} else if ("generatortoexecute".equals(nameNode.getTextContent())) {
        	            		String sGeneratorName = valueNode.getTextContent();
        	            		Collection<RigidEO> generators = helper.getByField(E_03_15_0004.GENERATION, E_03_15_0004.GENERATION.name, sGeneratorName);
        	            		switch (generators.size()) {
        	            		case 0:
        	            			LOG.warn(String.format("layout \"%s\": button generator \"%s\" does not exist", layoutname, sGeneratorName));
        	            			break;
        	            		case 1:
        	            			valueNode.setTextContent(getUID(E_03_15_0004.GENERATION, (Long) generators.iterator().next().getPrimaryKey())
        	            					.getStringifiedDefinitionWithEntity(E_03_15_0004.GENERATION));
        	            			break;
        	            		default:
        	            			LOG.warn(String.format("layout \"%s\": button generator \"%s\" is not unique", layoutname, sGeneratorName));
        	            		}
        	            	} else if ("hyperlinkField".equals(nameNode.getTextContent())) {
        	            		String sFieldName = valueNode.getTextContent();
        	            		UID fieldUID = getFieldUIDByNames(entity, sFieldName);
        	            		if (fieldUID != null) {
        	            			valueNode.setTextContent(fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD));
        	            		} else {
        	            			LOG.warn(String.format("layout \"%s\": button hyperklink field \"%s\" does not exist", layoutname, sFieldName));
        	    					break;
        	            		}
        	            	} else if ("ruletoexecute".equals(nameNode.getTextContent())) {
        	            		String sRuleNameInLayout = valueNode.getTextContent();
        	            		if (sRuleNameInLayout.endsWith(".class")) {
        	            			sRuleNameInLayout = sRuleNameInLayout.substring(0, sRuleNameInLayout.length()-6);
        	            		}
        	            		String sNewRuleNameInLayout = null;
        	            		// search for rule with name
        	            		for (RigidEO rule : helper.getAll(E_03_15_0004.SERVERCODE)) {
        	            			// with package=
        	            			String sRuleFullQualifiedName = rule.getValue(E_03_15_0004.SERVERCODE.name);
        	            			// without package=
        	            			String sRuleName = sRuleFullQualifiedName;
        	            			if (sRuleName.indexOf(".") > -1) {
        	            				sRuleName = sRuleName.substring(sRuleName.lastIndexOf(".")+1);
        	            			}
        	            			if (RigidUtils.equal(sRuleNameInLayout, sRuleName)) {
        	            				sNewRuleNameInLayout = sRuleFullQualifiedName;
        	            				break;
        	            			}
        	            		}
        	            		if (sNewRuleNameInLayout != null) {
        	            			valueNode.setTextContent(sNewRuleNameInLayout);
        	            		} else {
        	            			LOG.warn(String.format("layout \"%s\": button rule \"%s\" does not exist", layoutname, sRuleNameInLayout));
        	    					break;
        	            		}
        	            	}
            			}
            		}
	            }
	            
	            // scripts
	            migrateLayoutScript(document, "enabled");
	            migrateLayoutScript(document, "new-enabled");
	            migrateLayoutScript(document, "edit-enabled");
	            migrateLayoutScript(document, "delete-enabled");
	            migrateLayoutScript(document, "clone-enabled");
	            
	            // remove nodes
	            for (Node toRemove : nodesToRemove) {
	            	try {
	            		toRemove.getParentNode().removeChild(toRemove);
	            	} catch (Exception ex) {
	            		LOG.error(String.format("layout \"%s\": node \"%s\" could not be removed", layoutname, toRemove), ex);
	            	}
	            }
	            
	            StringWriter layoutWriter = new StringWriter();
	            javax.xml.transform.Transformer transformer = TransformerFactory.newInstance().newTransformer();
	            transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, LAYOUTML_DTD_SYSTEMIDENTIFIER);
	            transformer.transform(new DOMSource(document), new StreamResult(layoutWriter));
	            
	            layoutML = layoutWriter.toString();
	            eo.setValue(E_03_15_0004.LAYOUT.layoutML, layoutML);
	            helper.update(E_03_15_0004.LAYOUT, eo);
				
			} catch (Exception ex) {
				LOG.warn("unable to migrate layout with pk " + eo.getPrimaryKey(), ex);
			}
		}
	}
	
	private void migrateLayoutScript(Document document, String elementName) {
		NodeList scripts = document.getElementsByTagName(elementName);
        for (int i = 0; i < scripts.getLength(); i++) {
        	Node script = scripts.item(i);
        	NamedNodeMap scriptsAttributes = script.getAttributes();
        	Node language = scriptsAttributes.getNamedItem("language");
        	String groovycode = script.getTextContent();
        	groovycode = migrateGroovycode(groovycode);
        	
        	NodeList children = script.getChildNodes();
        	for (int j = children.getLength()-1; j >= 0; j--) {
        		script.removeChild(children.item(j));
        	}
        	script.appendChild(document.createCDATASection(groovycode));
        	//script.setTextContent(groovycode);
        }
	}
	
	private void migrateLayoutNextFocus(NamedNodeMap attributes, Document document, String layoutname, String entity) {
		Node nextfocuscomponent = attributes.getNamedItem("nextfocuscomponent");
    	if (nextfocuscomponent != null) {
        	String nfc = nextfocuscomponent.getTextContent();
        	if (nfc != null) {
            	if (nfc.contains(".")) {
            		// must be a subform field:
            		int index = nfc.indexOf(".");
            		String subformEntityName = nfc.substring(0, index);
            		String subformEntityFieldName = nfc.substring(index+1, nfc.length());
            		UID subformEntityFieldUID = getFieldUIDByNames(subformEntityName, subformEntityFieldName);
            		if (subformEntityFieldUID != null) {
            			Attr nextfocusfield = document.createAttribute("nextfocusfield");
            			nextfocusfield.setTextContent(subformEntityFieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            			attributes.setNamedItem(nextfocusfield);
	            	} else {
	            		LOG.warn(String.format("layout \"%s\": Next focus field \"%s\" in entity \"%s\" does not exist", layoutname, subformEntityFieldName, subformEntityName));	
	            	}
            		attributes.removeNamedItem("nextfocuscomponent");
            	} else {
            		// normal component or static
            		UID nfcUID = getFieldUIDByNames(entity, nfc);
            		if (nfcUID != null) {
	            		Attr nextfocusfield = document.createAttribute("nextfocusfield");
            			nextfocusfield.setTextContent(nfcUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
            			attributes.setNamedItem(nextfocusfield);
            			attributes.removeNamedItem("nextfocuscomponent");
            		}
            		// else... 
            		// otherwise nothing to do. should be a static component
            	}
        	}
    	}
	}
	
	private static class LayoutDTDResolver implements EntityResolver {

		@Override
		public InputSource resolveEntity(String publicId, String sSystemId) throws SAXException, IOException {
			try {
				InputSource result = null;
				if (sSystemId != null && sSystemId.equals(LAYOUTML_DTD_SYSTEMIDENTIFIER)) {
					final ClassLoader cl = Migration_04_00_0022_main.class.getClassLoader();
					final URL urlDtd = cl.getResource(LAYOUTML_DTD_RESSOURCEPATH);
					if (urlDtd == null) {
						throw new SAXException("LayoutML.dtd not found");
					}
					result = new InputSource(new BufferedInputStream(urlDtd.openStream()));
				}
				return result;
			}
			catch (Exception ex) {
				throw new SAXException("LayoutML.dtd not found", ex);
			}
		}
		
	}
	
	private void migrateSearchfilters() {
		logTitle(String.format("migrate search filters"));
		
		ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		final XStream xstreamOld = new XStream(new StaxDriver());
		final XStream xstreamNew = new XStream(new StaxDriver());
		
		xstreamOld.alias("org.nuclos.common.collect.collectable.CollectableSorting", Prefs_CollectableSortingOld.class);
		xstreamNew.alias("org.nuclos.common.collect.collectable.CollectableSorting", Prefs_CollectableSortingNew.class);
		
		final String PREFS_NODE_SEARCHFILTERS = "searchFilters";
		final String PREFS_NODE_SEARCHCONDITION = "searchCondition";
		final String PREFS_NODE_VISIBLECOLUMNS = "visibleColumns";
		final String PREFS_NODE_VISIBLECOLUMNENTITIES = "visibleColumnEntities";
		final String PREFS_NODE_COLLECTABLESORTING = "collectableSorting";
		
		for (final RigidEO eo : helper.getAll(E_03_15_0004.SEARCHFILTER)) {
		Runnable worker = new Runnable() {@Override	public void run() {
			
			Prefs_DOMPreferencesFactory prefsFactory = new Prefs_DOMPreferencesFactory();
			String searchfiltername = eo.getValue(E_03_15_0004.SEARCHFILTER.name);
			LOG.debug(String.format("migrate searchfilter \"%s\"", searchfiltername));
			try {				
				String entity = eo.getValue(E_03_15_0004.SEARCHFILTER.entity);
				String sfPrefs = eo.getValue(E_03_15_0004.SEARCHFILTER.clbsearchfilter);				
				
				final ByteArrayInputStream is = new ByteArrayInputStream(sfPrefs.getBytes("UTF-8"));
				final Prefs_DOMPreferences root = prefsFactory.read(is);
				Preferences prefs = root.node("org/nuclos/client");
				final Preferences prefsSearchfilter;
				if (prefs.nodeExists(PREFS_NODE_SEARCHFILTERS)) {
					prefsSearchfilter = prefs.node(PREFS_NODE_SEARCHFILTERS);
				}
				else {
					LOG.warn(String.format(
							"searchfilter \"%s\" not migrateable: unknown format (no PREFS_NODE_SEARCHFILTERS)", 
							searchfiltername));
					helper.delete(E_03_15_0004.SEARCHFILTER, eo);
					return;
				}
		
				if (prefsSearchfilter.nodeExists(searchfiltername)) {
					prefs = prefsSearchfilter.node(searchfiltername);
				}
				else {
					LOG.warn(String.format(
							"searchfilter \"%s\" not migrateable: unknown format (no PREFS_NODE_SEARCHFILTERS.<searchfiltername>)", 
							searchfiltername));
					helper.delete(E_03_15_0004.SEARCHFILTER, eo);
					return;
				}
				
				// search condition
				(new MigrateSearchConditionFromPrefs()).migrateSearchCondition(prefs.node(PREFS_NODE_SEARCHCONDITION), entity);
				
				
				// visible fields
				final List<String> lstSelectedFieldUIDs = new ArrayList<String>();
				final List<String> lstSelectedEntityUIDs = new ArrayList<String>();
				List<String> lstSelectedFieldNames;
				List<String> lstSelectedEntityNames;
				try {
					lstSelectedFieldNames = getStringList(prefs, PREFS_NODE_VISIBLECOLUMNS);
					lstSelectedEntityNames = getStringList(prefs, PREFS_NODE_VISIBLECOLUMNENTITIES);
				}
				catch (PreferencesException ex) {
					lstSelectedFieldNames = new ArrayList<String>();
					lstSelectedEntityNames = new ArrayList<String>();
					// no exception is thrown here.
				}
				// ensure backwards compatibility:
				if (lstSelectedEntityNames.isEmpty() && !lstSelectedFieldNames.isEmpty()) {
					lstSelectedEntityNames = Arrays.asList(new String[lstSelectedFieldNames.size()]);
					assert lstSelectedEntityNames.size() == lstSelectedFieldNames.size();
				}
				if (lstSelectedFieldNames.size() != lstSelectedEntityNames.size()) {
					lstSelectedFieldNames = new ArrayList<String>();
					lstSelectedEntityNames = new ArrayList<String>();
				}
				for (int i = 0; i < lstSelectedFieldNames.size(); i++) {
					final String sFieldName = lstSelectedFieldNames.get(i);
					final String sEntityName = lstSelectedEntityNames.get(i);
					UID entityUID = getEntityUIDByName(sEntityName);
					UID fieldUID = getFieldUIDByNames(sEntityName, sFieldName);
					if (entityUID != null && fieldUID != null) {
						lstSelectedFieldUIDs.add(fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
						lstSelectedEntityUIDs.add(entityUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
					}
				}
				putStringList(prefs, PREFS_NODE_VISIBLECOLUMNS, lstSelectedFieldUIDs);
				putStringList(prefs, PREFS_NODE_VISIBLECOLUMNENTITIES, lstSelectedEntityUIDs);
				
				
				// sorting
				if (nodeExists(prefs, PREFS_NODE_COLLECTABLESORTING)) {
					try {							
						final ArrayList<Prefs_CollectableSortingNew> newSortings = new ArrayList<Prefs_CollectableSortingNew>();
						Preferences prefSorting = prefs.node(PREFS_NODE_COLLECTABLESORTING);
						final int iSize = prefSorting.getInt(PREFS_KEY_LIST_SIZE, -1);
						if (iSize >= 0) {
							newSortings.ensureCapacity(iSize);
							for (int i = 0; i < iSize; ++i) {
								// use a separate node for each element:
								try {
									String s = prefSorting.get(String.valueOf(i), null);
									if(s != null) {
										
										Prefs_CollectableSortingOld oldSorting = (Prefs_CollectableSortingOld) xstreamOld.fromXML(s);
										UID entityUID = getEntityUIDByName(oldSorting.getEntity());
										UID fieldUID = getFieldUIDByNames(oldSorting.getEntity(), oldSorting.getFieldName());
										if (entityUID != null || fieldUID != null) {
											Prefs_CollectableSortingNew newSorting = new Prefs_CollectableSortingNew(
													entityUID, 
													oldSorting.isBaseEntity(), 
													fieldUID, 
													oldSorting.isAscending());
											newSortings.add(newSorting);
										} else {
											LOG.warn(String.format("searchfilter \"%s\": sorting entry ignored. field \"%s\" in entity \"%s\" does not exist", 
													searchfiltername, oldSorting.getFieldName(), oldSorting.getEntity()));
										}
										
									}
								}
								catch(XStreamException e) {
									LOG.warn(String.format("searchfilter \"%s\": sorting entry ignored", searchfiltername), e);
								}
							}
						}
						
						final int length = newSortings.size();
						prefSorting = getEmptyNode(prefs, PREFS_NODE_COLLECTABLESORTING);
						prefSorting.putInt(PREFS_KEY_LIST_SIZE, length);
						final Iterator<Prefs_CollectableSortingNew> it = newSortings.iterator();
						for (int i = 0; i < length; ++i) {
							// use a separate node for each element:
							prefSorting.put(String.valueOf(i), xstreamNew.toXML(it.next()));
						}
						
					} catch (Exception ex) {
						LOG.warn(String.format(
								"searchfilter \"%s\": sorting not migrateable: %s", 
								searchfiltername, ex.toString()), ex);
						prefs.remove(PREFS_NODE_COLLECTABLESORTING);
					}
				}
				
				StringWriter outWriter = new StringWriter();
				prefsFactory.write(root, new StreamResult(outWriter), true);
				sfPrefs = outWriter.toString();
				
	            eo.setValue(E_03_15_0004.SEARCHFILTER.clbsearchfilter, sfPrefs);
	            helper.update(E_03_15_0004.SEARCHFILTER, eo);
				
			} catch (Exception ex) {
				LOG.warn("unable to migrate searchfilter with pk " + eo.getPrimaryKey() + ": " + ex, ex);
			} catch (java.lang.LinkageError ex) {
				LOG.warn("unable to migrate searchfilter with pk " + eo.getPrimaryKey() + ": " + ex, ex);
			}

		}}; executor.execute(worker);}

	    executor.shutdown();
	    try {
			if (!executor.awaitTermination(1, TimeUnit.DAYS)) {
				throw new CommonFatalException("Timeout!");
			}
		} catch (InterruptedException e) {
			throw new CommonFatalException(e);
		}
	}
	
	private class MigrateSearchConditionFromPrefs {

		private static final String PREFS_KEY_PLAINSUBCONDITION_NAME = "plainSearchConditionName";
		private static final String PREFS_KEY_TYPE = "type";
		private static final String PREFS_KEY_FIELDNAME = "fieldName";
		private static final String PREFS_KEY_PARAMETER_COMPARAND = "parameterComparand";
		private static final String PREFS_KEY_DATEVALUES_COMPARAND = "dateValuesComparand";
		private static final String PREFS_KEY_FIELDNAME_COMPARAND = "fieldNameComparand";
		private static final String PREFS_KEY_COMPARISON_OPERATOR = "comparisonOperator";
		private static final String PREFS_KEY_LOGICAL_OPERATOR = "logicalOperator";
		private static final String PREFS_KEY_LIKE_COMPARAND = "likeComparand";
		private static final String PREFS_NODE_COMPARAND = "comparand";
		private static final String PREFS_NODE_COMPOSITESEARCHCONDITION = "compositeSearchCondition";
		private static final String PREFS_KEY_ENTITYNAME = "entity";
		private static final String PREFS_KEY_FOREIGNKEYFIELDNAME = "foreignKeyField";
		private static final String PREFS_NODE_SUBCONDITION = "subCondition";
		private static final String PREFS_NODE_JOINCONDITION = "joinCondition";
		private static final String PREFS_KEY_ID = "id";
		
		private static final String PREFS_KEY_FIELDUID = "fieldUid";
		private static final String PREFS_KEY_FIELDUID_COMPARAND = "fieldUidComparand";

		public MigrateSearchConditionFromPrefs() {
		}

		public void migrateSearchCondition(Preferences prefs, String sEntityName) throws PreferencesException {

			final int iType = prefs.getInt(PREFS_KEY_TYPE, -1);
			switch (iType) {
			case 0: //CollectableSearchCondition.TYPE_ATOMIC:
				migrateAtomicSearchCondition(prefs, sEntityName);
				break;
			case 1: //CollectableSearchCondition.TYPE_COMPOSITE:
				migrateCompositeSearchCondition(prefs, sEntityName);
				break;
			case 2: //CollectableSearchCondition.TYPE_SUB:
				if (isPlainSubCondition(prefs)) {
					migratePlainSubCondition(prefs);
				} else {
					migrateSubCondition(prefs);
				}
				break;
			case 4: //CollectableSearchCondition.TYPE_REFERENCING:
				migrateReferencingCondition(prefs, sEntityName);
				break;
			case 3: //CollectableSearchCondition.TYPE_ID:
				migrateIdCondition(prefs);
				break;
			default:
				// no searchcondition at all
			}
		}

		private boolean isPlainSubCondition(Preferences prefs) throws PreferencesException {
			return getSerializable(prefs, PREFS_KEY_PLAINSUBCONDITION_NAME) != null;
		}

		/**
		 * @param prefs
		 * @param clcteprovider
		 * @param sEntityName
		 * @return
		 * @throws PreferencesException
		 * @todo This method is duplicated in CollectableTextComponentHelper and AtomicNodeController - try to merge
		 */
		private void migrateAtomicSearchCondition(Preferences prefs, String sEntityName) throws PreferencesException {

			final String sFieldName = prefs.get(PREFS_KEY_FIELDNAME, null);
			UID fieldUID = getFieldUIDByNames(sEntityName, sFieldName);
			if (fieldUID != null) {
				prefs.remove(PREFS_KEY_FIELDNAME);
				prefs.put(PREFS_KEY_FIELDUID, fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
			} else {
				LOG.warn(String.format("searchcondition atomic: field \"%s\" in entity \"%s\" does not exist", sFieldName, sEntityName));
				try {
					prefs.removeNode();
				} catch (BackingStoreException e) {
					throw new PreferencesException(e);
				}
			}
			final int iComparisonOperator = prefs.getInt(PREFS_KEY_COMPARISON_OPERATOR, -1);

			switch (iComparisonOperator) {
			case 8: //IS_NULL:
			case 9: //IS_NOT_NULL:
				break;
			case 7: //LIKE:
				break;
			case 10: //NOT_LIKE:
				break;
			default:
				String sOtherFieldName = prefs.get(PREFS_KEY_FIELDNAME_COMPARAND, null);
				if (!RigidUtils.looksEmpty(sOtherFieldName)) {
					fieldUID = getFieldUIDByNames(sEntityName, sOtherFieldName);
					if (fieldUID != null) {
						prefs.remove(PREFS_KEY_FIELDNAME_COMPARAND);
						prefs.put(PREFS_KEY_FIELDUID_COMPARAND, fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
					} else {
						LOG.warn(String.format("field \"%s\" in entity \"%s\" does not exist", sOtherFieldName, sEntityName));
						try {
							prefs.removeNode();
						} catch (BackingStoreException e) {
							throw new PreferencesException(e);
						}
					}
				}
			}
		}

		private void migrateCompositeSearchCondition(Preferences prefs, String sEntityName) throws PreferencesException {
			migratePrefsGenericList(prefs, PREFS_NODE_COMPOSITESEARCHCONDITION, new PreferencesIO(sEntityName));
		}

		private void migrateSubCondition(Preferences prefs) throws PreferencesException {
			final String sSubEntityName = prefs.get(PREFS_KEY_ENTITYNAME, null);
			final String sForeignKeyFieldName = prefs.get(PREFS_KEY_FOREIGNKEYFIELDNAME, null);
			UID entityUID = getEntityUIDByName(sSubEntityName);
			UID fieldUID = getFieldUIDByNames(sSubEntityName, sForeignKeyFieldName);
			if (entityUID != null && fieldUID != null) {
				prefs.put(PREFS_KEY_ENTITYNAME, entityUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITY.getUID()));
				prefs.put(PREFS_KEY_FOREIGNKEYFIELDNAME, fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
				migrateSearchCondition(prefs.node(PREFS_NODE_SUBCONDITION), sSubEntityName);
			} else {
				LOG.warn(String.format("searchcondition foreign key field: entity, or field \"%s\" in entity \"%s\", does not exist", sForeignKeyFieldName, sSubEntityName));
				try {
					prefs.removeNode();
				} catch (BackingStoreException e) {
					throw new PreferencesException(e);
				}
			}
		}

		private void migratePlainSubCondition(Preferences prefs) throws PreferencesException {
			final String sPlainName = (String) getSerializable(prefs, PREFS_KEY_PLAINSUBCONDITION_NAME);
		}

		private void migrateReferencingCondition(Preferences prefs, String sEntityName) throws PreferencesException {
			final String sFieldName = prefs.get(PREFS_KEY_FIELDNAME, null);
			UID fieldUID = getFieldUIDByNames(sEntityName, sFieldName);
			if (fieldUID != null) {
				prefs.remove(PREFS_KEY_FIELDNAME);
				prefs.put(PREFS_KEY_FIELDUID, fieldUID.getStringifiedDefinitionWithEntity(E_03_15_0004.ENTITYFIELD.getUID()));
			} else {
				LOG.warn(String.format("searchcondition referencing: field \"%s\" in entity \"%s\" does not exist", sFieldName, sEntityName));
				try {
					prefs.removeNode();
				} catch (BackingStoreException e) {
					throw new PreferencesException(e);
				}
			}
		}

		private void migrateIdCondition(Preferences prefs) throws PreferencesException {
		}
		
		private Object getSerializable(Preferences prefs, String sKey) throws PreferencesException {
			Object result = null;
			final byte[] ab = prefs.getByteArray(sKey, null);
			if (ab != null) {
				try {
					final ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(ab));
					result = ois.readObject();
					ois.close();
				}
				catch (IOException ex) {
					throw new PreferencesException(ex.toString());
				}
				catch (ClassNotFoundException ex) {
					throw new PreferencesException(ex.toString());
				}
			}
			return result;
		}
	}
	
	private class PreferencesIO {
		private final String sEntityName;

		public PreferencesIO(String sEntityName) {
			this.sEntityName = sEntityName;
		}

		public void handle(Preferences prefs) throws PreferencesException {
			(new MigrateSearchConditionFromPrefs()).migrateSearchCondition(prefs, this.sEntityName);
		}

	}
	
	/**
	 * the preferences key to store the size of the array (number of elements)
	 */
	private static final String PREFS_KEY_LIST_SIZE = "size";
	
	private void migratePrefsGenericList(Preferences prefs, String sNode, PreferencesIO pio) throws PreferencesException {
		if (nodeExists(prefs, sNode)) {
			prefs = prefs.node(sNode);
			final int iSize = prefs.getInt(PREFS_KEY_LIST_SIZE, -1);
			if (iSize >= 0) {
				for (int i = 0; i < iSize; ++i) {
					// use a separate node for each element:
					final Preferences prefsElement = prefs.node(String.valueOf(i));
					pio.handle(prefsElement);
				}
			}
		}
	}
	
	public Preferences getEmptyNode(Preferences prefs, String sNode) throws PreferencesException {
		try {
			if (prefs.nodeExists(sNode)) {
				// remove the node completely (including all of its subnodes)
				prefs.node(sNode).clear();
				for (String sChild : prefs.node(sNode).childrenNames()) {
					prefs.node(sNode).node(sChild).remove(sChild);
				}
			}
			final Preferences result = prefs.node(sNode);

			assert prefs.nodeExists(sNode);
			assert result.childrenNames().length == 0;
			return result;
		}
		catch (BackingStoreException ex) {
			throw new PreferencesException(ex);
		}
	}
	
	public void putStringList(Preferences prefs, String sNode, List<String> list) throws PreferencesException {
		prefs = getEmptyNode(prefs, sNode);
		prefs.putInt(PREFS_KEY_LIST_SIZE, list.size());
		final Iterator<String> iter = list.listIterator();
		int i = 0;
		while (iter.hasNext()) {
			prefs.put(String.valueOf(i++), iter.next());
		}
	}
	
	private ArrayList<String> getStringList(Preferences prefs, String sNode) throws PreferencesException {
		final String[] as = getStringArray(prefs, sNode);
		// Note that we create an extra ArrayList around Arrays.asList, so the result allows for
		// removal of single elements.
		final ArrayList<String> result = (as == null) ? new ArrayList<String>() : new ArrayList<String>(Arrays.asList(as));
		assert result != null;
		return result;
	}
	
	private String[] getStringArray(Preferences prefs, String sNode) throws PreferencesException {
		final String[] as = getStringArrayOrNull(prefs, sNode);
		final String[] result = (as == null) ? new String[0] : as;

		assert result != null;
		return result;
	}
	
	public String[] getStringArrayOrNull(Preferences prefs, String sNode) throws PreferencesException {
		String[] result = null;

		if (nodeExists(prefs, sNode)) {
			prefs = prefs.node(sNode);
			final int iSize = prefs.getInt(PREFS_KEY_LIST_SIZE, -1);
			if (iSize >= 0) {
				result = new String[iSize];
				for (int i = 0; i < iSize; ++i) {
					result[i] = prefs.get(String.valueOf(i), null);
				}
			}
		}
		return result;
	}
	
	private boolean nodeExists(Preferences prefs, String sNode) throws PreferencesException {
		try {
			return prefs.nodeExists(sNode);
		}
		catch (BackingStoreException ex) {
			throw new PreferencesException(ex);
		}
	}
	
	private void migrateResPlans() {
		logTitle(String.format("migrate resource plans"));
		for (RigidEO eo : helper.getAll(E_03_15_0004.CUSTOMCOMPONENT)) {
			try {
				String resplan = eo.getValue(E_03_15_0004.CUSTOMCOMPONENT.name);
				LOG.debug(String.format("migrate resplan \"%s\"", resplan));
				byte[] bytes = eo.getValue(E_03_15_0004.CUSTOMCOMPONENT.data);
				RP_ResPlanOld rp3 = RP_ResPlanOld.fromBytes(bytes);
				RP_ResPlanNew rp4 = new RP_ResPlanNew();
				
				rp4.setResourceEntity(getEntityUIDByName(rp3.getResourceEntity()));
				rp4.setResourceSortField(getFieldUIDByNames(rp3.getResourceEntity(), rp3.getResourceSortField()));
				
				rp4.setResourceLabelText(migrateRefString(rp3.getResourceEntity(), rp3.getResourceLabelText(), 
						String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3.getResourceLabelText(), resplan, "ResourceLabelText")));
				rp4.setResourceToolTipText(migrateRefString(rp3.getResourceEntity(), rp3.getResourceToolTipText(), 
						String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3.getResourceToolTipText(), resplan, "ResourceToolTipText")));
				rp4.setCornerLabelText(migrateRefString(rp3.getResourceEntity(), rp3.getCornerLabelText(), 
						String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3.getCornerLabelText(), resplan, "CornerLabelText")));
				rp4.setCornerToolTipText(migrateRefString(rp3.getResourceEntity(), rp3.getCornerToolTipText(), 
						String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3.getCornerToolTipText(), resplan, "CornerToolTipText")));

				rp4.setDefaultViewFrom(rp3.getDefaultViewFrom());
				rp4.setDefaultViewUntil(rp3.getDefaultViewUntil());

				rp4.setEntryEntity(getEntityUIDByName(rp3.getEntryEntity()));
				rp4.setReferenceField(getFieldUIDByNames(rp3.getEntryEntity(), rp3.getReferenceField()));
				rp4.setMilestoneField(getFieldUIDByNames(rp3.getEntryEntity(), rp3.getMilestoneField()));
				
				rp4.setDateFromField(getFieldUIDByNames(rp3.getEntryEntity(), rp3.getDateFromField()));
				rp4.setDateUntilField(getFieldUIDByNames(rp3.getEntryEntity(), rp3.getDateUntilField()));
				rp4.setTimePeriodsString(rp3.getTimePeriodsString());
				rp4.setTimeFromField(getFieldUIDByNames(rp3.getEntryEntity(), rp3.getTimeFromField()));
				rp4.setTimeUntilField(getFieldUIDByNames(rp3.getEntryEntity(), rp3.getTimeUntilField()));
				
				rp4.setEntryLabelText(migrateRefString(rp3.getEntryEntity(), rp3.getEntryLabelText(), 
						String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3.getEntryLabelText(), resplan, "EntryLabelText")));
				rp4.setEntryToolTipText(migrateRefString(rp3.getEntryEntity(), rp3.getEntryToolTipText(), 
						String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3.getEntryToolTipText(), resplan, "EntryToolTipText")));
				
				rp4.setRelationEntity(getEntityUIDByName(rp3.getRelationEntity()));
				rp4.setRelationFromField(getFieldUIDByNames(rp3.getRelationEntity(), rp3.getRelationFromField()));
				rp4.setRelationToField(getFieldUIDByNames(rp3.getRelationEntity(), rp3.getRelationToField()));
				
				rp4.setRelationPresentation(rp3.getRelationPresentation());
				rp4.setRelationFromPresentation(rp3.getRelationFromPresentation());
				rp4.setRelationToPresentation(rp3.getRelationToPresentation());
				rp4.setNewRelationFromController(rp3.isNewRelationFromController());
				
				rp4.setScriptingActivated(rp3.isScriptingActivated());
				rp4.setScriptingCode(migrateGroovycode(rp3.getScriptingCode()));
				rp4.setScriptingBackgroundPaintMethod(migrateGroovycode(rp3.getScriptingBackgroundPaintMethod()));
				rp4.setScriptingResourceCellMethod(migrateGroovycode(rp3.getScriptingResourceCellMethod()));
				rp4.setScriptingEntryCellMethod(migrateGroovycode(rp3.getScriptingEntryCellMethod()));
				
				if (rp3.getResources() != null) {
					List<RP_ResourceNew> lst = new ArrayList<RP_ResourceNew>();
					for (RP_ResourceOld rp3res : rp3.getResources()) {
						RP_ResourceNew rp4res = new RP_ResourceNew();
						
						rp4res.setLocaleId(migrateInternalId(rp3res.getLocaleId()));
						rp4res.setLocaleLabel(rp3res.getLocaleLabel());
						
						rp4res.setResourceLabel(migrateRefString(rp3.getResourceEntity(), rp3res.getResourceLabel(), 
								String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3res.getResourceLabel(), resplan, "Resource-ResourceLabel")));
						rp4res.setResourceTooltip(migrateRefString(rp3.getResourceEntity(), rp3res.getResourceTooltip(), 
								String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3res.getResourceTooltip(), resplan, "Resource-ResourceTooltip")));
						
						rp4res.setBookingLabel(migrateRefString(rp3.getEntryEntity(), rp3res.getBookingLabel(), 
								String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3res.getBookingLabel(), resplan, "Resource-BookingLabel")));
						rp4res.setBookingTooltip(migrateRefString(rp3.getEntryEntity(), rp3res.getBookingTooltip(), 
								String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3res.getBookingTooltip(), resplan, "Resource-ResourceTooltip")));
						
						rp4res.setLegendLabel(rp3res.getLegendLabel());
						rp4res.setLegendTooltip(rp3res.getLegendTooltip());
						
						lst.add(rp4res);
					};
					rp4.setResources(lst);
				}
				
				if (rp3.getResourceLocales() != null) {
					List<RP_LocaleNew> lst = new ArrayList<RP_LocaleNew>();
					for (RP_LocaleOld rp3loc : rp3.getResourceLocales()) {
						RP_LocaleNew rp4loc = new RP_LocaleNew();
						
						rp4loc.setLocaleId(migrateInternalId(rp3loc.getLocaleId()));
						rp4loc.setLocaleLabel(rp3loc.getResourceLabel());
						
						rp4loc.setResourceLabel(migrateRefString(rp3.getResourceEntity(), rp3loc.getResourceLabel(), 
								String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3loc.getResourceLabel(), resplan, "Resource-ResourceLabel")));
						rp4loc.setResourceTooltip(migrateRefString(rp3.getResourceEntity(), rp3loc.getResourceTooltip(), 
								String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp4loc.getResourceTooltip(), resplan, "Resource-ResourceTooltip")));
						
						rp4loc.setLegendLabel(rp3loc.getLegendLabel());
						rp4loc.setLegendTooltip(rp3loc.getLegendTooltip());
						
						lst.add(rp4loc);
					}
					rp4.setResourceLocales(lst);
				}
				
				if (rp3.getPlanElements() != null) {
					List<RP_PlanElementNew> lstPE = new ArrayList<RP_PlanElementNew>();
					for (RP_PlanElementOld rp3pe : rp3.getPlanElements()) {
						RP_PlanElementNew rp4pe = new RP_PlanElementNew();
						
						rp4pe.setType(rp3pe.getType());
						rp4pe.setEntity(getEntityUIDByName(rp3pe.getEntity()));
						rp4pe.setPrimaryField(getFieldUIDByNames(rp3pe.getEntity(), rp3pe.getPrimaryField()));
						rp4pe.setSecondaryField(getFieldUIDByNames(rp3pe.getEntity(), rp3pe.getSecondaryField()));
						
						rp4pe.setDateFromField(getFieldUIDByNames(rp3pe.getEntity(), rp3pe.getDateFromField()));
						rp4pe.setDateUntilField(getFieldUIDByNames(rp3pe.getEntity(), rp3pe.getDateUntilField()));
						rp4pe.setTimePeriodsString(rp3pe.getTimePeriodsString());
						rp4pe.setTimeFromField(getFieldUIDByNames(rp3pe.getEntity(), rp3pe.getTimeFromField()));
						rp4pe.setTimeUntilField(getFieldUIDByNames(rp3pe.getEntity(), rp3pe.getTimeUntilField()));

						rp4pe.setLabelText(migrateRefString(rp3pe.getEntity(), rp3pe.getLabelText(), 
								String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3pe.getLabelText(), resplan, "Planelement-LabelText")));
						rp4pe.setToolTipText(migrateRefString(rp3pe.getEntity(), rp3pe.getToolTipText(), 
								String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3pe.getToolTipText(), resplan, "Planelement-ToolTipText")));
						
						rp4pe.setScriptingEntryCellMethod(migrateGroovycode(rp3pe.getScriptingEntryCellMethod()));
						
						rp4pe.setPresentation(rp3pe.getPresentation());
						rp4pe.setFromPresentation(rp3pe.getFromPresentation());
						rp4pe.setToPresentation(rp3pe.getToPresentation());
						rp4pe.setNewRelationFromController(rp3pe.isNewRelationFromController());
						
						rp4pe.setColor(rp3pe.getColor());
						rp4pe.setFromIcon(rp3pe.getFromIcon());
						rp4pe.setToIcon(rp3pe.getToIcon());
						
						if (rp3pe.getPlanElementLocaleVO() != null) {
							List<RP_PlanElementLocaleNew> lstPEL = new ArrayList<RP_PlanElementLocaleNew>();
							for(RP_PlanElementLocaleOld rp3pel : rp3pe.getPlanElementLocaleVO()) {
								RP_PlanElementLocaleNew rp4pel = new RP_PlanElementLocaleNew();
								
								rp4pel.setLocaleId(migrateInternalId(rp3pel.getLocaleId()));
								rp4pel.setLocaleLabel(rp3pel.getLocaleLabel());
								
								rp4pel.setBookingLabel(migrateRefString(rp3.getEntryEntity(), rp3pel.getBookingLabel(), 
										String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3pel.getBookingLabel(), resplan, "PlanelementLocale-BookingLabel")));
								rp4pel.setBookingTooltip(migrateRefString(rp3.getEntryEntity(), rp3pel.getBookingTooltip(), 
										String.format("ref definition \"%s\" of resplan %s.%s contains invalid value: ", rp3pel.getBookingTooltip(), resplan, "PlanelementLocale-ResourceTooltip")));
																
								lstPEL.add(rp4pel);
							}
							rp4pe.setPlanElementLocaleVO(lstPEL);
						}
						
						lstPE.add(rp4pe);
					}
					rp4.setPlanElements(lstPE);
				}
				
				bytes = rp4.toBytes();
				eo.setValue(E_03_15_0004.CUSTOMCOMPONENT.data, bytes);
				helper.update(E_03_15_0004.CUSTOMCOMPONENT, eo);
			} catch (Exception ex) {
				LOG.warn("unable to migrate resplan with pk " + eo.getPrimaryKey(), ex);
			} catch (java.lang.LinkageError ex) {
				LOG.warn("unable to migrate resplan with pk " + eo.getPrimaryKey(), ex);
			}
		}
	}
	
	private UID migrateInternalId(Integer id) {
		if (id == null) {
			return null;
		}
		return new UID(id.intValue()+"");
	}
	
	private void migrateLocalIdentifiers() {
		logTitle(String.format("migrate local identifiers"));
		for (RigidEO eoNuclet : helper.getAll(E_03_15_0004.NUCLET)) {
			String localident = createUniqueLocalIdentifier(
					eoNuclet.getForeignUID(E_03_15_0004.NUCLET.pkuid),
					eoNuclet.getValue(E_03_15_0004.NUCLET.namespace), 
					eoNuclet.getValue(E_03_15_0004.NUCLET.description));
			LOG.debug(String.format("new local identifier %s for nuclet %s", localident, eoNuclet.getForeignUID(E_03_15_0004.NUCLET.pkuid).getString()));
			eoNuclet.setValue(E_04_00_0022_pre.NUCLET.localidentifier, localident);
			eoNuclet.setValue(E_04_00_0022_pre.NUCLET.nuclon, false);
			eoNuclet.setValue(E_04_00_0022_pre.NUCLET.source, true);
			helper.update(E_04_00_0022_pre.NUCLET, eoNuclet);
		}
		
		// rename db objects
		Map<String, String> renamedObjects = new HashMap<String, String>();
		for (RigidEO eoDbObject : helper.getAll(E_03_15_0004.DBOBJECT)) {
			String name = eoDbObject.getValue(E_03_15_0004.DBOBJECT.name).toUpperCase();
			String uidNuclet = eoDbObject.getValue(E_03_15_0004.DBOBJECT.nucletuid);
			String localident;
			if (uidNuclet != null) {
				localident = localIdentifier.get(new UID(uidNuclet));
			} else {
				localident = NucletConstants.DEFAULT_LOCALIDENTIFIER;
			}
			assert localident != null;
			
			String newName = localident + "_" + name;
			if (newName.length() > 30) {
				// cut name
				newName = newName.substring(0, 30);
				int i = 1;
				while (renamedObjects.containsValue(newName)) {
					newName = newName.substring(0, 30-String.valueOf(i).length())+i;
					assert newName.length() <= 30;
					i++;
				}
			}
			
			LOG.debug(String.format("extend db object name \"%s\" --> \"%s\"", name, newName));
			eoDbObject.setValue(E_03_15_0004.DBOBJECT.name, newName);
			helper.update(E_03_15_0004.DBOBJECT, eoDbObject);
			
			renamedObjects.put(name, newName);
		}
		
		// rename tables
		Map<String, String> renamedTables = new HashMap<String, String>();
		for (RigidEO eoEntity : helper.getAll(E_03_15_0004.ENTITY)) {
			String entity = eoEntity.getValue(E_03_15_0004.ENTITY.entity);
			String virtualentity = eoEntity.getValue(E_03_15_0004.ENTITY.virtualentity);
			String readdelegate = eoEntity.getValue(E_03_15_0004.ENTITY.readDelegate);
			if (RigidUtils.looksEmpty(virtualentity)) {
				String tablename = MetaDbHelper.getTableName(
						virtualentity,
						eoEntity.getValue(E_03_15_0004.ENTITY.dbentity)).toUpperCase();
				String uidNuclet = eoEntity.getValue(E_03_15_0004.ENTITY.nucletuid);
				String localident;
				if (uidNuclet != null) {
					localident = localIdentifier.get(new UID(uidNuclet));
				} else {
					localident = NucletConstants.DEFAULT_LOCALIDENTIFIER;
				}
				assert localident != null;
				
				String newTablename = localident + tablename.substring(4);
				LOG.debug(String.format("entity \"%s\": rename db entity \"%s\" --> \"%s\"", entity, tablename, newTablename));
				
				try {
					if (!RigidUtils.equal(tablename, newTablename)) { //may be preferred l.i. is "T_EO"
						helper.renameTable(tablename, newTablename);
					}
				} catch (NotSupportedException e) {
					// could not happen
					throw new NuclosFatalException(e);
				}
				
				eoEntity.setValue(E_03_15_0004.ENTITY.dbentity, newTablename);
				
				// search for db object (read delegate)
				if (renamedObjects.containsKey(readdelegate)) {
					String newReaddelegate = renamedObjects.get(readdelegate);
					LOG.debug(String.format("entity \"%s\": rename read delegate \"%s\" --> \"%s\"", entity, readdelegate, newReaddelegate));
					eoEntity.setValue(E_03_15_0004.ENTITY.readDelegate, newReaddelegate);
				}
				
				helper.update(E_03_15_0004.ENTITY, eoEntity);
				
				entitiesByName.get(eoEntity.getValue(E_03_15_0004.ENTITY.entity)).setValue(E_03_15_0004.ENTITY.dbentity, newTablename);
				renamedTables.put(tablename, newTablename);
				renamedTables.put(MetaDbHelper.getViewName(tablename), newTablename);
				
			} else {
				// search for db object (virtual entity)
				virtualentity = virtualentity.toUpperCase(); 
				if (renamedObjects.containsKey(virtualentity)) {
					String newVirtualentity = renamedObjects.get(virtualentity);
					LOG.debug(String.format("entity \"%s\": rename virtual entity \"%s\" --> \"%s\"", entity, virtualentity, newVirtualentity));
					eoEntity.setValue(E_03_15_0004.ENTITY.virtualentity, newVirtualentity);
					eoEntity.setValue(E_03_15_0004.ENTITY.dbentity, newVirtualentity);
					helper.update(E_03_15_0004.ENTITY, eoEntity);
				}
			}
		}
		
		// replace row color script
		for (RigidEO eoEntity : helper.getAll(E_03_15_0004.ENTITY)) {
			String entity = eoEntity.getValue(E_03_15_0004.ENTITY.entity);
			NuclosScript rowcolorscript = eoEntity.getValue(E_03_15_0004.ENTITY.rowcolorscript);
			if (rowcolorscript != null) {
				if (!RigidUtils.looksEmpty(rowcolorscript.getSource())) {
					LOG.debug(String.format("entity \"%s\": migrate rowcolorscript", entity));
					String groovycode = rowcolorscript.getSource();
					rowcolorscript.setSource(migrateGroovycode(groovycode));
					helper.update(E_03_15_0004.ENTITY, eoEntity);
				}
			}
		}
		
		// replace calc function / script
		for (RigidEO eoField : helper.getAll(E_03_15_0004.ENTITYFIELD)) {
			String field = eoField.getValue(E_03_15_0004.ENTITYFIELD.field);
			String function = eoField.getValue(E_03_15_0004.ENTITYFIELD.calcfunction);
			boolean update = false;
			if (!RigidUtils.looksEmpty(function)) {
				function = function.toUpperCase();
				if (renamedObjects.containsKey(function)) {
					String newFunction = renamedObjects.get(function);
					LOG.debug(String.format("field \"%s\": rename calc function \"%s\" --> \"%s\"", field, function, newFunction));
					eoField.setValue(E_03_15_0004.ENTITYFIELD.calcfunction, newFunction);
					update = true;
				}
			}
			NuclosScript calcscript = eoField.getValue(E_03_15_0004.ENTITYFIELD.calculationscript);
			if (calcscript != null) {
				if (!RigidUtils.looksEmpty(calcscript.getSource())) {
					LOG.debug(String.format("field \"%s\": migrate calcscript", field));
					String groovycode = calcscript.getSource();
					calcscript.setSource(migrateGroovycode(groovycode));
					update = true;
				}
			}
			NuclosScript backgroundscript = eoField.getValue(E_03_15_0004.ENTITYFIELD.backgroundcolorscript);
			if (backgroundscript != null) {
				if (!RigidUtils.looksEmpty(backgroundscript.getSource())) {
					LOG.debug(String.format("field \"%s\": migrate backgroundscript", field));
					String groovycode = backgroundscript.getSource();
					backgroundscript.setSource(migrateGroovycode(groovycode));
					update = true;
				}
			}
			if (update) {
				helper.update(E_03_15_0004.ENTITYFIELD, eoField);
			}
		}
		
		// replace job function / procedure
		for (RigidEO eoJobDbObject : helper.getAll(E_03_15_0004.JOBDBOBJECT)) {
			String object = eoJobDbObject.getValue(E_03_15_0004.JOBDBOBJECT.object);
			if (!RigidUtils.looksEmpty(object)) {
				object = object.toUpperCase();
				if (renamedObjects.containsKey(object)) {
					String newObject = renamedObjects.get(object);
					LOG.debug(String.format("job dbobject: rename function/procedure \"%s\" --> \"%s\"", object, newObject));
					eoJobDbObject.setValue(E_03_15_0004.JOBDBOBJECT.object, newObject);
					helper.update(E_03_15_0004.JOBDBOBJECT, eoJobDbObject);
				}
			}
		}
		
		// replace in dbobject source
		for (RigidEO eoSource : helper.getAll(E_03_15_0004.DBSOURCE)) {
			String name = eoSource.getValue(E_03_15_0004.DBSOURCE.dbobject).toUpperCase();
			eoSource.setValue(E_03_15_0004.DBSOURCE.dbobject, renamedObjects.get(name));
			String source = eoSource.getValue(E_03_15_0004.DBSOURCE.source);
			String drop = eoSource.getValue(E_03_15_0004.DBSOURCE.dropstatement);
			for (String search : renamedObjects.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedObjects.get(search));
			}
			for (String search : renamedTables.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedTables.get(search));
			}
			if (!RigidUtils.looksEmpty(drop)) {
				for (String search : renamedObjects.keySet()) {
					drop = DbObjectUtils.replaceName(drop, search, renamedObjects.get(search));
				}
				for (String search : renamedTables.keySet()) {
					drop = DbObjectUtils.replaceName(drop, search, renamedTables.get(search));
				}
			}
			
			eoSource.setValue(E_03_15_0004.DBSOURCE.source, source);
			eoSource.setValue(E_03_15_0004.DBSOURCE.dropstatement, drop);
			helper.update(E_03_15_0004.DBSOURCE, eoSource);
		}
		
		// replace in datasources
		for (RigidEO eoDatasource : helper.getAll(E_03_15_0004.DATASOURCE)) {
			String source = eoDatasource.getValue(E_03_15_0004.DATASOURCE.source);
			for (String search : renamedObjects.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedObjects.get(search));
			}
			for (String search : renamedTables.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedTables.get(search));
			}
			eoDatasource.setValue(E_03_15_0004.DATASOURCE.source, source);
			helper.update(E_03_15_0004.DATASOURCE, eoDatasource);
		}
		for (RigidEO eoDatasource : helper.getAll(E_03_15_0004.DYNAMICENTITY)) {
			String source = eoDatasource.getValue(E_03_15_0004.DYNAMICENTITY.source);
			for (String search : renamedObjects.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedObjects.get(search));
			}
			for (String search : renamedTables.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedTables.get(search));
			}
			eoDatasource.setValue(E_03_15_0004.DYNAMICENTITY.source, source);
			helper.update(E_03_15_0004.DYNAMICENTITY, eoDatasource);
		}
		for (RigidEO eoDatasource : helper.getAll(E_03_15_0004.VALUELISTPROVIDER)) {
			String source = eoDatasource.getValue(E_03_15_0004.VALUELISTPROVIDER.source);
			for (String search : renamedObjects.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedObjects.get(search));
			}
			for (String search : renamedTables.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedTables.get(search));
			}
			eoDatasource.setValue(E_03_15_0004.VALUELISTPROVIDER.source, source);
			helper.update(E_03_15_0004.VALUELISTPROVIDER, eoDatasource);
		}
		for (RigidEO eoDatasource : helper.getAll(E_03_15_0004.RECORDGRANT)) {
			String source = eoDatasource.getValue(E_03_15_0004.RECORDGRANT.source);
			for (String search : renamedObjects.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedObjects.get(search));
			}
			for (String search : renamedTables.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedTables.get(search));
			}
			eoDatasource.setValue(E_03_15_0004.RECORDGRANT.source, source);
			helper.update(E_03_15_0004.RECORDGRANT, eoDatasource);
		}
		for (RigidEO eoDatasource : helper.getAll(E_03_15_0004.DYNAMICTASKLIST)) {
			String source = eoDatasource.getValue(E_03_15_0004.DYNAMICTASKLIST.source);
			for (String search : renamedObjects.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedObjects.get(search));
			}
			for (String search : renamedTables.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedTables.get(search));
			}
			eoDatasource.setValue(E_03_15_0004.DYNAMICTASKLIST.source, source);
			helper.update(E_03_15_0004.DYNAMICTASKLIST, eoDatasource);
		}
		for (RigidEO eoDatasource : helper.getAll(E_03_15_0004.CHART)) {
			String source = eoDatasource.getValue(E_03_15_0004.CHART.source);
			for (String search : renamedObjects.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedObjects.get(search));
			}
			for (String search : renamedTables.keySet()) {
				source = DbObjectUtils.replaceName(source, search, renamedTables.get(search));
			}
			eoDatasource.setValue(E_03_15_0004.CHART.source, source);
			helper.update(E_03_15_0004.CHART, eoDatasource);
		}
	}
	
	private void migrateResources() {
		logTitle(String.format("migrate resources"));
		File resourceDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.RESOURCE_PATH);
		for (RigidEO eoResource : helper.getAll(E_04_00_0022_pre.RESOURCE)) {
			try {
				if (eoResource.getValue(E_04_00_0022_pre.RESOURCE.content) == null) {
					DocumentFileBase dfb = eoResource.getValue(E_04_00_0022_pre.RESOURCE.file);
					String filename = dfb.getFilename();
				
					LOG.debug(String.format("migrate resource \"%s\" (file=%s)", eoResource.getValue(E_04_00_0022_pre.RESOURCE.name), filename));
				
					java.io.File file = new java.io.File(resourceDir, filename);
	
					// read contents
					FileInputStream in = new FileInputStream(file);
					byte[] content = new byte[(int) file.length()];    
					in.read(content);
					in.close();
					
					eoResource.setValue(E_04_00_0022_pre.RESOURCE.content, content);
					helper.update(E_04_00_0022_pre.RESOURCE, eoResource);
					
					//file.delete();
				}
			} catch (Exception ex) {
				LOG.warn("unable to migrate resource with pk " + eoResource.getPrimaryKey() + " -> filled with empty image.", ex);
				eoResource.setValue(E_04_00_0022_pre.RESOURCE.content, EMPTY_IMAGE);
				helper.update(E_04_00_0022_pre.RESOURCE, eoResource);
			}
		}
	}
	
	private class LocalIdentifierStore implements DbObjectUtils.LocalIdentifierStore {
		@Override
		public boolean exist(String li) {
			return localIdentifier.containsValue(li);
		}
		@Override
		public void set(UID nucletUID, String li) {
			localIdentifier.put(nucletUID, li);
		}
	}
	
	private LocalIdentifierStore liStore = new LocalIdentifierStore();
	
	private String createUniqueLocalIdentifier(UID uidNuclet, String oldNameSpace, String nucletDescription) {
		String li = null;
		if (nucletDescription != null) {
			final int keyLength = NUCLET_PREFERRED_LOCAL_IDENTIFIER.length();
			int preferredIndex = nucletDescription.indexOf(NUCLET_PREFERRED_LOCAL_IDENTIFIER);
			if (preferredIndex >= 0 && nucletDescription.length() >= preferredIndex + keyLength + 4) {
				String preferredLocalIdentifier = nucletDescription.substring(preferredIndex + keyLength, preferredIndex + keyLength + 4).toUpperCase();
				if (!liStore.exist(preferredLocalIdentifier)) {
					liStore.set(uidNuclet, preferredLocalIdentifier);
					li = preferredLocalIdentifier;
				}
			}
		}
		if (li == null) {
			li = DbObjectUtils.createUniqueLocalIdentifier(uidNuclet, liStore);
		}
		namespaceByLocalIdent.put(li, oldNameSpace);
		return li;
	}
	
	private void dropViews() {
		logTitle(String.format("drop views"));
		Set<String> existingDbObjectViews = new HashSet<String>();
		for (RigidEO eoDbObject : helper.getAll(E_03_15_0004.DBOBJECT)) {
			String sObjectName = eoDbObject.getValue(E_03_15_0004.DBOBJECT.name);
			String sObjectType = eoDbObject.getValue(E_03_15_0004.DBOBJECT.dbobjecttype);
			if ("view".equals(sObjectType.toLowerCase())) {
				existingDbObjectViews.add(sObjectName.toUpperCase());
			}
		}
		for (String viewName : helper.getDbAccess().getTableNames(DbTableType.VIEW)) {
			if (existingDbObjectViews.contains(viewName.toUpperCase())) {
				LOG.debug(String.format("View %s is a DbObject and will be dropped later", viewName));
				continue;
			}
			DbSimpleView dbView = new DbSimpleView(null, null, viewName, Collections.EMPTY_LIST);
			DbStructureChange drop = new DbStructureChange(DbStructureChange.Type.DROP, dbView);
			helper.getDbAccess().execute(drop);
	    }
	}
	
	
	private void manageDbObjects(int manage) {
		final boolean create = manage == 1;
		final boolean drop = manage == 0;
		logTitle(String.format((create?"create":"drop")+" db objects"));
		
		final Map<String, Integer> objOrder = new HashMap<String, Integer>();
		final Map<String, String> objType = new HashMap<String, String>();
		for (RigidEO eoDbObject : helper.getAll(E_03_15_0004.DBOBJECT)) {
			String sObjectName = eoDbObject.getValue(E_03_15_0004.DBOBJECT.name).toUpperCase();
			objOrder.put(sObjectName, RigidUtils.defaultIfNull(eoDbObject.getValue(E_03_15_0004.DBOBJECT.order), new Integer(-1)));
			objType.put(sObjectName, eoDbObject.getValue(E_03_15_0004.DBOBJECT.dbobjecttype));
		}
		List<RigidEO> sources = new ArrayList<RigidEO>(helper.getAll(E_03_15_0004.DBSOURCE));
		Collections.sort(sources, new Comparator<RigidEO>() {
			@Override
			public int compare(RigidEO o1, RigidEO o2) {
				if (create) {
					return RigidUtils.compare(
							RigidUtils.defaultIfNull(objOrder.get(o1.getValue(E_03_15_0004.DBSOURCE.dbobject).toUpperCase()), new Integer(-1)),
							RigidUtils.defaultIfNull(objOrder.get(o2.getValue(E_03_15_0004.DBSOURCE.dbobject).toUpperCase()), new Integer(-1)));
				} 
				if (drop) {
					return RigidUtils.compare(
							RigidUtils.defaultIfNull(objOrder.get(o2.getValue(E_03_15_0004.DBSOURCE.dbobject).toUpperCase()), new Integer(-1)),
							RigidUtils.defaultIfNull(objOrder.get(o1.getValue(E_03_15_0004.DBSOURCE.dbobject).toUpperCase()), new Integer(-1)));
				}
				return 0;
		}});
		
		for (RigidEO eoSource : sources) {
			if (Boolean.TRUE.equals(eoSource.getValue(E_03_15_0004.DBSOURCE.active)) &&
					helper.getDbAccess().getDbType().toString().equals(eoSource.getValue(E_03_15_0004.DBSOURCE.dbtype))) {
				try {
					String sName = eoSource.getValue(E_03_15_0004.DBSOURCE.dbobject).toUpperCase();
					String sType = objType.get(sName);
					String sCreateSQL = eoSource.getValue(E_03_15_0004.DBSOURCE.source);
					String sDropSQL = eoSource.getValue(E_03_15_0004.DBSOURCE.dropstatement);
					Pair<DbPlainStatement, DbStatement> stmts = DbObjectHelper.getStatements(sName, sType, sCreateSQL, sDropSQL);
					
					if (create) {
						LOG.debug(String.format("create db object \"%s\"", eoSource.getValue(E_03_15_0004.DBSOURCE.dbobject)));
						helper.getDbAccess().execute(stmts.x);
					}
					if (drop) {
						LOG.debug(String.format("drop db object \"%s\"", eoSource.getValue(E_03_15_0004.DBSOURCE.dbobject)));
						helper.getDbAccess().execute(stmts.y);
					}
					
				} catch (Exception ex) {
					LOG.error(String.format("%s of active db object \"%s\" failed: %s", (create?"create":"drop"), eoSource.getValue(E_03_15_0004.DBSOURCE.dbobject), ex.getMessage()), ex);
				}
			}
		}
	}
	
	private String replaceNamespace(String groovycode, String namespace, String localidentifier) {
		Matcher m = Pattern.compile(Pattern.quote("\"#{"+ namespace +".")).matcher(groovycode);
		
		StringBuffer sb = new StringBuffer(groovycode.length());
		int index = 0;
		while (m.find()) {
			sb.append(groovycode.substring(index, m.start()+3));
			sb.append(localidentifier);
			index = m.start()+3+namespace.length();
		}
		if (index < groovycode.length()-1) {
			sb.append(groovycode.substring(index));
		}
		
		return sb.toString();
	}
	
	private String migrateGroovycode(String groovycode) {
		if (groovycode == null) {
			return null;
		}
		for (String localident : namespaceByLocalIdent.keySet()) {
			String namespace = namespaceByLocalIdent.get(localident);
			if (namespace != null) {
				groovycode = replaceNamespace(groovycode, namespace, localident);
			}
		}
		return groovycode;
	}
	
	private void migrateDbStatics() {
		logTitle(String.format("migrate db statics"));
		for (Long oldId : entitiesById.keySet()) {
			RigidEO eoEntity = entitiesById.get(oldId);
			
			DbMap values = new DbMap();
			DbMap conditions = new DbMap();
			values.put(SimpleDbField.create("STRMODULE_UID", UID.class), (UID)eoEntity.getPrimaryKey());
			conditions.put(SimpleDbField.create("INTMODULE_ID", Long.class), oldId);
			DbUpdateStatement<Long> updstmt = new DbUpdateStatement<Long>("T_AD_MODULE_SEQUENTIALNUMBER", values, conditions);
			helper.getDbAccess().execute(updstmt);
		}
		
		DbMap conditions = new DbMap();
		conditions.putNull(SimpleDbField.create("STRMODULE_UID", UID.class));
		DbDeleteStatement<UID> del = new DbDeleteStatement<UID>("T_AD_MODULE_SEQUENTIALNUMBER", conditions);
		helper.getDbAccess().execute(del);
	}
	
	private void migrateDatasources() {
		logTitle(String.format("migrate datasources"));
		for (RigidEO eo : helper.getAll(E_03_15_0004.DATASOURCE)) {
			String datasourcename = eo.getValue(E_03_15_0004.DATASOURCE.name);
			LOG.debug(String.format("migrate datasource \"%s\"", datasourcename));
			
			try {
				String datasource = eo.getValue(E_03_15_0004.DATASOURCE.source);	
				
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		        DocumentBuilder builder = factory.newDocumentBuilder();
		        builder.setEntityResolver(new QuerybuildermodelDTDResolver());
		        Document document = builder.parse(new InputSource(new StringReader(datasource)));
		        
		        NodeList valuelistproviders = document.getElementsByTagName("valuelistprovider");
	            for (int i = 0; i < valuelistproviders.getLength(); i++) {
	            	Node valuelistprovider = valuelistproviders.item(i);
	            	NamedNodeMap vlpAttributes = valuelistprovider.getAttributes();
	            	Node typeNode = vlpAttributes.getNamedItem("type");
	            	String type = typeNode.getTextContent();
	            	final String name = type;
	            	final String value;
	            	if (RigidUtils.looksEmpty(type)) {
	            		value = null;
	            	} else if ("default".equals(type)) {
	            		value = type;
	            	} else if ("dependant".equals(type)) {
	            		value = type;
	            	} else if (type.endsWith(".ds")) {
	            		final String dsName = type.substring(0, type.length()-3);
	            		type = "ds";
	            		UID dsUID = null;
	            		// search for datasource with name
	            		for (RigidEO ds : helper.getAll(E_03_15_0004.VALUELISTPROVIDER)) {
	            			if (dsName.equalsIgnoreCase(ds.getValue(E_03_15_0004.VALUELISTPROVIDER.name))) {
	            				dsUID = getUID(E_03_15_0004.VALUELISTPROVIDER.getEntityName(), (Long) ds.getPrimaryKey());
	            				break;
	            			}
	            		}
	            		if (dsUID != null) {
	            			value = dsUID.getStringifiedDefinitionWithEntity(E_03_15_0004.VALUELISTPROVIDER);
	            		} else {
	            			value = null;
	            		}
	            	} else {
	            		value = type;
	            		type = "named";
	            	}
	            	if (value != null) {
	            		typeNode.setTextContent(type);
	            		Attr nameNode = document.createAttribute("name");
	            		Attr valueNode = document.createAttribute("value");
	            		nameNode.setTextContent(name);
	            		valueNode.setTextContent(value);
	            		vlpAttributes.setNamedItem(nameNode);
	            		vlpAttributes.setNamedItem(valueNode);
	            	}
	            }
		        
		        StringWriter datasourceWriter = new StringWriter();
	            javax.xml.transform.Transformer transformer = TransformerFactory.newInstance().newTransformer();
	            transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, QUERYBUILDERMODEL_DTD_SYSTEMIDENTIFIER);
	            transformer.transform(new DOMSource(document), new StreamResult(datasourceWriter));
	            
	            datasource = datasourceWriter.toString();
	            eo.setValue(E_03_15_0004.DATASOURCE.source, datasource);
	            helper.update(E_03_15_0004.DATASOURCE, eo);
		        
			} catch (Exception ex) {
				LOG.warn("unable to migrate datasource with pk " + eo.getPrimaryKey(), ex);
			}
		}
		
	}
	
	private static class QuerybuildermodelDTDResolver implements EntityResolver {

		@Override
		public InputSource resolveEntity(String publicId, String sSystemId) throws SAXException, IOException {
			try {
				InputSource result = null;
				if (sSystemId != null && sSystemId.equals(QUERYBUILDERMODEL_DTD_SYSTEMIDENTIFIER)) {
					final ClassLoader cl = Migration_04_00_0022_main.class.getClassLoader();
					final URL urlDtd = cl.getResource(QUERYBUILDERMODEL_DTD_RESSOURCEPATH);
					if (urlDtd == null) {
						throw new SAXException("Querybuildermodel.dtd not found");
					}
					result = new InputSource(new BufferedInputStream(urlDtd.openStream()));
				}
				return result;
			}
			catch (Exception ex) {
				throw new SAXException("Querybuildermodel.dtd not found", ex);
			}
		}
		
	}

}
