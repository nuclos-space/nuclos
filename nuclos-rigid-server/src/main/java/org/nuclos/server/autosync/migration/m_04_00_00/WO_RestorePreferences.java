package org.nuclos.server.autosync.migration.m_04_00_00;

import java.awt.Rectangle;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.UID;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.converters.ConverterLookup;
import com.thoughtworks.xstream.converters.DataHolder;
import com.thoughtworks.xstream.core.ReferenceByXPathMarshallingStrategy;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;

class WO_RestorePreferences {
	
	public static class CustomComponent3 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public String customComponentName;
		public String customComponentClass;
		public String instanceStateXML;
	}
	
	public static class CustomComponent4 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public UID customComponentUID;
		public String customComponentName;
		public String customComponentClass;
		public String instanceStateXML;
	}
	
	public static class ResPlan implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public String granularity;
		public String startDate;
		public String endDate;
		public String searchFilter;
		public Rectangle viewRect;
		@XStreamOmitField
		Object searchCondition;

		public int orientation;
		public final Map<String, Integer> resourceCellExtent = new HashMap<String, Integer>();
		public final Map<String, Integer> timelineCellExtent = new HashMap<String, Integer>();
	}
	
	public static class NuclosCollect4 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public UID  entity;
		public int  iCollectState;
		public Object objectId;
		public Map<String, String> inheritControllerPreferences = new HashMap<String, String>(1);
		public String customUsage;
	}
	
	public static class NuclosCollect3 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public String entity;
		public Integer iCollectState;
		public Long objectId;
		@XStreamOmitField
		Object searchCondition;
		public Map<String, String> inheritControllerPreferences = new HashMap<String, String>(1);
		public String customUsage;
	}
	
	public static class GOCollect3 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public String searchFilterName;
		public String resultTemplateName;

		public Integer processId;
	}
	
	public static class GOCollect4 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public UID searchFilterUID;
		public UID processUID;
	}
	
	public static class MDCollect4 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public UID searchFilterUID;
	}
	
	public static class MDCollect3 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public String searchFilterName;
	}
	
	public static class Task4 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public Integer type;
		public Integer refreshInterval;
		public UID searchFilterId;
		public UID tasklistId;
		public String tasklistName;
	}
	
	public static class Task3 implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		public Integer type;
		public Integer refreshInterval;
		public Integer searchFilterId;
		public Integer tasklistId;
		public String tasklistName;
	}
	
	public static class IgnoreExplorerStrategy extends ReferenceByXPathMarshallingStrategy {

		public IgnoreExplorerStrategy() {
			super(ReferenceByXPathMarshallingStrategy.RELATIVE);
		}

		@Override
		public Object unmarshal(Object root, HierarchicalStreamReader reader, DataHolder dataHolder, ConverterLookup converterLookup, Mapper mapper) {
			if ("org.nuclos.client.explorer.ExplorerController$RestorePreferences".equals(reader.getNodeName())) {
				return null;
			}
			if ("searchCondition".equals(reader.getNodeName())) {
				return null;
			}
			return super.unmarshal(root, reader, dataHolder, converterLookup, mapper);
		}

		@Override
		public void marshal(HierarchicalStreamWriter writer, Object obj, ConverterLookup converterLookup, Mapper mapper, DataHolder dataHolder) {
			super.marshal(writer, obj, converterLookup, mapper, dataHolder);
		}
		
	}
}
