package org.nuclos.server.datasource;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.UID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@SuppressWarnings("serial")
@XmlType(propOrder= {"entityUID","columns"})
@XmlRootElement(name="dsmeta")
public class DatasourceMetaVO implements Serializable {

	private static final Logger LOG = LoggerFactory.getLogger(DatasourceMetaVO.class);

	private UID entityUID;
	
	private List<ColumnMeta> columns;

	@XmlElement(name="entity")
	public final UID getEntityUID() {
		return entityUID;
	}

	public final void setEntityUID(UID entityUID) {
		this.entityUID = entityUID;
	}
	
	@XmlElementWrapper(name="columns")
	@XmlElement(name="column")
	public final List<ColumnMeta> getColumns() {
		return columns;
	}

	public final void setColumns(List<ColumnMeta> columns) {
		this.columns = columns;
	}

	@SuppressWarnings("serial")
	@XmlType(propOrder= {"fieldUID","columnName","javaType","scale","precision","readOnly"})
	@XmlRootElement(name="colmeta")
	public static class ColumnMeta implements Serializable {
		
		private UID fieldUID;
		
		private String columnName;
		
		private String javaType;
		
		private Integer scale;
		
		private Integer precision;
		
		private Boolean readOnly;
		
		
		@XmlElement(name="field")
		public final UID getFieldUID() {
			return fieldUID;
		}

		public final void setFieldUID(UID fieldUID) {
			this.fieldUID = fieldUID;
		}
		
		@XmlElement(name="columnname")
		public final String getColumnName() {
			return columnName;
		}

		public final void setColumnName(String columnName) {
			this.columnName = columnName;
		}
		
		@XmlElement(name="javatype")
		public final String getJavaType() {
			return javaType;
		}

		public final void setJavaType(String javaType) {
			this.javaType = javaType;
		}

		@XmlElement(name="scale")
		public final Integer getScale() {
			return scale;
		}

		public final void setScale(Integer scale) {
			this.scale = scale;
		}

		@XmlElement(name="precision")
		public final Integer getPrecision() {
			return precision;
		}

		public final void setPrecision(Integer precision) {
			this.precision = precision;
		}

		@XmlElement(name="readonly")
		public final Boolean getReadOnly() {
			return readOnly;
		}

		public final void setReadOnly(Boolean readOnly) {
			this.readOnly = readOnly;
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) return true;

			if (!(o instanceof ColumnMeta)) return false;

			final ColumnMeta that = (ColumnMeta) o;

			return new EqualsBuilder()
					.append(fieldUID, that.fieldUID)
					.append(columnName, that.columnName)
					.append(javaType, that.javaType)
					.append(scale, that.scale)
					.append(precision, that.precision)
					.append(readOnly, that.readOnly)
					.isEquals();
		}

		@Override
		public int hashCode() {
			return new HashCodeBuilder(17, 37)
					.append(fieldUID)
					.append(columnName)
					.append(javaType)
					.append(scale)
					.append(precision)
					.append(readOnly)
					.toHashCode();
		}
	}
	
	/**
	 * TODO:
	 * This is EXTREMELY AWKWARD: There is no real method to get the
	 * xml representation of an instance of this class.
	 * <p>
	 * The xml representation, however, is constructed with JAXB, 
	 * that can suffer from memory leaks if more than one JAXBContext is
	 * used. Refer to NUCLOS-2919.
	 * 
	 * @deprecated Use {@link #toXml} instead.
	 * @author Thomas Pasch (javadoc)
	 */
	@Override
	public String toString() {
		final StreamResult xmlResult = SourceResultHelper.newResultForString();
		JAXB.marshal(this, xmlResult);
		return ((StringWriter) xmlResult.getWriter()).toString();
	}
	
	public String toXml(Jaxb2Marshaller jaxb2Marshaller) {
		final StreamResult xmlResult = SourceResultHelper.newResultForString();
		jaxb2Marshaller.marshal(this, xmlResult);
		return ((StringWriter) xmlResult.getWriter()).toString();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;

		if (!(o instanceof DatasourceMetaVO)) return false;

		final DatasourceMetaVO that = (DatasourceMetaVO) o;

		return new EqualsBuilder()
				.append(entityUID, that.entityUID)
				.append(columns, that.columns)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(entityUID)
				.append(columns)
				.toHashCode();
	}
}
