package org.nuclos.layout.transformation.rule

import org.junit.Test
import org.nuclos.common2.JaxbMarshalUnmarshalUtil
import org.nuclos.schema.layout.layoutml.Layoutml
import org.nuclos.schema.layout.rule.Rules
import org.nuclos.layout.transformation.AbstractLayoutmlTransformerTest

import com.fasterxml.jackson.databind.ObjectMapper

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LayoutmlToRuleTransformerTest extends AbstractLayoutmlTransformerTest {

	@Test
	void testLayoutml2Rules() {
		InputStream xml = this.getClass().getResourceAsStream('../layout/example_rest_Order.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToRuleTransformer(getMetaProvider(), layoutml)
		Rules result = transformer.transform()

		assert !result.rules.empty
	}

	@Test
	void testRulesJSON() {
		InputStream xml = this.getClass().getResourceAsStream('../layout/example_rest_Order.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToRuleTransformer(getMetaProvider(), layoutml)
		Rules result = transformer.transform()

		ObjectMapper mapper = getMapper()
		StringWriter stringWriter = new StringWriter()

		mapper.writeValue(stringWriter, result);
		String serializedValue = stringWriter.toString()

		assert serializedValue
	}
}