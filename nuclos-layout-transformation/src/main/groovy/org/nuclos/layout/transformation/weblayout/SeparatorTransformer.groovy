package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Separator
import org.nuclos.schema.layout.layoutml.TitledSeparator
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebSeparator
import org.nuclos.schema.layout.web.WebTitledSeparator

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@CompileStatic
@PackageScope
class SeparatorTransformer extends ElementTransformer<Separator, WebComponent> {

	SeparatorTransformer() {
		super(Separator.class)
	}

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Separator separator) {
		WebSeparator result = factory.createWebSeparator()
		result.orientation = separator.orientation
		return result
	}

}
