package org.nuclos.layout.transformation.rule

import org.nuclos.schema.layout.layoutml.Clear
import org.nuclos.schema.layout.rule.RuleActionClear

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class ClearRuleTransformer extends ActionTransformer<Clear, RuleActionClear> {

	ClearRuleTransformer() {
		super(Clear.class)
	}

	@Override
	RuleActionClear transform(final Clear input) {
		RuleActionClear result = factory.createRuleActionClear()

		result.entity = input.entity
		result.targetcomponent = input.targetcomponent

		return result
	}
}
