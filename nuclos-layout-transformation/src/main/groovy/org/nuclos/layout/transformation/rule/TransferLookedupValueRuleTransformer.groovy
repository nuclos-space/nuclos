package org.nuclos.layout.transformation.rule

import org.nuclos.schema.layout.layoutml.TransferLookedupValue
import org.nuclos.schema.layout.rule.RuleActionTransferLookedupValue

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class TransferLookedupValueRuleTransformer extends ActionTransformer<TransferLookedupValue, RuleActionTransferLookedupValue> {

	TransferLookedupValueRuleTransformer() {
		super(TransferLookedupValue.class)
	}

	@Override
	RuleActionTransferLookedupValue transform(final TransferLookedupValue input) {
		RuleActionTransferLookedupValue result = factory.createRuleActionTransferLookedupValue()

		result.sourcefield = input.sourcefield
		result.targetcomponent = input.targetcomponent

		return result
	}
}
