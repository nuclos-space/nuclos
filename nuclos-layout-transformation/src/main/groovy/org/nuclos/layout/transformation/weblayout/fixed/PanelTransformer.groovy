package org.nuclos.layout.transformation.weblayout.fixed

import org.nuclos.schema.layout.layoutml.Panel
import org.nuclos.schema.layout.layoutml.Tablelayout
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebContainer
import org.nuclos.layout.transformation.weblayout.AbstractPanelTransformer
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class PanelTransformer extends AbstractPanelTransformer {
	private static final Logger log = LoggerFactory.getLogger(PanelTransformer.class)

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Panel panel) {
		if (panel.visible == org.nuclos.schema.layout.layoutml.Boolean.NO) {
			return
		}

		WebContainer result = createPanelOrContainer(panel)
		List<WebComponent> components = []

		result.table = factory.createWebTable()

		result.fontSize = convertFontSize(panel.font?.size)
		transferSizes(panel, result)

		result.opaque = panel.opaque == org.nuclos.schema.layout.layoutml.Boolean.YES

		panel.containerOrLabelOrTextfield.each {
			components.addAll(layoutTransformer.getWebComponents(it))
		}

		layoutTransformer.populateTable(result.table, components)

		return result
	}

	/**
	 * Transfers column and row sizes.
	 *
	 * @param panel
	 * @param result
	 */
	private void transferSizes(Panel panel, WebContainer result) {
		def layoutManager = panel.layoutmanager.value
		if (layoutManager instanceof Tablelayout) {
			Tablelayout tablelayout = (Tablelayout) layoutManager
			try {
				result.table.columnSizes.addAll(tablelayout.columns.split('\\|').collect {
					new BigDecimal("$it").toBigInteger()
				})
				result.table.rowSizes.addAll(tablelayout.rows.split('\\|').collect {
					new BigDecimal("$it").toBigInteger()
				})
			}
			catch (Exception ex) {
				log.warn 'Could not pare TableLayout', ex
			}
		}
	}
}
