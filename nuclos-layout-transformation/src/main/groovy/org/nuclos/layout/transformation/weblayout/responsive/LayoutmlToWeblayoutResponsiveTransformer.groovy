package org.nuclos.layout.transformation.weblayout.responsive

import org.nuclos.common.IMetaProvider
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer
import org.nuclos.layout.transformation.weblayout.TransformerRegistry
import org.nuclos.schema.layout.layoutml.Layoutml
import org.nuclos.schema.layout.layoutml.Panel
import org.nuclos.schema.layout.web.WebLayout
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic

/**
 * Transforms an instance of {@link Layoutml} to {@link WebLayout}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LayoutmlToWeblayoutResponsiveTransformer extends LayoutmlToWeblayoutTransformer {
	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutResponsiveTransformer.class)

	LayoutmlToWeblayoutResponsiveTransformer(
			final IMetaProvider metaProvider,
			final Layoutml layoutml
	) {
		super(metaProvider, layoutml)

		TransformerRegistry.register(Panel.class, new PanelTransformer())
	}

	@Override
	WebLayout internalTransform() {
		WebLayout result = factory.createWebLayout()

		result.grid = factory.createWebGrid()
		populateGrid(result.grid, webComponentStructure.componentsStructured)

		return result
	}
}
