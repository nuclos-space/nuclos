package org.nuclos.layout.transformation.weblayout.calculated

import org.nuclos.common.IMetaProvider
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer
import org.nuclos.layout.transformation.weblayout.TransformerRegistry
import org.nuclos.layout.transformation.weblayout.WebComponentStructure
import org.nuclos.schema.layout.layoutml.Layoutml
import org.nuclos.schema.layout.layoutml.Panel
import org.nuclos.schema.layout.web.WebCalcCell
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebGridCalculated
import org.nuclos.schema.layout.web.WebInputComponent
import org.nuclos.schema.layout.web.WebLabel
import org.nuclos.schema.layout.web.WebLayout
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic

/**
 * Transforms an instance of {@link Layoutml} to {@link WebLayout}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LayoutmlToWeblayoutCalcTransformer extends LayoutmlToWeblayoutTransformer {
	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutCalcTransformer.class)

	LayoutmlToWeblayoutCalcTransformer(
			final IMetaProvider metaProvider,
			final Layoutml layoutml
	) {
		super(metaProvider, layoutml)

		TransformerRegistry.register(Panel.class, new PanelTransformer())
	}

	@Override
	WebLayout internalTransform() {
		WebLayout result = factory.createWebLayout()

		result.calculated = factory.createWebGridCalculated()

		populateGridCalculated(result.calculated, webComponentStructure.componentsStructured, null)
		assignComponentIds(webComponentStructure)
		updateLabelsWithComponentIds(webComponentStructure)

		return result
	}

	private List<WebComponent> assignComponentIds(WebComponentStructure structure) {
		structure.componentsAsFlatList.findAll { WebInputComponent.class.isAssignableFrom(it.class) }.each {
			if (it.name) {
				int id = getNewAttributeId(it.name)
				it.id = layoutml.hashCode() + '-' + id
			}
		}
	}

	private List<WebComponent> updateLabelsWithComponentIds(WebComponentStructure structure) {
		structure.componentsAsFlatList.findAll { WebLabel.class.isAssignableFrom(it.class) }.each {
			WebLabel label = (WebLabel) it
			if (label.name) {
				Integer forId = getExistingAttributeId(label.name)
				if (forId) {
					label.forId =  layoutml.hashCode() + '-' + forId
				}
			}
		}
	}

	WebGridCalculated populateGridCalculated(
			WebGridCalculated grid,
			List<WebComponent> components,
			PanelTransformer.GridSizes gridSizes
	) {
		components.each {
			WebCalcCell cell = factory.createWebCalcCell()
			cell.components.add(it)

			PanelTransformer.CalculatedPosition pos = gridSizes?.getCellPosition(it)
			if (pos) {
				cell.left = pos.left
				cell.top = pos.top
				cell.width = pos.width
				cell.height = pos.height
			} else {
				cell.width = '100%'
				cell.height = '100%'
			}

			grid.cells << cell
		}

		// Sort cells by column and row (for correct tab order)
		grid.cells.sort { a, b ->
			WebComponent compA = a.components.first()
			WebComponent compB = b.components.first()
			compA?.column <=> compB?.column ?: compA?.row <=> compB?.row
		}

		return grid
	}
}
