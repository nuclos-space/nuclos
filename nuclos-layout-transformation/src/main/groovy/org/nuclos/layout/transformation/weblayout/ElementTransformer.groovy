package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Boolean
import org.nuclos.schema.layout.layoutml.TextFormat
import org.nuclos.schema.layout.layoutml.ValuelistProvider
import org.nuclos.schema.layout.web.ObjectFactory
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebInputComponent
import org.nuclos.schema.layout.web.WebSubformColumn
import org.nuclos.schema.layout.web.WebValuelistProvider

import groovy.transform.CompileStatic
/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
abstract class ElementTransformer<T, R extends WebComponent> {
	private final Class<T> clss
	protected ObjectFactory factory

	ElementTransformer(Class<T> clss) {
		this.clss = clss

		this.factory = new ObjectFactory()
	}

	abstract R transform(LayoutmlToWeblayoutTransformer layoutTransformer, T input)

	String convertFontSize(String fontSize) {
		if (!fontSize) {
			return null
		}

		try {
			int percent = 100 + fontSize.toInteger()*10
			return percent + '%'
		} catch (NumberFormatException e) {
			return null
		}
	}

	void setTextFormatInWebComponent(WebComponent webComponent, TextFormat textFormat) {
		if (!textFormat) {
			return
		}

		if (textFormat.bold == Boolean.YES) {
			webComponent.bold = true
		}
		if (textFormat.italic == Boolean.YES) {
			webComponent.italic = true
		}
		if (textFormat.underline == Boolean.YES) {
			webComponent.underline = true
		}
		if (textFormat.textColor) {
			int red = getIntValue(textFormat.textColor.red, 0)
			int green = getIntValue(textFormat.textColor.green, 0)
			int blue = getIntValue(textFormat.textColor.blue, 0)
			webComponent.textColor = String.format("#%02X%02X%02X", red, green, blue)
		}
	}

	int getIntValue(String sInteger, int iDefault) {
		if (!sInteger) {
			return iDefault
		}

		try {
			return sInteger.toInteger()
		} catch (NumberFormatException e) {
			return iDefault
		}
	}

	void transferValuelistProvider(
			final WebInputComponent webComponent,
			final ValuelistProvider valuelistProvider
	) {
		if (!valuelistProvider) {
			return
		}

		WebValuelistProvider vlp = transformValuelistProvider(valuelistProvider)

		webComponent.valuelistProvider = vlp
	}

	private WebValuelistProvider transformValuelistProvider(ValuelistProvider valuelistProvider) {
		WebValuelistProvider vlp = factory.createWebValuelistProvider()
		vlp.name = valuelistProvider.name
		vlp.type = valuelistProvider.type
		vlp.value = valuelistProvider.value
		vlp.fieldname = valuelistProvider.parameter.find { it.name == 'fieldname' }?.value
		vlp.idFieldname = valuelistProvider.parameter.find { it.name == 'id-fieldname' }?.value

		valuelistProvider.parameter.each {
			WebValuelistProvider.Parameter param = factory.createWebValuelistProviderParameter()
			param.name = it.name
			param.value = it.value
			vlp.parameter << param
		}
		vlp
	}

	void transferValuelistProvider(
			final WebSubformColumn subformColumn,
			final ValuelistProvider valuelistProvider
	) {
		if (!valuelistProvider) {
			return
		}

		WebValuelistProvider vlp = transformValuelistProvider(valuelistProvider)

		subformColumn.valuelistProvider = vlp
	}
}
