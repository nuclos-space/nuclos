package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.TitledSeparator
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebTitledSeparator

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@CompileStatic
@PackageScope
class TitledSeparatorTransformer extends ElementTransformer<TitledSeparator, WebComponent> {

	TitledSeparatorTransformer() {
		super(TitledSeparator.class)
	}

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, TitledSeparator titledSeparator) {
		WebTitledSeparator result = factory.createWebTitledSeparator()
		result.title = titledSeparator.title
		return result
	}

}
