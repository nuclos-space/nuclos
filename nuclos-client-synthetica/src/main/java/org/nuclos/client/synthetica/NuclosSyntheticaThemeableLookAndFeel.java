//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.synthetica;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;

import org.apache.log4j.Logger;

import de.javasoft.plaf.synthetica.SyntheticaBlackMoonLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaLookAndFeel;

public class NuclosSyntheticaThemeableLookAndFeel extends SyntheticaBlackMoonLookAndFeel {
	
	private static final Logger LOG = Logger.getLogger(NuclosSyntheticaThemeableLookAndFeel.class);

	private static final Map<String, URL> mapNuclosThemes = new HashMap<String, URL>();
	
	private static Class<?> nuclosThemeSettings;
	
	private static String NUCLOS_THEME;
	
	public NuclosSyntheticaThemeableLookAndFeel() throws ParseException {
		super();
	}

	@Override
	protected void loadCustomXML() throws ParseException {
		if (NUCLOS_THEME != null) {
			final URL url = mapNuclosThemes.get(NUCLOS_THEME);
			String s = url.toString();
			if (s.startsWith("jar:")) {
				s = s.substring(4);
			}
			if (s.startsWith("file:")) {
				s = s.substring(5);
			}
			final int idx = s.indexOf("!/");
			if (idx >= 0) {
				s = s.substring(idx + 1);
			}
			while (s.startsWith("//")) {
				s = s.substring(1);
			}
			LOG.info("Load SynthStyle from param string " + s + " (url is " + url + ")");
			loadXMLConfig(s);
			/*
			try {
				loadXMLConfig(url);
			} catch (IOException e) {
				throw new IllegalStateException(e);
			}
			 */
		}
	}
	
	/**
	 * This is a modified version from the (decompiled) {@link #loadXMLConfig(String)} in Synthetica 2.13.0.
	 * It is adapted to cope with URLs.
	 * <p>
	 * This is needed to get nuclos web start working (NUCLOS-2143).
	 * </p>
	 * @author Thomas Pasch
	 * @since 3.14.2
	 */
	private void loadXMLConfig(URL url) throws ParseException, IOException {
		final String paramString = url.toExternalForm();
		final boolean debug = isSystemPropertySet("synthetica.debug");
		final boolean outputVersion = isSystemPropertySet("synthetica.version");

		long l1 = System.currentTimeMillis();
		Object localObject1;
		String str2;
		if (paramString.endsWith(".xml")) {
			LOG.info("Load SynthStyle from URL " + url);
			load(url);
		}
		else {
			throw new UnsupportedOperationException();
		}

		if (isSystemPropertySet("synthetica.enhancedXMLLookup")) {
			try {
				localObject1 = "Synthetica.xml";
				LOG.info("Load SynthStyle from URL " + url + " [enhancedXMLLookup]");
				load(url);
				if (debug) {
					System.out.println("[Info] Found '" + ((String) localObject1) + "' configuration file.");
				}
			} catch (IllegalArgumentException localIllegalArgumentException1) {
				LOG.warn("Load SynthStyle failed: " + localIllegalArgumentException1);
			} catch (Exception localException1) {
				LOG.warn("Load SynthStyle failed: " + localException1, localException1);
			}

			String str1 = super.getClass().getName();
			try {
				str2 = str1.substring(str1.lastIndexOf(".") + 1) + ".xml";
				load(url);
				if (debug) {
					System.out.println("[Info] Found '" + str2 + "' configuration file.");
				}
			} catch (IllegalArgumentException localIllegalArgumentException2) {
			} catch (Exception localException2) {
				localException2.printStackTrace();
			}
		}

		long l2 = System.currentTimeMillis();
		if (isSystemPropertySet("synthetica.loadTime")) {
			System.out.println("Time for loading LAF: " + (l2 - l1) + "ms");
		}

		if (isSystemPropertySet("synthetica.blockLAFChange")) {
			throw new UnsupportedOperationException();
		}

		if (debug) {
			System.out.println("Synthetica debug mode is enabled!");
		}
		if (outputVersion)
			System.out.format("Synthetica V%s\n%s V%s\n", new Object[] {
					getSyntheticaVersion().toString(), getName(), getVersion().toString()
			});
	}

	/**
	 * 
	 * @return loaded nuclos theme name
	 */
	static String getNuclosTheme() {
		return NUCLOS_THEME;
	}

	void setNuclosTheme(String nuclosTheme, String nuclosFontFamily) {
		NUCLOS_THEME = null;
		if (nuclosTheme != null) {
			nuclosTheme = nuclosTheme.trim();
			if (mapNuclosThemes.get(nuclosTheme) != null) {
				NUCLOS_THEME = nuclosTheme;
			}
		} 
		LOG.info("setNuclosTheme(" + nuclosTheme + ")");
		setWindowsDecorated(false);
		try {
			setLookAndFeel(getClass().getName(), true, true);
			
			if (nuclosFontFamily != null)
				SyntheticaLookAndFeel.setFont(nuclosFontFamily, 11);
			
		} catch (Exception e) {
			LOG.error("failed to register L&F with setLookAndFeel(" + getClass().getName() + ", true, true): " + e, e);
		}
		updateNuclosThemeSettings();
	}
	
	private synchronized void loadNuclosThemeSettings() throws ClassNotFoundException {
		if (nuclosThemeSettings == null) {
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			if (cl == null) {
				cl = ClassLoader.getSystemClassLoader();
			}
			nuclosThemeSettings = cl.loadClass("org.nuclos.client.theme.NuclosThemeSettings");
		}
	}
	
	private void setDefaults() {
		try {
			loadNuclosThemeSettings();
			final Method setDefaults = nuclosThemeSettings.getDeclaredMethod("setDefaults", new Class[0]);
			setDefaults.invoke(null);
		} catch (Exception e) {
			throw new IllegalStateException("setDefaults failed", e);
		}
	}
	
	private void setFieldColor(String fieldname, Color color) {
		try {
			loadNuclosThemeSettings();
			final Field field = nuclosThemeSettings.getDeclaredField(fieldname);
			field.set(null, color);
		} catch (Exception e) {
			throw new IllegalStateException("setFieldColor(" + fieldname + ", " + color + ") failed", e);
		}
	}
	
	/**
	 * update NuclosThemeSettings from nuclosTheme.xml
	 * e.g. NuclosInactiveField background
	 */
	private void updateNuclosThemeSettings() {
		setDefaults();
		
		Color color;
		
		color = getDefaultColor("Panel.background");
		if (color != null) {
			setFieldColor("BACKGROUND_PANEL", color);
		}
			
		color = getDefaultColor("Nuclos.rootPane.background");
		if (color != null) {
			setFieldColor("BACKGROUND_ROOTPANE", color);
		}
		
		color = getDefaultColor("Nuclos.background3");
		if (color != null) {
			setFieldColor("BACKGROUND_COLOR3", color);
		}
		
		color = getDefaultColor("Nuclos.background4");
		if (color != null) {
			setFieldColor("BACKGROUND_COLOR4", color);
		}
		
		color = getDefaultColor("Nuclos.background5");
		if (color != null) {
			setFieldColor("BACKGROUND_COLOR5", color);
		}
		
		color = getDefaultColor("Nuclos.fieldInactive.background");
		if (color != null) {
			setFieldColor("BACKGROUND_INACTIVEFIELD", color);
		}
		
		color = getDefaultColor("Nuclos.rowInactive.background");
		if (color != null) {
			setFieldColor("BACKGROUND_INACTIVEROW", color);
		}
		
		color = getDefaultColor("Nuclos.columnInactive.background");
		if (color != null) {
			setFieldColor("BACKGROUND_INACTIVECOLUMN", color);
		}
		
		color = getDefaultColor("Nuclos.columnInactiveSelected.background");
		if (color != null) {
			setFieldColor("BACKGROUND_INACTIVESELECTEDCOLUMN", color);
		}
		
		color = getDefaultColor("Nuclos.bubble.background");
		if (color != null) {
			setFieldColor("BUBBLE_FILL_COLOR", color);
		}
		
		color = getDefaultColor("Nuclos.bubble.border");
		if (color != null) {
			setFieldColor("BUBBLE_BORDER_COLOR", color);
		}
	}
	
	static boolean registerNuclosTheme(String themeName, URL pathToXml) {
		if (themeName != null && pathToXml != null) {
			/*
			pathToXml = pathToXml.trim();
			if (!pathToXml.startsWith("/")) {
				pathToXml = "/" + pathToXml;
			}
			final ClassLoader cl = Thread.currentThread().getContextClassLoader();
			if (cl.getResource(pathToXml) != null) {
				mapNuclosThemes.put(themeName.trim(), pathToXml);
				return true;
			}
			 */
			mapNuclosThemes.put(themeName.trim(), pathToXml);
			return true;
		}
		return false;
	}
	
	static Set<String> getNuclosThemes() {
		return new HashSet<String>(mapNuclosThemes.keySet());
	}
	
	private Color getDefaultColor(String key) {
		final Color defaultColor = UIManager.getColor(key);
		if (defaultColor == null) {
			return defaultColor;
		}
		
		if (defaultColor instanceof ColorUIResource) {
			ColorUIResource c = (ColorUIResource) defaultColor;
			return new Color(c.getRed(), c.getGreen(), c.getBlue());
		} else {
			return defaultColor;
		}
	}
}
