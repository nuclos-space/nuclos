@echo off
rem Starting this script from a network drive there may be the error that UNC-Names are not supported
rem In this case add the value "DisableUNCCheck REG_DWORD 0x1" to the registry:
rem	HKEY_CURRENT_USER - Software - Microsoft - Command Processor
rem see also: https://support.microsoft.com/de-de/kb/156276

set HOST=${client.serverhost}
set HTTPS=${server.https.enabled}

if "%HTTPS%" == "true" (
	set SERVER=https://%HOST%:${server.https.port}/${server.name}
) ELSE (
	set SERVER=http://%HOST%:${server.http.port}/${server.name}
)

set MAXHEAP=-Xmx640m
set VMA0=-Dserver=%SERVER%
set VMA1=-Duser.home="%USERPROFILE%"
set VMA2=-Dfile.encoding=UTF-8
set MAINCLASS=org.nuclos.client.main.Nuclos

if exist jre if exist jre\bin if exist jre\bin\java.exe (
	set JAVAPATH=jre
	GOTO Unpacking
)

:FindJavaPath

SET KIT=JavaSoft\Java Runtime Environment
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

SET KIT=Wow6432Node\JavaSoft\Java Runtime Environment
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

SET KIT=JavaSoft\Java Development Kit
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

SET KIT=Wow6432Node\JavaSoft\Java Development Kit
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

@echo on
ECHO Failed to find Java
GOTO :EOF

:FoundJRE
call:ReadRegValue JAVAPATH "HKLM\Software\%KIT%\%VER%" "JavaHome"
@echo on
ECHO JavaPath: %JAVAPATH%
@echo off

GOTO Unpacking

:ReadRegValue
SET key=%2%
SET name=%3%
SET "%~1="
SET reg=reg
IF DEFINED ProgramFiles(x86) (
  IF EXIST %WINDIR%\sysnative\reg.exe SET reg=%WINDIR%\sysnative\reg.exe
)
FOR /F "usebackq tokens=3* skip=1" %%A IN (`%reg% QUERY %key% /v %name% 2^>NUL`) DO SET "%~1=%%A %%B"
GOTO :EOF

:Unpacking

set UNPACK200=%JAVAPATH%/bin/unpack200
set JAVAEXEC=%JAVAPATH%/bin/javaw

@echo on
echo Server=%SERVER%
echo JavaExec=%JAVAEXEC%
@echo off

if exist nuclos.jar (
	@echo on
	echo nuclos.jar exists
	@echo off
) else (
	@echo on
	echo extracting nuclos.jar...
	@echo off
	for %%i in (all-in-on*.jar.pack.gz) do "%UNPACK200%" %%i nuclos.jar
)

set NUCLOSLIBS="lib/*;nuclos.jar"

if exist extensions (
	set NUCLOSLIBS="lib/*;nuclos.jar;extensions/*"
)

@echo on

start "" "%JAVAEXEC%" %MAXHEAP% %VMA0% %VMA1% %VMA2% -cp %NUCLOSLIBS% %MAINCLASS%
exit 0
