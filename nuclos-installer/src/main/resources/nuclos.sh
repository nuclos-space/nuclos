#!/bin/bash
cd $(dirname $0)

HOST=${client.serverhost}
HTTPS=${server.https.enabled}

if [ "$HTTPS" = "true" ];
then
	SERVER=https://$HOST:${server.https.port}/${server.name}
else
	SERVER=http://$HOST:${server.http.port}/${server.name}
fi

MAXHEAP=-Xmx640m
VMA0=-Dserver=$SERVER
VMA1=-Duser.home=$HOME
VMA2=-Dfile.encoding=UTF-8
MAINCLASS=org.nuclos.client.main.Nuclos

UNPACK200=unpack200
JAVAEXEC=java

if [ -d jre -a -d jre/bin -a -f jre/bin/java ];
then
	JAVAPATH=jre
	UNPACK200=$JAVAPATH/bin/unpack200
	JAVAEXEC=$JAVAPATH/bin/java
	chmod +x $JAVAEXEC
	chmod +x $UNPACK200
fi

echo Server=$SERVER
echo JavaExec=$JAVAEXEC

if [ -f nuclos.jar ];
then
	echo "nuclos.jar exists"
else
	echo "extracting nuclos.jar..."
	$UNPACK200 all-in-on*.jar.pack.gz nuclos.jar
fi

NUCLOSLIBS="lib/*:nuclos.jar"

if [ -d extensions ];
then
	NUCLOSLIBS="lib/*:nuclos.jar:extensions/*"
fi

$JAVAEXEC $MAXHEAP $VMA0 $VMA1 $VMA2 -cp $NUCLOSLIBS $MAINCLASS
