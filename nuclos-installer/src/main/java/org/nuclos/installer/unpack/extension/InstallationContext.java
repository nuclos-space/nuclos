package org.nuclos.installer.unpack.extension;

import java.io.File;

import org.nuclos.installer.mode.Installer;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class InstallationContext {
	private final String tomcatVersion;
	private final File tomcatHome;
	private final File webclientDir;
	private final File nuclosHome;
	private final Installer installer;
	private final File extensionsDir;
	private final File webappDir;

	// TODO: Builder pattern
	public InstallationContext(
			final String tomcatVersion,
			final File tomcatHome,
			final File webclientDir,
			final File nuclosHome,
			final Installer installer,
			final File extensionsDir,
			final File webappDir
	) {
		this.tomcatVersion = tomcatVersion;
		this.tomcatHome = tomcatHome;
		this.webclientDir = webclientDir;
		this.nuclosHome = nuclosHome;
		this.installer = installer;
		this.extensionsDir = extensionsDir;
		this.webappDir = webappDir;
	}

	public String getTomcatVersion() {
		return tomcatVersion;
	}

	public File getWebclientDir() {
		return webclientDir;
	}

	public File getNuclosHome() {
		return nuclosHome;
	}

	public Installer getInstaller() {
		return installer;
	}

	public File getExtensionsDir() {
		return extensionsDir;
	}

	public File getWebappDir() {
		return webappDir;
	}

	public File getTomcatHome() {
		return tomcatHome;
	}
}
