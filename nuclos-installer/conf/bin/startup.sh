#!/bin/sh
# Nuclos Server Script

echo "startup.sh start set variables"

CATALINA_HOME="${server.tomcat.dir}"
export CATALINA_HOME
CATALINA_OPTS="-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom"
export CATALINA_OPTS
JRE_HOME="${server.java.home}"
export JRE_HOME
JAVA_OPTS="-Xmx${server.heap.size}m ${server.environment.jvm}"
export JAVA_OPTS

echo "startup.sh finish set variables"

exec "$CATALINA_HOME/bin/startup.sh" "$@"

