//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;

public class SplitViewSettings implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6588150189963954442L;
	
	public static final int NO_FIX_SIZE = -1;
	
	private int fixSize = NO_FIX_SIZE;
	private boolean fixFirstPane = false;
	private boolean horizontal = false;
	public SplitViewSettings() {
	}
	public int getFixSize() {
		return fixSize;
	}
	public boolean isFixSize() {
		return fixSize != NO_FIX_SIZE;
	}
	public void setFixSize(int fixSize) {
		this.fixSize = fixSize;
	}
	public boolean isFixFirstPane() {
		return fixFirstPane;
	}
	public void setFixFirstPane() {
		this.fixFirstPane = true;
	}
	public boolean isFixSecondPane() {
		return !fixFirstPane;
	}
	public void setFixSecondPane() {
		this.fixFirstPane = false;
	}
	public boolean isHorizontal() {
		return horizontal;
	}
	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}
	public static SplitViewSettings fromString(String s) {
		SplitViewSettings result = new SplitViewSettings();
		String[] parts = s.split(";");
		for (String part : parts) {
			String[] keyvalue = part.split("=");
			if ("fixsize".equals(keyvalue[0])) {
				result.fixSize = Integer.parseInt(keyvalue[1]);
			} else if ("fixfirst".equals(keyvalue[0])) {
				result.fixFirstPane = Boolean.parseBoolean(keyvalue[1]);
			} else if ("horizontal".equals(keyvalue[0])) {
				result.horizontal = Boolean.parseBoolean(keyvalue[1]);
			}
		}
		return result;
	}
	public SplitViewSettings copy() {
		SplitViewSettings settingsCopy = new SplitViewSettings();
		settingsCopy.fixSize = this.fixSize;
		settingsCopy.horizontal = this.horizontal;
		settingsCopy.fixFirstPane = this.fixFirstPane;
		return settingsCopy;
	}
	@Override
	public String toString() {
		return "fixsize=" + fixSize + ";fixfirst=" + fixFirstPane + ";horizontal=" + horizontal;
	}
}