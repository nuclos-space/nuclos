//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.nuclos.api.report.OutputFormat;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.ReportOutputVO.Format;

/**
 * {@link OutputFormat} transport object
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class OutputFormatTO implements OutputFormat, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7945365677039764740L;
	private final UID idOutputFormat;
	private PrintPropertiesTO properties;
	private final boolean isMandatory;
	private String description;
	private final Map<String, Object> datasourceParams;
	private final ReportOutputVO.Destination destination;
	private final String outputPath;
	private final Format format;
	private boolean attachDocument;

	public OutputFormatTO(final UID idOutputFormat, final boolean isMandatory, final ReportOutputVO.Destination destination, final String outputPath, final Format format) {
		this.idOutputFormat = idOutputFormat;
		this.isMandatory = isMandatory;
		this.datasourceParams = new HashMap<String, Object>();
		this.destination = destination;
		this.outputPath = outputPath;
		this.format = format;
	}
	
	public OutputFormatTO clone() {
		OutputFormatTO clone = new OutputFormatTO(idOutputFormat, isMandatory, destination, outputPath, format);
		if (properties != null) {
			clone.setProperties(properties.clone());
		}
		for (Entry<String, Object> paramEntry : datasourceParams.entrySet()) {
			clone.getDatasourceParams().put(paramEntry.getKey(), paramEntry.getValue());
		}
		clone.attachDocument = attachDocument;
		return clone;
	}
	
	@Override
	public UID getId() {
		return idOutputFormat;
	}

	@Override
	public PrintPropertiesTO getProperties() {
		return this.properties;
	}
	
	public void setProperties(final PrintPropertiesTO properties) {
		this.properties = properties;
	}
	
	public boolean isMandatory() {
		return this.isMandatory;
	}
	
	public ReportOutputVO.Destination getDestination() {
		return destination;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getOutputPath() {
		return this.outputPath;
	}
	
	public Format getFormat() {
		return format;
	}

	public Map<String, Object> getDatasourceParams() {
		return datasourceParams;
	}
	
	public boolean isAttachDocument() {
		return attachDocument;
	}
	
	public void setAttachDocument(boolean attachDocument) {
		this.attachDocument = attachDocument;
	}
	
	@Override
	public String toString() {
		return "OutputFormatTO [idOutputFormat=" + idOutputFormat
				+ ", description=" + description + "]";
	}

}
