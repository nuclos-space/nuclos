//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.database.query.definition.Schema;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.dblayer.ResultSetMetaDataTransformer;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoteException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

@RolesAllowed("Login")
public interface DatasourceFacadeRemote {

	/**
	 * get all datasources
	 *
	 * @return set of datasources
	 * @throws CommonPermissionException
	 */
	Collection<DatasourceVO> getDatasources() throws CommonPermissionException;

	/**
	 * get all datasources
	 *
	 * @return set of datasources
	 */
	Collection<DatasourceVO> getDatasourcesForCurrentUser();

	/**
	 * get datasource value object
	 *
	 * @param dataSourceUID datasource UID
	 *            primary key of datasource
	 * @return datasource value object
	 */
	@RolesAllowed("Login")
	DatasourceVO get(final UID dataSourceUID) throws CommonFinderException, CommonPermissionException;


	/**
	 * create new datasource
	 *
	 * @param datasourcevo
	 *            value object
	 * @return new datasource
	 * @throws CommonFinderException 
	 */
	DatasourceVO create(final DatasourceVO datasourcevo, final IDependentDataMap dependants, final List<UID> lstUsedDatasourceUIDs) throws CommonCreateException, CommonValidationException, NuclosBusinessRuleException, CommonPermissionException, CommonFinderException;

	/**
	 * modify an existing datasource without usages
	 *
	 * @param datasourcevo
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 * @throws CommonStaleVersionException
	 * @throws CommonValidationException
	 */
	void modify(DatasourceVO datasourcevo) throws CommonFinderException, CommonPermissionException, CommonStaleVersionException, CommonValidationException, NuclosBusinessRuleException;

	/**
	 * modify an existing datasource
	 *
	 * @param datasourcevo
	 *            value object
	 * @return modified datasource
	 */
	DatasourceVO modify(final DatasourceVO datasourcevo, final IDependentDataMap dependants, final List<UID> lstUsedDatasourceUIDs) throws CommonFinderException, CommonPermissionException, CommonStaleVersionException, CommonValidationException,
			NuclosBusinessRuleException, CommonRemoteException;

	/**
	 * get a list of DatasourceCVO which uses the datasource with the given id
	 *
	 * @param dataSourceUID datasource UID
	 * @return
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	List<DatasourceVO> getUsagesForDatasource(final UID dataSourceUID) throws CommonFinderException, CommonPermissionException;

	/**
	 * get a list of DatasourceVO which uses the datasource
	 *
	 * @param datasourceVO
	 *            could also be an instance of <code>DynamicEntityVO</code> or
	 *            <code>ValuelistProviderVO</code>
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	List<DatasourceVO> getUsagesForDatasource(final DatasourceVO datasourceVO) throws CommonFinderException, CommonPermissionException;

	/**
	 * get a list of DatasourceCVO which are used by the datasource with the
	 * given UID
	 *
	 * @param dataSourceUID datasource UID
	 * @return
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 */
	List<DatasourceVO> getUsingByForDatasource(final UID dataSourceUID) throws CommonFinderException, CommonPermissionException;

	/**
	 * delete an existing datasource
	 *
	 * @param datasourcevo
	 *            value object
	 */
	void remove(DatasourceVO datasourcevo) 
			throws CommonFinderException, CommonRemoveException, CommonPermissionException, 
			CommonStaleVersionException, NuclosBusinessRuleException;

	/**
	 * Retrieve the parameters a datasource accepts.
	 *
	 * @param sDatasourceXML
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */
	@RolesAllowed("Login")
	List<DatasourceParameterVO> getParametersFromXML(final String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException;

	/**
	 * Retrieve the parameters a datasource accepts.
	 *
	 * @param dataSourceUID datasource UID
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */
	@RolesAllowed("Login")
	List<DatasourceParameterVO> getParameters(final UID dataSourceUID) throws NuclosFatalException, NuclosDatasourceException;

	/**
	 * validate the given DatasourceXML
	 *
	 */
	@RolesAllowed("Login")
	void validateSqlFromXML(DatasourceVO datasourcevo) throws CommonValidationException, NuclosDatasourceException;

	/**
	 * validate the given SQL
	 */
	@RolesAllowed("Login")
	public <T> T validateSql(String sql, ResultSetMetaDataTransformer<T> transformer) throws CommonValidationException, NuclosDatasourceException;

	/**
	 * get sql string for datasource definition
	 *
	 * @param dataSourceUID datasource UID
	 *            UID of datasource
	 * @return string containing sql
	 */
	String createSQL(final UID dataSourceUID, final Map<String, Object> mpParams) throws NuclosDatasourceException;

	/**
	 * get sql string for datasource definition without parameter definition
	 */
	@RolesAllowed("Login")
	String createSQL(final DatasourceVO datasourcevo) throws NuclosDatasourceException;

	/**
	 * get sql string for datasource definition
	 */
	@RolesAllowed("Login")
	String createSQLOriginalParameter(final DatasourceVO datasourcevo) throws NuclosDatasourceException;

	/**
	 * invalidate datasource cache
	 */
	@RolesAllowed("Login")
	void invalidateCache();

	/**
	 * get all DynamicEntities
	 *
	 * @return set of DynamicEntityVO
	 */
	@RolesAllowed("Login")
	Collection<DynamicEntityVO> getDynamicEntities();

	/**
	 * get dynamic entity value object
	 *
	 * @param dynamicEntityUID dynamicentity UID
	 *            UID of dynamic entity
	 * @return DynamicEntityVO
	 * @throws CommonFinderException 
	 */
	@RolesAllowed("Login")
	DynamicEntityVO getDynamicEntity(final UID dynamicEntityUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * get all charts
	 * 
	 * @return set of ChartVO
	 */
	@RolesAllowed("Login")
	Collection<ChartVO> getCharts();

	/**
	 * get chart value object
	 * 
	 * @param chartUID chart UID
	 *            UID of chart
	 * @return ChartVO
	 * @throws CommonFinderException 
	 */
	@RolesAllowed("Login")
	ChartVO getChart(final UID chartUID) throws CommonPermissionException, CommonFinderException;

	
	/**
	 * get all ValuelistProvider
	 *
	 * @return set of ValuelistProviderVO
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	Collection<ValuelistProviderVO> getValuelistProvider() throws CommonPermissionException;

	/**
	 * get a datasource result by datasource id
	 *
	 * @param dataSourceUID
	 * @param mpParams
	 * @param iMaxRowCount
	 */
	@RolesAllowed("Login")
	ResultVO executeQuery(final UID dataSourceUID, final Map<String, Object> mpParams, final Integer iMaxRowCount, final UID languageUID) throws NuclosDatasourceException, CommonFinderException;

	/**
	 * gets a datasource result by datasource xml
	 */
	@RolesAllowed("Login")
	ResultVO executeQueryForDataSource(DatasourceVO datasourcevo, Map<String, Object> mpParams, UID entity, Integer iMaxRowCount, final UID languageUID) throws CommonBusinessException, NuclosDatasourceException;

	Schema getSchemaTables(UID dsEntity) throws CommonPermissionException;

	Table getSchemaColumns(Table table, UID dsEntity) throws CommonPermissionException;

	/**
	 * get valuelist provider value object
	 *
	 * @param uidValueListProvider
	 *            UID of valuelist provider
	 * @return valuelist provider value object
	 */
	@RolesAllowed("Login")
	ValuelistProviderVO getValuelistProvider(final UID uidValueListProvider) throws CommonFinderException, CommonPermissionException;


	/**
	 * get all RecordGrant
	 *
	 * @return set of RecordGrantVO
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	Collection<RecordGrantVO> getRecordGrant() throws CommonPermissionException;

	/**
	 * get RecordGrant value object
	 *
	 * @param recordGrantUID record grant UID
	 *            primary key of RecordGrant
	 * @return RecordGrantVO
	 */
	@RolesAllowed("Login")
	RecordGrantVO getRecordGrant(final UID recordGrantUID) throws CommonPermissionException;


	@RolesAllowed("Login")
	Collection<DynamicTasklistVO> getDynamicTasklists() throws CommonPermissionException;

	@RolesAllowed("Login")
	DynamicTasklistVO getDynamicTasklist(final UID dynamicTaskListUID) throws CommonPermissionException;

	@RolesAllowed("Login")
	Set<String> getDynamicTasklistAttributes(final UID dynamicTaskListUID) throws CommonPermissionException, NuclosDatasourceException;

	@RolesAllowed("Login")
	ResultVO getDynamicTasklistData(final UID dynamicTaskListUID) throws CommonPermissionException, NuclosDatasourceException;

	@RolesAllowed("Login")
	CollectableField getDefaultValue(final UID dataSourceUID, final String valuefield, String idfield, String defaultfield, Map<String, Object> params, UID baseEntityUID, UID mandatorUID, UID languageUID) throws CommonBusinessException;

	@RolesAllowed("Login")
	public List<String> getColumns(String sql);
	
	@RolesAllowed("Login")
	List<String> getColumnsFromVLP(UID valuelistProviderUid, Class clazz) throws CommonBusinessException;

	@RolesAllowed("Login")
	List<CollectableValueIdField> executeQueryForVLP(VLPQuery query) throws CommonBusinessException;

	@RolesAllowed("Login")
	DatasourceVO getDatasourceByName(String sDataSource) throws CommonBusinessException;
	
	@RolesAllowed("Login")
	Set<UID> getAccessibleMandators(UID mandatorUID);
	@RolesAllowed("Login")
	boolean isMandator(UID baseEntityUID);
	@RolesAllowed("Login")
	String getBaseTable(UID baseEntityUID);

	@RolesAllowed("Login")
	CalcAttributeVO getCalcAttribute(UID calcAttributeUid) throws CommonFinderException, CommonPermissionException;

	@RolesAllowed("Login")
	Collection<CalcAttributeVO> getCalcAttributes() throws CommonPermissionException;

	@RolesAllowed("Login")
	List<DatasourceParameterVO> getCalcAttributeParameters(UID calcAttributeUID)
			throws NuclosFatalException, NuclosDatasourceException;
	
	UID getReportLanguageToUse(UID report, UID language, Long pk) throws NuclosReportException;
	
	Object getBooleanRepresentationInSQL(Boolean b);
}
