//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value object representing a subreport template.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:thomas.schiffmann@novabit.de">thomas.schiffmann</a>
 * @version 00.01.000
 */
public class SubreportVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5338263425735084306L;
	private UID reportOutputUID;
	private String parameter;
	private String sourcefileName;
	private ByteArrayCarrier sourcefileContent;
	private ByteArrayCarrier reportCLS;

	/**
	 * constructor used by client only
	 */
	public SubreportVO(final NuclosValueObject<UID> nvo, final UID reportOutputUID, final String parameter, 
			final String sourcefileName, final ByteArrayCarrier sourcefileContent, final ByteArrayCarrier reportCLS) {
		
		super(nvo);

		this.reportOutputUID = reportOutputUID;
		this.parameter = parameter;
		this.sourcefileName = sourcefileName;
		this.sourcefileContent = sourcefileContent;
		this.reportCLS = reportCLS;
	}

	/**
	 * creates a report vo from a suitable masterdata cvo.
	 * @param mdvo
	 */
	public SubreportVO(MasterDataVO<UID> mdvo) {
		this(mdvo.getNuclosValueObject(),
				mdvo.getFieldUid(E.SUBREPORT.reportoutput),
				mdvo.getFieldValue(E.SUBREPORT.parametername),
				mdvo.getFieldValue(E.SUBREPORT.sourcefilename),
				mdvo.getFieldValue(E.SUBREPORT.sourcefileContent),
				mdvo.getFieldValue(E.SUBREPORT.reportCLS));
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getSourcefileName() {
		return sourcefileName;
	}

	public void setSourcefileName(String sourcefileName) {
		this.sourcefileName = sourcefileName;
	}

	public ByteArrayCarrier getSourcefileContent() {
		return sourcefileContent;
	}

	public void setSourcefileContent(ByteArrayCarrier sourcefileContent) {
		this.sourcefileContent = sourcefileContent;
	}

	public ByteArrayCarrier getReportCLS() {
		return reportCLS;
	}

	public void setReportCLS(ByteArrayCarrier reportCLS) {
		this.reportCLS = reportCLS;
	}

	public UID getReportOutputUID() {
		return reportOutputUID;
	}

	@Override
	public void validate() throws CommonValidationException {
		if (StringUtils.isNullOrEmpty(this.getParameter())) {
			throw new CommonValidationException("report.error.validation.value");
		}
		if (StringUtils.isNullOrEmpty(this.getSourcefileName())) {
			throw new CommonValidationException("report.error.validation.description");
		}
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("id=").append(getId());
		result.append(",srcFile=").append(getSourcefileName());
		result.append(",param=").append(getParameter());
		result.append("]");
		return result.toString();
	}

}	// class ReportVO
