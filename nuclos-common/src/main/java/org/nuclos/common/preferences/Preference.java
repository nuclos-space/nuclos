//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObject;

import org.nuclos.common.JsonUtils;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.PreferenceVO;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * (Client-) Preference class for a clients viewpoint.
 */
public class Preference implements Serializable {

	public static final String APP_NUCLOS = PreferenceVO.APP_NUCLOS;

	/**
	 * Main attributes for insert or update
	 */
	public static class WritablePreference implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4105700961217621940L;

		private UID uid;

		private String app;

		private String type;

		private UID nuclet;

		private UID entity;

		private UID layout;

		private String name;

		private Boolean menuRelevant;

		private Boolean selected;

		private JsonObject json;

		/*

			  MORE ATTRIBUTES ???
			DO NOT FORGET TO EXTEND

			>>>	  writeObject   <<<

			          AND

			>>>   readObject    <<<

	 	*/

		/**
		 * Constructor for new preferences
		 */
		public WritablePreference() {
			this.uid = new UID();
		}

		public WritablePreference(UID uid) {
			this.uid = uid;
		}

		private WritablePreference(final Preference pref) {
			this.uid = pref.getUID();
			this.app = pref.getApp();
			this.type = pref.getType();
			this.nuclet = pref.getNuclet();
			this.entity = pref.getEntity();
			this.layout = pref.getLayout();
			this.name = pref.getName();
			this.menuRelevant = pref.isMenuRelevant();
			this.json = pref.getJson();
			this.selected = pref.isSelected();
		}

		public UID getUID() {
			return uid;
		}

		public void setUID(final UID uid) {
			this.uid = uid;
		}

		public String getApp() {
			return app;
		}

		public void setApp(final String app) {
			this.app = app;
		}

		public String getType() {
			return type;
		}

		public void setType(final String type) {
			this.type = type;
		}

		public UID getNuclet() {
			return nuclet;
		}

		public void setNuclet(final UID nuclet) {
			this.nuclet = nuclet;
		}

		public UID getEntity() {
			return entity;
		}

		public void setEntity(final UID entity) {
			this.entity = entity;
		}

		public UID getLayout() {
			return layout;
		}

		public void setLayout(final UID layout) {
			this.layout = layout;
		}

		public String getName() {
			return name;
		}

		public void setName(final String name) {
			this.name = name;
		}

		public Boolean getMenuRelevant() {
			return menuRelevant;
		}

		public void setMenuRelevant(final Boolean menuRelevant) {
			this.menuRelevant = menuRelevant;
		}

		public Boolean getSelected() {
			return selected;
		}

		public void setSelected(final Boolean selected) {
			this.selected = selected;
		}

		public JsonObject getJson() {
			return json;
		}

		public void setJson(final JsonObject json) {
			this.json = json;
		}

		private void writeObject(ObjectOutputStream stream) throws IOException {
			writeUID(stream, uid);
			stream.writeObject(app);
			stream.writeObject(type);
			writeUID(stream, nuclet);
			writeUID(stream, entity);
			writeUID(stream, layout);
			stream.writeObject(name);
			stream.writeObject(menuRelevant);
			stream.writeBoolean(selected);
			stream.writeObject(json==null?null:JsonUtils.objectToString(json));
		}

		private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
			uid = readUID(stream);
			app = (String) stream.readObject();
			type = (String) stream.readObject();
			nuclet = readUID(stream);
			entity = readUID(stream);
			layout = readUID(stream);
			name = (String) stream.readObject();
			menuRelevant = (Boolean) stream.readObject();
			selected = stream.readBoolean();
			String sJson = (String) stream.readObject();
			json = sJson==null?null:JsonUtils.stringToObject(sJson);
		}

		private void writeUID(ObjectOutputStream stream, UID uid) throws IOException {
			stream.writeObject(uid==null?null:uid.getString());
		}

		private UID readUID(ObjectInputStream stream) throws IOException, ClassNotFoundException {
			String s = (String) stream.readObject();
			return s==null?null:new UID(s);
		}

		public void transferWritableAttributes(PreferenceVO target) {
			if (target == null) {
				return;
			}
			WritablePreference source = this;
			target.setApp(source.getApp());
			target.setType(source.getType());
			target.setEntity(source.getEntity());
			target.setLayout(source.getLayout());
			target.setName(source.getName());
			target.setMenuRelevant(source.getMenuRelevant());
			target.setJson(source.getJson());
		}

	}

	/**
	 *
	 */
	private static final long serialVersionUID = -4545290934843517894L;

	private final PreferenceVO vo;

	public Preference(PreferenceVO vo) {
		this.vo = vo;
	}

	/**
	 *
	 * @return The UID for a client viewpoint. For a shared preference it is always the shared UID. The customization UID is hidden.
	 */
	public UID getUID() {
		return RigidUtils.defaultIfNull(vo.getSharedPreference(), vo.getPrimaryKey());
	}

	public String getApp() {
		return vo.getApp();
	}

	public UID getNuclet() {
		return vo.getNuclet();
	}

	public UID getEntity() {
		return vo.getEntity();
	}

	public UID getLayout() {
		return vo.getLayout();
	}

	public String getType() {
		return vo.getType();
	}

	public boolean isShared() {
		return vo.getSharedPreference() != null || vo.getUser() == null;
	}

	public boolean isCustomized() {
		return vo.getSharedPreference() != null;
	}

	public String getName() {
		return vo.getName();
	}

	public JsonObject getJson() {
		return vo.getJson();
	}

	public Boolean isMenuRelevant() {
		return vo.isMenuRelevant();
	}

	public boolean isSelected() {
		return vo.isSelected();
	}

	public UID getCustomized() {
		if (isCustomized()) {
			return vo.getPrimaryKey();
		}
		return null;
	}

	public UID getShared() {
		if (isShared()) {
			return vo.getSharedPreference();
		}
		return null;
	}

	public WritablePreference createWriteableObject() {
		return new WritablePreference(this);
	}

	public boolean equalContents(WritablePreference other) {
		if (other == null) {
			return false;
		}
		return equalContents(other.getType(), other.getEntity(), other.getLayout(), other.getName(), other.getMenuRelevant(), other.getJson());
	}

	public boolean equalContents(Preference other) {
		if (other == null) {
			return false;
		}
		return equalContents(other.getType(), other.getEntity(), other.getLayout(), other.getName(), other.isMenuRelevant(), other.getJson());
	}

	private boolean equalContents(String type, UID entity, UID layout, String name, Boolean menuRelevant, JsonObject json) {
		boolean result = RigidUtils.equal(this.getType(), type) &&
				RigidUtils.equal(this.getEntity(), entity) &&
				RigidUtils.equal(this.getLayout(), layout) &&
				RigidUtils.equal(this.getName(), name) &&
				RigidUtils.equal(this.isMenuRelevant(), menuRelevant);
		if (result) {
			if ((this.getJson() != null && json == null) ||
				(this.getJson() == null && json != null)) {
				result = false;
			}
			if (this.getJson() != null && json != null) {
				JsonParser jsonParser = new JsonParser();
				JsonElement thisJsonElement = jsonParser.parse(JsonUtils.objectToString(this.getJson()));
				JsonElement thatJsonElement = jsonParser.parse(JsonUtils.objectToString(json));
				result = thisJsonElement.equals(thatJsonElement);
			}
		}
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("Preference[");
		result.append("uid=").append(getUID());
		result.append(",app=").append(getApp());
		result.append(",type=").append(getType());
		result.append(",entity=").append(getEntity());
		result.append(",layout=").append(getLayout());
		result.append(",name=").append(getName());
		result.append(",nuclet=").append(getNuclet());
		result.append(",customized=").append(isCustomized());
		result.append(",shared=").append(isShared());
		result.append(",menuRelevant=").append(isMenuRelevant());
		result.append(",selected=").append(isSelected());
		return result.toString();
	}

	public static List<Preference> transformVOs(List<PreferenceVO> volist) {
		List<Preference> result = new ArrayList<>(volist.size());
		for (PreferenceVO vo : volist) {
			result.add(new Preference(vo));
		}
		return result;
	}

}
