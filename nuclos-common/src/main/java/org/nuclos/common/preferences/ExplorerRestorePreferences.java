package org.nuclos.common.preferences;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.searchfilter.ISeachFilterWithId;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNode;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNodeParameters;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNodeParameters;
import org.nuclos.server.navigation.treenode.ITreeNodeDefaultConstructor;
import org.nuclos.server.navigation.treenode.ITreeNodeWithGenericObjectTreeNodeParameters;
import org.nuclos.server.navigation.treenode.MasterDataSearchResultNodeParameters;
import org.nuclos.server.navigation.treenode.MasterDataSearchResultTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.navigation.treenode.nuclet.NucletTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.NucletTreeNodeParameters;

import com.thoughtworks.xstream.XStream;

public class ExplorerRestorePreferences implements IExplorerTreeNodeState, Serializable {
	private static final long serialVersionUID = 6637996725938917463L;

	public static final int GENERIC = -1;
	public static final int PERSONAL = 1;
	public static final int TIMELIMIT = 2;
	public static final int DYNAMIC = 3;

	private final UID uid;
	/**
	 * use GENERIC, PERSONAL or TIMELIMIT
	 * TODO they are only here because of compatibility to old preferences
	 */
	private Integer type;
	private Integer refreshInterval;
	/**
	 * only for type GENERIC
	 */
	private TreeNode treenodeRoot;

	private UID searchFilterId;	
	private Object id;
	private String treeNodeClass;
	private MasterDataSearchResultNodeParameters masterDataSearchResultNodeParameters;
	private GenericObjectTreeNodeParameters genericObjectTreeNodeParameters;
	private NucletTreeNodeParameters nucletTreeNodeParameters;
	private DefaultMasterDataTreeNodeParameters<?> defaultMasterDataTreeNodeParameters;
	
	private List<String> lstExpandedPaths;
	private int[] selectedRows;
	
	public ExplorerRestorePreferences(UID uid) {
		if (uid == null) {
			this.uid = new UID();
		} else {
			this.uid = uid;
		}
	}
	
	public UID getUid() {
		return uid;
	}

	public static ExplorerRestorePreferences fromXML(String xml) {
		xml = xml.replaceAll("org\\.nuclos\\.client\\.explorer\\.ExplorerController_-RestorePreferences",
				"org\\.nuclos\\.common\\.preferences\\.ExplorerRestorePreferences");

		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			ExplorerRestorePreferences erp = (ExplorerRestorePreferences) xstream.fromXML(xml);
			migrateTreeNodeRoot(erp, erp.treenodeRoot);
			return erp;
		}
	}

	private static void migrateTreeNodeRoot(ExplorerRestorePreferences erp, TreeNode treeNodeRoot) {
		if (treeNodeRoot instanceof ISeachFilterWithId) {
			erp.searchFilterId = ((ISeachFilterWithId)treeNodeRoot).getSearchFilterId();
			erp.treenodeRoot = null;
			
		} else if (treeNodeRoot instanceof ITreeNodeDefaultConstructor) {
			erp.treeNodeClass = treeNodeRoot.getClass().getName();
			erp.treenodeRoot = null;
			
		} else if (treeNodeRoot instanceof ITreeNodeWithGenericObjectTreeNodeParameters) {
			erp.treeNodeClass = treeNodeRoot.getClass().getName();
			erp.genericObjectTreeNodeParameters = ((ITreeNodeWithGenericObjectTreeNodeParameters)treeNodeRoot).getGenericObjectTreeNodeParameters();
			erp.treenodeRoot = null;
			
		} else if (treeNodeRoot instanceof MasterDataSearchResultTreeNode) {
			erp.masterDataSearchResultNodeParameters = ((MasterDataSearchResultTreeNode<?>)treeNodeRoot).getMasterDataSearchResultNodeParameters();
			erp.treenodeRoot = null;
			
		} else if (treeNodeRoot instanceof NucletTreeNode) {
			erp.nucletTreeNodeParameters = ((NucletTreeNode)treeNodeRoot).getNucletTreeNodeParameters();
			erp.treenodeRoot = null;
			
		} else if (treeNodeRoot instanceof DefaultMasterDataTreeNode) {
			erp.defaultMasterDataTreeNodeParameters = ((DefaultMasterDataTreeNode<?>)treeNodeRoot).getParameters();
			erp.treenodeRoot = null;
			
		} else if (treeNodeRoot != null && treeNodeRoot.getId() != null) {
			erp.id = treeNodeRoot.getId();
			erp.treenodeRoot = treeNodeRoot;
			//TODO: Implement avoid saving the whole tree
		}  else {
			erp.treenodeRoot = treeNodeRoot;
		}
	}

	public String toXML(TreeNode treeNodeRoot) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			migrateTreeNodeRoot(this, treeNodeRoot);
			return xstream.toXML(this);
		}
	}

	@Override
	public List<String> getLstExpandedPaths() {
		return Collections.unmodifiableList(lstExpandedPaths);
	}
	
	@Override
	public int[] getSelectedRows() {
		return selectedRows;
	}

	public void setExplorerTreeNodeState(IExplorerTreeNodeState state) {
		this.lstExpandedPaths = state.getLstExpandedPaths();
		this.selectedRows = state.getSelectedRows();		
	}

	public UID getSearchFilterId() {
		return searchFilterId;
	}
	
	public Object getId() {
		return id;
	}
	
	public String getTreeNodeClassName() {
		return treeNodeClass;
	}

	public TreeNode getTreeNodeRoot() {
		return treenodeRoot;
	}

	public MasterDataSearchResultNodeParameters getMasterDataSearchResultNodeParameters() {
		return masterDataSearchResultNodeParameters;
	}

	public GenericObjectTreeNodeParameters getGenericObjectTreeNodeParameters() {
		return genericObjectTreeNodeParameters;
	}

	public NucletTreeNodeParameters getNucletTreeNodeParameters() {
		return nucletTreeNodeParameters;
	}

	public DefaultMasterDataTreeNodeParameters<?> getDefaultMasterDataTreeNodeParameters() {
		return defaultMasterDataTreeNodeParameters;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ExplorerRestorePreferences) {
			ExplorerRestorePreferences that = (ExplorerRestorePreferences) obj;
			return LangUtils.equal(id, that.id) && LangUtils.equal(searchFilterId, that.searchFilterId)
					&& LangUtils.equal(treeNodeClass, that.treeNodeClass) 
					&& LangUtils.equal(masterDataSearchResultNodeParameters, that.masterDataSearchResultNodeParameters)
					&& LangUtils.equal(genericObjectTreeNodeParameters, that.genericObjectTreeNodeParameters)
					&& LangUtils.equal(nucletTreeNodeParameters, that.nucletTreeNodeParameters)
					&& LangUtils.equal(defaultMasterDataTreeNodeParameters, that.defaultMasterDataTreeNodeParameters)
					&& LangUtils.equal(treenodeRoot, that.treenodeRoot);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(id, searchFilterId, treeNodeClass, masterDataSearchResultNodeParameters, 
				genericObjectTreeNodeParameters, nucletTreeNodeParameters, defaultMasterDataTreeNodeParameters, treenodeRoot);
	}
	
}
