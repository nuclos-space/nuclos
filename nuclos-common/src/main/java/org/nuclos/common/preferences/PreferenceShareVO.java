//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;

public class PreferenceShareVO implements Serializable, Comparable<PreferenceShareVO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 431373287448362929L;

	private final UID pref;

	private final UID role;

	private final String roleName;

	private final boolean shared;

	public PreferenceShareVO(final UID pref, final UID role, final String roleName, final boolean shared) {
		if (pref == null || role == null) {
			throw new IllegalArgumentException("pref and role must not be null");
		}
		this.pref = pref;
		this.role = role;
		this.roleName = roleName;
		this.shared = shared;
	}

	public UID getPref() {
		return pref;
	}

	public UID getRole() {
		return role;
	}

	public String getRoleName() {
		return roleName;
	}

	public boolean isShared() {
		return shared;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		final PreferenceShareVO that = (PreferenceShareVO) o;

		if (!pref.equals(that.pref)) return false;
		return role.equals(that.role);
	}

	@Override
	public int hashCode() {
		int result = pref.hashCode();
		result = 31 * result + role.hashCode();
		return result;
	}

	@Override
	public int compareTo(final PreferenceShareVO other) {
		return StringUtils.compareIgnoreCase(this.getRoleName(), other.getRoleName());
	}
}
