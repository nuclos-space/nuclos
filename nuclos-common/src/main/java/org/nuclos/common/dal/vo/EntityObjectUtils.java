package org.nuclos.common.dal.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common2.LangUtils;

public class EntityObjectUtils {
	
	private EntityObjectUtils() {
		// Never invoked.
	}

	public static void setOrUpdateValue(EntityObjectVO<?> eo, UID valueFieldUid, Object value) {
		final boolean process = !eo.isFlagNew() && !eo.isFlagRemoved();
		
		final Object old = eo.getFieldValue(valueFieldUid);
		if (!LangUtils.equal(old, value)) {
			eo.setFieldValue(valueFieldUid, value);
			if (process) {
				eo.flagUpdate();
			}
		}
	}
	
	public static void setOrUpdateUid(EntityObjectVO<?> eo, UID uidFieldUid, UID id) {
		final boolean process = !eo.isFlagNew() && !eo.isFlagRemoved();
		
		final Object old = eo.getFieldValue(uidFieldUid);
		if (!LangUtils.equal(old, id)) {
			eo.setFieldUid(uidFieldUid, id);
			if (process) {
				eo.flagUpdate();
			}
		}
	}
	
	public static void setOrUpdateId(EntityObjectVO<?> eo, UID idFieldUid, Long id) {
		final boolean process = !eo.isFlagNew() && !eo.isFlagRemoved();
		
		final Object old = eo.getFieldValue(idFieldUid);
		if (!LangUtils.equal(old, id)) {
			eo.setFieldId(idFieldUid, id);
			if (process) {
				eo.flagUpdate();
			}
		}
	}
	
	/**
	 * Add data at the end of dependency.
	 */
	public static <PK> void addData(IDependentDataMap target, IDependentKey dependentKey, EntityObjectVO<PK> eo) {
		final PK newPk = eo.getPrimaryKey();
		final Collection<EntityObjectVO<PK>> col = target.getDataPk(dependentKey);
		for (EntityObjectVO<PK> c: col) {
			if (newPk != null && newPk.equals(c.getPrimaryKey())) {
				// already in 
				return;
			}
			if (eo == c) {
				// already in
				return;
			}
		}
		target.addData(dependentKey, eo);
	}
	
	/**
	 * Add data at given index of dependency.
	 */
	public static <PK> void addData(int index, IDependentDataMap target, IDependentKey dependentKey, EntityObjectVO<PK> eo) {
		final PK newPk = eo.getPrimaryKey();
		final Collection<EntityObjectVO<PK>> col = target.getDataPk(dependentKey);
		for (EntityObjectVO<PK> c: col) {
			if (newPk != null && newPk.equals(c.getPrimaryKey())) {
				// already in 
				return;
			}
			if (eo == c) {
				// already in
				return;
			}
		}
		// defensive copy
		final List<EntityObjectVO<PK>> list = new ArrayList<EntityObjectVO<PK>>(target.<PK>getDataPk(dependentKey));
		list.add(index, eo);
		target.setData(dependentKey, list);
	}
	
	public static <PK> void removeData(IDependentDataMap target, IDependentKey dependentKey, EntityObjectVO<PK> toRemove) {
		final PK newPk = toRemove.getPrimaryKey();
		final Collection<EntityObjectVO<PK>> col = target.getDataPk(dependentKey);
		for (final Iterator<EntityObjectVO<PK>> it = col.iterator(); it.hasNext();) {
			final EntityObjectVO<PK> c = it.next();
			if (newPk != null && newPk.equals(c.getPrimaryKey())) {
				it.remove();
				return;
			}
			if (toRemove == c) {
				it.remove();
				return;
			}
		}
		// this could happen delete non-persisted stuff, i.e. throwing is wrong here (tp)
		// throw new IllegalStateException("Eo to remove not found: " + toRemove);
	}

	public static <PK> void mergeUnsetValues(EntityObjectVO<PK> target, EntityObjectVO<PK> merge) {
		// final Map<UID,Object> tValues = target.getFieldValues();
		final Map<UID,Object> mValues = merge.getFieldValues();
		for (UID mUid: mValues.keySet()) {
			final Object v = mValues.get(mUid);
			if (v != null) {
				target.setFieldValue(mUid, v);
			}
		}
	}
	
	public static <PK> void fixAfterSave(EntityObjectVO<PK> saved) {
		_fixAfterSave(saved);
		
		// ??? HACK but needed (tp)
		saved.setComplete(true);		
	}
	
	private static <PK> boolean _fixAfterSave(EntityObjectVO<PK> saved) {
		final boolean result = saved.isFlagRemoved();
		
		// increment version: not needed
		/*
		if (saved.isFlagUpdated() || saved.isFlagNew()) {
			saved.setVersion(saved.getVersion() + 1);
		}
		 */
		saved.reset();

		final IDependentDataMap dependencies = saved.getDependents();
		_fixAfterSave(dependencies);
		
		return result;
	}
	
	public static void fixAfterSave(IDependentDataMap dependencies) {
		_fixAfterSave(dependencies);
	}
	
	private static <PK> boolean _fixAfterSave(IDependentDataMap dependencies) {
		boolean allDeleted = true;
		
		// defensive copy is NEEDED here (tp)
		for (IDependentKey dependentKey: new HashSet<IDependentKey>(dependencies.getKeySet())) {
			boolean subEntityDeleted = true;
			final List<EntityObjectVO<PK>> toDelete = new ArrayList<EntityObjectVO<PK>>();
			final Collection<EntityObjectVO<PK>> col = dependencies.<PK>getDataPk(dependentKey);
			for (EntityObjectVO<PK> eo: col) {
				final boolean delete = _fixAfterSave(eo);
				if (delete) {
					toDelete.add(eo);
				}
				subEntityDeleted = subEntityDeleted && delete;
			}
			col.removeAll(toDelete);
			if (subEntityDeleted) {
				dependencies.removeKey(dependentKey);
			}
			allDeleted = allDeleted && subEntityDeleted;
		}
		
		return allDeleted;
	}
	
	public static <PK> EntityObjectVO<PK> flatClone(EntityObjectVO<PK> prototype, Set<UID> fieldDepsToNull, boolean cloneDeps) {
		if (prototype == null) {
			return null;
		}
		final EntityObjectVO<PK> clone = new EntityObjectVO<PK>(prototype.getDalEntity());
		clone.flagNew();
		
		// values
		for (UID k: prototype.getFieldValues().keySet()) {
			final Object value = prototype.getFieldValue(k);
			clone.setFieldValue(k, value);
		}
		// ids
		for (UID k: prototype.getFieldIds().keySet()) {
			if (!fieldDepsToNull.contains(k)) {
				final Long value = prototype.getFieldId(k);
				clone.setFieldId(k, value);
			}
		}
		// uids
		for (UID k: prototype.getFieldUids().keySet()) {
			if (!fieldDepsToNull.contains(k)) {
				final UID value = prototype.getFieldUid(k);
				clone.setFieldUid(k, value);
			}
		}
		
		if (cloneDeps) {
			final IDependentDataMap deps = prototype.getDependents();
			clone.setDependents(flatClone(deps, fieldDepsToNull), prototype.getApiCacheAlreadyLoadedDependents());
		}
		
		return clone;
	}
	
	public static <PK> IDependentDataMap flatClone(IDependentDataMap prototype, Set<UID> fieldDepsToNull) {
		if (prototype == null) {
			return null;
		}
		final IDependentDataMap clone = new DependentDataMap();
		for (IDependentKey k: prototype.getKeySet()) {
			final Collection<EntityObjectVO<PK>> col = prototype.getDataPk(k);
			for (EntityObjectVO<PK> eo: col) {
				final EntityObjectVO<PK> eoClone = flatClone(eo, fieldDepsToNull, true);
				clone.addData(k, eoClone);
			}
		}
		return clone;
	}
	
	public static <PK> void sort(final List<EntityObjectVO<PK>> list, final List<CollectableSorting> sorts) {
		
		Collections.sort(list, new Comparator<EntityObjectVO<PK>>() {
			@Override
			public int compare(EntityObjectVO<PK> eo1, EntityObjectVO<PK> eo2) {
				for (CollectableSorting sort : sorts) {
					final int comp;
					if (sort.isAscending()) {
						comp = LangUtils.compare(eo1.getFieldValue(sort.getField()), eo2.getFieldValue(sort.getField()));
					} else {
						comp = LangUtils.compare(eo2.getFieldValue(sort.getField()), eo1.getFieldValue(sort.getField()));
					}
					if (comp != 0) {
						return comp;
					}
				}
				return 0;
			}
		});
	}
	
	
	public static String toString(IDependentDataMap deps, boolean recursive) {
		final StringBuilder result = new StringBuilder();
		result.append("deps[\n");
		for (IDependentKey dependentKey: deps.getKeySet()) {
			result.append("dependentKey ").append(dependentKey.toString()).append("\n");
			final Collection<EntityObjectVO<?>>  col = deps.getData(dependentKey);
			int j = 0;
			for (EntityObjectVO<?> eo: col) {
				result.append(j++).append("= ").append(eo.toString()).append("\n");
				if (recursive) {
					final IDependentDataMap subDeps = eo.getDependents();
					if (subDeps != null && !subDeps.isEmpty()) {
						result.append(toString(subDeps, recursive));
					}
				}
			}
			result.append("end of ").append(dependentKey.toString()).append("\n");
		}
		result.append("]\n");
		return result.toString();
	}
	
}
