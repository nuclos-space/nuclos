//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.json.JsonObject;

import org.nuclos.common.E;
import org.nuclos.common.JsonUtils;
import org.nuclos.common.UID;

public class PreferenceVO extends AbstractDalVOWithVersion<UID> {
	/**
	 *
	 */
	private static final long serialVersionUID = -2756345258542681926L;

	public PreferenceVO() {
		super(E.PREFERENCE.getUID());
	}

	/**
	 * Do not use it - internal only!
	 * Used by org.nuclos.server.dal.processor.AbstractDalProcessor (org.nuclos.server.dal.processor.jdbc.impl.PreferenceProcessor)
	 */
	public PreferenceVO(UID entityUID) {
		super(entityUID);
	}

	public static final String APP_NUCLOS = "nuclos";

	private String app = APP_NUCLOS;

	/**
	 * TODO: This should be an enum.
	 */
	private String type;

	private UID nuclet;

	private UID entity;

	private UID layout;

	private UID sharedPreference;

	private UID user;

	private String name;

	private Boolean menuRelevant;

	private boolean selected;

	private transient JsonObject json;

	/*

			  MORE ATTRIBUTES ???
			DO NOT FORGET TO EXTEND

			>>>	  writeObject   <<<

			          AND

			>>>   readObject    <<<

	 */

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		if (app == null) {
			app = APP_NUCLOS;
		}
		this.app = app;
	}

	public UID getUser() {
		return user;
	}

	public void setUser(UID user) {
		this.user = user;
	}

	public UID getNuclet() {
		return nuclet;
	}

	public void setNuclet(UID nuclet) {
		this.nuclet = nuclet;
	}

	public UID getEntity() {
		return entity;
	}

	public void setEntity(UID entity) {
		this.entity = entity;
	}

	public UID getLayout() {
		return layout;
	}

	public void setLayout(final UID layout) {
		this.layout = layout;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UID getSharedPreference() {
		return sharedPreference;
	}

	public void setSharedPreference(UID sharedPreference) {
		this.sharedPreference = sharedPreference;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JsonObject getJson() {
		return json;
	}

	public void setJson(JsonObject json) {
		this.json = json;
	}

	public Boolean isMenuRelevant() {
		return menuRelevant;
	}

	public void setMenuRelevant(Boolean menuRelevant) {
		this.menuRelevant = menuRelevant;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(final boolean selected) {
		this.selected = selected;
	}

	private void writeObject(ObjectOutputStream stream) throws IOException {
		stream.writeObject(app);
		stream.writeObject(type);
		writeUID(stream, nuclet);
		writeUID(stream, entity);
		writeUID(stream, layout);
		writeUID(stream, sharedPreference);
		writeUID(stream, user);
		stream.writeObject(name);
		stream.writeObject(menuRelevant);
		stream.writeBoolean(selected);
		stream.writeObject(json==null?null:JsonUtils.objectToString(json));
	}

	private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
		app = (String) stream.readObject();
		type = (String) stream.readObject();
		nuclet = readUID(stream);
		entity = readUID(stream);
		layout = readUID(stream);
		sharedPreference = readUID(stream);
		user = readUID(stream);
		name = (String) stream.readObject();
		menuRelevant = (Boolean) stream.readObject();
		selected = stream.readBoolean();
		String sJson = (String) stream.readObject();
		json = sJson==null?null:JsonUtils.stringToObject(sJson);
	}

	private void writeUID(ObjectOutputStream stream, UID uid) throws IOException {
		stream.writeObject(uid==null?null:uid.getString());
	}

	private UID readUID(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		String s = (String) stream.readObject();
		return s==null?null:new UID(s);
	}

}
