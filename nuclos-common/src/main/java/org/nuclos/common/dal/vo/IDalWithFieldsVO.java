//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.util.Map;

import org.nuclos.common.UID;

/**
 * Base interface for all <em>data</em> value objects (VOs) transfered between client and server. 
 * <p>
 * On the server side, these VOs get persisted into the DB. These VOs represent the normal entities 
 * of a Nuclos instance, with their (custom) fields filled.
 * </p>
 * @since Nuclos 3.1.01
 * @author Thomas Pasch 
 */
public interface IDalWithFieldsVO<T, PK> extends IDalVO<PK> {

	boolean hasFields();
	
	Map<UID, T> getFieldValues();
	
	Map<UID, Long> getFieldIds();
	
	Map<UID, UID> getFieldUids();
	
	Long getFieldId(UID field);
	
	UID getFieldUid(UID field);
	
	void setFieldId(UID field, Long id);
	
	void removeFieldId(UID field);
	
	void setFieldUid(UID field, UID uid);
	
	void removeFieldUid(UID field);

	<S> S getFieldValue(UID field, Class<S> cls);
	
	T getFieldValue(UID field);
	
	void setFieldValue(UID field, T obj);
	
	void removeFieldValue(UID field);

	boolean systemFieldsOnRequest();
	
	boolean isComplete();
	
	void setComplete(boolean b);
	
	boolean exist(UID field);
	
	public IDataLanguageMap getDataLanguageMap();
	
	public void setDataLanguageMap(IDataLanguageMap map);
}
