package org.nuclos.common.dal.vo;

import java.util.Map;

import org.nuclos.common.UID;

public class Delete<PK> implements IDalReadVO<PK>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4771678812807554964L;
	private PK pk;
	private UID entity;
	
	public Delete(PK pk, UID entity) {
		this.pk = pk;
		this.entity = entity;
	}
	
	public Delete(PK pk) {
		this(pk, null);
	}

	@Override
	public PK getPrimaryKey() {
		return pk;
	}

	@Override
	public UID getDalEntity() {
		return entity;
	}

	@Override
	public int getFlag() {
		return IDalVO.STATE_REMOVED;
	}

	@Override
	public String toString() {
		return "Entity=" + entity + " Pk=" + pk + " Removed";
	}

	@Override
	public Map<UID, Object> getFieldValues() {
		return null;
	}

	@Override
	public Map<UID, Long> getFieldIds() {
		return null;
	}

	@Override
	public Map<UID, UID> getFieldUids() {
		return null;
	}
}
