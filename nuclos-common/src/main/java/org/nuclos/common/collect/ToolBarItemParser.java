//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.nuclos.common.collect.ToolBarItem.Type;



public class ToolBarItemParser {
	
	private static final Logger LOG = Logger.getLogger(ToolBarItemParser.class);
	
	public static String toString(ToolBarItem item) {
		StringBuffer result = new StringBuffer();
		if (item != null) {
			result.append(item.getType());
			result.append(":");
			result.append(item.getKey());
			Map<String, String> properties = item.getProperties();
			if (!properties.isEmpty()) {
				result.append("(");
				Iterator<Entry<String, String>> itprops = properties.entrySet().iterator();
				while (itprops.hasNext()) {
					Entry<String, String> p = itprops.next();
					result.append(p.getKey());
					result.append("=");
					result.append(p.getValue());
					if (itprops.hasNext()) {
						result.append('|');
					}
				}
				result.append(")");
			}
			if (item instanceof ToolBarItemGroup) {
				ToolBarItemGroup group = (ToolBarItemGroup) item;
				result.append('{');
				Iterator<ToolBarItem> groupItems = group.getItems().iterator();
				while (groupItems.hasNext()) {
					result.append(toString(groupItems.next()));
					if (groupItems.hasNext()) {
						result.append(',');
					}
				}
				result.append('}');
			}
		}
		return result.toString();
	}
	
	public static Type parseType(String sItem) {
		if (sItem != null) {
			int i = sItem.indexOf(":");
			if (i > 0) {
				String sType = sItem.substring(0, i).trim();
				try {
					Type type = Type.valueOf(sType);
					return type;
				} catch (Exception ex) {
					LOG.warn(String.format("ToolBarItem [Type=%s] not parsable", sType), ex);
				}
			}
		}
		return null;
	}

	public static ToolBarItem parse(String sItem) {
		Type type = parseType(sItem);
		if (type == null) {
			return null;
		}
		
		try {
			String sContent = sItem.substring(sItem.indexOf(":")+1).trim();
			switch (type) {
			case GROUP:
				return parseGroup(sContent);
			default:
				return parseItem(type, sContent);
			}
			
		} catch (Exception ex) {
			LOG.warn(String.format("ToolBarItem [%s] not parsable", sItem), ex);
		}
		
		return null;
	}
	
	private static ToolBarItem parseItem(Type type, String sItem) {
		int propertiesStartIndex = sItem.indexOf("(");
		if (propertiesStartIndex == -1) {
			// no properties
			return new ToolBarItem(type, sItem);
		} else {
			String sContent = sItem.substring(0, propertiesStartIndex);
			ToolBarItem result = new ToolBarItem(type, sContent);
			String[] properties = sItem.substring(propertiesStartIndex+1, sItem.length()-1).split("\\|");
			for (String entry : properties) {
				String[] keyValue = entry.split("=");
				result.setProperty(keyValue[0], keyValue[1]);
			}
			return result;
		}
	}
	
	private static ToolBarItemGroup parseGroup(String sItem) {
		try {
			int j = sItem.indexOf("{");
			int k = sItem.lastIndexOf("}");
			String sKey = sItem.substring(0, j);
			String sCompleteContent = sItem.substring(j+1, k);
			
			List<String> sContentItems = new ArrayList<String>();
			
			StringTokenizer tokenizer = new StringTokenizer(sCompleteContent, "{},", true);
			String sElement = null;
			int innerGroup = 0;
			StringBuffer sInnerGroup = null;
			while (tokenizer.hasMoreTokens()) {
				String sToken = tokenizer.nextToken();
				if ("{".equals(sToken)) {
					if (innerGroup == 0) {
						sInnerGroup = new StringBuffer();
						sInnerGroup.append(sElement);
						sElement = null;
					}
					sInnerGroup.append(sToken);
					innerGroup++;
				} else if ("}".equals(sToken)) {
					sInnerGroup.append(sToken);
					innerGroup--;
					if (innerGroup == 0) {
						sContentItems.add(sInnerGroup.toString());
					}
				} else {
					if (innerGroup > 0) {
						sInnerGroup.append(sToken);
					} else {
						if (!",".equals(sToken)) {
							if (sElement != null) {
								sContentItems.add(sElement);
							}
							sElement = sToken;
						}
					}
				}
			}
			if (sElement != null) {
				sContentItems.add(sElement);
			}
			
			ToolBarItemGroup group = new ToolBarItemGroup(sKey);
			for (String sContentItem : sContentItems) {
				group.addItem(parse(sContentItem));
			}
			return group;
			
		} catch (Exception ex) {
			LOG.warn(String.format("ToolBarItemGroup [%s] not parsable", sItem), ex);
		}

		return null;
	}
	
}
