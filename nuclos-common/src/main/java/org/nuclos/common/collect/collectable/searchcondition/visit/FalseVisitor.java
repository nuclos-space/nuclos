package org.nuclos.common.collect.collectable.searchcondition.visit;

import org.apache.xpath.operations.Bool;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.GeneralJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.ReferencingCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;

/**
 * Created by Sebastian Debring on 9/23/2019.
 */
public abstract class FalseVisitor<Ex extends Exception> implements Visitor<Boolean, Ex> {

	@Override
	public Boolean visitTrueCondition(final TrueCondition truecond) throws Ex {
		return false;
	}

	@Override
	public Boolean visitAtomicCondition(final AtomicCollectableSearchCondition atomiccond) throws Ex {
		return false;
	}

	@Override
	public Boolean visitCompositeCondition(final CompositeCollectableSearchCondition compositecond) throws Ex {
		return false;
	}

	@Override
	public Boolean visitIdCondition(final CollectableIdCondition idcond) throws Ex {
		return false;
	}

	@Override
	public Boolean visitIdListCondition(final CollectableIdListCondition collectableIdListCondition) throws Ex {
		return false;
	}

	@Override
	public <T> Boolean visitInCondition(final CollectableInCondition<T> collectableInCondition) throws Ex {
		return false;
	}

	@Override
	public Boolean visitSubCondition(final CollectableSubCondition subcond) throws Ex {
		return true;
	}

	@Override
	public Boolean visitRefJoinCondition(final RefJoinCondition joincond) throws Ex {
		return false;
	}

	@Override
	public Boolean visitGeneralJoinCondition(final GeneralJoinCondition joincond) throws Ex {
		return false;
	}

	@Override
	public Boolean visitReferencingCondition(final ReferencingCollectableSearchCondition refcond) throws Ex {
		return false;
	}
}
