package org.nuclos.common.collect;


public enum ChartRangeType {
	STRING("String", "CATEGORY"), 
	DATE("Date", "DATE"), 
	NUMBER("Number", "NUMBER");
	
	private final String name;
	private final String type;
	ChartRangeType(String name, String type) {
		this.name = name;
		this.type = type;
	}
	
	public final String getName() {
		return name;
	}

	public final String getType() {
		return type;
	}
	
}
