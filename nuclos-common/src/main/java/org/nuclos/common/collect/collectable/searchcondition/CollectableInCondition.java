//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable.searchcondition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.visit.AtomicVisitor;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;
import org.nuclos.common2.LangUtils;

/**
 * A "[NOT] IN" condition as a <code>CollectableSearchCondition</code>.
 * This class is immutable.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * §todo try to get rid of one or two of the constructors
 *
 * @version 01.00.00
 */
public class CollectableInCondition<T> extends AtomicCollectableSearchCondition {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7855068115145191650L;

	public static final String NAME = "InCondition";

	private final List<T> inComparands;
	private final Map<Object, T> keyMap;

	public CollectableInCondition(CollectableEntityField clctef, Map<Object, T> keyMap) {
		this(clctef, ComparisonOperator.IN, null, keyMap);
	}

	/**
	 * §precondition sInComparand != null
	 * §postcondition this.getComparisonOperator() == ComparisonOperator.IN
	 */
	public CollectableInCondition(CollectableEntityField clctef, List<T> inComparands) {
		this(clctef, ComparisonOperator.IN, inComparands, null);

		assert this.getComparisonOperator() == ComparisonOperator.IN;
	}

	/**
	 * §precondition isValidOperator(compop)
	 * §precondition sInComparand != null
	 * §postcondition this.getComparisonOperator() == compop
	 */
	public CollectableInCondition(CollectableEntityField clctef, ComparisonOperator compop, List<T> inComparands, Map<Object, T> keyMap) {
		super(clctef, compop);
		if (!isValidOperator(compop)) {
			throw new IllegalArgumentException("Illegal operator: " + compop);
		}
		if (inComparands == null) {
			inComparands = new ArrayList<T>();
		}
		if (keyMap != null) {
			for (T value : keyMap.values()) inComparands.add(value);
		}
		this.inComparands = inComparands;
		this.keyMap = keyMap;

		assert this.getComparisonOperator() == compop;
	}

	/**
	 * @param compop
	 * @return compop == ComparisonOperator.IN || compop == ComparisonOperator.NOT_IN
	 */
	public static boolean isValidOperator(ComparisonOperator compop) {
		return compop == ComparisonOperator.IN || compop == ComparisonOperator.NOT_IN;
	}

	/**
	 * @return this.getComparisonOperator() == ComparisonOperator.IN
	 */
	public boolean isPositive() {
		return this.getComparisonOperator() == ComparisonOperator.IN;
	}

	public List<T> getInComparands() {
		return this.inComparands;
	}
	
	@Override
	public String getComparandAsString() {
		StringBuilder s = new StringBuilder("");
		boolean first = true;
		for (Object o : inComparands) {
			if (first) {
				first = false;
			} else {
				s.append(",");
			}
			if (o == null) {
				o = "null";
			} else if (!(o instanceof Number)) {
				o = "'" + o.toString().replaceAll("'","''") + "'";
			}
			s.append(o.toString());
		}
		return s.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CollectableInCondition)) {
			return false;
		}
		final CollectableInCondition<T> that = (CollectableInCondition<T>) o;

		return super.equals(that) && this.inComparands.equals(that.inComparands);
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ LangUtils.hashCode(this.inComparands);
	}

	@Override
	public <O, Ex extends Exception> O accept(AtomicVisitor<O, Ex> visitor) throws Ex {
		return visitor.visitInCondition(this);
	}
	
	@Override
	public <O, Ex extends Exception> O accept(Visitor<O, Ex> visitor) throws Ex {
		return visitor.visitInCondition(this);
	}

	public Map<Object, T> getKeyMap() {
		return keyMap != null ? Collections.unmodifiableMap(keyMap) : null;
	}
	
	@Override
	public String toString() {
		return representation(this);
	}
	
	public static String representation(CollectableInCondition<?> c) {
		StringBuilder sb = new StringBuilder(NAME);		
		appendConditionNameIfAny(c, sb);
		appendConditionInfo(c, sb);
		
		sb.append(':');
		sb.append(c.inComparands);
		
		return sb.toString();		
	}
	
}	// class CollectableInCondition
