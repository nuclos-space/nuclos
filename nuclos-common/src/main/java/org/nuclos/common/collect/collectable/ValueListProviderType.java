package org.nuclos.common.collect.collectable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum ValueListProviderType {
	
	DEFAULT("default"),
	ENTITY("entity"),
	ATTRIBUTE_GROUP("attributeGroupFunction"),
	ENTITY_ID("entityId"),
	PROCESS("process"),
	MANDATOR("mandator"),
	ATTRIBUTE("attribute"),
	GENERATION_SOURCE("generationSourceType"),
	GENERATION_ATTRIBUTE("generationAttribute"),
	GENERATION_SUBENTITY("generationSubEntity"),
	MODULE_SUBENTITY("modulesubentity"),
	ALL_REPORTS("allreports"),
	ALL_GENERATIONS("allgenerations"),
	ALL_RECORD_GRANTS("allrecordgrants"),
	MASTERDATA_ENTITY("masterDataEntity"),
	MASTERDATA_FIELDS("masterDataEntityFields"),
	PARAMETERS("parameters"),
	STATUS("status"),
	SELECTIVE_ATTRIBUTE("selectiveattribute"),
	MODULE_SUBENTITY_FIELD("modulesubformfield"),
	DATASOURCE("datasource"),
	SUBENTITY("subform"),
	MASTERDATA_SUBENTITIES("masterdatasubforms"),
	FOREIGN_ENTITY_FIELDS("foreignentityfields"),
	JOB_DB_OBJECTS("dbobjects"),
	ENTITY_FIELDS("entityfields"),
	FIELD_OR_ATTRIBUTE("fieldOrAttribute"),
	DEPENDENTS("dependants"),
	STATUS_NUMERAL("statusNumeral"),
	IMPORT_ENTITIES("importentities"),
	IMPORT_FIELDS("importfields"),
	REFERENCED_ENTITY_FIELD("referencedEntityField"),
	CSV_IMPORT_STRUCTURES("importStructures"),
	XML_IMPORT_STRUCTURES("xmlImportStructures"),
	SUBENTITY_FIELDS("subformfields"),
	ACTIONS("actions"),
	GENERIC("generic"),
	ENUM("enum"),
	DB_TYPE("dbtype"),
	DB_OBJECT_TYPE("dbobjecttype"),
	DB_OBJECT("dbobject"),
	ROLE_HIERACHY_USERS("rolehierarchyusers"),
	READABLE_USERS("readableusers"),
	LOCALES("locales"),
	ASSIGN_WORKSPACES("assignableworkspaces"),
	DYNAMIC_TASKLIST_ATTRIBUTES("dynamicTasklistAttributes"),
	PRINTSERVICE_TRAYS("printServiceTrays"),
	PRINTSERVICE_TRAYS_SELF("printServiceTraysSelf"),
	COMMUNICATION_INTERFACE_PARAMETER("communicationInterfaceParameter"),
	USER_COMMUNICATION_ACCOUNT("userCommunicationAccount"),
	SHAREABLE_PREFERENCES("shareablePreferences"),
	NUCLETS("nuclets"),
	NUCLET_INTEGRATION_POINTS("nucletIntegrationPoints"),
	CUSTOM_REST_RULES("customRestRules"),
	SYSTEM_PARAMETER("systemParameter");
	
	//
	
	private static Map<String,ValueListProviderType> TYPE2ENUM;
	
	public static ValueListProviderType toValueListProviderType(String type) {
		if (type == null) return null;
		final ValueListProviderType result = TYPE2ENUM.get(type);
		if (result == null) {
			throw new IllegalArgumentException("Unknown type: " + type);
		}
		return result;
	}
	
	private static void register(ValueListProviderType t) {
		final String type = t.getType();
		if (TYPE2ENUM == null) {
			TYPE2ENUM = new ConcurrentHashMap<String, ValueListProviderType>();
		}
		if (TYPE2ENUM.containsKey(type)) {
			throw new IllegalArgumentException();
		}
		TYPE2ENUM.put(type, t);
	}
	
	//
	
	private final String type;
	
	private ValueListProviderType(String type) {
		this.type = type;
		register(this);
	}
	
	public String getType() {
		return type;
	}

}
