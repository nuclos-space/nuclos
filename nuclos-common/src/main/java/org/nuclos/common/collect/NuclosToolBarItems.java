//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import org.nuclos.common.collect.ToolBarItem.Type;


public class NuclosToolBarItems {

	public static final ToolBarItem COLLECT_INDICATOR = new ToolBarItem(Type.ACTION, "nuclos_collectindicator");
	public static final ToolBarItem INFO = new ToolBarItem(Type.ACTION, "nuclos_info");
	public static final ToolBarItem MENU_SEPARATOR = new ToolBarItem(Type.SEPARATOR, "nuclos_menuseparator");
	public static final ToolBarItem NEW = new ToolBarItem(Type.ACTION, "nuclos_new");
	public static final ToolBarItem NEW_FROM_SEARCH = new ToolBarItem(Type.ACTION, "nuclos_newfromsearch");
	public static final ToolBarItem SAVE = new ToolBarItem(Type.ACTION, "nuclos_save");
	public static final ToolBarItem DELETE = new ToolBarItem(Type.ACTION, "nuclos_delete");
	public static final ToolBarItem DELETE_REAL = new ToolBarItem(Type.ACTION, "nuclos_deletereal");
	public static final ToolBarItem RESTORE = new ToolBarItem(Type.ACTION, "nuclos_restore");
	public static final ToolBarItem MULTI_DELETE = new ToolBarItem(Type.ACTION, "nuclos_multidelete");
	public static final ToolBarItem MULTI_DELETE_REAL = new ToolBarItem(Type.ACTION, "nuclos_multideletereal");
	public static final ToolBarItem MULTI_RESTORE = new ToolBarItem(Type.ACTION, "nuclos_multirestore");
	public static final ToolBarItem MULTI_EDIT = new ToolBarItem(Type.ACTION, "nuclos_multiedit");
	public static final ToolBarItem MULTI_SHOW_IN_EXPLORER = new ToolBarItem(Type.ACTION, "nuclos_multishowinexplorer");
	public static final ToolBarItem QUICKSELECTION = new ToolBarItem(Type.ACTION, "nuclos_quickselection");
	public static final ToolBarItem REFRESH = new ToolBarItem(Type.ACTION, "nuclos_refresh");
	public static final ToolBarItem REFRESH_LIST = new ToolBarItem(Type.ACTION, "nuclos_refreshlist");
	public static final ToolBarItem SEARCH = new ToolBarItem(Type.ACTION, "nuclos_search");
	public static final ToolBarItem CLEAR_SEARCH_FORM = new ToolBarItem(Type.ACTION, "nuclos_clearsearchform");
	public static final ToolBarItem CHANGE_STATE = new ToolBarItem(Type.PSR_ACTION, "nuclos_changestate");
	public static final ToolBarItem SEARCH_STATE = new ToolBarItem(Type.DROPDOWN_ACTION, "nuclos_searchstate");
	public static final ToolBarItem CLONE = new ToolBarItem(Type.ACTION, "nuclos_clone");
	public static final ToolBarItem GENERATE = new ToolBarItem(Type.PSD_ACTION, "nuclos_generate");
	public static final ToolBarItem BOOKMARK = new ToolBarItem(Type.ACTION, "nuclos_bookmark");
	public static final ToolBarItem HELP = new ToolBarItem(Type.ACTION, "nuclos_help");
	public static final ToolBarItem OPEN_IN_NEW_TAB = new ToolBarItem(Type.ACTION, "nuclos_openinnewtab");
	public static final ToolBarItem SHOW_IN_EXPLORER = new ToolBarItem(Type.ACTION, "nuclos_showinexplorer");
	public static final ToolBarItem EXECUTE_RULE = new ToolBarItem(Type.ACTION, "nuclos_executerule");
	public static final ToolBarItem SHOW_STATE_HISTORY = new ToolBarItem(Type.ACTION, "nuclos_showstatehistory");
	public static final ToolBarItem SHOW_HISTORY = new ToolBarItem(Type.ACTION, "nuclos_showhistory");
	public static final ToolBarItem PRINT_DETAILS = new ToolBarItem(Type.ACTION, "nuclos_printdetails");
	public static final ToolBarItem CANCEL_EDIT = new ToolBarItem(Type.ACTION, "nuclos_canceledit");
	public static final ToolBarItem TEXT_SEARCH = new ToolBarItem(Type.TEXTFIELD, "nuclos_textsearch");
	public static final ToolBarItem CONFIG_COLUMNS = new ToolBarItem(Type.ACTION, "nuclos_configcolumns");
	public static final ToolBarItem LIST_PROFILE = new ToolBarItem(Type.ACTION, "nuclos_listprofile");
	public static final ToolBarItem RESET_FILTER = new ToolBarItem(Type.ACTION, "nuclos_resetfilter");
	public static final ToolBarItem SET_FILTER = new ToolBarItem(Type.PSR_ACTION, "nuclos_setfilter");
	public static final ToolBarItem LOAD_FILTER = new ToolBarItem(Type.PSR_ACTION, "nuclos_loadfilter");
	public static final ToolBarItem SEARCH_DELETED = new ToolBarItem(Type.PR_ACTION, "nuclos_searchdeleted");
	public static final ToolBarItem SEARCH_EDITOR = new ToolBarItem(Type.TOGGLE_ACTION, "nuclos_searcheditor");
	public static final ToolBarItem SAVE_FILTER = new ToolBarItem(Type.ACTION, "nuclos_savefilter");
	public static final ToolBarItem DELETE_FILTER = new ToolBarItem(Type.ACTION, "nuclos_deletefilter");
	public static final ToolBarItem NEW_AT_POSITION = new ToolBarItem(Type.ACTION, "nuclos_newatposition");
	public static final ToolBarItem COPY_ROW = new ToolBarItem(Type.ACTION, "nuclos_copyrow");
	public static final ToolBarItem CUT_ROW = new ToolBarItem(Type.ACTION, "nuclos_cutrow");
	public static final ToolBarItem PASTE_ROW = new ToolBarItem(Type.ACTION, "nuclos_pasterow");
	public static final ToolBarItem TRANSFER = new ToolBarItem(Type.ACTION, "nuclos_transfer");
	public static final ToolBarItem FILTER = new ToolBarItem(Type.TOGGLE_ACTION, "nuclos_filter");
	public static final ToolBarItem DOCUMENT_IMPORT = new ToolBarItem(Type.ACTION, "nuclos_documentimport");
	public static final ToolBarItem PRINT_REPORT = new ToolBarItem(Type.ACTION, "nuclos_printreport");
	public static final ToolBarItem AUTONUMBER_PUSH_UP = new ToolBarItem(Type.ACTION, "nuclos_autonumberpushup");
	public static final ToolBarItem AUTONUMBER_PUSH_DOWN = new ToolBarItem(Type.ACTION, "nuclos_autonumberpushdown");
	public static final ToolBarItem SUBFORM_DETAILS_VIEW = new ToolBarItem(Type.ACTION, "nuclos_subformdetailsview");
	public static final ToolBarItem SUBFORM_DETAILS_ZOOM = new ToolBarItem(Type.ACTION, "nuclos_subformdetailszoom");
	public static final ToolBarItem RECORDNAV_FIRST = new ToolBarItem(Type.ACTION, "nuclos_recordnavfirst");
	public static final ToolBarItem RECORDNAV_PREVIOUS = new ToolBarItem(Type.ACTION, "nuclos_recordnavprevious");
	public static final ToolBarItem RECORDNAV_NEXT = new ToolBarItem(Type.ACTION, "nuclos_recordnavnext");
	public static final ToolBarItem RECORDNAV_LAST = new ToolBarItem(Type.ACTION, "nuclos_recordnavlast");
	public static final ToolBarItem SUBFORM_CHANGE_STATE = new ToolBarItem(Type.ACTION, "nuclos_subformchangestate");
	public static final ToolBarItem LOCALIZATION = new ToolBarItem(Type.ACTION, "nuclos_localizsation");
	public static final ToolBarItem LOCK = new ToolBarItem(Type.TOGGLE_ACTION, "nuclos_lock");
	public static final ToolBarItem SEARCH_OWNER = new ToolBarItem(Type.LOV, "nuclos_searchOwner");
	
	public static final ToolBarItemGroup newExtras() {
		return new ToolBarItemGroup("nuclos_extras");
	}
	/**
	 * Do not add items! Useful for equals only
	 */
	public final static ToolBarItem EXTRAS = NuclosToolBarItems.newExtras();
	
}
