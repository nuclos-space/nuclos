package org.nuclos.common.collect.collectable;

public interface IUncompletable {
	
	public void markAsUncomplete();
	
}
