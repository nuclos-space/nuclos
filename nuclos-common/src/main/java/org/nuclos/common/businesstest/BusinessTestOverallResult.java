package org.nuclos.common.businesstest;

import java.io.Serializable;

import org.nuclos.common2.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents the overall result of all executed business tests.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusinessTestOverallResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3566355284886857800L;
	private BusinessTestVO.STATE state;
	private long testsTotal;
	private long testsRed;
	private long testsYellow;
	private long testsGreen;
	private int warningCount;
	private int errorCount;
	private DateTime start;
	private DateTime end;
	private long duration;

	public BusinessTestVO.STATE getState() {
		return state;
	}

	public void setState(BusinessTestVO.STATE state) {
		this.state = state;
	}

	public int getWarningCount() {
		return warningCount;
	}

	public void setWarningCount(int warningCount) {
		this.warningCount = warningCount;
	}

	public int getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(int errorCount) {
		this.errorCount = errorCount;
	}

	public DateTime getStart() {
		return start;
	}

	public void setStart(DateTime start) {
		this.start = start;
	}

	public DateTime getEnd() {
		return end;
	}

	public void setEnd(DateTime end) {
		this.end = end;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}
	public long getTestsRed() {
		return testsRed;
	}

	public void setTestsRed(long testsRed) {
		this.testsRed = testsRed;
	}

	public long getTestsYellow() {
		return testsYellow;
	}

	public void setTestsYellow(long testsYellow) {
		this.testsYellow = testsYellow;
	}

	public long getTestsGreen() {
		return testsGreen;
	}

	public void setTestsGreen(long testsGreen) {
		this.testsGreen = testsGreen;
	}

	public long getTestsTotal() {
		return testsTotal;
	}

	public void setTestsTotal(long testsTotal) {
		this.testsTotal = testsTotal;
	}
}
