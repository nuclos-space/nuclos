//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LafParameterHashMap<T,S> implements LafParameterMap<T,S>, Map<LafParameter<T,S>,T>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5318071077623311625L;
	private final Map<LafParameter<T,S>,T> wrapped;
	
	public LafParameterHashMap(Map<LafParameter<T,S>,T> wrapped) {
		if (wrapped == null) {
			throw new NullPointerException();
		}
		if (!(wrapped instanceof Serializable)) {
			throw new IllegalStateException("Wrapped Map must be Serializable");
		}
		this.wrapped = wrapped;
	}
	
	public LafParameterHashMap() {
		this(new HashMap<LafParameter<T,S>,T>());
	}

	@Override
	public T getValue(LafParameter<T,S> parameter) {
		return (T) get(parameter);
	}
	
	@Override
	public void putValue(LafParameter<T,S> parameter, T value) {
		put(parameter, value);
	}
	
	@Override
	public S getStorageValue(LafParameter<T, S> parameter) {
		return parameter.convertToStore(getValue(parameter));
	}

	@Override
	public void putStorageValue(LafParameter<T, S> parameter, S storageValue) throws ParseException {
		putValue(parameter, parameter.convertFromStore(storageValue));
	}
	
	// Map methods

	@Override
	public int size() {
		return wrapped.size();
	}

	@Override
	public boolean isEmpty() {
		return wrapped.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return wrapped.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return wrapped.containsValue(value);
	}

	@Override
	public T get(Object key) {
		return wrapped.get(key);
	}

	@Override
	public T put(LafParameter<T,S> key, T value) {
		return wrapped.put(key, value);
	}

	@Override
	public T remove(Object key) {
		return wrapped.remove(key);
	}

	@Override
	public void putAll(Map<? extends LafParameter<T,S>, ? extends T> m) {
		wrapped.putAll(m);
	}

	@Override
	public void clear() {
		wrapped.clear();
	}

	@Override
	public Set<LafParameter<T,S>> keySet() {
		return wrapped.keySet();
	}

	@Override
	public Collection<T> values() {
		return wrapped.values();
	}

	@Override
	public Set<Map.Entry<LafParameter<T,S>,T>> entrySet() {
		return wrapped.entrySet();
	}

}
