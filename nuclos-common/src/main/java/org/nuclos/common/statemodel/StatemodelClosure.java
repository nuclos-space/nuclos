//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.statemodel;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.transport.GzipMap;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Transferable container class for Statemodel-instances, which contains the
 * closure of all state models for a given module. Like the statemodels
 * themselves, an instance of this class is bound to the current user
 * <p>
 * TODO: This should be cached and NOT be transfered between server and client.
 * Instead, it should be re-constructed on the client side from information
 * available from (client) caches. (tp)
 */
public class StatemodelClosure implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -280414192713539487L;
	private UID                          module;
	private Map<UsageCriteria, Statemodel>   statemodels;
	
	private Map<UID, String>	         labelResourceSIDs;
	private Map<UID, String>	         desriptionResourceSIDs;

	private Map<UID, String> statusModelNameById = new HashMap<>();


	public StatemodelClosure(UID module) {
	    this.module = module;
	    statemodels = new GzipMap<UsageCriteria, Statemodel>();
    }

	public void addStatemodels(Collection<Statemodel> statemodel) {
		for (Statemodel sm : statemodel) {
		    statemodels.put(sm.getUsageCriteria(), sm);			
		}
    }

	public UID getModuleUID() {
    	return module;
    }

	public Map<UsageCriteria, Statemodel> getStatemodels() {
    	return statemodels;
    }
	
	public Statemodel getStatemodel(UsageCriteria crit) {
		Statemodel res = statemodels.get(crit);
		if(res == null)
			res = statemodels.get(new UsageCriteria(crit.getEntityUID(), crit.getProcessUID(), null, null));
		if(res == null)
			res = statemodels.get(new UsageCriteria(crit.getEntityUID(), null, null, null));
		return res;
	}
	
	public Set<StateVO> getAllStates() {
		HashSet<StateVO> res = new HashSet<StateVO>();
		for(Statemodel s : statemodels.values())
			res.addAll(s.getAllStates());
		return res;
	}
	
	public StateVO getState(UID state) {
		StateVO result = null;
		for(Statemodel s : statemodels.values()) {
			result = s.getState(state);
			if (result != null) {
				break;
			}
		}
		return result;
	}

	private void ensureResMaps() {
		if(labelResourceSIDs == null) {
			HashMap<UID, String> t = new HashMap<UID, String>();
			for(Statemodel s : statemodels.values())
				t.putAll(s.getLabelResourceSIDs());
			labelResourceSIDs = t;
		}
		if(desriptionResourceSIDs == null) {
			HashMap<UID, String> t = new HashMap<UID, String>();
			for(Statemodel s : statemodels.values())
				t.putAll(s.getDesriptionResourceSIDs());
			desriptionResourceSIDs = t;
		}
	}
	
	public String getResourceSIdForLabel(UID state) {
		ensureResMaps();
	    return labelResourceSIDs.get(state);
    }

	public String getResourceSIdForDescription(UID state) {
		ensureResMaps();
	    return desriptionResourceSIDs.get(state);
    }

	public Map<UID, String> getStatusModelNameById() {
		return statusModelNameById;
	}
}
