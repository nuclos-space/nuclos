//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

public class TransferNuclet implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2585657932535476300L;
	private UID uid;
	private String label;
	private Integer version;
	private final boolean nuclon;
	private final boolean source;
	
	public TransferNuclet(UID uid, String label, Integer version, boolean nuclon, boolean source) {
		super();
		this.uid = uid;
		this.label = label;
		this.version = version;
		this.nuclon = nuclon;
		this.source = source;
	}	
	
	public UID getUID() {
		return uid;
	}
	public void setUID(UID uid) {
		this.uid = uid;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}


	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		if (version == null) {
			version = Integer.valueOf(-1);
		}
		this.version = version;
	}

	public boolean isNuclon() {
		return nuclon;
	}

	public boolean isSource() {
		return source;
	}

	@Override
	public int hashCode() {
		return LangUtils.hashCode(label) ^ LangUtils.hashCode(uid);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TransferNuclet)
			return LangUtils.equal(((TransferNuclet)obj).uid, uid);
		
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return getLabel();
	}
}
