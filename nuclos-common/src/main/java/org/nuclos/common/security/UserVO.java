//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.security;

import java.util.Date;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value object representing a user for administration purposes (exclude preferences).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class UserVO extends NuclosValueObject<UID> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -878255780140323253L;
	private String name;
	private String email;
	private Boolean loginWithEmailAllowed;
	private String lastname;
	private String firstname;
	private Boolean superuser;
	private Boolean locked;
	private Date passwordChanged;
	private Date expirationDate;
	private Boolean requirePasswordChange;
	private UID communicationAccountPhone;
	private String activationcode;
	private String passwordresetcode;
	private Date lastlogin;
	private Integer loginattempts;
	private Boolean privacyconsent;

	// non-persistent attributes
	private Boolean setPassword;
	private String newPassword;
	private Boolean notifyUser;

	private Map<UID, Object> originalFields;

	public UserVO(MasterDataVO<UID> mdvo) {
		this(mdvo.getEntityObject());
	}

	public UserVO(EntityObjectVO<UID> mdvo) {
		super(mdvo.getId(), mdvo.getCreatedAt(), mdvo.getCreatedBy(), mdvo.getChangedAt(), mdvo.getChangedBy(), mdvo.getVersion());
		this.name = mdvo.getFieldValue(E.USER.username);
		this.email = mdvo.getFieldValue(E.USER.email);
		this.loginWithEmailAllowed = Boolean.TRUE.equals(mdvo.getFieldValue(E.USER.loginWithEmailAllowed));
		this.lastname = mdvo.getFieldValue(E.USER.lastname);
		this.firstname = mdvo.getFieldValue(E.USER.firstname);
		this.superuser = mdvo.getFieldValue(E.USER.superuser);
		this.locked = mdvo.getFieldValue(E.USER.locked);
		this.passwordChanged = mdvo.getFieldValue(E.USER.lastPasswordChange);
		this.expirationDate = mdvo.getFieldValue(E.USER.expirationDate);
		this.requirePasswordChange = mdvo.getFieldValue(E.USER.passwordChangeRequired);
		this.originalFields = mdvo.getFieldValues();
		this.communicationAccountPhone = mdvo.getFieldUid(E.USER.communicationAccountPhone); 
		this.activationcode = mdvo.getFieldValue(E.USER.activationCode);
		this.privacyconsent = mdvo.getFieldValue(E.USER.privacyConsentAccepted);
		this.passwordresetcode = mdvo.getFieldValue(E.USER.passwordResetCode);
		this.lastlogin = mdvo.getFieldValue(E.USER.lastLogin);
		this.loginattempts = mdvo.getFieldValue(E.USER.loginAttempts);
	}

	public String getUsername() {
		return name;
	}

	public void setUsername(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Boolean getSuperuser() {
		return superuser != null ? superuser : Boolean.FALSE;
	}

	public void setSuperuser(Boolean superuser) {
		this.superuser = superuser;
	}

	public boolean getLocked() {
		return locked != null ? locked : Boolean.FALSE;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Date getLastPasswordChange() {
		return passwordChanged;
	}

	public void setLastPasswordChange(Date passwordChanged) {
		this.passwordChanged = passwordChanged;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Boolean getPasswordChangeRequired() {
		return requirePasswordChange != null ? requirePasswordChange : Boolean.FALSE;
	}

	public void setPasswordChangeRequired(Boolean requirePasswordChange) {
		this.requirePasswordChange = requirePasswordChange;
	}

	public Boolean getSetPassword() {
		return setPassword != null ? setPassword : Boolean.FALSE;
	}

	public void setSetPassword(Boolean setPassword) {
		this.setPassword = setPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public Boolean getNotifyUser() {
		return notifyUser == null ? Boolean.FALSE : notifyUser;
	}

	public void setNotifyUser(Boolean notifyUser) {
		this.notifyUser = notifyUser;
	}

	public UID getCommunicationAccountPhone() {
		return communicationAccountPhone;
	}

	public void setCommunicationAccountPhone(UID communicationAccountPhone) {
		this.communicationAccountPhone = communicationAccountPhone;
	}

	public String getActivationCode() {
		return activationcode;
	}

	public void setActivationCode(String activationcode) {
		this.activationcode = activationcode;
	}
	
	public Boolean isPrivacyConsentAccepted() {
		return privacyconsent == null ? Boolean.FALSE : privacyconsent;
	}
	
	public void setPrivacyConsentAccepted(Boolean privacyconsent) {
		this.privacyconsent = privacyconsent;
	}

	public String getPasswordResetCode() {
		return passwordresetcode;
	}

	public void setPasswordResetCode(String passwordresetcode) {
		this.passwordresetcode = passwordresetcode;
	}

	public Date getLastlogin() {
		return lastlogin;
	}

	public void setLastlogin(final Date lastlogin) {
		this.lastlogin = lastlogin;
	}

	public Integer getLoginAttempts() {
		return loginattempts;
	}

	public void setLoginAttempts(final Integer loginattempts) {
		this.loginattempts = loginattempts;
	}

	public boolean isLoginWithEmailAllowed() {
		return Boolean.TRUE.equals(loginWithEmailAllowed);
	}

	public void setLoginWithEmailAllowed(final Boolean emailLogin) {
		this.loginWithEmailAllowed = emailLogin;
	}

	public MasterDataVO<UID> toMasterDataVO() {
		MasterDataVO<UID> mdvo = new MasterDataVO<UID>(E.USER.getUID(), getId(), 
				getCreatedAt(), getCreatedBy(), getChangedAt(), getChangedBy(), getVersion(), null, null, null, false);
		mdvo.getEntityObject().setComplete(true);
		
		mdvo.setFieldValue(E.USER.username, name);
		mdvo.setFieldValue(E.USER.email, email);
		mdvo.setFieldValue(E.USER.loginWithEmailAllowed, loginWithEmailAllowed);
		mdvo.setFieldValue(E.USER.lastname, lastname);
		mdvo.setFieldValue(E.USER.firstname, firstname);
		mdvo.setFieldValue(E.USER.superuser, superuser);
		mdvo.setFieldValue(E.USER.locked, locked);
		mdvo.setFieldValue(E.USER.lastPasswordChange, passwordChanged);
		mdvo.setFieldValue(E.USER.expirationDate, expirationDate);
		mdvo.setFieldValue(E.USER.passwordChangeRequired, requirePasswordChange);
		mdvo.setFieldUid(E.USER.communicationAccountPhone, communicationAccountPhone);
		mdvo.setFieldValue(E.USER.activationCode, activationcode);
		mdvo.setFieldValue(E.USER.privacyConsentAccepted, privacyconsent);
		mdvo.setFieldValue(E.USER.passwordResetCode, passwordresetcode);
		mdvo.setFieldValue(E.USER.lastLogin, lastlogin);
		mdvo.setFieldValue(E.USER.loginAttempts, loginattempts);

		return mdvo;
	}

	@Override
	public int hashCode() {
		return (getUsername() != null ? getUsername().hashCode() : 0);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof UserVO) {
			final UserVO that = (UserVO) o;
			// rules are equal if there names are equal
			return this.getUsername().equals(that.getUsername());
		}
		return false;
	}

	@Override
	public String toString() {
		return this.getUsername();
	}

}	// class UserVO
