package org.nuclos.common.security;

import org.nuclos.common.MandatorVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.exception.CommonBusinessException;

public interface MandatorFacadeRemote {

	public MandatorVO create(MandatorVO vo) throws CommonBusinessException;

	public MandatorVO modify(MandatorVO vo, IDependentDataMap mpDependants, String customUsage) throws CommonBusinessException;

	public void remove(MandatorVO vo) throws CommonBusinessException;


}

