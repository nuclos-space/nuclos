//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.common2.LocaleInfo;

public class TranslationVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5793725864378346340L;
	public static final String LABELS_ENTITY[] = {"titel","menupath","treeview","treeviewdescription"};
	public static final String LABELS_FIELD[] = {"label","description"};
	public static final String LABELS_CUSTOM_COMPONENT[] = {"label","menupath"};
	
	//

	private final UID locale;
	private final String sLanguage;
	private final String sCountry;
	private Map<String, String> name2translation;

	private TranslationVO(UID locale, String country, String sLanguage, Map<String, String> lstLanguage) {
		super();
		this.locale = locale;
		this.sCountry = country;
		this.sLanguage = sLanguage;
		this.name2translation = lstLanguage;
	}

	public TranslationVO(LocaleInfo li, Map<String, String> lstLanguage) {
		super();
		this.locale = li.getLocale();
		this.sCountry = li.getTitle();
		this.sLanguage = li.getLanguage();
		this.name2translation = lstLanguage;
	}

	public String getLanguage() {
		return sLanguage;
	}

	public String getCountry() {
		return this.sCountry;
	}

	public Map<String, String> getLabels() {
		return name2translation;
	}

	public void setLabels(Map<String, String> lstLabels) {
		this.name2translation = lstLabels;
	}

	public UID getLocale() {
		return locale;
	}

}
