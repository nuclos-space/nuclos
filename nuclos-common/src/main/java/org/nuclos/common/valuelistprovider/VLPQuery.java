package org.nuclos.common.valuelistprovider;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.common.UID;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class VLPQuery implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4309609141173785590L;

	private final UID valuelistProviderUid;

	private Map<String, Object> queryParams;
	private String nameField;
	private String idFieldName;
	private UID entityFieldUID;
	private UID baseEntityUID;
	private UID mandatorUID;
	private Integer maxRowCount;
	private UID languageUID;
	private String quickSearchInput;
	private String defaultField;

	public VLPQuery(final UID valuelistProviderUid) {
		this.valuelistProviderUid = valuelistProviderUid;
	}

	public UID getValuelistProviderUid() {
		return valuelistProviderUid;
	}

	public Map<String, Object> getQueryParams() {
		return queryParams;
	}

	public VLPQuery setQueryParams(final Map<String, Object> queryParams) {
		this.queryParams = queryParams;
		return this;
	}

	public String getIdFieldName() {
		return idFieldName;
	}

	public VLPQuery setIdFieldName(final String idFieldName) {
		this.idFieldName = idFieldName;
		return this;
	}

	public UID getBaseEntityUID() {
		return baseEntityUID;
	}

	public VLPQuery setBaseEntityUID(final UID baseEntityUID) {
		this.baseEntityUID = baseEntityUID;
		return this;
	}

	public UID getMandatorUID() {
		return mandatorUID;
	}

	public VLPQuery setMandatorUID(final UID mandatorUID) {
		this.mandatorUID = mandatorUID;
		return this;
	}

	public Integer getMaxRowCount() {
		return maxRowCount;
	}

	public VLPQuery setMaxRowCount(final Integer maxRowCount) {
		this.maxRowCount = maxRowCount;
		return this;
	}

	public UID getLanguageUID() {
		return languageUID;
	}

	public VLPQuery setLanguageUID(final UID languageUID) {
		this.languageUID = languageUID;
		return this;
	}

	public String getNameField() {
		return nameField;
	}

	public VLPQuery setNameField(final String nameField) {
		this.nameField = nameField;
		return this;
	}

	public String getQuickSearchInput() {
		return quickSearchInput;
	}

	public VLPQuery setQuickSearchInput(final String quickSearchInput) {
		this.quickSearchInput = quickSearchInput;
		return this;
	}

	public UID getEntityFieldUID() {
		return entityFieldUID;
	}

	public VLPQuery setEntityFieldUID(final UID entityFieldUID) {
		this.entityFieldUID = entityFieldUID;
		return this;
	}

	public VLPQuery setDefaultField(final String defaultField) {
		this.defaultField = defaultField;
		return this;
	}

	public String getDefaultField() {
		return defaultField;
	}
}
