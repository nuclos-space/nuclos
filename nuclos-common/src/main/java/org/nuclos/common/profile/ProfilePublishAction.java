package org.nuclos.common.profile;

import java.io.Serializable;

import org.nuclos.common.preferences.TablePreferences;

public class ProfilePublishAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5163845457902094450L;
	private final PublishType publishType;
	private final TablePreferences profile;
	

	public ProfilePublishAction(TablePreferences profile, PublishType publishType) {
		super();
		this.publishType = publishType;
		this.profile = profile;
	}

	public PublishType getPublishType() {
		return publishType;
	}
	
	public TablePreferences getProfile() {
		return this.profile;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("ProfilePublishAction[type=").append(publishType).append(",profile=").append(profile).append("]");
		return result.toString();
	}
	
}
