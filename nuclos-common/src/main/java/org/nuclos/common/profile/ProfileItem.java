//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.profile;

import org.nuclos.common.UID;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common2.SpringLocaleDelegate;

public class ProfileItem {

	private final TablePreferences tpref;

	public ProfileItem(TablePreferences tpref) {
		super();
		
		this.tpref = tpref;
	}
	
	public UID getUID() {
		return tpref.getUID();
	}
	
	public boolean isShared() {
		return tpref.isShared();
	}

	public String getName() {
		return this.tpref.getName();
	}
	
	public TablePreferences getPreferences() {
		return this.tpref;
	}
	
	public boolean isActive() {
		return this.tpref.isSelected();
	}
	
	@Override
	public String toString() {
		String name = (getName() != null) ? getName() : SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfileItem.1", "<Standard>");
		return name;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((tpref.getUID() == null) ? 0 : tpref.getUID().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfileItem other = (ProfileItem) obj;
		if (tpref.getUID() == null) {
			if (other.tpref.getUID() != null)
				return false;
		} else if (!tpref.getUID().equals(other.tpref.getUID()))
			return false;
		return true;
	}
}
