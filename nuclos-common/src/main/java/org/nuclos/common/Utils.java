//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.ServiceLocator;
import org.nuclos.server.i18n.language.data.DataLanguageFacadeRemote;
import org.nuclos.server.navigation.ejb3.TreeNodeFacadeRemote;

/**
 * Common utility methods for Nucleus.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
public class Utils {

	/** 
	 * §todo eliminate these constants 
	 */
	@Deprecated
	public static final Calendar MINDATE = GregorianCalendar.getInstance();
	@Deprecated
	public static final Calendar MAXDATE = GregorianCalendar.getInstance();

	static {
		MINDATE.clear();
		MINDATE.set(1900, 0, 1);
		MAXDATE.clear();
		MAXDATE.set(2900, 0, 1);
	}

	private Utils() {
	}

	/**
	 * transforms the given Collection into one containing <code>null</code> ids.
	 * 
	 * §todo eliminate this workaround!
	 */
	@Deprecated
	public static <PK> Collection<EntityObjectVO<PK>> clearIds(final Collection<EntityObjectVO<PK>> collmdvo) {
		class ClearId implements Transformer<EntityObjectVO<PK>, EntityObjectVO<PK>> {
			@Override
			public EntityObjectVO<PK> transform(EntityObjectVO<PK> mdvo) {
				final EntityObjectVO<PK> result = mdvo.copy();
				if (!mdvo.isFlagRemoved()) { //@see NUCLOS-2468
					result.flagNew();
				} else {
					result.flagRemove();
				}
				assert result.getId() == null;
				return result;
			}
		}

		return CollectionUtils.transform(collmdvo, new ClearId());
	}

	/**
	 * transforms the given map into one containing <code>null</code> ids.
	 * 
	 * §todo eliminate this workaround!
	 * 
	 * @return DependantMasterDataMap containing <code>null</code> ids.
	 */
	@Deprecated
	public static <PK> IDependentDataMap clearIds(IDependentDataMap mpDependents) {
		final IDependentDataMap result = new DependentDataMap();
		for (IDependentKey dependentKey : mpDependents.getKeySet()) {
			for (EntityObjectVO<?> mdVO : mpDependents.getData(dependentKey)) {
				mdVO.setDependents(clearIds(mdVO.getDependents()), mdVO.getApiCacheAlreadyLoadedDependents());
			}
			result.addAllData(dependentKey, Utils.clearIds(mpDependents.getDataPk(dependentKey)));
		}
		return result;
	}

	/**
	 * @return remote interface of tree node facade
	 */
	public static TreeNodeFacadeRemote getTreeNodeFacade() {
		try {
			return ServiceLocator.getInstance().getFacade(TreeNodeFacadeRemote.class);
		}
		catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	/**
	 * @return remote interface of tree node facade
	 */
	public static DataLanguageFacadeRemote getDataLanguageFacade() {
		try {
			return ServiceLocator.getInstance().getFacade(DataLanguageFacadeRemote.class);
		}
		catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	

}	// class Utils
