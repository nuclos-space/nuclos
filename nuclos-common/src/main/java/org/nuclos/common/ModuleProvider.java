//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * Encapsulation of the (generic object related) modules in a Nucleus application.
 * Note that the entity corresponding to the module is contained in
 * <code>MasterDataVO.getField("entity").getFieldValue()</code>,
 * not in <code>MasterDataVO.getEntity()</code>.
 * There is an artificial ("pseudo-")entity named "generalsearch". This one is not contained in the module table in
 * the database and it does not count to the number of modules.
 * There is another artificial ("pseudo-")entity named "fulltextsearch". This one is not contained in the module table in
 * the database and it does not count to the number of modules. Its mudule ID is '-2'.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public abstract class ModuleProvider {
	
	private static final Logger LOG = Logger.getLogger(ModuleProvider.class);
	
	//

	private Map<UID, EntityMeta<?>> mpModules;
	
	protected ModuleProvider() {
	}

	protected void setModules(Collection<EntityMeta<?>> collEntityMeta) {
		final Map<UID, EntityMeta<?>> uidMap = CollectionUtils.newHashMap();
		LOG.debug("Cleared entities in cache " + this);
		for (EntityMeta<?> eMeta : collEntityMeta) {
			if (eMeta.isStateModel()) {
				uidMap.put(eMeta.getUID(), eMeta);
			}
		}
		this.mpModules = uidMap;
		LOG.info("Refilled entities in cache " + this);
	}

	public void invalidate() {
		setModules(getModules());
	}

	/**
	 * @return Collection&lt;EntityMeta&gt;. The list of modules.
	 * The artificial entity "generalsearch" is not contained in the result.
	 */
	public Collection<EntityMeta<?>> getModules() {
		final List<EntityMeta<?>> result = new ArrayList<EntityMeta<?>>(this.mpModules.values());
		return result;
	}

	/**
	 * @return the number of modules in the application.
	 * The artificial entity "generalsearch" is not being counted in.
	 */
	public int getModuleCount() {
		return mpModules.size();
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param module uid
	 * @return the meta data corresponding to the given module uid.
	 * @throws NoSuchElementException if there is no module with the given uid.
	 */
	public EntityMeta<?> getModule(UID module) throws NoSuchElementException {
		final EntityMeta<?> result = this.mpModules.get(module);
		if (result == null) {
			throw new NoSuchElementException(String.format("A module with entity uid %s does not exist", module));
		}
		assert result != null;
		return result;
	}

	/**
	 * @param entity UID
	 * @return Is the entity with the given uid a module entity?
	 */
	public boolean isModule(UID entity) {
		return this.mpModules.get(entity) != null;
	}

	/**
	 * @param module a valid uid
	 */
	public String getLabel(UID module) throws NoSuchElementException {
		final String result = SpringLocaleDelegate.getInstance().getText(this.getModule(module).getLocaleResourceIdForLabel());
		assert result != null;
		return result;
	}

	/**
	 * @param module uid
	 * @return Is Logbook tracking enabled for the module with the given uid?
	 */
	public boolean isLogbookTracking(UID module) {
		final Boolean bLogbookTracking = this.getModule(module).isLogBookTracking();
		return bLogbookTracking != null && bLogbookTracking.booleanValue();
	}

	/**
	 * @param module uid
	 * @throws NoSuchElementException if there is no module with the given uid.
	 * @return the mnemonic for the system identifer to be used for objects of the given module.
	 */
	public String getSystemIdentifierMnemonic(UID module) {
		return this.getModule(module).getSystemIdPrefix();
	}

}	// class ModuleProvider
