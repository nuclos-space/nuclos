//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collection;

import org.nuclos.server.common.valueobject.NuclosValueObject;


/**
 * Utility methods for <code>Transformer</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * §todo add what is useful from Apache's TransformerUtils.
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
public class TransformerUtils {

	private TransformerUtils() {
	}

	/**
	 * @param transformer1
	 * @param transformer2
	 * @return a chained transformer combining the two given transformers.
	 */
	public static <I,M,O> ChainedTransformer<I, M, O> chained(Transformer<I, ? extends M> transformer1, Transformer<M, O> transformer2) {
		return ChainedTransformer.chained(transformer1, transformer2);
	}

	/**
	 * Factory method for the id-transformer
	 */
	public static <T> Transformer<T, T> id() {
		return IdentityTransformer.id();
	}

	/**
	 * Returns the toString transformer. The transformer uses {@link String#valueOf(Object)} and
	 * hence is null-safe.
	 */
	public static Transformer<Object, String> toStringTransformer() {
		return ToStringTransformer.INSTANCE;
	}

	/**
	 * Basically the same as the GetId-subclass in NuclosValueObject, but usable
	 * for the whole type hierarchy
	 */
	public static <PK, T extends NuclosValueObject<PK>> Transformer<T, PK> getPk(Class<PK> pkcls) {
		return new Transformer<T, PK>() {
			@Override
			public PK transform(T i) {
				return i == null ? null : i.getPrimaryKey();
			}};
	}

	//
	// Internal implementation classes and singletons (single-element enum pattern) 
	//

	private static enum ToStringTransformer implements Transformer<Object, String> {
		INSTANCE;
		
		@Override
		public String transform(Object o) {
			return String.valueOf(o); // null-safe toString()
		}
	}
	
}	// class TransformerUtils
