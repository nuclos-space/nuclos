package org.nuclos.common;

import org.nuclos.common.dal.vo.HasPrimaryKey;

public interface Versionable<PK> extends HasPrimaryKey<PK> {
	
	/**
	 * The version used in the Version Number pattern (according to Marinescu: EJB Design Patterns).
	 * It is required for a Collectable to implement the version pattern to allow for optimistic
	 * concurrency. If you don't need persistency at all, it's safe to always return 0.
	 * <p>
	 * In most cases a version value of <code>0</code> denotes an object not persisted to DB, and a 
	 * version value of <code>-1</code> denotes an object not supporting versioning at all.
	 * </p>
	 * @return the version of the last update of this <code>Collectable</code>.
	 * @author Thomas Pasch
	 * @since Nuclos 3.10
	 */
	int getVersion();

}
