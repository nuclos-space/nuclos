//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.ejb3;

import java.util.Collection;
import java.util.List;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNode;
import org.nuclos.server.navigation.treenode.DynamicTreeNode;
import org.nuclos.server.navigation.treenode.EntitySearchResultTreeNode;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.RelationDirection;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.SystemRelationType;
import org.nuclos.server.navigation.treenode.MasterDataSearchResultTreeNode;
import org.nuclos.server.navigation.treenode.MasterDataTreeNode;
import org.nuclos.server.navigation.treenode.SimpleTreeNode;
import org.nuclos.server.navigation.treenode.SubFormEntryTreeNode;
import org.nuclos.server.navigation.treenode.SubFormTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.navigation.treenode.nuclet.NucletTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.NuclosInstanceTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.AbstractNucletContentEntryTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentTreeNode;

// @Remote
public interface TreeNodeFacadeRemote {

	/**
	 * gets a generic object tree node for a specific generic object
	 * 
	 * §postcondition result != null
	 * 
	 * @param genericObjectId id of generic object to get tree node for
	 * @return generic object tree node for given id, if existing and allowed. null otherwise.
	 * @throws CommonFinderException if the object doesn't exist (anymore).
	 */
	GenericObjectTreeNode getGenericObjectTreeNode(Long genericObjectId, UID module) throws CommonFinderException,
		CommonPermissionException;

	/**
	 *
	 * §postcondition result != null
	 * 
	 * @param iGenericObjectId
	 * @param moduleId the module id
	 * @param iRelationId
	 * @param relationtype
	 * @param direction
	 * @param uidNode id for {@link EntityTreeViewVO}
	 * @param idRoot id for root {@link TreeNode}
	 * @return a new tree node for the generic object with the given id.
	 * @throws CommonFinderException if the object doesn't exist (anymore).
	 */
	GenericObjectTreeNode newGenericObjectTreeNode(
		Long iGenericObjectId, UID moduleId, Long iRelationId,
		SystemRelationType relationtype, RelationDirection direction, String customUsage, Long parentId, UID uidNode, Long idRoot)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * gets the list of sub nodes for a specific generic object tree node.
	 * Note that there is a specific method right on this method.
	 * 
	 * §postcondition result != null
	 * 
	 * @param node tree node of type generic object tree node
	 * @return list of sub nodes for given tree node
	 */
	List<TreeNode> getSubNodesForGenericObjectTreeNode(GenericObjectTreeNode node) throws CommonPermissionException;

	/**
	 * method to get a nuclet tree node for a specific nuclet
	 * 
	 * §postcondition result != null
	 * 
	 * @param nuclet UID of nuclet to get tree node for
	 * @return nuclet tree node for given id
	 */
	NucletTreeNode getNucletTreeNode(final UID nuclet) throws CommonFinderException;

	/**
	 * method to get a masterdata tree node for a specific masterdata record
	 * 
	 * §postcondition result != null
	 * 
	 * @param id id of masterdata record to get tree node for
	 * @param uidNode id for {@link EntityTreeViewVO}
	 * @return masterdata tree node for given id
	 * @throws CommonPermissionException
	 */
	<Id> MasterDataTreeNode<Id> getMasterDataTreeNode(
		Id id, UID entity, UID uidNode, boolean bLoadSubNodes)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * §postcondition result != null
	 * 
	 * @param node
	 * @return the subnodes for the given node.
	 */
	List<TreeNode> getSubNodes(NucletTreeNode node);

	/**
	 * §postcondition result != null
	 * 
	 * @param node
	 * @return the subnodes for the given node.
	 */
	List<AbstractNucletContentEntryTreeNode> getSubNodes(NucletContentTreeNode node);

	/**
	 * §postcondition result != null
	 *
	 * @return the available nodes.
	 */
	List<AbstractNucletContentEntryTreeNode> getAvailableNucletContents();

	/**
	 * §postcondition result != null
	 * 
	 * @param node
	 * @return the subnodes for the given node.
	 */
	List<TreeNode> getSubnodes(DefaultMasterDataTreeNode<?> node, CollectableSearchCondition additionalCondition) throws CommonPermissionException;

	<PK> DynamicTreeNode<PK> getDynamicTreeNode(TreeNode node, MasterDataVO<PK> mdVO);

	<PK> List<TreeNode> getSubNodesForDynamicTreeNode(TreeNode node);

	/**
	 *
	 * @param node
	 * @return
	 */
	<PK> List<SubFormEntryTreeNode<PK>> getSubNodesForSubFormTreeNode(TreeNode node, MasterDataVO<PK> mdVO);

	/**
	 * get the subnodes for a masterdata search result
	 * 
	 * §postcondition result != null
	 * 
	 * @param node
	 * @return the subnodes for the given node.
	 */
	<PK> List<DefaultMasterDataTreeNode<PK>> getSubNodes(MasterDataSearchResultTreeNode<PK> node)
		throws CommonPermissionException;

	/**
	 * method to get the list of sub nodes for a specific generic object search result tree node
	 * 
	 * §postcondition result != null
	 * 
	 * @return list of sub nodes for given tree node
	 */
	List<TreeNode> getSubNodes(EntitySearchResultTreeNode node) throws CommonPermissionException;

	<Id> SubFormEntryTreeNode<Id> getSubFormEntryTreeNode(Id id, UID entity, final UID uidNode, Long idRoot,
        boolean bLoadSubNodes) throws CommonFinderException,
        CommonPermissionException;

	<PK> SubFormTreeNode<PK> getSubFormTreeNode(TreeNode node, MasterDataVO<PK> mdVO);

	AbstractNucletContentEntryTreeNode getNucletContentEntryNode(EntityMeta<UID> entity, UID eoId);
	
	List<AbstractNucletContentEntryTreeNode> getNucletContent(NucletTreeNode node);

	List<NucletTreeNode> getSubNodes(NuclosInstanceTreeNode node);

	void importNodes(UID targetEntityUid, Object id, Collection<Pair<UID, Object>> sourceNodeIds) throws CommonBusinessException;

	/**
	 * Search Tree and return a full loaded TreeNode with all Subnodes. Limit of 200 
	 * @param treeNode
	 * @param search
	 * @return 
	 * @throws CommonBusinessException 
	 */
    SimpleTreeNode getExpandedTreeWithSearch(DefaultMasterDataTreeNode treeNode, String search) throws CommonBusinessException;
	
	
}
