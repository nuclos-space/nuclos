//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.rmi.RemoteException;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common.Utils;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoteException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * TreeNode for MasterDataRecords
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version	01.00.00
 */
public class DefaultMasterDataTreeNode<Id> extends MasterDataTreeNode<Id> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7238158616668860633L;
	/**
	 * Attention:
	 * Even if this sounds weird: mdvo could be null. Do not rely on the
	 * value. See {@link #init()} for details...
	 */
	private UID uidNode;
	private Object idRoot;
	// NUCLOS-6939 transient, because this condition will not be stored
	private transient CollectableSearchCondition additionalSubCondition;

	//Obsolete, only survives because it may exist serialized somewhere
	private final MasterDataVO<Id> mdvo;
	
	public DefaultMasterDataTreeNode(DefaultMasterDataTreeNodeParameters<Id> parameters) {
		super(parameters.getsEntity(), (Id)parameters.getId(), parameters.getLabel(), parameters.getDescription());
		this.mdvo = null;
		this.uidNode = parameters.getUidNode();
		this.idRoot = parameters.getIdRoot();
		super.setLabel(parameters.getLabel());
		super.setDescription(parameters.getDescription());
	}
	
	public DefaultMasterDataTreeNodeParameters<Id> getParameters() {
		return new DefaultMasterDataTreeNodeParameters<Id>(getEntityUID(), getId(), getNodeId(), getRootId(), getLabel(), getDescription());
	}
	
	// @PostConstruct
	final void init() {
		// after deserialization (e.g. from XStream) this is called again
		// but with mdvo == null!
		if (mdvo != null) {
			UID languageToUse = Utils.getDataLanguageFacade().getLanguageToUse();
			if(this.getLabel() == null || StringUtils.looksEmpty(this.getLabel()))
				this.setLabel(getIdentifier(mdvo, languageToUse));
			if(this.getDescription() == null || StringUtils.looksEmpty(this.getDescription()))
				this.setDescription(getDescription(mdvo, languageToUse));
		}
		assert getLabel() != null;
	}

	@Override
	protected List<TreeNode> getSubNodesImpl() throws RemoteException, CommonPermissionException {
		return Utils.getTreeNodeFacade().getSubnodes(this, additionalSubCondition);
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		try {
			DefaultMasterDataTreeNode<Id> ret = (DefaultMasterDataTreeNode<Id>)
				Utils.getTreeNodeFacade().getMasterDataTreeNode(this.getId(), getEntityUID(), getNodeId(), false);
			ret.additionalSubCondition = this.additionalSubCondition;
			return ret;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
		catch (CommonPermissionException ex) {
			throw new CommonRemoteException(ex);
		}
	}

	// NUCLOS-6939 There may be an additional condition for subnodes (e.g. performance)
	public void setAdditionalSubNodeCondition(CollectableSearchCondition additionalSubCondition) {
		this.additionalSubCondition = additionalSubCondition;
	}

	@Override
	public UID getNodeId() {
		return uidNode;
	}

	@Override
	public Object getRootId() {
		return idRoot;
	}
}
