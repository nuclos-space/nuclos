//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport.ejb3;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Remote business interface for import execution control.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:thomas.schiffmann@novabit.de">thomas.schiffmann</a>
 * @version 01.00.00
 */
// @Remote
public interface ImportFacadeRemote {

	/**
	 * Create an import structure.
	 *
	 * @param importStructure The import structure as <code>MasterDataVO</code>
	 * @return The created import structure as <code>MasterDataVO</code>
	 */
	MasterDataVO<UID> createImportStructure(MasterDataVO<UID> importStructure) throws CommonBusinessException;

	/**
	 * Modify an existing import structure.
	 *
	 * @param importStructure The import structure as <code>MasterDataVO</code>
	 * @return The import structure's id.
	 */
	Object modifyImportStructure(MasterDataVO<UID> importStructure) throws CommonBusinessException;

	/**
	 * Remove an existing import structure.
	 *
	 * @param importStructure The import structure to remove.
	 */
	void removeImportStructure(MasterDataVO<UID> importStructure) throws CommonBusinessException;

	/**
	 * Create an file import definition.
	 *
	 * @param fileImport The file import definition as <code>MasterDataVO</code>
	 * @return The created file import definition as <code>MasterDataVO</code>
	 */
	MasterDataVO<UID> createFileImport(MasterDataVO<UID> fileImport) throws CommonBusinessException;

	/**
	 * Modify an existing file import definition.
	 *
	 * @param fileImport The file import definition as <code>MasterDataVO</code>
	 * @return The file import definition's id.
	 */
	Object modifyFileImport(MasterDataVO<UID> fileImport) throws CommonBusinessException;

	/**
	 * Remove an existing file import definition.
	 *
	 * @param fileImport The file import definition to remove.
	 */
	void removeFileImport(MasterDataVO<UID> fileImport) throws CommonBusinessException;

	/**
	 * Start an import from clientside.
	 *
	 * @param importfileId
	 * @return
	 * @throws NuclosFileImportException
	 */
	String doImport(UID importfileId) throws NuclosFileImportException;

	/**
	 * Stop/Interrupt a running import
	 *
	 * @param importfileId
	 * @throws NuclosFileImportException
	 */
	void stopImport(UID importfileId) throws NuclosFileImportException;

	static String localize(String message, ResourceBundle bundle) {
		String resText = null;
		try {
			resText = bundle.getString(StringUtils.getFirstSubString(message, CollectionUtils.asSet("{", " ")));
		}
		catch (RuntimeException e) {
			// text seems to be no resourceId
		}

		if (resText != null) {
			Pattern REF_PATTERN = Pattern.compile("\\{([^\\}]*?)\\}");
			List<String> paramList = new ArrayList<>();
			StringBuffer rb = new StringBuffer();

			Matcher m = REF_PATTERN.matcher(message);
			while (m.find()) {
				String param = m.group(1);
				try {
					if (param.length() > 0 && bundle.containsKey(param.trim())) {
						param = bundle.getString(param.trim());
					}
				}
				catch (RuntimeException e) {
					// param seems to be no resourceId
				}
				paramList.add(param);
			}

			m = REF_PATTERN.matcher(resText);
			while (m.find()) {
				if (paramList.size() > Integer.valueOf(m.group(1))) {
					m.appendReplacement(rb, paramList.get(Integer.valueOf(m.group(1))));
				}
			}
			m.appendTail(rb);
			return rb.toString();
		}
		else {
			return message;
		}
	}

}
