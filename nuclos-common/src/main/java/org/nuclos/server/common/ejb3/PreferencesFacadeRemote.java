//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;


import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.UID;
import org.nuclos.common.preferences.Preference;
import org.nuclos.common.preferences.PreferenceShareVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

public interface PreferencesFacadeRemote extends CommonPreferencesFacade {

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * List of preferences for the current user.
	 *
	 * @param app
	 * @param type
	 * @param entityUID
	 * @param layoutUID
	 * 			return layout dependent preferences.
	 * 			if null and returnDepEntities is true, all layouts are used.
	 * @param orLayoutIsNull
	 *  		return preferences without layout definition (for all layouts)
	 * @param menuRelevant
	 * @param returnDepEntities
	 * 			return dependent entities instead of "entityUID" direct. Uses layouts for determination.
	 * @return a list of PreferenceVOs (not null)
	 */
	@RolesAllowed("Login")
	List<Preference> getPreferences(String app, String type, UID entityUID, UID layoutUID, boolean orLayoutIsNull, Boolean menuRelevant, Boolean returnDepEntities);

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Un-share the preference for the given user role
	 *
	 * @param prefUID
	 * @param roleUID
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void unSharePreference(UID prefUID, UID roleUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Share the preference with the given user role
	 *
	 * @param prefUID
	 * @param roleUID
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void sharePreference(UID prefUID, UID roleUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Get a list of usergroups to share prefernces with
	 *
	 * @param prefUID
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	List<PreferenceShareVO> getPreferenceShares(UID prefUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Get a single preference of the current user.
	 *
	 * @param prefUID
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	Preference getPreference(UID prefUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Insert a single preference for the current user.
	 * App is optional. Would be 'nuclos' if not set
	 * Type is mandatory. In case of app-'nuclos' it must be registered in org.nuclos.common.NuclosPreferenceType
	 *
	 * @param wpref
	 * @return
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	Preference insertPreference(Preference.WritablePreference wpref) throws CommonPermissionException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Update a single preference of the current user.
	 *
	 * @param wpref
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void updatePreference(Preference.WritablePreference wpref) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Update the shared preference with the given customization. (User publish changes.)
	 *
	 * @param wpref
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void updatePreferenceShare(Preference.WritablePreference wpref) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Delete multiple preference items.
	 * Shared preferences will be unshared.
	 *
	 * @param prefUIDs
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void deletePreference(UID ...prefUIDs) throws CommonPermissionException, CommonFinderException;

	@RolesAllowed("Login")
	Preference[] findReferencingPreferences(UID ...prefUIDs) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Delete all preferences of the current user.
	 *
	 */
	@RolesAllowed("Login")
	void deleteMyPreferences();

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Selects the preference for the current user and layout.
	 *
	 * @param prefUID
	 * @param layoutUID
	 */
	@RolesAllowed("Login")
	void selectPreference(UID prefUID, UID layoutUID);

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * De-Select the preference for the current user.
	 *
	 * @param prefUID
	 * @param layoutUID
	 */
	@RolesAllowed("Login")
	void deselectPreference(UID prefUID, final UID layoutUID);

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Resets all customized preferences of these types and entity for the current user.
	 *
	 * @param entity
	 * @param type
	 */
	@RolesAllowed("Login")
	void resetCustomizedPreferences(UID entity, String...type);

}
