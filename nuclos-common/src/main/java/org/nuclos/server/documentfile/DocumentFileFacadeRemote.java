package org.nuclos.server.documentfile;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;

public interface DocumentFileFacadeRemote {
	
	@RolesAllowed("Login")
	public NuclosFile loadContent(UID documentFileUID);
	
	@RolesAllowed("Login")
	public byte[] loadContent(UID documentFileUID, String sFileName);
}
