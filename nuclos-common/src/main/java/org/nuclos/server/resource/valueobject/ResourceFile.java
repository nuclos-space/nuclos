//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.resource.valueobject;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidFile;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.DocumentFile;
import org.nuclos.server.resource.ejb3.ResourceFacadeRemote;

public class ResourceFile extends DocumentFile {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4691058292908308312L;

	public ResourceFile(RigidFile file) {
		super(file);
	}

	public ResourceFile(String sFileName, Object documentFile) {
		super(sFileName, documentFile);
	}

	public ResourceFile(String sFileName, byte[] abContents) {
		super(sFileName, null, abContents);
	}

	public ResourceFile(String sFileName, Object documentFile, byte[] abContents) {
		super(sFileName, documentFile, abContents);
	}

	@Override
	protected byte[] getStoredContents() {
		try {
			final ResourceFacadeRemote facade = SpringApplicationContextHolder.getBean(ResourceFacadeRemote.class);
			return facade.loadResource((UID) this.getDocumentFilePk());
		}
		catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}

	protected String getDirectoryPath() {
		return null;
	}

}
