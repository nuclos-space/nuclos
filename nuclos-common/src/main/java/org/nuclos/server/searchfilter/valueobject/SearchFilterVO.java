//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.searchfilter.valueobject;

import java.util.Date;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value object representing a searchfilter.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 00.01.000
 */
public class SearchFilterVO extends NuclosValueObject<UID> implements Comparable<SearchFilterVO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3992641782914929838L;
	private String sFilterName;
	private String sDescription;
	private String sFilterPrefs;
	private String sOwner;
	private UID entity;
	private Integer iSearchDeleted;
	private String labelResourceId;
	private String descriptionResourceId;
	private Integer iOrder;
	private Boolean bFastSelectInResult;
	private UID fastSelectIcon;
	private String sFastSelectNuclosIcon;
	private Boolean bFastSelectDefault;
	private Boolean bCustomRules;

	/**
	 * NUCLOS-2182
	 */
	private String resultProfile;

	private SearchFilterUserVO searchFilterUserVO;
	
	private UID nuclet;

	public SearchFilterVO() {
		this.searchFilterUserVO = new SearchFilterUserVO();
	}

	public SearchFilterVO(UID uid, String sFilterName, String sDescription, String sFilterPrefs, String sOwner, UID entity,
			Integer iSearchDeleted, String labelResourceId, String descriptionResourceId, 
			Integer iOrder, Boolean bFastSelectInResult, UID fastSelectIcon, String sFastSelectNuclosIcon, Boolean bFastSelectDefault,
			String resultProfile, Boolean bCustomRules,
			UID nuclet, SearchFilterUserVO searchFilterUserVO,
			Date dCreated, String sCreated, Date dChanged, String sChanged, Integer iVersion) {
		super(uid, dCreated, sCreated, dChanged, sChanged, iVersion);

		setFilterName(sFilterName);
		setDescription(sDescription);
		setFilterPrefs(sFilterPrefs);
		setOwner(sOwner);
		setEntity(entity);
		setSearchDeleted(iSearchDeleted);
		setLabelResourceId(labelResourceId);
		setDescriptionResourceId(descriptionResourceId);
		setOrder(iOrder);
		setFastSelectInResult(bFastSelectInResult);
		setFastSelectIcon(fastSelectIcon);
		setFastSelectNuclosIcon(sFastSelectNuclosIcon);
		setFastSelectDefault(bFastSelectDefault);
		setSearchFilterUser(searchFilterUserVO);
		setNuclet(nuclet);
		setResultProfile(resultProfile);
		setCustomRules(bCustomRules);
	}

	public SearchFilterVO(UID uid, int version, SearchFilterVO searchFilterVO) {
		this(uid, searchFilterVO.getFilterName(), searchFilterVO.getDescription(), 
				searchFilterVO.getFilterPrefs(),
				searchFilterVO.getOwner(), searchFilterVO.getEntity(), searchFilterVO.getSearchDeleted(), 
				searchFilterVO.getLabelResourceId(), searchFilterVO.getDescriptionResourceId(),
				searchFilterVO.getOrder(), searchFilterVO.getFastSelectInResult(), 
				searchFilterVO.getFastSelectIcon(), searchFilterVO.getFastSelectNuclosIcon(), 
				searchFilterVO.getFastSelectDefault(),
				searchFilterVO.getResultProfile(), searchFilterVO.isCustomRules(),
				searchFilterVO.getNuclet(),
				searchFilterVO.getSearchFilterUser(), searchFilterVO.getChangedAt(), searchFilterVO.getCreatedBy(), searchFilterVO.getChangedAt(),
				searchFilterVO.getCreatedBy(), searchFilterVO.getVersion());
		if (uid == null) {
			final SearchFilterUserVO u = searchFilterVO.getSearchFilterUser();
			setVersion(0);
			if (u != null) {
				setSearchFilterUser(new SearchFilterUserVO(
						null, null, u.getUser(), u.isEditable(), u.isCompulsory(), 
						u.getValidFrom(), u.getValidUntil(), u.getChangedAt(), u.getCreatedBy(), 
						u.getChangedAt(), u.getChangedBy(), 0));
			}
		}
		if (getSearchFilterUser() == null) {
			setSearchFilterUser(new SearchFilterUserVO());
		}
	}

	public void setFilterName(String sFilterName) {
		this.sFilterName = sFilterName;
	}

	public String getFilterName() {
		return this.sFilterName;
	}

	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}

	public String getDescription() {
		return this.sDescription;
	}

	public void setFilterPrefs(String sFilterPrefs) {
		this.sFilterPrefs = sFilterPrefs;
	}

	public String getFilterPrefs() {
		return this.sFilterPrefs;
	}

	public void setOwner(String sOwner) {
		this.sOwner = sOwner;
	}

	public String getOwner() {
		return this.sOwner;
	}

	public void setEntity(UID entity) {
		this.entity = entity;
	}

	public UID getEntity() {
		return this.entity;
	}

	public void setSearchDeleted(Integer iSearchDeleted) {
		if (iSearchDeleted == null) {
			iSearchDeleted = CollectableGenericObjectSearchExpression.SEARCH_UNDELETED;
		}

		this.iSearchDeleted = iSearchDeleted;
	}

	public Integer getSearchDeleted() {
		return this.iSearchDeleted;
	}

	public void setSearchFilterUser(SearchFilterUserVO searchFilterUserVO) {
		this.searchFilterUserVO = searchFilterUserVO;
	}

	public SearchFilterUserVO getSearchFilterUser() {
		return this.searchFilterUserVO;
	}

	public void setEditable(Boolean bEditable) {
		this.getSearchFilterUser().setEditable(bEditable);
	}

	public boolean isEditable() {
		return Boolean.TRUE.equals(this.getSearchFilterUser().isEditable());
	}

	public void setValidFrom(Date dValidFrom) {
		this.getSearchFilterUser().setValidFrom(dValidFrom);
	}

	public Date getValidFrom() {
		return this.getSearchFilterUser().getValidFrom();
	}

	public void setValidUntil(Date dValidUntil) {
		this.getSearchFilterUser().setValidUntil(dValidUntil);
	}

	public Date getValidUntil() {
		return this.getSearchFilterUser().getValidUntil();
	}

	public String getLabelResourceId() {
		return labelResourceId;
	}

	public void setLabelResourceId(String labelResourceId) {
		this.labelResourceId = labelResourceId;
	}

	public String getDescriptionResourceId() {
		return descriptionResourceId;
	}

	public void setDescriptionResourceId(String descriptionResourceId) {
		this.descriptionResourceId = descriptionResourceId;
	}

	public Integer getOrder() {
		return iOrder;
	}

	public void setOrder(Integer iOrder) {
		this.iOrder = iOrder;
	}

	public Boolean getFastSelectInResult() {
		return bFastSelectInResult;
	}

	public void setFastSelectInResult(Boolean bFastSelectInResult) {
		this.bFastSelectInResult = bFastSelectInResult;
	}

	public UID getFastSelectIcon() {
		return fastSelectIcon;
	}

	public void setFastSelectIcon(UID fastSelectIcon) {
		this.fastSelectIcon = fastSelectIcon;
	}

	public String getFastSelectNuclosIcon() {
		return sFastSelectNuclosIcon;
	}

	public void setFastSelectNuclosIcon(String sFastSelectNuclosIcon) {
		this.sFastSelectNuclosIcon = sFastSelectNuclosIcon;
	}

	public Boolean getFastSelectDefault() {
		return bFastSelectDefault;
	}

	public void setFastSelectDefault(Boolean bFastSelectDefault) {
		this.bFastSelectDefault = bFastSelectDefault;
	}
	
	public UID getNuclet() {
		return this.nuclet;
	}

	public void setResultProfile(String resultProfile) {
		this.resultProfile = resultProfile;
	}

	public String getResultProfile() {
		return resultProfile;
	}

	public void setCustomRules(Boolean bCustomRules) {
		this.bCustomRules = bCustomRules;
	}

	public Boolean isCustomRules() {
		return bCustomRules;
	}

	public void setNuclet(UID nuclet) {
		this.nuclet = nuclet;
	}

	/**
	 * transforms a MasterDataVO into a SearchFilterVO
	 * @param mdVO_searchfilter
	 * @param mdVO_searchFilteruser
	 * @return SearchFilterVO
	 */
	public static SearchFilterVO transformToSearchFilter(MasterDataVO<UID> mdVO_searchfilter, MasterDataVO<UID> mdVO_searchFilteruser) {
		SearchFilterUserVO searchFilerUserVO = SearchFilterUserVO.transformToSearchFilterUser(mdVO_searchFilteruser);

		SearchFilterVO searchFilter = new SearchFilterVO(
				mdVO_searchfilter.getPrimaryKey(),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.name),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.description),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.clbsearchfilter),
				mdVO_searchfilter.getCreatedBy(),
				mdVO_searchfilter.getFieldUid(E.SEARCHFILTER.entity),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.searchDeleted),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.labelres),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.descriptionres),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.order),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.fastSelectInResult),
				mdVO_searchfilter.getFieldUid(E.SEARCHFILTER.fastSelectIcon),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.fastSelectNuclosIcon),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.fastSelectDefault),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.resultProfile),
				mdVO_searchfilter.getFieldValue(E.SEARCHFILTER.customRules),
				mdVO_searchfilter.getFieldUid(E.SEARCHFILTER.nuclet),
				searchFilerUserVO,
				mdVO_searchfilter.getCreatedAt(),
				mdVO_searchfilter.getCreatedBy(),
				mdVO_searchfilter.getChangedAt(),
				mdVO_searchfilter.getChangedBy(),
				mdVO_searchfilter.getVersion());

		return searchFilter;
	}

	/**
	 * transforms a SearchFilterVO into a MasterDataVO
	 * @param searchFilter
	 * @return MasterDataVO
	 */
	public static MasterDataVO<UID> transformToMasterData(SearchFilterVO searchFilter) {
		final MasterDataVO<UID> mdVO = new MasterDataVO<UID>(
				E.SEARCHFILTER.getUID(),
				searchFilter.getId(),
				searchFilter.getCreatedAt(),
				searchFilter.getCreatedBy(),
				searchFilter.getChangedAt(),
				searchFilter.getChangedBy(),
				searchFilter.getVersion()
		);
		mdVO.setFieldValue(E.SEARCHFILTER.resultProfile, searchFilter.getResultProfile());
		mdVO.setFieldValue(E.SEARCHFILTER.name, searchFilter.getFilterName());
		mdVO.setFieldValue(E.SEARCHFILTER.description, searchFilter.getDescription());
		mdVO.setFieldValue(E.SEARCHFILTER.labelres, searchFilter.getLabelResourceId());
		mdVO.setFieldValue(E.SEARCHFILTER.descriptionres, searchFilter.getDescriptionResourceId());
		mdVO.setFieldValue(E.SEARCHFILTER.clbsearchfilter, searchFilter.getFilterPrefs());
		mdVO.setFieldUid(E.SEARCHFILTER.entity, searchFilter.getEntity());
		mdVO.setFieldValue(E.SEARCHFILTER.searchDeleted, searchFilter.getSearchDeleted());
		mdVO.setFieldValue(E.SEARCHFILTER.order, searchFilter.getOrder());
		mdVO.setFieldValue(E.SEARCHFILTER.fastSelectInResult, searchFilter.getFastSelectInResult());
		mdVO.setFieldUid(E.SEARCHFILTER.fastSelectIcon, searchFilter.getFastSelectIcon());
		mdVO.setFieldValue(E.SEARCHFILTER.fastSelectNuclosIcon, searchFilter.getFastSelectNuclosIcon());
		mdVO.setFieldValue(E.SEARCHFILTER.fastSelectDefault, searchFilter.getFastSelectDefault());
		mdVO.setFieldUid(E.SEARCHFILTER.nuclet, searchFilter.getNuclet());
		
		return mdVO;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("uid=").append(getId());
		result.append(",name=").append(getFilterName());
		result.append(",entity=").append(getEntity());
		result.append(",owner=").append(getOwner());
		result.append(",resultProfile=").append(getResultProfile());
		result.append("]");
		return result.toString();
	}

	
	@Override
	public int compareTo(SearchFilterVO o) {
		SearchFilterVO vo1 = this;
		SearchFilterVO vo2 = (SearchFilterVO)o;
		final Integer order1 = vo1.getOrder();
		final Integer order2 = vo2.getOrder();
		if (order1 != null && order2 == null) {
			return -1;
		}
		if (order1 == null && order2 != null) {
			return 1;
		}
		if (order1 != null && order2 != null) {
			return LangUtils.compare(order1, order2);
		}
		return LangUtils.compare(vo1.getFilterName(), vo2.getFilterName());
	};
	
}
