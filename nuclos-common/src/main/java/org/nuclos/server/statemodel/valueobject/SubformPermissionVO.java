//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Value object representing a subform permission.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version	01.00.00
 */
public class SubformPermissionVO extends NuclosValueObject<UID> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7117512696736446620L;
	private UID subformUid;
	private final UID roleUid;
	private final UID stateUid;
	private boolean create;
	private boolean delete;
	
	private Set<SubformGroupPermissionVO> groupPermissions;

	/**
	 * constructor
	 * @param subformUid uid
	 * @param roleUid uid
	 * @param stateUid uid 
	 * @param groupPermissions is attribute group writable?
	 */
	public SubformPermissionVO(UID subformUid, UID roleUid,
			UID stateUid, boolean create, boolean delete, Set<SubformGroupPermissionVO> groupPermissions) {
		super();
		this.subformUid = subformUid;
		this.roleUid = roleUid;
		this.stateUid = stateUid;
		this.create = create;
		this.delete = delete;
		setGroupPermissions(groupPermissions);
	}

	/**
	 * constructor
	 * @param evo contains the common fields
	 * @param subform uId
	 * @param role uid 
	 * @param state uid 
	 * @param groupPermissions is attribute group writable?
	 */
	public SubformPermissionVO(NuclosValueObject<UID> evo, UID subform, UID role, 
			UID state, boolean create, boolean delete, Set<SubformGroupPermissionVO> groupPermissions) {
		super(evo);
		this.subformUid = subform;
		this.roleUid = role;
		this.stateUid = state;
		this.create = create;
		this.delete = delete;
		setGroupPermissions(groupPermissions);
	}
	
	/**
     * @return the GroupPermissions
     */
    public Set<SubformGroupPermissionVO> getGroupPermissions() {
    	if (groupPermissions == null) {
    		setGroupPermissions(null);
		}
    	return Collections.unmodifiableSet(groupPermissions);
    }

	/**
     * @param groupPermissions the GroupPermissions to set
     */
    public void setGroupPermissions(Set<SubformGroupPermissionVO> groupPermissions) {

    	if (this.groupPermissions == null) {
    		this.groupPermissions = new HashSet<SubformGroupPermissionVO>();
		}
		this.groupPermissions.clear();
    	if (groupPermissions != null) {
    		this.groupPermissions.addAll(groupPermissions);
        	setIdInGroupPermissions();
    	}
    }
    
    private void setIdInGroupPermissions() {
    	for (SubformGroupPermissionVO scp : groupPermissions) {
    		scp.setRoleSubform(getId());
    	}
    }

	/**
	 * get subform uid
	 * @return subform uid
	 */
	public UID getSubform() {
		return subformUid;
	}

	/**
	 * set subform uid
	 * @param subform uid
	 */
	public void setSubform(UID subform) {
		this.subformUid = subform;
	}

	/**
	 * get role uid
	 * @return role uid
	 */
	public UID getRole() {
		return roleUid;
	}

	/**
	 * get state uid
	 * @return state uid
	 */
	public UID getState() {
		return stateUid;
	}

	/**
	 * get create flag
	 * @return create flag
	 */
	public boolean canCreate() {
		return create;
	}

	/**
	 * get create flag
	 * @return create flag
	 */
	public boolean canDelete() {
		return delete;
	}

	public void setCreate(boolean b) {
		this.create = b;
	}

	public void setDelete(boolean b) {
		this.delete = b;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("uid=").append(getId());
		result.append(",role=").append(getRole());
		result.append(",state=").append(getState());
		result.append(",subform=").append(getSubform());
		result.append("]");
		return result.toString();
	}

}

