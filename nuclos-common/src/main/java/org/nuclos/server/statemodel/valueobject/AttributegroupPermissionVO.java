//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Value object representing a attribute group permission.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.00
 */
public class AttributegroupPermissionVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7823100880970260628L;
	private UID attributegroupUID;
	private UID roleUID;
	private UID stateUID;
	private boolean writeable;

	/**
	 * constructor
	 * @param attributegroupUID of attribute group
	 * @param roleUID of the role entity
	 * @param stateUID of state entity
	 * @param writeable is attribute group writable?
	 */
	public AttributegroupPermissionVO(UID attributegroupUID, UID roleUID, UID stateUID, boolean writeable) {
		super();
		this.attributegroupUID = attributegroupUID;
		this.roleUID = roleUID;
		this.stateUID = stateUID;
		this.writeable = writeable;
	}

	/**
	 * constructor
	 * @param evo contains the common fields
	 * @param attributegroupUID name of attribute group
	 * @param roleUID name of the role entity
	 * @param stateUID name of the state entity
	 * @param writeable is attribute group writable?
	 */
	public AttributegroupPermissionVO(NuclosValueObject<UID> evo, UID attributegroupUID, UID roleUID, UID stateUID, boolean writeable) {
		super(evo);
		this.attributegroupUID = attributegroupUID;
		this.roleUID = roleUID;
		this.stateUID = stateUID;
		this.writeable = writeable;
	}

	/**
	 * get attribute group uid
	 * @return attribute group uid
	 */
	public UID getAttributegroupUID() {
		return attributegroupUID;
	}

	/**
	 * set attribute group uid
	 * @param attributegroupUID attribute group uid
	 */
	public void setAttributegroupUID(UID attributegroupUID) {
		this.attributegroupUID = attributegroupUID;
	}
	
	/**
	 * get role entity uid
	 * @return role entity uid
	 */
	public UID getRoleUID() {
		return roleUID;
	}

	/**
	 * set role entity uid
	 * @param roleUID entity uid
	 */
	public void setRoleUID(UID roleUID) {
		this.roleUID = roleUID;
	}

	/**
	 * get state entity uid
	 * @return state entity uid
	 */
	public UID getStateUID() {
		return stateUID;
	}

	/**
	 * set state entity uid
	 * @param stateUID entity uid
	 */
	public void setStateUID(UID stateUID) {
		this.stateUID = stateUID;
	}

	/**
	 * get writeable flag
	 * @return writeable flag
	 */
	public boolean isWritable() {
		return writeable;
	}

	/**
	 * set writable flag
	 * @param writeable writeable flag
	 */
	public void setWritable(boolean writeable) {
		this.writeable = writeable;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("id=").append(getId());
		result.append(",roleId=").append(getRoleUID());
		result.append(",stateId=").append(getStateUID());
		result.append(",agroupId=").append(getAttributegroupUID());
		result.append("]");
		return result.toString();
	}

}
