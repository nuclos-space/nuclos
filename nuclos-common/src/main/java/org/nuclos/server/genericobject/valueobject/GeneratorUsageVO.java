//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.valueobject;

import java.io.Serializable;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

/**
 * Value object representing a leased object generator action usage.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:uwe.allner@novabit.de">uwe.allner</a>
 * @version 01.00.00
 */
public class GeneratorUsageVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8298391866404857643L;
	private final UID state;
	private final UID process;

	public GeneratorUsageVO(UID state, UID process) {
		this.state = state;
		this.process = process;
	}

	public UID getState() {
		return state;
	}

	public UID getProcess() {
		return process;
	}

	/**
	 * Tests the validity for the given usage. NULL state or process of the current object are wildcards and will be ignored for validity.
	 * See http://support.nuclos.de/browse/NUCLOS-5502
	 */
	public boolean test(final GeneratorUsageVO usage2Test) {
		boolean stateResult = false;
		boolean processResult = false;
		if (this.state != null) {
			stateResult = RigidUtils.equal(this.state, usage2Test.state);
		} else {
			stateResult = true;
		}
		if (this.process != null) {
			processResult = RigidUtils.equal(this.process, usage2Test.process);
		} else {
			processResult = true;
		}
		return stateResult && processResult;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GeneratorUsageVO) {
			GeneratorUsageVO that = (GeneratorUsageVO) obj;
			return LangUtils.equal(state, that.state) && LangUtils.equal(process, that.process);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(state, process);
	}
	
	@Override
	public String toString() {
		return "state=" + state + ", process=" + process;
	}

}	// class GeneratorUsageVO
