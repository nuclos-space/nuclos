//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.Collection;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorVO;

// @Remote
public interface GeneratorFacadeRemote {

	/**
	 * @return all generator actions
	 */
	@RolesAllowed("Login")
	GeneratorVO getGeneratorActions() throws CommonPermissionException;

    @RolesAllowed("Login")
    <PK> Map<String, Collection<EntityObjectVO<PK>>> groupObjects(Collection<PK> sourceIds, GeneratorActionVO generatoractionvo) throws CommonPermissionException;
    
	/**
	 * generate one or more generic objects from an existing generic object (copying selected attributes and subforms)
	 * 
	 * §nucleus.permission mayWrite(generatoractionvo.getTargetModuleId())
	 *
	 * @return id of generated generic object (if exactly one object was generated)
	 */
	@RolesAllowed("Login")
	<PK> GenerationResult<PK> generateGenericObjects(
			GenerationContext<PK> context
	) throws CommonBusinessException;

}
