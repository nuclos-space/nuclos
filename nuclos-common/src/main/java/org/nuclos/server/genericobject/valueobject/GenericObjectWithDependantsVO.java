//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.valueobject;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * A leased object, along with its dependants.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public class GenericObjectWithDependantsVO extends GenericObjectVO 
//TODO MULTINUCLET: Enable for backwards compatibility after refactoring...
// implements IGenericObjectVODeprecated
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6447622622669442819L;
	/**
	 * the dependants of this object.
	 */
	private IDependentDataMap mpDependents;
	
	/**
	 * §precondition permission != null
	 */
	public GenericObjectWithDependantsVO(NuclosValueObject<Long> nvo, UID module, boolean bDeleted, IDependentDataMap mpDependents, IDataLanguageMap mpDataLangs) {
		super(nvo, module, bDeleted);
		
		this.mpDependents = mpDependents;
		setDataLanguageMap(mpDataLangs);
	}

	/**
	 * @param govo the leased object itself (not the parent).
	 * @param mpDependents
	 */
	public GenericObjectWithDependantsVO(GenericObjectVO govo, IDependentDataMap mpDependents, IDataLanguageMap mpDataLangs) {
		super(govo);

		this.mpDependents = mpDependents;
		setDataLanguageMap(mpDataLangs);
	}

	public IDependentDataMap getDependents() {
		return this.mpDependents;
	}

	public void setDependents(IDependentDataMap mpDependents) {
		this.mpDependents = mpDependents;
	}

	public String toDescription() {
		final StringBuilder result = new StringBuilder();
		result.append("GowdVO[id=").append(getId());
		result.append(",module=").append(getModule());
		if (isDeleted()) {
			result.append(",deleted=").append(isDeleted());
		}
		if (mpDependents != null) {
			result.append(",deps=").append(mpDependents);
		}
		result.append(",fields=").append(getAttributes());
		result.append("]");
		return result.toString();
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("GenericObjectWithDependantsVO[");
		result.append("id=").append(getId());
		result.append(",moduleId=").append(getModule());
		result.append(",status=").append(getStatus());
		result.append(",dependents=").append(mpDependents);
		result.append("]");
		return result.toString();
	}
	
}	// class GenericObjectWithDependantsVO
