//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.attribute.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.UID;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.attribute.valueobject.LayoutVO;

// @Remote
public interface LayoutFacadeRemote {

	/**
	 * imports the given layouts, adding new and overwriting existing layouts. The other existing layouts are untouched.
	 * Currently, only the layoutml description is imported, not the usages.
	 * @param colllayoutvo
	 */
	@RolesAllowed("UseManagementConsole")
	void importLayouts(Collection<LayoutVO> colllayoutvo) throws CommonBusinessException;

	/**
	 * refreshes the module attribute relation table and all generic object views (console function)
	 */
	@RolesAllowed("UseManagementConsole")
	void refreshAll();
		
	/**
	 * Get all layouts that are connected to the given nucletId
	 * 
	 * @param nuclet
	 * @return
	 */
	@RolesAllowed("Login")
	List<LayoutVO> getMasterDataLayoutForNuclet(UID nuclet);
	
	/**
	 * Get all layouts that are connected to the given nucletId
	 */
	@RolesAllowed("Login")
	List<LayoutVO> getMasterDataLayout();
	
	Set<UID> getAllLayoutUidsForEntity(UID entityUid);
	
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesByLayoutId(UID layoutUID);
	
}
