//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.valueobject;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Value object representing a role transision
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christian.niehues@novabit.de">christian.niehues</a>
 * @version 00.01.000
 */
public class RoleTransitionVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6142151479280343955L;
	private UID role;
	private UID transition;

	/**
	 * constructor to be called by server only
	 */
	public RoleTransitionVO(UID id, UID transition, UID role
			, java.util.Date createdAt, String createdBy, java.util.Date changedAt, String changedBy, Integer version) {
		super(id, createdAt, createdBy, changedAt, changedBy, version);

		this.transition = transition;
		this.role = role;
	}

	public RoleTransitionVO(NuclosValueObject<UID> evo, UID transition, UID role) {
		super(evo);
		this.transition = transition;
		this.role = role;
	}

	@Override
	public String toString() {
		return " ID: " + this.getId() + " Transition Id: " + this.getTransition()
				+ " Role Id: " + this.getRole();
	}

	public UID getTransition() {
		return transition;
	}

	protected void setTransition(UID generation) {
		transition = generation;
	}

	public UID getRole() {
		return role;
	}

	protected void setRole(UID role) {
		this.role = role;
	}

}
