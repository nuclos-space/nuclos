package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class TitledBorder extends CompBorder {
	private final String title;
	
	public TitledBorder(Attributes attributes) {
		this.title = attributes.getValue(ATTRIBUTE_TITLE);
	}
	
	public String getTitle() {
		return title;
	}
	
	@Override
	public String getType() {
		return "titled";
	}

	@Override
	public String toString() {
		return "Title:" + title;
	}

}
