package org.nuclos.common2.layoutml;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.xml.sax.Attributes;

public class WebValueListProvider implements LayoutMLConstants {
	private final String type;
	private final UID value;
	private final Map<String, Object> mpParams;
	
	public WebValueListProvider(Attributes attributes) {
		this(attributes.getValue(ATTRIBUTE_TYPE), attributes.getValue(ATTRIBUTE_VALUE));
	}
	
	public WebValueListProvider(String type, String value) {
		this.type = type;
		this.value = UID.parseUID(value);
		this.mpParams = new HashMap<String, Object>();
		// NUCLOS-4300
		this.mpParams.put("searchmode", 0);
	}

	public String getType() {
		return type;
	}

	public UID getValue() {
		return value;
	}
	
	public void addParameter(Attributes attributes) {
		String sName = attributes.getValue(ATTRIBUTE_NAME);
		String sValue = attributes.getValue(ATTRIBUTE_VALUE);
		addParameter(sName, sValue);
	}
	
	public void addParameter(String key, String value) {
		if (ValuelistProviderVO.DATASOURCE_SEARCHMODE.equals(key)
				&& (Boolean.TRUE.toString().equals(value.toLowerCase())
					|| Boolean.FALSE.toString().equals(value.toLowerCase()))) {
			value = String.valueOf(Boolean.valueOf(value) ? 1 : 0);
		}
		mpParams.put(key, value);
	}

	public Map<String, Object> getParameters() {
		return new HashMap<String, Object>(mpParams);
	}
	
	public boolean hasDefaultMarker() {
		return mpParams.containsKey(ValuelistProviderVO.DATASOURCE_DEFAULTMARKERFIELD);
	}
	
	public boolean hasDatasourceParameters() {
		for (String key : mpParams.keySet()) {
			if (
				key.equals(ValuelistProviderVO.DATASOURCE_IDFIELD) ||
				key.equals(ValuelistProviderVO.DATASOURCE_NAMEFIELD) ||
				key.equals(ValuelistProviderVO.DATASOURCE_DEFAULTMARKERFIELD) ||
				key.equals(ValuelistProviderVO.DATASOURCE_VALUELISTPROVIDER) ||
				key.equals(ValuelistProviderVO.DATASOURCE_SEARCHMODE)
					) {
				continue;
			} else {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Type=" + type + " Value=" + value + " MpParams=" + mpParams;
	}
}
