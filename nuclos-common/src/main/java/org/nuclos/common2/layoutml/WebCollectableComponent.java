package org.nuclos.common2.layoutml;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public class WebCollectableComponent extends AbstractWebComponent implements ISupportsValueListProvider {
	private final UID name;
	private final String showonly;
	
	private final boolean showLabel;
	private final boolean showControl;
	private final boolean isVisible;
	private final String controltype;
	private final String controltypeclass;
	private WebValueListProvider valueListProvider;
	
	WebCollectableComponent(Attributes attributes) {
		super(attributes);
		name = UID.parseUID(attributes.getValue(ATTRIBUTE_NAME));
		isVisible = !ATTRIBUTEVALUE_NO.equals(attributes.getValue(ATTRIBUTE_VISIBLE));
		showonly = attributes.getValue(ATTRIBUTE_SHOWONLY);
		showLabel = showonly == null || 
				(!showonly.equals(ATTRIBUTEVALUE_CONTROL) && !showonly.equals(ATTRIBUTEVALUE_BROWSEBUTTON));
		showControl = showonly == null || !showonly.equals(ATTRIBUTEVALUE_LABEL);
		
		controltype = attributes.getValue(ATTRIBUTE_CONTROLTYPE);
		controltypeclass = attributes.getValue(ATTRIBUTE_CONTROLTYPECLASS);
	}
	
	public UID getName() {
		return name;
	}
	
	@Override
	public UID getUID() {
		return name;
	}

	public String getControlType() {
		return controltype;
	}
	
	@Override
	public boolean isVisible() {
		return isVisible;
	}
	
	@Override
	public boolean isMemo() {
		return CONTROLTYPE_TEXTAREA.equals(controltype);
	}
	
	@Override
	public boolean isJustLabel() {
		return showLabel && !showControl;
	}
	
	public boolean showLabel() {
		return showLabel;
	}
	
	public boolean showControl() {
		return showControl;
	}
	
	public String getControlTypeClass() {
		return controltypeclass;
	}

	@Override
	public WebValueListProvider getValueListProvider() {
		return valueListProvider;
	}

	@Override
	public void setValueListProvider(WebValueListProvider valueListProvider) {
		this.valueListProvider = valueListProvider;
	}
	
}