package org.nuclos.common2.layout;

import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.EntityAndField;

/**
 * A unified interface for working with layouts.
 * 
 * TODO: import/output stuff
 * 
 * @author Thomas Pasch
 */
public interface ILayoutCache2 {
	
	boolean hasLayout(UID entityUID);
	
	UID getMasterDataLayoutId(UID entityUID, String customUsage, boolean searchMode);

	UID getGenericObjectLayoutId(UsageCriteria usage, String customUsage, boolean searchMode);
	
	String getLayoutXml(UID layoutId);
	
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesById(UID layoutId);
	
}
