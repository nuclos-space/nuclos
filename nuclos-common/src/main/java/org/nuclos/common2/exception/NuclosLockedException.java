package org.nuclos.common2.exception;

import org.springframework.security.authentication.LockedException;

/**
 * Created by Oliver Brausch on 02.05.19.
 */
public class NuclosLockedException extends LockedException {
	public NuclosLockedException(String msg) {
		super(msg);
		NuclosExceptions.shortenStackTrace(this, 1);
	}
}
