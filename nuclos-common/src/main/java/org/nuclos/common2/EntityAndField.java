//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;

/**
 * Holds an entity name and a field name.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * §todo add assertions
 * §todo define equals/hashCode!
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public class EntityAndField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5182102324578179145L;
	private final UID entity;
	private final UID field;
	private Map<String, Object> mpParams;

	public EntityAndField(EntityMeta<?> entity, FieldMeta<?> field) {
		this(entity.getUID(), field.getUID());
	}
	
	public EntityAndField(UID entity, UID field) {
		if (field == null) {
			throw new IllegalArgumentException("field must not be null");
		}
		this.entity = entity;
		this.field = field;
	}

	/**
	 * @return the subform's entity name
	 */
	public UID getEntity() {
		return this.entity;
	}

	/**
	 * @return name of the foreign key field to the subform's parent entity.
	 */
	public UID getField() {
		return this.field;
	}
	
	/**
	 * @return dependent key of the foreign key field to the subform's parent entity.
	 */
	public IDependentKey getDependentKey() {
		return DependentDataMap.createDependentKey(this.field);
	}

	/**
	 * @return param map to the subform's parent entity. May be <code>null</code>.
	 */
	public Map<String, Object> getMapParams() {
		return this.mpParams;
	}

	/**
	 * @param mpParams map to the subform's parent entity. May be <code>null</code>.
	 */
	public void setMapParams(Map<String, Object> mpParams) {
		this.mpParams = mpParams;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("EntityAndField:").append(entity);
		result.append(":").append(field);
		return result.toString();
	}

	public static class GetEntityUID implements Transformer<EntityAndField, UID> {
		@Override
		public UID transform(EntityAndField o) {
			return o.getEntity();
		}
	}

}  // class EntityAndField
