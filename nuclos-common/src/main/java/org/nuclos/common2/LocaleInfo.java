//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.UID;


public class LocaleInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8386270475945164271L;
	/** The standardized (IETF) "i-default" language tag which represents our null locale */
	public static LocaleInfo I_DEFAULT = new LocaleInfo("Null", null, null, "i-default", null);
	public static final String I_DEFAULT_TAG = "i-default";

	public static Comparator<LocaleInfo> DESCRIPTION_COMPARATOR = DescriptionComparator.INSTANCE;

	private final String name;
	private final String title;
	private final UID locale;
	private final String language;
	private final String country;

	public LocaleInfo(String name, String title, UID locale, String language, String country) {
		this.name = name;
		this.language = language;
		this.country  = country;
		this.locale = locale;
		this.title = title;
	}

	public String getTag() {
		if (language == null) {
			return I_DEFAULT_TAG;
		} else if (country == null) {
			return language;
		} else {
			return language + "-" + country;
		}
	}

	public Locale toLocale() {
		if (language == null) {
			return null;
		} else {
			return new Locale(language, country != null ? country : "");
		}
	}

	@Override
	public int hashCode() {
		return LangUtils.hashCode(language) ^ LangUtils.hashCode(country);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LocaleInfo) {
			LocaleInfo that = (LocaleInfo) obj;
			return LangUtils.equal(language, that.language) && LangUtils.equal(country, that.country);
		}
		return false;
	}

	@Override
	public String toString() {
		return (title != null) ? title : getTag();
	}

	/**
	 * @deprecated
	 */
	public static LocaleInfo parseTag(String tag) {
		if (tag == null || tag.isEmpty())
			return LocaleInfo.I_DEFAULT;
		int i = tag.indexOf('-');
		if (i < 0) {
			i = tag.indexOf('_');
		}
		if (i == -1) {
			return new LocaleInfo(null, null, null, tag, null);
		} else {
			return new LocaleInfo(null, null, null, tag.substring(0, i), tag.substring(i+1));
		}
	}

	public static LocaleInfo parseTag(Locale tag) {
		if (tag == null)
			return LocaleInfo.I_DEFAULT;
		final String lang = tag.getLanguage();
		final String count = tag.getCountry();
		if (StringUtils.isBlank(count)) {
			return new LocaleInfo(null, null, null, lang, null);
		} else {
			return new LocaleInfo(null, null, null, lang, count);
		}
	}

	public static String getStandardParentTag(String tag) {
		LocaleInfo li = LocaleInfo.parseTag(tag);
		if (li != null && li.country != null) {
			return li.language;
		}
		else {
			return null;
		}
	}
	
	public String cacheKey(String resourceId) {
		final StringBuilder result = new StringBuilder();
		result.append(locale).append('_').append(language).append('_').append(country);
		result.append(':').append(resourceId);
		return result.toString();
	}

	private static enum DescriptionComparator implements Comparator<LocaleInfo> {
		INSTANCE;

		@Override
		public int compare(LocaleInfo o1, LocaleInfo o2) {
			return o1.toString().compareTo(o2.toString());
		}
	}

	public String getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public UID getLocale() {
		return locale;
	}

	public String getLanguage() {
		return language;
	}

	public String getCountry() {
		return country;
	}
}
