package org.nuclos.common2.searchfilter;

import java.util.List;

import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

public class EntitySearchFilter2 {
	
	private final SearchFilterVO searchFilterVO;

	private final CollectableSearchExpression searchExpr;
	
	/**
	 * list of visible columns
	 */
	private final List<? extends CollectableEntityField> visibleFields;
	
	private boolean valid = true;

	public EntitySearchFilter2(SearchFilterVO searchFilterVO, CollectableSearchExpression searchExpr,
			List<? extends CollectableEntityField> visibleFields) {
		
		this.searchFilterVO = searchFilterVO;
		this.searchExpr = searchExpr;
		this.visibleFields = visibleFields;
	}

	public boolean isValid() {
		return valid;
	}
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public List<? extends CollectableEntityField> getVisibleFields() {
		return visibleFields;
	}
	
	public CollectableSearchExpression getSearchExpression() {
		return searchExpr;
	}
	
	public SearchFilterVO getVO() {
		return searchFilterVO;
	}

}
