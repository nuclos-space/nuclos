package org.nuclos.common.file;

import static org.nuclos.common.file.FileNameMapper.escape;
import static org.nuclos.common.file.FileNameMapper.mustBeEscaped;
import static org.nuclos.common.file.FileNameMapper.unescape;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class FileNameMapperTest {

	@Test
	public void testEscape() {
		List<Character> forbiddenChars = getForbiddenChars();

		for (char c : forbiddenChars) {
			String escaped = escape("" + c);
			assert !escaped.contains("" + c);
			assertEscapeIsReversible("" + c);
			assertEscaped(escaped);
		}

		for (int i = 0; i < 10000; i++) {
			assertEscaped(escape("" + (char) i));
		}
	}

	@Test
	public void testUnescape() {
		assert FileNameMapper.unescape("%3C %3e").equals("< >");
	}

	@Test
	public void testEscapeUnescape() {
		assertEscapeIsReversible("");
		assertEscapeIsReversible("ä");
		assertEscapeIsReversible("äöü 4345?=");
		assertEscapeIsReversible("\uD83D\uDE01");
		assertEscapeIsReversible("\uD83D\uDE12\uD83D\uDE12\uD83D\uDE12");
	}

	@Test
	public void testMustBeEscaped() {
		assert mustBeEscaped("ä");
		assert mustBeEscaped("ß");
		assert mustBeEscaped("!");
		assert mustBeEscaped("Test 123/");

		assert !mustBeEscaped("Test");
		assert !mustBeEscaped("Test 123");
		assert !mustBeEscaped(".test");
		assert !mustBeEscaped("123");
		assert !mustBeEscaped("a_b-c.z_A_Z 0-9");
	}

	private void assertEscaped(String s) {
		for (byte b : s.getBytes()) {
			assert b == FileNameMapper.ESCAPE_CHARACTER || FileNameMapper.isAllowedChar(b);
		}
	}

	private void assertEscapeIsReversible(final String s) {
		assert unescape(escape(s)).equals(s) : "Could not unescape escaped string: s = " + s + ", escape(s) = " + escape(s) + ", unescape(escape(s)) = " + unescape(escape(s)) + " != " + s;
	}

	private List<Character> getForbiddenChars() {
		List<Character> forbiddenChars = new ArrayList<>();

		forbiddenChars.add('<');
		forbiddenChars.add('>');
		forbiddenChars.add(':');
		forbiddenChars.add('"');
		forbiddenChars.add('/');
		forbiddenChars.add('\\');
		forbiddenChars.add('|');
		forbiddenChars.add('?');
		forbiddenChars.add('*');

		for (int i = 0; i <= 31; i++) {
			forbiddenChars.add((char) i);
		}

		return forbiddenChars;
	}

}