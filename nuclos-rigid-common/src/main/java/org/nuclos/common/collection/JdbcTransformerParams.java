package org.nuclos.common.collection;

/**
 * Created by Sebastian Debring on 4/1/2019.
 */
public class JdbcTransformerParams {

	private final boolean bIncludeThumbnailsOnly;

	public JdbcTransformerParams() {
		this.bIncludeThumbnailsOnly = false;
	}

	public JdbcTransformerParams(final boolean bIncludeThumbnailsOnly) {
		this.bIncludeThumbnailsOnly = bIncludeThumbnailsOnly;
	}

	public boolean includeThumbnailsOnly() {
		return bIncludeThumbnailsOnly;
	}
}
