//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class FieldUtils {

	public static Collection<FieldMeta<?>> getFieldsReferencing(Collection<FieldMeta<?>> fields, UID refEntityUID) {
		if (fields == null || refEntityUID == null) {
			return null;
		}
		
		Collection<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		for (FieldMeta<?> fmeta : fields) {
			if (refEntityUID.equals(fmeta.getForeignEntity())) {
				result.add(fmeta);
			}
		}
		
		return result;
	}
	
	public final static boolean hideField(UID fieldUid, Map<UID, FieldMeta<?>> parMpEntityFields, boolean bSuperUser) {
		if (bSuperUser) {
			return false;
		}
		
		FieldMeta<?> fm = parMpEntityFields.get(fieldUid);
		return fm != null && fm.isHidden();
		
	}
	
	public final static boolean isBinary(UID fieldUid, Map<UID, FieldMeta<?>> parMpEntityFields) {
		FieldMeta<?> fm = parMpEntityFields.get(fieldUid);
		return fm != null && fm.getJavaClass() == byte[].class;
	}
	
}
