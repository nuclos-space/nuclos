package org.nuclos.common;

public enum LockMode {
	// in future, may be...
	//TEAM_MANUALLY,
	//TEAM_API_ONLY,
	//TEAM_OR_OWNER_MANUALLY,
	//TEAM_OR_OWNER_API_ONLY,
	//OWNER_MANUALLY,
	OWNER_API_ONLY;

	public static boolean isOwner(LockMode lockMode) {
		if (lockMode != null) {
			return lockMode == OWNER_API_ONLY;
		}
		return false;
	}
}
