//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Helper to get {@link StreamSource} and {@link StreamResult} instances
 * that could handle String, byte[], and char[] representations.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.0.18
 */
public class SourceResultHelper {
	
	public static final StreamSource NULL_SOURCE = new StreamSource();
	
	private SourceResultHelper() {
		// Never invoked.
	}
	
	public static StreamSource newSource(String content) {
		if (content == null || "".equals(content)) {
			return null;
		}
		final StreamSource result = new StreamSource(new StringReader(content));
		return result;
	}
	
	public static StreamSource newSource(byte[] content) {
		if (content == null || content.length == 0) {
			return null;
		}
		final StreamSource result = new StreamSource(new ByteArrayInputStream(content));
		return result;
	}
	
	public static StreamSource newSource(char[] content) {
		if (content == null || content.length == 0) {
			return null;
		}
		final StreamSource result = new StreamSource(new CharArrayReader(content));
		return result;
	}
	
	public static StreamResult newResultForString() {
		final StringWriter stringWriter = new StringWriter();
		final StreamResult result = new StreamResult() {
			
			@Override
			public String toString() {
				return stringWriter.toString();
			}
			
		};
		result.setWriter(stringWriter);
		return result;
	}
	
	public static StreamResult newResultForByteArray() {
		final ByteArrayOutputStream oaos = new ByteArrayOutputStream();
		final StreamResult result = new StreamResult() {
			
			@Override
			public String toString() {
				return oaos.toString();
			}
			
		};
		result.setOutputStream(oaos);
		return result;
	}
	
	public static StreamResult newResultForCharArray() {
		final CharArrayWriter caw = new CharArrayWriter();
		final StreamResult result = new StreamResult() {
			
			@Override
			public String toString() {
				return caw.toString();
			}
			
		};
		result.setWriter(caw);
		return result;
	}
	
}
