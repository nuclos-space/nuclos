package org.nuclos.server.businesstest.codegeneration.script;

import static org.nuclos.server.businesstest.codegeneration.script.BusinessTestInsertThinScriptGeneratorTest.checkMandatoryFieldsOfA;

import java.io.IOException;

import org.junit.Test;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestInsertFullScriptGeneratorTest extends AbstractBusinessTestScriptGeneratorTest {

	@Test
	public void testGenerator() throws CommonFinderException, IOException, CommonPermissionException {
		assert "INSERT FULL A".equals(generator.getTestName());

		ScriptResult script = parseScript();

		final String groovySource = script.getGroovySource();

		assert groovySource.contains(".name = 'Sample Data'");

		checkMandatoryFieldsOfA(groovySource);

		assert groovySource.contains(".refFieldId = B.first()?.id");

		// Nullable fields
		assert groovySource.contains(".bByRefA1 = 'Sample Data'");
		assert groovySource.contains(".refUserId = ");

		// Attribute names which are not valid java identifiers (e.g. starting with a number) must be quoted
		assert groovySource.contains(".'1zahl' = ");

		assert groovySource.contains(".save()");
	}

	@Override
	protected AbstractBusinessTestScriptGenerator newGenerator() {
		return getFactory(new A()).newInsertFullScriptGenerator();
	}
}