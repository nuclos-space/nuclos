package org.nuclos.server.businesstest.sampledata;

import java.util.Arrays;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SampleDoubleDataGeneratorTest {

	@Test
	public void testDataGeneration() {
		SampleDoubleDataGenerator generator = new SampleDoubleDataGenerator();

		generator.addSampleValues(Arrays.asList(42.0));

		assert generator.newValue() == 42.0;

		generator.addSampleValues(Arrays.asList(45.0));

		for (int i=0; i<10; i++) {
			Double value = generator.newValue();
			assert value >= 42.0;
			assert value <= 45.0;
		}
	}
}