package org.nuclos.server.businesstest.codegeneration.script;

import java.io.IOException;

import org.junit.Test;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestStateChangeScriptGeneratorTest extends AbstractBusinessTestScriptGeneratorTest {
	@Test
	public void testGenerator() throws CommonFinderException, IOException, CommonPermissionException {
		assert "STATECHANGE B 10 to 20".equals(generator.getTestName());

		ScriptResult script = parseScript();

		assert script != null;
		assert script.getGroovySource().contains(".changeState(20)");
	}

	@Override
	protected AbstractBusinessTestScriptGenerator newGenerator() {
		final StateVO sourceState = new StateVO(new UID(), 10, "Status 10", "Status 10", null, null, null, new B().getUID());
		final StateVO targetState = new StateVO(new UID(), 20, "Status 20", "Status 20", null, null, null, new B().getUID());

		return getFactory(new B()).newStateChangeScriptGenerator(sourceState, targetState);
	}
}