//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_newsConfirmed
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_AD_NEWS_CONFIRMED
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class NewsConfirmed extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "6ABA", "6ABA0", org.nuclos.common.UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "6ABA", "6ABA1", java.util.Date.class);


/**
 * Attribute: news
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NewsId = 
	new ForeignKeyAttribute<>("NewsId", "org.nuclos.businessentity", "6ABA", "6ABAa", org.nuclos.common.UID.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "6ABA", "6ABA4", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "6ABA", "6ABA3", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "6ABA", "6ABA2", java.lang.String.class);


/**
 * Attribute: user
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_MD_USER
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> UserId = 
	new ForeignKeyAttribute<>("UserId", "org.nuclos.businessentity", "6ABA", "6ABAb", org.nuclos.common.UID.class);


public NewsConfirmed() {
		super("6ABA");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("6ABA");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("6ABA1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNewsId() {
		return getFieldUid("6ABAa");
}


/**
 * Setter-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNewsId(org.nuclos.common.UID pNewsId) {
		setFieldId("6ABAa", pNewsId); 
}


/**
 * Getter-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.News getNewsBO() {
		return getReferencedBO(org.nuclos.businessentity.News.class, getFieldUid("6ABAa"), "6ABAa", "1oLi");
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("6ABA4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("6ABA3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("6ABA2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: user
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_MD_USER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_user
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.UID getUserId() {
		return getFieldUid("6ABAb");
}


/**
 * Setter-Method for attribute: user
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_MD_USER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_user
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setUserId(org.nuclos.api.UID pUserId) {
		setFieldId("6ABAb", pUserId); 
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(NewsConfirmed boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public NewsConfirmed copy() {
		return super.copy(NewsConfirmed.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("6ABA"), id);
}
/**
* Static Get by Id
*/
public static NewsConfirmed get(org.nuclos.common.UID id) {
		return get(NewsConfirmed.class, id);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
 }
