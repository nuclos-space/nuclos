//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * @Deprecated For backward compatibility of rules only (via nuclos-ccce.jar bundle)
 * Use org.nuclos.system.PrintService
 *
 * BusinessObject: nuclos_printservice
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_UD_PRINTSERVICE
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@Deprecated
public class nuclosprintservice extends AbstractBusinessObject<UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<UID> Id =
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "qGYQ", "qGYQ0", UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "qGYQ", "qGYQ1", Date.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "qGYQ", "qGYQa", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final Attribute<UID> PrimaryKey =
	new Attribute<>("PrimaryKey", "org.nuclos.businessentity", "qGYQ", "qGYQ0", UID.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "qGYQ", "qGYQ3", Date.class);


/**
 * Attribute: useNativePdfSupport
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: BLNUSENATIVEPDFSUPPORT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> UseNativePdfSupport =
	new Attribute<>("UseNativePdfSupport", "org.nuclos.businessentity", "qGYQ", "qGYQc", Boolean.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "qGYQ", "qGYQ2", String.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "qGYQ", "qGYQb", String.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "qGYQ", "qGYQ4", String.class);


/**
 * Attribute: defaultTray
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRUID_DEFAULTTRAY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> DefaultTrayId =
	new ForeignKeyAttribute<>("DefaultTrayId", "org.nuclos.businessentity", "qGYQ", "qGYQd", UID.class);


public nuclosprintservice() {
		super("qGYQ");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("qGYQ");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("qGYQ1", Date.class);
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getName() {
		return getField("qGYQa", String.class);
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public org.nuclos.common.UID getPrimaryKey() {
		return getField("qGYQ0", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("qGYQ3", Date.class);
}


/**
 * Getter-Method for attribute: useNativePdfSupport
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: BLNUSENATIVEPDFSUPPORT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getUseNativePdfSupport() {
		return getField("qGYQc", Boolean.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("qGYQ2", String.class);
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getDescription() {
		return getField("qGYQb", String.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("qGYQ4", String.class);
}


/**
 * Getter-Method for attribute: defaultTray
 *<br>
 *<br>Entity: nuclos_printservice
 *<br>DB-Name: STRUID_DEFAULTTRAY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_printservicetray
 *<br>Reference field: number
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getDefaultTrayId() {
		return getFieldUid("qGYQd");
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("qGYQ"), id);
}
/**
* Static Get by Id
*/
public static nuclosprintservice get(UID id) {
		return get(nuclosprintservice.class, id);
}
 }
