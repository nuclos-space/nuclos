//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_news
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_AD_NEWS
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class News extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "1oLi", "1oLi0", org.nuclos.common.UID.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "1oLi", "1oLia", java.lang.String.class);


/**
 * Attribute: content
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: CLBCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Content = new StringAttribute<>("Content", "org.nuclos.businessentity", "1oLi", "1oLib", java.lang.String.class);


/**
 * Attribute: revision
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: INTREVISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Revision = 
	new NumericAttribute<>("Revision", "org.nuclos.businessentity", "1oLi", "1oLii", java.lang.Integer.class);


/**
 * Attribute: predecessor
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID_PREDECESSOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> PredecessorId = 
	new ForeignKeyAttribute<>("PredecessorId", "org.nuclos.businessentity", "1oLi", "1oLij", org.nuclos.common.UID.class);


/**
 * Attribute: showAtStartup
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNSHOWATSTARTUP
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> ShowAtStartup = 
	new Attribute<>("ShowAtStartup", "org.nuclos.businessentity", "1oLi", "1oLig", java.lang.Boolean.class);


/**
 * Attribute: confirmationRequired
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNCONFIRMATIONREQUIRED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> ConfirmationRequired = 
	new Attribute<>("ConfirmationRequired", "org.nuclos.businessentity", "1oLi", "1oLih", java.lang.Boolean.class);


/**
 * Attribute: validFrom
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATVALIDFROM
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ValidFrom = 
	new NumericAttribute<>("ValidFrom", "org.nuclos.businessentity", "1oLi", "1oLie", java.util.Date.class);


/**
 * Attribute: validUntil
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATVALIDUNTIL
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ValidUntil = 
	new NumericAttribute<>("ValidUntil", "org.nuclos.businessentity", "1oLi", "1oLif", java.util.Date.class);


/**
 * Attribute: active
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Active = 
	new Attribute<>("Active", "org.nuclos.businessentity", "1oLi", "1oLic", java.lang.Boolean.class);


/**
 * Attribute: title
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRTITLE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Title = new StringAttribute<>("Title", "org.nuclos.businessentity", "1oLi", "1oLid", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "1oLi", "1oLi1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "1oLi", "1oLi2", java.lang.String.class);


/**
 * Attribute: privacyPolicy
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNPRIVACYPOLICY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> PrivacyPolicy = 
	new Attribute<>("PrivacyPolicy", "org.nuclos.businessentity", "1oLi", "1oLik", java.lang.Boolean.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "1oLi", "1oLi3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "1oLi", "1oLi4", java.lang.String.class);

public static final Dependent<org.nuclos.businessentity.News> _News = 
	new Dependent<>("_News", "null", "News", "1oLi", "predecessor", "1oLij", org.nuclos.businessentity.News.class);

public static final Dependent<org.nuclos.businessentity.NewsViewed> _NewsViewed = 
	new Dependent<>("_NewsViewed", "null", "NewsViewed", "qIx7", "news", "qIx7a", org.nuclos.businessentity.NewsViewed.class);

public static final Dependent<org.nuclos.businessentity.NewsConfirmed> _NewsConfirmed = 
	new Dependent<>("_NewsConfirmed", "null", "NewsConfirmed", "6ABA", "news", "6ABAa", org.nuclos.businessentity.NewsConfirmed.class);


public News() {
		super("1oLi");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("1oLi");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getName() {
		return getField("1oLia", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("1oLia", pName); 
}


/**
 * Getter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: CLBCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.String getContent() {
		return getField("1oLib", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: CLBCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setContent(java.lang.String pContent) {
		setField("1oLib", pContent); 
}


/**
 * Getter-Method for attribute: revision
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: INTREVISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public java.lang.Integer getRevision() {
		return getField("1oLii", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: revision
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: INTREVISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setRevision(java.lang.Integer pRevision) {
		setField("1oLii", pRevision); 
}


/**
 * Getter-Method for attribute: predecessor
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID_PREDECESSOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Reference field: name vrevision
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public org.nuclos.common.UID getPredecessorId() {
		return getFieldUid("1oLij");
}


/**
 * Setter-Method for attribute: predecessor
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID_PREDECESSOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Reference field: name vrevision
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void setPredecessorId(org.nuclos.common.UID pPredecessorId) {
		setFieldId("1oLij", pPredecessorId); 
}


/**
 * Getter-Method for attribute: predecessor
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID_PREDECESSOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Reference field: name vrevision
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public org.nuclos.businessentity.News getPredecessorBO() {
		return getReferencedBO(org.nuclos.businessentity.News.class, getFieldUid("1oLij"), "1oLij", "1oLi");
}


/**
 * Getter-Method for attribute: showAtStartup
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNSHOWATSTARTUP
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getShowAtStartup() {
		return getField("1oLig", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: showAtStartup
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNSHOWATSTARTUP
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setShowAtStartup(java.lang.Boolean pShowAtStartup) {
		setField("1oLig", pShowAtStartup); 
}


/**
 * Getter-Method for attribute: confirmationRequired
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNCONFIRMATIONREQUIRED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getConfirmationRequired() {
		return getField("1oLih", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: confirmationRequired
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNCONFIRMATIONREQUIRED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setConfirmationRequired(java.lang.Boolean pConfirmationRequired) {
		setField("1oLih", pConfirmationRequired); 
}


/**
 * Getter-Method for attribute: validFrom
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATVALIDFROM
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getValidFrom() {
		return getField("1oLie", java.util.Date.class); 
}


/**
 * Setter-Method for attribute: validFrom
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATVALIDFROM
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setValidFrom(java.util.Date pValidFrom) {
		setField("1oLie", pValidFrom); 
}


/**
 * Getter-Method for attribute: validUntil
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATVALIDUNTIL
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getValidUntil() {
		return getField("1oLif", java.util.Date.class); 
}


/**
 * Setter-Method for attribute: validUntil
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATVALIDUNTIL
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setValidUntil(java.util.Date pValidUntil) {
		setField("1oLif", pValidUntil); 
}


/**
 * Getter-Method for attribute: active
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getActive() {
		return getField("1oLic", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: active
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setActive(java.lang.Boolean pActive) {
		setField("1oLic", pActive); 
}


/**
 * Getter-Method for attribute: title
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRTITLE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.String getTitle() {
		return getField("1oLid", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: title
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRTITLE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setTitle(java.lang.String pTitle) {
		setField("1oLid", pTitle); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("1oLi1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("1oLi2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: privacyPolicy
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNPRIVACYPOLICY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getPrivacyPolicy() {
		return getField("1oLik", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: privacyPolicy
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: BLNPRIVACYPOLICY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setPrivacyPolicy(java.lang.Boolean pPrivacyPolicy) {
		setField("1oLik", pPrivacyPolicy); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("1oLi3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("1oLi4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: predecessor
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID_PREDECESSOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Reference field: name vrevision
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.News> getNews(Flag... flags) {
		return getDependents(_News, flags); 
}


/**
 * Insert-Method for attribute: predecessor
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID_PREDECESSOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Reference field: name vrevision
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void insertNews(org.nuclos.businessentity.News pNews) {
		insertDependent(_News, pNews);
}


/**
 * Delete-Method for attribute: predecessor
 *<br>
 *<br>Entity: nuclos_news
 *<br>DB-Name: STRUID_PREDECESSOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Reference field: name vrevision
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void deleteNews(org.nuclos.businessentity.News pNews) {
		deleteDependent(_News, pNews);
}


/**
 * Getter-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsViewed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NewsViewed> getNewsViewed(Flag... flags) {
		return getDependents(_NewsViewed, flags); 
}


/**
 * Insert-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsViewed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNewsViewed(org.nuclos.businessentity.NewsViewed pNewsViewed) {
		insertDependent(_NewsViewed, pNewsViewed);
}


/**
 * Delete-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsViewed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNewsViewed(org.nuclos.businessentity.NewsViewed pNewsViewed) {
		deleteDependent(_NewsViewed, pNewsViewed);
}


/**
 * Getter-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NewsConfirmed> getNewsConfirmed(Flag... flags) {
		return getDependents(_NewsConfirmed, flags); 
}


/**
 * Insert-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNewsConfirmed(org.nuclos.businessentity.NewsConfirmed pNewsConfirmed) {
		insertDependent(_NewsConfirmed, pNewsConfirmed);
}


/**
 * Delete-Method for attribute: news
 *<br>
 *<br>Entity: nuclos_newsConfirmed
 *<br>DB-Name: STRUID_T_AD_NEWS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_news
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNewsConfirmed(org.nuclos.businessentity.NewsConfirmed pNewsConfirmed) {
		deleteDependent(_NewsConfirmed, pNewsConfirmed);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(News boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public News copy() {
		return super.copy(News.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("1oLi"), id);
}
/**
* Static Get by Id
*/
public static News get(org.nuclos.common.UID id) {
		return get(News.class, id);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
 }
