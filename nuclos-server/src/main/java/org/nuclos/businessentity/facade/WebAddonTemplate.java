package org.nuclos.businessentity.facade;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

import org.nuclos.api.ide.utils.WebAddonUtils;
import org.nuclos.businessentity.WebAddon;
import org.slf4j.LoggerFactory;

public class WebAddonTemplate {

	public static final String ADDON_FILE_NAME_PLACEHOLDER = "${ADDON_FILE_NAME}";
	public static final String ADDON_NAME_CAMEL_CASE_PLACEHOLDER = "${ADDON_NAME_CAMEL_CASE}";

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WebAddonFacade.class);


	/**
	 * copy addon templates and replace placeholders with addon name
	 *
	 * @param webAddon
	 * @param webclientSrc
	 * @param addonOutputPath
	 */
	public static void initializeWebAddonFiles(final WebAddon webAddon, final Path webclientSrc, final Path addonOutputPath) {

		LOG.info("Writing addon template to: " + addonOutputPath);

		try {

			if (!Files.exists(addonOutputPath)) {
				Files.createDirectory(addonOutputPath);
			}



			Files.copy(
					webclientSrc.resolve("addon-template/.npmrc"),
					addonOutputPath.resolve(".npmrc"),
					StandardCopyOption.REPLACE_EXISTING
			);

			/* TODO
			Files.copy(
					webclientSrc.resolve("addon-template/README.md"),
					addonOutputPath.resolve("README.md"),
					StandardCopyOption.REPLACE_EXISTING
			);
			*/

			Files.copy(
					webclientSrc.resolve("addon-template/tsconfig.json"),
					addonOutputPath.resolve("tsconfig.json"),
					StandardCopyOption.REPLACE_EXISTING
			);

			Files.copy(
					webclientSrc.resolve("addon-template/rollup.config.js"),
					addonOutputPath.resolve("rollup.config.js"),
					StandardCopyOption.REPLACE_EXISTING
			);


			Files.createDirectories(
					addonOutputPath.resolve("src")
			);

			writeTemplate(webAddon, webclientSrc, addonOutputPath, "src/index.ts");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "src/" + ADDON_FILE_NAME_PLACEHOLDER + ".component.ts");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "src/" + ADDON_FILE_NAME_PLACEHOLDER + ".module.ts");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "src/" + ADDON_FILE_NAME_PLACEHOLDER + ".service.ts");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "package.json");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "package-dist.json");
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
		}
	}

	private static void writeTemplate(final WebAddon webAddon, final Path webclientSrc, final Path addonOutputPath, final String path) throws IOException {
		String content = new String(Files.readAllBytes(
				webclientSrc.resolve("addon-template/" + path)
		));
		content = content.replace(ADDON_NAME_CAMEL_CASE_PLACEHOLDER, webAddon.getName());
		final String addonFileName = WebAddonUtils.camelCaseToFileName(webAddon.getName());
		content = content.replace(ADDON_FILE_NAME_PLACEHOLDER, addonFileName);
		String outputPath = path.replace(ADDON_FILE_NAME_PLACEHOLDER, addonFileName);
		LOG.info("Write file from template to: " + outputPath);
		Files.write(addonOutputPath.resolve(outputPath), content.toString().getBytes("UTF-8"));
	}


	/**
	 * generate addon.modules.ts which contains the registered addon modules
	 */
	public static boolean generateWebAddonModulesRegistry(List<WebAddon> webAddons, boolean addonDevMode, Path path) {
		return WebAddonUtils.generateWebAddonModulesRegistry(webAddons.stream()
									.map(WebAddon::getName)
									.collect(Collectors.toSet()),
				addonDevMode, path);
	}

}
