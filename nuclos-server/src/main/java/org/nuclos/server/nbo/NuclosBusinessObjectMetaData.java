package org.nuclos.server.nbo;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.businessentity.utils.BusinessObjectBuilderForInternalUse;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessObjectImport;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;

public class NuclosBusinessObjectMetaData extends AbstractNuclosBusinessObjectMetaData{

	private long serialVersionUID;

	private String sPackage;
	private String sFormattedEntity;
	private String sEntity;
	private UID    entityUid;
	
	private boolean isProxy;
	private List<String> lstImplements;
	private String sExtend;
	private boolean isMandator;
	private String sComment;
	private String sHeader;
	
	private List<NuclosBusinessObjectMethodMetaData> lstMethods;
	private List<NuclosBusinessObjectAttributeMetaData> lstAttributes;
	private List<NuclosBusinessObjectConstantMetaData> lstQueryConstants;
	private List<NuclosBusinessObjectProcess> lstProcesses;
	
	public NuclosBusinessObjectMetaData(
			String pEntity,
			UID pEntityUid,
			String pPackage,
			String pFormattedEntity,
			boolean isProxy,
			boolean isMandator,
			String sHeader,
			String sComment
	) {
	
		lstImplements = new ArrayList<String>();
		lstMethods = new ArrayList<NuclosBusinessObjectMethodMetaData>();
		lstAttributes = new ArrayList<NuclosBusinessObjectAttributeMetaData>();
		lstQueryConstants = new ArrayList<NuclosBusinessObjectConstantMetaData>();
		lstProcesses = new ArrayList<NuclosBusinessObjectProcess>();
		
		this.sFormattedEntity = pFormattedEntity;
		this.sPackage = pPackage;
		this.sEntity = pEntity;
		this.entityUid = pEntityUid;
		this.isProxy = isProxy;
		this.isMandator = isMandator;
		this.sComment = sComment;
		this.sHeader =sHeader;
	}
	
	public String getHeader() {
		return this.sHeader;
	}
	
	public void setExtend(String pExtend, String pPackageOfExtend) {
		this.addImport(pPackageOfExtend + "." + pExtend);
		this.sExtend = pExtend;
	}
	
	public String getFullQualifiedName() {
		return sPackage + "." + sFormattedEntity;
	}
	
	public void addInterface(String pInterface, String pPackageOfInterface) {
		if (!this.lstImplements.contains(pInterface)) {
			this.lstImplements.add(pInterface);
			this.addImport(pPackageOfInterface + "." + pInterface);
		}
	}
	
	public void addAttribute(NuclosBusinessObjectAttributeMetaData attr) {
		if (!this.lstAttributes.contains(attr)) {
			this.lstAttributes.add(attr);
			//addImport(attr.getImports());
		}
	}
	
	public void addQueryConstant(NuclosBusinessObjectConstantMetaData attr) {
		if (!this.lstQueryConstants.contains(attr)) {
			this.lstQueryConstants.add(attr);
			//addImport(attr.getImports());
		}
	}
	
	public void addAttributes(List<NuclosBusinessObjectAttributeMetaData> attrs) {
		for (NuclosBusinessObjectAttributeMetaData data: attrs) {
			addAttribute(data);
		}
	}
	
	public void addProcessConstant(NuclosBusinessObjectProcess process) {
		if (!this.lstProcesses.contains(process))
			this.lstProcesses.add(process);
	}
	
	public void addMethod(NuclosBusinessObjectMethodMetaData pMethod) {
		if (!this.lstMethods.contains(pMethod)) {
			this.lstMethods.add(pMethod);
			//this.addImport(pMethod.getImports());
		}
	}

	public void addMethods(List<NuclosBusinessObjectMethodMetaData> pMethods) {
		for (NuclosBusinessObjectMethodMetaData data : pMethods) {
			addMethod(data);
		}
	}

	public long getSerialVersionUID() {
		return serialVersionUID;
	}

	public void setSerialVersionUID(final long serialVersionUID) {
		this.serialVersionUID = serialVersionUID;
	}

	public boolean isProxy() {
		return isProxy;
	}
	
	public boolean isMandator() {
		return isMandator;
	}
	
	public String getEntity() {
		return sEntity;
	}

	public UID getEntityUid() {
		return entityUid;
	}

	public String getPackage() {
		return sPackage;
	}

	public String getFormattedEntity() {
		return sFormattedEntity;
	}

	public List<String> getImplements() {
		return lstImplements;
	}

	public String getExtend() {
		return sExtend;
	}

	public List<NuclosBusinessObjectMethodMetaData> getMethods() {
		return lstMethods;
	}

	public List<NuclosBusinessObjectAttributeMetaData> getAttributes() {
		return lstAttributes;
	}

	List<NuclosBusinessObjectConstantMetaData> getConstants() {
		return lstQueryConstants;
	}
	
	List<NuclosBusinessObjectProcess> getProcesses() {
		return this.lstProcesses;
	}

	public Object getComment() {		
		return this.sComment;
	}
}

enum MethodType {
	SETTER,
	GETTER,
	INSERT,
	REMOVE,
	DELETE;
	
	@Override
	public String toString() {
		String retVal = "";
		switch (this) {
		case SETTER:
			retVal = "Setter-Method";
			break;
		case GETTER:
			retVal = "Getter-Method";
			break;
		case INSERT:
			retVal = "Insert-Method";
			break;
		case REMOVE:
			retVal = "Remove-Method";
			break;
		case DELETE:
			retVal = "Delete-Method";
			break;
		default:
			break;
		}
		return retVal;
	}
}

class NuclosBusinessObjectMethodMetaData extends AbstractNuclosBusinessObjectMetaData{
	
	private boolean isInterface;
	private String sMethodName;
	private String sAccessType;
	private String sReturnType;
	private String sMethodBody;
	private String sComment;
	private boolean bNotNullableBooleanSetter;
	
	private List<String> lstThrows = new ArrayList<String>();
	private List<NuclosBusinessObjectAttributeMetaData> lstParameter = new ArrayList<NuclosBusinessObjectAttributeMetaData>();
	private List<NuclosBusinessObjectAttributeMetaData> lstAnnotations = new ArrayList<NuclosBusinessObjectAttributeMetaData>();
	
	public NuclosBusinessObjectMethodMetaData (String pMethodName, String pAccessType, String pReturnType, boolean isInterface, String sComment) {
		this.sMethodName = pMethodName;
		this.sAccessType = pAccessType;
		this.sReturnType = pReturnType;
		this.isInterface = isInterface;
		this.sComment = sComment;
	}
	
	
	public String getComment() {
		return sComment;
	}
	public String getMethodBody() {
		return sMethodBody;
	}
	public void setMethodBody(String sMethodBody) {
		this.sMethodBody = sMethodBody;
	}
	public String getMethodName() {
		return sMethodName;
	}
	public void setMethodName(String sMethodName) {
		this.sMethodName = sMethodName;
	}
	public String getAccessType() {
		return sAccessType;
	}
	public void setAccessType(String sAccessType) {
		this.sAccessType = sAccessType;
	}	
	public String getReturnType() {
		return sReturnType;
	}
	public void setReturnType(String sReturnType) {
		this.sReturnType = sReturnType;
	}
	public boolean isInterface() {
		return isInterface;
	}
	public boolean isNotNullableBooleanSetter() {
		return bNotNullableBooleanSetter;
	}
	public void setNotNullableBooleanSetter(boolean bNotNullableBooleanSetter) {
		this.bNotNullableBooleanSetter = bNotNullableBooleanSetter;
	}
	public List<NuclosBusinessObjectAttributeMetaData> getParameter() {
		return this.lstParameter;
	}
	
	public List<NuclosBusinessObjectAttributeMetaData> getAnnotations() {
		return this.lstAnnotations;
	}
	
	public List<String> getThrows() {
		return this.lstThrows;
	}
	
	public void addAnnotation(NuclosBusinessObjectAttributeMetaData pParameter) {
		if (!this.lstAnnotations.contains(pParameter)) {
			this.lstAnnotations.add(pParameter);
			//this.addImport(pParameter.getImports());
		}
	}
	
	public void addParameter(NuclosBusinessObjectAttributeMetaData pParameter) {
		if (!this.lstParameter.contains(pParameter)) {
			this.lstParameter.add(pParameter);
			//this.addImport(pParameter.getImports());
		}
	}
	
	public void addThrows(String pThrows) {
		if (!this.lstThrows.contains(pThrows)) {
			this.lstThrows.add(pThrows);
			//this.addImport(pThrows);
		}
	}
	
	public boolean equals(Object obj) {
		 boolean retVal = false;
		 
		 if (obj instanceof NuclosBusinessObjectMethodMetaData) {
			 NuclosBusinessObjectMethodMetaData metaData = (NuclosBusinessObjectMethodMetaData) obj;
			 
			 if (metaData.getMethodName().equals(this.getMethodName()) &&
					 RigidUtils.equal(metaData.getReturnType(), this.getReturnType()) &&
				 metaData.getAccessType().equals(this.getAccessType()) && 
				 metaData.getParameter().equals(this.getParameter()))
				 	retVal = true;
		 }
		 
		 return retVal;
	}
	
}

class NuclosBusinessObjectProcess extends AbstractNuclosBusinessObjectMetaData{
	private String processName;
	private UID	   processUID;
	private UID	   moduleUID;
	private String  moduleName;
	/**
	 * @param processName
	 * @param processUID
	 * @param moduleUID
	 */
	public NuclosBusinessObjectProcess(String processName, UID processUID,
			String moduleName, UID moduleUID) {
		super();
		this.processName = processName;
		this.processUID = processUID;
		this.moduleUID = moduleUID;
		this.moduleName = moduleName;
	}
	public String getModuleName() {
		return moduleName;
	}
	
	public String getProcessName() {
		return processName;
	}
	public UID getProcessId() {
		return processUID;
	}
	public UID getModuleId() {
		return moduleUID;
	}
	
}

class NuclosBusinessObjectConstantMetaData extends AbstractNuclosBusinessObjectMetaData{
	private String  sAttributeName;
	private String  sFieldName;
	private UID 	fieldUid;
	private String  sEntityName;
	private UID     entityUid;
	private String  sPackageName;
	private String  tType;
	private Boolean isPrimKey;
	private Boolean isForeignKey;
	private String sComment;
	
	private List<NuclosBusinessObjectAttributeMetaData> lstAnnotations = new ArrayList<NuclosBusinessObjectAttributeMetaData>();
	
	public NuclosBusinessObjectConstantMetaData(String pName, String pPackageName, String pEntityName, 
			UID pEntityUid, String pFieldName, UID pFieldUid, String pType, Boolean pIsPrimKey, Boolean pIsForeignKey, String pComment) {
		
		this.sAttributeName = pName;
		this.sFieldName = 	pFieldName;
		this.fieldUid = 	pFieldUid;
		this.sEntityName = 	pEntityName;
		this.entityUid = 	pEntityUid;
		this.sPackageName = pPackageName;
		this.isPrimKey = 	pIsPrimKey;
		this.isForeignKey = pIsForeignKey;
		this.tType = 		pType;
		this.sComment = 	pComment;
	}
	
	public List<NuclosBusinessObjectAttributeMetaData> getAnnotations() {
		return this.lstAnnotations;
	}
	
	public void addAnnotation(NuclosBusinessObjectAttributeMetaData pParameter) {
		if (!this.lstAnnotations.contains(pParameter)) {
			this.lstAnnotations.add(pParameter);
			//this.addImport(pParameter.getImports());
		}
	}
	
	public String getComment() {
		return this.sComment;
	}
	
	String getAttributeName() {
		return sAttributeName;
	}

	public void setAttributeName(String name) {
		sAttributeName = name;
	}
	
	String getFieldName() {
		return sFieldName;
	}

	String getEntityName() {
		return sEntityName;
	}

	String getPackageName() {
		return sPackageName;
	}

	String getType() {
		return tType;
	}

	Boolean isPrimKey() {
		return isPrimKey;
	}

	Boolean isForeignKey() {
		return isForeignKey;
	}

	UID getFieldUid() {
		return fieldUid;
	}

	UID getEntityUid() {
		return entityUid;
	}	
	
}
class NuclosBusinessObjectAttributeMetaData extends AbstractNuclosBusinessObjectMetaData{
	
	private String sAttributeName;
	private String sAccessType;
	private String sDataType;	
	private String sComment;
	
	public NuclosBusinessObjectAttributeMetaData(String pAttribute, String pAccessType, String pDataType, String sComment) {
		this.sAttributeName = pAttribute;
		this.sAccessType = pAccessType;
		this.sDataType = pDataType;
		this.sComment = sComment;
	}
	
	public String getComment() {
		return sComment;
	}
	
	public String getAttributeName() {
		return sAttributeName;
	}
	public void setAttributeName(String sAttributeName) {
		this.sAttributeName = sAttributeName;
	}
	public String getAccessType() {
		return sAccessType;
	}
	public void setAccessType(String sAccessType) {
		this.sAccessType = sAccessType;
	}
	public String getDataType() {
		return sDataType;
	}
	public void setDataType(String sDataType) {
		this.sDataType = sDataType;
	}

	public boolean equals(Object obj) {
		 boolean retVal = false;
		
		 if (obj instanceof NuclosBusinessObjectAttributeMetaData) {
			 NuclosBusinessObjectAttributeMetaData metaData = (NuclosBusinessObjectAttributeMetaData) obj;
			 
			 if (metaData.getAccessType() != null && metaData.getAccessType().equals(this.getAccessType()) && 
				 metaData.getAttributeName().equals(this.getAttributeName()) && 
				 metaData.getDataType().equals(this.getDataType()))
				 	retVal = true;
		 }
		 return retVal;
	}
}

class AbstractNuclosBusinessObjectMetaData {
	
	public static final String PUBLIC = "public";
	public static final String PRIVATE = "private";
	public static final String PROTECTED = "protected";
	
	public static final String VOID = "void";
	public static final String OBJECT = "Object";

	private List<String> lstImports = new ArrayList<String>();

	public void addImport(NuclosBusinessObjectImport imp) {
		addImport(imp.getImportClass().getCanonicalName());
	}

	/**
	 * @deprecated Use {@link #addImport(NuclosBusinessObjectImport)} instead.
	 *
	 * @param pImports
	 */
	@Deprecated
	protected void addImport(String... pImports) {
		for (String imp : pImports) {
			if (!this.lstImports.contains(imp))
				this.lstImports.add(imp);
		}
	}

	/**
	 * Adds a "star" import, e.g. java.util.*
	 *
	 * @param pkg
	 */
	protected void addImportPackageStar(String pkg) {
		if (!pkg.endsWith(".*")) {
			pkg += ".*";
		}

		if (!this.lstImports.contains(pkg))
			this.lstImports.add(pkg);
	}
	private void addImport(List<String> pImports) {
		if (pImports != null) {
			for (String imp : pImports) {
				if (!this.lstImports.contains(imp))
					this.lstImports.add(imp);
			}			
		}
	}
	
	public List<String> getImports() {
		return this.lstImports;
	}

	public static String getPkClassName(EntityMeta<?> entity) {
		final String result;
		if (entity.isUidEntity()) {
			if (BusinessObjectBuilderForInternalUse.getEntityMetas().contains(entity)) {
				result = UID.class.getCanonicalName();
			} else {
				result = org.nuclos.api.UID.class.getCanonicalName();
			}
		} else {
			result = Long.class.getCanonicalName();
		}
		return result;
	}
	
}
