package org.nuclos.server.report;

import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.springframework.stereotype.Component;

@Component
public class ReportDatasourceObjectCompiler extends AbstractNuclosObjectCompiler {

	public ReportDatasourceObjectCompiler() {
		super(SourceType.DATASOURCE_REPORT,
		      NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE,
		      NuclosCodegeneratorConstants.DATASOURCEREPORT_SRC_DIR_NAME);
	}

}
