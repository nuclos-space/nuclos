package org.nuclos.server.report;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.api.datasource.Datasource;
import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("reportDatasourceObjectBuilder")
public class ReportDatasourceObjectBuilder extends NuclosObjectBuilder {

	private ReportDatasourceObjectCompiler compiler;
	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.datasource";
	private static final String DEFFAULT_ENTITY_PREFIX = "DS";
	private static final String DEFFAULT_ENTITY_POSTFIX = "DS";

	public ReportDatasourceObjectBuilder() {
	}

	@Autowired
	public void setReportDatasourceObjectCompiler(ReportDatasourceObjectCompiler comp) {
		this.compiler = comp;
	}

	public ReportDatasourceObjectCompiler getReportDatasourceObjectCompiler() {
		return this.compiler;
	}

	@Autowired
	private MetaProvider provider;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	public void createObjects(final ForkJoinPool builderThreadPool) throws NuclosCompileException, InterruptedException {
		List<NuclosBusinessJavaSource> retVal = new ArrayList<>();

		CollectableSearchExpression clctExpr = new CollectableSearchExpression(SearchConditionUtils.newComparison(E.DATASOURCE.withRuleClass, ComparisonOperator.EQUAL, Boolean.TRUE));

		// get all reports
		List<EntityObjectVO<UID>> allReport =
			nucletDalProvider.getEntityObjectProcessor(E.DATASOURCE).getBySearchExpression(clctExpr);

		if (allReport != null && allReport.size() > 0) {
			for (EntityObjectVO<UID> eoVO : allReport) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				DatasourceVO sourceVO = MasterDataWrapper
					.getDatasourceVO(new MasterDataVO<>(eoVO, false), "INITIAL", null);
				retVal.add(
					createJavaSourceFile(new DatasourceVO(sourceVO, sourceVO.getName(),
					                                      sourceVO.getDescription(),
					                                      sourceVO.getValid(), sourceVO.getSource(),
					                                      sourceVO.getNucletUID(),
					                                      DatasourceVO.PERMISSION_READWRITE)));
			}
		}

		if (retVal.size() > 0) {
			ReportDatasourceObjectCompiler comp = getReportDatasourceObjectCompiler();
			comp.compileSourcesAndJar(builderThreadPool, retVal);
		}
	}

	private NuclosBusinessJavaSource createJavaSourceFile(DatasourceVO model) {
		final String sPackage = getNucletPackageStatic(model.getNucletUID(), provider);
		final String formatEntity = getNameForFqn(model.getName());
		final String qname = sPackage + "." + formatEntity;
		final String
			filename =
			NuclosCodegeneratorUtils.datasourceReportSource(sPackage, formatEntity).toString();
		final String content = createJavaSourceContent(sPackage, formatEntity, model);

		return new NuclosBusinessJavaSource(qname, filename, content, true);
	}

	private String createJavaSourceContent(String pkgName, String className, DatasourceVO model) {
		StringBuilder s = new StringBuilder();
		s.append("package ").append(pkgName).append(";\n\n");

		s.append("import ").append(Datasource.class.getCanonicalName()).append(";\n");
		s.append("import ").append(UID.class.getCanonicalName()).append(";\n\n");
		s.append("public class ").append(className).append(" implements Datasource {\n\n");

		s.append(createJavaFileContent(model));

		s.append("\n\n}");
		return s.toString();
	}

	private String createJavaFileContent(DatasourceVO model) {
		StringBuilder s = new StringBuilder();

		String name = formatStringValues(model.getName(), "");
		;
		String description = formatStringValues(model.getDescription(), "");

		s.append("public static final org.nuclos.api.UID uid = UID.parseUID(\"")
			.append(model.getId().getString()).append("\");\n");
		s.append("\npublic static final String name = \"").append(name).append("\"; \n ");
		s.append("\npublic static final String description = \"").append(description)
			.append("\"; \n\n");

		s.append("public org.nuclos.api.UID getUid() { return uid;}\n\n"
		         + "public org.nuclos.api.UID getId() { return getUid(); }\n");

		return s.toString();
	}

	public static String getNameForFqn(String sName) {
		return formatMethodName(NuclosEntityValidator.escapeJavaIdentifier(sName, DEFFAULT_ENTITY_PREFIX))
		       + DEFFAULT_ENTITY_POSTFIX;
	}

	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}

}
