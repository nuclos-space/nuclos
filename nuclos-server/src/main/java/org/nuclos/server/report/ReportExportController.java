//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.i18n.language.data.DataLanguageFacadeRemote;
import org.nuclos.server.report.ejb3.DatasourceFacadeLocal;
import org.nuclos.server.report.ejb3.ReportFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReportExportController {

	private static final Logger LOG = LoggerFactory.getLogger(ReportExportController.class);

	/**
	 * Export a report via HTTP.
	 * TODO add support for forms (respect object level permissions).
	 *
	 * @param reportUID
	 * @param output
	 * @param request
	 * @param response
	 * @return
	 */
	@RolesAllowed("Login")
	@RequestMapping("/{reportUID}/{output}")
	public ModelAndView export(@PathVariable UID reportUID, @PathVariable String output, HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			// find report by UID in user's readable reports
			ReportFacadeLocal facade = ServerServiceLocator.getInstance().getFacade(ReportFacadeLocal.class);
			final DataLanguageFacadeRemote dataLangRemote = ServerServiceLocator.getInstance().getFacade(DataLanguageFacadeRemote.class);
			for (DefaultReportVO reportvo : facade.getReports()) {
			
			
				if (ObjectUtils.equals(reportvo.getPrimaryKey(), reportUID)) {
					if (reportvo.getType() != ReportType.REPORT) {
						response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "Only reports are available for http export.");
					}

					for (DefaultReportOutputVO outputvo : facade.getReportOutputs(reportvo.getId())) {
						if (output.equalsIgnoreCase(outputvo.getDescription())) {
							NuclosFile result = export(reportvo, outputvo, request.getParameterMap(), dataLangRemote.getPrimaryDataLanguageUID(), facade.getCurrentMandatorUID());
							response.setContentType("text/csv");
							response.setHeader("Content-disposition", "attachment; filename=" + result.getName());
							response.setContentLength(result.getContent().length);

							ServletOutputStream sos = response.getOutputStream();
							sos.write(result.getContent());
							sos.flush();
							sos.close();
							return null;
						}
					}
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "Requested report output does not exist.");
					return null;
				}
			}
			// report not found -> does not exist or permission was denied.
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Requested report does not exist or permission was denied.");
			return null;
		}
		catch (Exception e) {
			LOG.error("export failed: {}", e, e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
		}
		return null;
	}

	private NuclosFile export(ReportVO report, DefaultReportOutputVO output, Map<?,?> parameters, UID language, UID mandatorUID) throws CommonBusinessException, ClassNotFoundException {
		final Map<String, Object> params = CollectionUtils.newHashMap();
		List<DatasourceParameterVO> lstParameters = ServerServiceLocator.getInstance().getFacade(DatasourceFacadeLocal.class).getParameters(report.getDatasourceId());
		for (DatasourceParameterVO dspvo: lstParameters) {
			if (parameters.containsKey(dspvo.getParameter())) {
				params.put(dspvo.getParameter(), CollectableFieldFormat.getInstance(
						Class.forName(dspvo.getDatatype()))
						.parse(null, ((String[])parameters.get(dspvo.getParameter()))[0]));
			}
			else {
				params.put(dspvo.getParameter(), null);
			}
		}
		return ServerServiceLocator.getInstance().getFacade(ReportFacadeLocal.class).prepareReport(output.getId(), params, null, language, mandatorUID);
	}
}
