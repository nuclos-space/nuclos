//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.valuelistprovider.IDataSource;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

// @Local
public interface DatasourceFacadeLocal extends IDataSource {

	/**
	 * get a Datasource by id regardless of permisssions
	 */
	@RolesAllowed("Login")
	DatasourceVO getDatasource(final UID dataSourceUID)  throws CommonFinderException,	CommonPermissionException;

	/**
	 * get dynamic entity value object
	 * 
	 * @param dynamicEntityUID
	 *            UID of dynamic entity 
	 * @return dynamic entity value object
	 */
	DynamicEntityVO getDynamicEntity(final UID dynamicEntityUID) throws CommonFinderException, CommonPermissionException;
	
	/**
	 * get chart value object
	 * 
	 * @param chartUID
	 *            UID of chart
	 * @return chart value object
	 */
	ChartVO getChart(final UID chartUID) throws CommonFinderException, CommonPermissionException;

	/**
	 * get sql string for datasource definition
	 * @param dataSourceUID UID of datasource
	 * @return string containing sql
	 */
	String createSQL(final UID dataSourceUID,
		final Map<String, Object> mpParams) throws NuclosDatasourceException;

	/**
	 * get sql string for report execution.
	 * check that this method is only called by the local interface as there is no authorization applied.
	 *
	 * @param dataSourceUID datasource UID
	 * @return string containing sql
	 */
	String createSQLForReportExecution(final UID dataSourceUID, final Map<String, Object> mpParams, UID language) throws NuclosDatasourceException;

	/**
	 * get a datasource result by datasource UID
	 * 
	 * @param dataSourceUID datasource UID
	 */
	@RolesAllowed("Login")
	ResultVO executeQuery(final UID dataSourceUID,
		final Map<String, Object> mpParams, final Integer iMaxRowCount, UID language, UID mandatorUID)
		throws NuclosDatasourceException, CommonFinderException;

   /**
	 * Retrieve the parameters a datasource accepts.
	 * 
	 * @param dataSourceUID datasource UID
	 */
	@RolesAllowed("Login")
	List<DatasourceParameterVO> getParameters(
		final UID dataSourceUID) throws NuclosFatalException,
		NuclosDatasourceException;

	@RolesAllowed("Login")
	Collection<DynamicEntityVO> getDynamicEntities();
	
	
	String createSQL(final DatasourceVO datasourcevo, final Map<String, Object> mpParams) throws NuclosDatasourceException;
	
}

