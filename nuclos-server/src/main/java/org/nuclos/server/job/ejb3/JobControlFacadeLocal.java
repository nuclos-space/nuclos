//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.job.ejb3;

import java.util.Date;

import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

// @Local
public interface JobControlFacadeLocal {

	String NOT_ACTIVATED = "Deaktiviert";
	
	Pair<JobVO, MasterDataVO<Long>> prepare(UID oId);

	
	void scheduleJob(UID oId) throws CommonBusinessException;
	void unscheduleJob(UID oId) throws CommonBusinessException;

	void setJobExecutionResult(Object oResult, Date dFireTime, Date dNextFireTime, JobVO jobVO, MasterDataVO<Long> jobRun);

	void setJobExecutionResultError(UID oId, Date dFireTime, Date sNextFireTime, Long iSessionId, Exception e);

	void writeToJobRunMessages(Long iSessionId, String sLevel, String sMessage, String sRuleName);
	
	void setMandatorInUserContext(UID mandator);
	
}
