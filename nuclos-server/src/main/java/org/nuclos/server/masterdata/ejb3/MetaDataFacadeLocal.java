package org.nuclos.server.masterdata.ejb3;

import java.util.List;
import java.util.Map;

import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.transport.vo.EntityMetaTransport;
import org.nuclos.common.transport.vo.FieldMetaTransport;

public interface MetaDataFacadeLocal {
	
	void setMandatorLevel(UID entityUID, UID mandatorLevelUID, UID initialMandatorUID) throws NuclosBusinessException;

	String createOrModifyEntity(EntityMetaTransport updatedMDEntity,
								List<FieldMetaTransport> lstFields,
								boolean bRollBackOnStructureChangeExceptions,
								Map<String, Exception> structureChangeExceptions) throws NuclosBusinessException;
	
}
