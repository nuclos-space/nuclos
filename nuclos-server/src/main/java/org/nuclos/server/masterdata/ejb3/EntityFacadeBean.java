//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.MakeCollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = {Exception.class})
public class EntityFacadeBean extends NuclosFacadeBean implements EntityFacadeLocal, EntityFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(EntityFacadeBean.class);

	private static final String FIELDNAME_ACTIVE = "active";
	private static final String FIELDNAME_VALIDFROM = "validFrom";
	private static final String FIELDNAME_VALIDUNTIL = "validUntil";

	private final DatasourceCache datasourceCache;
	private final SpringDataBaseHelper dataBaseHelper;
	private final DatasourceServerUtils datasourceServerUtils;
	private final SessionUtils sessionUtils;
	private final MandatorUtils mandatorUtils;
	private final MetaProvider metaProvider;
	private final LayoutFacadeLocal layoutFacade;

	public EntityFacadeBean(
			final DatasourceCache datasourceCache,
			final SpringDataBaseHelper dataBaseHelper,
			final DatasourceServerUtils datasourceServerUtils,
			final SessionUtils sessionUtils,
			final MandatorUtils mandatorUtils,
			final MetaProvider metaProvider,
			final LayoutFacadeLocal layoutFacade
	) {
		this.datasourceCache = datasourceCache;
		this.dataBaseHelper = dataBaseHelper;
		this.datasourceServerUtils = datasourceServerUtils;
		this.sessionUtils = sessionUtils;
		this.mandatorUtils = mandatorUtils;
		this.metaProvider = metaProvider;
		this.layoutFacade = layoutFacade;
	}

	/**
	 * §ejb.interface-method view-type="remote"
	 * §ejb.permission role-name="Login"
	 */
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNames(UID layoutUID) {
		return layoutFacade.getSubFormEntityAndParentSubFormEntityNamesByLayoutId(layoutUID);
	}

	/**
	 * §todo this method should be used in CollectableFieldsProvider
	 *
	 * @return list of collectable fields
	 */
	@Override
	public List<CollectableField> getCollectableFieldsByName(
			final CollectableFieldsByNameParams params
	) throws CommonPermissionException {

		return _getCollectableFieldsByName(
				params
		);
	}

	private <PK> List<CollectableField> _getCollectableFieldsByName(
			final CollectableFieldsByNameParams params
	) throws CommonPermissionException {

		final MakeCollectableValueIdField<PK> clctMaker = new MakeCollectableValueIdField<>(
				params.getStringifiedFieldDefinition(),
				params.getDataLanguage()
		);

		for (UID fieldUid: clctMaker.getFields()) {
			FieldMeta<?> fieldMeta = metaProvider.getEntityField(fieldUid);
			boolean readAllowed = securityCache.isReadAllowedForFieldViaStringifiedReference(
					fieldMeta,
					getCurrentUserName(),
					getCurrentMandatorUID()
			);

			if (!readAllowed) {
				throw new CommonPermissionException("Not allowed to read field: " + fieldMeta);
			}
		}

		CollectableSearchCondition clctcond = null;
		if (params.isCheckValidity()) {
			FieldMeta<?> active = null;
			FieldMeta<?> validFrom = null;
			FieldMeta<?> validUntil = null;
			for (FieldMeta<?> fMeta : metaProvider.getAllEntityFieldsByEntity(params.getEntityUid()).values()) {
				if (FIELDNAME_ACTIVE.equals(fMeta.getFieldName())) {
					active = fMeta;
				} else if (FIELDNAME_VALIDFROM.equals(fMeta.getFieldName())) {
					validFrom = fMeta;
				} else if (FIELDNAME_VALIDUNTIL.equals(fMeta.getFieldName())) {
					validUntil = fMeta;
				}
			}
			Date dateNow = new Date(System.currentTimeMillis());

			CollectableSearchCondition clctcondActiv = null;
			CollectableSearchCondition clctcondValids = null;

			if (active != null)
				clctcondActiv = SearchConditionUtils.newComparison(active, ComparisonOperator.EQUAL, Boolean.TRUE);

			if (validFrom != null && validUntil != null) {
				clctcondValids = SearchConditionUtils.and(
						SearchConditionUtils.or(
								SearchConditionUtils.newComparison(validFrom, ComparisonOperator.LESS_OR_EQUAL, dateNow),
								SearchConditionUtils.newIsNullCondition(validFrom)
						),
						SearchConditionUtils.or(
								SearchConditionUtils.newComparison(validUntil, ComparisonOperator.GREATER_OR_EQUAL, dateNow),
								SearchConditionUtils.newIsNullCondition(validUntil)
						)
				);
			}

			if (clctcondActiv != null && clctcondValids != null) {
				clctcond = SearchConditionUtils.and(clctcondActiv, clctcondValids);
			} else {
				if (clctcondActiv != null)
					clctcond = clctcondActiv;
				else
					clctcond = clctcondValids;
			}

		}

		final FieldMeta<?> efDeleted = SF.LOGICALDELETED.getMetaData(params.getEntityUid());
		final EntityMeta<?> em = metaProvider.getEntity(params.getEntityUid());
		if (efDeleted != null && em.getFields().contains(efDeleted)) {
			final CollectableSearchCondition condSearchDeleted = new CollectableComparison(
					new CollectableEOEntityField(efDeleted), ComparisonOperator.EQUAL, new CollectableValueField(false));
			clctcond = clctcond == null ? condSearchDeleted : SearchConditionUtils.and(clctcond, condSearchDeleted);
		}

		if (params.getAdditionalCondition() != null && params.getAdditionalCondition().y != null) {
			FieldMeta<?> fm = metaProvider.getEntityField(params.getAdditionalCondition().x);
			CollectableSubCondition subCond = new CollectableSubCondition(fm.getEntity(), fm.getUID(), params.getAdditionalCondition().y);
			clctcond = clctcond == null ? subCond : SearchConditionUtils.and(clctcond, subCond);
		}

		IEntityObjectProcessor<PK> eoProcessor = nucletDalProvider.getEntityObjectProcessor(params.getEntityUid());
		clctcond = mandatorUtils.append(clctcond, em, params.getMandator());
		List<EntityObjectVO<PK>> eoResult = eoProcessor.getBySearchExprResultParams(new CollectableSearchExpression(clctcond), new ResultParams(clctMaker.getFields()));

		return CollectionUtils.sorted(
				CollectionUtils.transform(eoResult, clctMaker),
				(CollectableField o1, CollectableField o2) -> LangUtils.compare(o1.getValue(), o2.getValue())
		);
	}

	/**
	 * TODO: Too many parameters.
	 */
	@Override
	public List<CollectableValueIdField> getQuickSearchResult(
			UID field,
			String search,
			UID vlpUID,
			Map<String, Object> vlpParameter,
			Long iMaxRowCount,
			UID mandator
	) throws CommonBusinessException {
		final FieldMeta<?> efMeta = metaProvider.getEntityField(field);
		return getQuickSearchResult(efMeta, search, vlpUID, vlpParameter, iMaxRowCount, mandator);
	}

	/**
	 * TODO: Too many parameters.
	 */
	@Override
	public List<CollectableValueIdField> getQuickSearchResult(
			FieldMeta<?> efMeta,
			String search,
			UID vlpUID,
			Map<String, Object> vlpParameter,
			Long iMaxRowCount,
			UID mandator
	) throws CommonBusinessException {
		return super.getReferenceList(
				dataBaseHelper,
				datasourceServerUtils,
				sessionUtils,
				efMeta,
				search,
				vlpUID,
				vlpParameter,
				null,
				iMaxRowCount,
				mandator,
				true
		);
	}

	@Override
	public UID getBaseEntity(UID dynamicentityUID) {
		return datasourceCache.getDynamicEntity(dynamicentityUID).getEntityUID();
	}
}
