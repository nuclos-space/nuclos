package org.nuclos.server.common.mail.properties;

import java.util.Properties;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class MailConnectionProperties {
	/**  */
	private static final long serialVersionUID = 1L;

	protected String host;
	protected int port;
	protected final String protocol;

	private String socketFactoryClass;

	protected String username;
	protected String password;
	private String folderFrom = "INBOX";
	private String folderTo;

	private boolean ssl = false;
	private boolean startTls = false;

	protected MailConnectionProperties(final String protocol) {
		this.protocol = protocol;
	}

	public abstract boolean isUsingIMAP();

	public abstract Properties toProperties();

	public abstract void setUser(String user);

	public void setPassword(String pwd) {
		this.password = pwd;
	}

	public String getUser() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public String getFolderFrom() {
		return folderFrom;
	}

	public void setFolderFrom(String folderFrom) {
		if (org.apache.commons.lang.StringUtils.isNotBlank(folderFrom)) {
			this.folderFrom = folderFrom;
		}
	}

	public String getFolderTo() {
		return folderTo;
	}

	public void setFolderTo(String folderTo) {
		if (org.apache.commons.lang.StringUtils.isNotBlank(folderTo)) {
			this.folderTo = folderTo;
		}
	}

	public boolean isStartTls() {
		return startTls;
	}

	public void setStartTls(final boolean startTls) {
		this.startTls = startTls;
	}

	public boolean isSsl() {
		return ssl;
	}

	public void setSsl(final boolean ssl) {
		this.ssl = ssl;
	}

	public String getHost() {
		return host;
	}

	public void setHost(final String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(final int port) {
		this.port = port;
	}

	public String getSocketFactoryClass() {
		return socketFactoryClass;
	}

	public void setSocketFactoryClass(final String socketFactoryClass) {
		this.socketFactoryClass = socketFactoryClass;
	}

	public String getProtocol() {
		return protocol;
	}
}
