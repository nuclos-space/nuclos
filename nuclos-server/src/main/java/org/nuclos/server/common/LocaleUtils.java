//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;

public class LocaleUtils {
	
	public static void setResourceIdForField(EntityMeta<UID> entity, UID uid, FieldMeta<String> field, String sResourceId) {
		setResourceIdForDbField(entity, uid, field, sResourceId);
	}
	
	public static String[] getResourceIdsForFields(EntityMeta<UID> entity, UID uid, List<FieldMeta<String>> lstFields) {
		DbQueryBuilder builder = SpringDataBaseHelper.getInstance().getDbAccess().getQueryBuilder();
		DbQuery<Object[]> query = builder.createQuery(Object[].class);
		DbFrom<UID> t = query.from(entity);
		List<DbSelection<String>> lstSelections = new ArrayList<DbSelection<String>>();
		for (FieldMeta<String> field : lstFields) {
			lstSelections.add(t.baseColumn(field));
		}
		query.multiselect(lstSelections);
		query.where(builder.equalValue(t.baseColumn(entity.getPk()), uid));
		
		List<Object[]> lstResult = SpringDataBaseHelper.getInstance().getDbAccess().executeQuery(query);
		if (lstResult.size() == 0) {
			return null;
		}
		Object[] first = lstResult.get(0);
		return Arrays.copyOf(first, first.length, String[].class);
	}

	public static void setResourceIdForDbField(EntityMeta<?> entity, UID uid, FieldMeta<String> field, String sResourceId) {
		SpringDataBaseHelper.getInstance().execute(DbStatementUtils
				.updateValuesUnsafe(entity, field, DbNull.escapeNull(sResourceId, String.class))
				.where(entity.getPk(), uid));
	}
}
