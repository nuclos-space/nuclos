//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.nuclos.api.Direction;
import org.nuclos.api.Message;
import org.nuclos.api.Progress;
import org.nuclos.api.command.Command;
import org.nuclos.api.service.MessageContextService;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.api.ApiCommandImpl;
import org.nuclos.common.api.ApiDirectionImpl;
import org.nuclos.common.api.ApiMessageImpl;
import org.nuclos.server.common.LockedTabProgressNotifier;
import org.nuclos.server.common.MessageReceiverContext;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("Login")
public class ApiMessageContextService implements MessageContextService {

	private static final ThreadLocal<List<Message>> messageStore = new ThreadLocal<>();

	/**
	 * Register a message store for the current thread only. Don't forget to clear the store in a finally block after the work is done!
	 * The store receives all messages send via 'sendMessage' by this service, a JMS message will not be sent.
	 * @param store
	 */
	public static void setMessageStore(List<Message> store) {
		messageStore.set(store);
	}

	public static void clearMessageStore() {
		messageStore.remove();
	}

	@Override
	public void sendMessage(Message message) {
		List<Message> messageStore = ApiMessageContextService.messageStore.get();
		if (messageStore != null) {
			messageStore.add(message);
		} else {
			ApiMessageImpl apiMsg = new ApiMessageImpl(message, MessageReceiverContext.getInstance().getId());
			NuclosJMSUtils.sendObjectMessageAfterCommit(apiMsg, JMSConstants.TOPICNAME_RULENOTIFICATION, SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
		}
	}
	
	@Override
	public void sendProgress(Progress progress) {
		LockedTabProgressNotifier.notify(progress);
	}
	
	@Override
	public void navigate(Direction direction) {
		ApiDirectionImpl apiDirection = new ApiDirectionImpl(direction, MessageReceiverContext.getInstance().getId());
		NuclosJMSUtils.sendObjectMessageAfterCommit(apiDirection, JMSConstants.TOPICNAME_RULEDIRECTION, getReceiver());
	}

	/**
	 * Instantiates a new command implementation for the given command and sends it to the client.
	 *
	 * @param command
	 */
	@Override
	public void sendCommand(final Command command) {
		ApiCommandImpl apiCommand = new ApiCommandImpl(MessageReceiverContext.getInstance().getId(), command);
		NuclosJMSUtils.sendObjectMessageAfterCommit(apiCommand, JMSConstants.TOPICNAME_RULECOMMAND, getReceiver());
	}

	/**
	 * Returns the current principal.
	 *
	 * @return
	 */
	private String getReceiver() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}
}
