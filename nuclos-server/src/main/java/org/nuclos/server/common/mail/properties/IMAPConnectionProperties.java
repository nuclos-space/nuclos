package org.nuclos.server.common.mail.properties;

import java.util.Properties;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class IMAPConnectionProperties extends MailConnectionProperties {
	/**  */
	private static final long serialVersionUID = 1L;

	public IMAPConnectionProperties() {
		super("imap");

		setSocketFactoryClass("mail.imap.socketFactory.class");
	}

	public boolean isUsingIMAP() {
		return true;
	}

	@Override
	public Properties toProperties() {
		Properties p = new Properties();

		p.put("mail.imap.host", getHost());
		p.put("mail.imap.port", getPort());
		p.put("mail.imap.auth", "true");
		p.put("mail.imap.socketFactory.class", getSocketFactoryClass());

		return p;
	}

	public void setUser(String user) {
		this.username = user;
	}
}
