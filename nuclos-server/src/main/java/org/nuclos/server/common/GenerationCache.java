package org.nuclos.server.common;

import java.awt.Dimension;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorUsageVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;

@Component
@DependsOn({"metaDataProvider", "resourceCache"})
public class GenerationCache {

	private static Logger LOG = LoggerFactory.getLogger(GenerationCache.class);

	private boolean generationsLoaded = false;
	
	private List<GeneratorActionVO> lstAllGenerationActions;
	
	// Spring injection
	
	@Autowired
	private NucletDalProvider nucletDalProvider;

	@Autowired
	private GenericObjectMetaDataCache genericObjectMetaDataCache;

	@Autowired
	private MetaProvider metaProvider;
	
	// end of Spring injection
	
	GenerationCache() {
	}
	
	@PostConstruct
	final void init() {
		loadGenerations();
	}
	
	private void loadGenerations() {
		List<GeneratorActionVO> lstAllGenerationActionsTemp = new ArrayList<GeneratorActionVO>();
		List<EntityObjectVO<UID>> allUsages = nucletDalProvider.getEntityObjectProcessor(E.GENERATIONUSAGE).getAll();
		LayoutMLParser layoutParser = null;
		final Map<UID, Dimension> mapLayoutMinimumPanelSizes = new HashMap<>();
		for (EntityObjectVO<UID> genAction : nucletDalProvider.getEntityObjectProcessor(E.GENERATION).getAll()) {			
				List<GeneratorUsageVO> genUsages = new ArrayList<GeneratorUsageVO>();
				for (EntityObjectVO<UID> genUsage : allUsages) {
					if (genAction.getPrimaryKey().equals(genUsage.getFieldUid(E.GENERATIONUSAGE.generation))) {
						genUsages.add(
							MasterDataWrapper.getGeneratorUsageVO(new MasterDataVO<UID>(genUsage, false)));
					}
				}
			GeneratorActionVO generatorActionVO = MasterDataWrapper.getGeneratorActionVO(new MasterDataVO<UID>(genAction, false), genUsages, 400, 200);
			if (generatorActionVO.isDialogMode()) {
				Dimension dialogSize = null;
				try {
					// try to get minimum panel size from best matching target layout
					UID bestMatchingLayout = genericObjectMetaDataCache.getBestMatchingLayout(new UsageCriteria(
							genAction.getFieldUid(E.GENERATION.targetModule),
							genAction.getFieldUid(E.GENERATION.targetProcess),
							null,
							null), false);
					Dimension minimumPanelSize = null;
					if (mapLayoutMinimumPanelSizes.containsKey(bestMatchingLayout)) {
						minimumPanelSize = mapLayoutMinimumPanelSizes.get(bestMatchingLayout);
					} else {
						String sLayoutML = genericObjectMetaDataCache.getLayoutML(bestMatchingLayout);
						if (sLayoutML != null) {
							if (layoutParser == null) {
								layoutParser = new LayoutMLParser();
							}
							minimumPanelSize = layoutParser.calculateMinimumPanelSize(new InputSource(new StringReader(sLayoutML)));
							mapLayoutMinimumPanelSizes.put(bestMatchingLayout, minimumPanelSize);
						}
					}
					if (minimumPanelSize != null) {
						dialogSize = minimumPanelSize;
					}
				} catch (Exception ex) {
					LOG.error("get dialog dimension failed: " + ex.getMessage(), ex);
					// ignore here (FinderException) ...
				}
				if (dialogSize != null) {
					// ensure minimum size for a dialog window
					generatorActionVO.setDialogWidth(Integer.max(200, dialogSize.width));
					generatorActionVO.setDialogHeight(Integer.max(50, dialogSize.height));
				}
			}
			lstAllGenerationActionsTemp.add(generatorActionVO);
		}
		lstAllGenerationActions = lstAllGenerationActionsTemp;
		generationsLoaded = true;
	}
	
	@Cacheable(value="generationActions", key="#uid", condition="#uid != null")
	public GeneratorActionVO getGenerationAction(UID uid) {
		GeneratorActionVO retVal = null;
		
		for (GeneratorActionVO curjob : getGeneratorActions()) {
			if (uid.equals(curjob.getId())) {
				retVal = curjob;
				break;
			}
		}
		
		return retVal;
	}
	
	public List<GeneratorActionVO> getGeneratorActions() {
		if (!generationsLoaded) {
			loadGenerations();
		}
		return new ArrayList<GeneratorActionVO>(this.lstAllGenerationActions);
	}

	
	@CacheEvict(value="generationActions", allEntries=true) 
	public void evict() {}
	
	public void invalidate() {
		evict();
		this.lstAllGenerationActions.clear();
		this.generationsLoaded = false;
	}
	
}
