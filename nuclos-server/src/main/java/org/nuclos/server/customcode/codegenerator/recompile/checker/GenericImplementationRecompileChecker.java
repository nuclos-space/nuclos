package org.nuclos.server.customcode.codegenerator.recompile.checker;

import java.util.Collection;

import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Checks if the impl require a recompile.
 */
@Component
public class GenericImplementationRecompileChecker extends CompositeRecompileChecker {

    @Autowired
    private MasterDataFacadeHelper masterDataFacadeHelper;

    public GenericImplementationRecompileChecker() {
        super(new PKRecompileChecker(),
              new UIDFieldRecompileChecker(E.ENTITY_GENERIC_IMPLEMENTATION.implementingEntity),
              new UIDFieldRecompileChecker(E.ENTITY_GENERIC_IMPLEMENTATION.genericEntity));
    }

    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        super.loadRequiredDependentsOf(mdvo);

        // load field map:
        String username =
            SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        Collection<EntityObjectVO<UID>> alltibuteEOs =
            masterDataFacadeHelper.getDependantMasterData(E.ENTITY_GENERIC_FIELDMAPPING.genericImplementation.getUID(),
					username, null, (UID)mdvo.getPrimaryKey());
        mdvo.getDependents().addAllData(E.ENTITY_GENERIC_FIELDMAPPING.genericImplementation, alltibuteEOs);
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(
        MasterDataVO<?> oldImpl,
        MasterDataVO<?> updatedImpl) {

        if (super.isRecompileRequiredOnUpdate(oldImpl, updatedImpl)) {
            return true;
        }

		GenericFieldMappingRecompileChecker fieldChecker = new GenericFieldMappingRecompileChecker();
		final Collection<EntityObjectVO<?>> oldFieldMap = oldImpl.getDependents().getData(DependentDataMap.createDependentKey(E.ENTITY_GENERIC_FIELDMAPPING.genericImplementation));
		final Collection<EntityObjectVO<?>> updFieldMap = updatedImpl.getDependents().getData(DependentDataMap.createDependentKey(E.ENTITY_GENERIC_FIELDMAPPING.genericImplementation));

		for (EntityObjectVO<?> oldEO : oldFieldMap) {
			boolean found = false;
			for (EntityObjectVO<?> updEO : updFieldMap) {
				if (RigidUtils.equal(updEO.getPrimaryKey(), oldEO.getPrimaryKey())) {
					found = true;
					if (fieldChecker.isRecompileRequiredOnUpdate(
							new MasterDataVO(oldEO),
							new MasterDataVO(updEO))) {
						return true;
					}
				}
			}
			if (!found) {
				if (fieldChecker.isRecompileRequiredOnInsertOrDelete(new MasterDataVO(oldEO))) {
					return true;
				}
			}
		}
		for (EntityObjectVO<?> updEO : updFieldMap) {
			boolean found = false;
			for (EntityObjectVO<?> oldEO : oldFieldMap) {
				if (RigidUtils.equal(updEO.getPrimaryKey(), oldEO.getPrimaryKey())) {
					found = true;
				}
			}
			if (!found) {
				if (fieldChecker.isRecompileRequiredOnInsertOrDelete(new MasterDataVO(updEO))) {
					return true;
				}
			}
		}

        return false;
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted force a recompile
        return true;
    }

}
