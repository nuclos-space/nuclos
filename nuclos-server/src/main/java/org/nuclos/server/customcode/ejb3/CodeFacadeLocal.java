//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.ejb3;

import java.util.Collection;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.customcode.valueobject.CodeVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public interface CodeFacadeLocal {

	public MasterDataVO<UID> get(UID pk) throws CommonFinderException, CommonPermissionException;

	MasterDataVO<UID> create(MasterDataVO<UID> vo) throws CommonBusinessException;

	MasterDataVO<UID> create(MasterDataVO<UID> vo, boolean bCompile) throws CommonBusinessException;

	MasterDataVO<UID> modify(MasterDataVO<UID> vo) throws CommonBusinessException;

	MasterDataVO<UID> modify(MasterDataVO<UID> vo, boolean bCompile) throws CommonBusinessException;

	void remove(MasterDataVO<UID> vo) throws CommonBusinessException;

	void remove(MasterDataVO<UID> vo, boolean bCompile) throws CommonBusinessException;

	void check(MasterDataVO<UID> vo) throws CommonBusinessException;

	List<CodeVO> getAll(UID entity) throws CommonPermissionException;

	public void createOrModifyBatch(Collection<MasterDataVO<UID>> colVo) throws CommonBusinessException;

}
