package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to some state must cause a recompile. <p/>
 *
 * @see StatemodelRecompileChecker
 */
@Component
public class StateRecompileChecker extends CompositeRecompileChecker {

    // State*
    // 	- name (english)
    // 	- description (english)
    //      - numeral
    //      - PK
    //      - model (UID of statemodel)

    public StateRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.STATE.name),
              new FieldRecompileChecker<>(E.STATE.description),
              new FieldRecompileChecker<>(E.STATE.numeral),
              new UIDFieldRecompileChecker(E.STATE.model)
        );
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // when a state is added/removed we must recompile
        return true;
    }
}
