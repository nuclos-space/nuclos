package org.nuclos.server.businesstest;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nuclos.server.rest.services.BusinessTestRestService;

/**
 * Used for logging during test execution and generation.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestLogger implements IBusinessTestLogger, AutoCloseable {
	private final BusinessTestRestService.BusinessTestLogWriter writer;

	private boolean debug = false;

	public BusinessTestLogger(final BusinessTestRestService.BusinessTestLogWriter writer) {
		this.writer = writer;
	}

	@Override
	public boolean isDebug() {
		return debug;
	}

	@Override
	public void setDebug(final boolean debug) {
		this.debug = debug;
	}

	public BusinessTestRestService.BusinessTestLogWriter getWriter() {
		return writer;
	}

	@Override
	public void debugln(String message) {
		if (debug) {
			writer.println(message);
		}
	}

	@Override
	public void println(String message) {
		writer.println(message);
	}

	@Override
	public void print(String message) {
		writer.print(message);
	}

	/**
	 * Writes the given message and the Stacktrace of the given Throwable.
	 */
	@Override
	public void printError(final String message, final Throwable t) {
		println("[ERROR]   " + message);
		if (t != null) {
			println(ExceptionUtils.getFullStackTrace(t));
		}
	}

	/**
	 * Reports the current printProgress to the client.
	 *
	 * @param progress, decimal value between 0 and 1.
	 */
	@Override
	public void printProgress(final BigDecimal progress) {
		writer.printProgress(progress);
	}


	/**
	 * @see #printProgress(BigDecimal)
	 */
	@Override
	public void printProgress(final int current, final int max) {
		final BigDecimal progress = new BigDecimal(current).divide(new BigDecimal(max), 4, RoundingMode.HALF_UP);
		writer.printProgress(progress);
	}

	public void close() throws IOException {
		writer.close();
	}
}
