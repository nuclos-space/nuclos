package org.nuclos.server.businesstest.sampledata;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Generates new sample data values based on given sample data or completely at random.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class SampleDataGenerator<T> {
	private Set<T> sampleValues = new HashSet<>();
	protected final Class<T> cls;

	SampleDataGenerator(final Class<T> cls) {
		this.cls = cls;
	}

	/**
	 * Adds sample values, on which the generation of new values is based.
	 */
	public void addSampleValues(Collection<T> values) {
		sampleValues.addAll(values);
	}

	public abstract T newValue();

	public Class<T> getCls() {
		return cls;
	}
}
