package org.nuclos.server.businesstest.sampledata;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Random;

/**
 * Generates a new Double value by picking a random value that lies within the range of the given sample values.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SampleBigDecimalDataGenerator extends SampleDataGenerator<BigDecimal> {
	private final Random random = new Random();

	private BigDecimal min;
	private BigDecimal max;
	private Integer scale;

	SampleBigDecimalDataGenerator() {
		super(BigDecimal.class);
	}

	@Override
	public void addSampleValues(final Collection<BigDecimal> values) {
		for (BigDecimal value : values) {
			if (min == null || value.compareTo(min) < 0) {
				min = value;
			}
			if (max == null || value.compareTo(max) > 0) {
				max = value;
			}
			if (scale == null || scale < value.scale()) {
				scale = value.scale();
			}
		}
	}

	@Override
	public BigDecimal newValue() {
		if (min == null || max == null) {
			return null;
		}

		return min.add(max.subtract(min).multiply(new BigDecimal(random.nextDouble()))).setScale(scale, RoundingMode.HALF_UP);
	}
}
