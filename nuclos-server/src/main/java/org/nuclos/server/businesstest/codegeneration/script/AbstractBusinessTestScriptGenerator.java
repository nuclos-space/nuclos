package org.nuclos.server.businesstest.codegeneration.script;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.NucletVO;
import org.nuclos.server.businesstest.codegeneration.BusinessTestGenerationContext;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptSource;
import org.nuclos.server.businesstest.execution.BusinessTestScript;

import groovy.transform.BaseScript;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class AbstractBusinessTestScriptGenerator {
	protected final EntityMeta<?> meta;
	protected final IBusinessTestNucletCache nucletCache;
	protected final IBusinessTestLogger logger;
	protected final BusinessTestGenerationContext context;

	AbstractBusinessTestScriptGenerator(final EntityMeta<?> meta, final IBusinessTestNucletCache nucletCache, final IBusinessTestLogger logger, final BusinessTestGenerationContext context) {
		this.meta = meta;
		this.nucletCache = nucletCache;
		this.logger = logger;
		this.context = context;
	}

	protected static String escape(final String identifier) {
		return NuclosEntityValidator.escapeJavaIdentifier(identifier);
	}

	public abstract String getTestName();

	public abstract BusinessTestScriptSource generateScriptSource();

	public EntityMeta<?> getMeta() {
		return meta;
	}

	protected IBusinessTestNucletCache getNucletCache() {
		return nucletCache;
	}

	protected NucletVO getNuclet() {
		return getNucletCache().getNuclet(meta.getNuclet());
	}

	protected String getClassName() {
		return escape(meta.getEntityName());
	}

	protected IBusinessTestLogger getLogger() {
		return logger;
	}

	BusinessTestScriptSource createDefaultScript() {
		final NucletVO nuclet = getNuclet();
		final String pkg = nuclet.getPackage();

		return createDefaultScript(pkg);
	}

	public static BusinessTestScriptSource createDefaultScript(final String pkg) {
		final BusinessTestScriptSource script = new BusinessTestScriptSource(pkg);

		if (StringUtils.isNotBlank(pkg)) {
			script.addImport(pkg + ".*");
		}

		script.addImport(BaseScript.class);
		script.addImport(BusinessTestScript.class);

		script.addScriptLine("@BaseScript");
		script.addScriptLine("BusinessTestScript context");
		script.addScriptLine("");
		script.addScriptLine("");

		return script;
	}

	protected BusinessTestGenerationContext getContext() {
		return context;
	}
}
