package org.nuclos.server.businesstest.sampledata;

import java.util.Collection;
import java.util.Random;

/**
 * Generates a new Integer value by picking a random value that lies within the range of the given sample values.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SampleIntegerDataGenerator extends SampleDataGenerator<Integer> {
	private final Random random = new Random();

	private Integer min;
	private Integer max;

	SampleIntegerDataGenerator() {
		super(Integer.class);
	}

	@Override
	public void addSampleValues(final Collection<Integer> values) {
		for (Integer value : values) {
			if (min == null || value < min) {
				min = value;
			}
			if (max == null || value > max) {
				max = value;
			}
		}
	}

	/**
	 * @return
	 */
	@Override
	public Integer newValue() {
		if (min == null || max == null) {
			return null;
		}

		return min + random.nextInt(max - min + 1);
	}
}
