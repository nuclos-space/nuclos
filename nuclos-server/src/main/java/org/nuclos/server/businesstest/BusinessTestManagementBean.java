package org.nuclos.server.businesstest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.businesstest.BusinessTestOverallResult;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.businesstest.codegeneration.BusinessTestNucletCacheBean;
import org.nuclos.server.businesstest.codegeneration.script.AbstractBusinessTestScriptGenerator;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptSource;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Manages business tests.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Service
public class BusinessTestManagementBean {

	private static final Logger LOG = LogManager.getLogger(BusinessTestManagementBean.class);

	@Qualifier("masterDataService")
	private final MasterDataFacadeBean masterDataFacade;

	private final SpringDataBaseHelper dataBaseHelper;

	private final BusinessTestNucletCacheBean nucletCache;

	public BusinessTestManagementBean(
			@Qualifier("masterDataService") final MasterDataFacadeBean masterDataFacade,
			final SpringDataBaseHelper dataBaseHelper,
			final BusinessTestNucletCacheBean nucletCache
	) {
		this.masterDataFacade = masterDataFacade;
		this.dataBaseHelper = dataBaseHelper;
		this.nucletCache = nucletCache;
	}

	public static BusinessTestManagementBean getInstance() {
		return SpringApplicationContextHolder.getBean(BusinessTestManagementBean.class);
	}

	public List<BusinessTestVO> getAll() {
		List<BusinessTestVO> result = new ArrayList<>();

		List<CollectableSorting> sort = Collections.singletonList(new CollectableSorting(E.BUSINESSTEST.name.getUID(), true));
		Collection<MasterDataVO<Object>> mdvos = masterDataFacade.getMasterDataImpl(
				E.BUSINESSTEST.getUID(),
				new CollectableSearchExpression().setSortingOrder(sort),
				true
		);

		mdvos.iterator().forEachRemaining((MasterDataVO<?> mdvo) -> {
			BusinessTestVO test = new BusinessTestVO((MasterDataVO<UID>) mdvo);
			result.add(test);
		});

		return result;
	}

	public BusinessTestVO get(UID testUID) {
		MasterDataVO<UID> mdvo;

		try {
			mdvo = masterDataFacade.get(E.BUSINESSTEST.getUID(), testUID);
		} catch (CommonFinderException | CommonPermissionException e) {
			LOG.warn("Failed to get business test: " + e.getMessage(), e);
			return null;
		}

		return new BusinessTestVO(mdvo);
	}

	/**
	 * Searches for a business test with the given name.
	 */
	public BusinessTestVO get(final String name) {
		TruncatableCollection<MasterDataVO<UID>> masterData = masterDataFacade.getMasterDataImpl(
				E.BUSINESSTEST.getUID(),
				new CollectableSearchExpression(
				SearchConditionUtils.newComparison(
						E.BUSINESSTEST.name,
						ComparisonOperator.EQUAL,
						name
				)),
				true
		);

		if (masterData.size() != 1) {
			LOG.warn("There is no unique business test with the name: " + name);
			return null;
		}

		MasterDataVO<UID> mdvo = masterData.iterator().next();
		return new BusinessTestVO(mdvo);
	}

	/**
	 * Searches for a business test in the given Nuclet with the given name.
	 */
	public BusinessTestVO get(final UID nucletUID, final String name) {
		final CompositeCollectableSearchCondition search = new CompositeCollectableSearchCondition(
				LogicalOperator.AND,
				SearchConditionUtils.newUidComparison(
						E.BUSINESSTEST.nuclet,
						ComparisonOperator.EQUAL,
						nucletUID
				),
				SearchConditionUtils.newComparison(
						E.BUSINESSTEST.name,
						ComparisonOperator.EQUAL,
						name
				)
		);

		TruncatableCollection<MasterDataVO<UID>> masterData = masterDataFacade.getMasterDataImpl(
				E.BUSINESSTEST.getUID(),
				new CollectableSearchExpression(search),
				true
		);

		if (masterData.size() != 1) {
			LOG.warn("There is no unique business test with the name \"" + name + "\" in the nuclet \"" + nucletUID + "\"");
			return null;
		}

		MasterDataVO<UID> mdvo = masterData.iterator().next();
		return new BusinessTestVO(mdvo);
	}

	public void create(BusinessTestVO vo) {
		createDefaultScript(vo);

		try {
			masterDataFacade.create(vo, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		} catch (NuclosBusinessRuleException | CommonCreateException | CommonPermissionException e) {
			throw new RuntimeException("Failed to create business test: " + e.getMessage(), e);
		}
	}

	/**
	 * Creates the default script content, containing e.g. a context variable.
	 */
	private void createDefaultScript(final BusinessTestVO vo) {
		String pkg = null;
		if (vo.getNuclet() != null) {
			final NucletVO nuclet = nucletCache.getNuclet(vo.getNuclet());
			if (nuclet != null) {
				pkg = nuclet.getPackage();
			}
		}

		final BusinessTestScriptSource defaultScript = AbstractBusinessTestScriptGenerator.createDefaultScript(pkg);
		final String groovySource = defaultScript.getGroovySource();

		// TODO: This could use some improvement
		if (!StringUtils.startsWith(vo.getSource(), "package " + defaultScript.getPkg())) {
			final String source = groovySource + StringUtils.trimToEmpty(vo.getSource());
			vo.setSource(source);
		}
	}

	public void modify(BusinessTestVO vo) {
		try {
			masterDataFacade.modify(vo);
		} catch (NuclosBusinessRuleException | CommonCreateException | CommonFinderException | CommonRemoveException | CommonStaleVersionException
				| CommonValidationException | CommonPermissionException e) {
			throw new RuntimeException("Failed to update business test: " + e.getMessage(), e);
		}
	}

	public void delete(BusinessTestVO vo) {
		try {
			masterDataFacade.remove(E.BUSINESSTEST.getUID(), vo.getId(), true);
		} catch (NuclosBusinessRuleException | CommonFinderException | CommonRemoveException | CommonStaleVersionException
				| CommonPermissionException e) {
			throw new RuntimeException("Failed to delete business test: " + e.getMessage(), e);
		}
	}

	/**
	 * Deletes all existing business tests.
	 */
	public void deleteAll() {
		for (BusinessTestVO vo : getAll()) {
			delete(vo);
		}
	}

	/**
	 * Returns the overall result of all executed business tests.
	 */
	public BusinessTestOverallResult getOverallResult() {
		final BusinessTestOverallResult result = new BusinessTestOverallResult();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<Object[]> query = builder.createQuery(Object[].class);
		query.from(E.BUSINESSTEST);

		query.multiselect(
				builder.subSelect(createPkCountSubquery(query)),
				builder.subSelect(createMinStartdateSubquery(query)),
				builder.subSelect(createMaxEnddateSubquery(query)),
				builder.subSelect(createSumDurationSubquery(query)),
				builder.subSelect(createStateCountSubquery(query, BusinessTestVO.STATE.GREEN)),
				builder.subSelect(createStateCountSubquery(query, BusinessTestVO.STATE.YELLOW)),
				builder.subSelect(createStateCountSubquery(query, BusinessTestVO.STATE.RED)),
				builder.subSelect(createSumWarningsSubQuery(query)),
				builder.subSelect(createSumErrorsSubQuery(query))
		).limit(1L);

		List<Object[]> rows = dataBaseHelper.getDbAccess().executeQuery(query);

		if (!rows.isEmpty()) {
			fillResult(result, rows.get(0));
		}

		return result;
	}

	private void fillResult(final BusinessTestOverallResult result, final Object[] row) {
		int i = 0;
		result.setTestsTotal(NumberUtils.toLong("" + row[i++], 0));
		result.setStart((DateTime) row[i++]);
		result.setEnd((DateTime) row[i++]);
		result.setDuration(NumberUtils.toLong("" + row[i++], 0));
		result.setTestsGreen(NumberUtils.toLong("" + row[i++], 0));
		result.setTestsYellow(NumberUtils.toLong("" + row[i++], 0));
		result.setTestsRed(NumberUtils.toLong("" + row[i++], 0));
		result.setWarningCount(NumberUtils.toInt("" + row[i++], 0));
		result.setErrorCount(NumberUtils.toInt("" + row[i++], 0));

		if (result.getTestsRed() > 0) {
			result.setState(BusinessTestVO.STATE.RED);
		} else if (result.getTestsYellow() > 0) {
			result.setState(BusinessTestVO.STATE.YELLOW);
		} else if (result.getTestsGreen() > 0) {
			result.setState(BusinessTestVO.STATE.GREEN);
		}
	}

	/**
	 * Creates a DbQuery to be used as subselect for total counts by STATE of the business tests.
	 */
	private DbQuery<?> createStateCountSubquery(DbQuery<?> query, BusinessTestVO.STATE state) {
		DbQuery<Long> subQuery = query.subquery(Long.class);
		DbFrom<UID> fromCount = subQuery.from(E.BUSINESSTEST);
		subQuery.select(
				query.getBuilder().count(fromCount.baseColumn(E.BUSINESSTEST.getPk()))
		);
		subQuery.where(query.getBuilder().equal(
				fromCount.baseColumn(E.BUSINESSTEST.state),
				query.getBuilder().literal(state)
		));

		return subQuery;
	}

	private DbQuery<?> createPkCountSubquery(DbQuery<?> query) {
		DbQuery<Long> subQuery = query.subquery(Long.class);
		DbFrom<UID> from = subQuery.from(E.BUSINESSTEST);

		subQuery.select(
				subQuery.getBuilder().count(from.baseColumn(E.BUSINESSTEST.getPk()))
		);

		return subQuery;
	}

	private DbQuery<?> createMinStartdateSubquery(DbQuery<?> query) {
		DbQuery<DateTime> subQuery = query.subquery(DateTime.class);
		DbFrom<UID> from = subQuery.from(E.BUSINESSTEST);

		subQuery.selectLiberate(
				subQuery.getBuilder().min(from.baseColumn(E.BUSINESSTEST.startdate))
		);

		return subQuery;
	}

	private DbQuery<?> createMaxEnddateSubquery(DbQuery<?> query) {
		DbQuery<DateTime> subQuery = query.subquery(DateTime.class);
		DbFrom<UID> from = subQuery.from(E.BUSINESSTEST);

		subQuery.selectLiberate(
				subQuery.getBuilder().max(from.baseColumn(E.BUSINESSTEST.enddate))
		);

		return subQuery;
	}

	private DbQuery<?> createSumDurationSubquery(DbQuery<?> query) {
		DbQuery<Long> subQuery = query.subquery(Long.class);
		DbFrom<UID> from = subQuery.from(E.BUSINESSTEST);

		subQuery.selectLiberate(
				subQuery.getBuilder().sum(from.baseColumn(E.BUSINESSTEST.duration))
		);

		return subQuery;
	}

	private DbQuery<?> createSumWarningsSubQuery(DbQuery<?> query) {
		DbQuery<Long> subQuery = query.subquery(Long.class);
		DbFrom<UID> from = subQuery.from(E.BUSINESSTEST);

		subQuery.selectLiberate(
				subQuery.getBuilder().sum(from.baseColumn(E.BUSINESSTEST.warningCount))
		);

		return subQuery;
	}

	private DbQuery<?> createSumErrorsSubQuery(DbQuery<?> query) {
		DbQuery<Long> subQuery = query.subquery(Long.class);
		DbFrom<UID> from = subQuery.from(E.BUSINESSTEST);

		subQuery.selectLiberate(
				subQuery.getBuilder().sum(from.baseColumn(E.BUSINESSTEST.errorCount))
		);

		return subQuery;
	}
}
