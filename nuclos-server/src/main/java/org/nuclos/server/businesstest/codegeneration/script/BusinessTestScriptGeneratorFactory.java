package org.nuclos.server.businesstest.codegeneration.script;

import org.nuclos.common.EntityMeta;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.codegeneration.BusinessTestGenerationContext;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestSampleDataProvider;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Factory for business test script generators.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestScriptGeneratorFactory {
	protected final EntityMeta<?> entity;
	protected final IBusinessTestLogger logger;
	protected final IBusinessTestNucletCache nucletCache;
	protected final BusinessTestGenerationContext context;

	private final IBusinessTestSampleDataProvider sampleDataProvider;

	public BusinessTestScriptGeneratorFactory(
			final EntityMeta<?> entity,
			final IBusinessTestNucletCache nucletCache,
			final IBusinessTestLogger logger,
			final IBusinessTestSampleDataProvider sampleDataProvider,
			final BusinessTestGenerationContext context) {
		this.entity = entity;
		this.logger = logger;
		this.nucletCache = nucletCache;
		this.sampleDataProvider = sampleDataProvider;
		this.context = context;
	}

	public EntityMeta<?> getEntity() {
		return entity;
	}

	public IBusinessTestLogger getLogger() {
		return logger;
	}

	public BusinessTestInsertThinScriptGenerator newInsertThinScriptGenerator() {
		return new BusinessTestInsertThinScriptGenerator(entity, nucletCache, logger, context, sampleDataProvider);
	}

	public BusinessTestInsertFullScriptGenerator newInsertFullScriptGenerator() {
		return new BusinessTestInsertFullScriptGenerator(entity, nucletCache, logger, context, sampleDataProvider);
	}

	public BusinessTestUpdateScriptGenerator newUpdateScriptGenerator() {
		return new BusinessTestUpdateScriptGenerator(entity, nucletCache, logger, context);
	}

	public BusinessTestDeleteScriptGenerator newDeleteScriptGenerator() {
		return new BusinessTestDeleteScriptGenerator(entity, nucletCache, logger, context);
	}

	public BusinessTestCustomRuleScriptGenerator newCustomRuleScriptGenerator(String ruleName) {
		return new BusinessTestCustomRuleScriptGenerator(entity, nucletCache, logger, context, ruleName);
	}

	public BusinessTestStateChangeScriptGenerator newStateChangeScriptGenerator(StateVO sourceState, StateVO targetState) {
		return new BusinessTestStateChangeScriptGenerator(entity, nucletCache, logger, context, sourceState, targetState);
	}
}
