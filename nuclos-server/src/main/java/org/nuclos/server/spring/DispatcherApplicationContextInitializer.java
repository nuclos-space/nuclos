package org.nuclos.server.spring;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Spring WebMVC {@link DispatcherServlet}s are used by Nuclos but their parent context must be set individually because 
 * the spring context on startup is minimal. Refer to {@link AutoDbSetupComplete} for more details.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.0
 */
public class DispatcherApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		try {
			final WebApplicationContext ctx = AutoDbSetupComplete.getMainContext();
			applicationContext.setParent(ctx);
			if (applicationContext instanceof DefaultResourceLoader) {
				((DefaultResourceLoader) applicationContext).setClassLoader(ctx.getClassLoader());
			}
		} catch (InterruptedException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

}
