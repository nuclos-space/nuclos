//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.webservice.ejb3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WebServiceObjectBuilder extends NuclosObjectBuilder {

	public static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.webservice";
	public static final String DEFFAULT_WS_CLASSNAME = "WebServices";
	private static final String DEFFAULT_ENTITY_PREFIX = "WS";
	private static final String DEFFAULT_ENTITY_POSTFIX = "WS";


	@Autowired
	private WebServiceObjectCompiler compiler;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}

	public void createObjects(final ForkJoinPool builderThreadPool) throws CommonBusinessException, InterruptedException {
		List<NuclosBusinessJavaSource> retVal = new ArrayList<>();
		StringBuilder s = new StringBuilder();

		// get all reports
		List<EntityObjectVO<UID>>
			allWS =
			nucletDalProvider.getEntityObjectProcessor(E.WEBSERVICE).getAll();

		final String qname = DEFFAULT_PACKAGE_NUCLET + "." + DEFFAULT_WS_CLASSNAME;
		final String filename = NuclosCodegeneratorUtils
			.webserviceSource(DEFFAULT_PACKAGE_NUCLET, DEFFAULT_WS_CLASSNAME).toString();

		s.append("package " + DEFFAULT_PACKAGE_NUCLET + ";\n\n");
		s.append("import org.nuclos.api.ws.WebServiceObject;\n");
		s.append("import org.nuclos.common.UID;\n\n");

		s.append("public class " + DEFFAULT_WS_CLASSNAME + " {\n\n");

		if (allWS != null && allWS.size() > 0) {
			for (EntityObjectVO<UID> eoVO : allWS) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				final String unformatedEntity = NuclosEntityValidator.escapeJavaIdentifier(
					eoVO.getFieldValue(E.WEBSERVICE.name), DEFFAULT_ENTITY_PREFIX);
				final String formatEntity = unformatedEntity.substring(0, 1).toUpperCase() +
				                            unformatedEntity.substring(1) + DEFFAULT_ENTITY_POSTFIX;
				s.append("\tpublic static WebServiceObject ").append(formatEntity)
					.append(" = new WebServiceObject() {\n\t");
				s.append("public UID getUID() {\n\t\treturn UID.parseUID(\"")
					.append(eoVO.getPrimaryKey().getString()).append("\");\n\t}\n};\n");
			}
		}

		s.append("\n\n}");

		retVal.add(new NuclosBusinessJavaSource(qname, filename, s.toString(), true));

		if (retVal.size() > 0) {
			compiler.compileSourcesAndJar(builderThreadPool, retVal);
		}

	}

}
