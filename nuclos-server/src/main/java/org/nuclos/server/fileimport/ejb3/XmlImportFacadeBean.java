//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport.ejb3;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.objectimport.ImportResult;
import org.nuclos.api.objectimport.XmlImportStructureDefinition;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.HashResourceBundle;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.GeneralJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.LocaleFacadeBean;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.fileimport.Import;
import org.nuclos.server.fileimport.ImportContext;
import org.nuclos.server.fileimport.ImportFactory;
import org.nuclos.server.fileimport.ImportLogger;
import org.nuclos.server.fileimport.ImportStatus;
import org.nuclos.server.fileimport.ImportUtils;
import org.nuclos.server.fileimport.SimpleXPathUtils;
import org.nuclos.server.fileimport.XmlImportJob;
import org.nuclos.server.fileimport.XmlImportStructure;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.NuclosQuartzJob;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.NoTransactionException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

/**
 * Facade for managing and executing imports.
 * Takes care of executing an import with respect to its transaction settings.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@RolesAllowed("Login")
@Transactional(noRollbackFor = { Exception.class })
@Qualifier("XmlImportService")
public class XmlImportFacadeBean extends AbstractImportFacadeBean implements XmlImportFacadeLocal, XmlImportFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(XmlImportFacadeBean.class);
	
	//
	
	private final XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
	
	private final XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance();

	// Spring injection

	@Autowired
	private XmlImportFacadeLocal importFacade;
	
	@Autowired
	private EntityObjectFacadeLocal entityObjectFacade;
	
	@Autowired
	private MetaProvider metaProvider;

	// end of Spring injection

	public XmlImportFacadeBean() {
	}

	@Override
	EntityMeta<UID> getImportStructureEntityMeta() {
		return E.XML_IMPORT;
	}
	
	@Override
	EntityMeta<UID> getImportFileEntityMeta() {
		return E.XMLIMPORTFILE;
	}

	/*
	@Transactional(propagation = Propagation.SUPPORTS, noRollbackFor = { Exception.class })
	public ImportResult doImport(NuclosFile file, boolean isAtomic,
			Class<? extends ImportStructureDefinition>... structureDefClasses) throws BusinessException {

		ImportResult retVal = null;
		ImportMode mode = null;

		GenericObjectDocumentFile goFile = new GenericObjectDocumentFile(file.getName(), file.getContent());
		LocaleInfo info = localeFacade.getDefaultLocale();
		HashResourceBundle bundle = localeFacade.getResourceBundle(info);

		List<XmlImportStructure> structures = new ArrayList<XmlImportStructure>();

		for (Class<? extends ImportStructureDefinition> defClass : structureDefClasses) {
			try {
				XmlImportStructure istr = XmlImportStructure.newXmlImportStructure((UID) defClass.newInstance().getId());

				if (mode == null) {
					if (istr.getMode() != null)
						mode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class, istr.getMode());
				}
				else {
					if (istr.getMode() != null) {
						if (!mode.equals(org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class, istr.getMode())))
							throw new BusinessException(
									"ImportStructureDefinitions contain different import modes. " +
									"Do not mix 'Direct import' and 'Standardimport'. " +
									"This feature is not implemented yet.");
					}
				}
				structures.add(istr);
			}
			catch (Exception e) {
				throw new BusinessException(e);
			}
		}
		java.io.File fileLogger = new java.io.File(
				NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH),
				"ruleImport." + file.getName().substring(0, file.getName().lastIndexOf(".")) + ".log");

		ImportLogger logger = new ImportLogger(fileLogger, bundle);
		// create import instance
		try {
			Import instance = ImportFactory.newXmlImport(
					mode, goFile, null, structures, logger, isAtomic);

			ImportStatus result;
			if (isAtomic) {
				result = doAtomicImport(instance);
			}
			else {
				result = doNonAtomicImport(instance);
			}

			final org.nuclos.common.fileimport.ImportResult ir = result.getImportResult();
			final boolean isSuccessful = ir.getValue() != null ? ir.getValue().equals(org.nuclos.common.fileimport.ImportResult.OK) ? true
					: false
					: false;
			org.nuclos.common.NuclosFile logNuclosFile =
					new org.nuclos.common.NuclosFile(file.getName().substring(0, file.getName().lastIndexOf("."))
							+ ".log", IOUtils.readFromBinaryFile(logger.getFile()));

			retVal = new org.nuclos.api.objectimport.ImportResult(!isSuccessful, result.getMessage(), logNuclosFile);
		}
		catch (NuclosFileImportException e) {
			throw new BusinessException(e);
		}
		catch (IOException e) {
			throw new BusinessException(e);
		}
		return retVal;
	}
	 */

	@Transactional(propagation = Propagation.SUPPORTS, noRollbackFor = { Exception.class })
	public void doImport(ImportContext context) throws NuclosFileImportException {
		MasterDataVO<UID> importfilevo;
		try {
			importfilevo = masterDataFacade
					.getWithDependants(E.XMLIMPORTFILE.getUID(), context.getImportfileId(), null);
		}
		catch (Exception e) {
			throw new NuclosFatalException(e);
		}

		GenericObjectDocumentFile file = (GenericObjectDocumentFile) importfilevo.getFieldValue(E.XMLIMPORTFILE.documentfile.getUID());

		// initialize import logger
		ImportLogger logger;
		try {
			UID logFileUID = importfilevo.getFieldUid(E.IMPORTFILE.logfile);
			if (logFileUID == null) {
				logFileUID = DocumentFileBase.newFileUID();
			}
			GenericObjectDocumentFile logFile = new GenericObjectDocumentFile(file.getFilename().substring(0,
					file.getFilename().lastIndexOf("."))
					+ ".log", logFileUID, new byte[] {});
			importfilevo.setFieldUid(E.XMLIMPORTFILE.logfile, logFileUID);
			importfilevo.setFieldValue(E.XMLIMPORTFILE.logfile.getUID(), logFile);
			masterDataFacade.modify(importfilevo, null);

			// get path for log file
			java.io.File f = new java.io.File(
					NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH),
					logFileUID.getString() + ".log");

			// get translations
			LocaleInfo info = null;
			for (LocaleInfo i: localeFacade.getAllLocales(true)) {
				if (i.getLocale().equals(context.getLocaleId())) {
					info = i;
				}
			}
			if (info == null) {
				info = localeFacade.getDefaultLocale();
			}
			HashResourceBundle bundle = localeFacade.getResourceBundle(info);

			logger = new ImportLogger(f, bundle);
		}
		catch (Exception ex) {
			throw new NuclosFatalException();
		}

		// prepare import structures		
		List<EntityObjectVO<UID>> order = CollectionUtils.sorted(importfilevo.getDependents().<UID>getDataPk(E.IMPORTUSAGE.xmlimportfile),
				new Comparator<EntityObjectVO<UID>>() {
					@Override
					public int compare(EntityObjectVO<UID> o1, EntityObjectVO<UID> o2) {
						Integer index1 = o1.getFieldValue(E.IMPORTUSAGE.order.getUID(), Integer.class);
						Integer index2 = o2.getFieldValue(E.IMPORTUSAGE.order.getUID(), Integer.class);
						return index1.compareTo(index2);
					}
				});

		List<XmlImportStructure> structures = new ArrayList<XmlImportStructure>();
		for (EntityObjectVO<UID> importusage : order) {
			final UID iu = importusage.getFieldUid(E.IMPORTUSAGE.xmlimportfield);
			assert iu != null;
			structures.add(XmlImportStructure.newXmlImportStructure(iu));
		}

		ImportMode mode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class,
				(String) importfilevo.getFieldValue(E.XMLIMPORTFILE.mode));

		boolean atomic = importfilevo.getFieldValue(E.XMLIMPORTFILE.atomic) == null ? false : importfilevo
				.getFieldValue(E.XMLIMPORTFILE.atomic);

		// create import instance
		Import instance = ImportFactory.newXmlImport(mode, file, context, structures, logger, atomic);

		ImportStatus result;
		if (atomic) {
			result = importFacade.doAtomicImport(instance);
		}
		else {
			result = importFacade.doNonAtomicImport(instance);
		}

		importfilevo.setFieldValue(E.XMLIMPORTFILE.laststate, result.getImportResult().getValue());
		importfilevo.setFieldValue(E.XMLIMPORTFILE.result, result.getMessage());

		// @see NUCLOS-1522
		try {
			final GenericObjectDocumentFile logFile = (GenericObjectDocumentFile) importfilevo
					.getFieldValue(E.XMLIMPORTFILE.logfile.getUID());
			UID docFileUID = DocumentFileBase.newFileUID();
			importfilevo.setFieldUid(E.XMLIMPORTFILE.logfile.getUID(), docFileUID);
			importfilevo.setFieldValue(E.XMLIMPORTFILE.logfile.getUID(), new GenericObjectDocumentFile(
					logFile.getFilename(),
					docFileUID,
					IOUtils.readFromBinaryFile(logger.getFile())));
		}
		catch (Exception e) {
			// ignore.
		}

		try {
			masterDataFacade.modify(importfilevo, null);
		}
		catch (Exception e) {
			throw new NuclosFatalException(e);
		}
		finally {
			result.cleanUp(); // trigger finish.
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = { Exception.class })
	public ImportStatus doAtomicImport(Import instance) throws NuclosFileImportException {
		try {
			ImportStatus result = doImportInternal(instance);
			if (result.getImportResult().equals(org.nuclos.common.fileimport.ImportResult.ERROR)) {
				try {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				}
				catch (NoTransactionException e) {
					LOG.error("Unable to do atomic import: ", e);
				}
			}
			return result;
		}
		catch (NuclosFileImportException ex) {
			try {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
			catch (NoTransactionException e) {
				LOG.error("Unable to do atomic import: ", e);
			}
			throw ex;
		}
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, noRollbackFor = { Exception.class })
	public ImportStatus doNonAtomicImport(Import instance) throws NuclosFileImportException {
		return doImportInternal(instance);
	}

	private ImportStatus doImportInternal(Import instance) throws NuclosFileImportException {
		return instance.doImport();
	}

	public void stopImport(UID importfileId) throws NuclosFileImportException {
		try {
			final JobKey jk = JobKey.jobKey(importfileId.toString(), NuclosQuartzJob.JOBGROUP_IMPORT);
			nuclosScheduler.interrupt(jk);
		}
		catch (UnableToInterruptJobException e) {
			throw new NuclosFileImportException("import.exception.stop");
		}
	}

	public String doImport(UID importfileId) throws NuclosFileImportException {
		String correlationId = getImportCorrelationId(importfileId);

		try {
			MasterDataVO<UID> importfile = masterDataFacade.getWithDependants(E.XMLIMPORTFILE.getUID(), importfileId,
					null);
			ImportUtils.validateXmlFileImport(importfile);

			// reset state
			UID newDocFileUID = DocumentFileBase.newFileUID();
			GenericObjectDocumentFile file = (GenericObjectDocumentFile) importfile.getFieldValue(E.XMLIMPORTFILE.documentfile.getUID());
			GenericObjectDocumentFile logFile = new GenericObjectDocumentFile(file.getFilename().substring(0,
					file.getFilename().lastIndexOf("."))
					+ ".log", newDocFileUID, new byte[] {});
			importfile.setFieldUid(E.XMLIMPORTFILE.logfile, newDocFileUID);
			importfile.setFieldValue(E.XMLIMPORTFILE.logfile.getUID(), logFile);
			importfile.setFieldValue(E.XMLIMPORTFILE.laststate, null);
			importfile.setFieldValue(E.XMLIMPORTFILE.result, null);
			masterDataFacade.modify(importfile, null);
		}
		catch (Exception ex) {
			throw new NuclosFileImportException(ex.getMessage(), ex);
		}

		if (StringUtils.isNullOrEmpty(correlationId)) {
			correlationId = UUID.randomUUID().toString();

			try {
				final JobKey jk = JobKey.jobKey(importfileId.getString(), NuclosQuartzJob.JOBGROUP_IMPORT);
				if (nuclosScheduler.getJobDetail(jk) != null) {
					nuclosScheduler.deleteJob(jk);
				}
				final JobDataMap jdm = new JobDataMap();
				jdm.put("ProcessId", correlationId);
				jdm.put("LocaleId",
						SpringApplicationContextHolder.getBean(LocaleFacadeBean.class)
						.getUserLocale().getLocale().getString());
				jdm.put("User", getCurrentUserName());
				final JobBuilder jb = JobBuilder.newJob(XmlImportJob.class).withIdentity(jk).setJobData(jdm)
						.storeDurably();
				final JobDetail jobDetail = jb.build();

				nuclosScheduler.addJob(jobDetail, false);
				nuclosScheduler.triggerJob(jk);
			}
			catch (SchedulerException ex) {
				throw new NuclosFileImportException("import.exception.start");
			}
		}
		return correlationId;
	}
	
	public ImportResult doXmlImport(NuclosFile file, boolean isAtomic, Class<? extends XmlImportStructureDefinition>... structureDefClasses) throws BusinessException {
		ImportResult retVal = null;
		ImportMode mode = null;

		GenericObjectDocumentFile goFile = new GenericObjectDocumentFile(file.getName(), file.getContent());
		LocaleInfo info = localeFacade.getDefaultLocale();
		HashResourceBundle bundle = localeFacade.getResourceBundle(info);

		List<XmlImportStructure> structures = new ArrayList<XmlImportStructure>();

		for (Class<? extends XmlImportStructureDefinition> defClass : structureDefClasses) {
			try {
				XmlImportStructure istr = XmlImportStructure.newXmlImportStructure((UID) defClass.newInstance().getId());

				if (mode == null) {
					if (istr.getMode() != null)
						mode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class, istr.getMode());
				}
				else {
					if (istr.getMode() != null) {
						if (!mode.equals(org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class, istr.getMode())))
							throw new BusinessException(
									"XmlImportStructureDefinitions contain different import modes. " +
									"Do not mix 'Direct import' and 'Standardimport'. " +
									"This feature is not implemented yet.");
					}
				}
				structures.add(istr);
			}
			catch (Exception e) {
				throw new BusinessException(e);
			}
		}
		java.io.File fileLogger = new java.io.File(
				NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH),
				"ruleImport." + file.getName().substring(0, file.getName().lastIndexOf(".")) + ".log");

		// create import instance
		try {
			ImportLogger logger = new ImportLogger(fileLogger, bundle);

			Import instance = ImportFactory.newXmlImport(
					mode, goFile, null, structures, logger, isAtomic);

			ImportStatus result;
			if (isAtomic) {
				result = doAtomicImport(instance);
			}
			else {
				result = doNonAtomicImport(instance);
			}

			final org.nuclos.common.fileimport.ImportResult ir = result.getImportResult();
			final boolean isSuccessful = ir.getValue() != null ? ir.getValue().equals(org.nuclos.common.fileimport.ImportResult.OK) ? true
					: false
					: false;
			org.nuclos.common.NuclosFile logNuclosFile =
					new org.nuclos.common.NuclosFile(file.getName().substring(0, file.getName().lastIndexOf("."))
							+ ".log", IOUtils.readFromBinaryFile(logger.getFile()));

			retVal = new ImportResult(!isSuccessful, result.getMessage(), logNuclosFile);
		}
		catch (NuclosFileImportException e) {
			throw new BusinessException(e);
		}
		catch (IOException e) {
			throw new BusinessException(e);
		}
		return retVal;
	}

	@Override
	void validateImportStructure(MasterDataVO<UID> importStructure) throws CommonValidationException {
		ImportUtils.validateXmlImportStructure(importStructure);
	}

	@Override
	void validateFileImport(MasterDataVO<UID> fileImport) throws CommonValidationException {
		ImportUtils.validateXmlFileImport(fileImport);
	}

	@Override
	public ProxyList<UID, EntityObjectVO<UID>> getFileImportsForEntity(UID entity) {
		/*
		final String sql = "select if.* " +
				"from t_md_xmlimportfile xif " +
				"join t_md_importusage iu on xif.struid = iu.struid_t_md_xmlimportfile " +
				"join t_md_xmlimport i on i.struid = iu.struid_t_md_xmlimport " +
				"where i.struid_t_ad_masterdata = '" + entity.toString() + "'";
		final CollectableSearchCondition cond = new PlainSubCondition(sql);
		 */
		// final CollectableSearchCondition cond1 = new RefJoinCondition(E.IMPORTUSAGE.xmlimportfile, "iu");
		final CollectableSearchCondition cond1 = new GeneralJoinCondition(
				E.XMLIMPORTFILE.getPk(), "t", E.IMPORTUSAGE.xmlimportfile, "iu");
		final CollectableSearchCondition cond2 = new GeneralJoinCondition(
				E.IMPORTUSAGE.xmlimportfield, "iu", E.XML_IMPORT.getPk(), "i");
		final CollectableSearchCondition cond3 = SearchConditionUtils.newComparison(
				E.XML_IMPORT.entity, ComparisonOperator.EQUAL, entity.toString());
		final CollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.AND, cond1, cond2, cond3);		
		
		final CollectableSearchExpression se = new CollectableSearchExpression(cond);
		Collection<UID> fields = new HashSet<UID>();
		for (FieldMeta<?> fm : E.XMLIMPORTFILE.getFields()) {
			fields.add(fm.getUID());
		}
		return entityObjectFacade.getEntityObjectProxyListNoCheck(E.XMLIMPORTFILE.getUID(), se, fields, null);
	}

	@Override
	public byte[] exportFile(String rootTag, UID xmlImportFile, UID entity, CollectableSearchCondition cond) throws CommonPermissionException, XMLStreamException, IOException {
		final CollectableComparison importUsagesCond = SearchConditionUtils.newUidComparison(
				E.IMPORTUSAGE.xmlimportfile, ComparisonOperator.EQUAL, xmlImportFile);
		final CollectableSorting importUsagesSort = new CollectableSorting("t", E.IMPORTUSAGE.getUID(), true, E.IMPORTUSAGE.order.getUID(), true);
		final CollectableSearchExpression importUsagesExpr = new CollectableSearchExpression(importUsagesCond, importUsagesSort);
		final long importUsagesCount = entityObjectFacade.countEntityObjectRowsNoCheck(E.IMPORTUSAGE.getUID(), importUsagesExpr);
		if (importUsagesCount > 200) {
			throw new IllegalArgumentException();
		}
		final Collection<EntityObjectVO<UID>> importUsages = entityObjectFacade.getEntityObjectsChunkNoCheck(
				E.IMPORTUSAGE.getUID(), importUsagesExpr, new ResultParams(importUsagesCount, true), null);
		
		// retrieve meta data
		final Multimap<UID,EntityObjectVO<UID>> e2XmlImport = ArrayListMultimap.create();
		final Multimap<UID,EntityObjectVO<UID>> e2XmlImportAttr = ArrayListMultimap.create();
		final Multimap<UID,UID> e2f = ArrayListMultimap.create();
		final Multimap<UID,FieldMeta<?>> esReferredByField = ArrayListMultimap.create();
		final Multimap<UID,EntityObjectVO<UID>> f2XmlImportFieldIdentifier = ArrayListMultimap.create();
		final Map<UID,EntityObjectVO<UID>> f2XmlImportAttr = new HashMap<UID, EntityObjectVO<UID>>();
		for (EntityObjectVO<UID> iu: importUsages) {
			// find entity referenced from importUsage
			final EntityObjectVO<UID> xmlImport = entityObjectFacade.get(
					E.XML_IMPORT.getUID(), iu.getFieldUid(E.IMPORTUSAGE.xmlimportfield));
			final UID e = xmlImport.getFieldUid(E.XML_IMPORT.entity);
			e2XmlImport.put(e, xmlImport);
			
			// find xml import attributes referenced
			final CollectableComparison xiasCond = SearchConditionUtils.newUidComparison(
					E.XMLIMPORTATTRIBUTE.importfield.getUID(), ComparisonOperator.EQUAL, xmlImport.getPrimaryKey());
			final CollectableSearchExpression xiasExpr = new CollectableSearchExpression(xiasCond);
			final long xiasCount = entityObjectFacade.countEntityObjectRowsNoCheck(E.XMLIMPORTATTRIBUTE.getUID(), xiasExpr);
			if (xiasCount > 200) {
				throw new IllegalArgumentException();
			}
			final Collection<EntityObjectVO<UID>> xias = entityObjectFacade.getEntityObjectsChunkNoCheck(
					E.XMLIMPORTATTRIBUTE.getUID(), xiasExpr, new ResultParams(xiasCount, true), null);
			e2XmlImportAttr.putAll(e, xias);
			
			for (EntityObjectVO<UID> xia: xias) {
				// find import field identifier
				final CollectableComparison ifCond = SearchConditionUtils.newUidComparison(
						E.XMLIMPORTFEIDENTIFIER.importattribute.getUID(), ComparisonOperator.EQUAL, xia.getPrimaryKey());
				final CollectableSearchExpression ifExpr = new CollectableSearchExpression(ifCond);
				final long ifCount = entityObjectFacade.countEntityObjectRowsNoCheck(E.XMLIMPORTFEIDENTIFIER.getUID(), ifExpr);
				if (ifCount > 200) {
					throw new IllegalArgumentException();
				}
				final Collection<EntityObjectVO<UID>> ifs = entityObjectFacade.getEntityObjectsChunkNoCheck(
						E.XMLIMPORTFEIDENTIFIER.getUID(), ifExpr, new ResultParams(ifCount, false), null);
				final UID f = xia.getFieldUid(E.XMLIMPORTATTRIBUTE.attribute);
				e2f.put(e, f);
				f2XmlImportFieldIdentifier.putAll(f, ifs);
				f2XmlImportAttr.put(f, xia);
			}
		}
		
		// construct 'backward' references
		final Set<UID> exportEntities = new HashSet<UID>(e2f.keys());
		for (UID e: exportEntities) {
			final EntityMeta<Long> em = metaProvider.getEntity(e);
			for (FieldMeta<?> fm: em.getFields()) {
				final UID fe = fm.getForeignEntity();
				// but only if we export the foreign entity
				if (fe != null && exportEntities.contains(fe) && !esReferredByField.get(fe).contains(fm)) {
					esReferredByField.put(fe, fm);
				}
			}
		}
		
		final XmlExportMetaData xemd = new XmlExportMetaData();
		xemd.setE2XmlImport(e2XmlImport);
		xemd.setE2XmlImportAttr(e2XmlImportAttr);
		xemd.setE2f(e2f);
		xemd.setEsReferredByField(esReferredByField);
		xemd.setF2XmlImportFieldIdentifier(f2XmlImportFieldIdentifier);
		xemd.setF2XmlImportAttr(f2XmlImportAttr);
		
		// retrieve (base) entity data to write as xml
		// final EntityObjectVO<UID> xif = entityObjectFacade.get(E.XMLIMPORTFILE.getUID(), xmlImportFile);
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final XMLStreamWriter writer = new IndentingXMLStreamWriter(
				xmlOutputFactory.createXMLStreamWriter(out));
		try {
			writer.writeStartDocument();
			writer.writeStartElement(rootTag);
			
			final CollectableSearchExpression expr = new CollectableSearchExpression(cond);
			final long count = entityObjectFacade.countEntityObjectRowsNoCheck(entity, expr);
			final Collection<EntityObjectVO<Long>> resultList = entityObjectFacade.getEntityObjectsChunkNoCheck(
					entity, expr, new ResultParams(count, true), null);
			for (EntityObjectVO<Long> eo: resultList) {
				exportEntity(writer, xemd, eo);
			}
			
			// close rootTag
			writer.writeEndElement();
			writer.writeEndDocument();
			
			out.flush();
			return out.toByteArray();
		} finally {
			writer.close();
			out.close();
		}
	}
	
	private void exportEntity(XMLStreamWriter writer, XmlExportMetaData xemd, EntityObjectVO<Long> entity) throws XMLStreamException, CommonPermissionException {
		final UID entityMetaUid = entity.getDalEntity();
		final EntityMeta<UID> entityMeta = metaProvider.getEntity(entityMetaUid);
		if (xemd.getE2XmlImport().get(entityMetaUid).size() != 1) {
			throw new IllegalStateException();
		}
		final EntityObjectVO<UID> xmlImport = CollectionUtils.getUnique(xemd.getE2XmlImport().get(entityMetaUid));
		final String xpath = xmlImport.getFieldValue(E.XML_IMPORT.match);
		writeStartXPath(writer, xpath);
		writer.writeAttribute("id", Long.toString(entity.getPrimaryKey()));
		
		// 'backward' references for inner tags
		final Collection<FieldMeta<?>> fes = xemd.getEsReferredByField().get(entityMetaUid);
		for (FieldMeta<?> referencingField: fes) {
			final CollectableComparison feCond = SearchConditionUtils.newIdComparison(
					referencingField.getUID(), ComparisonOperator.EQUAL, entity.getPrimaryKey());
			final CollectableSearchExpression feExpr = new CollectableSearchExpression(feCond);
			final Long feCount = entityObjectFacade.countEntityObjectRowsNoCheck(referencingField.getEntity(), feExpr);
			final Collection<EntityObjectVO<Long>> inners = entityObjectFacade.getEntityObjectsChunkNoCheck(
					referencingField.getEntity(), feExpr, new ResultParams(feCount, true), null);
			for (EntityObjectVO<Long> i: inners) {
				// recursive call for inner entities
				exportEntity(writer, xemd, i);
			}
		}
		
		for (UID f: xemd.getE2f().get(entityMetaUid)) {
			final EntityObjectVO<UID> importAttr = xemd.getF2XmlImportAttr().get(f);
			final String fieldXpath = importAttr.getFieldValue(E.XMLIMPORTATTRIBUTE.match);
			final Pair<Integer,String> plap = SimpleXPathUtils.getParentLevelAndPath(fieldXpath);
			
			// TODO: currently we ignore 'outer' references
			if (plap != null && plap.getX().intValue() > 0) continue;
			
			final Collection<EntityObjectVO<UID>> importFieldIdentifiers = xemd.getF2XmlImportFieldIdentifier().get(f);
			final FieldMeta<?> field = metaProvider.getEntityField(f);
			final UID ref = field.getForeignEntity();
			final String value;
			if (ref == null) {
				final Object v = entity.getFieldValue(f);
				if (v != null) {
					// TODO
					value = v.toString();
				} else {
					value = "";
				}
				writeValueAtXPath(writer, fieldXpath, value);
			} else {
				final Long refId = entity.getFieldId(f);
				if (refId != null) {
					// TODO
					value = refId.toString();
					writeValueAtXPath(writer, fieldXpath, value);
				} else {
					value = "";
				}
			}
		}
		writeEndXPath(writer, xpath);
	}
	
	private void writeStartXPath(XMLStreamWriter writer, String xpath) throws XMLStreamException {
		for (String elem: SimpleXPathUtils.path(xpath)) {
			if (SimpleXPathUtils.isAttribute(elem)) {
				// writer.writeAttribute(elem.substring(1), value);
				throw new IllegalStateException(xpath);
			} else {
				writer.writeStartElement(elem);
			}
		}
	}
	
	private void writeEndXPath(XMLStreamWriter writer, String xpath) throws XMLStreamException {
		for (String elem: SimpleXPathUtils.path(xpath)) {
			if (SimpleXPathUtils.isAttribute(elem)) {
				// writer.writeAttribute(elem.substring(1), value);
				throw new IllegalStateException(xpath);
			} else {
				writer.writeEndElement();
			}
		}
	}

	private void writeValueAtXPath(XMLStreamWriter writer, String xpath, String value) throws XMLStreamException {
		final Iterator<String> it = SimpleXPathUtils.pathIterator(new StringBuilder(xpath));
		while (it.hasNext()) {
			final String elem = it.next();
			final boolean hasNext = it.hasNext();
			final boolean isAttr = SimpleXPathUtils.isAttribute(elem);
			if (isAttr) {
				if (hasNext) {
					throw new IllegalStateException(xpath);
				} else {
					writer.writeAttribute(elem, value);
				}
			} else {
				writer.writeStartElement(elem);
			}
			if (!hasNext) {
				writer.writeCharacters(value);
				if (!isAttr) {
					writer.writeEndElement();
				}
			}
		}
	}
	
}
