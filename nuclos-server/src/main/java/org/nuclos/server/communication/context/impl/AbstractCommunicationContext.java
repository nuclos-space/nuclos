package org.nuclos.server.communication.context.impl;

import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.context.communication.CommunicationContext;
import org.slf4j.LoggerFactory;

public abstract class AbstractCommunicationContext implements
		CommunicationContext {

	private final CommunicationPort port;
	
	private final String logClassname;
	
	public AbstractCommunicationContext(CommunicationPort port, String logClassname) {
		this.port = port;
		this.logClassname = logClassname;
	}
	
	@Override
	public CommunicationPort getPort() {
		return port;
	}
	
	@Override
	public void log(String message) {
		LoggerFactory.getLogger(this.logClassname).info(message);
	}
	
	@Override
	public void logWarn(String message) {
		LoggerFactory.getLogger(this.logClassname).warn(message);
	}
	
	@Override
	public void logError(String message, Exception ex) {
		LoggerFactory.getLogger(this.logClassname).error(message, ex);
	}

}
