//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.communication.context.impl;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.common.NuclosUserCommunicationAccount;
import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.communication.response.PhoneCallResponse;
import org.nuclos.api.context.communication.PhoneCallRequestContext;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.communication.response.PhoneCallResponseImpl;

public class PhoneCallRequestContextImpl extends AbstractCommunicationContext implements PhoneCallRequestContext {
	
	private String toNumber;
	
	private NuclosUserCommunicationAccount userAccount;
	
	private BusinessObject<?> businessObject;
	
	private EntityObjectVO<?> eo;
	
	private PhoneCallResponse response;

	public PhoneCallRequestContextImpl(CommunicationPort port, String logClassname) {
		super(port, logClassname);
	}
	
	/**
	 * 
	 * @param toNumber
	 */
	@Override
	public void setToNumber(String toNumber) {
		this.toNumber = toNumber;
	}
	
	/**
	 * 
	 * @param userAccount
	 */
	@Override
	public void setNuclosUserAccount(NuclosUserCommunicationAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	/**
	 * 
	 * @param businessObject
	 */
	@Override
	public <T extends BusinessObject<?>> void setBusinessObject(T businessObject) {
		this.businessObject = businessObject;
	}
	
	/**
	 * 
	 * @return
	 * 		the called phone number
	 */
	@Override
	public String getToNumber() {
		return toNumber;
	}

	/**
	 * 
	 * @return
	 * 		the calling user (account) 
	 */
	@Override
	public NuclosUserCommunicationAccount getNuclosUserAccount() {
		return userAccount;
	}
	
	/**
	 * 
	 * @return
	 * 		the business object the number belongs to
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T extends BusinessObject<?>> T getBusinessObject(Class<T> t) {
		return (T) businessObject;
	}

	/**
	 * @return
	 * 		overrides old response with a new one
	 */
	@Override
	public PhoneCallResponse clearResponse() {
		response = new PhoneCallResponseImpl();
		return response;
	}

	/**
	 * if no response is present clearResponse is called automatically
	 * 
	 * @return
	 * 		the response
	 */
	@Override
	public PhoneCallResponse getResponse() {
		if (response == null) {
			return clearResponse();
		}
		return response;
	}

	/**
	 * internal use only
	 * @return 
	 * 		true, if a response object is created via {@link #clearResponse()} or {@link #getResponse()} before
	 */
	@Override
	public boolean isResponse() {
		return response != null;
	}

	/**
	 * internal use only
	 * @param eo
	 */
	public void setEntityObject(EntityObjectVO<?> eo) {
		this.eo = eo;
	}
	
	/**
	 * internal use only
	 * @return
	 */
	public EntityObjectVO<?> getEntityObject() {
		return eo;
	}
	
	@Override
	public String toString() {
		return "PhoneCallRequestContext["
				+ "toNumber=" + toNumber
				+ ", userAccount=" + userAccount
				+ ", businessObject=" + businessObject
				+ ", response=" + response
				+ "]";
	}

}
