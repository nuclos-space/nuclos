//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityLafParameterVO;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityLafParameterProcessor;
import org.nuclos.server.dblayer.DbException;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;

@Configurable
public class EntityLafParameterProcessor extends AbstractJdbcDalProcessor<EntityLafParameterVO, UID> 
	implements IEntityLafParameterProcessor {
	
	private final IColumnToVOMapping<UID, UID> idColumn;
	
	public EntityLafParameterProcessor(List<IColumnToVOMapping<? extends Object, UID>> allColumns, 
			IColumnToVOMapping<UID, UID> idColumn) {
		super(E.ENTITYLAFPARAMETER.getUID(), EntityLafParameterVO.class, UID.class, allColumns);
		this.idColumn = idColumn;
	}
	
	@Override
	public EntityMeta<UID> getMetaData() {
		return E.ENTITYLAFPARAMETER;
	}
	
	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return idColumn;
	}

	@Override
	public List<EntityLafParameterVO> getAll() {
		return super.getAll();
	}

	@Override
	public void delete(Delete<UID> id) throws DbException {
		super.delete(id);
	}

	@Override
	public EntityLafParameterVO getByPrimaryKey(UID id) {
		return super.getByPrimaryKey(id);
	}
	
	@Override
	public List<EntityLafParameterVO> getByPrimaryKeys(List<UID> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

	@Override
	public Object insertOrUpdate(EntityLafParameterVO dalVO) {
		return super.insertOrUpdate(dalVO);
	}
	
	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<UID> dalVO) throws DbException {
		throw new NotImplementedException();
	}

}
