package org.nuclos.server.dal.processor.nuclet;

import org.nuclos.common.NucletEntityContext;
import org.nuclos.common.UID;
import org.nuclos.server.dal.specification.IDalReadSpecification;

public interface JdbcEntityContextProcessor extends
		IDalReadSpecification<NucletEntityContext, UID> {
}
