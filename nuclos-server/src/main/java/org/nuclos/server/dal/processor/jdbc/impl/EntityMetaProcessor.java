//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityMetaDataProcessor;
import org.nuclos.server.dblayer.DbException;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;

@Configurable
public class EntityMetaProcessor extends AbstractJdbcDalProcessor<NucletEntityMeta, UID> 
	implements JdbcEntityMetaDataProcessor {
	
	private final IColumnToVOMapping<UID, UID> uidColumn;
	
	public EntityMetaProcessor(List<IColumnToVOMapping<? extends Object, UID>> allColumns, 
			IColumnToVOMapping<UID, UID> uidColumn) {
		super(E.ENTITY.getUID(), NucletEntityMeta.class, UID.class, allColumns);
		this.uidColumn = uidColumn;
	}
	
	@Override
	public EntityMeta<UID> getMetaData() {
		return E.ENTITY;
	}
	
	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return uidColumn;
	}

	@Override
	public void delete(Delete<UID> id) throws DbException {
		super.delete(id);
	}

	@Override
	public List<NucletEntityMeta> getAll() {
		return super.getAll();
	}

	@Override
	public NucletEntityMeta getByPrimaryKey(UID uid) {
		return super.getByPrimaryKey(uid);
	}
	
	@Override
	public List<NucletEntityMeta> getByPrimaryKeys(List<UID> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

	@Override
	public Object insertOrUpdate(NucletEntityMeta dalVO) {
		return super.insertOrUpdate(dalVO);
	}

	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<UID> dalVO) throws DbException {
		throw new NotImplementedException();
	}
	
	@Override
	protected void setDebugInfo(NucletEntityMeta dalVO) {
		if (dalVO.getUID() != null) {
			dalVO.getUID().setDebugInfo(dalVO.getEntityName());
		}
   	}
	
}
