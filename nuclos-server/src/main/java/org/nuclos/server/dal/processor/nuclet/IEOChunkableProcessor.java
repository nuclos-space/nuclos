package org.nuclos.server.dal.processor.nuclet;

import java.util.List;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;

public interface IEOChunkableProcessor<PK> {

	List<EntityObjectVO<PK>> getChunkBySearchExpressionImpl(CollectableSearchExpression clctexpr, ResultParams resultParams);

}
