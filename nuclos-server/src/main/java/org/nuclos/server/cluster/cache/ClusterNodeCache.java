//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.cluster.cache;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.server.cluster.ClusterNode;
import org.nuclos.server.cluster.NewMasterNodeStrategy;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.nuclos.server.cluster.OldestNodeStrategy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component
public class ClusterNodeCache implements InitializingBean, ApplicationContextAware, Serializable {
	
	private final Collection<ClusterNode> colNodes = new HashSet<ClusterNode>();
	private NewMasterNodeStrategy newMasterNodeStrategy = new OldestNodeStrategy();
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
				
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		
	}
	
	public void invalidate() {
		colNodes.addAll(NuclosClusterRegisterHelper.getAllNodes());
	}
	
	public void init() {
		colNodes.addAll(NuclosClusterRegisterHelper.getAllNodes());
	}
	
	public void addNode(ClusterNode node) {
		colNodes.add(node);
	}
	
	public void removeNode(ClusterNode node) {
		colNodes.remove(node);
	}
	
	public Collection<ClusterNode> getNodes() {
		return colNodes;
	}
	
	public Collection<ClusterNode> getNotConnectedNodes() {
		return CollectionUtils.select(colNodes, new Predicate<ClusterNode>() {

			@Override
			public boolean evaluate(ClusterNode node) {
				if(NuclosClusterRegisterHelper.getServerIp().equals(node.getServerip())) {
					return false;
				}
				return !node.isConnected();
			}
			
		});
	}
	
	public ClusterNode getNewMasterNode() {		
		return newMasterNodeStrategy.getNewMasterNode();
	}
	
}
