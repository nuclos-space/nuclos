package org.nuclos.server.cluster.jms;

import java.io.Serializable;

/**
 * all action to be executed on a cluster node
 * implement this interface
 * @see ClusterActionFactory
 */
public interface NuclosClusterAction extends Serializable {

	enum Type {
		 PARAMTER_ACTION,
		 STATE_ACTION,
		 RESOURCE_ACTION,
		 ATTRIBUTE_ACTION,
		 SCHEMA_ACTION,
		 RULE_ACTION,
		 MASTERDATAMETA_ACTION,
		 DATASOURCE_ACTION,
		 SECURITY_ACTION,
		 LOCALE_ACTION,
		 MODULES_ACTION,
		 METADATA_ACTION,
		 LUCENIAN_ACTION,
		 STARTUP_ACTION,
		 SERVERKEEPALIVE_ACTION,
		 SHUTDOWN_ACTION,
		 RAISETOMASTER_ACTION,
		 SPRINGCACHE_ACTION,
		 METAPROVIDER_ACTION
	}
	
	public void doAction();
	
	public boolean doOnMaster();

}
