//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.jms;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.cluster.cache.NuclosSpringClusterCache;
import org.springframework.cache.Cache;
import org.springframework.cache.support.CompositeCacheManager;


public class SpringCacheClusterAction implements NuclosClusterAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7634916521489028139L;
	private final String cacheName;
	private final Object key;
	private final boolean bClear;
	
	public SpringCacheClusterAction(String cacheName, Object key, boolean bClear) {
		this.cacheName = cacheName;
		this.key = key;
		this.bClear = bClear;
	}

	@Override
	public void doAction() {
		CompositeCacheManager cacheManager = (CompositeCacheManager) SpringApplicationContextHolder.getBean("cacheManager");
		Cache cache = cacheManager.getCache(cacheName);
		((NuclosSpringClusterCache)cache).setParameterForClusterCloud(false, false);
		if(bClear) {
			cache.clear();
		}
		else {
			cache.evict(key);
		}
	}

	@Override
	public boolean doOnMaster() {
		return true;
	}

}
