//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice.printout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.api.service.PrintoutService;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.ejb3.ReportFacadeLocal;
import org.nuclos.server.ruleengine.NuclosFatalRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("printoutServiceProvider")
public class PrintoutServiceProvider implements PrintoutService {

	private static final Logger LOG = LoggerFactory.getLogger(PrintoutServiceProvider.class);
	
	@Autowired
	DataLanguageCache dataLangCache;
	
	DatasourceFacadeRemote dsFacadeRemote;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Override
	public NuclosFile run(OutputFormat format, Long iBusinessObjectId)
			throws BusinessException {
		return this.run(format, iBusinessObjectId, null, null);
	}

	private static class ReportExecutionException {
		
		private Exception exc;
		
		public ReportExecutionException() {
			exc = null;
		}
		public void setException(Exception e) {
			this.exc = e;
		}
		
		public boolean hasException() {
			return this.exc != null;
		}
		
		public Exception getException() {
			return this.exc;
		}
	}

	@Override
	public NuclosFile run(OutputFormat format, Long iBusinessObjectId,
			NuclosLocale locale) throws BusinessException {
		return run(format, iBusinessObjectId, null, locale);
	}

	@Override
	public NuclosFile run(OutputFormat format, Long iBusinessObjectId,
			Map<String, Object> params)
			throws BusinessException {

		if (format == null) {
			throw new NuclosFatalRuleException("ReportOutputFormat must not be null");
		}
		return run((UID)format.getId(), iBusinessObjectId, params, null);
	}
	
	public NuclosFile run(final UID formatUID, final Long iBusinessObjectId,
			Map<String, Object> params, NuclosLocale locale) throws BusinessException {
		
		if (formatUID == null) {
			throw new NuclosFatalRuleException("formatUID must not be null");
		}
		if (iBusinessObjectId == null) {
			throw new NuclosFatalRuleException("iBusinessObjectId must not be null");
		}
		
		final Map<String, Object> repoParam = params != null ? params : new HashMap<String, Object>();
		
		repoParam.put("intid", iBusinessObjectId);
		repoParam.put("iGenericObjectId", iBusinessObjectId);
		
		
		final List<NuclosFile> retVal = new ArrayList<NuclosFile>();
			
		final ReportFacadeLocal reportFacade = 
				ServerServiceLocator.getInstance().getFacade(ReportFacadeLocal.class);		

		final EntityObjectVO<UID> reportOutput = 
				NucletDalProvider.getInstance().getEntityObjectProcessor(E.REPORTOUTPUT).getByPrimaryKey((UID) formatUID);
		final ReportOutputVO reportOutputVO = MasterDataWrapper.getReportOutputVO(new MasterDataVO<UID>(reportOutput, true));
	
		final EntityObjectVO<UID> report = 
				NucletDalProvider.getInstance().getEntityObjectProcessor(
						E.REPORT).getByPrimaryKey(reportOutputVO.getReportUID());

		UID foundlanguage = null;
		
		try {			
			if (locale == null) {
				foundlanguage = getDsFacadeRemote().getReportLanguageToUse(report.getPrimaryKey(), 
						dataLangCache.getLanguageToUse(), iBusinessObjectId);				
			} else {
				foundlanguage = new UID(locale.toString());
			}			
		} catch (NuclosReportException e) {
			throw new CommonFatalException(e);
		}
		
		final UID language = foundlanguage;
		
		if (language != null) {
			repoParam.put("dataLocale", language.toString());
		}
		
		final DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
		final String fileName = report.getFieldValue(E.REPORT.name) + "_" + dateformat.format(Calendar.getInstance(Locale.getDefault()).getTime());
		
		final ReportExecutionException exceptionExecution = new ReportExecutionException();
		final Runnable r = new Runnable() {
			@Override
			public void run() {
				try {
					NuclosFile file = reportFacade.prepareReport(formatUID, repoParam, null, language, getMandatorUID(reportOutputVO.getReportUID(), iBusinessObjectId));
					retVal.add(new org.nuclos.common.NuclosFile(
							file.getName() != null ? file.getName() : fileName + reportOutputVO.getFormat().getExtension(), file.getContent()));
				}
				catch (CommonBusinessException e) {
					exceptionExecution.setException(e);
				}
			}
		};
		r.run();

		if (exceptionExecution.hasException())
			throw new BusinessException(exceptionExecution.getException());
		
		if (retVal.size() == 0)
			throw new BusinessException("No files found for Printout " + report.getFieldValue(E.REPORT.name));
		
		return retVal.get(0);
	}

	private DatasourceFacadeRemote getDsFacadeRemote() {
		if (dsFacadeRemote == null)
			dsFacadeRemote = SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class);
		
		return dsFacadeRemote;
	}

	@Override
	public NuclosFile run(OutputFormat format, Long iBusinessObjectId,
			Map<String, Object> params, NuclosLocale locale)
			throws BusinessException {
		return run((UID)format.getId(),iBusinessObjectId, params, locale);
	}
	
	private UID getMandatorUID(UID reportUID, Long boId) {
		UID result = null;
		if (SecurityCache.getInstance().isMandatorPresent()) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> entityUsageQuery = builder.createQuery(UID.class);
			DbFrom<UID> p = entityUsageQuery.from(E.FORMUSAGE);
			entityUsageQuery.select(p.baseColumn(E.FORMUSAGE.module));
			entityUsageQuery.where(builder.equalValue(p.baseColumn(E.FORMUSAGE.form), reportUID));
			List<UID> allEntityUsages = dataBaseHelper.getDbAccess().executeQuery(entityUsageQuery);
			
			Map<UID, UID> resultFromUsage = new HashMap<UID, UID>();
			for (UID entityUID : new HashSet<UID>(allEntityUsages)) {	
				if (!resultFromUsage.containsKey(entityUID)) {
					EntityMeta<Long> eMeta = MetaProvider.getInstance().getEntity(entityUID);
					UID qResult = null;
					if (eMeta.isMandator()) {
						DbQuery<UID> mandatorQuery = builder.createQuery(UID.class);
						DbFrom<Long> bo = mandatorQuery.from(eMeta);
						mandatorQuery.select(bo.baseColumn(SF.MANDATOR_UID));
						mandatorQuery.where(builder.equalValue(bo.basePk(), boId));
						try {
							qResult = dataBaseHelper.getDbAccess().executeQuerySingleResult(mandatorQuery);
						} catch (DbInvalidResultSizeException ex) {
							// bo not found? try other usages...
						}
					}
					resultFromUsage.put(entityUID, qResult);
				}
			}
			// remove null mandators...
			for (Iterator<Entry<UID, UID>> it = resultFromUsage.entrySet().iterator(); it.hasNext(); ) {
				if (it.next().getValue() == null) {
					it.remove();
				}
			}
			switch (resultFromUsage.size()) {
			case 0:
				result = NucletDalProvider.getInstance().getAccessibleMandatorsOrigin();
				break;
			case 1:
				result = resultFromUsage.values().iterator().next();
				break;
			default:
				result = NucletDalProvider.getInstance().getAccessibleMandatorsOrigin();
				LOG.error("Mandator search for bo {} and report {} returned more than one result: {}",
				          boId, reportUID.getString(), resultFromUsage);
				break;
			}
		} 
		return result;
	}

}
