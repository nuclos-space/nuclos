//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.Mutable;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dbtransfer.TransferFacadeLocal;
import org.nuclos.server.dbtransfer.TransferUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class EntityFieldNucletContent extends DefaultNucletContent {

	private static final Logger LOG = LoggerFactory.getLogger(EntityFieldNucletContent.class);

	@Autowired
	private TransferFacadeLocal transferFacade;

	public EntityFieldNucletContent(List<INucletContent> contentTypes) {
		super(E.ENTITYFIELD.entity, contentTypes);
	}

	@Override
	public String getIdentifier(EntityObjectVO<UID> eo, NucletContentMap importContentMap) {
		return eo.getFieldValue(getIdentifierField(), String.class);
	}

	@Override
	public UID getIdentifierField() {
		return E.ENTITYFIELD.field.getUID();
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		storeLocaleResources(ncObject, E.ENTITYFIELD.localeresourcel, E.ENTITYFIELD.localeresourced);
		return ncObject;
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly, final Transfer transfer, final LogEntry log) {
		restoreLocaleResources(ncObject);

		final UID foreignentity = ncObject.getFieldUid(E.ENTITYFIELD.foreignentity);
		if (testOnly && foreignentity != null) {
			Mutable<Long> idDefault = new Mutable<>(ncObject.getFieldValue(E.ENTITYFIELD.foreigndefault));
			Mutable<UID> uidDefault = new Mutable<>(null);
			Mutable<String> valueDefault = new Mutable<>(ncObject.getFieldValue(E.ENTITYFIELD.valuedefault));
			transferFacade.validateEntityFieldDefaultValue(idDefault, uidDefault, valueDefault, toFieldMeta(ncObject));
			ncObject.setFieldValue(E.ENTITYFIELD.foreigndefault, idDefault.getValue());
			ncObject.setFieldValue(E.ENTITYFIELD.valuedefault, valueDefault.getValue());

			if (RigidUtils.equal(valueDefault.getValue(), ncObject.getFieldValue(E.ENTITYFIELD.defaultmandatory)) && idDefault.getValue() != null) {
				transfer.addDefaultMandatoryId(ncObject.getPrimaryKey(), idDefault.getValue());
			} else {
				if (Boolean.FALSE.equals(ncObject.getFieldValue(E.ENTITYFIELD.nullable))) {
					Mutable<String> defaultMandatoryId = new Mutable<>();
					Mutable<String> defaultMandatoryValue = new Mutable<>(ncObject.getFieldValue(E.ENTITYFIELD.defaultmandatory));
					transferFacade.validateEntityFieldDefaultMandatory(defaultMandatoryId, defaultMandatoryValue, toFieldMeta(ncObject), log.sbNonRepeatedValidationWarning);
					if (defaultMandatoryId.getValue() != null) {
						try {
							transfer.addDefaultMandatoryId(ncObject.getPrimaryKey(), Long.parseLong(defaultMandatoryId.getValue()));
						} catch (NumberFormatException ex) {
							transfer.addDefaultMandatoryId(ncObject.getPrimaryKey(), UID.parseUID(defaultMandatoryId.getValue()));
						}
					}
				}
			}
		}

		return super.insertOrUpdateNcObject(result, ncObject, testOnly, transfer, log);
	}
	
	
	@Override
	public boolean validate(TransferEO teo, ValidationType type, NucletContentMap importContentMap,
							Set<UID> existingNucletUIDs, LogEntry log, Map<TransferOption, Serializable> transferOptions, boolean hasDataLanguages) {
		boolean result =  super.validate(teo, type, importContentMap, existingNucletUIDs, log, transferOptions, hasDataLanguages);
		if (result == false) {
			return result;
		}
		final EntityObjectVO<UID> ncObject = teo.eo;
		final UID foreignentity = ncObject.getFieldUid(E.ENTITYFIELD.foreignentity);
		
		// validate simple refs
		switch (type) {
		case INSERT:
		case UPDATE:
			Boolean isLocalized = ncObject.getFieldValue(E.ENTITYFIELD.isLocalized) == null ? Boolean.FALSE: 
				Boolean.valueOf(ncObject.getFieldValue(E.ENTITYFIELD.isLocalized));
			
			if (isLocalized && !hasDataLanguages) {
				log.newCriticalLine("Keine Mehrsprachigkeit im Zielsystem vorhanden");
			}

			UID fieldGroupUID = ncObject.getFieldUid(E.ENTITYFIELD.entityfieldgroup);
			if (!validateForeignKeyInCompleteInstance(importContentMap, E.ENTITYFIELDGROUP, fieldGroupUID, E.ENTITYFIELDGROUP.nuclet)) {
				ncObject.removeFieldUid(E.ENTITYFIELD.entityfieldgroup.getUID());
			}

			UID autoNumberUID = ncObject.getFieldUid(E.ENTITYFIELD.autonumberentity);
			if (!validateForeignKeyInCompleteInstance(importContentMap, E.ENTITY, autoNumberUID, E.ENTITY.nuclet)) {
				ncObject.removeFieldUid(E.ENTITYFIELD.autonumberentity.getUID());
			}

			break;
		default: break;
		}
		
		// validate foreign entity
		if (foreignentity != null) {
			TransferEO importingForeignEntity = TransferUtils.getEntityObjectVO(importContentMap.getValues(E.ENTITY), foreignentity);
			switch (type) {
				case INSERT:
					// check if references are possible
					if (foreignentity != null && importingForeignEntity == null) {
						boolean dummy = true;
						if (E.isNuclosEntity(foreignentity)) {
							dummy = false;
						} else {
							EntityObjectVO<UID> foreinentityVO = TransferUtils.getEntityObject(E.ENTITY, foreignentity);
							if (foreinentityVO != null) {
								dummy = false;
							} else {
								// no entity with uid found. seach for entity with same name and use it
								for (EntityMeta<?> eMeta : MetaProvider.getInstance().getAllEntities()) {
									if (LangUtils.equal(eMeta.getEntityName(), ncObject.getFieldValue(E.ENTITYFIELD.foreignentity.getUID()))) {
										dummy = false;
									}
								}
							}
							//  TODO check field presentation
						}
						if (dummy) {
							EntityObjectVO<UID> entityVO = TransferUtils.getEntityObjectVO(importContentMap.getValues(E.ENTITY), ncObject.getFieldUid(E.ENTITYFIELD.entity)).eo;
							log.newWarningLine("Entity " + entityVO.getFieldValue(E.ENTITY.entity) + " references to unknown entity " + foreignentity + ". Redirect to dummy entity!");
							ncObject.setFieldUid(E.ENTITYFIELD.foreignentity, E.DUMMY.getUID());
							ncObject.setFieldValue(E.ENTITYFIELD.foreignentityfield, E.DUMMY.name.getUID().getStringifiedDefinition());
						}
					}
					break;

				case UPDATE:
					final UID importingForeignIntegrationPointUID = ncObject.getFieldUid(E.ENTITYFIELD.foreignIntegrationPoint);
					final EntityObjectVO<UID> existingField = getEOOriginal(E.ENTITYFIELD.getUID(), ncObject.getPrimaryKey());
					if (importingForeignIntegrationPointUID == null) {
						if (importingForeignEntity == null && !E.isNuclosEntity(foreignentity) && existingField != null) {
							// foreign entity is not in import and is not a system entity -> use old value
							UID existingForeignEntityUID = existingField.getFieldUid(E.ENTITYFIELD.foreignentity);
							ncObject.setFieldUid(E.ENTITYFIELD.foreignentity, existingForeignEntityUID);
							ncObject.setFieldValue(E.ENTITYFIELD.foreignentityfield, existingField.getFieldValue(E.ENTITYFIELD.foreignentityfield));
						}
					} else {
						final UID existingForeignIntegrationPointUID = existingField.getFieldUid(E.ENTITYFIELD.foreignIntegrationPoint);
						if (RigidUtils.equal(importingForeignIntegrationPointUID, existingForeignIntegrationPointUID) && existingField != null) {

							UID existingForeignEntityUID = existingField.getFieldUid(E.ENTITYFIELD.foreignentity);
							String existingForeignEntityField = existingField.getFieldValue(E.ENTITYFIELD.foreignentityfield);
							UID importingForeignEntityUID = ncObject.getFieldUid(E.ENTITYFIELD.foreignentity);
							String importingForeignEntityField = ncObject.getFieldValue(E.ENTITYFIELD.foreignentityfield);

							if (!RigidUtils.equal(existingForeignEntityUID, importingForeignEntityUID) ||
								!RigidUtils.equal(existingForeignEntityField, importingForeignEntityField)) {
								// NUCLOS-7644: if the integration points to an entity which is also part of the import,
								// then the complete integration should be imported.
								if (validateForeignKeyInImportOnly(importContentMap, E.ENTITY, foreignentity)) {
									// foreign entity is part of the import -> do nothing
									LOG.debug("Update complete field ({}) integration: [entity={}->{}; field={}->{}]",
											ncObject.getPrimaryKey(), existingForeignEntityUID, importingForeignEntityUID, existingForeignEntityField, importingForeignEntityField);
								} else {
									// protect integration
									ncObject.setFieldUid(E.ENTITYFIELD.foreignentity, existingForeignEntityUID);
									ncObject.setFieldValue(E.ENTITYFIELD.foreignentityfield, existingForeignEntityField);
								}
							}
						}
					}

					break;

				default:
					break;
			}
		}

		// apply data language flag 'isLocalized'
		if (type == ValidationType.UPDATE) {
			// FullImport do not revert a localization flag
			EntityObjectVO<UID> existing = getEOOriginal(E.ENTITYFIELD.getUID(), ncObject.getPrimaryKey());
			if (ncObject.getFieldValue(E.ENTITYFIELD.isLocalized) == null && existing.getFieldValue(E.ENTITYFIELD.isLocalized) != null) {
				ncObject.setFieldValue(E.ENTITYFIELD.isLocalized, existing.getFieldValue(E.ENTITYFIELD.isLocalized));
			}
		}

		// validate foreign defaults
		// only for "test" -> moved to insertOrUpdate

		return true;
	}

	private static FieldMeta<?> toFieldMeta(EntityObjectVO<UID> ncObject) {
		NucletFieldMeta<?> result = new NucletFieldMeta<>();
		// all relevant attributes for validation
		result.setUID(ncObject.getPrimaryKey());
		result.setFieldName(ncObject.getFieldValue(E.ENTITYFIELD.field));
		result.setDbColumn(ncObject.getFieldValue(E.ENTITYFIELD.dbfield));
		result.setDataType(ncObject.getFieldValue(E.ENTITYFIELD.datatype));
		result.setPrecision(ncObject.getFieldValue(E.ENTITYFIELD.dataprecision));
		result.setScale(ncObject.getFieldValue(E.ENTITYFIELD.datascale));
		result.setEntity(ncObject.getFieldUid(E.ENTITYFIELD.entity));
		result.setForeignEntity(ncObject.getFieldUid(E.ENTITYFIELD.foreignentity));
		result.setForeignEntityField(ncObject.getFieldValue(E.ENTITYFIELD.foreignentityfield));
		result.setOnDeleteCascade(ncObject.getFieldValue(E.ENTITYFIELD.ondeletecascade));
		result.setNullable(ncObject.getFieldValue(E.ENTITYFIELD.nullable));
		result.setDefaultMandatory(ncObject.getFieldValue(E.ENTITYFIELD.defaultmandatory));
		result.setDefaultValue(ncObject.getFieldValue(E.ENTITYFIELD.valuedefault));
		result.setDefaultForeignId(ncObject.getFieldValue(E.ENTITYFIELD.foreigndefault));
		result.setLocalized(ncObject.getFieldValue(E.ENTITYFIELD.isLocalized));
		result.setForeignIntegrationPoint(ncObject.getFieldUid(E.ENTITYFIELD.foreignIntegrationPoint));
		result.setIndexed(ncObject.getFieldValue(E.ENTITYFIELD.indexed));
		result.setReadonly(ncObject.getFieldValue(E.ENTITYFIELD.readonly));
		result.setModifiable(ncObject.getFieldValue(E.ENTITYFIELD.modifiable));
		return result;
	}
	
}
