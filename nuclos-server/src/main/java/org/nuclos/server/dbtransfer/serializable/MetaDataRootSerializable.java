//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.serializable;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.Version;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.dbtransfer.MetaDataRoot;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class MetaDataRootSerializable implements Converter {

	public static void register(XStream xstream) {
		xstream.alias("root", org.nuclos.server.dbtransfer.MetaDataRoot.class);
		xstream.registerConverter(new MetaDataRootSerializable());
	}
	
	@Override
	public boolean canConvert(Class type) {
		return MetaDataRoot.class.equals(type);
	}

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer,	MarshallingContext context) {
		MetaDataRoot root = (MetaDataRoot) source;
		final boolean isDirMode = root.exportOptions != null && root.exportOptions.containsKey(TransferOption.IS_DIRECTORY_MODE);
		
		writer.startNode("transferVersion");
		writer.setValue(root.transferVersion + "");
		writer.endNode();
		
		if (root.nucletUID != null) {
			writer.startNode("uid");
			writer.setValue(root.nucletUID + "");
			writer.endNode();
		}
		
		writer.startNode("nuclosVersion");
		Version v = root.version;
		if (isDirMode) {
			// remove version date from export
			v = new Version(root.version.getAppId(), root.version.getAppName(), root.version.getVersionNumber(), null);
		}
		context.convertAnother(v);
		writer.endNode();
		
		writer.startNode("database");
		writer.setValue(root.database + "");
		writer.endNode();
		
		if (!isDirMode) {
			writer.startNode("exportDate");
			context.convertAnother(root.exportDate);
			writer.endNode();
		}
		
		if (root.exportOptions != null && !root.exportOptions.isEmpty()) {
			writer.startNode("exportOptions");
			for (TransferOption to : CollectionUtils.sorted(root.exportOptions.keySet(), new Comparator<TransferOption>() {
				@Override
				public int compare(TransferOption o1, TransferOption o2) {
					return StringUtils.compareIgnoreCase(o1.name(), o2.name());
				}
			})) {
				writer.startNode(to.name());
				if (root.exportOptions.get(to) != null) {
					context.convertAnother(root.exportOptions.get(to));
				}
				writer.endNode();
			}
			writer.endNode();
		}
		
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Integer transferVersion = null;
		String nucletUID = null;
		Version version = null;
		String database = null;
		Date exportDate = null;
		final Map<TransferOption, Serializable> exportOptions = new HashMap<TransferOption, Serializable>();
		
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			String field = reader.getNodeName();
			if ("transferVersion".equals(field)) {
				transferVersion = Integer.parseInt(reader.getValue());
			} else if ("uid".equals(field)) {
				nucletUID = reader.getValue();
			} else if ("nuclosVersion".equals(field)) {
				version = (Version) context.convertAnother(field, Version.class);
			} else if ("database".equals(field)) {
				database = reader.getValue();
			} else if ("exportDate".equals(field)) {
				exportDate = (Date) context.convertAnother(field, Date.class);
			} else if ("exportOptions".equals(field)) {
				reader.getValue();
				while (reader.hasMoreChildren()) {
					reader.moveDown();
					String option = reader.getNodeName();
					Serializable obj = null;
					if (!StringUtils.looksEmpty(reader.getValue())) {
						obj = (Serializable) context.convertAnother(option, Serializable.class);
					}
					TransferOption to = TransferOption.valueOf(option);
					exportOptions.put(to, obj);
					reader.moveUp();
				}
			}
			reader.moveUp();
		}
		
		return new MetaDataRoot(transferVersion, nucletUID, version, database, exportDate, exportOptions);
	}
	
}
