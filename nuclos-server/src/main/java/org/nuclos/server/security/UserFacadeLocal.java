//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.Map;

import org.nuclos.common.MandatorVO;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.security.UserVO;
import org.nuclos.common.valueobject.MandatorUserVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public interface UserFacadeLocal {

	void setPassword(String username, String password, String customUsage) throws CommonBusinessException;
	
	UserVO getByUserName(String username) throws CommonBusinessException;
	
	UserVO getByUserEmail(String email) throws CommonBusinessException;
	
	UserVO getByActivationCode(String code) throws CommonBusinessException;
	
	MasterDataVO<UID> getRoleByName(String rolename) throws CommonBusinessException;
	
	UserVO create(UserVO vo, IDependentDataMap mpDependants) throws CommonBusinessException;

	UserVO modify(UserVO vo, IDependentDataMap mpDependants, String customUsage) throws CommonBusinessException;

	void remove(UserVO vo) throws CommonBusinessException;

	UserVO getByUID(UID id) throws CommonBusinessException;

	Map<UID, String> getAllRolesForUser(String username);

	org.nuclos.api.UID grantMandator(MandatorUserVO mandatorUserVO) throws CommonCreateException, NuclosBusinessRuleException, CommonPermissionException;

	void revokeMandator(UserVO user, MandatorVO mandator) throws NuclosBusinessRuleException;

	boolean isMandatorGranted(UserVO user, MandatorVO mandator) throws CommonBusinessException;
}
