package org.nuclos.server.web;

import static org.nuclos.server.common.NuclosSystemParameters.getNuclosHome;
import static org.nuclos.server.common.NuclosSystemParameters.getServerExtensionPaths;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;

public class NucletExtensionLoader {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NucletExtensionLoader.class);

	public static final String EXTENSION_FILE_IDENTIFIER = "nuclet-extension";

	public static Path getNucletExtensionsHomePath() {
		final Path nuclosHome = getNuclosHome();
		if (nuclosHome == null) {
			return null;
		}
		return nuclosHome.resolve("nuclet-extensions");
	}

	public static Path getNucletExtensionsServerHomePath() {
		final Path nucletExtensionsHomePath = getNucletExtensionsHomePath();
		if (nucletExtensionsHomePath == null) {
			return null;
		}
		return nucletExtensionsHomePath.resolve("server");
	}

	static void updateAndLoadServerExtensions() {
		final Path homeExtensionsServer = getNucletExtensionsServerHomePath();
		if (homeExtensionsServer != null && homeExtensionsServer.toFile().exists() && homeExtensionsServer.toFile().isDirectory()) {
			for (Path serverExtensionPath : getServerExtensionPaths()) {
				try {
					// 1. delete all nuclet extensions from dir
					getNucletExtensionPathEntries(serverExtensionPath).stream().forEach(n -> uninstallExtension(n));

					// 2. copy all nuclet extensions to dir
					getNucletExtensionPathEntries(homeExtensionsServer).stream().forEach(n -> installAndLoadExtension(n, serverExtensionPath));

				} catch (IOException ex) {
					LOG.error("Unexpected IO ERROR: {} on {}", ex, serverExtensionPath, ex);
				}
			}
		}
	}

	private static void installAndLoadExtension(Path file, Path toDir) {
		try {
			final Method addURLMethod = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
			final URLClassLoader cl = (URLClassLoader) NucletExtensionLoader.class.getClassLoader();
			final Path newFile = toDir.resolve(file.getFileName());
			Files.copy(file, newFile);
			addURLMethod.setAccessible(true);
			addURLMethod.invoke(cl, newFile.toUri().toURL());
		} catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
			LOG.error("Extension could not be installed: {} on {} -> {}", ex, file, toDir, ex);
		}
	}

	private static void uninstallExtension(Path file) {
		try {
			Files.delete(file);
		} catch (IOException ex) {
			LOG.error("Extension could not be uninstalled: {} on {}", ex, file, ex);
		}
	}

	public static List<Path> getNucletExtensionPathEntries(final Path dir) throws IOException {
		final List<Path> entries = new ArrayList<>();
		Files.newDirectoryStream(dir, path -> path.getFileName().toString().contains(EXTENSION_FILE_IDENTIFIER)).forEach(entries::add);
		return entries;
	}

	public static void addServerExtensions(List<File> toFileList) {
		for (Path serverExtensionPath : getServerExtensionPaths()) {
			try {
				getNucletExtensionPathEntries(serverExtensionPath).stream().forEach(n -> toFileList.add(n.toFile()));
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, serverExtensionPath, ex);
			}
		}
	}

}
