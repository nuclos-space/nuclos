//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.web;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.nuclos.server.web.activemq.NuclosJMSBrokerTunnelServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * A (servlet) session listener used to track client http session lifecycle.
 * <p>
 * As there is no spring support for session listeners, it runs outside of spring.
 *
 * @author Thomas Pasch
 * @since Nuclos 4.0.10
 */
@WebListener
public class ClientLifecycleSessionListener implements HttpSessionListener {

	private static final Logger LOG = LoggerFactory.getLogger(ClientLifecycleSessionListener.class);

	@Autowired
	private Session session;

	public ClientLifecycleSessionListener() {
	}

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		final ClientLifecycle clientLifecycle = ClientLifecycle.getInstance();
		clientLifecycle.newSession(se.getSession());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		SecurityContext context = SecurityContextHolder.getContext();

		if (context != null) {
			Authentication authentication = context.getAuthentication();
			if (authentication != null) {
				Object principal = authentication.getPrincipal();
				if (principal != null) {
					String username = principal.toString();
					NuclosJMSBrokerTunnelServlet.invalidateRecentUsersInfo(username);
				}
			}
		}

		if (session != null) {
			session.invalidate();
		} else {
			LOG.warn("session is null - could not invalidate");
		}
	}

}
