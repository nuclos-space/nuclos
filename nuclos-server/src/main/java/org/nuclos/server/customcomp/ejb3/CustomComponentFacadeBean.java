//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.customcomp.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.JMSMessage;
import org.nuclos.common.SF;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional(noRollbackFor= {Exception.class})
public class CustomComponentFacadeBean extends NuclosFacadeBean implements CustomComponentFacadeLocal, CustomComponentFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(CustomComponentFacadeBean.class);

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;	
	
	@Autowired
	private LocaleFacadeLocal localeFacade;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	public CustomComponentFacadeBean() {
	}

	private void notifyClients() {
		NuclosJMSUtils.sendOnceAfterCommitDelayed(JMSMessage.REFRESH_MENUS, JMSConstants.TOPICNAME_CUSTOMCOMPONENTCACHE);
	}

	public List<CustomComponentVO> getAll() {
		Collection<MasterDataVO<UID>> mdvos = 
				masterDataFacade.getMasterData(E.CUSTOMCOMPONENT, null);
		List<CustomComponentVO> vos = new ArrayList<CustomComponentVO>(mdvos.size());
		for (MasterDataVO mdvo : mdvos) {
			vos.add(getCustomComponentVO(mdvo));
		}
		return vos;
	}

	public void create(CustomComponentVO vo, List<TranslationVO> translations) throws CommonBusinessException {
		checkWriteAllowed(E.CUSTOMCOMPONENT);
		MasterDataVO result = masterDataFacade.create(wrapVO(vo), null);

		setResources(getCustomComponentVO(result), translations);
		notifyClients();
	}

	public void modify(CustomComponentVO vo, List<TranslationVO> translations) throws CommonBusinessException {
		checkWriteAllowed(E.CUSTOMCOMPONENT);
		masterDataFacade.modify(wrapVO(vo), null);

		setResources(vo, translations);
		notifyClients();
	}

	public void remove(CustomComponentVO vo) throws CommonBusinessException {
		masterDataFacade.remove(E.CUSTOMCOMPONENT.getUID(), vo.getId(), true, null);

		notifyClients();
	}

	private static MasterDataVO<UID> wrapVO(CustomComponentVO vo) {
		
		Map<UID, Object> mpFields = new HashMap<UID,Object>();
		mpFields.put(E.CUSTOMCOMPONENT.name.getUID(), vo.getInternalName());
		mpFields.put(E.CUSTOMCOMPONENT.label.getUID(), vo.getLabelResourceId());
		mpFields.put(E.CUSTOMCOMPONENT.menupath.getUID(), vo.getMenupathResourceId());
		mpFields.put(E.CUSTOMCOMPONENT.componenttype.getUID(), vo.getComponentType());
		mpFields.put(E.CUSTOMCOMPONENT.componentversion.getUID(), vo.getComponentVersion());
		mpFields.put(E.CUSTOMCOMPONENT.data.getUID(), vo.getData());
		
		Map<UID, UID> mpUidFields = new HashMap<UID, UID>();		
		mpUidFields.put(E.CUSTOMCOMPONENT.nuclet.getUID(), vo.getNucletUID() == null ? null : vo.getNucletUID());

		return new MasterDataVO<UID>(E.CUSTOMCOMPONENT.getUID(), vo.getId(), 
				vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion(), mpFields, null, mpUidFields, false);
	}

	private static CustomComponentVO getCustomComponentVO(MasterDataVO<UID> mdVO) {
		NuclosValueObject<UID> nvo = new NuclosValueObject<UID>(
			mdVO.getId(),
			mdVO.getCreatedAt(),
			mdVO.getChangedBy(),
			mdVO.getChangedAt(),
			mdVO.getChangedBy(),
			mdVO.getVersion());
		
		CustomComponentVO vo = new CustomComponentVO(nvo);
		
		vo.setInternalName(mdVO.getFieldValue(E.CUSTOMCOMPONENT.name));
		vo.setLabelResourceId(mdVO.getFieldValue(E.CUSTOMCOMPONENT.label));
		vo.setMenupathResourceId(mdVO.getFieldValue(E.CUSTOMCOMPONENT.menupath));
		vo.setComponentType(mdVO.getFieldValue(E.CUSTOMCOMPONENT.componenttype));
		vo.setComponentVersion(mdVO.getFieldValue(E.CUSTOMCOMPONENT.componentversion));
		vo.setData(mdVO.getFieldValue(E.CUSTOMCOMPONENT.data));
		vo.setNuclet(mdVO.getFieldUid(E.CUSTOMCOMPONENT.nuclet)==null?null:mdVO.getFieldUid(E.CUSTOMCOMPONENT.nuclet));
		
		return vo;
	}

	private void setResources(CustomComponentVO cc, List<TranslationVO>  translations) {
		String labelResourceId = cc.getLabelResourceId();
		String menupathResourceId = cc.getMenupathResourceId();

		if (!isResourceId(labelResourceId)) {
			labelResourceId = null;
		}

		if (!isResourceId(menupathResourceId)) {
			menupathResourceId = null;
		}

		Map<UID, LocaleInfo> lis = 
				CollectionUtils.transformIntoMap(localeFacade.getAllLocales(false), new Transformer<LocaleInfo, UID>() {
					
				@Override
				public UID transform(LocaleInfo i) {
					return i.getLocale();
				}
			}, new Transformer<LocaleInfo, LocaleInfo>() {
				@Override
				public LocaleInfo transform(LocaleInfo i) {
					return i;
				}
			});

		for(TranslationVO vo : translations) {
			LocaleInfo li = lis.get(vo.getLocale());

			labelResourceId = localeFacade.setResourceForLocale(labelResourceId, li, vo.getLabels().get(TranslationVO.LABELS_CUSTOM_COMPONENT[0]));
			menupathResourceId = localeFacade.setResourceForLocale(menupathResourceId, li, vo.getLabels().get(TranslationVO.LABELS_CUSTOM_COMPONENT[1]));

		}
		setResourceIdForField(cc.getId(), E.CUSTOMCOMPONENT.label, labelResourceId);
		setResourceIdForField(cc.getId(), E.CUSTOMCOMPONENT.menupath, menupathResourceId);
		
		localeFacade.flushInternalCaches(true);
	}

	private static boolean isResourceId(String s) {
		return ServerServiceLocator.getInstance().getFacade(LocaleFacadeLocal.class).isResourceId(s);
	}

	private void setResourceIdForField(UID uid, FieldMeta<?> field, String sResourceId) {
		dataBaseHelper.execute(
				DbStatementUtils.updateValuesUnsafe(E.CUSTOMCOMPONENT, field, DbNull.escapeNull(sResourceId, String.class)).where(SF.PK_UID, uid));
	}

	public List<TranslationVO> getTranslations(UID uid) throws CommonBusinessException {
		ArrayList<TranslationVO> result = new ArrayList<TranslationVO>();

		CustomComponentVO cc = getCustomComponentVO(masterDataFacade.get(E.CUSTOMCOMPONENT, uid));

		LocaleFacadeLocal service = ServerServiceLocator.getInstance().getFacade(LocaleFacadeLocal.class);

		// If the resource-ids do not exist, setup the translation list with these values (backwards compatibility)
		boolean isResourceIdLabel = service.isResourceId(cc.getLabelResourceId());
		boolean isResourceIdMenupath = service.isResourceId(cc.getMenupathResourceId());

		for (LocaleInfo li : service.getAllLocales(false)) {
			Map<String, String> labels = new HashMap<String, String>();
			labels.put(TranslationVO.LABELS_CUSTOM_COMPONENT[0], isResourceIdLabel ? service.getResourceById(li, cc.getLabelResourceId()) : cc.getLabelResourceId());
			labels.put(TranslationVO.LABELS_CUSTOM_COMPONENT[1], isResourceIdMenupath ? service.getResourceById(li, cc.getMenupathResourceId()) : cc.getMenupathResourceId());

			TranslationVO vo = new TranslationVO(li, labels);
			result.add(vo);
		}
		return result;
	}
}
