package org.nuclos.server.i18n.language.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

@Component
public class DataLanguageCache {

	private static final Logger LOG = LoggerFactory.getLogger(DataLanguageCache.class);
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	private List<DataLanguageVO> allDataLanguages;
	private UID primaryLanguage;
	
	public Collection<DataLanguageVO> getDataLanguages() {
		
		if (allDataLanguages == null) {
			allDataLanguages = new ArrayList<DataLanguageVO>();
			
			NucletDalProvider nucletDalProvider = NucletDalProvider.getInstance();
			for (EntityObjectVO<UID> eovo : nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).getAll()) {
				allDataLanguages.add(new DataLanguageVO(eovo));
			}
			
			Collections.sort(allDataLanguages, new Comparator<DataLanguageVO>() {
				@Override
				public int compare(DataLanguageVO o1, DataLanguageVO o2) {
					return o1.isPrimary().compareTo(o2.isPrimary());
				}
			});			
		}
		
		return allDataLanguages;
	}
	
	public List<org.nuclos.api.UID> getDataLanguageUIDs() {		
		return CollectionUtils.transform(getDataLanguages(), new Transformer<DataLanguageVO, org.nuclos.api.UID>() {
			@Override
			public org.nuclos.api.UID transform(DataLanguageVO i) {
				return i.getPrimaryKey();
			}
		});
	}
	
	public org.nuclos.api.UID getNuclosPrimaryDataLanguage() {
		if (primaryLanguage == null) {
			for (DataLanguageVO dlvo : getDataLanguages()) {
				if (dlvo.isPrimary()) {
					primaryLanguage = dlvo.getPrimaryKey();
					break;
				}
			}
		}
		
		return primaryLanguage;
	}
	
	public DataLanguageVO getNuclosPrimaryDataLanguageObject() {
		DataLanguageVO retVal = null;
		for (DataLanguageVO dlvo : getDataLanguages()) {
			if (dlvo.isPrimary()) {
				retVal = dlvo;
				break;
			}
		}
		
		return retVal;
	}
	
	public UID getLanguageToUse() {
		return getCurrentUserDataLanguage() != null ? 
				(UID)getCurrentUserDataLanguage() : (UID)getNuclosPrimaryDataLanguage();
	}
	
	public void invalidate() {
		this.primaryLanguage = null;
		this.allDataLanguages = null;
	}

	public org.nuclos.api.UID getCurrentUserDataLanguage() {
		return this.userCtx.getDataLocal();
	}

	public DataLanguageVO getCurrentUserDataLanguageObject() {
		DataLanguageVO retVal = null;
		
		org.nuclos.api.UID currentUserDataLanguage = getLanguageToUse();
		
		for (DataLanguageVO dlvo : getDataLanguages()) {
			if (currentUserDataLanguage.equals(dlvo.getPrimaryKey())) {
				retVal = dlvo;
				break;
			}
		}
		
		return retVal;
	}
}
