//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.validation;

import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.server.autosync.AutoDbSetup;
import org.nuclos.server.autosync.SchemaHelper;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.PersistentDbAccess;

public class DefaultSchemaValidation {

	/**
	 * Überprüft und korrigiert das aktuelle Schema.
	 * 
	 * Nicht mehr gültige Referenzen werden abgeräumt oder geleert, je nach Möglichkeit (NOT NULL).
	 * Die Prüfung wird mehrfach durchlaufen, falls etwas gefunden und korrigiert wurde.
	 * 
	 * @param grantRemoveEntityFields 
	 * @see SchemaHelper#grantRemoveEntityFields()
	 */
	public static void validate(boolean grantRemoveEntityFields, AnnotationJaxb2Marshaller jaxb2Marshaller, boolean bUsePersistantDbAccess) {
		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		AutoDbSetup autoDbSetup = new AutoDbSetup(dbAccess, E.getThis(), ApplicationProperties.getInstance().getNuclosVersion());
		AutoDbSetup.Schema validationSchema = autoDbSetup.getSchema(E.getThis(), autoDbSetup.nuclosStaticsVersion, null);
		SchemaHelper schemaHelper = bUsePersistantDbAccess ?
				new SchemaHelper(validationSchema, E.getSchemaVersion(), E.getThis(), new PersistentDbAccess(dbAccess)) :
				new SchemaHelper(validationSchema, E.getSchemaVersion(), E.getThis(), dbAccess);
		schemaHelper.setJaxb2Marshaller(jaxb2Marshaller);
		if (grantRemoveEntityFields) {
			schemaHelper.grantRemoveEntityFields();
		}
		schemaHelper.validateNoLogFile();
	}
	
	/**
	 * Löscht alle System Constraints (Unique &amp; Foreign)
	 * 
	 * @param reason (Grund für das Löschen)
	 * @throws Exception
	 */
	public static void removeSysContraints(String reason) throws Exception {
		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		AutoDbSetup autoDbSetup = new AutoDbSetup(dbAccess, E.getThis(), ApplicationProperties.getInstance().getNuclosVersion());
		autoDbSetup.removeSysConstraints(true, true, reason);
	}
	
	/**
	 * Legt alle System Constraints (Unique &amp; Foreign) wieder an
	 * 
	 * @param reason (Grund für das Anlagen)
	 * @throws Exception
	 */
	public static void createSysConstraints(String reason) throws Exception {
		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		AutoDbSetup autoDbSetup = new AutoDbSetup(dbAccess, E.getThis(), ApplicationProperties.getInstance().getNuclosVersion());
		autoDbSetup.createSysConstraints(true, true, reason);
	}
	
}
