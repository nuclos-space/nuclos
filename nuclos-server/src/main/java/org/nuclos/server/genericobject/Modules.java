//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.ModuleProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.EntityObjectToEntityTreeViewVO;
import org.nuclos.common.collection.EntityObjectToMasterDataTransformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Provides information about the modules contained in a Nucleus application.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class Modules extends ModuleProvider implements ClusterCache {

	
	private static Modules INSTANCE;
	
	//

	/**
	 * The specfied subnodes of the module used in the treenode 
	 */
	private Map<UID, List<MasterDataVO<?>>> mpModuleSubnodes;
	
	private Map<UID, List<EntityTreeViewVO>> subNodes;
	
	private MasterDataFacadeHelper masterDataFacadeHelper;
	
	private NucletDalProvider nucletDalProvider;
	
	private NuclosLocalServerSession nuclosLocalServerSession;
	
	@Autowired
	private MetaProvider metaProv;
	
	//

	Modules() {
		INSTANCE = this;
	}
	
	@PostConstruct
	void init() {
		setModules(getModules());
	}
	
	@Autowired
	void setMasterDataFacadeHelper(MasterDataFacadeHelper masterDataFacadeHelper) {
		this.masterDataFacadeHelper = masterDataFacadeHelper;
	}
	
	@Autowired
	void setNucletDalProvider(NucletDalProvider nucletDalProvider) {
		this.nucletDalProvider = nucletDalProvider;
	}
	
	@Autowired
	void setNuclosLocalServerSession(NuclosLocalServerSession nuclosLocalServerSession) {
		this.nuclosLocalServerSession = nuclosLocalServerSession;
	}

	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static Modules getInstance() {
		return INSTANCE;
	}
	
	@Override
	public Collection<EntityMeta<?>> getModules() {
		try {
			List<EntityMeta<?>> colModules = new ArrayList<EntityMeta<?>>();
			for (EntityMeta<?> eMeta : metaProv.getAllEntities()) {
				if (eMeta.isStateModel()) {
					colModules.add(eMeta);
				}
			}

			mpModuleSubnodes = new HashMap<UID, List<MasterDataVO<?>>>();
			subNodes = new HashMap<UID, List<EntityTreeViewVO>>();
			List<EntityObjectVO<UID>> allSubnodes = nucletDalProvider.getEntityObjectProcessor(E.ENTITYSUBNODES).getAll();
			for (EntityMeta<?> eMeta : colModules) {
				Collection<EntityObjectVO<UID>> colVO = new ArrayList<EntityObjectVO<UID>>();
				for (EntityObjectVO<UID> subnode : allSubnodes) {
					if (RigidUtils.equal(subnode.getFieldUid(E.ENTITYSUBNODES.originentityid), eMeta.getUID())) {
						colVO.add(subnode);
					}
				}
				mpModuleSubnodes.put(eMeta.getUID(), CollectionUtils.transform(colVO,
					new EntityObjectToMasterDataTransformer()));
				subNodes.put(eMeta.getUID(), CollectionUtils.transform(colVO,
					new EntityObjectToEntityTreeViewVO()));
			}

			return colModules;
		}
		catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/**
	 * @deprecated Todo: Get rid of it! (Thomas Pasch)
	 */
	public List<MasterDataVO<?>> getSubnodesMD(final UID entity) {
		return mpModuleSubnodes.get(entity);
	}
	
	public List<EntityTreeViewVO> getSubnodesETV(final UID entity) {
		return subNodes.get(entity);
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.Type.MODULES_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);				
	}

	@Override
	public void registerCache() {		
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {		
		return null;
	}

	@Override
	public void invalidateCache(boolean notifyClients, boolean notifyClusterCloud) {
		invalidate();
		if(notifyClients) {
			notifyClusterCloud();
		}
		
	}

}	// class Modules

