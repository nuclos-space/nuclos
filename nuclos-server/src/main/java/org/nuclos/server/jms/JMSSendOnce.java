package org.nuclos.server.jms;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.nuclos.common.JMSOnce;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JMSSendOnce extends JMSOnce {
	
	private static final Logger LOG = LoggerFactory.getLogger(JMSSendOnce.class);
	
	//
	
	public JMSSendOnce() {	
	}
	
	public void once() {
		LOG.info("Now sending all JMS messages: textSize={} objectSize={}",
		         topic2TextMessage.size(), topic2ObjectMessage.size());
		for (String topic: new HashSet<>(topic2TextMessage.keySet())) {
			final Set<String> set = topic2TextMessage.get(topic);
			for (String text: set) {
				NuclosJMSUtils.sendMessage(text, topic);
			}
		}
		for (String topic: new HashSet<>(topic2ObjectMessage.keySet())) {
			final Set<Serializable> set = topic2ObjectMessage.get(topic);
			for (Serializable object: set) {
				NuclosJMSUtils.sendObjectMessage(object, topic, null);
			}
		}
		clear();
	}

}
