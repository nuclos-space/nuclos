package org.nuclos.server.rest.services.helper;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.json.JsonObjectBuilder;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

import org.nuclos.api.Message;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.E;
import org.nuclos.common.JsonUtils;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.http.RequestConfigHolder;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.CollectiveProcessingObjectInfo;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ejb3.ApiMessageContextService;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.rest.ejb3.BoGenerationResult;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@Component
public class CollectiveProcessingExecutionService {

	private static final Logger LOG = LoggerFactory.getLogger(CollectiveProcessingExecutionService.class);

	@Lazy
	@Autowired
	protected JsonFactory jsonFactory;

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private MetaProvider metaProv;

	@Autowired
	private SpringLocaleDelegate localeDelegate;

	@Autowired
	private NucletDalProvider dalProvider;

	@Autowired
	private NuclosUserDetailsContextHolder userDetails;

	@PostConstruct
	final void init() {
		Timer timer = new Timer(true);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				evictForgottenCacheEntries();
			}
		}, 1000 * 60 * 60 * 24);
	}

	//private final ThreadPoolExecutor executor = Executors.newCachedThreadPool();
	private final ThreadPoolExecutor executor = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
			60L, TimeUnit.SECONDS,
			new SynchronousQueue<Runnable>()) {
		@Override
		protected void beforeExecute(final Thread t, final Runnable r) {
			super.beforeExecute(t, ((ThreadLocalePropagationRunnable)r).initThreadLocales());
		}
	};

	@Transactional(propagation= Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void executeStateChangeTransactional(UID entityUID, Object id, UID transitionUID) throws CommonBusinessException {
		UID stateUID = Rest.facade().getTargetStateId(transitionUID);
		Rest.facade().changeState(entityUID, (Long) id, stateUID);
	}

	@Transactional(propagation= Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void executeDeleteTransactional(UID entityUID, Object id) throws CommonBusinessException {
		Rest.facade().delete(entityUID, id, true);
	}

	@Transactional(propagation= Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void executeCustomRuleTransactional(UID entityUID, Object id, String rule) throws CommonBusinessException {
		EntityObjectVO<Object> eo = Rest.facade().get(entityUID, id, true);
		Rest.facade().fireCustomEventSupport(eo, rule);
	}

	public void executeCustomRule(
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID,
			final String rule) {
		executeOn(cacheEntry, clctexpr, entityUID, (id, oInfo) -> executeCustomRuleTransactional(entityUID, id, rule));
	}

	public void executeDelete(
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID) {
		executeOn(cacheEntry, clctexpr, entityUID, (id, oInfo) -> executeDeleteTransactional(entityUID, id));
	}

	public void executeStateChanges(
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID,
			final UID transitionUID) {
		executeOn(cacheEntry, clctexpr, entityUID, (id, oInfo) -> executeStateChangeTransactional(entityUID, id, transitionUID));
	}

	public void executeGeneration(
			final WebContext context,
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID,
			final UID generationUID) {
		executor.execute(new ThreadLocalePropagationRunnable() {
			@Override
			public void run() {
				try {
					GeneratorActionVO generation = Rest.facade().getGeneration(generationUID);
					if (generation.isGroupAttributes()) {
						final Mutable<Long> infoRowNumber = new Mutable<>(0L);
						final Collection<Object> ids = Rest.facade().getEntityObjectIds(entityUID, clctexpr);
						cacheEntry.setTotalCount(ids.size());
						final Map<String, Collection<EntityObjectVO<Object>>> groupedObjects = Rest.facade().groupObjectsForGeneration(ids, generation);
						groupedObjects.values().stream().forEach(group -> {
							final Collection<Object> groupIds = group.stream().map(EntityObjectVO::getPrimaryKey).collect(Collectors.toList());
							final Map<Long, Integer> eoIdsAndVersions = new HashMap<>();
							group.stream().forEach(eo -> eoIdsAndVersions.put((Long)eo.getPrimaryKey(), eo.getVersion()));
							BoGenerationResult genResult = null;
							String jsonGenResult = null;
							CommonBusinessException generationException = null;
							try {
								if (!cacheEntry.isAborted()) {
									genResult = Rest.facade().generateBo(entityUID, eoIdsAndVersions, generationUID);
								}
							} catch (CommonBusinessException e) {
								generationException = e;
							}
							if (genResult != null) {
								jsonGenResult = JsonUtils.objectToString(jsonFactory.buildJsonObject(genResult, context).build());
							}
							final String finalJsonGenResult = jsonGenResult;
							final CommonBusinessException finalGenerationException  = generationException;
							final BoGenerationResult finalGenResult = genResult;
							_run(cacheEntry, groupIds, infoRowNumber, entityUID, (id, oInfo) -> {
								oInfo.setBoGenerationResult(finalJsonGenResult);
								if (finalGenResult != null && finalGenResult.getBusinessError() != null) {
									throw new BusinessException(finalGenResult.getBusinessError());
								}
								if (finalGenerationException != null) {
									throw finalGenerationException;
								}
							});
						});
					} else {
						_run(cacheEntry, clctexpr, entityUID, (id, oInfo) -> {
							final Map<Long, Integer> eoIdsAndVersions = new HashMap<>();
							eoIdsAndVersions.put((Long)id, Rest.facade().get(entityUID, id, false).getVersion());
							final BoGenerationResult genResult = Rest.facade().generateBo(entityUID, eoIdsAndVersions, generationUID);
							final String jsonGenResult = JsonUtils.objectToString(jsonFactory.buildJsonObject(genResult, context).build());
							oInfo.setBoGenerationResult(jsonGenResult);
							if (genResult.getBusinessError() != null) {
								throw new BusinessException(genResult.getBusinessError());
							}
						});
					}
				} catch (Exception e) {
					throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		});
	}

	private void executeOn(final CollectiveProcessingExecutionEntry cacheEntry, final CollectableSearchExpression clctexpr, final UID entityUID, CollectiveProcessingExecutionConsumer<Object> consumer) {
		executor.execute(new ThreadLocalePropagationRunnable() {
			@Override
			public void run() {
				_run(cacheEntry, clctexpr, entityUID, consumer);
			}
		});
	}

	private void _run(final CollectiveProcessingExecutionEntry cacheEntry, final CollectableSearchExpression clctexpr, final UID entityUID, CollectiveProcessingExecutionConsumer<Object> consumer) {
		try {
			final Collection<Object> ids = Rest.facade().getEntityObjectIds(entityUID, clctexpr);
			cacheEntry.setTotalCount(ids.size());
			final Mutable<Long> infoRowNumber = new Mutable<>(0L);
			_run(cacheEntry, ids, infoRowNumber, entityUID, consumer);
		} catch (Exception e) {
			throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	private void _run(final CollectiveProcessingExecutionEntry cacheEntry, final Collection<Object> ids, final Mutable<Long> infoRowNumber, final UID entityUID, CollectiveProcessingExecutionConsumer<Object> consumer) {
		for (Object id : ids) {
			final String eoTitle = getEoTitle(entityUID, id);
			final String entityFQN = Rest.translateUid(E.ENTITY, entityUID);
			final Long rowNumber = infoRowNumber.setValue(infoRowNumber.getValue() + 1l);
			final CollectiveProcessingObjectInfo oInfo = CollectiveProcessingExecutionEntry.newObjectInfo(entityFQN, id, eoTitle, rowNumber);
			oInfo.setInProgress(true);
			cacheEntry.storeObjectInfo(oInfo);

			if (cacheEntry.isAborted()) {
				oInfo.setSuccess(false);
				oInfo.setNote("Aborted by User."); // internationalize ?
				oInfo.setInProgress(false);
				cacheEntry.storeObjectInfo(oInfo);
				cacheEntry.setTotalCount(1);

				break;
			}

			final List<Message> messageStore = new ArrayList<>();
			try {
				ApiMessageContextService.setMessageStore(messageStore);
				consumer.apply(id, oInfo);
				oInfo.setSuccess(true);
				String resultMessage = messageStore.stream()
						.map(m -> Stream.of(m.getTitle(), m.getMessage())
								.filter(s -> !StringUtils.looksEmpty(s))
								.collect(joining(": ")))
						.collect(joining(" | "));
				if (resultMessage.length() > 0) {
					oInfo.setNote(resultMessage);
				}
			} catch (Throwable e) {
				LOG.error("CollectiveProcessingExecution exception: executionId=" + cacheEntry.getExecutionId() +
								", row=" + rowNumber +
								", exception=" + e.getMessage(),
						e);
				oInfo.setSuccess(false);
				oInfo.setNote(e.getMessage());
			} finally {
				ApiMessageContextService.clearMessageStore();
				oInfo.setInProgress(false);
				cacheEntry.storeObjectInfo(oInfo);
			}
		}
	}

	private String getEoTitle(UID entityUID, Object oId) {
		final EntityObjectVO<?> eo = dalProvider.getEntityObjectProcessor(metaProv.getEntity(entityUID)).getByPrimaryKey(oId);
		return localeDelegate.getTreeViewLabel(eo, metaProv, userDetails.getDataLocal());
	}

	public String newExecutionId() {
		return new UID().getString();
	}

	public CollectiveProcessingExecutionEntry createCacheEntry(@NotNull String executionId, @NotNull String user) {
		checkExecutionParameter(executionId, user);
		final CollectiveProcessingExecutionEntry result = getCacheEntry(executionId);
		if (result.getUser() != null) {
			throw new NuclosFatalException("Cache entry exist already: executionId=" + executionId + ", userInCache=" + result.getUser() + ", userCreate=" + user);
		}
		result.setUser(user);
		return result;
	}

	public CollectiveProcessingExecutionEntry getCacheEntry(@NotNull String executionId, @NotNull String user) throws CommonPermissionException, CommonFinderException {
		checkExecutionParameter(executionId, user);
		final CollectiveProcessingExecutionEntry result = getCacheEntry(executionId);
		if (result.getUser() == null) {
			evictCache(executionId);
			throw new CommonFinderException("Execution with id " + executionId + " does not exist!");
		}
		if (!RigidUtils.equal(result.getUser(), user)) {
			throw new CommonPermissionException("User " + user + " is not authorized to access this information!");
		}
		return result;
	}

	private void checkExecutionParameter(@NotNull final String executionId, @NotNull final String user) {
		if (executionId == null) {
			throw new IllegalArgumentException("executionId must not be null");
		}
		if (user == null) {
			throw new IllegalArgumentException("user must not be null");
		}
	}

	private void evictForgottenCacheEntries() {
		Collection<Object> collectiveProcessingExecutionEntries = new ArrayList(
				((ConcurrentMapCache) cacheManager.getCache("collectiveProcessingExecutionCache")).getNativeCache().values()
		);
		collectiveProcessingExecutionEntries.stream().forEach(object -> {
			CollectiveProcessingExecutionEntry cacheEntry = (CollectiveProcessingExecutionEntry) object;
			if (cacheEntry.getLastUpdate() + (1000 * 60 * 60 * 12) < System.currentTimeMillis()) {
				// 12h no update? Dead client or client is not watching the progress
				LOG.warn("Removing progress cache entry for execution " + cacheEntry.getExecutionId() + ". " +
						"It's been more than 12 hours since the last update, and the information was not requested by any client");
				this.evictCache(cacheEntry.getExecutionId());
			}
		});
	}

	@Cacheable(value="collectiveProcessingExecutionCache", key="#p0")
	public CollectiveProcessingExecutionEntry getCacheEntry(@NotNull String executionId) {
		return new CollectiveProcessingExecutionEntry(executionId);
	}

	@Caching(evict = {
			@CacheEvict(value = "collectiveProcessingExecutionCache", key = "#p0")
	})
	public void evictCache(@NotNull String executionId) {
	}

	private abstract class ThreadLocalePropagationRunnable implements Runnable {
		private final SecurityContext securityContext;
		private final NuclosUserDetailsContextHolder.AllValues userDetailsValues;
		private final RequestAttributes requestAttributes;
		public ThreadLocalePropagationRunnable() {
			this.securityContext = SecurityContextHolder.getContext();
			this.userDetailsValues = userDetails.getAllValues();
			this.requestAttributes = RequestContextHolder.getRequestAttributes();
		}

		public ThreadLocalePropagationRunnable initThreadLocales() {
			SecurityContextHolder.setContext(this.securityContext);
			userDetails.setAllValues(this.userDetailsValues);
			RequestContextHolder.setRequestAttributes(requestAttributes);
			return this;
		}
	}

}
