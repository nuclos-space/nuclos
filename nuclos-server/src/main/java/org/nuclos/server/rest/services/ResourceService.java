package org.nuclos.server.rest.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.E;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.resource.ResourceCache;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.WebContext;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/resources")
@Produces(MediaType.APPLICATION_JSON)
public class ResourceService extends WebContext {

	public static final int ICONS_MAX_AGE_IN_SECDONS = 60 * 60 * 24;

	public static Response getStateIcon(String stateId, StateCache stateCache) {
		try {
			UID stateUID = Rest.translateFqn(E.STATE, stateId);
			StateVO state = null;
			try {
				state = stateCache.getStateById(stateUID);
			} catch (CommonFinderException e) {
				throw new NuclosWebException(Response.Status.NOT_FOUND, String.format("Status %s(%s) not found", stateUID, stateId));
			}
			NuclosImage image = state.getIcon();
			final Response.ResponseBuilder builder;

			if (image != null) {
				byte[] imageBytes = image.getContent();
				builder = Response.ok(imageBytes, "image/png");
				// return Response.ok(imageBytes).type("image/png").build();
			} else {
				builder = Response.ok(Rest.EMPTY_IMAGE, "image/gif");
				// return Response.ok(Rest.EMPTY_IMAGE).type("image/gif").build();
			}

			CacheControl cc = new CacheControl();
			cc.setMaxAge(ICONS_MAX_AGE_IN_SECDONS);
			builder.cacheControl(cc);
			return builder.build();
		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.STATE, stateId));
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/stateIcons/{stateId}")
	@RestServiceInfo(identifier="stateIcon", isFinalized=true, description="List icon of state")
	public Response stateIcon(@PathParam("stateId") String stateId) {
		return getStateIcon(stateId, stateCache);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/images/{resourceId}")
	@RestServiceInfo(identifier="resourceImage", isFinalized=true, description="Resource Image")
	public Response resourceImage(@PathParam("resourceId") String resourceId) {
		try {
			//TODO: E.RESOURCE correct? Compare with JsonFacory WebStaticComponent
			UID resourceUid = Rest.translateFqn(E.RESOURCE, resourceId);
			byte[] imageBytes = ResourceCache.getInstance().getResourceBytes(resourceUid);
			if (imageBytes != null) {
				return Response.ok(imageBytes).type("image/png").build();
			} else {
				return Response.ok(Rest.EMPTY_IMAGE).type("image/gif").build();
			}
		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.RESOURCE, resourceId));    		
		}
	}
	
}
