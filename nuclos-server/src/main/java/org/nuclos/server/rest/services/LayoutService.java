//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.nuclos.common.E;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.rule.Rules;
import org.nuclos.schema.layout.web.WebLayout;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.services.base.LayoutServiceBase;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.rvo.LayoutInfo;
import org.xml.sax.SAXException;

@Path("/layout")
@Produces(MediaType.APPLICATION_JSON)
public class LayoutService extends LayoutServiceBase {

	@GET
	@Path("/{layoutId}/calculated")
	@RestServiceInfo(identifier = "weblayoutCalculated", description = "Parsed weblayout (calc()-based) for the corresponding layout-id in JSON format.")
	public WebLayout layoutCalculated(@PathParam("layoutId") String layoutId) throws ParserConfigurationException, JAXBException, SAXException {
		return getWebLayoutCalculated(layoutId);
	}

	@GET
	@Path("/{layoutId}/fixed")
	@RestServiceInfo(identifier = "weblayoutFixed", description = "Parsed weblayout (table-based) for the corresponding layout-id in JSON format.")
	public WebLayout layoutFixed(@PathParam("layoutId") String layoutId) throws ParserConfigurationException, JAXBException, SAXException {
		return getWebLayoutFixed(layoutId);
	}

	@GET
	@Path("/{layoutId}/responsive")
	@RestServiceInfo(identifier = "weblayoutResponsive", description = "Parsed weblayout (grid-based) for the corresponding layout-id in JSON format.")
	public WebLayout layoutResponsive(@PathParam("layoutId") String layoutId) throws ParserConfigurationException, JAXBException, SAXException {
		return getWebLayoutResponsive(layoutId);
	}

	@GET
	@Path("/{layoutId}/layoutml")
	@RestServiceInfo(identifier = "layoutml", description = "Returns the raw LayoutML String.")
	public String layoutml(@PathParam("layoutId") String layoutId) {
		return super.getLayoutML(layoutId);
	}

	@GET
	@Path("/{layoutId}/layoutml_json")
	@RestServiceInfo(identifier = "layoutmlJson", description = "Returns the LayoutML object as JSON.")
	public Layoutml layoutmlObj(@PathParam("layoutId") String layoutId) throws JAXBException, SAXException, ParserConfigurationException {
		return getLayoutMLObj(layoutId);
	}

	/**
	 * TODO: Separate rules from the layout.
	 */
	@GET
	@Path("/{layoutId}/rules")
	@RestServiceInfo(identifier = "layoutRules", description = "Extracted rules of the Layout with the given ID.")
	public Rules layoutRules(@PathParam("layoutId") String layoutId) throws ParserConfigurationException, JAXBException, SAXException {
		return getRules(layoutId);
	}

	@GET
	@Path("/{layoutId}/info")
	@RestServiceInfo(identifier = "layoutInfo", description = "Returns the Layout info.")
	public LayoutInfo layoutInfo(@PathParam("layoutId") String layoutId) throws CommonPermissionException {
		return Rest.facade().getLayoutInfo(Rest.translateFqn(E.LAYOUT, layoutId));
	}

}