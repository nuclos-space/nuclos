package org.nuclos.server.rest.services.rvo;

import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.eclipse.jgit.util.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.common2.DateTime;

public class BusinessTestRVO implements JsonRVO {

	private static final EntityMeta<?> meta = E.BUSINESSTEST;

	private final BusinessTestVO vo;

	public BusinessTestRVO(BusinessTestVO vo) {
		this.vo = vo;
	}

	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		JsonObjectBuilder json = Json.createObjectBuilder();

		json.add("id", JsonFactory.buildJsonValue(vo.getId()));
		for (FieldMeta<?> field : meta.getFields()) {
			Object value = vo.getFieldValue(field.getUID());
			if (value == null) {
				json.addNull(field.getFieldName());
			} else {
				json.add(field.getFieldName(), JsonFactory.buildJsonValue(value));
			}
		}

		return json;
	}

	private boolean isMetaField(FieldMeta<?> field) {
		return StringUtils.equalsIgnoreCase(field.getFieldName(), "createdAt")
				|| StringUtils.equalsIgnoreCase(field.getFieldName(), "createdBy")
				|| StringUtils.equalsIgnoreCase(field.getFieldName(), "changedAt")
				|| StringUtils.equalsIgnoreCase(field.getFieldName(), "changedBy");
	}

	public void updateFromJson(JsonObject json) {
		for (FieldMeta<?> field : meta.getFields()) {
			if (field.isReadonly() || isMetaField(field)) {
				continue;
			}
			if (json.containsKey(field.getFieldName())) {
				if (json.isNull(field.getFieldName())) {
					vo.setFieldValue(field.getUID(), null);
				} else if (field.getJavaClass().isAssignableFrom(Integer.class)) {
					vo.setFieldValue(field.getUID(), json.getInt(field.getFieldName()));
				} else if (field.getJavaClass().isAssignableFrom(String.class)) {
					vo.setFieldValue(field.getUID(), json.getString(field.getFieldName()));
				} else if (field.getJavaClass().isAssignableFrom(DateTime.class)) {
					Long value = ((JsonNumber) json.get(field.getFieldName())).longValue();
					vo.setFieldValue(field.getUID(), new DateTime(value));
				} else if (field.getJavaClass().isAssignableFrom(Long.class)) {
					Long value = ((JsonNumber) json.get(field.getFieldName())).longValue();
					vo.setFieldValue(field.getUID(), value);
				} else {
					vo.setFieldValue(field.getUID(), json.get(field.getFieldName()));
				}
			}
		}
	}
}
