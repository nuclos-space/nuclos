package org.nuclos.server.rest.services.rvo;

import javax.json.JsonObjectBuilder;

public interface JsonRVO {

	JsonObjectBuilder getJSONObjectBuilder();
}
