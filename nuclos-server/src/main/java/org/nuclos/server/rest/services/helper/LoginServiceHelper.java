package org.nuclos.server.rest.services.helper;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Nonnull;
import javax.json.JsonObjectBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.LegalDisclaimer;
import org.nuclos.schema.rest.LoginInfo;
import org.nuclos.schema.rest.LoginInfoLinks;
import org.nuclos.schema.rest.LoginParams;
import org.nuclos.schema.rest.Mandator;
import org.nuclos.schema.rest.MandatorLinks;
import org.nuclos.schema.rest.ObjectFactory;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeBean;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.spring.NuclosWebApplicationInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Lazy;

@Configurable
public class LoginServiceHelper extends WebContext {

	@Lazy
	@Autowired
	ServerParameterProvider parameterProvider;

	@Lazy
	@Autowired
	MaintenanceFacadeBean maintenanceFacadeBean;

	protected LoginInfo buildLoginInfo() {
		return buildLoginInfo(getSessionContext());
	}

	protected LoginInfo buildLoginInfo(final SessionContext context) {
		LoginInfo.Builder<Void> builder = LoginInfo.builder()
				.withUsername(getUser())
				.withSessionId(context.getJSessionId())
				.withLocale(context.getLocale().toString())
				.withLinks(LoginInfoLinks.builder()
						.withBoMetas(buildRestLink("boMetas", RestLinks.Verbs.GET))
						.withMenu(buildRestLink("menu", RestLinks.Verbs.GET))
						.withSearch(buildRestLink("search", RestLinks.Verbs.GET))
						.withTasks(buildRestLink("tasks", RestLinks.Verbs.GET))
						.build()
				).withAllowedActions(buildAllowedActions(context));

		if (MaintenanceConstants.MAINTENANCE_MODE_ON.equals(maintenanceFacadeBean.getMaintenanceMode())) {
			builder = builder.withMaintenanceMode(true);
		}

		if (NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			builder = builder.withClientIp(getRemoteAddr());
		}

		if (Rest.facade().isSuperUser()) {
			builder = builder.withSuperUser(true);
		}

		String initialEntity = Rest.facade().getInitialEntityEntry();
		if (StringUtils.isNotBlank(initialEntity)) {
			builder = builder.withInitialEntity(initialEntity);
		}

		if (context.getMandators() != null) {
			for (MandatorVO mandatorVO: context.getMandators()) {
				builder = builder.addMandators(buildMandator(mandatorVO));
			}
		}

		if (context.getMandatorUID() != null) {
			final MandatorVO mandatorVO = SecurityCache.getInstance().getMandator(context.getMandatorUID());
			builder = builder.withMandator(buildMandator(mandatorVO));
		}

		return builder.build();
	}

	private Mandator buildMandator(final MandatorVO mandatorVO) {
		Mandator.Builder<Void> builder = Mandator.builder()
				.withMandatorId(mandatorVO.getUID().toString())
				.withMandatorLevelId(mandatorVO.getLevelUID().toString())
				.withName(mandatorVO.getName())
				.withPath(mandatorVO.getPath())
				.withLinks(
						MandatorLinks.builder()
						.withChooseMandator(buildRestLink("chooseMandator", RestLinks.Verbs.POST, mandatorVO.getUID().getString()))
						.build()
				);

		if (StringUtils.isNotBlank(mandatorVO.getColor())) {
			builder = builder.withColor(mandatorVO.getColor());
		}

		return builder.build();
	}

	private LoginInfo.AllowedActions buildAllowedActions(final SessionContext context) {
		LoginInfo.AllowedActions.Builder<Void> builder = LoginInfo.AllowedActions.builder();

		if (isActionAllowed(context, Actions.ACTION_CONFIGURE_CHARTS)) {
			builder = builder.withConfigureCharts(true);
		}
		if (isActionAllowed(context, Actions.ACTION_CONFIGURE_PERSPECTIVES)) {
			builder = builder.withConfigurePerspectives(true);
		}
		if (isActionAllowed(context, Actions.ACTION_EXPORT_SEARCHRESULT)) {
			builder = builder.withPrintSearchResultList(true);
		}
		if (isActionAllowed(context, Actions.ACTION_SHARE_PREFERENCES)) {
			builder = builder.withSharePreferences(true);
		}
		if (isActionAllowed(context, Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS)) {
			builder = builder.withWorkspaceCustomizeEntityAndSubFormColumn(true);
		}
		if (isActionAllowed(context, Actions.ACTION_BULKEDIT)) {
			builder = builder.withCollectiveProcessing(true);
		}

		return builder.build();
	}

	/**
	 * Adds the given action to allowedActions, if the current user has the respective right.
	 *
	 * Returns true, if the action is allowed.
	 */
	private static boolean isActionAllowed(
			final SessionContext sessionContext,
			final String action
	) {
		return SecurityCache.getInstance().getAllowedActions(
				sessionContext.getUsername(),
				null
		).contains(action);
	}

	@Nonnull
	protected LoginResult doLogin(
			final LoginParams data,
			final HttpServletRequest request
	) {
		try {
			final LoginResult result = new LoginResult();
			final String jSessionId = request.getSession().getId();

			final SessionContext sessionContext = login(data, jSessionId);

			LoginInfo loginInfo = buildLoginInfo(sessionContext);
			result.setLoginInfo(loginInfo);

			final NewCookie sessionCookie = getSessionCookie(data, request, jSessionId);
			result.addCookie(sessionCookie);

			synchronizeSessionAndCookieTimeout(request, sessionCookie);

			return result;
		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
	}

	/**
	 * Adjusts the HTTP session timeout to the cookie expiration.
	 */
	private void synchronizeSessionAndCookieTimeout(
			final HttpServletRequest request,
			final NewCookie sessionCookie
	) {
		int maxAge = sessionCookie.getMaxAge();
		if (maxAge <= 0) {
			// A max age of -1 means "ends with the browser session" for the cookie,
			// 0 means "ends immediately",
			// but for the HTTP session max age <= 0 means "no timeout at all".
			// Therefore we use the default session timeout here.
			maxAge = parameterProvider.getIntValue(
					ParameterProvider.SECURITY_SESSION_TIMEOUT_IN_SECONDS,
					NuclosWebApplicationInitializer.DEFAULT_SESSION_TIMEOUT
			);
		}
		request.getSession().setMaxInactiveInterval(maxAge);
	}

	/**
	 * Determines the session timeout based on login and system parameters.
	 */
	private int getSessionTimeout(final LoginParams params) {
		// For autologin the session is valid for a year
		if (params.isAutologin()) {
			return 60 * 60 * 24 * 365;
		}

		return parameterProvider.getIntValue(
				ParameterProvider.SECURITY_SESSION_TIMEOUT_IN_SECONDS,
				NuclosWebApplicationInitializer.DEFAULT_SESSION_TIMEOUT
		);
	}

	protected void doChooseMandator(String mandatorId) {
		SessionContext session = getSessionContext();
		UID mandatorUID = UID.parseUID(mandatorId);
		try {
			Rest.facade().chooseMandator(mandatorUID);
			session.setMandatorUID(mandatorUID);
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN, e.getMessage());
		}
	}

	protected void doLogout(HttpServletRequest request) {
		SessionContext context = getSessionContext();
		Rest.facade().webLogout(context.getJSessionId());

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}
	}

	protected String getVersion() {
		return ApplicationProperties.getInstance().getNuclosVersion().getSimpleVersionNumber();
	}

	protected String getVersionTimestamp() {
		return ApplicationProperties.getInstance().getNuclosVersion().getVersionDateString();
	}

	protected String getDbVersion() {
		try {
			return Rest.facade().getDbVersion();
		} catch (Exception ex) {
			// TODO
			EntityMeta<?> TODO = null;
			throw new NuclosWebException(ex, Rest.translateFqn(TODO, "dbversion"));
		}
	}

	private NewCookie getSessionCookie(
			final LoginParams params,
			final HttpServletRequest request,
			final String jSessionId
	) {
		String path = request.getContextPath();

		// Cookie path must always end with a slash
		if (!StringUtils.endsWith(path, "/")) {
			path = path + "/";
		}

		final int sessionTimeout = getSessionTimeout(params);

		// A cookie without "expires" date and max-age -1,
		// that expires at the end of the browser session!
		NewCookie sessionCookie = new NewCookie(
				"JSESSIONID",				// name
				jSessionId,					// value
				path,						// path
				null,						// domain
				null,						// comment
				-1,							// max age in seconds
				request.isSecure(),			// HTTPS?
				true						// HttpOnly
		);

		// If Autologin is enabled, set an explicit expiration date, 
		// so the cookie does not expire at the end of the browser session.
		if (params.isAutologin()) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.SECOND, sessionTimeout);
			sessionCookie = new NewCookie(
					sessionCookie,
					sessionCookie.getComment(),
					sessionTimeout,
					cal.getTime(),
					sessionCookie.isSecure(),
					sessionCookie.isHttpOnly()
			);
		}

		return sessionCookie;
	}

	public static class LoginResult {
		private List<NewCookie> cookies;
		private JsonObjectBuilder responseJSON;
		private LoginInfo loginInfo;

		public LoginResult() {
			cookies = new ArrayList<>();
		}

		public NewCookie[] getCookies() {
			return cookies.toArray(new NewCookie[]{});
		}

		public void addCookie(NewCookie cookie) {
			cookies.add(cookie);
		}

		public LoginInfo getLoginInfo() {
			return loginInfo;
		}

		public void setLoginInfo(final LoginInfo loginInfo) {
			this.loginInfo = loginInfo;
		}
	}

	protected List<LegalDisclaimer> getLegalDisclaimers() {
		IEntityObjectProcessor<UID> proc = NucletDalProvider.getInstance().getEntityObjectProcessor(E.RESOURCE);
		CollectableSearchCondition csc = SearchConditionUtils.newLikeCondition(E.RESOURCE.name, "LEGAL_DISCLAIMER*");
		CollectableSorting sort = CollectableSorting.createSorting(E.RESOURCE.name.getUID(), true);

		List<EntityObjectVO<UID>> lstEO = proc.getBySearchExpression(new CollectableSearchExpression(csc, sort));

		List<LegalDisclaimer> ret = new ArrayList<>();

		for (EntityObjectVO<UID> eo : lstEO) {
			byte[] content = eo.getFieldValue(E.RESOURCE.content);

			if (content != null) {
				LegalDisclaimer disclaimer = new ObjectFactory().createLegalDisclaimer();
				disclaimer.setName(eo.getFieldValue(E.RESOURCE.description));
				try {
					disclaimer.setText(new String(content, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					LOG.warn("Disclaimer content is not UTF-8 encoded!", e);
					disclaimer.setText(new String(content));
				}
				ret.add(disclaimer);
			}
		}

		return ret;
	}
}
