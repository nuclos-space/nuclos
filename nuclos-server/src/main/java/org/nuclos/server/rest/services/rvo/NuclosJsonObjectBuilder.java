package org.nuclos.server.rest.services.rvo;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

public class NuclosJsonObjectBuilder implements JsonObjectBuilder {
	private final JsonObjectBuilder builder;
	
	public NuclosJsonObjectBuilder() {
		this(Json.createObjectBuilder());
	}

	public NuclosJsonObjectBuilder(JsonObjectBuilder builder) {
		this.builder = builder;
	}
	
	public JsonObjectBuilder add(String name, Object value) {
		if (value == null) {
			return builder.addNull(name);
		} 
		if (value instanceof String) {
			return builder.add(name, (String)value);
		}
		if (value instanceof Integer) {
			return builder.add(name, (Integer)value);
		}
		if (value instanceof Long) {
			return builder.add(name, (Long)value);
		}
		if (value instanceof Boolean) {
			return builder.add(name, (Boolean) value);
		}
		if (value instanceof JsonValue) {
			return builder.add(name, (JsonValue)value);
		}
		if (value instanceof JsonObjectBuilder) {
			return builder.add(name, (JsonObjectBuilder)value);
		}
		if (value instanceof JsonArrayBuilder) {
			return builder.add(name, (JsonArrayBuilder)value);
		}
		if (value instanceof Float) {
			return builder.add(name, (Float)value);
		}
		if (value instanceof Double) {
			return builder.add(name, (Double)value);
		}
		if (value instanceof BigDecimal) {
			return builder.add(name, (BigDecimal)value);
		}
		if (value instanceof BigInteger) {
			return builder.add(name, (BigInteger)value);
		}
		return builder.add(name, value.toString());
	}

	@Override
	public JsonObjectBuilder add(String name, JsonValue value) {
		return builder.add(name, value);
	}

	@Override
	public JsonObjectBuilder add(String name, String value) {
		return builder.add(name, value);
	}

	@Override
	public JsonObjectBuilder add(String name, BigInteger value) {
		return builder.add(name, value);
	}

	@Override
	public JsonObjectBuilder add(String name, BigDecimal value) {
		return builder.add(name, value);
	}

	@Override
	public JsonObjectBuilder add(String name, int value) {
		return builder.add(name, value);
	}

	@Override
	public JsonObjectBuilder add(String name, long value) {
		return builder.add(name, value);
	}

	@Override
	public JsonObjectBuilder add(String name, double value) {
		return builder.add(name, value);
	}

	@Override
	public JsonObjectBuilder add(String name, boolean value) {
		return builder.add(name, value);
	}

	@Override
	public JsonObjectBuilder addNull(String name) {
		return builder.add(name, name);
	}

	@Override
	public JsonObjectBuilder add(String name, JsonObjectBuilder b) {
		return builder.add(name, b);
	}

	@Override
	public JsonObjectBuilder add(String name, JsonArrayBuilder b) {
		return builder.add(name, b);
	}

	@Override
	public JsonObject build() {
		return builder.build();
	}
}