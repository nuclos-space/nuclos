package org.nuclos.server.rest.misc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.PostConstruct;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RestPrintoutFileDownloadCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(RestPrintoutFileDownloadCache.class);
	
	private ConcurrentMap<CacheKey, CacheValue> cache = new ConcurrentHashMap<CacheKey, CacheValue>();
	
	@PostConstruct
	private void init() {
		Thread backgroundCleanupThread = new Thread("RestPrintoutFileDownloadCleaner") {
			@Override
			public void run() {
				try {
					while (true) {
						Thread.sleep(1000 * 60 * 5);
						long sysdate = System.currentTimeMillis();
						try {
							for (Entry<CacheKey, CacheValue> entry : cache.entrySet()) {
								if (entry.getKey().createdAt + (1000 * 60 * 60 * 12) < sysdate) {
									try {
										entry.getValue().file.delete();
									} finally {
										cache.remove(entry.getKey());
									}
								}
							}
						} catch (Exception ex) {
							LOG.warn("Cleanup cache throws exception: {}", ex.getMessage());
							LOG.debug(ex.getMessage(), ex);
						}
					}
				} catch (InterruptedException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		};
		backgroundCleanupThread.setDaemon(true);
		backgroundCleanupThread.start();
	}
	
	public String cacheFile(String boMetaId, String boId, String outputFormatId, NuclosFile file) throws IOException {
		final String fileId = new UID().getString();
		final CacheKey key = new CacheKey(boMetaId, boId, outputFormatId, fileId);
		final File tempFile = File.createTempFile("NuclosRestPrintout", null);
		tempFile.deleteOnExit();
		
		FileOutputStream fop = null;
		try {
			fop = new FileOutputStream(tempFile);
			fop.write(file.getContent());
			fop.flush();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		cache.put(key, new CacheValue(file.getName(), tempFile));
		
		return fileId;
	}
	
	public CacheResult get(String boMetaId, String boId, String outputFormatId, String fileId) throws IOException {
		final CacheKey key = new CacheKey(boMetaId, boId, outputFormatId, fileId);
		CacheValue cacheValue = cache.get(key);
		if (cacheValue == null || !cacheValue.file.exists()) {
			throw new IOException();
		}
		FileInputStream fis = null;
		 
		try {
			fis = new FileInputStream(cacheValue.file);
			byte[] content = new byte[(int) cacheValue.file.length()];    
			fis.read(content);
			return new CacheResult(cacheValue.fileName, content);
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	private static class CacheKey {
		private final String cacheKey;
		public final long createdAt;
		
		public CacheKey(String boMetaId, String boId, String outputFormatId,
				String fileId) {
			super();
			cacheKey = boMetaId + boId + outputFormatId + fileId;
			createdAt = System.currentTimeMillis();
		}
		
		@Override
		public boolean equals(Object that) {
			if (this == that) {
				return true;
			}
			if (that instanceof CacheKey) {
				return LangUtils.equal(this.cacheKey, ((CacheKey) that).cacheKey);
			}
			return super.equals(that);
		}
		
		@Override
		public int hashCode() {
			return cacheKey.hashCode();
		}
		
		@Override
		public String toString() {
			return cacheKey;
		}
	}
	
	private static class CacheValue {
		public final String fileName;
		public final File file;
		public CacheValue(String fileName, File file) {
			super();
			this.fileName = fileName;
			this.file = file;
		}
		
	}
	
	public static class CacheResult {
		public final String fileName;
		public final byte[] content;
		public CacheResult(String fileName, byte[] content) {
			super();
			this.fileName = fileName;
			this.content = content;
		}
		
	}
	
}
