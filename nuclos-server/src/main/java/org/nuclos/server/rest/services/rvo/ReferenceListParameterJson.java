package org.nuclos.server.rest.services.rvo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.RigidUtils;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.server.rest.misc.IWebContext;

public class ReferenceListParameterJson {

	private final String vlptype;
	private final String vlpvalue;
	private final Map<String, String> params;
	private final String pk;
	private final String mandatorId;

	private final String quickSearchInput;
	private Integer chunkSize;

	public ReferenceListParameterJson(MultivaluedMap<String, String> params, IWebContext webContext) {
		this.vlptype = webContext.getFirstParameter("vlptype", String.class);
		this.vlpvalue = webContext.getFirstParameter("vlpvalue", String.class);
		this.pk = webContext.getFirstParameter("intid", String.class);
		this.quickSearchInput = webContext.getFirstParameter("quickSearchInput", String.class);
		this.mandatorId = RigidUtils.nullIfEmpty(webContext.getFirstParameter("mandatorId", String.class));
		setChunkSize(webContext.getFirstParameter("chunkSize", String.class));

		this.params = new HashMap<String, String>();
		for (String key : params.keySet()) {
			if ("vlptype".equals(key) || "vlpvalue".equals(key)) {
				continue;
			}
			this.params.put(key, (String)webContext.getFirstParameter(key, String.class));
		}
	}

	/**
	 * only for VLP
	 */
	public ReferenceListParameterJson(MultivaluedMap<String, String> params) {
		this.vlptype = params.getFirst("vlptype");
		this.vlpvalue = params.getFirst("vlpvalue");
		this.pk = params.getFirst("intid");
		this.quickSearchInput = params.getFirst("quickSearchInput");
		this.mandatorId = RigidUtils.nullIfEmpty(params.getFirst("mandatorId"));
		setChunkSize(params.getFirst("chunkSize"));

		this.params = new HashMap<String, String>();
		for (String key : params.keySet()) {
			if ("vlptype".equals(key) || "vlpvalue".equals(key)) {
				continue;
			}
			this.params.put(key, (String)params.getFirst(key));
		}
	}

	private void setChunkSize(String chunkSize) {
		if (StringUtils.isNotBlank(chunkSize)) {
			this.chunkSize = Integer.parseInt(chunkSize);
 		} else {
			this.chunkSize = null;
		}
	}

	public String getPK() {
		return pk;
	}

	public String getVlpType() {
		return vlptype;
	}

	public String getVlpvalue() {
		return vlpvalue;
	}

	public Map<String, String> getParams() {
		return Collections.unmodifiableMap(params);
	}

	public String getQuickSearchInput() {
		return quickSearchInput;
	}

	public String getMandatorId() {
		return mandatorId;
	}

	public Integer getChunkSize() {
		return this.chunkSize;
	}

	@Override
	public String toString() {
		return "VlpType=" + vlptype + " VlpValue=" + vlpvalue + " params=" + params + " quickSearchInput=" + quickSearchInput;
	}

	public WebValueListProvider getWebValueListProviderIfAny() {
		return getWebValueListProviderIfAny(this);
	}

	public static WebValueListProvider getWebValueListProviderIfAny(ReferenceListParameterJson paramJson) {
		if (paramJson.getVlpType() != null) {
			WebValueListProvider wvlp = new WebValueListProvider(paramJson.getVlpType(), paramJson.getVlpvalue());
			for (String key : paramJson.getParams().keySet()) {
				wvlp.addParameter(key, paramJson.getParams().get(key));
			}
			return wvlp;
		}
		return null;
	}

}
