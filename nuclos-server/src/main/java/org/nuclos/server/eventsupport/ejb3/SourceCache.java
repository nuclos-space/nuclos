//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.eventsupport.ejb3;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.nuclos.api.ide.valueobject.ISourceItem;
import org.nuclos.api.ide.valueobject.SourceItem;
import org.nuclos.server.customcode.codegenerator.GeneratedFile;
import org.nuclos.server.customcode.valueobject.CodeVO;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Component;

/**
 * Spring component to track information on java code compiled by Nuclos
 * (rules, business objects, business support objects). This is the 
 * central place that 'knows' if java code has changed (and thus must
 * be recompiled and/or re-delivered to the eclipse plugin.
 * <p>
 * Information is retrieved by the fully qualified (java) class name.
 * </p>
 * @see org.nuclos.api.ide.valueobject.SourceType
 * @author Thomas Pasch
 * @since Nuclos 3.14.18, 3.15.18, 4.0.15
 */
@Component
public class SourceCache {
	
	public static final String DIGEST = "SHA-512";
	
	//
	
	private final Map<String,ISourceItem> qn2Item = new ConcurrentHashMap<String, ISourceItem>();
	
	private final Map<String,SourceInfo> qn2Source = new ConcurrentHashMap<String, SourceInfo>();
	
	private final Map<String,CodeInfo> qn2Code = new ConcurrentHashMap<String, CodeInfo>();
	
	SourceCache() {
	}
	
	public void addOrUpdate(ISourceItem item, SourceInfo info) {
		final String qn = item.getQualifiedName();
		final String qn2 = info.getQualifiedName();
		if (!qn.equals(qn2)) {
			throw new IllegalStateException();
		}
		addOrUpdate(item);
		addOrUpdate(info);
	}
	
	void addOrUpdate(SourceInfo info) {
		final SourceInfo old = qn2Source.put(info.getQualifiedName(), info);
	}
	
	private void addOrUpdate(ISourceItem item) {
		final String qn = item.getQualifiedName();
		final String hashValue = item.getHashValue();
		final CodeInfo code = qn2Code.get(qn);
		if (code != null) {
			if (hashValue == null) {
				item.setHashValue(code.getHashValue());
			} else {
				code.setHashValue(hashValue);
			}
		}
		qn2Item.put(qn, item);
	}
	
	public void addOrUpdate(CodeVO codeVO) {
		final CodeInfo info = new CodeInfo(codeVO);
		addOrUpdate(info);
	}
	
	private void addOrUpdate (CodeInfo info) {
		final String qn = info.getQualifiedName();
		String hashValue = info.getHashValue();
		final CodeInfo old = qn2Code.put(qn, info);
		if (hashValue == null && old != null) {
			// old value should survive
			hashValue = old.getHashValue();
			info.setHashValue(hashValue);
		}
		
		final ISourceItem item = qn2Item.get(qn);
		if (item != null) {
			if (hashValue != null) {
				item.setHashValue(hashValue);
			} else {
				info.setHashValue(item.getHashValue());
			}
		}
	}
	
	public void addOrUpdate(GeneratedFile gf) {
		final CodeInfo info = new CodeInfo(gf);
		addOrUpdate(info);
	}
	
	public void updateHashValue(String qname, String hashValue) {
		if (hashValue == null) {
			throw new NullPointerException();
		}
		final ISourceItem ci = qn2Item.get(qname);
		if (ci != null) {
			ci.setHashValue(hashValue);
		} 
		final CodeInfo code = qn2Code.get(qname);
		if (code != null) {
			code.setHashValue(hashValue);
		}
	}
	
	public String getHashValue(String qname) {
		final ISourceItem ci = qn2Item.get(qname);
		String result = null;
		if (ci != null) {
			result = ci.getHashValue();
		}
		if (result == null) {
			final CodeInfo code = qn2Code.get(qname);
			if (code != null) {
				result = code.getHashValue();
			}
		}
		return result;
	}
	
	public ISourceItem getOrCreateSourceItem(SourceInfo info) {
		final String qn = info.getQualifiedName();
		ISourceItem result = qn2Item.get(qn);
		if (result == null) {
			result = new SourceItem();
			result.setType(info.getType());
			result.setQualifiedName(qn);
			
			final CodeInfo code = qn2Code.get(qn);
			if (code != null) {
				result.setHashValue(code.getHashValue());
				// no UID in ISourceItems! (tp)
				result.setId(code.getPrimaryKey().getString());
			}
			
			qn2Item.put(qn, result);
		} 
		return result;
	}

}
