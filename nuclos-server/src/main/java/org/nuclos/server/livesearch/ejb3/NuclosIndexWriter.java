package org.nuclos.server.livesearch.ejb3;

import java.io.IOException;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.lucene.IndexVO;

public class NuclosIndexWriter extends IndexWriter {
	private static final String VERSION = "V011";
	
	public static String getVersion() {
		return VERSION;
	}

	public static final String IDFIELD = "field";
	public static final String IDPK = "pk";
	public static final String IDTEXT = "text";
	public static final String IDREF = "ref";

	public NuclosIndexWriter(Directory d, IndexWriterConfig conf) throws IOException {
		super(d, conf);
	}
	
	public boolean indexVO(IndexVO<?> vo) throws IOException {
		if (IndexTools.INSTANCE().switchedOff()) {
			return false;
		}

		boolean changed = false;
		String sPk = String.valueOf(vo.getPrimaryKey());
		
		if (!vo.isFlagNew()) {
			deleteDocuments(new Term(IDPK, sPk));
			changed = true;
			
			if (vo.isFlagRemoved()) {
				return changed;
			}
		}
				
		Map<UID, Object> mpValues = vo.indexFields();
		if (mpValues == null) {
			//TODO: In this case, refill from DB.
			return changed;
		}
		
		StringField lfid = new StringField(IDPK, sPk, Field.Store.YES);
		
		for (UID field : mpValues.keySet()) {
			Pair<String, Boolean> text = parseVOString(mpValues.get(field));
			if (text == null || text.x.length() < 2) {
				continue;
			}
			
			StringField sffield = new StringField(IDFIELD, field.getString(), Field.Store.YES);
			Field f;
			if (text.y) {
				f = new StringField(IDREF, text.x, Field.Store.NO);				
			} else {
				f = new TextField(IDTEXT, text.x, Field.Store.NO);
			}
			
			Document doc = new Document();
			doc.add(lfid);
			doc.add(sffield);
			doc.add(f);
			addDocument(doc);
			changed = true;
		}
		
		return changed;
	}
	
	public static Pair<String, Boolean> parseVOString(Object o) {

		if (o instanceof String) {
			return new Pair<String, Boolean>(((String)o).trim(), false);
		}
		
		if (o instanceof Long) {
			return new Pair<String, Boolean>(o.toString(), true);
		}
		
		if (o instanceof UID) {
			// toLowerCase: Lucene builds lowercase search strings and without indexing with lowercase uids, nothing found.
			return new Pair<String, Boolean>(((UID)o).getString().toLowerCase(), true);			
		}
		
		return null;
	}
}
