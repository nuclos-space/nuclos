import { Component, Inject, Injectable, Input, OnDestroy, OnInit } from '@angular/core';

import { ${ADDON_NAME_CAMEL_CASE}Service } from './${ADDON_FILE_NAME}.service';
import { IEntityObject, ResultlistContext, RESULTLIST_CONTEXT, LayoutContext, LAYOUT_CONTEXT } from '@nuclos/nuclos-addon-api';
import { Subscription } from 'rxjs/Subscription';

@Component({
	selector: 'nuc-addon-${ADDON_FILE_NAME}',
	styles: [`
        :host {
			display: block;
			width: 100%;
			height: 100%;
			border: 1px dashed lightskyblue;
			text-align: center;
        }
	`],
	template: `
		<div>
			Addon ${ADDON_NAME_CAMEL_CASE} works.
			<br>
			<div *ngIf="eo">
				selected item: {{eo.getId()}}
			</div>
		</div>
	`
})
export class ${ADDON_NAME_CAMEL_CASE}Component implements OnInit, OnDestroy {


	private subscriptions: Subscription[] = [];

	eo: IEntityObject;

	constructor(
		// @Inject(LAYOUT_CONTEXT) private layoutContext: LayoutContext,
		// @Inject(RESULTLIST_CONTEXT) private resultlistContext: ResultlistContext,
		private service: ${ADDON_NAME_CAMEL_CASE}Service
	) {
	}

	ngOnInit() {
		// get addon property for layout adddon
		// let someAddonProperty = this.layoutContext.getAddonProperty('someAddonProperty');

		/*
		// applies for layout addons
		if (this.layoutContext) {
			this.subscriptions.push(
				this.layoutContext.onEoSelection().subscribe(
					(eo: IEntityObject) => {
						if (eo) {
							this.eo = eo;
						}
					}
				)
			);
		}
		*/
	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

}
