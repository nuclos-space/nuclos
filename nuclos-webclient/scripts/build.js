#!/usr/bin/env node

const buildMode = process.argv[2];
const ram = process.env.npm_package_config_build_max_space_mb;
const baseHref = process.env.npm_package_config_build_base_href;
const outputPath = process.env.npm_package_config_build_output_path;
var cmd = "node --max_old_space_size="+ram+" ./node_modules/@angular/cli/bin/ng build --aot --base-href "+baseHref+" --output-path "+outputPath;
if ( buildMode == "prod" ) {
	cmd += " --prod --build-optimizer --vendor-chunk --output-hashing all --extract-css true --named-chunks false"
}
console.log('Building: ', cmd);
child = require('child_process').execSync(cmd, {
	cwd: '.',
	stdio: [ 'ignore', 1, 2 ]
});
