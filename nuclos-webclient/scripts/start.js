#!/usr/bin/env node

// change host value by adding --@nuclos/nuclos-webclient:webclient-host="172.17.0.1"
// change port value by adding --@nuclos/nuclos-webclient:webclient-port=4300

const host = process.env.npm_package_config_webclient_host;
const port = process.env.npm_package_config_webclient_port;
const cmd = "node ./node_modules/@angular/cli/bin/ng serve --host="+host+" --port="+port+" --aot"
console.log('Starting: ', cmd);
child = require('child_process').execSync(cmd, {
	cwd: '.',
	stdio: [ 'ignore', 1, 2 ]
});
