/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebLabelComponent } from './web-label.component';

xdescribe('WebLabelComponent', () => {
	let component: WebLabelComponent;
	let fixture: ComponentFixture<WebLabelComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebLabelComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebLabelComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
