/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebDatechooserComponent } from './web-datechooser.component';

xdescribe('WebDatechooserComponent', () => {
	let component: WebDatechooserComponent;
	let fixture: ComponentFixture<WebDatechooserComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebDatechooserComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebDatechooserComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
