import { Component, Injector, OnInit } from '@angular/core';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-checkbox',
	templateUrl: './web-checkbox.component.html',
	styleUrls: ['./web-checkbox.component.css']
})
export class WebCheckboxComponent extends AbstractInputComponent<WebCheckbox> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

}
