/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebTextfieldComponent } from './web-textfield.component';

xdescribe('WebTextfieldComponent', () => {
	let component: WebTextfieldComponent;
	let fixture: ComponentFixture<WebTextfieldComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebTextfieldComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebTextfieldComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
