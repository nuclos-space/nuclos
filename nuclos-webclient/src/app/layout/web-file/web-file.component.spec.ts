/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebFileComponent } from './web-file.component';

xdescribe('WebFileComponent', () => {
	let component: WebFileComponent;
	let fixture: ComponentFixture<WebFileComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebFileComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebFileComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
