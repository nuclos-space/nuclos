/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebTabcontainerComponent } from './web-tabcontainer.component';

xdescribe('WebTabcontainerComponent', () => {
	let component: WebTabcontainerComponent;
	let fixture: ComponentFixture<WebTabcontainerComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebTabcontainerComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebTabcontainerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
