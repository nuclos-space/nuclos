/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebButtonExecuteRuleComponent } from './web-button-execute-rule.component';

xdescribe('WebButtonExecuteRuleComponent', () => {
	let component: WebButtonExecuteRuleComponent;
	let fixture: ComponentFixture<WebButtonExecuteRuleComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebButtonExecuteRuleComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebButtonExecuteRuleComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
