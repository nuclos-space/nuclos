/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebButtonChangeStateComponent } from './web-button-change-state.component';

xdescribe('WebButtonChangeStateComponent', () => {
	let component: WebButtonChangeStateComponent;
	let fixture: ComponentFixture<WebButtonChangeStateComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebButtonChangeStateComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebButtonChangeStateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
