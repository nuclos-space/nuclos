/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebButtonHyperlinkComponent } from './web-button-hyperlink.component';

xdescribe('WebButtonHyperlinkComponent', () => {
	let component: WebButtonHyperlinkComponent;
	let fixture: ComponentFixture<WebButtonHyperlinkComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebButtonHyperlinkComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebButtonHyperlinkComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
