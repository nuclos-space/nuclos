import { Injector, OnInit } from '@angular/core';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { AbstractWebComponent } from '../shared/abstract-web-component';

/**
 * Abstract base for different kinds of buttons (state-change, custom-rule, etc.).
 */
export abstract class WebButtonComponent<T extends WebButton>  extends AbstractWebComponent<T> implements OnInit {

	private config: NuclosConfigService;

	constructor(injector: Injector) {
		super(injector);

		this.config = injector.get(NuclosConfigService);
	}

	ngOnInit() {
	}

	abstract buttonClicked();

	abstract getCssClass(): string;

	isEnabled(): boolean {
		return this.webComponent && this.webComponent.enabled
			&& (!this.webComponent.disableDuringEdit || !this.eo.isDirty());
	}

	hasIcon() {
		return !!this.webComponent.icon;
	}

	getIconUrl() {
		return this.config.getResourceURL(this.webComponent.icon);
	}

	getLabel() {
		return this.webComponent && this.webComponent.label;
	}
}
