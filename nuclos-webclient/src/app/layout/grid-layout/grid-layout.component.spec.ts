/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GridLayoutComponent } from './grid-layout.component';

xdescribe('GridComponent', () => {
	let component: GridLayoutComponent;
	let fixture: ComponentFixture<GridLayoutComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GridLayoutComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GridLayoutComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
