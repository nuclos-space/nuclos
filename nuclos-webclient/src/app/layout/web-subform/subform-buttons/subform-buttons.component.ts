import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EntityMeta } from '../../../modules/entity-object-data/shared/bo-view.model';
import { EntityObjectPreferenceService } from '../../../modules/entity-object/shared/entity-object-preference.service';
import { Preference, SideviewmenuPreferenceContent } from '../../../modules/preferences/preferences.model';
import { WebSubformComponent } from '../web-subform.component';

@Component({
	selector: 'nuc-subform-buttons',
	templateUrl: './subform-buttons.component.html',
	styleUrls: ['./subform-buttons.component.css']
})
export class SubformButtonsComponent implements OnInit {

	@Input() subform: WebSubformComponent;
	@Input() meta: EntityMeta;

	selectedSideviewmenuPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> = new BehaviorSubject(undefined);

	constructor(private entityObjectPreferenceService: EntityObjectPreferenceService) {
	}

	ngOnInit() {
		this.selectedSideviewmenuPref$ = this.entityObjectPreferenceService.getSubformColumnPreferenceSelection(this.meta.getBoMetaId());
	}

}
