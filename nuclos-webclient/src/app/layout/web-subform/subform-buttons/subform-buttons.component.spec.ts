import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubformButtonsComponent } from './subform-buttons.component';

xdescribe('SubformButtonsComponent', () => {
	let component: SubformButtonsComponent;
	let fixture: ComponentFixture<SubformButtonsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SubformButtonsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SubformButtonsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
