/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebSubformComponent } from './web-subform.component';

xdescribe('WebSubformComponent', () => {
	let component: WebSubformComponent;
	let fixture: ComponentFixture<WebSubformComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebSubformComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebSubformComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
