import { LinkContainer } from '../../../data/schema/link.model';

export interface MenuItem {
	name: string;
	boMetaId?: string;
	icon?: string;
	createNew: boolean;
	links: LinkContainer;
	entries?: MenuItem[];
}
