import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { I18nModule } from '../i18n/i18n.module';
import { InputRequiredModule } from '../input-required/input-required.module';
import { GenerationResultComponent } from './generation-result/generation-result.component';
import { GenerationComponent } from './generation.component';
import { NuclosGenerationService } from './shared/nuclos-generation.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		SharedModule,

		I18nModule,
		InputRequiredModule
	],
	declarations: [
		GenerationComponent,
		GenerationResultComponent
	],
	providers: [
		NuclosGenerationService
	],
	exports: [
		GenerationComponent
	],
	entryComponents: [
		GenerationResultComponent
	]
})
export class GenerationModule {
}
