import { Link } from '../../../data/schema/link.model';
import { EntityObjectData } from '../../entity-object-data/shared/bo-view.model';

/**
 * Represents an object generator.
 */
export class Generation {
	generationId: string;
	name: string;
	target: string;
	nonstop: boolean;
	internal: boolean;

	links: {
		generate: Link;
	};
}

/**
 * Represents the result of a generation request to the REST service.
 */
export class GenerationResult {
	bo: EntityObjectData;
	complete: boolean;
	closeOnException: boolean;
	refreshSource: boolean;
	showGenerated: boolean;
	openInOverlay: boolean;
	dialogTitle: string;
	dialogMode: boolean;
	dialogWidth: number;
	dialogHeight: number;

	businessError?: string;
}
