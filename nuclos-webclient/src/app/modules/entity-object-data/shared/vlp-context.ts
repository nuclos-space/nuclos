import { Logger } from '../../log/shared/logger';
import { EntityObject } from './entity-object.class';
import { VlpParameters } from './vlp-parameters';

export class VlpContext {
	private vlpParamMap = new Map<string, VlpParameters>();

	constructor(
		private eo: EntityObject
	) {
	}

	clear() {
		this.vlpParamMap.clear();
	}

	/**
	 * @param entityClassId Can be the entity class of this EO or of a sub-EO.
	 */
	getVlpParametersForAttribute(
		entityClassId: string,
		attributeId: string
	) {
		let result;
		let parametersForEntity = this.getVlpParameters(entityClassId);

		if (parametersForEntity) {
			result = parametersForEntity.getParametersForAttribute(attributeId);
		}

		Logger.instance.debug(
			'Getting VLP params from %o for entity %o, attribute %o, result = %o',
			this,
			entityClassId,
			attributeId,
			result
		);

		return result;
	}

	getVlpParameters(entityClassId: string) {
		return this.vlpParamMap.get(entityClassId);
	}

	setVlpParameter(
		entityClassId: string,
		attributeId: string,
		parameterName: string,
		parameterValue: any
	) {
		let parameters = this.vlpParamMap.get(entityClassId);

		if (!parameters) {
			parameters = new VlpParameters();
			this.vlpParamMap.set(entityClassId, parameters);
		}

		Logger.instance.debug(
			'Setting vlp parameter for entity %o, attribute %o: %o = %o',
			entityClassId,
			attributeId,
			parameterName,
			parameterValue
		);

		let valueChanged = parameters.setParameter(
			attributeId,
			parameterName,
			parameterValue
		);

		if (valueChanged) {
			this.eo.clearLovEntries(attributeId);
		}
	}
}
