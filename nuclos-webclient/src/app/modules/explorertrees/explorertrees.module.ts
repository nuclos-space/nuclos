import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../../shared/shared.module';
import { I18nModule } from '../i18n/i18n.module';
import { ExplorerTreesComponent } from './explorertrees.component';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		I18nModule,
		NgbModule,
	],
	exports: [
		ExplorerTreesComponent
	],
	declarations: [
		ExplorerTreesComponent
	]
})
export class ExplorerTreesModule {
}
