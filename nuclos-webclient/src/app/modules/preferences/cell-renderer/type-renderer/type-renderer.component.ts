import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { Preference, PreferenceType } from '../../preferences.model';

@Component({
	selector: 'nuc-type-renderer',
	templateUrl: 'type-renderer.component.html',
	styleUrls: ['type-renderer.component.css']
})
export class TypeRendererComponent implements AgRendererComponent {

	preferenceItem: Preference<any>;

	constructor() {
	}

	agInit(params: any) {
		this.preferenceItem = params.value;
	}

	getIconClass() {
		return PreferenceType.getIconClass(this.preferenceItem.type);
	}

	refresh(params: any): boolean {
		return false;
	}
}
