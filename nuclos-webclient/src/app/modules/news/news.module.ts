import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NewsMenuComponent } from './news-menu/news-menu.component';
import { NewsRouteModule } from './news-route/news-route.module';
import { NewsRoutes } from './news.routes';

@NgModule({
	imports: [
		CommonModule,
		NewsRouteModule,
		NewsRoutes
	],
	declarations: [
		NewsMenuComponent
	],
	exports: [
		NewsMenuComponent
	]
})
export class NewsModule {
}
