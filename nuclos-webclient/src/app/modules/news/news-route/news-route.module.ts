import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NewsRouteComponent } from './news-route.component';

@NgModule({
	imports: [
		CommonModule,
	],
	declarations: [
		NewsRouteComponent
	],
	exports: [
		NewsRouteComponent
	]
})
export class NewsRouteModule {
}
