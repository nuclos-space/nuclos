import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ISecurityService } from '@nuclos/nuclos-addon-api';
import { SecurityService } from './shared/security.service';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	providers: [
		{
			provide: ISecurityService,
			useClass: SecurityService
		}
	]
})
export class SecurityModule {
}
