import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { TaskService } from '../../../core/service/task.service';
import { StringUtils } from '../../../shared/string-utils';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { Logger } from '../../log/shared/logger';

@Component({
	selector: 'nuc-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, OnDestroy {
	@Input() meta: EntityMeta;
	@Input() boId: number;
	@Input() eo: EntityObject | undefined;
	@Input() canCreateBo: boolean;
	@Input() triggerUnsavedChangesPopover: Date;
	@Input() collectiveProcessing: boolean;

	@Output() onBackToCollectiveProcessingClick = new EventEmitter();

	private subscriptions: Subscription = new Subscription();

	constructor(
		private eoEventService: EntityObjectEventService,
		private eoService: EntityObjectService,
		private $log: Logger,
		private taskService: TaskService,
	) {
	}

	ngOnInit() {
		this.subscriptions.add(
			this.eoEventService.observeSelectedEo().subscribe(
				eo => this.setEo(eo as EntityObject)
			)
		);
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	private setEo(eo: EntityObject | undefined) {
		// TODO: There are more kinds of dynamic entities (dynamic BOs, virtual BOs), not only dynamic task lists - handle this more generic
		if (eo && eo.isDynamicTaskList()) {
			this.taskService.getTaskListDefinition(eo.getEntityClassId()).pipe(take(1)).subscribe( taskListMeta => {
				let realEntity = this.getRealEntity(taskListMeta, eo);

				if (!realEntity) {
					this.$log.warn('%o is a dynamic task list and the real entity could not be determined -> Could not load the real entity object!');
					return;
				}

				let realId = eo.getId();
				if (realEntity && realId) {
					this.$log.info(
						'Loading real EO %o:%o for dynamic EO %o:%o...',
						realEntity,
						realId,
						eo.getEntityClassId(),
						eo.getId()
					);

					this.eoService.loadEO(realEntity, realId).pipe(take(1)).subscribe(
						realEo => this.setEo(realEo)
					);
					return;
				}
			});
		}

		this.eo = eo;

		if (this.eo) {
			this.subscriptions.add(this.eo!.observeWarnChanges().subscribe(
				val => {
					if (val) {
						this.triggerUnsavedChangesPopover = new Date();
					}
				}
			));
		}
	}

	private getRealEntity(taskListMeta: EntityMeta, eo: EntityObject) {
		let dynamicEntityFieldName = taskListMeta.getDynamicEntityFieldName();
		let realEntity;

		if (dynamicEntityFieldName) {
			dynamicEntityFieldName = StringUtils.replaceAll(dynamicEntityFieldName, '_', '');
			realEntity = eo.getAttribute(dynamicEntityFieldName);
		}

		if (!realEntity) {
			realEntity = taskListMeta.getTaskEntity();
		}

		return realEntity;
	}
}
