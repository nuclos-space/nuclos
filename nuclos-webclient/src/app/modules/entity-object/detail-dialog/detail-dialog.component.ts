import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { from as observableFrom, Observable, of as observableOf } from 'rxjs';
import { take } from 'rxjs/operators';
import { StringUtils } from '../../../shared/string-utils';
import { ClickOutsideService } from '../../click-outside/click-outside.service';
import { EntityObjectEventListener } from '../../entity-object-data/shared/entity-object-event-listener';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { DialogService } from '../../popup/dialog/dialog.service';
// import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
// import { EntityObjectNavigationService } from '../../entity-object-data/shared/entity-object-navigation.service'

@Component({
	selector: 'nuc-detail-dialog',
	templateUrl: './detail-dialog.component.html',
	styleUrls: ['./detail-dialog.component.css']
})
export class DetailDialogComponent implements OnInit {

	@Input() sourceEo: EntityObject | undefined;
	@Input() title: string;
	@Input() width = 400;
	@Input() height = 200;
	errorMessage: string;
	disableControls = false;

	private _eo: EntityObject;


	private eoDefaultListener: EntityObjectEventListener = {
		isSaving: (entityObject: EntityObject, inProgress: boolean) => {
			this.disableControls = inProgress;
		},
		error: (entityObject: EntityObject, errorMessage: any) => {
			this.errorMessage = StringUtils.textToHtml(errorMessage);
			this._eo.removeListener(this.eoDuringSaveListener);
		}
	};

	// close only on 'OK', not if saved via custom-rule
	private eoDuringSaveListener: EntityObjectEventListener = {
		afterSave: (entityObject: EntityObject) => {
			this.reloadSourceObject().pipe(take(1)).subscribe(() => {
					this.activeModal.close(true);
					this.removeListeners();
				}
			);
		}
	};

	constructor(
		protected clickoutside: ClickOutsideService,
		// protected eoResultService: EntityObjectResultService,
		// protected eoNavigationService: EntityObjectNavigationService,
		private dialog: DialogService,
		protected activeModal: NgbActiveModal,
		protected ref: ElementRef
	) {}

	ngOnInit() {}

	@Input()
	set eo(eo: EntityObject) {
		this._eo = eo;
		this._eo.setSuppressDefaultErrorMessage(true);
		this._eo.addListener(this.eoDefaultListener);
		let error = eo.getError();
		if (error) {
			this.errorMessage = error;
		}
	}

	get eo(): EntityObject {
		return this._eo;
	}

	cancel() {
		this.activeModal.close(true);
		this.removeListeners();
	}

	ok() {
		this.disableControls = true;
		this._eo.addListener(this.eoDuringSaveListener);
		this._eo.save().subscribe();
	}

	removeListeners() {
		this._eo.removeListener(this.eoDefaultListener);
		this._eo.removeListener(this.eoDuringSaveListener);
	}

	private reloadSourceObject(): Observable<IEntityObject | undefined> {
		if (this.sourceEo) {
			return observableFrom(this.sourceEo.reload());
		}
		return observableOf(undefined);
	}
}
