import { Component, Input } from '@angular/core';

@Component({
	selector: 'nuc-detail-statusbar',
	templateUrl: './detail-statusbar.component.html',
	styleUrls: ['./detail-statusbar.component.scss']
})
export class DetailStatusbarComponent {
	@Input() rightSideString;
	@Input() rightSideTitleString;
}
