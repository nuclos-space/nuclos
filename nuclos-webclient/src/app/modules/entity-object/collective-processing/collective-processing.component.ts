import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { DialogService } from 'app/modules/popup/dialog/dialog.service';
import { BusyService } from 'app/shared/service/busy.service';
import { EMPTY, Subject } from 'rxjs';
import { catchError, take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { Logger } from '../../log/shared/logger';
import { EntityObjectGridMultiSelectionResult } from '../entity-object-grid/entity-object-grid-multi-selection-result';

@Component({
	selector: 'nuc-collective-processing',
	templateUrl: 'collective-processing.component.html',
	styleUrls: ['collective-processing.component.scss']
})
export class CollectiveProcessingComponent implements OnInit, OnDestroy {
	@Output() onMultiSelectionChange = new EventEmitter();
	@Output() statusLine = new EventEmitter();

	multiSelectionResult: EntityObjectGridMultiSelectionResult;

	selectionOptions: CollectiveProcessingSelectionOptions;
	isProgressing = false;
	noProgressingOptions = false;

	private unsubscribe$ = new Subject<void>();

	private abortUrl: string | undefined;


	constructor(
		protected i18n: NuclosI18nService,
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private $log: Logger,
		private dialogService: DialogService,
		private busyService: BusyService
	) { }

	ngOnDestroy() {
		if (this.isProgressing) {
			this.abortProgress();
		}
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.setMultiSelectionResult(this.eoResultService.getMultiSelectionResult());
		this.eoResultService.observeMultiSelectionChange().pipe(takeUntil(this.unsubscribe$)).subscribe(
			multiSelectionResult => {
			this.setMultiSelectionResult(multiSelectionResult);
		});
		this.statusLine.emit('');
	}

	executeAction(action: CollectiveProcessingAction) {
		if (action.description && action.withoutConfirmation !== undefined && !action.withoutConfirmation) {
			this.dialogService.confirm({ title: this.i18n.getI18n('webclient.dialog.statechange'),
				message: action.description}).then(_ => {
				this.triggerAction(action);
			});
		} else if (action.name === 'delete') {
			this.dialogService.confirm({ title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n('webclient.dialog.delete.multiple')}).then(_ => {
				this.triggerAction(action);
			});
		} else {
			this.triggerAction(action);
		}
	}

	showProgress() {
		return this.eoResultService.forceCollectiveProcessingView;
	}

	closeProgress() {
		this.eoResultService.forceCollectiveProcessingView = false;
		this.onMultiSelectionChange.emit(this.multiSelectionResult);
	}

	backToDetails() {
		this.eoResultService.showCollectiveProcessing = false;
	}

	cancelCollectiveProcessing() {
		this.multiSelectionResult.clear();
		this.multiSelectionResult.headerMultiSelectionAll = false;
		this.onMultiSelectionChange.emit(this.multiSelectionResult);
	}

	abortProgress() {
		this.eoResultUpdateService
			.abortCollectiveProcessing(this.abortUrl!)
			.pipe(
				take(1),
				catchError(e => {
					if (e.status === 404) {
						this.dialogService.alert({
							title: this.i18n.getI18n('webclient.error.404.title'),
							message: this.i18n.getI18n('webclient.error.code404')
						});
					} else {
						this.dialogService.alert({
							title: this.i18n.getI18n('webclient.error.unknown.title'),
							message: this.i18n.getI18n('webclient.error.unknown.text')
						});
					}

					return EMPTY;
				})
			)
			.subscribe(_ => {
				this.dialogService.alert({
					title: this.i18n.getI18n('webclient.collectiveProcessing.confirm.title'),
					message: this.i18n.getI18n('webclient.collectiveProcessing.confirm.message')
				});
				this.progress(100);
			});
	}

	progress(newProgress: number) {
		if (newProgress >= 100) {
			this.isProgressing = false;
		}
	}

	updateStatusLine(infoObject) {
		if (infoObject.total > 1) {
			if (infoObject.failure > 1) {
				this.statusLine.emit(this.i18n.getI18n('webclient.collectiveProcessing.status.line.manyAll',
					infoObject.total, infoObject.failure));
			} else if (infoObject.failure === 1) {
				this.statusLine.emit(
					this.i18n.getI18n('webclient.collectiveProcessing.status.line.manyDataOneFailure',
						infoObject.total)
				);
			} else {
				this.statusLine.emit(
					this.i18n.getI18n('webclient.collectiveProcessing.status.line.manyDataNoFailure',
						infoObject.total)
				);
			}
		} else {
			if (infoObject.failure === 1) {
				this.statusLine.emit(
					this.i18n.getI18n('webclient.collectiveProcessing.status.line.oneDataOneFailure')
				);
			} else {
				this.statusLine.emit(
					this.i18n.getI18n('webclient.collectiveProcessing.status.line.oneDataNoFailure')
				);
			}
		}
	}

	calculateTextColorFromBackgroundColor(color: string, isSubText: boolean) {
		if (color !== undefined && color.length === 7) {
			const red = parseInt(color.substr(1, 2), 16);
			const green = parseInt(color.substr(3, 2), 16);
			const blue = parseInt(color.substr(5, 2), 16);

			const brightness = (red * 2 + blue + green * 3) / 6;

			if (brightness > 160) {
				// dark color
				return '#000000';
			} else {
				// bright color
				return '#FFFFFF';
			}
		}
		if (isSubText) {
			return 'gray';
		}
		return '#3d93d9';
	}

	cleanStringForId(input: string) {
		return input.replace(/[^a-zA-Z0-9]+/g, '');
	}

	private triggerAction(action: CollectiveProcessingAction) {
		this.eoResultUpdateService
			.executeCollectiveProcessing(this.multiSelectionResult, action.links.execute.href!)
			.pipe(take(1))
			.subscribe((execution: CollectiveProcessingExecution) => {
				this.abortUrl = execution.links.abort.href!;
				this.isProgressing = true;
			});
	}

	private setMultiSelectionResult(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.multiSelectionResult = multiSelectionResult;
		if (multiSelectionResult.hasSelection()) {
			this.loadSelectionOptions(multiSelectionResult);
		}
	}

	private loadSelectionOptions(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.busyService.busy(
			this.eoResultUpdateService.loadCollectiveProcessingSelectionOptions(multiSelectionResult)
		)
		.pipe(take(1))
		.subscribe(selOptions => {
			this.selectionOptions = selOptions;
			this.noProgressingOptions = false;
			if (Object.keys(selOptions).length === 0 && multiSelectionResult.hasSelection()) {
				this.$log.warn('selOptions received: %o (MultiSelection: %o)', selOptions, multiSelectionResult);
				this.noProgressingOptions = true;
			}
		});
	}
}
