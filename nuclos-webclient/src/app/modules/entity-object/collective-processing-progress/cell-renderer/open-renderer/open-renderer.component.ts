import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';

@Component({
	selector: 'nuc-open-renderer',
	templateUrl: 'open-renderer.component.html',
	styleUrls: ['open-renderer.component.css']
})
export class OpenRendererComponent implements AgRendererComponent {

	constructor() {
	}

	agInit(params: any) {
	}

	refresh(params: any): boolean {
		return false;
	}
}
