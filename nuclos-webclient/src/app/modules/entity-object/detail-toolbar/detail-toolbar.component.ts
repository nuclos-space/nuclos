import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-toolbar',
	templateUrl: './detail-toolbar.component.html',
	styleUrls: ['./detail-toolbar.component.css']
})
export class DetailToolbarComponent implements OnInit {

	@Input() eo: EntityObject;
	@Input() canCreateBo: boolean;
	@Input() autoSelectAfterSaveEnabled = true;
	@Input() autoSelectAfterDeleteEnabled = true;
	@Input() useResultServiceForCancelation = true;
	@Input() showCancelForNewObject = true;
	@Input() triggerUnsavedChangesPopover: Date;
	@Input() collectiveProcessing: boolean;

	@Output() onBackToCollectiveProcessingClick = new EventEmitter();

	constructor() {
	}

	ngOnInit() {
	}

}
