import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-toolbar-state-item',
	templateUrl: './detail-toolbar-state-item.component.html',
	styleUrls: ['./detail-toolbar-state-item.component.css']
})
export class DetailToolbarStateItemComponent implements OnInit {
	@Input() eo: EntityObject;

	constructor() {
	}

	ngOnInit() {
	}

	isVisible(): boolean {
		if (!this.eo) {
			return false;
		}

		return !!(this.eo.getState() || this.eo.getNextStates().length > 0);
	}
}
