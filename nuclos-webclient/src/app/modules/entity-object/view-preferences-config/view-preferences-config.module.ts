import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { I18nModule } from '../../i18n/i18n.module';
import { TwoSideMultiSelectModule } from '../../two-side-multi-select/two-side-multi-select.module';
import {
	PreferenceDialogState,
	ViewPreferencesModalComponent,
	ViewPreferencesModalContainerComponent,
	ViewPreferencesModalDirective
} from './view-preferences-config.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		SharedModule,
		I18nModule,
		TwoSideMultiSelectModule
	],
	declarations: [
		ViewPreferencesModalDirective,
		ViewPreferencesModalContainerComponent,
		ViewPreferencesModalComponent,
	],
	providers: [
		PreferenceDialogState
	],
	exports: [
		ViewPreferencesModalContainerComponent,
		ViewPreferencesModalComponent
	],
	entryComponents: [
		ViewPreferencesModalComponent
	]
})
export class ViewPreferencesConfigModule {
}
