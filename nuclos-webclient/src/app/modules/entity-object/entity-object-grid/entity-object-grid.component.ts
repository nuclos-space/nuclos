import {
	Component,
	EventEmitter,
	HostListener,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	SimpleChanges
} from '@angular/core';
import { ColDef, GridOptions } from 'ag-grid';
import { EntityObjectResultService } from 'app/modules/entity-object-data/shared/entity-object-result.service';
import { ObjectUtils } from 'app/shared/object-utils';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosCellEditorParams, NuclosCellRendererParams } from '../../../layout/web-subform/web-subform.model';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { SystemParameter } from '../../../shared/system-parameters';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { SortModel } from '../../entity-object-data/shared/sort.model';
import {
	BooleanRendererComponent,
	DateRendererComponent,
	NumberRendererComponent,
	StateIconRendererComponent
} from '../../grid/grid/cell-renderer';
import { ImageRendererComponent } from '../../grid/grid/cell-renderer/image-renderer/image-renderer.component';
import { Logger } from '../../log/shared/logger';
import { SidebarViewItem } from '../sidebar/view/sidebar-view.model';
import { ReferenceRendererComponent } from './cell-renderer';
import { EntityObjectGridColumn } from './entity-object-grid-column';
import { EntityObjectGridMultiSelectionResult } from './entity-object-grid-multi-selection-result';
import { EntityObjectGridService } from './entity-object-grid.service';
import { MultiSelectionEditorComponent } from './multi-selection-editor/multi-selection-editor.component';
import { MultiSelectionHeaderComponent } from './multi-selection-header/multi-selection-header.component';
import { MultiSelectionRendererComponent } from './multi-selection-renderer/multi-selection-renderer.component';

/**
 * A Grid component for EntityObject data that handles column preferences and NuclosRowColor.
 *
 * TODO: Init columns
 * TODO: Init sorting
 * TODO: Handle NuclosRowColor
 */
@Component({
	selector: 'nuc-entity-object-grid',
	templateUrl: './entity-object-grid.component.html',
	styleUrls: ['./entity-object-grid.component.css']
})
export class EntityObjectGridComponent implements OnInit, OnDestroy, OnChanges {
	@Input() columns: EntityObjectGridColumn[];
	@Input() sortModel: SortModel;

	// TODO: Should not be used externally
	@Input() gridOptions: GridOptions = <GridOptions>{};
	@Input() allowMultiRowSelection = false;

	@Output() onMultiSelectionChange = new EventEmitter();
	@Output() rowSelected = new EventEmitter();
	@Output() gridReady = new EventEmitter();
	@Output() rowClicked = new EventEmitter();
	@Output() modelUpdated = new EventEmitter();

	private _showMultiSelection = false;
	private _multiSelectionDisabled = false;
	private _headerMultiSelectionChangeObserver = new Subject<boolean>();
	private _rowMultiSelectionChangeObserver = new Subject();
	private _multiSelectionResult = new EntityObjectGridMultiSelectionResult();

	private unsubscribe$ = new Subject<void>();
	private sortCalculatedAttributes: boolean;
	private selectionColumnShown = false;

	constructor(
		private eoGridService: EntityObjectGridService,
		private $log: Logger,
		private eoResultService: EntityObjectResultService,
		private nuclosConfig: NuclosConfigService) {

		this.nuclosConfig.getSystemParameters().subscribe(params => {
			this.sortCalculatedAttributes = params.is(SystemParameter.SORT_CALCULATED_ATTRIBUTES);
		});
	}

	ngOnInit() {
		this.initGrid();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.$log.debug('changes (entity-object-grid component) = %o', changes);

		if (changes['sortModel']) {
			if (!ObjectUtils.isEqual(changes['sortModel'].currentValue, changes['sortModel'].previousValue)) {
				this.applySortModel();
			}
		}

		if (changes['columns']) {
			if (!ObjectUtils.isEqual(changes['columns'].currentValue, changes['columns'].previousValue)) {
				this.applyColumns();
			}
		}
	}

	gridRowSelectionChanged(data) {
		const rows = data.api.getSelectedNodes();
		this.$log.debug('row selection changed, selected rows: %o', rows);
		if (rows.length > 1) {
			rows.forEach((node) => {
				node.setSelected(false);
				this.updateNodeSelection(node);
			});
		}
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	handleModelUpdated(value?: any) {
		this.modelUpdated.emit();
		this.resetMultiSelection();
	}

	@HostListener('document:keydown.Space', ['$event'])
	handleSpaceSelect(event) {
		if (this.gridOptions.api && this.selectionColumnShown) {
			this.gridOptions.api.getSelectedNodes().forEach(node => {
				this.updateNodeSelection(node);
			});
		}
	}

	private updateNodeSelection(node) {
		if (node.data.selected === undefined) {
			node.data.selected = false;
		}
		if (!node.data.selected) {
			this.$log.debug('selecting row: %o', node.data);
			node.data.selected = true;
			node.data.unselected = false;
		} else {
			this.$log.debug('deselecting row: %o', node.data);
			node.data.selected = false;
			node.data.unselected = true;
		}
		this.rowMultiSelectionChanged(node.data);
	}

	private applySortModel() {
		if (!this.sortModel) {
			return;
		}

		if (this.gridOptions.api) {
			if (!ObjectUtils.isEqual(this.gridOptions.api.getSortModel(), this.sortModel.getColumns())) {
				this.gridOptions.api.setSortModel(this.sortModel.getColumns());
				this.$log.debug('SortModel changed: %o', this.sortModel);
			}

		} else {
			this.gridReady.pipe(take(1)).subscribe(() => this.applySortModel());
		}
	}

	private applyColumns() {
		if (!this.gridOptions.api || !this.columns) {
			return;
		}

		this.columns.forEach(column => {
			let attributeMeta = column.attributeMeta;
			if (attributeMeta) {
				column.headerClass = this.eoGridService.getTextAlignmentClass(attributeMeta);
				column.cellClass = this.eoGridService.getTextAlignmentClass(attributeMeta);

				// suppress sorting for images (NULCOS-8092)
				column.suppressSorting = attributeMeta.isImage()
					|| attributeMeta.isStateIcon()
					|| (attributeMeta.isCalculated()
						&& !this.sortCalculatedAttributes);

				// provide data for render component
				column.cellRendererParams = {
					nuclosCellRenderParams: {
						editable: false,
						attrMeta: attributeMeta
					} as NuclosCellRendererParams
				};

				// provide data for editor component
				column.cellEditorParams = {
					attrMeta: attributeMeta
				} as NuclosCellEditorParams;

				if (attributeMeta.isBoolean()) {
					column.cellRendererFramework = BooleanRendererComponent;
				} else if (attributeMeta.isDate() || attributeMeta.isTimestamp()) {
					column.cellRendererFramework = DateRendererComponent;
				} else if (attributeMeta.isNumber()) {
					column.cellRendererFramework = NumberRendererComponent;
				} else if (attributeMeta.isStateIcon()) {
					column.cellRendererFramework = StateIconRendererComponent;
				} else if (attributeMeta.isReference()) {
					column.cellRendererFramework = ReferenceRendererComponent;
				} else if (attributeMeta.isImage()) {
					column.cellRendererFramework = ImageRendererComponent;
				}
			}
		});

		let gridColumns: ColDef[] = this.columns;
		if (this.showMultiSelection === true) {
			this.selectionColumnShown = true;
			gridColumns.unshift(this.createMultiSelectionColumn());
		}
		this.gridOptions.api.setColumnDefs(gridColumns);

		// we dont want to change the sort model if we enable multiselection
		if (this.showMultiSelection !== true) {
			this.selectionColumnShown = false;
			this.applySortModel();
		}
	}

	private createMultiSelectionColumn(): ColDef {
		let result = {
			width: 20,
			pinned: true,
			lockPosition: true,
			lockPinned: true,
			suppressFilter: true,
			suppressMovable: true,
			suppressNavigable: true,
			suppressResize: true,
			suppressSorting: true,
			field: 'selected',
			headerName: '',
			headerComponentFramework: MultiSelectionHeaderComponent,
			editable: true,
			cellRendererFramework: MultiSelectionRendererComponent,
			cellEditorFramework: MultiSelectionEditorComponent
		} as ColDef;
		return result;
	}

	private initGrid() {
		this._headerMultiSelectionChangeObserver.pipe(takeUntil(this.unsubscribe$)).subscribe(value => {
			if (!this.checkForDirty()) {
				this.setHeaderMultiSelectionAll(value);
			} else {
				this.resetMultiSelection();
			}
		});
		this._rowMultiSelectionChangeObserver.pipe(takeUntil(this.unsubscribe$)).subscribe((rowNode: any) => {
			if (!this.checkForDirty()) {
				this.rowMultiSelectionChanged(rowNode.data);
			} else {
				this.resetMultiSelection();
			}
		});
		this.gridOptions.context = {
			headerMultiSelectionChangeObserver: this._headerMultiSelectionChangeObserver,
			rowMultiSelectionChangeObserver: this._rowMultiSelectionChangeObserver,
			multiSelectionDisabled: this.multiSelectionDisabled
		};

		this.gridOptions.enableColResize = true;
		this.gridOptions.enableSorting = true;
		this.gridOptions.cacheBlockSize = 100;

		this.gridOptions.columnDefs = [{}];

		this.gridOptions.getRowNodeId = item => {
			return item.getId();
		};

		this.gridOptions.rowSelection = this.allowMultiRowSelection ? 'multiple' : 'single';

		// NuclosRowColor
		this.gridOptions.getRowClass = (params: { data: SidebarViewItem }) => {
			if (params.data) {
				return params.data.rowclass || '';
			}
			return '';
		};
		this.gridOptions.getRowStyle = (params: { data: SidebarViewItem }) => {
			if (params.data) {
				let rowColor = params.data.getRowColor();
				let textColor = params.data.getTextColor();
				let result = {};
				if (rowColor !== undefined) {
					result['background-color'] = rowColor;
				}
				if (textColor !== undefined) {
					result['color'] = textColor;
				}
				return result;
			}
			return undefined;
		};

		this.eoResultService.observeMultiSelectionChange().pipe(takeUntil(this.unsubscribe$)).subscribe(multiSelectionResult => {
			// update selection
			if (!this.checkForDirty()) {
				this.updateMultiSelection(multiSelectionResult);
			} else {
				this.resetMultiSelection();
			}
		});

		this.gridOptions.onGridReady = () => {
			this.applyColumns();
		};
	}

	get multiSelectionDisabled(): boolean {
		return this._multiSelectionDisabled;
	}

	@Input()
	set multiSelectionDisabled(multiSelectionDisabled: boolean) {
		this._multiSelectionDisabled = multiSelectionDisabled;
		if (this.gridOptions && this.gridOptions.context) {
			this.gridOptions.context.multiSelectionDisabled = multiSelectionDisabled;
		}
	}

	get showMultiSelection(): boolean {
		return this._showMultiSelection;
	}

	@Input()
	set showMultiSelection(showMultiselection: boolean) {
		if (this._showMultiSelection !== showMultiselection) {
			if (this._showMultiSelection) {
				this.columns.splice(0, 1);
			}
			this._showMultiSelection = showMultiselection;
			this.applyColumns();
			// only reset if we set showMultiSelection to false
			if (!this._showMultiSelection) {
				this.eoResultService.deleteMultiSelectionResultFromStroage();
				this.resetMultiSelection();
			}
		}
	}

	private setHeaderMultiSelectionAll(headerMultiSelectionAll: boolean) {
		this._multiSelectionResult.headerMultiSelectionAll = headerMultiSelectionAll;
		this.resetMultiSelectionIds();
		this.onMultiSelectionChange.emit(this._multiSelectionResult);
	}

	private getDirtyEOFromNodes(): EntityObject | undefined {
		let dirtyEO = undefined;
		this.gridOptions.api!.forEachNode(function (rowNode) {
			let rowNodeEO = rowNode.data.getEO();
			if (rowNodeEO.isDirty()) {
				dirtyEO = rowNodeEO;
			}
		});
		return dirtyEO;
	}

	private checkForDirty(): boolean {
		let selectedEo = this.getDirtyEOFromNodes();
		if (selectedEo !== undefined && selectedEo.isDirty()) {
			selectedEo.triggerWarnChange();
			return true;
		}
		return false;
	}

	private rowMultiSelectionChanged(rowNode: any) {
		let eoId = rowNode.getId();
		if (eoId) {
			if (this._multiSelectionResult.headerMultiSelectionAll) {
				if (rowNode.unselected !== true) {
					this._multiSelectionResult.unselectedIds.delete(eoId);
				} else if (rowNode.unselected === true) {
					this._multiSelectionResult.unselectedIds.add(eoId);
				}
			} else {
				if (rowNode.selected !== true) {
					this._multiSelectionResult.selectedIds.delete(eoId);
				} else if (rowNode.selected === true) {
					this._multiSelectionResult.selectedIds.add(eoId);
				}
			}
			this.onMultiSelectionChange.emit(this._multiSelectionResult);
		}
	}

	private updateMultiSelection(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this._multiSelectionResult = multiSelectionResult;
		this.gridOptions.context.headerMultiSelectionAll = multiSelectionResult.headerMultiSelectionAll;
		if (this.gridOptions.api) {
			this.gridOptions.api.forEachNode(function (rowNode) {
				let eoId = rowNode.data.getId();
				rowNode.data.selected = multiSelectionResult.selectedIds.has(eoId);
				rowNode.data.unselected = multiSelectionResult.unselectedIds.has(eoId);
			});

			if (this.gridOptions.api.getRenderedNodes().length > 0) {
				this.$log.debug('renderedNodes: %o', this.gridOptions.api.getRenderedNodes());
				let foundNodes = this.gridOptions.api.getRenderedNodes()
					.filter(node => node.data.selected || node.data.unselected)
					.map(node => node.id);

				this.$log.debug('foundNodes: %o', foundNodes);
				if (foundNodes.length === 0 && !multiSelectionResult.headerMultiSelectionAll) {
					this.eoResultService.deleteMultiSelectionResultFromStroage();
					this.resetMultiSelection();
					return;
				}
			}

			this.gridOptions.api.refreshCells();

			if (!multiSelectionResult.hasSelection()) {
				this.gridOptions.api.deselectAll();
			}
		}
	}

	private resetMultiSelectionIds() {
		if (this._multiSelectionResult.hasIds()) {
			this._multiSelectionResult.clear();
		}
		if (this.gridOptions.api) {
			this.gridOptions.api.forEachNode(function (rowNode) {
				if (rowNode.data) {
					rowNode.data.selected = undefined;
					rowNode.data.unselected = undefined;
				}
			});
			this.gridOptions.api.refreshCells();
		}
	}

	private resetMultiSelection() {
		let storageMultiSelection = this.eoResultService.retrieveMultiSelectionResultFromStorage();
		if (storageMultiSelection.hasSelection()) {
			this.updateMultiSelection(storageMultiSelection);
			return;
		}
		let selectionChanged = this._multiSelectionResult.hasSelection();
		this._multiSelectionResult.headerMultiSelectionAll = false;
		this.gridOptions.context.headerMultiSelectionAll = this._multiSelectionResult.headerMultiSelectionAll;
		this.resetMultiSelectionIds();
		if (selectionChanged) {
			// ExpressionChangedAfterItHasBeenCheckedErrors will occur without timeout
			setTimeout(() => this.onMultiSelectionChange.emit(this._multiSelectionResult));
		}
	}
}
