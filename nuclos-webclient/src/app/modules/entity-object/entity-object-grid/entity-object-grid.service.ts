import { Injectable } from '@angular/core';
import { ValueGetterParams } from 'ag-grid/dist/lib/entities/colDef';
import { EntityAttrMeta } from '../../entity-object-data/shared/bo-view.model';
import { GridService } from '../../grid/grid.service';
import { Logger } from '../../log/shared/logger';
import { EntityObjectGridColumn } from './entity-object-grid-column';

@Injectable()
export class EntityObjectGridService {

	constructor(
		private $log: Logger,
		private gridService: GridService,
	) {
	}

	createColumnDefinition(attribute: EntityAttrMeta) {
		let fieldName = attribute.getAttributeID();

		let colDef: EntityObjectGridColumn = {
			headerName: attribute.getName(),
			field: fieldName,
			colId: attribute.getAttributeID(),
			// width: column.width,
			editable: false,
			valueGetter: this.valueGetter,

			cellEditorParams: {},
			// sort: column.sort && column.sort.direction,
			icons: {},

			attributeMeta: attribute
		};

		return colDef;
	}

	getTextAlignmentClass(attributeMeta: EntityAttrMeta) {
		let result = 'text-left';
		if (attributeMeta.isBoolean() || attributeMeta.isDate() || attributeMeta.isTimestamp() || attributeMeta.isStateIcon()) {
			result = 'text-center';
		} else if (attributeMeta.isNumber()) {
			result = 'text-right';
		}
		return result;
	}

	/**
	 * Returns values from the underlying Sub-EO via its attribute Getter method.
	 */
	valueGetter(params: ValueGetterParams): any {
		if (!params.colDef.field) {
			this.$log.warn('Col has no field: %o', params);
			return undefined;
		}

		return params.node.data.getAttribute(params.colDef.field);
	}

}
