import { take } from 'rxjs/operators';
import { FqnService } from '../../../../shared/service/fqn.service';
import { NuclosConfigService } from '../../../../shared/service/nuclos-config.service';
import { EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../../entity-object-data/shared/data.service';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';

/**
 * prepare view only if needed for performance reasons
 */
export class SidebarViewItem {
	id;
	nuclosStateIconUrl: string | undefined;
	entityObject: EntityObject;
	nuclosConfigService: NuclosConfigService;
	rowcolor: string | undefined;
	rowclass: string | undefined;
	textcolor: string | undefined;

	constructor(entityObject: EntityObject, nuclosConfigService: NuclosConfigService) {
		this.entityObject = entityObject;
		this.nuclosConfigService = nuclosConfigService;
		this.id = this.getId();
	}

	build(): SidebarViewItem {
		this.buildViewAttributes(this.entityObject);
		return this;
	}

	getEO() {
		return this.entityObject;
	}

	public getId() {
		return this.entityObject.getId();
	}

	public getRowColor() {
		return this.rowcolor;
	}

	public getTextColor() {
		return this.textcolor;
	}

	protected buildViewAttributes(entityObject: EntityObject) {
		for (let attrKey of Object.keys(entityObject.getAttributes())) {
			let value = entityObject.getAttribute(attrKey);
			if (
				attrKey === 'nuclosState' &&
				entityObject.getState() &&
				entityObject.getState()!.nuclosStateId
			) {
				this.nuclosStateIconUrl =
					this.nuclosConfigService.getRestHost() +
					'/resources/stateIcons/' +
					entityObject.getState()!.nuclosStateId;
			}
			if (value && value.name !== undefined) {
				value = value.name;
			}
			let attrFqnKey = entityObject.getEntityClassId() + '_' + attrKey;
			this[attrFqnKey] = value;

			this.rowcolor = entityObject.getRowColor();
			this.textcolor = entityObject.getTextColor();

			if (entityObject.isDirty()) {
				this.rowclass = 'dirty-eo';
			}
			if (entityObject.isNew()) {
				this.rowclass = 'new-eo';
			}
		}
	}
}

export class SidebarCardLayoutViewItem extends SidebarViewItem {
	title: string;
	info: string;

	constructor(
		entityObject: EntityObject,
		nuclosConfigService: NuclosConfigService,
		private fqnService: FqnService,
		private dataService: DataService
	) {
		super(entityObject, nuclosConfigService);
	}

	protected buildViewAttributes(eo: EntityObject) {
		this.entityObject.getMeta().pipe(take(1)).subscribe(meta => {
			if (meta) {
				this.title = this.formatTitleOrInfo(meta.getTitlePattern(), eo, meta);
				this.info = this.formatTitleOrInfo(meta.getInfoPattern(), eo, meta);
			}
		});
	}

	private formatTitleOrInfo(
		inputString: string | undefined,
		eo: EntityObject,
		meta: EntityMeta
	): string {
		if (inputString === undefined) {
			return '';
		}

		return this.fqnService.formatFqnString(inputString, fqn => {
			let attribute = eo.getAttribute(fqn);
			let attributeMeta = meta.getAttributeMetaByFqn(fqn);
			if (attribute && attributeMeta) {
				return this.dataService.formatAttribute(attribute, attributeMeta);
			}
			return '';
		});
	}
}
