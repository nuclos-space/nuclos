import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { EntityAttrMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-image-renderer',
	templateUrl: './image-renderer.component.html',
	styleUrls: ['./image-renderer.component.css']
})
export class ImageRendererComponent implements AgRendererComponent {

	imageHref: string;

	agInit(params: any) {
		if (params.data) {
			const entityObject = (<EntityObject>params.data.entityObject);
			const attributeId = (<EntityAttrMeta>params.colDef.attributeMeta).getAttributeID();
			if (attributeId !== undefined && entityObject !== undefined) {
				this.imageHref = entityObject.getImageUrl(attributeId);
			}
		}
	}


	refresh(params: any): boolean {
		return false;
	}
}
