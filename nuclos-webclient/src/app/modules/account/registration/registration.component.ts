import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { HasMessage } from '../../../shared/has-message';
import { BusyService } from '../../../shared/service/busy.service';
import { NewsService } from '../../news/shared/news.service';
import { AccountData } from '../shared/account-data';
import { NuclosAccountService } from '../shared/nuclos-account.service';

@Component({
	selector: 'nuc-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.css']
})
export class RegistrationComponent extends HasMessage implements OnInit, OnDestroy {

	account: AccountData;

	showForm = true;

	private privacyPolicy: News | undefined;
	private unsubscribe$ = new Subject<void>();

	constructor(
		private accountService: NuclosAccountService,
		private newsService: NewsService,
		private busyService: BusyService,
		private location: Location,
		i18n: NuclosI18nService
	) {
		super(i18n);

		this.account = {};
	}

	ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.newsService.getPrivacyPolicy()
		.pipe(takeUntil(this.unsubscribe$))
		.subscribe(
			privacyPolicy => this.privacyPolicy = privacyPolicy
		);
	}

	onSubmit() {
		if (this.showForm && this.hasDisclaimer() && !this.account.privacyconsent) {
			this.setMessage(
				'danger',
				'webclient.account.privacyconsent2',
				'webclient.account.noprivacyconsent'
			);
			return;
		}

		this.busyService.busy(
			this.accountService.create(this.account)
		).pipe(takeUntil(this.unsubscribe$)).subscribe(
				() => {
					this.showSuccessMessage();
					this.showForm = false;
				},
				error => this.showErrorFromResponse('webclient.account.error', error)
			);
	}

	cancel() {
		this.showForm = false;
		this.location.back();
	}

	hasDisclaimer() {
		return this.privacyPolicy;
	}

	showDisclaimer() {
		if (this.privacyPolicy) {
			this.newsService.showNews(this.privacyPolicy);
		}
	}

	private showSuccessMessage() {
		this.setMessage(
			'success',
			'webclient.account.successful.registered',
			'webclient.account.you.get.an.email.to.activate.your.account'
		);
	}
}
