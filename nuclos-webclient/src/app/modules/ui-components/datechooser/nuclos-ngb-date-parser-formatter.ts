import { Injectable } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { DatetimeService } from '../../../shared/service/datetime.service';
import { Logger } from '../../log/shared/logger';

/**
 * custom datepicker parser / formatter for handling locale dependent format
 */
@Injectable()
export class NuclosNgbDateParserFormatter extends NgbDateParserFormatter {
	constructor(
		private datetimeService: DatetimeService,
		private $log: Logger,
		private i18n: NuclosI18nService
	) {
		super();
	}

	format(date: NgbDateStruct): string {
		if (date === null) {
			return '';
		}
		try {
			let dateMoment = moment(date.year + '-' + date.month + '-' + date.day, 'Y-M-D');
			return dateMoment.format(this.datetimeService.getDatePattern());
		} catch (e) {
			return '';
		}
	}

	parse(value: string): NgbDateStruct {
		let returnVal: any = null;
		if (value) {
			try {
				let patterns = [this.i18n.getCurrentLocale().datePattern, 'YYYY-MM-DD'];
				returnVal = this.datetimeService.buildNgbDateStruct(value, patterns);
			} catch (e) {
				this.$log.warn('Unable to parse date.', value);
			}
		}
		return returnVal;
	}

}
