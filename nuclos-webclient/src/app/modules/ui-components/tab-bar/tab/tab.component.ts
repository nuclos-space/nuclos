import { Component, ContentChild, Input, OnInit } from '@angular/core';
import { TabContentDirective } from '../tab-content.directive';
import { TabTitleDirective } from '../tab-title.directive';

@Component({
	selector: 'nuc-tab',
	templateUrl: './tab.component.html',
	styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {

	@Input() id: any;
	@Input() value: any;
	@Input() index: any;
	@Input() title: string;

	@ContentChild(TabTitleDirective) titleTemplate: TabTitleDirective;
	@ContentChild(TabContentDirective) contentTemplate: TabContentDirective;

	constructor() {
	}

	ngOnInit() {
	}

}
