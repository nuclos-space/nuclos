import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Logger } from '@nuclos/nuclos-addon-api';
import { EMPTY, from as observableFrom, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ModalService {

	constructor(
		private modalService: NgbModal,
		private $log: Logger,
	) {
	}

	open(templateRefOrType: any, context?: any, beforeDismiss?: () => boolean): Observable<any> {
		let ngbModalRef = this.modalService.open(
			templateRefOrType,
			{
				size: 'lg',
				windowClass: 'fullsize-modal-window',
				beforeDismiss: beforeDismiss
			}
		);

		for (let x in context) {
			if (context.hasOwnProperty(x)) {
				ngbModalRef.componentInstance[x] = context[x];
			}
		}

		return observableFrom(ngbModalRef.result).pipe(
			catchError(e => {
				this.$log.debug('Caught error from modal: %o', e);
				return EMPTY;
			})
		);
	}
}
