import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'nuc-modal',
	templateUrl: './modal.component.html',
	styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

	@Input() title: string;
	@Input() headerTemplate: TemplateRef<any>;
	@Input() footerTemplate: TemplateRef<any>;

	constructor(
		private activeModal: NgbActiveModal
	) {
	}

	ngOnInit() {
	}

	close(result) {
		this.activeModal.close(result);
	}
}
