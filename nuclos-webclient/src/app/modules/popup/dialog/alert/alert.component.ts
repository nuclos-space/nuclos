import { Component } from '@angular/core';
import { DialogOptions } from '../dialog.model';
import { DialogService, DialogState } from '../dialog.service';

@Component({
	selector: 'nuc-alert-modal-component',
	templateUrl: './alert.component.html',
	styleUrls: ['./alert.component.css']
})
export class AlertComponent {

	options: DialogOptions;

	constructor(
		private dialogService: DialogService,
		private state: DialogState) {
		this.options = state.options;
	}

	close() {
		if (this.options.dialogId) {
			this.dialogService.closeDialog(this.options.dialogId);
		}
	}

}
