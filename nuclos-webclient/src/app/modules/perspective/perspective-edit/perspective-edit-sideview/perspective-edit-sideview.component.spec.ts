import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerspectiveEditSideviewComponent } from './perspective-edit-sideview.component';

xdescribe('PerspectiveEditSideviewComponent', () => {
	let component: PerspectiveEditSideviewComponent;
	let fixture: ComponentFixture<PerspectiveEditSideviewComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PerspectiveEditSideviewComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PerspectiveEditSideviewComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
