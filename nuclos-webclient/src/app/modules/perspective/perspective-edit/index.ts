export * from './perspective-edit-detailview/perspective-edit-detailview.component';
export * from './perspective-edit-layout/perspective-edit-layout.component';
export * from './perspective-edit-searchtemplate/perspective-edit-searchtemplate.component';
export * from './perspective-edit-sideview/perspective-edit-sideview.component';
export * from './perspective-edit-subforms/perspective-edit-subforms.component';
