import { EventEmitter, Input, Output } from '@angular/core';

export class AbstractSearchItem {
	@Input() model: any;
	@Input() disabled;

	@Output() modelChanged = new EventEmitter();
}
