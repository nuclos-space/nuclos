/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InputRequiredDialogComponent } from './input-required-dialog.component';

xdescribe('InputRequiredDialogComponent', () => {
	let component: InputRequiredDialogComponent;
	let fixture: ComponentFixture<InputRequiredDialogComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [InputRequiredDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(InputRequiredDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
