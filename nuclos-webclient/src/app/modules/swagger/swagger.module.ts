import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SwaggerUiComponent } from './swagger-ui/swagger-ui.component';
import { SwaggerRoutesModule } from './swagger.routes';

@NgModule({
	declarations: [SwaggerUiComponent],
	imports: [
		CommonModule,

		SwaggerRoutesModule,
	]
})
export class SwaggerModule {
}
