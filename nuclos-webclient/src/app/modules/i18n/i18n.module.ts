import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { LogModule } from '../log/log.module';
import { I18nComponent } from './i18n.component';
import { LocaleComponent } from './locale/locale.component';
import { SetLocaleComponent } from './set-locale/set-locale.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		LogModule
	],
	declarations: [
		I18nComponent,
		LocaleComponent,
		SetLocaleComponent
	],
	exports: [
		LocaleComponent,
		SetLocaleComponent
	],
	providers: [
		NuclosI18nService
	]
})
export class I18nModule {
}
