import { Component, OnInit } from '@angular/core';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';

@Component({
	selector: 'nuc-locale',
	templateUrl: './locale.component.html',
	styleUrls: ['./locale.component.css']
})
export class LocaleComponent implements OnInit {
	locales;

	constructor(
		private nuclosI18n: NuclosI18nService,
		private $log: Logger
	) {
	}

	ngOnInit() {
		this.locales = this.nuclosI18n.getLocales();
	}

	getSelectedLocale() {
		return this.nuclosI18n.getCurrentLocale().key;
	}

	setSelectedLocale(locale: string) {
		this.$log.debug('set locale: %o', locale);
		this.nuclosI18n.setLocaleString(locale);
	}
}
