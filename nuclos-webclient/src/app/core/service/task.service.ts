import { Injectable } from '@angular/core';
import { EMPTY, Observable, of as observableOf } from 'rxjs';

import { catchError, map, mergeMap } from 'rxjs/operators';
import { TaskList } from '../../data/schema/task-list';
import { EntityMeta, EntityMetaData } from '../../modules/entity-object-data/shared/bo-view.model';
import { DataService } from '../../modules/entity-object-data/shared/data.service';
import { EntityObjectService } from '../../modules/entity-object-data/shared/entity-object.service';
import { MetaService } from '../../modules/entity-object-data/shared/meta.service';
import { ResultParams } from '../../modules/entity-object-data/shared/result-params';
import { Logger } from '../../modules/log/shared/logger';
import { AttributeSelectionContent, Preference } from '../../modules/preferences/preferences.model';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { NuclosHttpService } from '../../shared/service/nuclos-http.service';

@Injectable({
	providedIn: 'root'
})
export class TaskService {

	constructor(
		private config: NuclosConfigService,
		private httpService: NuclosHttpService,
		private $log: Logger,
		private metaService: MetaService,
		// TODO: Refactor to some EO-data service
		private dataService: DataService,
		// TODO: Only injected here to make sure it is instantiated
		private eoService: EntityObjectService
	) {
	}

	/**
	 * Does not return complete entity meta data!
	 */
	getTaskListDefinitions(): Observable<EntityMetaData[]> {
		return this.httpService.getCachedJSON(this.config.getRestHost() + '/meta/tasklists');
	}

	getTaskListDefinitionByTaskMetaId(taskMetaId: string): Observable<EntityMeta> {
		return this.getTaskListDefinitions().pipe(map(
			metas => {
				let taskListMeta = metas.find(
					meta => meta.taskMetaId === taskMetaId
				);

				if (!taskListMeta) {
					throw new Error('Could not find task list meta for taskMetaId=' + taskMetaId);
				}

				return new EntityMeta(taskListMeta);
			}
		));
	}

	getTaskListDefinition(taskListFqn: string): Observable<EntityMeta> {
		return this.getTaskListDefinitions().pipe(map(
			metas => {
				let taskListMeta = metas.find(
					meta => meta.boMetaId === taskListFqn
				);

				if (!taskListMeta) {
					throw new Error('Could not find task list meta for ' + taskListFqn);
				}

				return new EntityMeta(taskListMeta);
			}
		));
	}

	getTaskData(
		taskList: TaskList,
		attributeSelection?: Preference<AttributeSelectionContent>,
	) {
		return this.getTaskListMeta(taskList).pipe(mergeMap(
			meta => {
				let params: ResultParams = {
					offset: 0,
					chunkSize: 10,
					countTotal: false
				};

				// Only search filter based task lists have a search filter
				if (taskList.searchfilter) {
					params.searchFilterId = taskList.searchfilter;
				}

				return this.dataService.loadList(
					meta,
					undefined,
					undefined,
					attributeSelection,
					undefined,
					params,
				);
			}
		));
	}

	/**
	 * Returns a complete entity meta for the task list entity.
	 */
	getTaskListMeta(taskList: TaskList) {
		return this.getTaskListMetaByFqn(taskList.entityClassId);
	}

	getTaskListMetaByFqn(taskListFqn: string) {
		return this.metaService.getBoMeta(taskListFqn);
	}

	/**
	 * TODO: Should be refactored later to some kind of push notification from the server (Websocket).
	 */
	getTaskCountSince(taskList: TaskList, since: Date) {
		return this.getTaskListMeta(taskList).pipe(
			mergeMap(meta => {
				// TODO: This method only works for search filter based task lists at the moment
				if (!taskList.searchfilter) {
					return observableOf(0);
				}

				let params: ResultParams = {
					offset: 0,
					chunkSize: 0,
					countTotal: true,
					searchFilterId: taskList.searchfilter
				};

				return this.metaService.getEntityMeta(meta.getEntityClassId()).pipe(
					mergeMap(entityMeta =>
						this.dataService.loadEoData(
							entityMeta,
							params,
							{
								where: {
									aliases: {},
									clause: entityMeta.getAttribute('changedAt').boAttrId + ' > \'' + since.toISOString() + '\''
								}
							}
						).pipe(
							map(
								data => data.total
							),
							catchError(e => {
								this.$log.warn('Failed to get count for task list: %o', e);
								return EMPTY;
							}),
						)
					)
				);
			})
		);
	}
}
