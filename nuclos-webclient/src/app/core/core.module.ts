import { ErrorHandler, NgModule, Optional, SkipSelf } from '@angular/core';

import { throwIfAlreadyLoaded } from './guard/module-import.guard';
import { GlobalErrorHandlerService } from './service/global-error-handler.service';

@NgModule({
	imports: [],
	providers: [
		{
			provide: ErrorHandler,
			useClass: GlobalErrorHandlerService
		}
	]
})
export class CoreModule {
	constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
		throwIfAlreadyLoaded(parentModule, 'CoreModule');
	}
}
