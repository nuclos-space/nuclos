import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { GridsterModule } from 'angular-gridster2';
import { ResizableModule } from 'angular-resizable-element';

import { GridModule } from 'app/modules/grid/grid.module';
import { AceEditorModule } from 'ng2-ace-editor';
import { DndModule } from 'ng2-dnd';
import { CookieDisclaimerComponent } from './components/cookie-disclaimer/cookie-disclaimer.component';
import { CurrentMandatorComponent } from './components/current-mandator/current-mandator.component';
import { ResizehandledividerComponent } from './components/resizehandledivider/resizehandledivider.component';
import { I18nPipe } from './pipes/i18n.pipe';
import { ListFilterPipe } from './pipes/list-filter.pipe';
import { MapEntriesPipe } from './pipes/map-entries.pipe';

@NgModule({
	declarations: [
		ResizehandledividerComponent,
		CurrentMandatorComponent,
		CookieDisclaimerComponent,
		ListFilterPipe,
		I18nPipe,
		MapEntriesPipe
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		GridsterModule,
		GridModule,
		AceEditorModule,
		DndModule.forRoot(),
		ResizableModule
	],
	exports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		GridsterModule,
		AceEditorModule,
		DndModule,
		GridModule,
		ResizableModule,
		ResizehandledividerComponent,
		CurrentMandatorComponent,
		CookieDisclaimerComponent,
		ListFilterPipe,
		I18nPipe,
		MapEntriesPipe
	]
})
export class SharedModule { }
