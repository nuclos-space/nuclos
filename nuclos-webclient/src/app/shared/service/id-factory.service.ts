import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class IdFactoryService {

	private nextId = 1;

	constructor() {
	}

	getNextId() {
		return this.nextId++;
	}
}
