import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { SetLocaleComponent } from './modules/i18n/set-locale/set-locale.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'auth',
		loadChildren: './modules/authentication/authentication.module#AuthenticationModule'
	},
	{
		path: 'account',
		loadChildren: './modules/account/account.module#AccountModule'
	},
	{
		path: 'maintenance',
		loadChildren: './modules/admin/maintenance/maintenance.module#MaintenanceModule'
	},
	{
		path: 'businesstests',
		loadChildren: './modules/businesstest/businesstest.module#BusinesstestModule'
	},
	{
		path: 'cache',
		loadChildren: './modules/cache/cache.module#CacheModule'
	},
	{
		path: 'dashboard',
		loadChildren: './modules/dashboard/dashboard.module#DashboardModule'
	},
	{
		path: '',
		loadChildren: './modules/entity-object/entity-object.module#EntityObjectModule'
	},
	{
		path: 'error',
		loadChildren: './modules/error/error.module#ErrorModule'
	},
	{
		path: 'news',
		loadChildren: './modules/news/news.module#NewsModule'
	},
	{
		path: 'preferences',
		loadChildren: './modules/preferences/preferences.module#PreferencesModule'
	},
	{
		path: 'serverinfo',
		loadChildren: './modules/server-info/server-info.module#ServerInfoModule'
	},
	{
		path: 'swagger-ui',
		loadChildren: './modules/swagger/swagger.module#SwaggerModule'
	},
	{
		path: 'login',
		pathMatch: 'full',
		redirectTo: '/auth/login'
	},
	{
		path: 'logout',
		pathMatch: 'full',
		redirectTo: '/auth/logout'
	},
	{
		path: 'session-info',
		pathMatch: 'full',
		redirectTo: '/auth/session-info'
	},
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'index.html',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'locale/:locale',
		component: SetLocaleComponent
	},
	{
		path: '**',
		redirectTo: 'error/404'
	}
];

export const AppRoutesModule = RouterModule.forRoot(ROUTE_CONFIG, {
	useHash: true,
	preloadingStrategy: PreloadAllModules
});
