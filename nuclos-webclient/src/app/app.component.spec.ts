/* tslint:disable:no-unused-variable */

import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { MenuModule } from './modules/menu/menu.module';
import { DialogModule } from './modules/popup/dialog/dialog.module';

xdescribe('App: NuclosWebclient', () => {

	beforeEach(() => {
		TestBed.configureTestingModule({
			declarations: [
				AppComponent
			],
			imports: [RouterTestingModule, DialogModule, MenuModule]
		});
	});

	it('should create the app', async(() => {
		let fixture = TestBed.createComponent(AppComponent);
		let app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));
});
