export interface ValidationError {
	entity: string;
	field: string;
	errortype: 'MANDATORY_FIELD_ERROR' |
		'FIELD_FORMAT_ERROR' |
		'FIELD_DIMENSION_ERROR' |
		'FIELD_RANGE_ERROR' |
		'FIELD_TYPE_ERROR' |
		'NUCLET_VALIDATION_ERROR';
}
