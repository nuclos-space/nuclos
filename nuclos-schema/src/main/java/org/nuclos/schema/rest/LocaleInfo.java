
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for locale-info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="locale-info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="locale" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "locale-info")
public class LocaleInfo implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "locale", required = true)
    protected String locale;

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocale(String value) {
        this.locale = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theLocale;
            theLocale = this.getLocale();
            strategy.appendField(locator, this, "locale", buffer, theLocale);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LocaleInfo)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LocaleInfo that = ((LocaleInfo) object);
        {
            String lhsLocale;
            lhsLocale = this.getLocale();
            String rhsLocale;
            rhsLocale = that.getLocale();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locale", lhsLocale), LocatorUtils.property(thatLocator, "locale", rhsLocale), lhsLocale, rhsLocale)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theLocale;
            theLocale = this.getLocale();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locale", theLocale), currentHashCode, theLocale);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LocaleInfo) {
            final LocaleInfo copy = ((LocaleInfo) draftCopy);
            if (this.locale!= null) {
                String sourceLocale;
                sourceLocale = this.getLocale();
                String copyLocale = ((String) strategy.copy(LocatorUtils.property(locator, "locale", sourceLocale), sourceLocale));
                copy.setLocale(copyLocale);
            } else {
                copy.locale = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LocaleInfo();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LocaleInfo.Builder<_B> _other) {
        _other.locale = this.locale;
    }

    public<_B >LocaleInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LocaleInfo.Builder<_B>(_parentBuilder, this, true);
    }

    public LocaleInfo.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LocaleInfo.Builder<Void> builder() {
        return new LocaleInfo.Builder<Void>(null, null, false);
    }

    public static<_B >LocaleInfo.Builder<_B> copyOf(final LocaleInfo _other) {
        final LocaleInfo.Builder<_B> _newBuilder = new LocaleInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LocaleInfo.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree localePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("locale"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(localePropertyTree!= null):((localePropertyTree == null)||(!localePropertyTree.isLeaf())))) {
            _other.locale = this.locale;
        }
    }

    public<_B >LocaleInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LocaleInfo.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public LocaleInfo.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LocaleInfo.Builder<_B> copyOf(final LocaleInfo _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LocaleInfo.Builder<_B> _newBuilder = new LocaleInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LocaleInfo.Builder<Void> copyExcept(final LocaleInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LocaleInfo.Builder<Void> copyOnly(final LocaleInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final LocaleInfo _storedValue;
        private String locale;

        public Builder(final _B _parentBuilder, final LocaleInfo _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.locale = _other.locale;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final LocaleInfo _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree localePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("locale"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(localePropertyTree!= null):((localePropertyTree == null)||(!localePropertyTree.isLeaf())))) {
                        this.locale = _other.locale;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends LocaleInfo >_P init(final _P _product) {
            _product.locale = this.locale;
            return _product;
        }

        /**
         * Sets the new value of "locale" (any previous value will be replaced)
         * 
         * @param locale
         *     New value of the "locale" property.
         */
        public LocaleInfo.Builder<_B> withLocale(final String locale) {
            this.locale = locale;
            return this;
        }

        @Override
        public LocaleInfo build() {
            if (_storedValue == null) {
                return this.init(new LocaleInfo());
            } else {
                return ((LocaleInfo) _storedValue);
            }
        }

        public LocaleInfo.Builder<_B> copyOf(final LocaleInfo _other) {
            _other.copyTo(this);
            return this;
        }

        public LocaleInfo.Builder<_B> copyOf(final LocaleInfo.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LocaleInfo.Selector<LocaleInfo.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LocaleInfo.Select _root() {
            return new LocaleInfo.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, LocaleInfo.Selector<TRoot, TParent>> locale = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.locale!= null) {
                products.put("locale", this.locale.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, LocaleInfo.Selector<TRoot, TParent>> locale() {
            return ((this.locale == null)?this.locale = new com.kscs.util.jaxb.Selector<TRoot, LocaleInfo.Selector<TRoot, TParent>>(this._root, this, "locale"):this.locale);
        }

    }

}
