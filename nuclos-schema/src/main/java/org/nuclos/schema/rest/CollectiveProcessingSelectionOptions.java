
package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for collective-processing-selection-options complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="collective-processing-selection-options"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="actions" type="{urn:org.nuclos.schema.rest}collective-processing-action" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="stateChanges" type="{urn:org.nuclos.schema.rest}collective-processing-action" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="customRules" type="{urn:org.nuclos.schema.rest}collective-processing-action" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="generators" type="{urn:org.nuclos.schema.rest}collective-processing-action" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collective-processing-selection-options", propOrder = {
    "actions",
    "stateChanges",
    "customRules",
    "generators"
})
public class CollectiveProcessingSelectionOptions implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<CollectiveProcessingAction> actions;
    protected List<CollectiveProcessingAction> stateChanges;
    protected List<CollectiveProcessingAction> customRules;
    protected List<CollectiveProcessingAction> generators;

    /**
     * Gets the value of the actions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollectiveProcessingAction }
     * 
     * 
     */
    public List<CollectiveProcessingAction> getActions() {
        if (actions == null) {
            actions = new ArrayList<CollectiveProcessingAction>();
        }
        return this.actions;
    }

    /**
     * Gets the value of the stateChanges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stateChanges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStateChanges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollectiveProcessingAction }
     * 
     * 
     */
    public List<CollectiveProcessingAction> getStateChanges() {
        if (stateChanges == null) {
            stateChanges = new ArrayList<CollectiveProcessingAction>();
        }
        return this.stateChanges;
    }

    /**
     * Gets the value of the customRules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customRules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomRules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollectiveProcessingAction }
     * 
     * 
     */
    public List<CollectiveProcessingAction> getCustomRules() {
        if (customRules == null) {
            customRules = new ArrayList<CollectiveProcessingAction>();
        }
        return this.customRules;
    }

    /**
     * Gets the value of the generators property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the generators property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenerators().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CollectiveProcessingAction }
     * 
     * 
     */
    public List<CollectiveProcessingAction> getGenerators() {
        if (generators == null) {
            generators = new ArrayList<CollectiveProcessingAction>();
        }
        return this.generators;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<CollectiveProcessingAction> theActions;
            theActions = (((this.actions!= null)&&(!this.actions.isEmpty()))?this.getActions():null);
            strategy.appendField(locator, this, "actions", buffer, theActions);
        }
        {
            List<CollectiveProcessingAction> theStateChanges;
            theStateChanges = (((this.stateChanges!= null)&&(!this.stateChanges.isEmpty()))?this.getStateChanges():null);
            strategy.appendField(locator, this, "stateChanges", buffer, theStateChanges);
        }
        {
            List<CollectiveProcessingAction> theCustomRules;
            theCustomRules = (((this.customRules!= null)&&(!this.customRules.isEmpty()))?this.getCustomRules():null);
            strategy.appendField(locator, this, "customRules", buffer, theCustomRules);
        }
        {
            List<CollectiveProcessingAction> theGenerators;
            theGenerators = (((this.generators!= null)&&(!this.generators.isEmpty()))?this.getGenerators():null);
            strategy.appendField(locator, this, "generators", buffer, theGenerators);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectiveProcessingSelectionOptions)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectiveProcessingSelectionOptions that = ((CollectiveProcessingSelectionOptions) object);
        {
            List<CollectiveProcessingAction> lhsActions;
            lhsActions = (((this.actions!= null)&&(!this.actions.isEmpty()))?this.getActions():null);
            List<CollectiveProcessingAction> rhsActions;
            rhsActions = (((that.actions!= null)&&(!that.actions.isEmpty()))?that.getActions():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actions", lhsActions), LocatorUtils.property(thatLocator, "actions", rhsActions), lhsActions, rhsActions)) {
                return false;
            }
        }
        {
            List<CollectiveProcessingAction> lhsStateChanges;
            lhsStateChanges = (((this.stateChanges!= null)&&(!this.stateChanges.isEmpty()))?this.getStateChanges():null);
            List<CollectiveProcessingAction> rhsStateChanges;
            rhsStateChanges = (((that.stateChanges!= null)&&(!that.stateChanges.isEmpty()))?that.getStateChanges():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "stateChanges", lhsStateChanges), LocatorUtils.property(thatLocator, "stateChanges", rhsStateChanges), lhsStateChanges, rhsStateChanges)) {
                return false;
            }
        }
        {
            List<CollectiveProcessingAction> lhsCustomRules;
            lhsCustomRules = (((this.customRules!= null)&&(!this.customRules.isEmpty()))?this.getCustomRules():null);
            List<CollectiveProcessingAction> rhsCustomRules;
            rhsCustomRules = (((that.customRules!= null)&&(!that.customRules.isEmpty()))?that.getCustomRules():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customRules", lhsCustomRules), LocatorUtils.property(thatLocator, "customRules", rhsCustomRules), lhsCustomRules, rhsCustomRules)) {
                return false;
            }
        }
        {
            List<CollectiveProcessingAction> lhsGenerators;
            lhsGenerators = (((this.generators!= null)&&(!this.generators.isEmpty()))?this.getGenerators():null);
            List<CollectiveProcessingAction> rhsGenerators;
            rhsGenerators = (((that.generators!= null)&&(!that.generators.isEmpty()))?that.getGenerators():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "generators", lhsGenerators), LocatorUtils.property(thatLocator, "generators", rhsGenerators), lhsGenerators, rhsGenerators)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<CollectiveProcessingAction> theActions;
            theActions = (((this.actions!= null)&&(!this.actions.isEmpty()))?this.getActions():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "actions", theActions), currentHashCode, theActions);
        }
        {
            List<CollectiveProcessingAction> theStateChanges;
            theStateChanges = (((this.stateChanges!= null)&&(!this.stateChanges.isEmpty()))?this.getStateChanges():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "stateChanges", theStateChanges), currentHashCode, theStateChanges);
        }
        {
            List<CollectiveProcessingAction> theCustomRules;
            theCustomRules = (((this.customRules!= null)&&(!this.customRules.isEmpty()))?this.getCustomRules():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customRules", theCustomRules), currentHashCode, theCustomRules);
        }
        {
            List<CollectiveProcessingAction> theGenerators;
            theGenerators = (((this.generators!= null)&&(!this.generators.isEmpty()))?this.getGenerators():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "generators", theGenerators), currentHashCode, theGenerators);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectiveProcessingSelectionOptions) {
            final CollectiveProcessingSelectionOptions copy = ((CollectiveProcessingSelectionOptions) draftCopy);
            if ((this.actions!= null)&&(!this.actions.isEmpty())) {
                List<CollectiveProcessingAction> sourceActions;
                sourceActions = (((this.actions!= null)&&(!this.actions.isEmpty()))?this.getActions():null);
                @SuppressWarnings("unchecked")
                List<CollectiveProcessingAction> copyActions = ((List<CollectiveProcessingAction> ) strategy.copy(LocatorUtils.property(locator, "actions", sourceActions), sourceActions));
                copy.actions = null;
                if (copyActions!= null) {
                    List<CollectiveProcessingAction> uniqueActionsl = copy.getActions();
                    uniqueActionsl.addAll(copyActions);
                }
            } else {
                copy.actions = null;
            }
            if ((this.stateChanges!= null)&&(!this.stateChanges.isEmpty())) {
                List<CollectiveProcessingAction> sourceStateChanges;
                sourceStateChanges = (((this.stateChanges!= null)&&(!this.stateChanges.isEmpty()))?this.getStateChanges():null);
                @SuppressWarnings("unchecked")
                List<CollectiveProcessingAction> copyStateChanges = ((List<CollectiveProcessingAction> ) strategy.copy(LocatorUtils.property(locator, "stateChanges", sourceStateChanges), sourceStateChanges));
                copy.stateChanges = null;
                if (copyStateChanges!= null) {
                    List<CollectiveProcessingAction> uniqueStateChangesl = copy.getStateChanges();
                    uniqueStateChangesl.addAll(copyStateChanges);
                }
            } else {
                copy.stateChanges = null;
            }
            if ((this.customRules!= null)&&(!this.customRules.isEmpty())) {
                List<CollectiveProcessingAction> sourceCustomRules;
                sourceCustomRules = (((this.customRules!= null)&&(!this.customRules.isEmpty()))?this.getCustomRules():null);
                @SuppressWarnings("unchecked")
                List<CollectiveProcessingAction> copyCustomRules = ((List<CollectiveProcessingAction> ) strategy.copy(LocatorUtils.property(locator, "customRules", sourceCustomRules), sourceCustomRules));
                copy.customRules = null;
                if (copyCustomRules!= null) {
                    List<CollectiveProcessingAction> uniqueCustomRulesl = copy.getCustomRules();
                    uniqueCustomRulesl.addAll(copyCustomRules);
                }
            } else {
                copy.customRules = null;
            }
            if ((this.generators!= null)&&(!this.generators.isEmpty())) {
                List<CollectiveProcessingAction> sourceGenerators;
                sourceGenerators = (((this.generators!= null)&&(!this.generators.isEmpty()))?this.getGenerators():null);
                @SuppressWarnings("unchecked")
                List<CollectiveProcessingAction> copyGenerators = ((List<CollectiveProcessingAction> ) strategy.copy(LocatorUtils.property(locator, "generators", sourceGenerators), sourceGenerators));
                copy.generators = null;
                if (copyGenerators!= null) {
                    List<CollectiveProcessingAction> uniqueGeneratorsl = copy.getGenerators();
                    uniqueGeneratorsl.addAll(copyGenerators);
                }
            } else {
                copy.generators = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectiveProcessingSelectionOptions();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingSelectionOptions.Builder<_B> _other) {
        if (this.actions == null) {
            _other.actions = null;
        } else {
            _other.actions = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            for (CollectiveProcessingAction _item: this.actions) {
                _other.actions.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.stateChanges == null) {
            _other.stateChanges = null;
        } else {
            _other.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            for (CollectiveProcessingAction _item: this.stateChanges) {
                _other.stateChanges.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.customRules == null) {
            _other.customRules = null;
        } else {
            _other.customRules = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            for (CollectiveProcessingAction _item: this.customRules) {
                _other.customRules.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.generators == null) {
            _other.generators = null;
        } else {
            _other.generators = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            for (CollectiveProcessingAction _item: this.generators) {
                _other.generators.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >CollectiveProcessingSelectionOptions.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectiveProcessingSelectionOptions.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectiveProcessingSelectionOptions.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectiveProcessingSelectionOptions.Builder<Void> builder() {
        return new CollectiveProcessingSelectionOptions.Builder<Void>(null, null, false);
    }

    public static<_B >CollectiveProcessingSelectionOptions.Builder<_B> copyOf(final CollectiveProcessingSelectionOptions _other) {
        final CollectiveProcessingSelectionOptions.Builder<_B> _newBuilder = new CollectiveProcessingSelectionOptions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingSelectionOptions.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree actionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actions"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actionsPropertyTree!= null):((actionsPropertyTree == null)||(!actionsPropertyTree.isLeaf())))) {
            if (this.actions == null) {
                _other.actions = null;
            } else {
                _other.actions = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                for (CollectiveProcessingAction _item: this.actions) {
                    _other.actions.add(((_item == null)?null:_item.newCopyBuilder(_other, actionsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree stateChangesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("stateChanges"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stateChangesPropertyTree!= null):((stateChangesPropertyTree == null)||(!stateChangesPropertyTree.isLeaf())))) {
            if (this.stateChanges == null) {
                _other.stateChanges = null;
            } else {
                _other.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                for (CollectiveProcessingAction _item: this.stateChanges) {
                    _other.stateChanges.add(((_item == null)?null:_item.newCopyBuilder(_other, stateChangesPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree customRulesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customRules"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customRulesPropertyTree!= null):((customRulesPropertyTree == null)||(!customRulesPropertyTree.isLeaf())))) {
            if (this.customRules == null) {
                _other.customRules = null;
            } else {
                _other.customRules = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                for (CollectiveProcessingAction _item: this.customRules) {
                    _other.customRules.add(((_item == null)?null:_item.newCopyBuilder(_other, customRulesPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree generatorsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("generators"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(generatorsPropertyTree!= null):((generatorsPropertyTree == null)||(!generatorsPropertyTree.isLeaf())))) {
            if (this.generators == null) {
                _other.generators = null;
            } else {
                _other.generators = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                for (CollectiveProcessingAction _item: this.generators) {
                    _other.generators.add(((_item == null)?null:_item.newCopyBuilder(_other, generatorsPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >CollectiveProcessingSelectionOptions.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectiveProcessingSelectionOptions.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectiveProcessingSelectionOptions.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectiveProcessingSelectionOptions.Builder<_B> copyOf(final CollectiveProcessingSelectionOptions _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectiveProcessingSelectionOptions.Builder<_B> _newBuilder = new CollectiveProcessingSelectionOptions.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectiveProcessingSelectionOptions.Builder<Void> copyExcept(final CollectiveProcessingSelectionOptions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectiveProcessingSelectionOptions.Builder<Void> copyOnly(final CollectiveProcessingSelectionOptions _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectiveProcessingSelectionOptions _storedValue;
        private List<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>> actions;
        private List<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>> stateChanges;
        private List<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>> customRules;
        private List<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>> generators;

        public Builder(final _B _parentBuilder, final CollectiveProcessingSelectionOptions _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.actions == null) {
                        this.actions = null;
                    } else {
                        this.actions = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                        for (CollectiveProcessingAction _item: _other.actions) {
                            this.actions.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.stateChanges == null) {
                        this.stateChanges = null;
                    } else {
                        this.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                        for (CollectiveProcessingAction _item: _other.stateChanges) {
                            this.stateChanges.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.customRules == null) {
                        this.customRules = null;
                    } else {
                        this.customRules = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                        for (CollectiveProcessingAction _item: _other.customRules) {
                            this.customRules.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.generators == null) {
                        this.generators = null;
                    } else {
                        this.generators = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                        for (CollectiveProcessingAction _item: _other.generators) {
                            this.generators.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectiveProcessingSelectionOptions _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree actionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actions"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actionsPropertyTree!= null):((actionsPropertyTree == null)||(!actionsPropertyTree.isLeaf())))) {
                        if (_other.actions == null) {
                            this.actions = null;
                        } else {
                            this.actions = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                            for (CollectiveProcessingAction _item: _other.actions) {
                                this.actions.add(((_item == null)?null:_item.newCopyBuilder(this, actionsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree stateChangesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("stateChanges"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stateChangesPropertyTree!= null):((stateChangesPropertyTree == null)||(!stateChangesPropertyTree.isLeaf())))) {
                        if (_other.stateChanges == null) {
                            this.stateChanges = null;
                        } else {
                            this.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                            for (CollectiveProcessingAction _item: _other.stateChanges) {
                                this.stateChanges.add(((_item == null)?null:_item.newCopyBuilder(this, stateChangesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree customRulesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customRules"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customRulesPropertyTree!= null):((customRulesPropertyTree == null)||(!customRulesPropertyTree.isLeaf())))) {
                        if (_other.customRules == null) {
                            this.customRules = null;
                        } else {
                            this.customRules = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                            for (CollectiveProcessingAction _item: _other.customRules) {
                                this.customRules.add(((_item == null)?null:_item.newCopyBuilder(this, customRulesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree generatorsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("generators"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(generatorsPropertyTree!= null):((generatorsPropertyTree == null)||(!generatorsPropertyTree.isLeaf())))) {
                        if (_other.generators == null) {
                            this.generators = null;
                        } else {
                            this.generators = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                            for (CollectiveProcessingAction _item: _other.generators) {
                                this.generators.add(((_item == null)?null:_item.newCopyBuilder(this, generatorsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectiveProcessingSelectionOptions >_P init(final _P _product) {
            if (this.actions!= null) {
                final List<CollectiveProcessingAction> actions = new ArrayList<CollectiveProcessingAction>(this.actions.size());
                for (CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> _item: this.actions) {
                    actions.add(_item.build());
                }
                _product.actions = actions;
            }
            if (this.stateChanges!= null) {
                final List<CollectiveProcessingAction> stateChanges = new ArrayList<CollectiveProcessingAction>(this.stateChanges.size());
                for (CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> _item: this.stateChanges) {
                    stateChanges.add(_item.build());
                }
                _product.stateChanges = stateChanges;
            }
            if (this.customRules!= null) {
                final List<CollectiveProcessingAction> customRules = new ArrayList<CollectiveProcessingAction>(this.customRules.size());
                for (CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> _item: this.customRules) {
                    customRules.add(_item.build());
                }
                _product.customRules = customRules;
            }
            if (this.generators!= null) {
                final List<CollectiveProcessingAction> generators = new ArrayList<CollectiveProcessingAction>(this.generators.size());
                for (CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> _item: this.generators) {
                    generators.add(_item.build());
                }
                _product.generators = generators;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "actions"
         * 
         * @param actions
         *     Items to add to the value of the "actions" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addActions(final Iterable<? extends CollectiveProcessingAction> actions) {
            if (actions!= null) {
                if (this.actions == null) {
                    this.actions = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                }
                for (CollectiveProcessingAction _item: actions) {
                    this.actions.add(new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "actions" (any previous value will be replaced)
         * 
         * @param actions
         *     New value of the "actions" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withActions(final Iterable<? extends CollectiveProcessingAction> actions) {
            if (this.actions!= null) {
                this.actions.clear();
            }
            return addActions(actions);
        }

        /**
         * Adds the given items to the value of "actions"
         * 
         * @param actions
         *     Items to add to the value of the "actions" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addActions(CollectiveProcessingAction... actions) {
            addActions(Arrays.asList(actions));
            return this;
        }

        /**
         * Sets the new value of "actions" (any previous value will be replaced)
         * 
         * @param actions
         *     New value of the "actions" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withActions(CollectiveProcessingAction... actions) {
            withActions(Arrays.asList(actions));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Actions" property.
         * Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Actions" property.
         *     Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         */
        public CollectiveProcessingAction.Builder<? extends CollectiveProcessingSelectionOptions.Builder<_B>> addActions() {
            if (this.actions == null) {
                this.actions = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            }
            final CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> actions_Builder = new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, null, false);
            this.actions.add(actions_Builder);
            return actions_Builder;
        }

        /**
         * Adds the given items to the value of "stateChanges"
         * 
         * @param stateChanges
         *     Items to add to the value of the "stateChanges" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addStateChanges(final Iterable<? extends CollectiveProcessingAction> stateChanges) {
            if (stateChanges!= null) {
                if (this.stateChanges == null) {
                    this.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                }
                for (CollectiveProcessingAction _item: stateChanges) {
                    this.stateChanges.add(new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "stateChanges" (any previous value will be replaced)
         * 
         * @param stateChanges
         *     New value of the "stateChanges" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withStateChanges(final Iterable<? extends CollectiveProcessingAction> stateChanges) {
            if (this.stateChanges!= null) {
                this.stateChanges.clear();
            }
            return addStateChanges(stateChanges);
        }

        /**
         * Adds the given items to the value of "stateChanges"
         * 
         * @param stateChanges
         *     Items to add to the value of the "stateChanges" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addStateChanges(CollectiveProcessingAction... stateChanges) {
            addStateChanges(Arrays.asList(stateChanges));
            return this;
        }

        /**
         * Sets the new value of "stateChanges" (any previous value will be replaced)
         * 
         * @param stateChanges
         *     New value of the "stateChanges" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withStateChanges(CollectiveProcessingAction... stateChanges) {
            withStateChanges(Arrays.asList(stateChanges));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "StateChanges" property.
         * Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "StateChanges" property.
         *     Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         */
        public CollectiveProcessingAction.Builder<? extends CollectiveProcessingSelectionOptions.Builder<_B>> addStateChanges() {
            if (this.stateChanges == null) {
                this.stateChanges = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            }
            final CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> stateChanges_Builder = new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, null, false);
            this.stateChanges.add(stateChanges_Builder);
            return stateChanges_Builder;
        }

        /**
         * Adds the given items to the value of "customRules"
         * 
         * @param customRules
         *     Items to add to the value of the "customRules" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addCustomRules(final Iterable<? extends CollectiveProcessingAction> customRules) {
            if (customRules!= null) {
                if (this.customRules == null) {
                    this.customRules = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                }
                for (CollectiveProcessingAction _item: customRules) {
                    this.customRules.add(new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "customRules" (any previous value will be replaced)
         * 
         * @param customRules
         *     New value of the "customRules" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withCustomRules(final Iterable<? extends CollectiveProcessingAction> customRules) {
            if (this.customRules!= null) {
                this.customRules.clear();
            }
            return addCustomRules(customRules);
        }

        /**
         * Adds the given items to the value of "customRules"
         * 
         * @param customRules
         *     Items to add to the value of the "customRules" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addCustomRules(CollectiveProcessingAction... customRules) {
            addCustomRules(Arrays.asList(customRules));
            return this;
        }

        /**
         * Sets the new value of "customRules" (any previous value will be replaced)
         * 
         * @param customRules
         *     New value of the "customRules" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withCustomRules(CollectiveProcessingAction... customRules) {
            withCustomRules(Arrays.asList(customRules));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "CustomRules" property.
         * Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "CustomRules" property.
         *     Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         */
        public CollectiveProcessingAction.Builder<? extends CollectiveProcessingSelectionOptions.Builder<_B>> addCustomRules() {
            if (this.customRules == null) {
                this.customRules = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            }
            final CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> customRules_Builder = new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, null, false);
            this.customRules.add(customRules_Builder);
            return customRules_Builder;
        }

        /**
         * Adds the given items to the value of "generators"
         * 
         * @param generators
         *     Items to add to the value of the "generators" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addGenerators(final Iterable<? extends CollectiveProcessingAction> generators) {
            if (generators!= null) {
                if (this.generators == null) {
                    this.generators = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
                }
                for (CollectiveProcessingAction _item: generators) {
                    this.generators.add(new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "generators" (any previous value will be replaced)
         * 
         * @param generators
         *     New value of the "generators" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withGenerators(final Iterable<? extends CollectiveProcessingAction> generators) {
            if (this.generators!= null) {
                this.generators.clear();
            }
            return addGenerators(generators);
        }

        /**
         * Adds the given items to the value of "generators"
         * 
         * @param generators
         *     Items to add to the value of the "generators" property
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> addGenerators(CollectiveProcessingAction... generators) {
            addGenerators(Arrays.asList(generators));
            return this;
        }

        /**
         * Sets the new value of "generators" (any previous value will be replaced)
         * 
         * @param generators
         *     New value of the "generators" property.
         */
        public CollectiveProcessingSelectionOptions.Builder<_B> withGenerators(CollectiveProcessingAction... generators) {
            withGenerators(Arrays.asList(generators));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Generators" property.
         * Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Generators" property.
         *     Use {@link org.nuclos.schema.rest.CollectiveProcessingAction.Builder#end()} to return to the current builder.
         */
        public CollectiveProcessingAction.Builder<? extends CollectiveProcessingSelectionOptions.Builder<_B>> addGenerators() {
            if (this.generators == null) {
                this.generators = new ArrayList<CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>>();
            }
            final CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>> generators_Builder = new CollectiveProcessingAction.Builder<CollectiveProcessingSelectionOptions.Builder<_B>>(this, null, false);
            this.generators.add(generators_Builder);
            return generators_Builder;
        }

        @Override
        public CollectiveProcessingSelectionOptions build() {
            if (_storedValue == null) {
                return this.init(new CollectiveProcessingSelectionOptions());
            } else {
                return ((CollectiveProcessingSelectionOptions) _storedValue);
            }
        }

        public CollectiveProcessingSelectionOptions.Builder<_B> copyOf(final CollectiveProcessingSelectionOptions _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectiveProcessingSelectionOptions.Builder<_B> copyOf(final CollectiveProcessingSelectionOptions.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectiveProcessingSelectionOptions.Selector<CollectiveProcessingSelectionOptions.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectiveProcessingSelectionOptions.Select _root() {
            return new CollectiveProcessingSelectionOptions.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> actions = null;
        private CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> stateChanges = null;
        private CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> customRules = null;
        private CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> generators = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.actions!= null) {
                products.put("actions", this.actions.init());
            }
            if (this.stateChanges!= null) {
                products.put("stateChanges", this.stateChanges.init());
            }
            if (this.customRules!= null) {
                products.put("customRules", this.customRules.init());
            }
            if (this.generators!= null) {
                products.put("generators", this.generators.init());
            }
            return products;
        }

        public CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> actions() {
            return ((this.actions == null)?this.actions = new CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>>(this._root, this, "actions"):this.actions);
        }

        public CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> stateChanges() {
            return ((this.stateChanges == null)?this.stateChanges = new CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>>(this._root, this, "stateChanges"):this.stateChanges);
        }

        public CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> customRules() {
            return ((this.customRules == null)?this.customRules = new CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>>(this._root, this, "customRules"):this.customRules);
        }

        public CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>> generators() {
            return ((this.generators == null)?this.generators = new CollectiveProcessingAction.Selector<TRoot, CollectiveProcessingSelectionOptions.Selector<TRoot, TParent>>(this._root, this, "generators"):this.generators);
        }

    }

}
