
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A VLP for an input component.
 * 
 * <p>Java class for web-valuelist-provider complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-valuelist-provider"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parameter" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="type" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="default"/&gt;
 *             &lt;enumeration value="dependant"/&gt;
 *             &lt;enumeration value="named"/&gt;
 *             &lt;enumeration value="ds"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="id-fieldname" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="fieldname" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-valuelist-provider", propOrder = {
    "parameter"
})
public class WebValuelistProvider implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<WebValuelistProvider.Parameter> parameter;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "type", required = true)
    protected String type;
    @XmlAttribute(name = "value", required = true)
    protected String value;
    @XmlAttribute(name = "id-fieldname")
    protected String idFieldname;
    @XmlAttribute(name = "fieldname")
    protected String fieldname;

    /**
     * Gets the value of the parameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebValuelistProvider.Parameter }
     * 
     * 
     */
    public List<WebValuelistProvider.Parameter> getParameter() {
        if (parameter == null) {
            parameter = new ArrayList<WebValuelistProvider.Parameter>();
        }
        return this.parameter;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the idFieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFieldname() {
        return idFieldname;
    }

    /**
     * Sets the value of the idFieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFieldname(String value) {
        this.idFieldname = value;
    }

    /**
     * Gets the value of the fieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldname() {
        return fieldname;
    }

    /**
     * Sets the value of the fieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldname(String value) {
        this.fieldname = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebValuelistProvider.Parameter> theParameter;
            theParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            strategy.appendField(locator, this, "parameter", buffer, theParameter);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        {
            String theIdFieldname;
            theIdFieldname = this.getIdFieldname();
            strategy.appendField(locator, this, "idFieldname", buffer, theIdFieldname);
        }
        {
            String theFieldname;
            theFieldname = this.getFieldname();
            strategy.appendField(locator, this, "fieldname", buffer, theFieldname);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebValuelistProvider)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebValuelistProvider that = ((WebValuelistProvider) object);
        {
            List<WebValuelistProvider.Parameter> lhsParameter;
            lhsParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            List<WebValuelistProvider.Parameter> rhsParameter;
            rhsParameter = (((that.parameter!= null)&&(!that.parameter.isEmpty()))?that.getParameter():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameter", lhsParameter), LocatorUtils.property(thatLocator, "parameter", rhsParameter), lhsParameter, rhsParameter)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsIdFieldname;
            lhsIdFieldname = this.getIdFieldname();
            String rhsIdFieldname;
            rhsIdFieldname = that.getIdFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "idFieldname", lhsIdFieldname), LocatorUtils.property(thatLocator, "idFieldname", rhsIdFieldname), lhsIdFieldname, rhsIdFieldname)) {
                return false;
            }
        }
        {
            String lhsFieldname;
            lhsFieldname = this.getFieldname();
            String rhsFieldname;
            rhsFieldname = that.getFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fieldname", lhsFieldname), LocatorUtils.property(thatLocator, "fieldname", rhsFieldname), lhsFieldname, rhsFieldname)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebValuelistProvider.Parameter> theParameter;
            theParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parameter", theParameter), currentHashCode, theParameter);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        {
            String theIdFieldname;
            theIdFieldname = this.getIdFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idFieldname", theIdFieldname), currentHashCode, theIdFieldname);
        }
        {
            String theFieldname;
            theFieldname = this.getFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fieldname", theFieldname), currentHashCode, theFieldname);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebValuelistProvider) {
            final WebValuelistProvider copy = ((WebValuelistProvider) draftCopy);
            if ((this.parameter!= null)&&(!this.parameter.isEmpty())) {
                List<WebValuelistProvider.Parameter> sourceParameter;
                sourceParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
                @SuppressWarnings("unchecked")
                List<WebValuelistProvider.Parameter> copyParameter = ((List<WebValuelistProvider.Parameter> ) strategy.copy(LocatorUtils.property(locator, "parameter", sourceParameter), sourceParameter));
                copy.parameter = null;
                if (copyParameter!= null) {
                    List<WebValuelistProvider.Parameter> uniqueParameterl = copy.getParameter();
                    uniqueParameterl.addAll(copyParameter);
                }
            } else {
                copy.parameter = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.idFieldname!= null) {
                String sourceIdFieldname;
                sourceIdFieldname = this.getIdFieldname();
                String copyIdFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "idFieldname", sourceIdFieldname), sourceIdFieldname));
                copy.setIdFieldname(copyIdFieldname);
            } else {
                copy.idFieldname = null;
            }
            if (this.fieldname!= null) {
                String sourceFieldname;
                sourceFieldname = this.getFieldname();
                String copyFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "fieldname", sourceFieldname), sourceFieldname));
                copy.setFieldname(copyFieldname);
            } else {
                copy.fieldname = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebValuelistProvider();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebValuelistProvider.Builder<_B> _other) {
        if (this.parameter == null) {
            _other.parameter = null;
        } else {
            _other.parameter = new ArrayList<WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>>();
            for (WebValuelistProvider.Parameter _item: this.parameter) {
                _other.parameter.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other.type = this.type;
        _other.value = this.value;
        _other.idFieldname = this.idFieldname;
        _other.fieldname = this.fieldname;
    }

    public<_B >WebValuelistProvider.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebValuelistProvider.Builder<_B>(_parentBuilder, this, true);
    }

    public WebValuelistProvider.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebValuelistProvider.Builder<Void> builder() {
        return new WebValuelistProvider.Builder<Void>(null, null, false);
    }

    public static<_B >WebValuelistProvider.Builder<_B> copyOf(final WebValuelistProvider _other) {
        final WebValuelistProvider.Builder<_B> _newBuilder = new WebValuelistProvider.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebValuelistProvider.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree parameterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameter"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterPropertyTree!= null):((parameterPropertyTree == null)||(!parameterPropertyTree.isLeaf())))) {
            if (this.parameter == null) {
                _other.parameter = null;
            } else {
                _other.parameter = new ArrayList<WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>>();
                for (WebValuelistProvider.Parameter _item: this.parameter) {
                    _other.parameter.add(((_item == null)?null:_item.newCopyBuilder(_other, parameterPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
        final PropertyTree idFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idFieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idFieldnamePropertyTree!= null):((idFieldnamePropertyTree == null)||(!idFieldnamePropertyTree.isLeaf())))) {
            _other.idFieldname = this.idFieldname;
        }
        final PropertyTree fieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldnamePropertyTree!= null):((fieldnamePropertyTree == null)||(!fieldnamePropertyTree.isLeaf())))) {
            _other.fieldname = this.fieldname;
        }
    }

    public<_B >WebValuelistProvider.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebValuelistProvider.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebValuelistProvider.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebValuelistProvider.Builder<_B> copyOf(final WebValuelistProvider _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebValuelistProvider.Builder<_B> _newBuilder = new WebValuelistProvider.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebValuelistProvider.Builder<Void> copyExcept(final WebValuelistProvider _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebValuelistProvider.Builder<Void> copyOnly(final WebValuelistProvider _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebValuelistProvider _storedValue;
        private List<WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>> parameter;
        private String name;
        private String type;
        private String value;
        private String idFieldname;
        private String fieldname;

        public Builder(final _B _parentBuilder, final WebValuelistProvider _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.parameter == null) {
                        this.parameter = null;
                    } else {
                        this.parameter = new ArrayList<WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>>();
                        for (WebValuelistProvider.Parameter _item: _other.parameter) {
                            this.parameter.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this.type = _other.type;
                    this.value = _other.value;
                    this.idFieldname = _other.idFieldname;
                    this.fieldname = _other.fieldname;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebValuelistProvider _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree parameterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameter"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterPropertyTree!= null):((parameterPropertyTree == null)||(!parameterPropertyTree.isLeaf())))) {
                        if (_other.parameter == null) {
                            this.parameter = null;
                        } else {
                            this.parameter = new ArrayList<WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>>();
                            for (WebValuelistProvider.Parameter _item: _other.parameter) {
                                this.parameter.add(((_item == null)?null:_item.newCopyBuilder(this, parameterPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                    final PropertyTree idFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idFieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idFieldnamePropertyTree!= null):((idFieldnamePropertyTree == null)||(!idFieldnamePropertyTree.isLeaf())))) {
                        this.idFieldname = _other.idFieldname;
                    }
                    final PropertyTree fieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldnamePropertyTree!= null):((fieldnamePropertyTree == null)||(!fieldnamePropertyTree.isLeaf())))) {
                        this.fieldname = _other.fieldname;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebValuelistProvider >_P init(final _P _product) {
            if (this.parameter!= null) {
                final List<WebValuelistProvider.Parameter> parameter = new ArrayList<WebValuelistProvider.Parameter>(this.parameter.size());
                for (WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>> _item: this.parameter) {
                    parameter.add(_item.build());
                }
                _product.parameter = parameter;
            }
            _product.name = this.name;
            _product.type = this.type;
            _product.value = this.value;
            _product.idFieldname = this.idFieldname;
            _product.fieldname = this.fieldname;
            return _product;
        }

        /**
         * Adds the given items to the value of "parameter"
         * 
         * @param parameter
         *     Items to add to the value of the "parameter" property
         */
        public WebValuelistProvider.Builder<_B> addParameter(final Iterable<? extends WebValuelistProvider.Parameter> parameter) {
            if (parameter!= null) {
                if (this.parameter == null) {
                    this.parameter = new ArrayList<WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>>();
                }
                for (WebValuelistProvider.Parameter _item: parameter) {
                    this.parameter.add(new WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "parameter" (any previous value will be replaced)
         * 
         * @param parameter
         *     New value of the "parameter" property.
         */
        public WebValuelistProvider.Builder<_B> withParameter(final Iterable<? extends WebValuelistProvider.Parameter> parameter) {
            if (this.parameter!= null) {
                this.parameter.clear();
            }
            return addParameter(parameter);
        }

        /**
         * Adds the given items to the value of "parameter"
         * 
         * @param parameter
         *     Items to add to the value of the "parameter" property
         */
        public WebValuelistProvider.Builder<_B> addParameter(WebValuelistProvider.Parameter... parameter) {
            addParameter(Arrays.asList(parameter));
            return this;
        }

        /**
         * Sets the new value of "parameter" (any previous value will be replaced)
         * 
         * @param parameter
         *     New value of the "parameter" property.
         */
        public WebValuelistProvider.Builder<_B> withParameter(WebValuelistProvider.Parameter... parameter) {
            withParameter(Arrays.asList(parameter));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Parameter" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Parameter.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Parameter" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Parameter.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Parameter.Builder<? extends WebValuelistProvider.Builder<_B>> addParameter() {
            if (this.parameter == null) {
                this.parameter = new ArrayList<WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>>();
            }
            final WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>> parameter_Builder = new WebValuelistProvider.Parameter.Builder<WebValuelistProvider.Builder<_B>>(this, null, false);
            this.parameter.add(parameter_Builder);
            return parameter_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public WebValuelistProvider.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public WebValuelistProvider.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public WebValuelistProvider.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        /**
         * Sets the new value of "idFieldname" (any previous value will be replaced)
         * 
         * @param idFieldname
         *     New value of the "idFieldname" property.
         */
        public WebValuelistProvider.Builder<_B> withIdFieldname(final String idFieldname) {
            this.idFieldname = idFieldname;
            return this;
        }

        /**
         * Sets the new value of "fieldname" (any previous value will be replaced)
         * 
         * @param fieldname
         *     New value of the "fieldname" property.
         */
        public WebValuelistProvider.Builder<_B> withFieldname(final String fieldname) {
            this.fieldname = fieldname;
            return this;
        }

        @Override
        public WebValuelistProvider build() {
            if (_storedValue == null) {
                return this.init(new WebValuelistProvider());
            } else {
                return ((WebValuelistProvider) _storedValue);
            }
        }

        public WebValuelistProvider.Builder<_B> copyOf(final WebValuelistProvider _other) {
            _other.copyTo(this);
            return this;
        }

        public WebValuelistProvider.Builder<_B> copyOf(final WebValuelistProvider.Builder _other) {
            return copyOf(_other.build());
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Parameter implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
    {

        private final static long serialVersionUID = 1L;
        @XmlAttribute(name = "name", required = true)
        protected String name;
        @XmlAttribute(name = "value", required = true)
        protected String value;

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        public String toString() {
            final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
            final StringBuilder buffer = new StringBuilder();
            append(null, buffer, strategy);
            return buffer.toString();
        }

        public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
            strategy.appendStart(locator, this, buffer);
            appendFields(locator, buffer, strategy);
            strategy.appendEnd(locator, this, buffer);
            return buffer;
        }

        public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
            {
                String theName;
                theName = this.getName();
                strategy.appendField(locator, this, "name", buffer, theName);
            }
            {
                String theValue;
                theValue = this.getValue();
                strategy.appendField(locator, this, "value", buffer, theValue);
            }
            return buffer;
        }

        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
            if (!(object instanceof WebValuelistProvider.Parameter)) {
                return false;
            }
            if (this == object) {
                return true;
            }
            final WebValuelistProvider.Parameter that = ((WebValuelistProvider.Parameter) object);
            {
                String lhsName;
                lhsName = this.getName();
                String rhsName;
                rhsName = that.getName();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                    return false;
                }
            }
            {
                String lhsValue;
                lhsValue = this.getValue();
                String rhsValue;
                rhsValue = that.getValue();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object object) {
            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
            return equals(null, null, object, strategy);
        }

        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
            int currentHashCode = 1;
            {
                String theName;
                theName = this.getName();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
            }
            {
                String theValue;
                theValue = this.getValue();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
            }
            return currentHashCode;
        }

        public int hashCode() {
            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
            return this.hashCode(null, strategy);
        }

        public Object clone() {
            return copyTo(createNewInstance());
        }

        public Object copyTo(Object target) {
            final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
            return copyTo(null, target, strategy);
        }

        public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
            final Object draftCopy = ((target == null)?createNewInstance():target);
            if (draftCopy instanceof WebValuelistProvider.Parameter) {
                final WebValuelistProvider.Parameter copy = ((WebValuelistProvider.Parameter) draftCopy);
                if (this.name!= null) {
                    String sourceName;
                    sourceName = this.getName();
                    String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                    copy.setName(copyName);
                } else {
                    copy.name = null;
                }
                if (this.value!= null) {
                    String sourceValue;
                    sourceValue = this.getValue();
                    String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                    copy.setValue(copyValue);
                } else {
                    copy.value = null;
                }
            }
            return draftCopy;
        }

        public Object createNewInstance() {
            return new WebValuelistProvider.Parameter();
        }

        /**
         * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
         * 
         * @param _other
         *     A builder instance to which the state of this object will be copied.
         */
        public<_B >void copyTo(final WebValuelistProvider.Parameter.Builder<_B> _other) {
            _other.name = this.name;
            _other.value = this.value;
        }

        public<_B >WebValuelistProvider.Parameter.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
            return new WebValuelistProvider.Parameter.Builder<_B>(_parentBuilder, this, true);
        }

        public WebValuelistProvider.Parameter.Builder<Void> newCopyBuilder() {
            return newCopyBuilder(null);
        }

        public static WebValuelistProvider.Parameter.Builder<Void> builder() {
            return new WebValuelistProvider.Parameter.Builder<Void>(null, null, false);
        }

        public static<_B >WebValuelistProvider.Parameter.Builder<_B> copyOf(final WebValuelistProvider.Parameter _other) {
            final WebValuelistProvider.Parameter.Builder<_B> _newBuilder = new WebValuelistProvider.Parameter.Builder<_B>(null, null, false);
            _other.copyTo(_newBuilder);
            return _newBuilder;
        }

        /**
         * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
         * 
         * @param _other
         *     A builder instance to which the state of this object will be copied.
         */
        public<_B >void copyTo(final WebValuelistProvider.Parameter.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                _other.name = this.name;
            }
            final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                _other.value = this.value;
            }
        }

        public<_B >WebValuelistProvider.Parameter.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            return new WebValuelistProvider.Parameter.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
        }

        public WebValuelistProvider.Parameter.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
        }

        public static<_B >WebValuelistProvider.Parameter.Builder<_B> copyOf(final WebValuelistProvider.Parameter _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            final WebValuelistProvider.Parameter.Builder<_B> _newBuilder = new WebValuelistProvider.Parameter.Builder<_B>(null, null, false);
            _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
            return _newBuilder;
        }

        public static WebValuelistProvider.Parameter.Builder<Void> copyExcept(final WebValuelistProvider.Parameter _other, final PropertyTree _propertyTree) {
            return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
        }

        public static WebValuelistProvider.Parameter.Builder<Void> copyOnly(final WebValuelistProvider.Parameter _other, final PropertyTree _propertyTree) {
            return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
        }

        public static class Builder<_B >implements Buildable
        {

            protected final _B _parentBuilder;
            protected final WebValuelistProvider.Parameter _storedValue;
            private String name;
            private String value;

            public Builder(final _B _parentBuilder, final WebValuelistProvider.Parameter _other, final boolean _copy) {
                this._parentBuilder = _parentBuilder;
                if (_other!= null) {
                    if (_copy) {
                        _storedValue = null;
                        this.name = _other.name;
                        this.value = _other.value;
                    } else {
                        _storedValue = _other;
                    }
                } else {
                    _storedValue = null;
                }
            }

            public Builder(final _B _parentBuilder, final WebValuelistProvider.Parameter _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
                this._parentBuilder = _parentBuilder;
                if (_other!= null) {
                    if (_copy) {
                        _storedValue = null;
                        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                            this.name = _other.name;
                        }
                        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                            this.value = _other.value;
                        }
                    } else {
                        _storedValue = _other;
                    }
                } else {
                    _storedValue = null;
                }
            }

            public _B end() {
                return this._parentBuilder;
            }

            protected<_P extends WebValuelistProvider.Parameter >_P init(final _P _product) {
                _product.name = this.name;
                _product.value = this.value;
                return _product;
            }

            /**
             * Sets the new value of "name" (any previous value will be replaced)
             * 
             * @param name
             *     New value of the "name" property.
             */
            public WebValuelistProvider.Parameter.Builder<_B> withName(final String name) {
                this.name = name;
                return this;
            }

            /**
             * Sets the new value of "value" (any previous value will be replaced)
             * 
             * @param value
             *     New value of the "value" property.
             */
            public WebValuelistProvider.Parameter.Builder<_B> withValue(final String value) {
                this.value = value;
                return this;
            }

            @Override
            public WebValuelistProvider.Parameter build() {
                if (_storedValue == null) {
                    return this.init(new WebValuelistProvider.Parameter());
                } else {
                    return ((WebValuelistProvider.Parameter) _storedValue);
                }
            }

            public WebValuelistProvider.Parameter.Builder<_B> copyOf(final WebValuelistProvider.Parameter _other) {
                _other.copyTo(this);
                return this;
            }

            public WebValuelistProvider.Parameter.Builder<_B> copyOf(final WebValuelistProvider.Parameter.Builder _other) {
                return copyOf(_other.build());
            }

        }

        public static class Select
            extends WebValuelistProvider.Parameter.Selector<WebValuelistProvider.Parameter.Select, Void>
        {


            Select() {
                super(null, null, null);
            }

            public static WebValuelistProvider.Parameter.Select _root() {
                return new WebValuelistProvider.Parameter.Select();
            }

        }

        public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
            extends com.kscs.util.jaxb.Selector<TRoot, TParent>
        {

            private com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Parameter.Selector<TRoot, TParent>> name = null;
            private com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Parameter.Selector<TRoot, TParent>> value = null;

            public Selector(final TRoot root, final TParent parent, final String propertyName) {
                super(root, parent, propertyName);
            }

            @Override
            public Map<String, PropertyTree> buildChildren() {
                final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
                products.putAll(super.buildChildren());
                if (this.name!= null) {
                    products.put("name", this.name.init());
                }
                if (this.value!= null) {
                    products.put("value", this.value.init());
                }
                return products;
            }

            public com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Parameter.Selector<TRoot, TParent>> name() {
                return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Parameter.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
            }

            public com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Parameter.Selector<TRoot, TParent>> value() {
                return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Parameter.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
            }

        }

    }

    public static class Select
        extends WebValuelistProvider.Selector<WebValuelistProvider.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebValuelistProvider.Select _root() {
            return new WebValuelistProvider.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebValuelistProvider.Parameter.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> parameter = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> value = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> idFieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> fieldname = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.parameter!= null) {
                products.put("parameter", this.parameter.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            if (this.idFieldname!= null) {
                products.put("idFieldname", this.idFieldname.init());
            }
            if (this.fieldname!= null) {
                products.put("fieldname", this.fieldname.init());
            }
            return products;
        }

        public WebValuelistProvider.Parameter.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> parameter() {
            return ((this.parameter == null)?this.parameter = new WebValuelistProvider.Parameter.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "parameter"):this.parameter);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> idFieldname() {
            return ((this.idFieldname == null)?this.idFieldname = new com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "idFieldname"):this.idFieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>> fieldname() {
            return ((this.fieldname == null)?this.fieldname = new com.kscs.util.jaxb.Selector<TRoot, WebValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "fieldname"):this.fieldname);
        }

    }

}
