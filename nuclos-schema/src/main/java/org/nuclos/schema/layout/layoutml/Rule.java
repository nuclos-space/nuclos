
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}event"/&gt;
 *         &lt;element ref="{}condition" minOccurs="0"/&gt;
 *         &lt;element ref="{}actions"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "event",
    "condition",
    "actions"
})
@XmlRootElement(name = "rule")
public class Rule implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected Event event;
    protected Condition condition;
    @XmlElement(required = true)
    protected Actions actions;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;

    /**
     * Gets the value of the event property.
     * 
     * @return
     *     possible object is
     *     {@link Event }
     *     
     */
    public Event getEvent() {
        return event;
    }

    /**
     * Sets the value of the event property.
     * 
     * @param value
     *     allowed object is
     *     {@link Event }
     *     
     */
    public void setEvent(Event value) {
        this.event = value;
    }

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link Condition }
     *     
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link Condition }
     *     
     */
    public void setCondition(Condition value) {
        this.condition = value;
    }

    /**
     * Gets the value of the actions property.
     * 
     * @return
     *     possible object is
     *     {@link Actions }
     *     
     */
    public Actions getActions() {
        return actions;
    }

    /**
     * Sets the value of the actions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Actions }
     *     
     */
    public void setActions(Actions value) {
        this.actions = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Event theEvent;
            theEvent = this.getEvent();
            strategy.appendField(locator, this, "event", buffer, theEvent);
        }
        {
            Condition theCondition;
            theCondition = this.getCondition();
            strategy.appendField(locator, this, "condition", buffer, theCondition);
        }
        {
            Actions theActions;
            theActions = this.getActions();
            strategy.appendField(locator, this, "actions", buffer, theActions);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Rule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Rule that = ((Rule) object);
        {
            Event lhsEvent;
            lhsEvent = this.getEvent();
            Event rhsEvent;
            rhsEvent = that.getEvent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "event", lhsEvent), LocatorUtils.property(thatLocator, "event", rhsEvent), lhsEvent, rhsEvent)) {
                return false;
            }
        }
        {
            Condition lhsCondition;
            lhsCondition = this.getCondition();
            Condition rhsCondition;
            rhsCondition = that.getCondition();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "condition", lhsCondition), LocatorUtils.property(thatLocator, "condition", rhsCondition), lhsCondition, rhsCondition)) {
                return false;
            }
        }
        {
            Actions lhsActions;
            lhsActions = this.getActions();
            Actions rhsActions;
            rhsActions = that.getActions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actions", lhsActions), LocatorUtils.property(thatLocator, "actions", rhsActions), lhsActions, rhsActions)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Event theEvent;
            theEvent = this.getEvent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "event", theEvent), currentHashCode, theEvent);
        }
        {
            Condition theCondition;
            theCondition = this.getCondition();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "condition", theCondition), currentHashCode, theCondition);
        }
        {
            Actions theActions;
            theActions = this.getActions();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "actions", theActions), currentHashCode, theActions);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Rule) {
            final Rule copy = ((Rule) draftCopy);
            if (this.event!= null) {
                Event sourceEvent;
                sourceEvent = this.getEvent();
                Event copyEvent = ((Event) strategy.copy(LocatorUtils.property(locator, "event", sourceEvent), sourceEvent));
                copy.setEvent(copyEvent);
            } else {
                copy.event = null;
            }
            if (this.condition!= null) {
                Condition sourceCondition;
                sourceCondition = this.getCondition();
                Condition copyCondition = ((Condition) strategy.copy(LocatorUtils.property(locator, "condition", sourceCondition), sourceCondition));
                copy.setCondition(copyCondition);
            } else {
                copy.condition = null;
            }
            if (this.actions!= null) {
                Actions sourceActions;
                sourceActions = this.getActions();
                Actions copyActions = ((Actions) strategy.copy(LocatorUtils.property(locator, "actions", sourceActions), sourceActions));
                copy.setActions(copyActions);
            } else {
                copy.actions = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Rule();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Rule.Builder<_B> _other) {
        _other.event = ((this.event == null)?null:this.event.newCopyBuilder(_other));
        _other.condition = ((this.condition == null)?null:this.condition.newCopyBuilder(_other));
        _other.actions = ((this.actions == null)?null:this.actions.newCopyBuilder(_other));
        _other.name = this.name;
    }

    public<_B >Rule.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Rule.Builder<_B>(_parentBuilder, this, true);
    }

    public Rule.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Rule.Builder<Void> builder() {
        return new Rule.Builder<Void>(null, null, false);
    }

    public static<_B >Rule.Builder<_B> copyOf(final Rule _other) {
        final Rule.Builder<_B> _newBuilder = new Rule.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Rule.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree eventPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("event"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventPropertyTree!= null):((eventPropertyTree == null)||(!eventPropertyTree.isLeaf())))) {
            _other.event = ((this.event == null)?null:this.event.newCopyBuilder(_other, eventPropertyTree, _propertyTreeUse));
        }
        final PropertyTree conditionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("condition"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(conditionPropertyTree!= null):((conditionPropertyTree == null)||(!conditionPropertyTree.isLeaf())))) {
            _other.condition = ((this.condition == null)?null:this.condition.newCopyBuilder(_other, conditionPropertyTree, _propertyTreeUse));
        }
        final PropertyTree actionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actions"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actionsPropertyTree!= null):((actionsPropertyTree == null)||(!actionsPropertyTree.isLeaf())))) {
            _other.actions = ((this.actions == null)?null:this.actions.newCopyBuilder(_other, actionsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
    }

    public<_B >Rule.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Rule.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Rule.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Rule.Builder<_B> copyOf(final Rule _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Rule.Builder<_B> _newBuilder = new Rule.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Rule.Builder<Void> copyExcept(final Rule _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Rule.Builder<Void> copyOnly(final Rule _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Rule _storedValue;
        private Event.Builder<Rule.Builder<_B>> event;
        private Condition.Builder<Rule.Builder<_B>> condition;
        private Actions.Builder<Rule.Builder<_B>> actions;
        private String name;

        public Builder(final _B _parentBuilder, final Rule _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.event = ((_other.event == null)?null:_other.event.newCopyBuilder(this));
                    this.condition = ((_other.condition == null)?null:_other.condition.newCopyBuilder(this));
                    this.actions = ((_other.actions == null)?null:_other.actions.newCopyBuilder(this));
                    this.name = _other.name;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Rule _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree eventPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("event"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventPropertyTree!= null):((eventPropertyTree == null)||(!eventPropertyTree.isLeaf())))) {
                        this.event = ((_other.event == null)?null:_other.event.newCopyBuilder(this, eventPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree conditionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("condition"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(conditionPropertyTree!= null):((conditionPropertyTree == null)||(!conditionPropertyTree.isLeaf())))) {
                        this.condition = ((_other.condition == null)?null:_other.condition.newCopyBuilder(this, conditionPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree actionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actions"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actionsPropertyTree!= null):((actionsPropertyTree == null)||(!actionsPropertyTree.isLeaf())))) {
                        this.actions = ((_other.actions == null)?null:_other.actions.newCopyBuilder(this, actionsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Rule >_P init(final _P _product) {
            _product.event = ((this.event == null)?null:this.event.build());
            _product.condition = ((this.condition == null)?null:this.condition.build());
            _product.actions = ((this.actions == null)?null:this.actions.build());
            _product.name = this.name;
            return _product;
        }

        /**
         * Sets the new value of "event" (any previous value will be replaced)
         * 
         * @param event
         *     New value of the "event" property.
         */
        public Rule.Builder<_B> withEvent(final Event event) {
            this.event = ((event == null)?null:new Event.Builder<Rule.Builder<_B>>(this, event, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "event" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Event.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "event" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Event.Builder#end()} to return to the current builder.
         */
        public Event.Builder<? extends Rule.Builder<_B>> withEvent() {
            if (this.event!= null) {
                return this.event;
            }
            return this.event = new Event.Builder<Rule.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "condition" (any previous value will be replaced)
         * 
         * @param condition
         *     New value of the "condition" property.
         */
        public Rule.Builder<_B> withCondition(final Condition condition) {
            this.condition = ((condition == null)?null:new Condition.Builder<Rule.Builder<_B>>(this, condition, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "condition" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Condition.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "condition" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Condition.Builder#end()} to return to the current builder.
         */
        public Condition.Builder<? extends Rule.Builder<_B>> withCondition() {
            if (this.condition!= null) {
                return this.condition;
            }
            return this.condition = new Condition.Builder<Rule.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "actions" (any previous value will be replaced)
         * 
         * @param actions
         *     New value of the "actions" property.
         */
        public Rule.Builder<_B> withActions(final Actions actions) {
            this.actions = ((actions == null)?null:new Actions.Builder<Rule.Builder<_B>>(this, actions, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "actions" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Actions.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "actions" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Actions.Builder#end()} to return to the current builder.
         */
        public Actions.Builder<? extends Rule.Builder<_B>> withActions() {
            if (this.actions!= null) {
                return this.actions;
            }
            return this.actions = new Actions.Builder<Rule.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Rule.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        @Override
        public Rule build() {
            if (_storedValue == null) {
                return this.init(new Rule());
            } else {
                return ((Rule) _storedValue);
            }
        }

        public Rule.Builder<_B> copyOf(final Rule _other) {
            _other.copyTo(this);
            return this;
        }

        public Rule.Builder<_B> copyOf(final Rule.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Rule.Selector<Rule.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Rule.Select _root() {
            return new Rule.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Event.Selector<TRoot, Rule.Selector<TRoot, TParent>> event = null;
        private Condition.Selector<TRoot, Rule.Selector<TRoot, TParent>> condition = null;
        private Actions.Selector<TRoot, Rule.Selector<TRoot, TParent>> actions = null;
        private com.kscs.util.jaxb.Selector<TRoot, Rule.Selector<TRoot, TParent>> name = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.event!= null) {
                products.put("event", this.event.init());
            }
            if (this.condition!= null) {
                products.put("condition", this.condition.init());
            }
            if (this.actions!= null) {
                products.put("actions", this.actions.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            return products;
        }

        public Event.Selector<TRoot, Rule.Selector<TRoot, TParent>> event() {
            return ((this.event == null)?this.event = new Event.Selector<TRoot, Rule.Selector<TRoot, TParent>>(this._root, this, "event"):this.event);
        }

        public Condition.Selector<TRoot, Rule.Selector<TRoot, TParent>> condition() {
            return ((this.condition == null)?this.condition = new Condition.Selector<TRoot, Rule.Selector<TRoot, TParent>>(this._root, this, "condition"):this.condition);
        }

        public Actions.Selector<TRoot, Rule.Selector<TRoot, TParent>> actions() {
            return ((this.actions == null)?this.actions = new Actions.Selector<TRoot, Rule.Selector<TRoot, TParent>>(this._root, this, "actions"):this.actions);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Rule.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Rule.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

    }

}
