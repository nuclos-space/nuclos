
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * The columns of a matrix component.
 * 
 * <p>Java class for web-matrix-column complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-matrix-column"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="controltype" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="controltypeclass" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="enabled" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="notcloneable" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="insertable" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="rows" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="columns" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="resourceId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="width" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="next-focus-component" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="next-focus-field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-matrix-column")
public class WebMatrixColumn implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "controltype")
    protected String controltype;
    @XmlAttribute(name = "controltypeclass")
    protected String controltypeclass;
    @XmlAttribute(name = "enabled")
    protected String enabled;
    @XmlAttribute(name = "notcloneable")
    protected String notcloneable;
    @XmlAttribute(name = "insertable")
    protected String insertable;
    @XmlAttribute(name = "rows")
    protected String rows;
    @XmlAttribute(name = "columns")
    protected String columns;
    @XmlAttribute(name = "resourceId")
    protected String resourceId;
    @XmlAttribute(name = "width")
    protected String width;
    @XmlAttribute(name = "next-focus-component")
    protected String nextFocusComponent;
    @XmlAttribute(name = "next-focus-field")
    protected String nextFocusField;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the controltype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControltype() {
        return controltype;
    }

    /**
     * Sets the value of the controltype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControltype(String value) {
        this.controltype = value;
    }

    /**
     * Gets the value of the controltypeclass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControltypeclass() {
        return controltypeclass;
    }

    /**
     * Sets the value of the controltypeclass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControltypeclass(String value) {
        this.controltypeclass = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnabled(String value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the notcloneable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotcloneable() {
        return notcloneable;
    }

    /**
     * Sets the value of the notcloneable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotcloneable(String value) {
        this.notcloneable = value;
    }

    /**
     * Gets the value of the insertable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsertable() {
        return insertable;
    }

    /**
     * Sets the value of the insertable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsertable(String value) {
        this.insertable = value;
    }

    /**
     * Gets the value of the rows property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRows(String value) {
        this.rows = value;
    }

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumns(String value) {
        this.columns = value;
    }

    /**
     * Gets the value of the resourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidth(String value) {
        this.width = value;
    }

    /**
     * Gets the value of the nextFocusComponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFocusComponent() {
        return nextFocusComponent;
    }

    /**
     * Sets the value of the nextFocusComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFocusComponent(String value) {
        this.nextFocusComponent = value;
    }

    /**
     * Gets the value of the nextFocusField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFocusField() {
        return nextFocusField;
    }

    /**
     * Sets the value of the nextFocusField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFocusField(String value) {
        this.nextFocusField = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theControltype;
            theControltype = this.getControltype();
            strategy.appendField(locator, this, "controltype", buffer, theControltype);
        }
        {
            String theControltypeclass;
            theControltypeclass = this.getControltypeclass();
            strategy.appendField(locator, this, "controltypeclass", buffer, theControltypeclass);
        }
        {
            String theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            String theNotcloneable;
            theNotcloneable = this.getNotcloneable();
            strategy.appendField(locator, this, "notcloneable", buffer, theNotcloneable);
        }
        {
            String theInsertable;
            theInsertable = this.getInsertable();
            strategy.appendField(locator, this, "insertable", buffer, theInsertable);
        }
        {
            String theRows;
            theRows = this.getRows();
            strategy.appendField(locator, this, "rows", buffer, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            strategy.appendField(locator, this, "columns", buffer, theColumns);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            strategy.appendField(locator, this, "resourceId", buffer, theResourceId);
        }
        {
            String theWidth;
            theWidth = this.getWidth();
            strategy.appendField(locator, this, "width", buffer, theWidth);
        }
        {
            String theNextFocusComponent;
            theNextFocusComponent = this.getNextFocusComponent();
            strategy.appendField(locator, this, "nextFocusComponent", buffer, theNextFocusComponent);
        }
        {
            String theNextFocusField;
            theNextFocusField = this.getNextFocusField();
            strategy.appendField(locator, this, "nextFocusField", buffer, theNextFocusField);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebMatrixColumn)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebMatrixColumn that = ((WebMatrixColumn) object);
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsControltype;
            lhsControltype = this.getControltype();
            String rhsControltype;
            rhsControltype = that.getControltype();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controltype", lhsControltype), LocatorUtils.property(thatLocator, "controltype", rhsControltype), lhsControltype, rhsControltype)) {
                return false;
            }
        }
        {
            String lhsControltypeclass;
            lhsControltypeclass = this.getControltypeclass();
            String rhsControltypeclass;
            rhsControltypeclass = that.getControltypeclass();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "controltypeclass", lhsControltypeclass), LocatorUtils.property(thatLocator, "controltypeclass", rhsControltypeclass), lhsControltypeclass, rhsControltypeclass)) {
                return false;
            }
        }
        {
            String lhsEnabled;
            lhsEnabled = this.getEnabled();
            String rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            String lhsNotcloneable;
            lhsNotcloneable = this.getNotcloneable();
            String rhsNotcloneable;
            rhsNotcloneable = that.getNotcloneable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "notcloneable", lhsNotcloneable), LocatorUtils.property(thatLocator, "notcloneable", rhsNotcloneable), lhsNotcloneable, rhsNotcloneable)) {
                return false;
            }
        }
        {
            String lhsInsertable;
            lhsInsertable = this.getInsertable();
            String rhsInsertable;
            rhsInsertable = that.getInsertable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "insertable", lhsInsertable), LocatorUtils.property(thatLocator, "insertable", rhsInsertable), lhsInsertable, rhsInsertable)) {
                return false;
            }
        }
        {
            String lhsRows;
            lhsRows = this.getRows();
            String rhsRows;
            rhsRows = that.getRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        {
            String lhsColumns;
            lhsColumns = this.getColumns();
            String rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            String lhsResourceId;
            lhsResourceId = this.getResourceId();
            String rhsResourceId;
            rhsResourceId = that.getResourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceId", lhsResourceId), LocatorUtils.property(thatLocator, "resourceId", rhsResourceId), lhsResourceId, rhsResourceId)) {
                return false;
            }
        }
        {
            String lhsWidth;
            lhsWidth = this.getWidth();
            String rhsWidth;
            rhsWidth = that.getWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "width", lhsWidth), LocatorUtils.property(thatLocator, "width", rhsWidth), lhsWidth, rhsWidth)) {
                return false;
            }
        }
        {
            String lhsNextFocusComponent;
            lhsNextFocusComponent = this.getNextFocusComponent();
            String rhsNextFocusComponent;
            rhsNextFocusComponent = that.getNextFocusComponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextFocusComponent", lhsNextFocusComponent), LocatorUtils.property(thatLocator, "nextFocusComponent", rhsNextFocusComponent), lhsNextFocusComponent, rhsNextFocusComponent)) {
                return false;
            }
        }
        {
            String lhsNextFocusField;
            lhsNextFocusField = this.getNextFocusField();
            String rhsNextFocusField;
            rhsNextFocusField = that.getNextFocusField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextFocusField", lhsNextFocusField), LocatorUtils.property(thatLocator, "nextFocusField", rhsNextFocusField), lhsNextFocusField, rhsNextFocusField)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theControltype;
            theControltype = this.getControltype();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controltype", theControltype), currentHashCode, theControltype);
        }
        {
            String theControltypeclass;
            theControltypeclass = this.getControltypeclass();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "controltypeclass", theControltypeclass), currentHashCode, theControltypeclass);
        }
        {
            String theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            String theNotcloneable;
            theNotcloneable = this.getNotcloneable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "notcloneable", theNotcloneable), currentHashCode, theNotcloneable);
        }
        {
            String theInsertable;
            theInsertable = this.getInsertable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "insertable", theInsertable), currentHashCode, theInsertable);
        }
        {
            String theRows;
            theRows = this.getRows();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rows", theRows), currentHashCode, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "columns", theColumns), currentHashCode, theColumns);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceId", theResourceId), currentHashCode, theResourceId);
        }
        {
            String theWidth;
            theWidth = this.getWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "width", theWidth), currentHashCode, theWidth);
        }
        {
            String theNextFocusComponent;
            theNextFocusComponent = this.getNextFocusComponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextFocusComponent", theNextFocusComponent), currentHashCode, theNextFocusComponent);
        }
        {
            String theNextFocusField;
            theNextFocusField = this.getNextFocusField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextFocusField", theNextFocusField), currentHashCode, theNextFocusField);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebMatrixColumn) {
            final WebMatrixColumn copy = ((WebMatrixColumn) draftCopy);
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.controltype!= null) {
                String sourceControltype;
                sourceControltype = this.getControltype();
                String copyControltype = ((String) strategy.copy(LocatorUtils.property(locator, "controltype", sourceControltype), sourceControltype));
                copy.setControltype(copyControltype);
            } else {
                copy.controltype = null;
            }
            if (this.controltypeclass!= null) {
                String sourceControltypeclass;
                sourceControltypeclass = this.getControltypeclass();
                String copyControltypeclass = ((String) strategy.copy(LocatorUtils.property(locator, "controltypeclass", sourceControltypeclass), sourceControltypeclass));
                copy.setControltypeclass(copyControltypeclass);
            } else {
                copy.controltypeclass = null;
            }
            if (this.enabled!= null) {
                String sourceEnabled;
                sourceEnabled = this.getEnabled();
                String copyEnabled = ((String) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.notcloneable!= null) {
                String sourceNotcloneable;
                sourceNotcloneable = this.getNotcloneable();
                String copyNotcloneable = ((String) strategy.copy(LocatorUtils.property(locator, "notcloneable", sourceNotcloneable), sourceNotcloneable));
                copy.setNotcloneable(copyNotcloneable);
            } else {
                copy.notcloneable = null;
            }
            if (this.insertable!= null) {
                String sourceInsertable;
                sourceInsertable = this.getInsertable();
                String copyInsertable = ((String) strategy.copy(LocatorUtils.property(locator, "insertable", sourceInsertable), sourceInsertable));
                copy.setInsertable(copyInsertable);
            } else {
                copy.insertable = null;
            }
            if (this.rows!= null) {
                String sourceRows;
                sourceRows = this.getRows();
                String copyRows = ((String) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.setRows(copyRows);
            } else {
                copy.rows = null;
            }
            if (this.columns!= null) {
                String sourceColumns;
                sourceColumns = this.getColumns();
                String copyColumns = ((String) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.resourceId!= null) {
                String sourceResourceId;
                sourceResourceId = this.getResourceId();
                String copyResourceId = ((String) strategy.copy(LocatorUtils.property(locator, "resourceId", sourceResourceId), sourceResourceId));
                copy.setResourceId(copyResourceId);
            } else {
                copy.resourceId = null;
            }
            if (this.width!= null) {
                String sourceWidth;
                sourceWidth = this.getWidth();
                String copyWidth = ((String) strategy.copy(LocatorUtils.property(locator, "width", sourceWidth), sourceWidth));
                copy.setWidth(copyWidth);
            } else {
                copy.width = null;
            }
            if (this.nextFocusComponent!= null) {
                String sourceNextFocusComponent;
                sourceNextFocusComponent = this.getNextFocusComponent();
                String copyNextFocusComponent = ((String) strategy.copy(LocatorUtils.property(locator, "nextFocusComponent", sourceNextFocusComponent), sourceNextFocusComponent));
                copy.setNextFocusComponent(copyNextFocusComponent);
            } else {
                copy.nextFocusComponent = null;
            }
            if (this.nextFocusField!= null) {
                String sourceNextFocusField;
                sourceNextFocusField = this.getNextFocusField();
                String copyNextFocusField = ((String) strategy.copy(LocatorUtils.property(locator, "nextFocusField", sourceNextFocusField), sourceNextFocusField));
                copy.setNextFocusField(copyNextFocusField);
            } else {
                copy.nextFocusField = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebMatrixColumn();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebMatrixColumn.Builder<_B> _other) {
        _other.name = this.name;
        _other.label = this.label;
        _other.controltype = this.controltype;
        _other.controltypeclass = this.controltypeclass;
        _other.enabled = this.enabled;
        _other.notcloneable = this.notcloneable;
        _other.insertable = this.insertable;
        _other.rows = this.rows;
        _other.columns = this.columns;
        _other.resourceId = this.resourceId;
        _other.width = this.width;
        _other.nextFocusComponent = this.nextFocusComponent;
        _other.nextFocusField = this.nextFocusField;
    }

    public<_B >WebMatrixColumn.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebMatrixColumn.Builder<_B>(_parentBuilder, this, true);
    }

    public WebMatrixColumn.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebMatrixColumn.Builder<Void> builder() {
        return new WebMatrixColumn.Builder<Void>(null, null, false);
    }

    public static<_B >WebMatrixColumn.Builder<_B> copyOf(final WebMatrixColumn _other) {
        final WebMatrixColumn.Builder<_B> _newBuilder = new WebMatrixColumn.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebMatrixColumn.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree controltypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltype"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypePropertyTree!= null):((controltypePropertyTree == null)||(!controltypePropertyTree.isLeaf())))) {
            _other.controltype = this.controltype;
        }
        final PropertyTree controltypeclassPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltypeclass"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypeclassPropertyTree!= null):((controltypeclassPropertyTree == null)||(!controltypeclassPropertyTree.isLeaf())))) {
            _other.controltypeclass = this.controltypeclass;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree notcloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notcloneable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notcloneablePropertyTree!= null):((notcloneablePropertyTree == null)||(!notcloneablePropertyTree.isLeaf())))) {
            _other.notcloneable = this.notcloneable;
        }
        final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
            _other.insertable = this.insertable;
        }
        final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
            _other.rows = this.rows;
        }
        final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
            _other.columns = this.columns;
        }
        final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
            _other.resourceId = this.resourceId;
        }
        final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
            _other.width = this.width;
        }
        final PropertyTree nextFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusComponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusComponentPropertyTree!= null):((nextFocusComponentPropertyTree == null)||(!nextFocusComponentPropertyTree.isLeaf())))) {
            _other.nextFocusComponent = this.nextFocusComponent;
        }
        final PropertyTree nextFocusFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusFieldPropertyTree!= null):((nextFocusFieldPropertyTree == null)||(!nextFocusFieldPropertyTree.isLeaf())))) {
            _other.nextFocusField = this.nextFocusField;
        }
    }

    public<_B >WebMatrixColumn.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebMatrixColumn.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebMatrixColumn.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebMatrixColumn.Builder<_B> copyOf(final WebMatrixColumn _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebMatrixColumn.Builder<_B> _newBuilder = new WebMatrixColumn.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebMatrixColumn.Builder<Void> copyExcept(final WebMatrixColumn _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebMatrixColumn.Builder<Void> copyOnly(final WebMatrixColumn _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebMatrixColumn _storedValue;
        private String name;
        private String label;
        private String controltype;
        private String controltypeclass;
        private String enabled;
        private String notcloneable;
        private String insertable;
        private String rows;
        private String columns;
        private String resourceId;
        private String width;
        private String nextFocusComponent;
        private String nextFocusField;

        public Builder(final _B _parentBuilder, final WebMatrixColumn _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.name = _other.name;
                    this.label = _other.label;
                    this.controltype = _other.controltype;
                    this.controltypeclass = _other.controltypeclass;
                    this.enabled = _other.enabled;
                    this.notcloneable = _other.notcloneable;
                    this.insertable = _other.insertable;
                    this.rows = _other.rows;
                    this.columns = _other.columns;
                    this.resourceId = _other.resourceId;
                    this.width = _other.width;
                    this.nextFocusComponent = _other.nextFocusComponent;
                    this.nextFocusField = _other.nextFocusField;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebMatrixColumn _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree controltypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltype"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypePropertyTree!= null):((controltypePropertyTree == null)||(!controltypePropertyTree.isLeaf())))) {
                        this.controltype = _other.controltype;
                    }
                    final PropertyTree controltypeclassPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("controltypeclass"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(controltypeclassPropertyTree!= null):((controltypeclassPropertyTree == null)||(!controltypeclassPropertyTree.isLeaf())))) {
                        this.controltypeclass = _other.controltypeclass;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree notcloneablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("notcloneable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(notcloneablePropertyTree!= null):((notcloneablePropertyTree == null)||(!notcloneablePropertyTree.isLeaf())))) {
                        this.notcloneable = _other.notcloneable;
                    }
                    final PropertyTree insertablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("insertable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(insertablePropertyTree!= null):((insertablePropertyTree == null)||(!insertablePropertyTree.isLeaf())))) {
                        this.insertable = _other.insertable;
                    }
                    final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
                        this.rows = _other.rows;
                    }
                    final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
                        this.columns = _other.columns;
                    }
                    final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
                        this.resourceId = _other.resourceId;
                    }
                    final PropertyTree widthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("width"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(widthPropertyTree!= null):((widthPropertyTree == null)||(!widthPropertyTree.isLeaf())))) {
                        this.width = _other.width;
                    }
                    final PropertyTree nextFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusComponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusComponentPropertyTree!= null):((nextFocusComponentPropertyTree == null)||(!nextFocusComponentPropertyTree.isLeaf())))) {
                        this.nextFocusComponent = _other.nextFocusComponent;
                    }
                    final PropertyTree nextFocusFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusFieldPropertyTree!= null):((nextFocusFieldPropertyTree == null)||(!nextFocusFieldPropertyTree.isLeaf())))) {
                        this.nextFocusField = _other.nextFocusField;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebMatrixColumn >_P init(final _P _product) {
            _product.name = this.name;
            _product.label = this.label;
            _product.controltype = this.controltype;
            _product.controltypeclass = this.controltypeclass;
            _product.enabled = this.enabled;
            _product.notcloneable = this.notcloneable;
            _product.insertable = this.insertable;
            _product.rows = this.rows;
            _product.columns = this.columns;
            _product.resourceId = this.resourceId;
            _product.width = this.width;
            _product.nextFocusComponent = this.nextFocusComponent;
            _product.nextFocusField = this.nextFocusField;
            return _product;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public WebMatrixColumn.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public WebMatrixColumn.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "controltype" (any previous value will be replaced)
         * 
         * @param controltype
         *     New value of the "controltype" property.
         */
        public WebMatrixColumn.Builder<_B> withControltype(final String controltype) {
            this.controltype = controltype;
            return this;
        }

        /**
         * Sets the new value of "controltypeclass" (any previous value will be replaced)
         * 
         * @param controltypeclass
         *     New value of the "controltypeclass" property.
         */
        public WebMatrixColumn.Builder<_B> withControltypeclass(final String controltypeclass) {
            this.controltypeclass = controltypeclass;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public WebMatrixColumn.Builder<_B> withEnabled(final String enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "notcloneable" (any previous value will be replaced)
         * 
         * @param notcloneable
         *     New value of the "notcloneable" property.
         */
        public WebMatrixColumn.Builder<_B> withNotcloneable(final String notcloneable) {
            this.notcloneable = notcloneable;
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        public WebMatrixColumn.Builder<_B> withInsertable(final String insertable) {
            this.insertable = insertable;
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public WebMatrixColumn.Builder<_B> withRows(final String rows) {
            this.rows = rows;
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        public WebMatrixColumn.Builder<_B> withColumns(final String columns) {
            this.columns = columns;
            return this;
        }

        /**
         * Sets the new value of "resourceId" (any previous value will be replaced)
         * 
         * @param resourceId
         *     New value of the "resourceId" property.
         */
        public WebMatrixColumn.Builder<_B> withResourceId(final String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        /**
         * Sets the new value of "width" (any previous value will be replaced)
         * 
         * @param width
         *     New value of the "width" property.
         */
        public WebMatrixColumn.Builder<_B> withWidth(final String width) {
            this.width = width;
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        public WebMatrixColumn.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            this.nextFocusComponent = nextFocusComponent;
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        public WebMatrixColumn.Builder<_B> withNextFocusField(final String nextFocusField) {
            this.nextFocusField = nextFocusField;
            return this;
        }

        @Override
        public WebMatrixColumn build() {
            if (_storedValue == null) {
                return this.init(new WebMatrixColumn());
            } else {
                return ((WebMatrixColumn) _storedValue);
            }
        }

        public WebMatrixColumn.Builder<_B> copyOf(final WebMatrixColumn _other) {
            _other.copyTo(this);
            return this;
        }

        public WebMatrixColumn.Builder<_B> copyOf(final WebMatrixColumn.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebMatrixColumn.Selector<WebMatrixColumn.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebMatrixColumn.Select _root() {
            return new WebMatrixColumn.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> controltype = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> controltypeclass = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> notcloneable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> insertable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> rows = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> columns = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> resourceId = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> width = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> nextFocusComponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> nextFocusField = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.controltype!= null) {
                products.put("controltype", this.controltype.init());
            }
            if (this.controltypeclass!= null) {
                products.put("controltypeclass", this.controltypeclass.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.notcloneable!= null) {
                products.put("notcloneable", this.notcloneable.init());
            }
            if (this.insertable!= null) {
                products.put("insertable", this.insertable.init());
            }
            if (this.rows!= null) {
                products.put("rows", this.rows.init());
            }
            if (this.columns!= null) {
                products.put("columns", this.columns.init());
            }
            if (this.resourceId!= null) {
                products.put("resourceId", this.resourceId.init());
            }
            if (this.width!= null) {
                products.put("width", this.width.init());
            }
            if (this.nextFocusComponent!= null) {
                products.put("nextFocusComponent", this.nextFocusComponent.init());
            }
            if (this.nextFocusField!= null) {
                products.put("nextFocusField", this.nextFocusField.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> controltype() {
            return ((this.controltype == null)?this.controltype = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "controltype"):this.controltype);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> controltypeclass() {
            return ((this.controltypeclass == null)?this.controltypeclass = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "controltypeclass"):this.controltypeclass);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> notcloneable() {
            return ((this.notcloneable == null)?this.notcloneable = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "notcloneable"):this.notcloneable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> insertable() {
            return ((this.insertable == null)?this.insertable = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "insertable"):this.insertable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> rows() {
            return ((this.rows == null)?this.rows = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "rows"):this.rows);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> columns() {
            return ((this.columns == null)?this.columns = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "columns"):this.columns);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> resourceId() {
            return ((this.resourceId == null)?this.resourceId = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "resourceId"):this.resourceId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> width() {
            return ((this.width == null)?this.width = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "width"):this.width);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> nextFocusComponent() {
            return ((this.nextFocusComponent == null)?this.nextFocusComponent = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "nextFocusComponent"):this.nextFocusComponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>> nextFocusField() {
            return ((this.nextFocusField == null)?this.nextFocusField = new com.kscs.util.jaxb.Selector<TRoot, WebMatrixColumn.Selector<TRoot, TParent>>(this._root, this, "nextFocusField"):this.nextFocusField);
        }

    }

}
