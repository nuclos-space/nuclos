
package org.nuclos.schema.layout.rule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Base type for any rules.
 * 
 * <p>Java class for rule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rule"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="event" type="{urn:org.nuclos.schema.layout.rule}rule-event"/&gt;
 *         &lt;element name="actions" type="{urn:org.nuclos.schema.layout.rule}rule-action" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rule", propOrder = {
    "event",
    "actions"
})
public class Rule implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RuleEvent event;
    @XmlElement(required = true)
    protected List<RuleAction> actions;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;

    /**
     * Gets the value of the event property.
     * 
     * @return
     *     possible object is
     *     {@link RuleEvent }
     *     
     */
    public RuleEvent getEvent() {
        return event;
    }

    /**
     * Sets the value of the event property.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleEvent }
     *     
     */
    public void setEvent(RuleEvent value) {
        this.event = value;
    }

    /**
     * Gets the value of the actions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RuleAction }
     * 
     * 
     */
    public List<RuleAction> getActions() {
        if (actions == null) {
            actions = new ArrayList<RuleAction>();
        }
        return this.actions;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RuleEvent theEvent;
            theEvent = this.getEvent();
            strategy.appendField(locator, this, "event", buffer, theEvent);
        }
        {
            List<RuleAction> theActions;
            theActions = (((this.actions!= null)&&(!this.actions.isEmpty()))?this.getActions():null);
            strategy.appendField(locator, this, "actions", buffer, theActions);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Rule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Rule that = ((Rule) object);
        {
            RuleEvent lhsEvent;
            lhsEvent = this.getEvent();
            RuleEvent rhsEvent;
            rhsEvent = that.getEvent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "event", lhsEvent), LocatorUtils.property(thatLocator, "event", rhsEvent), lhsEvent, rhsEvent)) {
                return false;
            }
        }
        {
            List<RuleAction> lhsActions;
            lhsActions = (((this.actions!= null)&&(!this.actions.isEmpty()))?this.getActions():null);
            List<RuleAction> rhsActions;
            rhsActions = (((that.actions!= null)&&(!that.actions.isEmpty()))?that.getActions():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "actions", lhsActions), LocatorUtils.property(thatLocator, "actions", rhsActions), lhsActions, rhsActions)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RuleEvent theEvent;
            theEvent = this.getEvent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "event", theEvent), currentHashCode, theEvent);
        }
        {
            List<RuleAction> theActions;
            theActions = (((this.actions!= null)&&(!this.actions.isEmpty()))?this.getActions():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "actions", theActions), currentHashCode, theActions);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Rule) {
            final Rule copy = ((Rule) draftCopy);
            if (this.event!= null) {
                RuleEvent sourceEvent;
                sourceEvent = this.getEvent();
                RuleEvent copyEvent = ((RuleEvent) strategy.copy(LocatorUtils.property(locator, "event", sourceEvent), sourceEvent));
                copy.setEvent(copyEvent);
            } else {
                copy.event = null;
            }
            if ((this.actions!= null)&&(!this.actions.isEmpty())) {
                List<RuleAction> sourceActions;
                sourceActions = (((this.actions!= null)&&(!this.actions.isEmpty()))?this.getActions():null);
                @SuppressWarnings("unchecked")
                List<RuleAction> copyActions = ((List<RuleAction> ) strategy.copy(LocatorUtils.property(locator, "actions", sourceActions), sourceActions));
                copy.actions = null;
                if (copyActions!= null) {
                    List<RuleAction> uniqueActionsl = copy.getActions();
                    uniqueActionsl.addAll(copyActions);
                }
            } else {
                copy.actions = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Rule();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Rule.Builder<_B> _other) {
        _other.event = ((this.event == null)?null:this.event.newCopyBuilder(_other));
        if (this.actions == null) {
            _other.actions = null;
        } else {
            _other.actions = new ArrayList<RuleAction.Builder<Rule.Builder<_B>>>();
            for (RuleAction _item: this.actions) {
                _other.actions.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
    }

    public<_B >Rule.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Rule.Builder<_B>(_parentBuilder, this, true);
    }

    public Rule.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Rule.Builder<Void> builder() {
        return new Rule.Builder<Void>(null, null, false);
    }

    public static<_B >Rule.Builder<_B> copyOf(final Rule _other) {
        final Rule.Builder<_B> _newBuilder = new Rule.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Rule.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree eventPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("event"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventPropertyTree!= null):((eventPropertyTree == null)||(!eventPropertyTree.isLeaf())))) {
            _other.event = ((this.event == null)?null:this.event.newCopyBuilder(_other, eventPropertyTree, _propertyTreeUse));
        }
        final PropertyTree actionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actions"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actionsPropertyTree!= null):((actionsPropertyTree == null)||(!actionsPropertyTree.isLeaf())))) {
            if (this.actions == null) {
                _other.actions = null;
            } else {
                _other.actions = new ArrayList<RuleAction.Builder<Rule.Builder<_B>>>();
                for (RuleAction _item: this.actions) {
                    _other.actions.add(((_item == null)?null:_item.newCopyBuilder(_other, actionsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
    }

    public<_B >Rule.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Rule.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Rule.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Rule.Builder<_B> copyOf(final Rule _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Rule.Builder<_B> _newBuilder = new Rule.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Rule.Builder<Void> copyExcept(final Rule _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Rule.Builder<Void> copyOnly(final Rule _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Rule _storedValue;
        private RuleEvent.Builder<Rule.Builder<_B>> event;
        private List<RuleAction.Builder<Rule.Builder<_B>>> actions;
        private String name;

        public Builder(final _B _parentBuilder, final Rule _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.event = ((_other.event == null)?null:_other.event.newCopyBuilder(this));
                    if (_other.actions == null) {
                        this.actions = null;
                    } else {
                        this.actions = new ArrayList<RuleAction.Builder<Rule.Builder<_B>>>();
                        for (RuleAction _item: _other.actions) {
                            this.actions.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Rule _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree eventPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("event"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventPropertyTree!= null):((eventPropertyTree == null)||(!eventPropertyTree.isLeaf())))) {
                        this.event = ((_other.event == null)?null:_other.event.newCopyBuilder(this, eventPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree actionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("actions"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(actionsPropertyTree!= null):((actionsPropertyTree == null)||(!actionsPropertyTree.isLeaf())))) {
                        if (_other.actions == null) {
                            this.actions = null;
                        } else {
                            this.actions = new ArrayList<RuleAction.Builder<Rule.Builder<_B>>>();
                            for (RuleAction _item: _other.actions) {
                                this.actions.add(((_item == null)?null:_item.newCopyBuilder(this, actionsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Rule >_P init(final _P _product) {
            _product.event = ((this.event == null)?null:this.event.build());
            if (this.actions!= null) {
                final List<RuleAction> actions = new ArrayList<RuleAction>(this.actions.size());
                for (RuleAction.Builder<Rule.Builder<_B>> _item: this.actions) {
                    actions.add(_item.build());
                }
                _product.actions = actions;
            }
            _product.name = this.name;
            return _product;
        }

        /**
         * Sets the new value of "event" (any previous value will be replaced)
         * 
         * @param event
         *     New value of the "event" property.
         */
        public Rule.Builder<_B> withEvent(final RuleEvent event) {
            this.event = ((event == null)?null:new RuleEvent.Builder<Rule.Builder<_B>>(this, event, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "event" property.
         * Use {@link org.nuclos.schema.layout.rule.RuleEvent.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "event" property.
         *     Use {@link org.nuclos.schema.layout.rule.RuleEvent.Builder#end()} to return to the current builder.
         */
        public RuleEvent.Builder<? extends Rule.Builder<_B>> withEvent() {
            if (this.event!= null) {
                return this.event;
            }
            return this.event = new RuleEvent.Builder<Rule.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "actions"
         * 
         * @param actions
         *     Items to add to the value of the "actions" property
         */
        public Rule.Builder<_B> addActions(final Iterable<? extends RuleAction> actions) {
            if (actions!= null) {
                if (this.actions == null) {
                    this.actions = new ArrayList<RuleAction.Builder<Rule.Builder<_B>>>();
                }
                for (RuleAction _item: actions) {
                    this.actions.add(new RuleAction.Builder<Rule.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "actions" (any previous value will be replaced)
         * 
         * @param actions
         *     New value of the "actions" property.
         */
        public Rule.Builder<_B> withActions(final Iterable<? extends RuleAction> actions) {
            if (this.actions!= null) {
                this.actions.clear();
            }
            return addActions(actions);
        }

        /**
         * Adds the given items to the value of "actions"
         * 
         * @param actions
         *     Items to add to the value of the "actions" property
         */
        public Rule.Builder<_B> addActions(RuleAction... actions) {
            addActions(Arrays.asList(actions));
            return this;
        }

        /**
         * Sets the new value of "actions" (any previous value will be replaced)
         * 
         * @param actions
         *     New value of the "actions" property.
         */
        public Rule.Builder<_B> withActions(RuleAction... actions) {
            withActions(Arrays.asList(actions));
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Rule.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        @Override
        public Rule build() {
            if (_storedValue == null) {
                return this.init(new Rule());
            } else {
                return ((Rule) _storedValue);
            }
        }

        public Rule.Builder<_B> copyOf(final Rule _other) {
            _other.copyTo(this);
            return this;
        }

        public Rule.Builder<_B> copyOf(final Rule.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Rule.Selector<Rule.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Rule.Select _root() {
            return new Rule.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RuleEvent.Selector<TRoot, Rule.Selector<TRoot, TParent>> event = null;
        private RuleAction.Selector<TRoot, Rule.Selector<TRoot, TParent>> actions = null;
        private com.kscs.util.jaxb.Selector<TRoot, Rule.Selector<TRoot, TParent>> name = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.event!= null) {
                products.put("event", this.event.init());
            }
            if (this.actions!= null) {
                products.put("actions", this.actions.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            return products;
        }

        public RuleEvent.Selector<TRoot, Rule.Selector<TRoot, TParent>> event() {
            return ((this.event == null)?this.event = new RuleEvent.Selector<TRoot, Rule.Selector<TRoot, TParent>>(this._root, this, "event"):this.event);
        }

        public RuleAction.Selector<TRoot, Rule.Selector<TRoot, TParent>> actions() {
            return ((this.actions == null)?this.actions = new RuleAction.Selector<TRoot, Rule.Selector<TRoot, TParent>>(this._root, this, "actions"):this.actions);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Rule.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Rule.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

    }

}
