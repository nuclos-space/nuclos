
package org.nuclos.schema.layout.rule;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * An action to be executed by the rule.
 * 
 * <p>Java class for rule-action complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rule-action"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="targetcomponent" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rule-action")
@XmlSeeAlso({
    RuleActionTransferLookedupValue.class,
    RuleActionReinitSubform.class,
    RuleActionClear.class,
    RuleActionEnable.class,
    RuleActionRefreshValuelist.class
})
public abstract class RuleAction implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "targetcomponent")
    protected String targetcomponent;

    /**
     * Gets the value of the targetcomponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetcomponent() {
        return targetcomponent;
    }

    /**
     * Sets the value of the targetcomponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetcomponent(String value) {
        this.targetcomponent = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theTargetcomponent;
            theTargetcomponent = this.getTargetcomponent();
            strategy.appendField(locator, this, "targetcomponent", buffer, theTargetcomponent);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RuleAction)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RuleAction that = ((RuleAction) object);
        {
            String lhsTargetcomponent;
            lhsTargetcomponent = this.getTargetcomponent();
            String rhsTargetcomponent;
            rhsTargetcomponent = that.getTargetcomponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetcomponent", lhsTargetcomponent), LocatorUtils.property(thatLocator, "targetcomponent", rhsTargetcomponent), lhsTargetcomponent, rhsTargetcomponent)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theTargetcomponent;
            theTargetcomponent = this.getTargetcomponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "targetcomponent", theTargetcomponent), currentHashCode, theTargetcomponent);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof RuleAction) {
            final RuleAction copy = ((RuleAction) target);
            if (this.targetcomponent!= null) {
                String sourceTargetcomponent;
                sourceTargetcomponent = this.getTargetcomponent();
                String copyTargetcomponent = ((String) strategy.copy(LocatorUtils.property(locator, "targetcomponent", sourceTargetcomponent), sourceTargetcomponent));
                copy.setTargetcomponent(copyTargetcomponent);
            } else {
                copy.targetcomponent = null;
            }
        }
        return target;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RuleAction.Builder<_B> _other) {
        _other.targetcomponent = this.targetcomponent;
    }

    public abstract<_B >RuleAction.Builder<_B> newCopyBuilder(final _B _parentBuilder);

    public abstract RuleAction.Builder<Void> newCopyBuilder();

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RuleAction.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree targetcomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetcomponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetcomponentPropertyTree!= null):((targetcomponentPropertyTree == null)||(!targetcomponentPropertyTree.isLeaf())))) {
            _other.targetcomponent = this.targetcomponent;
        }
    }

    public abstract<_B >RuleAction.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public abstract RuleAction.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RuleAction _storedValue;
        private String targetcomponent;

        public Builder(final _B _parentBuilder, final RuleAction _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.targetcomponent = _other.targetcomponent;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RuleAction _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree targetcomponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetcomponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetcomponentPropertyTree!= null):((targetcomponentPropertyTree == null)||(!targetcomponentPropertyTree.isLeaf())))) {
                        this.targetcomponent = _other.targetcomponent;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RuleAction >_P init(final _P _product) {
            _product.targetcomponent = this.targetcomponent;
            return _product;
        }

        /**
         * Sets the new value of "targetcomponent" (any previous value will be replaced)
         * 
         * @param targetcomponent
         *     New value of the "targetcomponent" property.
         */
        public RuleAction.Builder<_B> withTargetcomponent(final String targetcomponent) {
            this.targetcomponent = targetcomponent;
            return this;
        }

        @Override
        public RuleAction build() {
            return ((RuleAction) _storedValue);
        }

        public RuleAction.Builder<_B> copyOf(final RuleAction _other) {
            _other.copyTo(this);
            return this;
        }

        public RuleAction.Builder<_B> copyOf(final RuleAction.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RuleAction.Selector<RuleAction.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RuleAction.Select _root() {
            return new RuleAction.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RuleAction.Selector<TRoot, TParent>> targetcomponent = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.targetcomponent!= null) {
                products.put("targetcomponent", this.targetcomponent.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RuleAction.Selector<TRoot, TParent>> targetcomponent() {
            return ((this.targetcomponent == null)?this.targetcomponent = new com.kscs.util.jaxb.Selector<TRoot, RuleAction.Selector<TRoot, TParent>>(this._root, this, "targetcomponent"):this.targetcomponent);
        }

    }

}
