
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A container for a list of Tabs.
 * 
 * <p>Java class for web-tabcontainer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-tabcontainer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-input-component"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tabs" type="{urn:org.nuclos.schema.layout.web}web-tab" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="selected-index" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-tabcontainer", propOrder = {
    "tabs"
})
public class WebTabcontainer
    extends WebInputComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<WebTab> tabs;
    @XmlAttribute(name = "selected-index")
    protected BigInteger selectedIndex;

    /**
     * Gets the value of the tabs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tabs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTabs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebTab }
     * 
     * 
     */
    public List<WebTab> getTabs() {
        if (tabs == null) {
            tabs = new ArrayList<WebTab>();
        }
        return this.tabs;
    }

    /**
     * Gets the value of the selectedIndex property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSelectedIndex() {
        return selectedIndex;
    }

    /**
     * Sets the value of the selectedIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSelectedIndex(BigInteger value) {
        this.selectedIndex = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            List<WebTab> theTabs;
            theTabs = (((this.tabs!= null)&&(!this.tabs.isEmpty()))?this.getTabs():null);
            strategy.appendField(locator, this, "tabs", buffer, theTabs);
        }
        {
            BigInteger theSelectedIndex;
            theSelectedIndex = this.getSelectedIndex();
            strategy.appendField(locator, this, "selectedIndex", buffer, theSelectedIndex);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebTabcontainer)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebTabcontainer that = ((WebTabcontainer) object);
        {
            List<WebTab> lhsTabs;
            lhsTabs = (((this.tabs!= null)&&(!this.tabs.isEmpty()))?this.getTabs():null);
            List<WebTab> rhsTabs;
            rhsTabs = (((that.tabs!= null)&&(!that.tabs.isEmpty()))?that.getTabs():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tabs", lhsTabs), LocatorUtils.property(thatLocator, "tabs", rhsTabs), lhsTabs, rhsTabs)) {
                return false;
            }
        }
        {
            BigInteger lhsSelectedIndex;
            lhsSelectedIndex = this.getSelectedIndex();
            BigInteger rhsSelectedIndex;
            rhsSelectedIndex = that.getSelectedIndex();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "selectedIndex", lhsSelectedIndex), LocatorUtils.property(thatLocator, "selectedIndex", rhsSelectedIndex), lhsSelectedIndex, rhsSelectedIndex)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            List<WebTab> theTabs;
            theTabs = (((this.tabs!= null)&&(!this.tabs.isEmpty()))?this.getTabs():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tabs", theTabs), currentHashCode, theTabs);
        }
        {
            BigInteger theSelectedIndex;
            theSelectedIndex = this.getSelectedIndex();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "selectedIndex", theSelectedIndex), currentHashCode, theSelectedIndex);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebTabcontainer) {
            final WebTabcontainer copy = ((WebTabcontainer) draftCopy);
            if ((this.tabs!= null)&&(!this.tabs.isEmpty())) {
                List<WebTab> sourceTabs;
                sourceTabs = (((this.tabs!= null)&&(!this.tabs.isEmpty()))?this.getTabs():null);
                @SuppressWarnings("unchecked")
                List<WebTab> copyTabs = ((List<WebTab> ) strategy.copy(LocatorUtils.property(locator, "tabs", sourceTabs), sourceTabs));
                copy.tabs = null;
                if (copyTabs!= null) {
                    List<WebTab> uniqueTabsl = copy.getTabs();
                    uniqueTabsl.addAll(copyTabs);
                }
            } else {
                copy.tabs = null;
            }
            if (this.selectedIndex!= null) {
                BigInteger sourceSelectedIndex;
                sourceSelectedIndex = this.getSelectedIndex();
                BigInteger copySelectedIndex = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "selectedIndex", sourceSelectedIndex), sourceSelectedIndex));
                copy.setSelectedIndex(copySelectedIndex);
            } else {
                copy.selectedIndex = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebTabcontainer();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebTabcontainer.Builder<_B> _other) {
        super.copyTo(_other);
        if (this.tabs == null) {
            _other.tabs = null;
        } else {
            _other.tabs = new ArrayList<WebTab.Builder<WebTabcontainer.Builder<_B>>>();
            for (WebTab _item: this.tabs) {
                _other.tabs.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.selectedIndex = this.selectedIndex;
    }

    @Override
    public<_B >WebTabcontainer.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebTabcontainer.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebTabcontainer.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebTabcontainer.Builder<Void> builder() {
        return new WebTabcontainer.Builder<Void>(null, null, false);
    }

    public static<_B >WebTabcontainer.Builder<_B> copyOf(final WebComponent _other) {
        final WebTabcontainer.Builder<_B> _newBuilder = new WebTabcontainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebTabcontainer.Builder<_B> copyOf(final WebInputComponent _other) {
        final WebTabcontainer.Builder<_B> _newBuilder = new WebTabcontainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebTabcontainer.Builder<_B> copyOf(final WebTabcontainer _other) {
        final WebTabcontainer.Builder<_B> _newBuilder = new WebTabcontainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebTabcontainer.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree tabsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tabs"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tabsPropertyTree!= null):((tabsPropertyTree == null)||(!tabsPropertyTree.isLeaf())))) {
            if (this.tabs == null) {
                _other.tabs = null;
            } else {
                _other.tabs = new ArrayList<WebTab.Builder<WebTabcontainer.Builder<_B>>>();
                for (WebTab _item: this.tabs) {
                    _other.tabs.add(((_item == null)?null:_item.newCopyBuilder(_other, tabsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree selectedIndexPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("selectedIndex"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selectedIndexPropertyTree!= null):((selectedIndexPropertyTree == null)||(!selectedIndexPropertyTree.isLeaf())))) {
            _other.selectedIndex = this.selectedIndex;
        }
    }

    @Override
    public<_B >WebTabcontainer.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebTabcontainer.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebTabcontainer.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebTabcontainer.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebTabcontainer.Builder<_B> _newBuilder = new WebTabcontainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebTabcontainer.Builder<_B> copyOf(final WebInputComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebTabcontainer.Builder<_B> _newBuilder = new WebTabcontainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebTabcontainer.Builder<_B> copyOf(final WebTabcontainer _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebTabcontainer.Builder<_B> _newBuilder = new WebTabcontainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebTabcontainer.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebTabcontainer.Builder<Void> copyExcept(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebTabcontainer.Builder<Void> copyExcept(final WebTabcontainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebTabcontainer.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebTabcontainer.Builder<Void> copyOnly(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebTabcontainer.Builder<Void> copyOnly(final WebTabcontainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebInputComponent.Builder<_B>
        implements Buildable
    {

        private List<WebTab.Builder<WebTabcontainer.Builder<_B>>> tabs;
        private BigInteger selectedIndex;

        public Builder(final _B _parentBuilder, final WebTabcontainer _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                if (_other.tabs == null) {
                    this.tabs = null;
                } else {
                    this.tabs = new ArrayList<WebTab.Builder<WebTabcontainer.Builder<_B>>>();
                    for (WebTab _item: _other.tabs) {
                        this.tabs.add(((_item == null)?null:_item.newCopyBuilder(this)));
                    }
                }
                this.selectedIndex = _other.selectedIndex;
            }
        }

        public Builder(final _B _parentBuilder, final WebTabcontainer _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree tabsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tabs"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tabsPropertyTree!= null):((tabsPropertyTree == null)||(!tabsPropertyTree.isLeaf())))) {
                    if (_other.tabs == null) {
                        this.tabs = null;
                    } else {
                        this.tabs = new ArrayList<WebTab.Builder<WebTabcontainer.Builder<_B>>>();
                        for (WebTab _item: _other.tabs) {
                            this.tabs.add(((_item == null)?null:_item.newCopyBuilder(this, tabsPropertyTree, _propertyTreeUse)));
                        }
                    }
                }
                final PropertyTree selectedIndexPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("selectedIndex"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selectedIndexPropertyTree!= null):((selectedIndexPropertyTree == null)||(!selectedIndexPropertyTree.isLeaf())))) {
                    this.selectedIndex = _other.selectedIndex;
                }
            }
        }

        protected<_P extends WebTabcontainer >_P init(final _P _product) {
            if (this.tabs!= null) {
                final List<WebTab> tabs = new ArrayList<WebTab>(this.tabs.size());
                for (WebTab.Builder<WebTabcontainer.Builder<_B>> _item: this.tabs) {
                    tabs.add(_item.build());
                }
                _product.tabs = tabs;
            }
            _product.selectedIndex = this.selectedIndex;
            return super.init(_product);
        }

        /**
         * Adds the given items to the value of "tabs"
         * 
         * @param tabs
         *     Items to add to the value of the "tabs" property
         */
        public WebTabcontainer.Builder<_B> addTabs(final Iterable<? extends WebTab> tabs) {
            if (tabs!= null) {
                if (this.tabs == null) {
                    this.tabs = new ArrayList<WebTab.Builder<WebTabcontainer.Builder<_B>>>();
                }
                for (WebTab _item: tabs) {
                    this.tabs.add(new WebTab.Builder<WebTabcontainer.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "tabs" (any previous value will be replaced)
         * 
         * @param tabs
         *     New value of the "tabs" property.
         */
        public WebTabcontainer.Builder<_B> withTabs(final Iterable<? extends WebTab> tabs) {
            if (this.tabs!= null) {
                this.tabs.clear();
            }
            return addTabs(tabs);
        }

        /**
         * Adds the given items to the value of "tabs"
         * 
         * @param tabs
         *     Items to add to the value of the "tabs" property
         */
        public WebTabcontainer.Builder<_B> addTabs(WebTab... tabs) {
            addTabs(Arrays.asList(tabs));
            return this;
        }

        /**
         * Sets the new value of "tabs" (any previous value will be replaced)
         * 
         * @param tabs
         *     New value of the "tabs" property.
         */
        public WebTabcontainer.Builder<_B> withTabs(WebTab... tabs) {
            withTabs(Arrays.asList(tabs));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Tabs" property.
         * Use {@link org.nuclos.schema.layout.web.WebTab.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Tabs" property.
         *     Use {@link org.nuclos.schema.layout.web.WebTab.Builder#end()} to return to the current builder.
         */
        public WebTab.Builder<? extends WebTabcontainer.Builder<_B>> addTabs() {
            if (this.tabs == null) {
                this.tabs = new ArrayList<WebTab.Builder<WebTabcontainer.Builder<_B>>>();
            }
            final WebTab.Builder<WebTabcontainer.Builder<_B>> tabs_Builder = new WebTab.Builder<WebTabcontainer.Builder<_B>>(this, null, false);
            this.tabs.add(tabs_Builder);
            return tabs_Builder;
        }

        /**
         * Sets the new value of "selectedIndex" (any previous value will be replaced)
         * 
         * @param selectedIndex
         *     New value of the "selectedIndex" property.
         */
        public WebTabcontainer.Builder<_B> withSelectedIndex(final BigInteger selectedIndex) {
            this.selectedIndex = selectedIndex;
            return this;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            super.withValuelistProvider(valuelistProvider);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebTabcontainer.Builder<_B>> withValuelistProvider() {
            return ((WebValuelistProvider.Builder<? extends WebTabcontainer.Builder<_B>> ) super.withValuelistProvider());
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withEnabled(final Boolean enabled) {
            super.withEnabled(enabled);
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withEditable(final Boolean editable) {
            super.withEditable(editable);
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withInsertable(final Boolean insertable) {
            super.withInsertable(insertable);
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withColumns(final String columns) {
            super.withColumns(columns);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebTabcontainer.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebTabcontainer.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebTabcontainer.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebTabcontainer.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebTabcontainer.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebTabcontainer build() {
            if (_storedValue == null) {
                return this.init(new WebTabcontainer());
            } else {
                return ((WebTabcontainer) _storedValue);
            }
        }

        public WebTabcontainer.Builder<_B> copyOf(final WebTabcontainer _other) {
            _other.copyTo(this);
            return this;
        }

        public WebTabcontainer.Builder<_B> copyOf(final WebTabcontainer.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebTabcontainer.Selector<WebTabcontainer.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebTabcontainer.Select _root() {
            return new WebTabcontainer.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebInputComponent.Selector<TRoot, TParent>
    {

        private WebTab.Selector<TRoot, WebTabcontainer.Selector<TRoot, TParent>> tabs = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebTabcontainer.Selector<TRoot, TParent>> selectedIndex = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.tabs!= null) {
                products.put("tabs", this.tabs.init());
            }
            if (this.selectedIndex!= null) {
                products.put("selectedIndex", this.selectedIndex.init());
            }
            return products;
        }

        public WebTab.Selector<TRoot, WebTabcontainer.Selector<TRoot, TParent>> tabs() {
            return ((this.tabs == null)?this.tabs = new WebTab.Selector<TRoot, WebTabcontainer.Selector<TRoot, TParent>>(this._root, this, "tabs"):this.tabs);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebTabcontainer.Selector<TRoot, TParent>> selectedIndex() {
            return ((this.selectedIndex == null)?this.selectedIndex = new com.kscs.util.jaxb.Selector<TRoot, WebTabcontainer.Selector<TRoot, TParent>>(this._root, this, "selectedIndex"):this.selectedIndex);
        }

    }

}
