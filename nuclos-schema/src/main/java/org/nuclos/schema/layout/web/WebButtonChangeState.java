
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A state change button component.
 * 
 * <p>Java class for web-button-change-state complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-button-change-state"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-button"&gt;
 *       &lt;attribute name="target-state" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-button-change-state")
public class WebButtonChangeState
    extends WebButton
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "target-state")
    protected String targetState;

    /**
     * Gets the value of the targetState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetState() {
        return targetState;
    }

    /**
     * Sets the value of the targetState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetState(String value) {
        this.targetState = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theTargetState;
            theTargetState = this.getTargetState();
            strategy.appendField(locator, this, "targetState", buffer, theTargetState);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebButtonChangeState)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebButtonChangeState that = ((WebButtonChangeState) object);
        {
            String lhsTargetState;
            lhsTargetState = this.getTargetState();
            String rhsTargetState;
            rhsTargetState = that.getTargetState();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetState", lhsTargetState), LocatorUtils.property(thatLocator, "targetState", rhsTargetState), lhsTargetState, rhsTargetState)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theTargetState;
            theTargetState = this.getTargetState();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "targetState", theTargetState), currentHashCode, theTargetState);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebButtonChangeState) {
            final WebButtonChangeState copy = ((WebButtonChangeState) draftCopy);
            if (this.targetState!= null) {
                String sourceTargetState;
                sourceTargetState = this.getTargetState();
                String copyTargetState = ((String) strategy.copy(LocatorUtils.property(locator, "targetState", sourceTargetState), sourceTargetState));
                copy.setTargetState(copyTargetState);
            } else {
                copy.targetState = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebButtonChangeState();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebButtonChangeState.Builder<_B> _other) {
        super.copyTo(_other);
        _other.targetState = this.targetState;
    }

    @Override
    public<_B >WebButtonChangeState.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebButtonChangeState.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebButtonChangeState.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebButtonChangeState.Builder<Void> builder() {
        return new WebButtonChangeState.Builder<Void>(null, null, false);
    }

    public static<_B >WebButtonChangeState.Builder<_B> copyOf(final WebComponent _other) {
        final WebButtonChangeState.Builder<_B> _newBuilder = new WebButtonChangeState.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebButtonChangeState.Builder<_B> copyOf(final WebInputComponent _other) {
        final WebButtonChangeState.Builder<_B> _newBuilder = new WebButtonChangeState.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebButtonChangeState.Builder<_B> copyOf(final WebButton _other) {
        final WebButtonChangeState.Builder<_B> _newBuilder = new WebButtonChangeState.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebButtonChangeState.Builder<_B> copyOf(final WebButtonChangeState _other) {
        final WebButtonChangeState.Builder<_B> _newBuilder = new WebButtonChangeState.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebButtonChangeState.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree targetStatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetState"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetStatePropertyTree!= null):((targetStatePropertyTree == null)||(!targetStatePropertyTree.isLeaf())))) {
            _other.targetState = this.targetState;
        }
    }

    @Override
    public<_B >WebButtonChangeState.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebButtonChangeState.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebButtonChangeState.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebButtonChangeState.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebButtonChangeState.Builder<_B> _newBuilder = new WebButtonChangeState.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebButtonChangeState.Builder<_B> copyOf(final WebInputComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebButtonChangeState.Builder<_B> _newBuilder = new WebButtonChangeState.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebButtonChangeState.Builder<_B> copyOf(final WebButton _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebButtonChangeState.Builder<_B> _newBuilder = new WebButtonChangeState.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebButtonChangeState.Builder<_B> copyOf(final WebButtonChangeState _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebButtonChangeState.Builder<_B> _newBuilder = new WebButtonChangeState.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebButtonChangeState.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebButtonChangeState.Builder<Void> copyExcept(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebButtonChangeState.Builder<Void> copyExcept(final WebButton _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebButtonChangeState.Builder<Void> copyExcept(final WebButtonChangeState _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebButtonChangeState.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebButtonChangeState.Builder<Void> copyOnly(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebButtonChangeState.Builder<Void> copyOnly(final WebButton _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebButtonChangeState.Builder<Void> copyOnly(final WebButtonChangeState _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebButton.Builder<_B>
        implements Buildable
    {

        private String targetState;

        public Builder(final _B _parentBuilder, final WebButtonChangeState _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.targetState = _other.targetState;
            }
        }

        public Builder(final _B _parentBuilder, final WebButtonChangeState _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree targetStatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetState"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetStatePropertyTree!= null):((targetStatePropertyTree == null)||(!targetStatePropertyTree.isLeaf())))) {
                    this.targetState = _other.targetState;
                }
            }
        }

        protected<_P extends WebButtonChangeState >_P init(final _P _product) {
            _product.targetState = this.targetState;
            return super.init(_product);
        }

        /**
         * Sets the new value of "targetState" (any previous value will be replaced)
         * 
         * @param targetState
         *     New value of the "targetState" property.
         */
        public WebButtonChangeState.Builder<_B> withTargetState(final String targetState) {
            this.targetState = targetState;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withLabel(final String label) {
            super.withLabel(label);
            return this;
        }

        /**
         * Sets the new value of "icon" (any previous value will be replaced)
         * 
         * @param icon
         *     New value of the "icon" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withIcon(final String icon) {
            super.withIcon(icon);
            return this;
        }

        /**
         * Sets the new value of "disableDuringEdit" (any previous value will be replaced)
         * 
         * @param disableDuringEdit
         *     New value of the "disableDuringEdit" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withDisableDuringEdit(final Boolean disableDuringEdit) {
            super.withDisableDuringEdit(disableDuringEdit);
            return this;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            super.withValuelistProvider(valuelistProvider);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebButtonChangeState.Builder<_B>> withValuelistProvider() {
            return ((WebValuelistProvider.Builder<? extends WebButtonChangeState.Builder<_B>> ) super.withValuelistProvider());
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withEnabled(final Boolean enabled) {
            super.withEnabled(enabled);
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withEditable(final Boolean editable) {
            super.withEditable(editable);
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withInsertable(final Boolean insertable) {
            super.withInsertable(insertable);
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withColumns(final String columns) {
            super.withColumns(columns);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebButtonChangeState.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebButtonChangeState.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebButtonChangeState.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebButtonChangeState.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebButtonChangeState.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebButtonChangeState build() {
            if (_storedValue == null) {
                return this.init(new WebButtonChangeState());
            } else {
                return ((WebButtonChangeState) _storedValue);
            }
        }

        public WebButtonChangeState.Builder<_B> copyOf(final WebButtonChangeState _other) {
            _other.copyTo(this);
            return this;
        }

        public WebButtonChangeState.Builder<_B> copyOf(final WebButtonChangeState.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebButtonChangeState.Selector<WebButtonChangeState.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebButtonChangeState.Select _root() {
            return new WebButtonChangeState.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebButton.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebButtonChangeState.Selector<TRoot, TParent>> targetState = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.targetState!= null) {
                products.put("targetState", this.targetState.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebButtonChangeState.Selector<TRoot, TParent>> targetState() {
            return ((this.targetState == null)?this.targetState = new com.kscs.util.jaxb.Selector<TRoot, WebButtonChangeState.Selector<TRoot, TParent>>(this._root, this, "targetState"):this.targetState);
        }

    }

}
