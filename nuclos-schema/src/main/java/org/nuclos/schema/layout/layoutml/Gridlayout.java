
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="rows" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="columns" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="hgap" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="vgap" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Gridlayout implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "rows", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String rows;
    @XmlAttribute(name = "columns", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String columns;
    @XmlAttribute(name = "hgap")
    @XmlSchemaType(name = "anySimpleType")
    protected String hgap;
    @XmlAttribute(name = "vgap")
    @XmlSchemaType(name = "anySimpleType")
    protected String vgap;

    /**
     * Gets the value of the rows property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRows(String value) {
        this.rows = value;
    }

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumns(String value) {
        this.columns = value;
    }

    /**
     * Gets the value of the hgap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHgap() {
        return hgap;
    }

    /**
     * Sets the value of the hgap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHgap(String value) {
        this.hgap = value;
    }

    /**
     * Gets the value of the vgap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVgap() {
        return vgap;
    }

    /**
     * Sets the value of the vgap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVgap(String value) {
        this.vgap = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theRows;
            theRows = this.getRows();
            strategy.appendField(locator, this, "rows", buffer, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            strategy.appendField(locator, this, "columns", buffer, theColumns);
        }
        {
            String theHgap;
            theHgap = this.getHgap();
            strategy.appendField(locator, this, "hgap", buffer, theHgap);
        }
        {
            String theVgap;
            theVgap = this.getVgap();
            strategy.appendField(locator, this, "vgap", buffer, theVgap);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Gridlayout)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Gridlayout that = ((Gridlayout) object);
        {
            String lhsRows;
            lhsRows = this.getRows();
            String rhsRows;
            rhsRows = that.getRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        {
            String lhsColumns;
            lhsColumns = this.getColumns();
            String rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            String lhsHgap;
            lhsHgap = this.getHgap();
            String rhsHgap;
            rhsHgap = that.getHgap();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hgap", lhsHgap), LocatorUtils.property(thatLocator, "hgap", rhsHgap), lhsHgap, rhsHgap)) {
                return false;
            }
        }
        {
            String lhsVgap;
            lhsVgap = this.getVgap();
            String rhsVgap;
            rhsVgap = that.getVgap();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vgap", lhsVgap), LocatorUtils.property(thatLocator, "vgap", rhsVgap), lhsVgap, rhsVgap)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theRows;
            theRows = this.getRows();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rows", theRows), currentHashCode, theRows);
        }
        {
            String theColumns;
            theColumns = this.getColumns();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "columns", theColumns), currentHashCode, theColumns);
        }
        {
            String theHgap;
            theHgap = this.getHgap();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hgap", theHgap), currentHashCode, theHgap);
        }
        {
            String theVgap;
            theVgap = this.getVgap();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vgap", theVgap), currentHashCode, theVgap);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Gridlayout) {
            final Gridlayout copy = ((Gridlayout) draftCopy);
            if (this.rows!= null) {
                String sourceRows;
                sourceRows = this.getRows();
                String copyRows = ((String) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.setRows(copyRows);
            } else {
                copy.rows = null;
            }
            if (this.columns!= null) {
                String sourceColumns;
                sourceColumns = this.getColumns();
                String copyColumns = ((String) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.hgap!= null) {
                String sourceHgap;
                sourceHgap = this.getHgap();
                String copyHgap = ((String) strategy.copy(LocatorUtils.property(locator, "hgap", sourceHgap), sourceHgap));
                copy.setHgap(copyHgap);
            } else {
                copy.hgap = null;
            }
            if (this.vgap!= null) {
                String sourceVgap;
                sourceVgap = this.getVgap();
                String copyVgap = ((String) strategy.copy(LocatorUtils.property(locator, "vgap", sourceVgap), sourceVgap));
                copy.setVgap(copyVgap);
            } else {
                copy.vgap = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Gridlayout();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Gridlayout.Builder<_B> _other) {
        _other.rows = this.rows;
        _other.columns = this.columns;
        _other.hgap = this.hgap;
        _other.vgap = this.vgap;
    }

    public<_B >Gridlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Gridlayout.Builder<_B>(_parentBuilder, this, true);
    }

    public Gridlayout.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Gridlayout.Builder<Void> builder() {
        return new Gridlayout.Builder<Void>(null, null, false);
    }

    public static<_B >Gridlayout.Builder<_B> copyOf(final Gridlayout _other) {
        final Gridlayout.Builder<_B> _newBuilder = new Gridlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Gridlayout.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
            _other.rows = this.rows;
        }
        final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
            _other.columns = this.columns;
        }
        final PropertyTree hgapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("hgap"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hgapPropertyTree!= null):((hgapPropertyTree == null)||(!hgapPropertyTree.isLeaf())))) {
            _other.hgap = this.hgap;
        }
        final PropertyTree vgapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vgap"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vgapPropertyTree!= null):((vgapPropertyTree == null)||(!vgapPropertyTree.isLeaf())))) {
            _other.vgap = this.vgap;
        }
    }

    public<_B >Gridlayout.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Gridlayout.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Gridlayout.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Gridlayout.Builder<_B> copyOf(final Gridlayout _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Gridlayout.Builder<_B> _newBuilder = new Gridlayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Gridlayout.Builder<Void> copyExcept(final Gridlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Gridlayout.Builder<Void> copyOnly(final Gridlayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Gridlayout _storedValue;
        private String rows;
        private String columns;
        private String hgap;
        private String vgap;

        public Builder(final _B _parentBuilder, final Gridlayout _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.rows = _other.rows;
                    this.columns = _other.columns;
                    this.hgap = _other.hgap;
                    this.vgap = _other.vgap;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Gridlayout _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
                        this.rows = _other.rows;
                    }
                    final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
                        this.columns = _other.columns;
                    }
                    final PropertyTree hgapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("hgap"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hgapPropertyTree!= null):((hgapPropertyTree == null)||(!hgapPropertyTree.isLeaf())))) {
                        this.hgap = _other.hgap;
                    }
                    final PropertyTree vgapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vgap"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vgapPropertyTree!= null):((vgapPropertyTree == null)||(!vgapPropertyTree.isLeaf())))) {
                        this.vgap = _other.vgap;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Gridlayout >_P init(final _P _product) {
            _product.rows = this.rows;
            _product.columns = this.columns;
            _product.hgap = this.hgap;
            _product.vgap = this.vgap;
            return _product;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public Gridlayout.Builder<_B> withRows(final String rows) {
            this.rows = rows;
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        public Gridlayout.Builder<_B> withColumns(final String columns) {
            this.columns = columns;
            return this;
        }

        /**
         * Sets the new value of "hgap" (any previous value will be replaced)
         * 
         * @param hgap
         *     New value of the "hgap" property.
         */
        public Gridlayout.Builder<_B> withHgap(final String hgap) {
            this.hgap = hgap;
            return this;
        }

        /**
         * Sets the new value of "vgap" (any previous value will be replaced)
         * 
         * @param vgap
         *     New value of the "vgap" property.
         */
        public Gridlayout.Builder<_B> withVgap(final String vgap) {
            this.vgap = vgap;
            return this;
        }

        @Override
        public Gridlayout build() {
            if (_storedValue == null) {
                return this.init(new Gridlayout());
            } else {
                return ((Gridlayout) _storedValue);
            }
        }

        public Gridlayout.Builder<_B> copyOf(final Gridlayout _other) {
            _other.copyTo(this);
            return this;
        }

        public Gridlayout.Builder<_B> copyOf(final Gridlayout.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Gridlayout.Selector<Gridlayout.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Gridlayout.Select _root() {
            return new Gridlayout.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>> rows = null;
        private com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>> columns = null;
        private com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>> hgap = null;
        private com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>> vgap = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.rows!= null) {
                products.put("rows", this.rows.init());
            }
            if (this.columns!= null) {
                products.put("columns", this.columns.init());
            }
            if (this.hgap!= null) {
                products.put("hgap", this.hgap.init());
            }
            if (this.vgap!= null) {
                products.put("vgap", this.vgap.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>> rows() {
            return ((this.rows == null)?this.rows = new com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>>(this._root, this, "rows"):this.rows);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>> columns() {
            return ((this.columns == null)?this.columns = new com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>>(this._root, this, "columns"):this.columns);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>> hgap() {
            return ((this.hgap == null)?this.hgap = new com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>>(this._root, this, "hgap"):this.hgap);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>> vgap() {
            return ((this.vgap == null)?this.vgap = new com.kscs.util.jaxb.Selector<TRoot, Gridlayout.Selector<TRoot, TParent>>(this._root, this, "vgap"):this.vgap);
        }

    }

}
