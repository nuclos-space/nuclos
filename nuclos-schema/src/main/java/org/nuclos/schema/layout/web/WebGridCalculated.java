
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A container with absolute positioned (calculated) cells, .
 * 
 * <p>Java class for web-grid-calculated complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-grid-calculated"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cells" type="{urn:org.nuclos.schema.layout.web}web-calc-cell" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="min-width" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="min-height" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-grid-calculated", propOrder = {
    "cells"
})
public class WebGridCalculated implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<WebCalcCell> cells;
    @XmlAttribute(name = "min-width")
    protected BigInteger minWidth;
    @XmlAttribute(name = "min-height")
    protected BigInteger minHeight;

    /**
     * Gets the value of the cells property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cells property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCells().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebCalcCell }
     * 
     * 
     */
    public List<WebCalcCell> getCells() {
        if (cells == null) {
            cells = new ArrayList<WebCalcCell>();
        }
        return this.cells;
    }

    /**
     * Gets the value of the minWidth property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinWidth() {
        return minWidth;
    }

    /**
     * Sets the value of the minWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinWidth(BigInteger value) {
        this.minWidth = value;
    }

    /**
     * Gets the value of the minHeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinHeight() {
        return minHeight;
    }

    /**
     * Sets the value of the minHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinHeight(BigInteger value) {
        this.minHeight = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebCalcCell> theCells;
            theCells = (((this.cells!= null)&&(!this.cells.isEmpty()))?this.getCells():null);
            strategy.appendField(locator, this, "cells", buffer, theCells);
        }
        {
            BigInteger theMinWidth;
            theMinWidth = this.getMinWidth();
            strategy.appendField(locator, this, "minWidth", buffer, theMinWidth);
        }
        {
            BigInteger theMinHeight;
            theMinHeight = this.getMinHeight();
            strategy.appendField(locator, this, "minHeight", buffer, theMinHeight);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebGridCalculated)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebGridCalculated that = ((WebGridCalculated) object);
        {
            List<WebCalcCell> lhsCells;
            lhsCells = (((this.cells!= null)&&(!this.cells.isEmpty()))?this.getCells():null);
            List<WebCalcCell> rhsCells;
            rhsCells = (((that.cells!= null)&&(!that.cells.isEmpty()))?that.getCells():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cells", lhsCells), LocatorUtils.property(thatLocator, "cells", rhsCells), lhsCells, rhsCells)) {
                return false;
            }
        }
        {
            BigInteger lhsMinWidth;
            lhsMinWidth = this.getMinWidth();
            BigInteger rhsMinWidth;
            rhsMinWidth = that.getMinWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minWidth", lhsMinWidth), LocatorUtils.property(thatLocator, "minWidth", rhsMinWidth), lhsMinWidth, rhsMinWidth)) {
                return false;
            }
        }
        {
            BigInteger lhsMinHeight;
            lhsMinHeight = this.getMinHeight();
            BigInteger rhsMinHeight;
            rhsMinHeight = that.getMinHeight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minHeight", lhsMinHeight), LocatorUtils.property(thatLocator, "minHeight", rhsMinHeight), lhsMinHeight, rhsMinHeight)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebCalcCell> theCells;
            theCells = (((this.cells!= null)&&(!this.cells.isEmpty()))?this.getCells():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cells", theCells), currentHashCode, theCells);
        }
        {
            BigInteger theMinWidth;
            theMinWidth = this.getMinWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minWidth", theMinWidth), currentHashCode, theMinWidth);
        }
        {
            BigInteger theMinHeight;
            theMinHeight = this.getMinHeight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minHeight", theMinHeight), currentHashCode, theMinHeight);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebGridCalculated) {
            final WebGridCalculated copy = ((WebGridCalculated) draftCopy);
            if ((this.cells!= null)&&(!this.cells.isEmpty())) {
                List<WebCalcCell> sourceCells;
                sourceCells = (((this.cells!= null)&&(!this.cells.isEmpty()))?this.getCells():null);
                @SuppressWarnings("unchecked")
                List<WebCalcCell> copyCells = ((List<WebCalcCell> ) strategy.copy(LocatorUtils.property(locator, "cells", sourceCells), sourceCells));
                copy.cells = null;
                if (copyCells!= null) {
                    List<WebCalcCell> uniqueCellsl = copy.getCells();
                    uniqueCellsl.addAll(copyCells);
                }
            } else {
                copy.cells = null;
            }
            if (this.minWidth!= null) {
                BigInteger sourceMinWidth;
                sourceMinWidth = this.getMinWidth();
                BigInteger copyMinWidth = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "minWidth", sourceMinWidth), sourceMinWidth));
                copy.setMinWidth(copyMinWidth);
            } else {
                copy.minWidth = null;
            }
            if (this.minHeight!= null) {
                BigInteger sourceMinHeight;
                sourceMinHeight = this.getMinHeight();
                BigInteger copyMinHeight = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "minHeight", sourceMinHeight), sourceMinHeight));
                copy.setMinHeight(copyMinHeight);
            } else {
                copy.minHeight = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebGridCalculated();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebGridCalculated.Builder<_B> _other) {
        if (this.cells == null) {
            _other.cells = null;
        } else {
            _other.cells = new ArrayList<WebCalcCell.Builder<WebGridCalculated.Builder<_B>>>();
            for (WebCalcCell _item: this.cells) {
                _other.cells.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.minWidth = this.minWidth;
        _other.minHeight = this.minHeight;
    }

    public<_B >WebGridCalculated.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebGridCalculated.Builder<_B>(_parentBuilder, this, true);
    }

    public WebGridCalculated.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebGridCalculated.Builder<Void> builder() {
        return new WebGridCalculated.Builder<Void>(null, null, false);
    }

    public static<_B >WebGridCalculated.Builder<_B> copyOf(final WebGridCalculated _other) {
        final WebGridCalculated.Builder<_B> _newBuilder = new WebGridCalculated.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebGridCalculated.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree cellsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cells"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellsPropertyTree!= null):((cellsPropertyTree == null)||(!cellsPropertyTree.isLeaf())))) {
            if (this.cells == null) {
                _other.cells = null;
            } else {
                _other.cells = new ArrayList<WebCalcCell.Builder<WebGridCalculated.Builder<_B>>>();
                for (WebCalcCell _item: this.cells) {
                    _other.cells.add(((_item == null)?null:_item.newCopyBuilder(_other, cellsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree minWidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minWidth"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minWidthPropertyTree!= null):((minWidthPropertyTree == null)||(!minWidthPropertyTree.isLeaf())))) {
            _other.minWidth = this.minWidth;
        }
        final PropertyTree minHeightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minHeight"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minHeightPropertyTree!= null):((minHeightPropertyTree == null)||(!minHeightPropertyTree.isLeaf())))) {
            _other.minHeight = this.minHeight;
        }
    }

    public<_B >WebGridCalculated.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebGridCalculated.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebGridCalculated.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebGridCalculated.Builder<_B> copyOf(final WebGridCalculated _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebGridCalculated.Builder<_B> _newBuilder = new WebGridCalculated.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebGridCalculated.Builder<Void> copyExcept(final WebGridCalculated _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebGridCalculated.Builder<Void> copyOnly(final WebGridCalculated _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebGridCalculated _storedValue;
        private List<WebCalcCell.Builder<WebGridCalculated.Builder<_B>>> cells;
        private BigInteger minWidth;
        private BigInteger minHeight;

        public Builder(final _B _parentBuilder, final WebGridCalculated _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.cells == null) {
                        this.cells = null;
                    } else {
                        this.cells = new ArrayList<WebCalcCell.Builder<WebGridCalculated.Builder<_B>>>();
                        for (WebCalcCell _item: _other.cells) {
                            this.cells.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.minWidth = _other.minWidth;
                    this.minHeight = _other.minHeight;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebGridCalculated _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree cellsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("cells"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cellsPropertyTree!= null):((cellsPropertyTree == null)||(!cellsPropertyTree.isLeaf())))) {
                        if (_other.cells == null) {
                            this.cells = null;
                        } else {
                            this.cells = new ArrayList<WebCalcCell.Builder<WebGridCalculated.Builder<_B>>>();
                            for (WebCalcCell _item: _other.cells) {
                                this.cells.add(((_item == null)?null:_item.newCopyBuilder(this, cellsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree minWidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minWidth"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minWidthPropertyTree!= null):((minWidthPropertyTree == null)||(!minWidthPropertyTree.isLeaf())))) {
                        this.minWidth = _other.minWidth;
                    }
                    final PropertyTree minHeightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minHeight"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minHeightPropertyTree!= null):((minHeightPropertyTree == null)||(!minHeightPropertyTree.isLeaf())))) {
                        this.minHeight = _other.minHeight;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebGridCalculated >_P init(final _P _product) {
            if (this.cells!= null) {
                final List<WebCalcCell> cells = new ArrayList<WebCalcCell>(this.cells.size());
                for (WebCalcCell.Builder<WebGridCalculated.Builder<_B>> _item: this.cells) {
                    cells.add(_item.build());
                }
                _product.cells = cells;
            }
            _product.minWidth = this.minWidth;
            _product.minHeight = this.minHeight;
            return _product;
        }

        /**
         * Adds the given items to the value of "cells"
         * 
         * @param cells
         *     Items to add to the value of the "cells" property
         */
        public WebGridCalculated.Builder<_B> addCells(final Iterable<? extends WebCalcCell> cells) {
            if (cells!= null) {
                if (this.cells == null) {
                    this.cells = new ArrayList<WebCalcCell.Builder<WebGridCalculated.Builder<_B>>>();
                }
                for (WebCalcCell _item: cells) {
                    this.cells.add(new WebCalcCell.Builder<WebGridCalculated.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "cells" (any previous value will be replaced)
         * 
         * @param cells
         *     New value of the "cells" property.
         */
        public WebGridCalculated.Builder<_B> withCells(final Iterable<? extends WebCalcCell> cells) {
            if (this.cells!= null) {
                this.cells.clear();
            }
            return addCells(cells);
        }

        /**
         * Adds the given items to the value of "cells"
         * 
         * @param cells
         *     Items to add to the value of the "cells" property
         */
        public WebGridCalculated.Builder<_B> addCells(WebCalcCell... cells) {
            addCells(Arrays.asList(cells));
            return this;
        }

        /**
         * Sets the new value of "cells" (any previous value will be replaced)
         * 
         * @param cells
         *     New value of the "cells" property.
         */
        public WebGridCalculated.Builder<_B> withCells(WebCalcCell... cells) {
            withCells(Arrays.asList(cells));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Cells" property.
         * Use {@link org.nuclos.schema.layout.web.WebCalcCell.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Cells" property.
         *     Use {@link org.nuclos.schema.layout.web.WebCalcCell.Builder#end()} to return to the current builder.
         */
        public WebCalcCell.Builder<? extends WebGridCalculated.Builder<_B>> addCells() {
            if (this.cells == null) {
                this.cells = new ArrayList<WebCalcCell.Builder<WebGridCalculated.Builder<_B>>>();
            }
            final WebCalcCell.Builder<WebGridCalculated.Builder<_B>> cells_Builder = new WebCalcCell.Builder<WebGridCalculated.Builder<_B>>(this, null, false);
            this.cells.add(cells_Builder);
            return cells_Builder;
        }

        /**
         * Sets the new value of "minWidth" (any previous value will be replaced)
         * 
         * @param minWidth
         *     New value of the "minWidth" property.
         */
        public WebGridCalculated.Builder<_B> withMinWidth(final BigInteger minWidth) {
            this.minWidth = minWidth;
            return this;
        }

        /**
         * Sets the new value of "minHeight" (any previous value will be replaced)
         * 
         * @param minHeight
         *     New value of the "minHeight" property.
         */
        public WebGridCalculated.Builder<_B> withMinHeight(final BigInteger minHeight) {
            this.minHeight = minHeight;
            return this;
        }

        @Override
        public WebGridCalculated build() {
            if (_storedValue == null) {
                return this.init(new WebGridCalculated());
            } else {
                return ((WebGridCalculated) _storedValue);
            }
        }

        public WebGridCalculated.Builder<_B> copyOf(final WebGridCalculated _other) {
            _other.copyTo(this);
            return this;
        }

        public WebGridCalculated.Builder<_B> copyOf(final WebGridCalculated.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebGridCalculated.Selector<WebGridCalculated.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebGridCalculated.Select _root() {
            return new WebGridCalculated.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebCalcCell.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>> cells = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>> minWidth = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>> minHeight = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.cells!= null) {
                products.put("cells", this.cells.init());
            }
            if (this.minWidth!= null) {
                products.put("minWidth", this.minWidth.init());
            }
            if (this.minHeight!= null) {
                products.put("minHeight", this.minHeight.init());
            }
            return products;
        }

        public WebCalcCell.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>> cells() {
            return ((this.cells == null)?this.cells = new WebCalcCell.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>>(this._root, this, "cells"):this.cells);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>> minWidth() {
            return ((this.minWidth == null)?this.minWidth = new com.kscs.util.jaxb.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>>(this._root, this, "minWidth"):this.minWidth);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>> minHeight() {
            return ((this.minHeight == null)?this.minHeight = new com.kscs.util.jaxb.Selector<TRoot, WebGridCalculated.Selector<TRoot, TParent>>(this._root, this, "minHeight"):this.minHeight);
        }

    }

}
