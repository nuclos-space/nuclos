
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A non-responsive table with fixed sizes.
 * 
 * <p>Java class for web-table complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-table"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rows" type="{urn:org.nuclos.schema.layout.web}web-row" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="column-sizes" type="{http://www.w3.org/2001/XMLSchema}integer" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="row-sizes" type="{http://www.w3.org/2001/XMLSchema}integer" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-table", propOrder = {
    "rows",
    "columnSizes",
    "rowSizes"
})
public class WebTable implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<WebRow> rows;
    @XmlElement(name = "column-sizes")
    protected List<BigInteger> columnSizes;
    @XmlElement(name = "row-sizes")
    protected List<BigInteger> rowSizes;

    /**
     * Gets the value of the rows property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rows property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRows().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebRow }
     * 
     * 
     */
    public List<WebRow> getRows() {
        if (rows == null) {
            rows = new ArrayList<WebRow>();
        }
        return this.rows;
    }

    /**
     * Gets the value of the columnSizes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the columnSizes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getColumnSizes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BigInteger }
     * 
     * 
     */
    public List<BigInteger> getColumnSizes() {
        if (columnSizes == null) {
            columnSizes = new ArrayList<BigInteger>();
        }
        return this.columnSizes;
    }

    /**
     * Gets the value of the rowSizes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rowSizes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRowSizes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BigInteger }
     * 
     * 
     */
    public List<BigInteger> getRowSizes() {
        if (rowSizes == null) {
            rowSizes = new ArrayList<BigInteger>();
        }
        return this.rowSizes;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebRow> theRows;
            theRows = (((this.rows!= null)&&(!this.rows.isEmpty()))?this.getRows():null);
            strategy.appendField(locator, this, "rows", buffer, theRows);
        }
        {
            List<BigInteger> theColumnSizes;
            theColumnSizes = (((this.columnSizes!= null)&&(!this.columnSizes.isEmpty()))?this.getColumnSizes():null);
            strategy.appendField(locator, this, "columnSizes", buffer, theColumnSizes);
        }
        {
            List<BigInteger> theRowSizes;
            theRowSizes = (((this.rowSizes!= null)&&(!this.rowSizes.isEmpty()))?this.getRowSizes():null);
            strategy.appendField(locator, this, "rowSizes", buffer, theRowSizes);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebTable)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebTable that = ((WebTable) object);
        {
            List<WebRow> lhsRows;
            lhsRows = (((this.rows!= null)&&(!this.rows.isEmpty()))?this.getRows():null);
            List<WebRow> rhsRows;
            rhsRows = (((that.rows!= null)&&(!that.rows.isEmpty()))?that.getRows():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        {
            List<BigInteger> lhsColumnSizes;
            lhsColumnSizes = (((this.columnSizes!= null)&&(!this.columnSizes.isEmpty()))?this.getColumnSizes():null);
            List<BigInteger> rhsColumnSizes;
            rhsColumnSizes = (((that.columnSizes!= null)&&(!that.columnSizes.isEmpty()))?that.getColumnSizes():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columnSizes", lhsColumnSizes), LocatorUtils.property(thatLocator, "columnSizes", rhsColumnSizes), lhsColumnSizes, rhsColumnSizes)) {
                return false;
            }
        }
        {
            List<BigInteger> lhsRowSizes;
            lhsRowSizes = (((this.rowSizes!= null)&&(!this.rowSizes.isEmpty()))?this.getRowSizes():null);
            List<BigInteger> rhsRowSizes;
            rhsRowSizes = (((that.rowSizes!= null)&&(!that.rowSizes.isEmpty()))?that.getRowSizes():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rowSizes", lhsRowSizes), LocatorUtils.property(thatLocator, "rowSizes", rhsRowSizes), lhsRowSizes, rhsRowSizes)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebRow> theRows;
            theRows = (((this.rows!= null)&&(!this.rows.isEmpty()))?this.getRows():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rows", theRows), currentHashCode, theRows);
        }
        {
            List<BigInteger> theColumnSizes;
            theColumnSizes = (((this.columnSizes!= null)&&(!this.columnSizes.isEmpty()))?this.getColumnSizes():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "columnSizes", theColumnSizes), currentHashCode, theColumnSizes);
        }
        {
            List<BigInteger> theRowSizes;
            theRowSizes = (((this.rowSizes!= null)&&(!this.rowSizes.isEmpty()))?this.getRowSizes():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rowSizes", theRowSizes), currentHashCode, theRowSizes);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebTable) {
            final WebTable copy = ((WebTable) draftCopy);
            if ((this.rows!= null)&&(!this.rows.isEmpty())) {
                List<WebRow> sourceRows;
                sourceRows = (((this.rows!= null)&&(!this.rows.isEmpty()))?this.getRows():null);
                @SuppressWarnings("unchecked")
                List<WebRow> copyRows = ((List<WebRow> ) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.rows = null;
                if (copyRows!= null) {
                    List<WebRow> uniqueRowsl = copy.getRows();
                    uniqueRowsl.addAll(copyRows);
                }
            } else {
                copy.rows = null;
            }
            if ((this.columnSizes!= null)&&(!this.columnSizes.isEmpty())) {
                List<BigInteger> sourceColumnSizes;
                sourceColumnSizes = (((this.columnSizes!= null)&&(!this.columnSizes.isEmpty()))?this.getColumnSizes():null);
                @SuppressWarnings("unchecked")
                List<BigInteger> copyColumnSizes = ((List<BigInteger> ) strategy.copy(LocatorUtils.property(locator, "columnSizes", sourceColumnSizes), sourceColumnSizes));
                copy.columnSizes = null;
                if (copyColumnSizes!= null) {
                    List<BigInteger> uniqueColumnSizesl = copy.getColumnSizes();
                    uniqueColumnSizesl.addAll(copyColumnSizes);
                }
            } else {
                copy.columnSizes = null;
            }
            if ((this.rowSizes!= null)&&(!this.rowSizes.isEmpty())) {
                List<BigInteger> sourceRowSizes;
                sourceRowSizes = (((this.rowSizes!= null)&&(!this.rowSizes.isEmpty()))?this.getRowSizes():null);
                @SuppressWarnings("unchecked")
                List<BigInteger> copyRowSizes = ((List<BigInteger> ) strategy.copy(LocatorUtils.property(locator, "rowSizes", sourceRowSizes), sourceRowSizes));
                copy.rowSizes = null;
                if (copyRowSizes!= null) {
                    List<BigInteger> uniqueRowSizesl = copy.getRowSizes();
                    uniqueRowSizesl.addAll(copyRowSizes);
                }
            } else {
                copy.rowSizes = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebTable();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebTable.Builder<_B> _other) {
        if (this.rows == null) {
            _other.rows = null;
        } else {
            _other.rows = new ArrayList<WebRow.Builder<WebTable.Builder<_B>>>();
            for (WebRow _item: this.rows) {
                _other.rows.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.columnSizes == null) {
            _other.columnSizes = null;
        } else {
            _other.columnSizes = new ArrayList<Buildable>();
            for (BigInteger _item: this.columnSizes) {
                _other.columnSizes.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        if (this.rowSizes == null) {
            _other.rowSizes = null;
        } else {
            _other.rowSizes = new ArrayList<Buildable>();
            for (BigInteger _item: this.rowSizes) {
                _other.rowSizes.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
    }

    public<_B >WebTable.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebTable.Builder<_B>(_parentBuilder, this, true);
    }

    public WebTable.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebTable.Builder<Void> builder() {
        return new WebTable.Builder<Void>(null, null, false);
    }

    public static<_B >WebTable.Builder<_B> copyOf(final WebTable _other) {
        final WebTable.Builder<_B> _newBuilder = new WebTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebTable.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
            if (this.rows == null) {
                _other.rows = null;
            } else {
                _other.rows = new ArrayList<WebRow.Builder<WebTable.Builder<_B>>>();
                for (WebRow _item: this.rows) {
                    _other.rows.add(((_item == null)?null:_item.newCopyBuilder(_other, rowsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree columnSizesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columnSizes"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnSizesPropertyTree!= null):((columnSizesPropertyTree == null)||(!columnSizesPropertyTree.isLeaf())))) {
            if (this.columnSizes == null) {
                _other.columnSizes = null;
            } else {
                _other.columnSizes = new ArrayList<Buildable>();
                for (BigInteger _item: this.columnSizes) {
                    _other.columnSizes.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree rowSizesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowSizes"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowSizesPropertyTree!= null):((rowSizesPropertyTree == null)||(!rowSizesPropertyTree.isLeaf())))) {
            if (this.rowSizes == null) {
                _other.rowSizes = null;
            } else {
                _other.rowSizes = new ArrayList<Buildable>();
                for (BigInteger _item: this.rowSizes) {
                    _other.rowSizes.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
    }

    public<_B >WebTable.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebTable.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebTable.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebTable.Builder<_B> copyOf(final WebTable _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebTable.Builder<_B> _newBuilder = new WebTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebTable.Builder<Void> copyExcept(final WebTable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebTable.Builder<Void> copyOnly(final WebTable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebTable _storedValue;
        private List<WebRow.Builder<WebTable.Builder<_B>>> rows;
        private List<Buildable> columnSizes;
        private List<Buildable> rowSizes;

        public Builder(final _B _parentBuilder, final WebTable _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.rows == null) {
                        this.rows = null;
                    } else {
                        this.rows = new ArrayList<WebRow.Builder<WebTable.Builder<_B>>>();
                        for (WebRow _item: _other.rows) {
                            this.rows.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.columnSizes == null) {
                        this.columnSizes = null;
                    } else {
                        this.columnSizes = new ArrayList<Buildable>();
                        for (BigInteger _item: _other.columnSizes) {
                            this.columnSizes.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    if (_other.rowSizes == null) {
                        this.rowSizes = null;
                    } else {
                        this.rowSizes = new ArrayList<Buildable>();
                        for (BigInteger _item: _other.rowSizes) {
                            this.rowSizes.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebTable _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
                        if (_other.rows == null) {
                            this.rows = null;
                        } else {
                            this.rows = new ArrayList<WebRow.Builder<WebTable.Builder<_B>>>();
                            for (WebRow _item: _other.rows) {
                                this.rows.add(((_item == null)?null:_item.newCopyBuilder(this, rowsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree columnSizesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columnSizes"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnSizesPropertyTree!= null):((columnSizesPropertyTree == null)||(!columnSizesPropertyTree.isLeaf())))) {
                        if (_other.columnSizes == null) {
                            this.columnSizes = null;
                        } else {
                            this.columnSizes = new ArrayList<Buildable>();
                            for (BigInteger _item: _other.columnSizes) {
                                this.columnSizes.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree rowSizesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowSizes"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowSizesPropertyTree!= null):((rowSizesPropertyTree == null)||(!rowSizesPropertyTree.isLeaf())))) {
                        if (_other.rowSizes == null) {
                            this.rowSizes = null;
                        } else {
                            this.rowSizes = new ArrayList<Buildable>();
                            for (BigInteger _item: _other.rowSizes) {
                                this.rowSizes.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebTable >_P init(final _P _product) {
            if (this.rows!= null) {
                final List<WebRow> rows = new ArrayList<WebRow>(this.rows.size());
                for (WebRow.Builder<WebTable.Builder<_B>> _item: this.rows) {
                    rows.add(_item.build());
                }
                _product.rows = rows;
            }
            if (this.columnSizes!= null) {
                final List<BigInteger> columnSizes = new ArrayList<BigInteger>(this.columnSizes.size());
                for (Buildable _item: this.columnSizes) {
                    columnSizes.add(((BigInteger) _item.build()));
                }
                _product.columnSizes = columnSizes;
            }
            if (this.rowSizes!= null) {
                final List<BigInteger> rowSizes = new ArrayList<BigInteger>(this.rowSizes.size());
                for (Buildable _item: this.rowSizes) {
                    rowSizes.add(((BigInteger) _item.build()));
                }
                _product.rowSizes = rowSizes;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "rows"
         * 
         * @param rows
         *     Items to add to the value of the "rows" property
         */
        public WebTable.Builder<_B> addRows(final Iterable<? extends WebRow> rows) {
            if (rows!= null) {
                if (this.rows == null) {
                    this.rows = new ArrayList<WebRow.Builder<WebTable.Builder<_B>>>();
                }
                for (WebRow _item: rows) {
                    this.rows.add(new WebRow.Builder<WebTable.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public WebTable.Builder<_B> withRows(final Iterable<? extends WebRow> rows) {
            if (this.rows!= null) {
                this.rows.clear();
            }
            return addRows(rows);
        }

        /**
         * Adds the given items to the value of "rows"
         * 
         * @param rows
         *     Items to add to the value of the "rows" property
         */
        public WebTable.Builder<_B> addRows(WebRow... rows) {
            addRows(Arrays.asList(rows));
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public WebTable.Builder<_B> withRows(WebRow... rows) {
            withRows(Arrays.asList(rows));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Rows" property.
         * Use {@link org.nuclos.schema.layout.web.WebRow.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Rows" property.
         *     Use {@link org.nuclos.schema.layout.web.WebRow.Builder#end()} to return to the current builder.
         */
        public WebRow.Builder<? extends WebTable.Builder<_B>> addRows() {
            if (this.rows == null) {
                this.rows = new ArrayList<WebRow.Builder<WebTable.Builder<_B>>>();
            }
            final WebRow.Builder<WebTable.Builder<_B>> rows_Builder = new WebRow.Builder<WebTable.Builder<_B>>(this, null, false);
            this.rows.add(rows_Builder);
            return rows_Builder;
        }

        /**
         * Adds the given items to the value of "columnSizes"
         * 
         * @param columnSizes
         *     Items to add to the value of the "columnSizes" property
         */
        public WebTable.Builder<_B> addColumnSizes(final Iterable<? extends BigInteger> columnSizes) {
            if (columnSizes!= null) {
                if (this.columnSizes == null) {
                    this.columnSizes = new ArrayList<Buildable>();
                }
                for (BigInteger _item: columnSizes) {
                    this.columnSizes.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "columnSizes" (any previous value will be replaced)
         * 
         * @param columnSizes
         *     New value of the "columnSizes" property.
         */
        public WebTable.Builder<_B> withColumnSizes(final Iterable<? extends BigInteger> columnSizes) {
            if (this.columnSizes!= null) {
                this.columnSizes.clear();
            }
            return addColumnSizes(columnSizes);
        }

        /**
         * Adds the given items to the value of "columnSizes"
         * 
         * @param columnSizes
         *     Items to add to the value of the "columnSizes" property
         */
        public WebTable.Builder<_B> addColumnSizes(BigInteger... columnSizes) {
            addColumnSizes(Arrays.asList(columnSizes));
            return this;
        }

        /**
         * Sets the new value of "columnSizes" (any previous value will be replaced)
         * 
         * @param columnSizes
         *     New value of the "columnSizes" property.
         */
        public WebTable.Builder<_B> withColumnSizes(BigInteger... columnSizes) {
            withColumnSizes(Arrays.asList(columnSizes));
            return this;
        }

        /**
         * Adds the given items to the value of "rowSizes"
         * 
         * @param rowSizes
         *     Items to add to the value of the "rowSizes" property
         */
        public WebTable.Builder<_B> addRowSizes(final Iterable<? extends BigInteger> rowSizes) {
            if (rowSizes!= null) {
                if (this.rowSizes == null) {
                    this.rowSizes = new ArrayList<Buildable>();
                }
                for (BigInteger _item: rowSizes) {
                    this.rowSizes.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "rowSizes" (any previous value will be replaced)
         * 
         * @param rowSizes
         *     New value of the "rowSizes" property.
         */
        public WebTable.Builder<_B> withRowSizes(final Iterable<? extends BigInteger> rowSizes) {
            if (this.rowSizes!= null) {
                this.rowSizes.clear();
            }
            return addRowSizes(rowSizes);
        }

        /**
         * Adds the given items to the value of "rowSizes"
         * 
         * @param rowSizes
         *     Items to add to the value of the "rowSizes" property
         */
        public WebTable.Builder<_B> addRowSizes(BigInteger... rowSizes) {
            addRowSizes(Arrays.asList(rowSizes));
            return this;
        }

        /**
         * Sets the new value of "rowSizes" (any previous value will be replaced)
         * 
         * @param rowSizes
         *     New value of the "rowSizes" property.
         */
        public WebTable.Builder<_B> withRowSizes(BigInteger... rowSizes) {
            withRowSizes(Arrays.asList(rowSizes));
            return this;
        }

        @Override
        public WebTable build() {
            if (_storedValue == null) {
                return this.init(new WebTable());
            } else {
                return ((WebTable) _storedValue);
            }
        }

        public WebTable.Builder<_B> copyOf(final WebTable _other) {
            _other.copyTo(this);
            return this;
        }

        public WebTable.Builder<_B> copyOf(final WebTable.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebTable.Selector<WebTable.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebTable.Select _root() {
            return new WebTable.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebRow.Selector<TRoot, WebTable.Selector<TRoot, TParent>> rows = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebTable.Selector<TRoot, TParent>> columnSizes = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebTable.Selector<TRoot, TParent>> rowSizes = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.rows!= null) {
                products.put("rows", this.rows.init());
            }
            if (this.columnSizes!= null) {
                products.put("columnSizes", this.columnSizes.init());
            }
            if (this.rowSizes!= null) {
                products.put("rowSizes", this.rowSizes.init());
            }
            return products;
        }

        public WebRow.Selector<TRoot, WebTable.Selector<TRoot, TParent>> rows() {
            return ((this.rows == null)?this.rows = new WebRow.Selector<TRoot, WebTable.Selector<TRoot, TParent>>(this._root, this, "rows"):this.rows);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebTable.Selector<TRoot, TParent>> columnSizes() {
            return ((this.columnSizes == null)?this.columnSizes = new com.kscs.util.jaxb.Selector<TRoot, WebTable.Selector<TRoot, TParent>>(this._root, this, "columnSizes"):this.columnSizes);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebTable.Selector<TRoot, TParent>> rowSizes() {
            return ((this.rowSizes == null)?this.rowSizes = new com.kscs.util.jaxb.Selector<TRoot, WebTable.Selector<TRoot, TParent>>(this._root, this, "rowSizes"):this.rowSizes);
        }

    }

}
