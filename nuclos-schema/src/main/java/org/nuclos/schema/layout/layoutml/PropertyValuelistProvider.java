
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}parameter" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="prop-name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="value" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="field" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "parameter"
})
public class PropertyValuelistProvider implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<Parameter> parameter;
    @XmlAttribute(name = "prop-name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String propName;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "type", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String type;
    @XmlAttribute(name = "value", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String value;
    @XmlAttribute(name = "entity")
    @XmlSchemaType(name = "anySimpleType")
    protected String entity;
    @XmlAttribute(name = "field")
    @XmlSchemaType(name = "anySimpleType")
    protected String field;

    /**
     * Gets the value of the parameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Parameter }
     * 
     * 
     */
    public List<Parameter> getParameter() {
        if (parameter == null) {
            parameter = new ArrayList<Parameter>();
        }
        return this.parameter;
    }

    /**
     * Gets the value of the propName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropName() {
        return propName;
    }

    /**
     * Sets the value of the propName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropName(String value) {
        this.propName = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the field property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField() {
        return field;
    }

    /**
     * Sets the value of the field property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField(String value) {
        this.field = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Parameter> theParameter;
            theParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            strategy.appendField(locator, this, "parameter", buffer, theParameter);
        }
        {
            String thePropName;
            thePropName = this.getPropName();
            strategy.appendField(locator, this, "propName", buffer, thePropName);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            String theField;
            theField = this.getField();
            strategy.appendField(locator, this, "field", buffer, theField);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PropertyValuelistProvider)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PropertyValuelistProvider that = ((PropertyValuelistProvider) object);
        {
            List<Parameter> lhsParameter;
            lhsParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            List<Parameter> rhsParameter;
            rhsParameter = (((that.parameter!= null)&&(!that.parameter.isEmpty()))?that.getParameter():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameter", lhsParameter), LocatorUtils.property(thatLocator, "parameter", rhsParameter), lhsParameter, rhsParameter)) {
                return false;
            }
        }
        {
            String lhsPropName;
            lhsPropName = this.getPropName();
            String rhsPropName;
            rhsPropName = that.getPropName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "propName", lhsPropName), LocatorUtils.property(thatLocator, "propName", rhsPropName), lhsPropName, rhsPropName)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            String lhsField;
            lhsField = this.getField();
            String rhsField;
            rhsField = that.getField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "field", lhsField), LocatorUtils.property(thatLocator, "field", rhsField), lhsField, rhsField)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Parameter> theParameter;
            theParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parameter", theParameter), currentHashCode, theParameter);
        }
        {
            String thePropName;
            thePropName = this.getPropName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "propName", thePropName), currentHashCode, thePropName);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            String theField;
            theField = this.getField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "field", theField), currentHashCode, theField);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PropertyValuelistProvider) {
            final PropertyValuelistProvider copy = ((PropertyValuelistProvider) draftCopy);
            if ((this.parameter!= null)&&(!this.parameter.isEmpty())) {
                List<Parameter> sourceParameter;
                sourceParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
                @SuppressWarnings("unchecked")
                List<Parameter> copyParameter = ((List<Parameter> ) strategy.copy(LocatorUtils.property(locator, "parameter", sourceParameter), sourceParameter));
                copy.parameter = null;
                if (copyParameter!= null) {
                    List<Parameter> uniqueParameterl = copy.getParameter();
                    uniqueParameterl.addAll(copyParameter);
                }
            } else {
                copy.parameter = null;
            }
            if (this.propName!= null) {
                String sourcePropName;
                sourcePropName = this.getPropName();
                String copyPropName = ((String) strategy.copy(LocatorUtils.property(locator, "propName", sourcePropName), sourcePropName));
                copy.setPropName(copyPropName);
            } else {
                copy.propName = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.field!= null) {
                String sourceField;
                sourceField = this.getField();
                String copyField = ((String) strategy.copy(LocatorUtils.property(locator, "field", sourceField), sourceField));
                copy.setField(copyField);
            } else {
                copy.field = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PropertyValuelistProvider();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PropertyValuelistProvider.Builder<_B> _other) {
        if (this.parameter == null) {
            _other.parameter = null;
        } else {
            _other.parameter = new ArrayList<Parameter.Builder<PropertyValuelistProvider.Builder<_B>>>();
            for (Parameter _item: this.parameter) {
                _other.parameter.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.propName = this.propName;
        _other.name = this.name;
        _other.type = this.type;
        _other.value = this.value;
        _other.entity = this.entity;
        _other.field = this.field;
    }

    public<_B >PropertyValuelistProvider.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PropertyValuelistProvider.Builder<_B>(_parentBuilder, this, true);
    }

    public PropertyValuelistProvider.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PropertyValuelistProvider.Builder<Void> builder() {
        return new PropertyValuelistProvider.Builder<Void>(null, null, false);
    }

    public static<_B >PropertyValuelistProvider.Builder<_B> copyOf(final PropertyValuelistProvider _other) {
        final PropertyValuelistProvider.Builder<_B> _newBuilder = new PropertyValuelistProvider.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PropertyValuelistProvider.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree parameterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameter"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterPropertyTree!= null):((parameterPropertyTree == null)||(!parameterPropertyTree.isLeaf())))) {
            if (this.parameter == null) {
                _other.parameter = null;
            } else {
                _other.parameter = new ArrayList<Parameter.Builder<PropertyValuelistProvider.Builder<_B>>>();
                for (Parameter _item: this.parameter) {
                    _other.parameter.add(((_item == null)?null:_item.newCopyBuilder(_other, parameterPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree propNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("propName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propNamePropertyTree!= null):((propNamePropertyTree == null)||(!propNamePropertyTree.isLeaf())))) {
            _other.propName = this.propName;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree fieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("field"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldPropertyTree!= null):((fieldPropertyTree == null)||(!fieldPropertyTree.isLeaf())))) {
            _other.field = this.field;
        }
    }

    public<_B >PropertyValuelistProvider.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PropertyValuelistProvider.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PropertyValuelistProvider.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PropertyValuelistProvider.Builder<_B> copyOf(final PropertyValuelistProvider _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyValuelistProvider.Builder<_B> _newBuilder = new PropertyValuelistProvider.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PropertyValuelistProvider.Builder<Void> copyExcept(final PropertyValuelistProvider _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PropertyValuelistProvider.Builder<Void> copyOnly(final PropertyValuelistProvider _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PropertyValuelistProvider _storedValue;
        private List<Parameter.Builder<PropertyValuelistProvider.Builder<_B>>> parameter;
        private String propName;
        private String name;
        private String type;
        private String value;
        private String entity;
        private String field;

        public Builder(final _B _parentBuilder, final PropertyValuelistProvider _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.parameter == null) {
                        this.parameter = null;
                    } else {
                        this.parameter = new ArrayList<Parameter.Builder<PropertyValuelistProvider.Builder<_B>>>();
                        for (Parameter _item: _other.parameter) {
                            this.parameter.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.propName = _other.propName;
                    this.name = _other.name;
                    this.type = _other.type;
                    this.value = _other.value;
                    this.entity = _other.entity;
                    this.field = _other.field;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PropertyValuelistProvider _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree parameterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameter"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterPropertyTree!= null):((parameterPropertyTree == null)||(!parameterPropertyTree.isLeaf())))) {
                        if (_other.parameter == null) {
                            this.parameter = null;
                        } else {
                            this.parameter = new ArrayList<Parameter.Builder<PropertyValuelistProvider.Builder<_B>>>();
                            for (Parameter _item: _other.parameter) {
                                this.parameter.add(((_item == null)?null:_item.newCopyBuilder(this, parameterPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree propNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("propName"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propNamePropertyTree!= null):((propNamePropertyTree == null)||(!propNamePropertyTree.isLeaf())))) {
                        this.propName = _other.propName;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree fieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("field"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldPropertyTree!= null):((fieldPropertyTree == null)||(!fieldPropertyTree.isLeaf())))) {
                        this.field = _other.field;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PropertyValuelistProvider >_P init(final _P _product) {
            if (this.parameter!= null) {
                final List<Parameter> parameter = new ArrayList<Parameter>(this.parameter.size());
                for (Parameter.Builder<PropertyValuelistProvider.Builder<_B>> _item: this.parameter) {
                    parameter.add(_item.build());
                }
                _product.parameter = parameter;
            }
            _product.propName = this.propName;
            _product.name = this.name;
            _product.type = this.type;
            _product.value = this.value;
            _product.entity = this.entity;
            _product.field = this.field;
            return _product;
        }

        /**
         * Adds the given items to the value of "parameter"
         * 
         * @param parameter
         *     Items to add to the value of the "parameter" property
         */
        public PropertyValuelistProvider.Builder<_B> addParameter(final Iterable<? extends Parameter> parameter) {
            if (parameter!= null) {
                if (this.parameter == null) {
                    this.parameter = new ArrayList<Parameter.Builder<PropertyValuelistProvider.Builder<_B>>>();
                }
                for (Parameter _item: parameter) {
                    this.parameter.add(new Parameter.Builder<PropertyValuelistProvider.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "parameter" (any previous value will be replaced)
         * 
         * @param parameter
         *     New value of the "parameter" property.
         */
        public PropertyValuelistProvider.Builder<_B> withParameter(final Iterable<? extends Parameter> parameter) {
            if (this.parameter!= null) {
                this.parameter.clear();
            }
            return addParameter(parameter);
        }

        /**
         * Adds the given items to the value of "parameter"
         * 
         * @param parameter
         *     Items to add to the value of the "parameter" property
         */
        public PropertyValuelistProvider.Builder<_B> addParameter(Parameter... parameter) {
            addParameter(Arrays.asList(parameter));
            return this;
        }

        /**
         * Sets the new value of "parameter" (any previous value will be replaced)
         * 
         * @param parameter
         *     New value of the "parameter" property.
         */
        public PropertyValuelistProvider.Builder<_B> withParameter(Parameter... parameter) {
            withParameter(Arrays.asList(parameter));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Parameter" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Parameter.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Parameter" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Parameter.Builder#end()} to return to the current builder.
         */
        public Parameter.Builder<? extends PropertyValuelistProvider.Builder<_B>> addParameter() {
            if (this.parameter == null) {
                this.parameter = new ArrayList<Parameter.Builder<PropertyValuelistProvider.Builder<_B>>>();
            }
            final Parameter.Builder<PropertyValuelistProvider.Builder<_B>> parameter_Builder = new Parameter.Builder<PropertyValuelistProvider.Builder<_B>>(this, null, false);
            this.parameter.add(parameter_Builder);
            return parameter_Builder;
        }

        /**
         * Sets the new value of "propName" (any previous value will be replaced)
         * 
         * @param propName
         *     New value of the "propName" property.
         */
        public PropertyValuelistProvider.Builder<_B> withPropName(final String propName) {
            this.propName = propName;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public PropertyValuelistProvider.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public PropertyValuelistProvider.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public PropertyValuelistProvider.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public PropertyValuelistProvider.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "field" (any previous value will be replaced)
         * 
         * @param field
         *     New value of the "field" property.
         */
        public PropertyValuelistProvider.Builder<_B> withField(final String field) {
            this.field = field;
            return this;
        }

        @Override
        public PropertyValuelistProvider build() {
            if (_storedValue == null) {
                return this.init(new PropertyValuelistProvider());
            } else {
                return ((PropertyValuelistProvider) _storedValue);
            }
        }

        public PropertyValuelistProvider.Builder<_B> copyOf(final PropertyValuelistProvider _other) {
            _other.copyTo(this);
            return this;
        }

        public PropertyValuelistProvider.Builder<_B> copyOf(final PropertyValuelistProvider.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PropertyValuelistProvider.Selector<PropertyValuelistProvider.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PropertyValuelistProvider.Select _root() {
            return new PropertyValuelistProvider.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Parameter.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> parameter = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> propName = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> value = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> field = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.parameter!= null) {
                products.put("parameter", this.parameter.init());
            }
            if (this.propName!= null) {
                products.put("propName", this.propName.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.field!= null) {
                products.put("field", this.field.init());
            }
            return products;
        }

        public Parameter.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> parameter() {
            return ((this.parameter == null)?this.parameter = new Parameter.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "parameter"):this.parameter);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> propName() {
            return ((this.propName == null)?this.propName = new com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "propName"):this.propName);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>> field() {
            return ((this.field == null)?this.field = new com.kscs.util.jaxb.Selector<TRoot, PropertyValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "field"):this.field);
        }

    }

}
