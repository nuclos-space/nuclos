
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * An optiongroup.
 * 
 * <p>Java class for web-optiongroup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-optiongroup"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-input-component"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="options" type="{urn:org.nuclos.schema.layout.web}web-options"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-optiongroup", propOrder = {
    "options"
})
public class WebOptiongroup
    extends WebInputComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected WebOptions options;

    /**
     * Gets the value of the options property.
     * 
     * @return
     *     possible object is
     *     {@link WebOptions }
     *     
     */
    public WebOptions getOptions() {
        return options;
    }

    /**
     * Sets the value of the options property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebOptions }
     *     
     */
    public void setOptions(WebOptions value) {
        this.options = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            WebOptions theOptions;
            theOptions = this.getOptions();
            strategy.appendField(locator, this, "options", buffer, theOptions);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebOptiongroup)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebOptiongroup that = ((WebOptiongroup) object);
        {
            WebOptions lhsOptions;
            lhsOptions = this.getOptions();
            WebOptions rhsOptions;
            rhsOptions = that.getOptions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "options", lhsOptions), LocatorUtils.property(thatLocator, "options", rhsOptions), lhsOptions, rhsOptions)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            WebOptions theOptions;
            theOptions = this.getOptions();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "options", theOptions), currentHashCode, theOptions);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebOptiongroup) {
            final WebOptiongroup copy = ((WebOptiongroup) draftCopy);
            if (this.options!= null) {
                WebOptions sourceOptions;
                sourceOptions = this.getOptions();
                WebOptions copyOptions = ((WebOptions) strategy.copy(LocatorUtils.property(locator, "options", sourceOptions), sourceOptions));
                copy.setOptions(copyOptions);
            } else {
                copy.options = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebOptiongroup();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebOptiongroup.Builder<_B> _other) {
        super.copyTo(_other);
        _other.options = ((this.options == null)?null:this.options.newCopyBuilder(_other));
    }

    @Override
    public<_B >WebOptiongroup.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebOptiongroup.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebOptiongroup.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebOptiongroup.Builder<Void> builder() {
        return new WebOptiongroup.Builder<Void>(null, null, false);
    }

    public static<_B >WebOptiongroup.Builder<_B> copyOf(final WebComponent _other) {
        final WebOptiongroup.Builder<_B> _newBuilder = new WebOptiongroup.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebOptiongroup.Builder<_B> copyOf(final WebInputComponent _other) {
        final WebOptiongroup.Builder<_B> _newBuilder = new WebOptiongroup.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebOptiongroup.Builder<_B> copyOf(final WebOptiongroup _other) {
        final WebOptiongroup.Builder<_B> _newBuilder = new WebOptiongroup.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebOptiongroup.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree optionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("options"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionsPropertyTree!= null):((optionsPropertyTree == null)||(!optionsPropertyTree.isLeaf())))) {
            _other.options = ((this.options == null)?null:this.options.newCopyBuilder(_other, optionsPropertyTree, _propertyTreeUse));
        }
    }

    @Override
    public<_B >WebOptiongroup.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebOptiongroup.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebOptiongroup.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebOptiongroup.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebOptiongroup.Builder<_B> _newBuilder = new WebOptiongroup.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebOptiongroup.Builder<_B> copyOf(final WebInputComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebOptiongroup.Builder<_B> _newBuilder = new WebOptiongroup.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebOptiongroup.Builder<_B> copyOf(final WebOptiongroup _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebOptiongroup.Builder<_B> _newBuilder = new WebOptiongroup.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebOptiongroup.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebOptiongroup.Builder<Void> copyExcept(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebOptiongroup.Builder<Void> copyExcept(final WebOptiongroup _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebOptiongroup.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebOptiongroup.Builder<Void> copyOnly(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebOptiongroup.Builder<Void> copyOnly(final WebOptiongroup _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebInputComponent.Builder<_B>
        implements Buildable
    {

        private WebOptions.Builder<WebOptiongroup.Builder<_B>> options;

        public Builder(final _B _parentBuilder, final WebOptiongroup _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.options = ((_other.options == null)?null:_other.options.newCopyBuilder(this));
            }
        }

        public Builder(final _B _parentBuilder, final WebOptiongroup _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree optionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("options"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionsPropertyTree!= null):((optionsPropertyTree == null)||(!optionsPropertyTree.isLeaf())))) {
                    this.options = ((_other.options == null)?null:_other.options.newCopyBuilder(this, optionsPropertyTree, _propertyTreeUse));
                }
            }
        }

        protected<_P extends WebOptiongroup >_P init(final _P _product) {
            _product.options = ((this.options == null)?null:this.options.build());
            return super.init(_product);
        }

        /**
         * Sets the new value of "options" (any previous value will be replaced)
         * 
         * @param options
         *     New value of the "options" property.
         */
        public WebOptiongroup.Builder<_B> withOptions(final WebOptions options) {
            this.options = ((options == null)?null:new WebOptions.Builder<WebOptiongroup.Builder<_B>>(this, options, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "options" property.
         * Use {@link org.nuclos.schema.layout.web.WebOptions.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "options" property.
         *     Use {@link org.nuclos.schema.layout.web.WebOptions.Builder#end()} to return to the current builder.
         */
        public WebOptions.Builder<? extends WebOptiongroup.Builder<_B>> withOptions() {
            if (this.options!= null) {
                return this.options;
            }
            return this.options = new WebOptions.Builder<WebOptiongroup.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            super.withValuelistProvider(valuelistProvider);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebOptiongroup.Builder<_B>> withValuelistProvider() {
            return ((WebValuelistProvider.Builder<? extends WebOptiongroup.Builder<_B>> ) super.withValuelistProvider());
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withEnabled(final Boolean enabled) {
            super.withEnabled(enabled);
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withEditable(final Boolean editable) {
            super.withEditable(editable);
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withInsertable(final Boolean insertable) {
            super.withInsertable(insertable);
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withColumns(final String columns) {
            super.withColumns(columns);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebOptiongroup.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebOptiongroup.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebOptiongroup.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebOptiongroup.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebOptiongroup.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebOptiongroup build() {
            if (_storedValue == null) {
                return this.init(new WebOptiongroup());
            } else {
                return ((WebOptiongroup) _storedValue);
            }
        }

        public WebOptiongroup.Builder<_B> copyOf(final WebOptiongroup _other) {
            _other.copyTo(this);
            return this;
        }

        public WebOptiongroup.Builder<_B> copyOf(final WebOptiongroup.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebOptiongroup.Selector<WebOptiongroup.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebOptiongroup.Select _root() {
            return new WebOptiongroup.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebInputComponent.Selector<TRoot, TParent>
    {

        private WebOptions.Selector<TRoot, WebOptiongroup.Selector<TRoot, TParent>> options = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.options!= null) {
                products.put("options", this.options.init());
            }
            return products;
        }

        public WebOptions.Selector<TRoot, WebOptiongroup.Selector<TRoot, TParent>> options() {
            return ((this.options == null)?this.options = new WebOptions.Selector<TRoot, WebOptiongroup.Selector<TRoot, TParent>>(this._root, this, "options"):this.options);
        }

    }

}
