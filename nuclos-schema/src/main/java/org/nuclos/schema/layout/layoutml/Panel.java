
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;element ref="{}layoutmanager" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}font" minOccurs="0"/&gt;
 *         &lt;element ref="{}background" minOccurs="0"/&gt;
 *         &lt;element ref="{}description" minOccurs="0"/&gt;
 *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element ref="{}container"/&gt;
 *           &lt;element ref="{}label"/&gt;
 *           &lt;element ref="{}textfield"/&gt;
 *           &lt;element ref="{}textarea"/&gt;
 *           &lt;element ref="{}combobox"/&gt;
 *           &lt;element ref="{}button"/&gt;
 *           &lt;element ref="{}collectable-component"/&gt;
 *           &lt;element ref="{}separator"/&gt;
 *           &lt;element ref="{}titled-separator"/&gt;
 *           &lt;element ref="{}layoutcomponent"/&gt;
 *           &lt;element ref="{}webaddon"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="visible" type="{}boolean" /&gt;
 *       &lt;attribute name="opaque" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "layoutmanager",
    "clearBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "font",
    "background",
    "description",
    "containerOrLabelOrTextfield"
})
public class Panel implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElementRef(name = "layoutmanager", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutmanager;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElementRef(name = "border", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    protected Font font;
    protected Background background;
    protected String description;
    @XmlElementRefs({
        @XmlElementRef(name = "container", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "label", type = Label.class, required = false),
        @XmlElementRef(name = "textfield", type = Textfield.class, required = false),
        @XmlElementRef(name = "textarea", type = Textarea.class, required = false),
        @XmlElementRef(name = "combobox", type = Combobox.class, required = false),
        @XmlElementRef(name = "button", type = Button.class, required = false),
        @XmlElementRef(name = "collectable-component", type = CollectableComponent.class, required = false),
        @XmlElementRef(name = "separator", type = Separator.class, required = false),
        @XmlElementRef(name = "titled-separator", type = TitledSeparator.class, required = false),
        @XmlElementRef(name = "layoutcomponent", type = Layoutcomponent.class, required = false),
        @XmlElementRef(name = "webaddon", type = Webaddon.class, required = false)
    })
    protected List<Serializable> containerOrLabelOrTextfield;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "visible")
    protected Boolean visible;
    @XmlAttribute(name = "opaque")
    protected Boolean opaque;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the layoutmanager property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutmanager() {
        return layoutmanager;
    }

    /**
     * Sets the value of the layoutmanager property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutmanager(JAXBElement<?> value) {
        this.layoutmanager = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<JAXBElement<?>> getBorder() {
        if (border == null) {
            border = new ArrayList<JAXBElement<?>>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the font property.
     * 
     * @return
     *     possible object is
     *     {@link Font }
     *     
     */
    public Font getFont() {
        return font;
    }

    /**
     * Sets the value of the font property.
     * 
     * @param value
     *     allowed object is
     *     {@link Font }
     *     
     */
    public void setFont(Font value) {
        this.font = value;
    }

    /**
     * Gets the value of the background property.
     * 
     * @return
     *     possible object is
     *     {@link Background }
     *     
     */
    public Background getBackground() {
        return background;
    }

    /**
     * Sets the value of the background property.
     * 
     * @param value
     *     allowed object is
     *     {@link Background }
     *     
     */
    public void setBackground(Background value) {
        this.background = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the containerOrLabelOrTextfield property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the containerOrLabelOrTextfield property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContainerOrLabelOrTextfield().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * {@link Label }
     * {@link Textfield }
     * {@link Textarea }
     * {@link Combobox }
     * {@link Button }
     * {@link CollectableComponent }
     * {@link Separator }
     * {@link TitledSeparator }
     * {@link Layoutcomponent }
     * {@link Webaddon }
     * 
     * 
     */
    public List<Serializable> getContainerOrLabelOrTextfield() {
        if (containerOrLabelOrTextfield == null) {
            containerOrLabelOrTextfield = new ArrayList<Serializable>();
        }
        return this.containerOrLabelOrTextfield;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the visible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVisible() {
        return visible;
    }

    /**
     * Sets the value of the visible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisible(Boolean value) {
        this.visible = value;
    }

    /**
     * Gets the value of the opaque property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpaque() {
        return opaque;
    }

    /**
     * Sets the value of the opaque property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpaque(Boolean value) {
        this.opaque = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            JAXBElement<?> theLayoutmanager;
            theLayoutmanager = this.getLayoutmanager();
            strategy.appendField(locator, this, "layoutmanager", buffer, theLayoutmanager);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            strategy.appendField(locator, this, "font", buffer, theFont);
        }
        {
            Background theBackground;
            theBackground = this.getBackground();
            strategy.appendField(locator, this, "background", buffer, theBackground);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            List<Serializable> theContainerOrLabelOrTextfield;
            theContainerOrLabelOrTextfield = (((this.containerOrLabelOrTextfield!= null)&&(!this.containerOrLabelOrTextfield.isEmpty()))?this.getContainerOrLabelOrTextfield():null);
            strategy.appendField(locator, this, "containerOrLabelOrTextfield", buffer, theContainerOrLabelOrTextfield);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            Boolean theVisible;
            theVisible = this.getVisible();
            strategy.appendField(locator, this, "visible", buffer, theVisible);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            strategy.appendField(locator, this, "opaque", buffer, theOpaque);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Panel)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Panel that = ((Panel) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            JAXBElement<?> lhsLayoutmanager;
            lhsLayoutmanager = this.getLayoutmanager();
            JAXBElement<?> rhsLayoutmanager;
            rhsLayoutmanager = that.getLayoutmanager();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutmanager", lhsLayoutmanager), LocatorUtils.property(thatLocator, "layoutmanager", rhsLayoutmanager), lhsLayoutmanager, rhsLayoutmanager)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<JAXBElement<?>> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            Font lhsFont;
            lhsFont = this.getFont();
            Font rhsFont;
            rhsFont = that.getFont();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "font", lhsFont), LocatorUtils.property(thatLocator, "font", rhsFont), lhsFont, rhsFont)) {
                return false;
            }
        }
        {
            Background lhsBackground;
            lhsBackground = this.getBackground();
            Background rhsBackground;
            rhsBackground = that.getBackground();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "background", lhsBackground), LocatorUtils.property(thatLocator, "background", rhsBackground), lhsBackground, rhsBackground)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            List<Serializable> lhsContainerOrLabelOrTextfield;
            lhsContainerOrLabelOrTextfield = (((this.containerOrLabelOrTextfield!= null)&&(!this.containerOrLabelOrTextfield.isEmpty()))?this.getContainerOrLabelOrTextfield():null);
            List<Serializable> rhsContainerOrLabelOrTextfield;
            rhsContainerOrLabelOrTextfield = (((that.containerOrLabelOrTextfield!= null)&&(!that.containerOrLabelOrTextfield.isEmpty()))?that.getContainerOrLabelOrTextfield():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "containerOrLabelOrTextfield", lhsContainerOrLabelOrTextfield), LocatorUtils.property(thatLocator, "containerOrLabelOrTextfield", rhsContainerOrLabelOrTextfield), lhsContainerOrLabelOrTextfield, rhsContainerOrLabelOrTextfield)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            Boolean lhsVisible;
            lhsVisible = this.getVisible();
            Boolean rhsVisible;
            rhsVisible = that.getVisible();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "visible", lhsVisible), LocatorUtils.property(thatLocator, "visible", rhsVisible), lhsVisible, rhsVisible)) {
                return false;
            }
        }
        {
            Boolean lhsOpaque;
            lhsOpaque = this.getOpaque();
            Boolean rhsOpaque;
            rhsOpaque = that.getOpaque();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opaque", lhsOpaque), LocatorUtils.property(thatLocator, "opaque", rhsOpaque), lhsOpaque, rhsOpaque)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            JAXBElement<?> theLayoutmanager;
            theLayoutmanager = this.getLayoutmanager();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutmanager", theLayoutmanager), currentHashCode, theLayoutmanager);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "font", theFont), currentHashCode, theFont);
        }
        {
            Background theBackground;
            theBackground = this.getBackground();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "background", theBackground), currentHashCode, theBackground);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            List<Serializable> theContainerOrLabelOrTextfield;
            theContainerOrLabelOrTextfield = (((this.containerOrLabelOrTextfield!= null)&&(!this.containerOrLabelOrTextfield.isEmpty()))?this.getContainerOrLabelOrTextfield():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "containerOrLabelOrTextfield", theContainerOrLabelOrTextfield), currentHashCode, theContainerOrLabelOrTextfield);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            Boolean theVisible;
            theVisible = this.getVisible();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "visible", theVisible), currentHashCode, theVisible);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opaque", theOpaque), currentHashCode, theOpaque);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Panel) {
            final Panel copy = ((Panel) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.layoutmanager!= null) {
                JAXBElement<?> sourceLayoutmanager;
                sourceLayoutmanager = this.getLayoutmanager();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutmanager = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutmanager", sourceLayoutmanager), sourceLayoutmanager));
                copy.setLayoutmanager(copyLayoutmanager);
            } else {
                copy.layoutmanager = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<JAXBElement<?>> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyBorder = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<JAXBElement<?>> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if (this.font!= null) {
                Font sourceFont;
                sourceFont = this.getFont();
                Font copyFont = ((Font) strategy.copy(LocatorUtils.property(locator, "font", sourceFont), sourceFont));
                copy.setFont(copyFont);
            } else {
                copy.font = null;
            }
            if (this.background!= null) {
                Background sourceBackground;
                sourceBackground = this.getBackground();
                Background copyBackground = ((Background) strategy.copy(LocatorUtils.property(locator, "background", sourceBackground), sourceBackground));
                copy.setBackground(copyBackground);
            } else {
                copy.background = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if ((this.containerOrLabelOrTextfield!= null)&&(!this.containerOrLabelOrTextfield.isEmpty())) {
                List<Serializable> sourceContainerOrLabelOrTextfield;
                sourceContainerOrLabelOrTextfield = (((this.containerOrLabelOrTextfield!= null)&&(!this.containerOrLabelOrTextfield.isEmpty()))?this.getContainerOrLabelOrTextfield():null);
                @SuppressWarnings("unchecked")
                List<Serializable> copyContainerOrLabelOrTextfield = ((List<Serializable> ) strategy.copy(LocatorUtils.property(locator, "containerOrLabelOrTextfield", sourceContainerOrLabelOrTextfield), sourceContainerOrLabelOrTextfield));
                copy.containerOrLabelOrTextfield = null;
                if (copyContainerOrLabelOrTextfield!= null) {
                    List<Serializable> uniqueContainerOrLabelOrTextfieldl = copy.getContainerOrLabelOrTextfield();
                    uniqueContainerOrLabelOrTextfieldl.addAll(copyContainerOrLabelOrTextfield);
                }
            } else {
                copy.containerOrLabelOrTextfield = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.visible!= null) {
                Boolean sourceVisible;
                sourceVisible = this.getVisible();
                Boolean copyVisible = ((Boolean) strategy.copy(LocatorUtils.property(locator, "visible", sourceVisible), sourceVisible));
                copy.setVisible(copyVisible);
            } else {
                copy.visible = null;
            }
            if (this.opaque!= null) {
                Boolean sourceOpaque;
                sourceOpaque = this.getOpaque();
                Boolean copyOpaque = ((Boolean) strategy.copy(LocatorUtils.property(locator, "opaque", sourceOpaque), sourceOpaque));
                copy.setOpaque(copyOpaque);
            } else {
                copy.opaque = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Panel();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Panel.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.layoutmanager = this.layoutmanager;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other));
        _other.background = ((this.background == null)?null:this.background.newCopyBuilder(_other));
        _other.description = this.description;
        if (this.containerOrLabelOrTextfield == null) {
            _other.containerOrLabelOrTextfield = null;
        } else {
            _other.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            for (Serializable _item: this.containerOrLabelOrTextfield) {
                _other.containerOrLabelOrTextfield.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.name = this.name;
        _other.visible = this.visible;
        _other.opaque = this.opaque;
    }

    public<_B >Panel.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Panel.Builder<_B>(_parentBuilder, this, true);
    }

    public Panel.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Panel.Builder<Void> builder() {
        return new Panel.Builder<Void>(null, null, false);
    }

    public static<_B >Panel.Builder<_B> copyOf(final Panel _other) {
        final Panel.Builder<_B> _newBuilder = new Panel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Panel.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree layoutmanagerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutmanager"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutmanagerPropertyTree!= null):((layoutmanagerPropertyTree == null)||(!layoutmanagerPropertyTree.isLeaf())))) {
            _other.layoutmanager = this.layoutmanager;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
            _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other, fontPropertyTree, _propertyTreeUse));
        }
        final PropertyTree backgroundPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("background"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundPropertyTree!= null):((backgroundPropertyTree == null)||(!backgroundPropertyTree.isLeaf())))) {
            _other.background = ((this.background == null)?null:this.background.newCopyBuilder(_other, backgroundPropertyTree, _propertyTreeUse));
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree containerOrLabelOrTextfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("containerOrLabelOrTextfield"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(containerOrLabelOrTextfieldPropertyTree!= null):((containerOrLabelOrTextfieldPropertyTree == null)||(!containerOrLabelOrTextfieldPropertyTree.isLeaf())))) {
            if (this.containerOrLabelOrTextfield == null) {
                _other.containerOrLabelOrTextfield = null;
            } else {
                _other.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                for (Serializable _item: this.containerOrLabelOrTextfield) {
                    _other.containerOrLabelOrTextfield.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree visiblePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("visible"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(visiblePropertyTree!= null):((visiblePropertyTree == null)||(!visiblePropertyTree.isLeaf())))) {
            _other.visible = this.visible;
        }
        final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
            _other.opaque = this.opaque;
        }
    }

    public<_B >Panel.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Panel.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Panel.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Panel.Builder<_B> copyOf(final Panel _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Panel.Builder<_B> _newBuilder = new Panel.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Panel.Builder<Void> copyExcept(final Panel _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Panel.Builder<Void> copyOnly(final Panel _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Panel _storedValue;
        private JAXBElement<?> layoutconstraints;
        private JAXBElement<?> layoutmanager;
        private ClearBorder.Builder<Panel.Builder<_B>> clearBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<Panel.Builder<_B>> minimumSize;
        private PreferredSize.Builder<Panel.Builder<_B>> preferredSize;
        private StrictSize.Builder<Panel.Builder<_B>> strictSize;
        private Font.Builder<Panel.Builder<_B>> font;
        private Background.Builder<Panel.Builder<_B>> background;
        private String description;
        private List<Buildable> containerOrLabelOrTextfield;
        private String name;
        private Boolean visible;
        private Boolean opaque;

        public Builder(final _B _parentBuilder, final Panel _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.layoutmanager = _other.layoutmanager;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this));
                    this.background = ((_other.background == null)?null:_other.background.newCopyBuilder(this));
                    this.description = _other.description;
                    if (_other.containerOrLabelOrTextfield == null) {
                        this.containerOrLabelOrTextfield = null;
                    } else {
                        this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                        for (Serializable _item: _other.containerOrLabelOrTextfield) {
                            this.containerOrLabelOrTextfield.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.name = _other.name;
                    this.visible = _other.visible;
                    this.opaque = _other.opaque;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Panel _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree layoutmanagerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutmanager"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutmanagerPropertyTree!= null):((layoutmanagerPropertyTree == null)||(!layoutmanagerPropertyTree.isLeaf())))) {
                        this.layoutmanager = _other.layoutmanager;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
                        this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this, fontPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree backgroundPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("background"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundPropertyTree!= null):((backgroundPropertyTree == null)||(!backgroundPropertyTree.isLeaf())))) {
                        this.background = ((_other.background == null)?null:_other.background.newCopyBuilder(this, backgroundPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree containerOrLabelOrTextfieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("containerOrLabelOrTextfield"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(containerOrLabelOrTextfieldPropertyTree!= null):((containerOrLabelOrTextfieldPropertyTree == null)||(!containerOrLabelOrTextfieldPropertyTree.isLeaf())))) {
                        if (_other.containerOrLabelOrTextfield == null) {
                            this.containerOrLabelOrTextfield = null;
                        } else {
                            this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                            for (Serializable _item: _other.containerOrLabelOrTextfield) {
                                this.containerOrLabelOrTextfield.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree visiblePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("visible"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(visiblePropertyTree!= null):((visiblePropertyTree == null)||(!visiblePropertyTree.isLeaf())))) {
                        this.visible = _other.visible;
                    }
                    final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
                        this.opaque = _other.opaque;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Panel >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.layoutmanager = this.layoutmanager;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            if (this.border!= null) {
                final List<JAXBElement<?>> border = new ArrayList<JAXBElement<?>>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((JAXBElement<?> ) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            _product.font = ((this.font == null)?null:this.font.build());
            _product.background = ((this.background == null)?null:this.background.build());
            _product.description = this.description;
            if (this.containerOrLabelOrTextfield!= null) {
                final List<Serializable> containerOrLabelOrTextfield = new ArrayList<Serializable>(this.containerOrLabelOrTextfield.size());
                for (Buildable _item: this.containerOrLabelOrTextfield) {
                    containerOrLabelOrTextfield.add(((Serializable) _item.build()));
                }
                _product.containerOrLabelOrTextfield = containerOrLabelOrTextfield;
            }
            _product.name = this.name;
            _product.visible = this.visible;
            _product.opaque = this.opaque;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public Panel.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "layoutmanager" (any previous value will be replaced)
         * 
         * @param layoutmanager
         *     New value of the "layoutmanager" property.
         */
        public Panel.Builder<_B> withLayoutmanager(final JAXBElement<?> layoutmanager) {
            this.layoutmanager = layoutmanager;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public Panel.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<Panel.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Panel.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<Panel.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Panel.Builder<_B> addBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Panel.Builder<_B> withBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Panel.Builder<_B> addBorder(JAXBElement<?> ... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Panel.Builder<_B> withBorder(JAXBElement<?> ... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public Panel.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<Panel.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Panel.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<Panel.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public Panel.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<Panel.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Panel.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<Panel.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public Panel.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<Panel.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Panel.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<Panel.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "font" (any previous value will be replaced)
         * 
         * @param font
         *     New value of the "font" property.
         */
        public Panel.Builder<_B> withFont(final Font font) {
            this.font = ((font == null)?null:new Font.Builder<Panel.Builder<_B>>(this, font, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "font" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "font" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         */
        public Font.Builder<? extends Panel.Builder<_B>> withFont() {
            if (this.font!= null) {
                return this.font;
            }
            return this.font = new Font.Builder<Panel.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "background" (any previous value will be replaced)
         * 
         * @param background
         *     New value of the "background" property.
         */
        public Panel.Builder<_B> withBackground(final Background background) {
            this.background = ((background == null)?null:new Background.Builder<Panel.Builder<_B>>(this, background, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "background" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Background.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "background" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Background.Builder#end()} to return to the current builder.
         */
        public Background.Builder<? extends Panel.Builder<_B>> withBackground() {
            if (this.background!= null) {
                return this.background;
            }
            return this.background = new Background.Builder<Panel.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public Panel.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Adds the given items to the value of "containerOrLabelOrTextfield"
         * 
         * @param containerOrLabelOrTextfield
         *     Items to add to the value of the "containerOrLabelOrTextfield" property
         */
        public Panel.Builder<_B> addContainerOrLabelOrTextfield(final Iterable<? extends Serializable> containerOrLabelOrTextfield) {
            if (containerOrLabelOrTextfield!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Serializable _item: containerOrLabelOrTextfield) {
                    this.containerOrLabelOrTextfield.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "containerOrLabelOrTextfield" (any previous value will be replaced)
         * 
         * @param containerOrLabelOrTextfield
         *     New value of the "containerOrLabelOrTextfield" property.
         */
        public Panel.Builder<_B> withContainerOrLabelOrTextfield(final Iterable<? extends Serializable> containerOrLabelOrTextfield) {
            if (this.containerOrLabelOrTextfield!= null) {
                this.containerOrLabelOrTextfield.clear();
            }
            return addContainerOrLabelOrTextfield(containerOrLabelOrTextfield);
        }

        /**
         * Adds the given items to the value of "containerOrLabelOrTextfield"
         * 
         * @param containerOrLabelOrTextfield
         *     Items to add to the value of the "containerOrLabelOrTextfield" property
         */
        public Panel.Builder<_B> addContainerOrLabelOrTextfield(Serializable... containerOrLabelOrTextfield) {
            addContainerOrLabelOrTextfield(Arrays.asList(containerOrLabelOrTextfield));
            return this;
        }

        /**
         * Sets the new value of "containerOrLabelOrTextfield" (any previous value will be replaced)
         * 
         * @param containerOrLabelOrTextfield
         *     New value of the "containerOrLabelOrTextfield" property.
         */
        public Panel.Builder<_B> withContainerOrLabelOrTextfield(Serializable... containerOrLabelOrTextfield) {
            withContainerOrLabelOrTextfield(Arrays.asList(containerOrLabelOrTextfield));
            return this;
        }

        /**
         * Adds the given items to the value of "container_"
         * 
         * @param container_
         *     Items to add to the value of the "container_" property
         */
        public Panel.Builder<_B> addContainer(final Iterable<? extends JAXBElement<Object>> container_) {
            return addContainerOrLabelOrTextfield(container_);
        }

        /**
         * Adds the given items to the value of "container_"
         * 
         * @param container_
         *     Items to add to the value of the "container_" property
         */
        public Panel.Builder<_B> addContainer(JAXBElement<Object> ... container_) {
            return addContainer(Arrays.asList(container_));
        }

        /**
         * Adds the given items to the value of "label_"
         * 
         * @param label_
         *     Items to add to the value of the "label_" property
         */
        public Panel.Builder<_B> addLabel(final Iterable<? extends Label> label_) {
            if (label_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Label _item: label_) {
                    this.containerOrLabelOrTextfield.add(new Label.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "label_"
         * 
         * @param label_
         *     Items to add to the value of the "label_" property
         */
        public Panel.Builder<_B> addLabel(Label... label_) {
            return addLabel(Arrays.asList(label_));
        }

        /**
         * Returns a new builder to build an additional value of the "label" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Label.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "label" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Label.Builder#end()} to return to the current builder.
         */
        public Label.Builder<? extends Panel.Builder<_B>> addLabel() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final Label.Builder<Panel.Builder<_B>> label_Builder = new Label.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(label_Builder);
            return label_Builder;
        }

        /**
         * Adds the given items to the value of "textfield_"
         * 
         * @param textfield_
         *     Items to add to the value of the "textfield_" property
         */
        public Panel.Builder<_B> addTextfield(final Iterable<? extends Textfield> textfield_) {
            if (textfield_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Textfield _item: textfield_) {
                    this.containerOrLabelOrTextfield.add(new Textfield.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "textfield_"
         * 
         * @param textfield_
         *     Items to add to the value of the "textfield_" property
         */
        public Panel.Builder<_B> addTextfield(Textfield... textfield_) {
            return addTextfield(Arrays.asList(textfield_));
        }

        /**
         * Returns a new builder to build an additional value of the "textfield" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Textfield.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "textfield" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Textfield.Builder#end()} to return to the current builder.
         */
        public Textfield.Builder<? extends Panel.Builder<_B>> addTextfield() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final Textfield.Builder<Panel.Builder<_B>> textfield_Builder = new Textfield.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(textfield_Builder);
            return textfield_Builder;
        }

        /**
         * Adds the given items to the value of "textarea_"
         * 
         * @param textarea_
         *     Items to add to the value of the "textarea_" property
         */
        public Panel.Builder<_B> addTextarea(final Iterable<? extends Textarea> textarea_) {
            if (textarea_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Textarea _item: textarea_) {
                    this.containerOrLabelOrTextfield.add(new Textarea.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "textarea_"
         * 
         * @param textarea_
         *     Items to add to the value of the "textarea_" property
         */
        public Panel.Builder<_B> addTextarea(Textarea... textarea_) {
            return addTextarea(Arrays.asList(textarea_));
        }

        /**
         * Returns a new builder to build an additional value of the "textarea" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Textarea.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "textarea" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Textarea.Builder#end()} to return to the current builder.
         */
        public Textarea.Builder<? extends Panel.Builder<_B>> addTextarea() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final Textarea.Builder<Panel.Builder<_B>> textarea_Builder = new Textarea.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(textarea_Builder);
            return textarea_Builder;
        }

        /**
         * Adds the given items to the value of "combobox_"
         * 
         * @param combobox_
         *     Items to add to the value of the "combobox_" property
         */
        public Panel.Builder<_B> addCombobox(final Iterable<? extends Combobox> combobox_) {
            if (combobox_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Combobox _item: combobox_) {
                    this.containerOrLabelOrTextfield.add(new Combobox.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "combobox_"
         * 
         * @param combobox_
         *     Items to add to the value of the "combobox_" property
         */
        public Panel.Builder<_B> addCombobox(Combobox... combobox_) {
            return addCombobox(Arrays.asList(combobox_));
        }

        /**
         * Returns a new builder to build an additional value of the "combobox" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Combobox.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "combobox" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Combobox.Builder#end()} to return to the current builder.
         */
        public Combobox.Builder<? extends Panel.Builder<_B>> addCombobox() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final Combobox.Builder<Panel.Builder<_B>> combobox_Builder = new Combobox.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(combobox_Builder);
            return combobox_Builder;
        }

        /**
         * Adds the given items to the value of "button_"
         * 
         * @param button_
         *     Items to add to the value of the "button_" property
         */
        public Panel.Builder<_B> addButton(final Iterable<? extends Button> button_) {
            if (button_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Button _item: button_) {
                    this.containerOrLabelOrTextfield.add(new Button.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "button_"
         * 
         * @param button_
         *     Items to add to the value of the "button_" property
         */
        public Panel.Builder<_B> addButton(Button... button_) {
            return addButton(Arrays.asList(button_));
        }

        /**
         * Returns a new builder to build an additional value of the "button" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Button.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "button" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Button.Builder#end()} to return to the current builder.
         */
        public Button.Builder<? extends Panel.Builder<_B>> addButton() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final Button.Builder<Panel.Builder<_B>> button_Builder = new Button.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(button_Builder);
            return button_Builder;
        }

        /**
         * Adds the given items to the value of "collectableComponent_"
         * 
         * @param collectableComponent_
         *     Items to add to the value of the "collectableComponent_" property
         */
        public Panel.Builder<_B> addCollectableComponent(final Iterable<? extends CollectableComponent> collectableComponent_) {
            if (collectableComponent_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (CollectableComponent _item: collectableComponent_) {
                    this.containerOrLabelOrTextfield.add(new CollectableComponent.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "collectableComponent_"
         * 
         * @param collectableComponent_
         *     Items to add to the value of the "collectableComponent_" property
         */
        public Panel.Builder<_B> addCollectableComponent(CollectableComponent... collectableComponent_) {
            return addCollectableComponent(Arrays.asList(collectableComponent_));
        }

        /**
         * Returns a new builder to build an additional value of the "collectableComponent" property.
         * Use {@link org.nuclos.schema.layout.layoutml.CollectableComponent.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "collectableComponent" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.CollectableComponent.Builder#end()} to return to the current builder.
         */
        public CollectableComponent.Builder<? extends Panel.Builder<_B>> addCollectableComponent() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final CollectableComponent.Builder<Panel.Builder<_B>> collectableComponent_Builder = new CollectableComponent.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(collectableComponent_Builder);
            return collectableComponent_Builder;
        }

        /**
         * Adds the given items to the value of "separator_"
         * 
         * @param separator_
         *     Items to add to the value of the "separator_" property
         */
        public Panel.Builder<_B> addSeparator(final Iterable<? extends Separator> separator_) {
            if (separator_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Separator _item: separator_) {
                    this.containerOrLabelOrTextfield.add(new Separator.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "separator_"
         * 
         * @param separator_
         *     Items to add to the value of the "separator_" property
         */
        public Panel.Builder<_B> addSeparator(Separator... separator_) {
            return addSeparator(Arrays.asList(separator_));
        }

        /**
         * Returns a new builder to build an additional value of the "separator" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Separator.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "separator" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Separator.Builder#end()} to return to the current builder.
         */
        public Separator.Builder<? extends Panel.Builder<_B>> addSeparator() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final Separator.Builder<Panel.Builder<_B>> separator_Builder = new Separator.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(separator_Builder);
            return separator_Builder;
        }

        /**
         * Adds the given items to the value of "titledSeparator_"
         * 
         * @param titledSeparator_
         *     Items to add to the value of the "titledSeparator_" property
         */
        public Panel.Builder<_B> addTitledSeparator(final Iterable<? extends TitledSeparator> titledSeparator_) {
            if (titledSeparator_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (TitledSeparator _item: titledSeparator_) {
                    this.containerOrLabelOrTextfield.add(new TitledSeparator.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "titledSeparator_"
         * 
         * @param titledSeparator_
         *     Items to add to the value of the "titledSeparator_" property
         */
        public Panel.Builder<_B> addTitledSeparator(TitledSeparator... titledSeparator_) {
            return addTitledSeparator(Arrays.asList(titledSeparator_));
        }

        /**
         * Returns a new builder to build an additional value of the "titledSeparator" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TitledSeparator.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "titledSeparator" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TitledSeparator.Builder#end()} to return to the current builder.
         */
        public TitledSeparator.Builder<? extends Panel.Builder<_B>> addTitledSeparator() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final TitledSeparator.Builder<Panel.Builder<_B>> titledSeparator_Builder = new TitledSeparator.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(titledSeparator_Builder);
            return titledSeparator_Builder;
        }

        /**
         * Adds the given items to the value of "layoutcomponent_"
         * 
         * @param layoutcomponent_
         *     Items to add to the value of the "layoutcomponent_" property
         */
        public Panel.Builder<_B> addLayoutcomponent(final Iterable<? extends Layoutcomponent> layoutcomponent_) {
            if (layoutcomponent_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Layoutcomponent _item: layoutcomponent_) {
                    this.containerOrLabelOrTextfield.add(new Layoutcomponent.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "layoutcomponent_"
         * 
         * @param layoutcomponent_
         *     Items to add to the value of the "layoutcomponent_" property
         */
        public Panel.Builder<_B> addLayoutcomponent(Layoutcomponent... layoutcomponent_) {
            return addLayoutcomponent(Arrays.asList(layoutcomponent_));
        }

        /**
         * Returns a new builder to build an additional value of the "layoutcomponent" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Layoutcomponent.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "layoutcomponent" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Layoutcomponent.Builder#end()} to return to the current builder.
         */
        public Layoutcomponent.Builder<? extends Panel.Builder<_B>> addLayoutcomponent() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final Layoutcomponent.Builder<Panel.Builder<_B>> layoutcomponent_Builder = new Layoutcomponent.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(layoutcomponent_Builder);
            return layoutcomponent_Builder;
        }

        /**
         * Adds the given items to the value of "webaddon_"
         * 
         * @param webaddon_
         *     Items to add to the value of the "webaddon_" property
         */
        public Panel.Builder<_B> addWebaddon(final Iterable<? extends Webaddon> webaddon_) {
            if (webaddon_!= null) {
                if (this.containerOrLabelOrTextfield == null) {
                    this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
                }
                for (Webaddon _item: webaddon_) {
                    this.containerOrLabelOrTextfield.add(new Webaddon.Builder<Panel.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Adds the given items to the value of "webaddon_"
         * 
         * @param webaddon_
         *     Items to add to the value of the "webaddon_" property
         */
        public Panel.Builder<_B> addWebaddon(Webaddon... webaddon_) {
            return addWebaddon(Arrays.asList(webaddon_));
        }

        /**
         * Returns a new builder to build an additional value of the "webaddon" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Webaddon.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "webaddon" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Webaddon.Builder#end()} to return to the current builder.
         */
        public Webaddon.Builder<? extends Panel.Builder<_B>> addWebaddon() {
            if (this.containerOrLabelOrTextfield == null) {
                this.containerOrLabelOrTextfield = new ArrayList<Buildable>();
            }
            final Webaddon.Builder<Panel.Builder<_B>> webaddon_Builder = new Webaddon.Builder<Panel.Builder<_B>>(this, null, false);
            this.containerOrLabelOrTextfield.add(webaddon_Builder);
            return webaddon_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Panel.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "visible" (any previous value will be replaced)
         * 
         * @param visible
         *     New value of the "visible" property.
         */
        public Panel.Builder<_B> withVisible(final Boolean visible) {
            this.visible = visible;
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        public Panel.Builder<_B> withOpaque(final Boolean opaque) {
            this.opaque = opaque;
            return this;
        }

        @Override
        public Panel build() {
            if (_storedValue == null) {
                return this.init(new Panel());
            } else {
                return ((Panel) _storedValue);
            }
        }

        public Panel.Builder<_B> copyOf(final Panel _other) {
            _other.copyTo(this);
            return this;
        }

        public Panel.Builder<_B> copyOf(final Panel.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Panel.Selector<Panel.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Panel.Select _root() {
            return new Panel.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> layoutconstraints = null;
        private com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> layoutmanager = null;
        private ClearBorder.Selector<TRoot, Panel.Selector<TRoot, TParent>> clearBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, Panel.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, Panel.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, Panel.Selector<TRoot, TParent>> strictSize = null;
        private Font.Selector<TRoot, Panel.Selector<TRoot, TParent>> font = null;
        private Background.Selector<TRoot, Panel.Selector<TRoot, TParent>> background = null;
        private com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> description = null;
        private com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> containerOrLabelOrTextfield = null;
        private com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> visible = null;
        private com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> opaque = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.layoutmanager!= null) {
                products.put("layoutmanager", this.layoutmanager.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.font!= null) {
                products.put("font", this.font.init());
            }
            if (this.background!= null) {
                products.put("background", this.background.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.containerOrLabelOrTextfield!= null) {
                products.put("containerOrLabelOrTextfield", this.containerOrLabelOrTextfield.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.visible!= null) {
                products.put("visible", this.visible.init());
            }
            if (this.opaque!= null) {
                products.put("opaque", this.opaque.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> layoutmanager() {
            return ((this.layoutmanager == null)?this.layoutmanager = new com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "layoutmanager"):this.layoutmanager);
        }

        public ClearBorder.Selector<TRoot, Panel.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, Panel.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, Panel.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, Panel.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public Font.Selector<TRoot, Panel.Selector<TRoot, TParent>> font() {
            return ((this.font == null)?this.font = new Font.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "font"):this.font);
        }

        public Background.Selector<TRoot, Panel.Selector<TRoot, TParent>> background() {
            return ((this.background == null)?this.background = new Background.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "background"):this.background);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> containerOrLabelOrTextfield() {
            return ((this.containerOrLabelOrTextfield == null)?this.containerOrLabelOrTextfield = new com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "containerOrLabelOrTextfield"):this.containerOrLabelOrTextfield);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> visible() {
            return ((this.visible == null)?this.visible = new com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "visible"):this.visible);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>> opaque() {
            return ((this.opaque == null)?this.opaque = new com.kscs.util.jaxb.Selector<TRoot, Panel.Selector<TRoot, TParent>>(this._root, this, "opaque"):this.opaque);
        }

    }

}
