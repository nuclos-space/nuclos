
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}property" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}description" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="entity" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="toolbarorientation"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="horizontal"/&gt;
 *             &lt;enumeration value="vertical"/&gt;
 *             &lt;enumeration value="hide"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="scrollpane"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="horizontal"/&gt;
 *             &lt;enumeration value="vertical"/&gt;
 *             &lt;enumeration value="both"/&gt;
 *             &lt;enumeration value="none"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="opaque" type="{}boolean" /&gt;
 *       &lt;attribute name="foreignkeyfield-to-parent" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "clearBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "property",
    "description"
})
public class Chart implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElementRef(name = "border", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    protected List<Property> property;
    protected String description;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "entity", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String entity;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "toolbarorientation")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String toolbarorientation;
    @XmlAttribute(name = "scrollpane")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String scrollpane;
    @XmlAttribute(name = "opaque")
    protected Boolean opaque;
    @XmlAttribute(name = "foreignkeyfield-to-parent")
    @XmlSchemaType(name = "anySimpleType")
    protected String foreignkeyfieldToParent;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<JAXBElement<?>> getBorder() {
        if (border == null) {
            border = new ArrayList<JAXBElement<?>>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the property property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the property property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Property }
     * 
     * 
     */
    public List<Property> getProperty() {
        if (property == null) {
            property = new ArrayList<Property>();
        }
        return this.property;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the toolbarorientation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToolbarorientation() {
        return toolbarorientation;
    }

    /**
     * Sets the value of the toolbarorientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToolbarorientation(String value) {
        this.toolbarorientation = value;
    }

    /**
     * Gets the value of the scrollpane property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrollpane() {
        return scrollpane;
    }

    /**
     * Sets the value of the scrollpane property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrollpane(String value) {
        this.scrollpane = value;
    }

    /**
     * Gets the value of the opaque property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpaque() {
        return opaque;
    }

    /**
     * Sets the value of the opaque property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpaque(Boolean value) {
        this.opaque = value;
    }

    /**
     * Gets the value of the foreignkeyfieldToParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignkeyfieldToParent() {
        return foreignkeyfieldToParent;
    }

    /**
     * Sets the value of the foreignkeyfieldToParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignkeyfieldToParent(String value) {
        this.foreignkeyfieldToParent = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            strategy.appendField(locator, this, "property", buffer, theProperty);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            String theToolbarorientation;
            theToolbarorientation = this.getToolbarorientation();
            strategy.appendField(locator, this, "toolbarorientation", buffer, theToolbarorientation);
        }
        {
            String theScrollpane;
            theScrollpane = this.getScrollpane();
            strategy.appendField(locator, this, "scrollpane", buffer, theScrollpane);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            strategy.appendField(locator, this, "opaque", buffer, theOpaque);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            strategy.appendField(locator, this, "foreignkeyfieldToParent", buffer, theForeignkeyfieldToParent);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Chart)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Chart that = ((Chart) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<JAXBElement<?>> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            List<Property> lhsProperty;
            lhsProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            List<Property> rhsProperty;
            rhsProperty = (((that.property!= null)&&(!that.property.isEmpty()))?that.getProperty():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "property", lhsProperty), LocatorUtils.property(thatLocator, "property", rhsProperty), lhsProperty, rhsProperty)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            String lhsToolbarorientation;
            lhsToolbarorientation = this.getToolbarorientation();
            String rhsToolbarorientation;
            rhsToolbarorientation = that.getToolbarorientation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "toolbarorientation", lhsToolbarorientation), LocatorUtils.property(thatLocator, "toolbarorientation", rhsToolbarorientation), lhsToolbarorientation, rhsToolbarorientation)) {
                return false;
            }
        }
        {
            String lhsScrollpane;
            lhsScrollpane = this.getScrollpane();
            String rhsScrollpane;
            rhsScrollpane = that.getScrollpane();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "scrollpane", lhsScrollpane), LocatorUtils.property(thatLocator, "scrollpane", rhsScrollpane), lhsScrollpane, rhsScrollpane)) {
                return false;
            }
        }
        {
            Boolean lhsOpaque;
            lhsOpaque = this.getOpaque();
            Boolean rhsOpaque;
            rhsOpaque = that.getOpaque();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opaque", lhsOpaque), LocatorUtils.property(thatLocator, "opaque", rhsOpaque), lhsOpaque, rhsOpaque)) {
                return false;
            }
        }
        {
            String lhsForeignkeyfieldToParent;
            lhsForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            String rhsForeignkeyfieldToParent;
            rhsForeignkeyfieldToParent = that.getForeignkeyfieldToParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "foreignkeyfieldToParent", lhsForeignkeyfieldToParent), LocatorUtils.property(thatLocator, "foreignkeyfieldToParent", rhsForeignkeyfieldToParent), lhsForeignkeyfieldToParent, rhsForeignkeyfieldToParent)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            List<Property> theProperty;
            theProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "property", theProperty), currentHashCode, theProperty);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            String theToolbarorientation;
            theToolbarorientation = this.getToolbarorientation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "toolbarorientation", theToolbarorientation), currentHashCode, theToolbarorientation);
        }
        {
            String theScrollpane;
            theScrollpane = this.getScrollpane();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "scrollpane", theScrollpane), currentHashCode, theScrollpane);
        }
        {
            Boolean theOpaque;
            theOpaque = this.getOpaque();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opaque", theOpaque), currentHashCode, theOpaque);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foreignkeyfieldToParent", theForeignkeyfieldToParent), currentHashCode, theForeignkeyfieldToParent);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Chart) {
            final Chart copy = ((Chart) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<JAXBElement<?>> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyBorder = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<JAXBElement<?>> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if ((this.property!= null)&&(!this.property.isEmpty())) {
                List<Property> sourceProperty;
                sourceProperty = (((this.property!= null)&&(!this.property.isEmpty()))?this.getProperty():null);
                @SuppressWarnings("unchecked")
                List<Property> copyProperty = ((List<Property> ) strategy.copy(LocatorUtils.property(locator, "property", sourceProperty), sourceProperty));
                copy.property = null;
                if (copyProperty!= null) {
                    List<Property> uniquePropertyl = copy.getProperty();
                    uniquePropertyl.addAll(copyProperty);
                }
            } else {
                copy.property = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.toolbarorientation!= null) {
                String sourceToolbarorientation;
                sourceToolbarorientation = this.getToolbarorientation();
                String copyToolbarorientation = ((String) strategy.copy(LocatorUtils.property(locator, "toolbarorientation", sourceToolbarorientation), sourceToolbarorientation));
                copy.setToolbarorientation(copyToolbarorientation);
            } else {
                copy.toolbarorientation = null;
            }
            if (this.scrollpane!= null) {
                String sourceScrollpane;
                sourceScrollpane = this.getScrollpane();
                String copyScrollpane = ((String) strategy.copy(LocatorUtils.property(locator, "scrollpane", sourceScrollpane), sourceScrollpane));
                copy.setScrollpane(copyScrollpane);
            } else {
                copy.scrollpane = null;
            }
            if (this.opaque!= null) {
                Boolean sourceOpaque;
                sourceOpaque = this.getOpaque();
                Boolean copyOpaque = ((Boolean) strategy.copy(LocatorUtils.property(locator, "opaque", sourceOpaque), sourceOpaque));
                copy.setOpaque(copyOpaque);
            } else {
                copy.opaque = null;
            }
            if (this.foreignkeyfieldToParent!= null) {
                String sourceForeignkeyfieldToParent;
                sourceForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
                String copyForeignkeyfieldToParent = ((String) strategy.copy(LocatorUtils.property(locator, "foreignkeyfieldToParent", sourceForeignkeyfieldToParent), sourceForeignkeyfieldToParent));
                copy.setForeignkeyfieldToParent(copyForeignkeyfieldToParent);
            } else {
                copy.foreignkeyfieldToParent = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Chart();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Chart.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        if (this.property == null) {
            _other.property = null;
        } else {
            _other.property = new ArrayList<Property.Builder<Chart.Builder<_B>>>();
            for (Property _item: this.property) {
                _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.description = this.description;
        _other.name = this.name;
        _other.entity = this.entity;
        _other.enabled = this.enabled;
        _other.toolbarorientation = this.toolbarorientation;
        _other.scrollpane = this.scrollpane;
        _other.opaque = this.opaque;
        _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
    }

    public<_B >Chart.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Chart.Builder<_B>(_parentBuilder, this, true);
    }

    public Chart.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Chart.Builder<Void> builder() {
        return new Chart.Builder<Void>(null, null, false);
    }

    public static<_B >Chart.Builder<_B> copyOf(final Chart _other) {
        final Chart.Builder<_B> _newBuilder = new Chart.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Chart.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
            if (this.property == null) {
                _other.property = null;
            } else {
                _other.property = new ArrayList<Property.Builder<Chart.Builder<_B>>>();
                for (Property _item: this.property) {
                    _other.property.add(((_item == null)?null:_item.newCopyBuilder(_other, propertyPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree toolbarorientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toolbarorientation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toolbarorientationPropertyTree!= null):((toolbarorientationPropertyTree == null)||(!toolbarorientationPropertyTree.isLeaf())))) {
            _other.toolbarorientation = this.toolbarorientation;
        }
        final PropertyTree scrollpanePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("scrollpane"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(scrollpanePropertyTree!= null):((scrollpanePropertyTree == null)||(!scrollpanePropertyTree.isLeaf())))) {
            _other.scrollpane = this.scrollpane;
        }
        final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
            _other.opaque = this.opaque;
        }
        final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
            _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        }
    }

    public<_B >Chart.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Chart.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Chart.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Chart.Builder<_B> copyOf(final Chart _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Chart.Builder<_B> _newBuilder = new Chart.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Chart.Builder<Void> copyExcept(final Chart _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Chart.Builder<Void> copyOnly(final Chart _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Chart _storedValue;
        private JAXBElement<?> layoutconstraints;
        private ClearBorder.Builder<Chart.Builder<_B>> clearBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<Chart.Builder<_B>> minimumSize;
        private PreferredSize.Builder<Chart.Builder<_B>> preferredSize;
        private StrictSize.Builder<Chart.Builder<_B>> strictSize;
        private List<Property.Builder<Chart.Builder<_B>>> property;
        private String description;
        private String name;
        private String entity;
        private Boolean enabled;
        private String toolbarorientation;
        private String scrollpane;
        private Boolean opaque;
        private String foreignkeyfieldToParent;

        public Builder(final _B _parentBuilder, final Chart _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    if (_other.property == null) {
                        this.property = null;
                    } else {
                        this.property = new ArrayList<Property.Builder<Chart.Builder<_B>>>();
                        for (Property _item: _other.property) {
                            this.property.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.description = _other.description;
                    this.name = _other.name;
                    this.entity = _other.entity;
                    this.enabled = _other.enabled;
                    this.toolbarorientation = _other.toolbarorientation;
                    this.scrollpane = _other.scrollpane;
                    this.opaque = _other.opaque;
                    this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Chart _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree propertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("property"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(propertyPropertyTree!= null):((propertyPropertyTree == null)||(!propertyPropertyTree.isLeaf())))) {
                        if (_other.property == null) {
                            this.property = null;
                        } else {
                            this.property = new ArrayList<Property.Builder<Chart.Builder<_B>>>();
                            for (Property _item: _other.property) {
                                this.property.add(((_item == null)?null:_item.newCopyBuilder(this, propertyPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree toolbarorientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toolbarorientation"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toolbarorientationPropertyTree!= null):((toolbarorientationPropertyTree == null)||(!toolbarorientationPropertyTree.isLeaf())))) {
                        this.toolbarorientation = _other.toolbarorientation;
                    }
                    final PropertyTree scrollpanePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("scrollpane"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(scrollpanePropertyTree!= null):((scrollpanePropertyTree == null)||(!scrollpanePropertyTree.isLeaf())))) {
                        this.scrollpane = _other.scrollpane;
                    }
                    final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
                        this.opaque = _other.opaque;
                    }
                    final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
                        this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Chart >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            if (this.border!= null) {
                final List<JAXBElement<?>> border = new ArrayList<JAXBElement<?>>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((JAXBElement<?> ) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            if (this.property!= null) {
                final List<Property> property = new ArrayList<Property>(this.property.size());
                for (Property.Builder<Chart.Builder<_B>> _item: this.property) {
                    property.add(_item.build());
                }
                _product.property = property;
            }
            _product.description = this.description;
            _product.name = this.name;
            _product.entity = this.entity;
            _product.enabled = this.enabled;
            _product.toolbarorientation = this.toolbarorientation;
            _product.scrollpane = this.scrollpane;
            _product.opaque = this.opaque;
            _product.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public Chart.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public Chart.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<Chart.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Chart.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<Chart.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Chart.Builder<_B> addBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Chart.Builder<_B> withBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Chart.Builder<_B> addBorder(JAXBElement<?> ... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Chart.Builder<_B> withBorder(JAXBElement<?> ... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public Chart.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<Chart.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Chart.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<Chart.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public Chart.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<Chart.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Chart.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<Chart.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public Chart.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<Chart.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Chart.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<Chart.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public Chart.Builder<_B> addProperty(final Iterable<? extends Property> property) {
            if (property!= null) {
                if (this.property == null) {
                    this.property = new ArrayList<Property.Builder<Chart.Builder<_B>>>();
                }
                for (Property _item: property) {
                    this.property.add(new Property.Builder<Chart.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public Chart.Builder<_B> withProperty(final Iterable<? extends Property> property) {
            if (this.property!= null) {
                this.property.clear();
            }
            return addProperty(property);
        }

        /**
         * Adds the given items to the value of "property"
         * 
         * @param property
         *     Items to add to the value of the "property" property
         */
        public Chart.Builder<_B> addProperty(Property... property) {
            addProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Sets the new value of "property" (any previous value will be replaced)
         * 
         * @param property
         *     New value of the "property" property.
         */
        public Chart.Builder<_B> withProperty(Property... property) {
            withProperty(Arrays.asList(property));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Property" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Property" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Property.Builder#end()} to return to the current builder.
         */
        public Property.Builder<? extends Chart.Builder<_B>> addProperty() {
            if (this.property == null) {
                this.property = new ArrayList<Property.Builder<Chart.Builder<_B>>>();
            }
            final Property.Builder<Chart.Builder<_B>> property_Builder = new Property.Builder<Chart.Builder<_B>>(this, null, false);
            this.property.add(property_Builder);
            return property_Builder;
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public Chart.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Chart.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public Chart.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public Chart.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "toolbarorientation" (any previous value will be replaced)
         * 
         * @param toolbarorientation
         *     New value of the "toolbarorientation" property.
         */
        public Chart.Builder<_B> withToolbarorientation(final String toolbarorientation) {
            this.toolbarorientation = toolbarorientation;
            return this;
        }

        /**
         * Sets the new value of "scrollpane" (any previous value will be replaced)
         * 
         * @param scrollpane
         *     New value of the "scrollpane" property.
         */
        public Chart.Builder<_B> withScrollpane(final String scrollpane) {
            this.scrollpane = scrollpane;
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        public Chart.Builder<_B> withOpaque(final Boolean opaque) {
            this.opaque = opaque;
            return this;
        }

        /**
         * Sets the new value of "foreignkeyfieldToParent" (any previous value will be replaced)
         * 
         * @param foreignkeyfieldToParent
         *     New value of the "foreignkeyfieldToParent" property.
         */
        public Chart.Builder<_B> withForeignkeyfieldToParent(final String foreignkeyfieldToParent) {
            this.foreignkeyfieldToParent = foreignkeyfieldToParent;
            return this;
        }

        @Override
        public Chart build() {
            if (_storedValue == null) {
                return this.init(new Chart());
            } else {
                return ((Chart) _storedValue);
            }
        }

        public Chart.Builder<_B> copyOf(final Chart _other) {
            _other.copyTo(this);
            return this;
        }

        public Chart.Builder<_B> copyOf(final Chart.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Chart.Selector<Chart.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Chart.Select _root() {
            return new Chart.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> layoutconstraints = null;
        private ClearBorder.Selector<TRoot, Chart.Selector<TRoot, TParent>> clearBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, Chart.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, Chart.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, Chart.Selector<TRoot, TParent>> strictSize = null;
        private Property.Selector<TRoot, Chart.Selector<TRoot, TParent>> property = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> description = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> toolbarorientation = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> scrollpane = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> opaque = null;
        private com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> foreignkeyfieldToParent = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.property!= null) {
                products.put("property", this.property.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.toolbarorientation!= null) {
                products.put("toolbarorientation", this.toolbarorientation.init());
            }
            if (this.scrollpane!= null) {
                products.put("scrollpane", this.scrollpane.init());
            }
            if (this.opaque!= null) {
                products.put("opaque", this.opaque.init());
            }
            if (this.foreignkeyfieldToParent!= null) {
                products.put("foreignkeyfieldToParent", this.foreignkeyfieldToParent.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public ClearBorder.Selector<TRoot, Chart.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, Chart.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, Chart.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, Chart.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public Property.Selector<TRoot, Chart.Selector<TRoot, TParent>> property() {
            return ((this.property == null)?this.property = new Property.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "property"):this.property);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> toolbarorientation() {
            return ((this.toolbarorientation == null)?this.toolbarorientation = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "toolbarorientation"):this.toolbarorientation);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> scrollpane() {
            return ((this.scrollpane == null)?this.scrollpane = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "scrollpane"):this.scrollpane);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> opaque() {
            return ((this.opaque == null)?this.opaque = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "opaque"):this.opaque);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>> foreignkeyfieldToParent() {
            return ((this.foreignkeyfieldToParent == null)?this.foreignkeyfieldToParent = new com.kscs.util.jaxb.Selector<TRoot, Chart.Selector<TRoot, TParent>>(this._root, this, "foreignkeyfieldToParent"):this.foreignkeyfieldToParent);
        }

    }

}
