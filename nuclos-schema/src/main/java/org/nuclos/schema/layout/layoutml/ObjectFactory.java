
package org.nuclos.schema.layout.layoutml;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.nuclos.schema.layout.layoutml package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PropertyType_QNAME = new QName("", "property-type");
    private final static QName _Border_QNAME = new QName("", "border");
    private final static QName _Layoutmanager_QNAME = new QName("", "layoutmanager");
    private final static QName _Layoutconstraints_QNAME = new QName("", "layoutconstraints");
    private final static QName _Container_QNAME = new QName("", "container");
    private final static QName _Panel_QNAME = new QName("", "panel");
    private final static QName _Splitpane_QNAME = new QName("", "splitpane");
    private final static QName _EmptyPanel_QNAME = new QName("", "empty-panel");
    private final static QName _Tabbedpane_QNAME = new QName("", "tabbedpane");
    private final static QName _Matrix_QNAME = new QName("", "matrix");
    private final static QName _Property_QNAME = new QName("", "property");
    private final static QName _Subform_QNAME = new QName("", "subform");
    private final static QName _PropertyScript_QNAME = new QName("", "property-script");
    private final static QName _Chart_QNAME = new QName("", "chart");
    private final static QName _Scrollpane_QNAME = new QName("", "scrollpane");
    private final static QName _Description_QNAME = new QName("", "description");
    private final static QName _PropertyValuelistProvider_QNAME = new QName("", "property-valuelist-provider");
    private final static QName _Borderlayout_QNAME = new QName("", "borderlayout");
    private final static QName _Flowlayout_QNAME = new QName("", "flowlayout");
    private final static QName _Gridlayout_QNAME = new QName("", "gridlayout");
    private final static QName _Gridbaglayout_QNAME = new QName("", "gridbaglayout");
    private final static QName _Boxlayout_QNAME = new QName("", "boxlayout");
    private final static QName _Rowlayout_QNAME = new QName("", "rowlayout");
    private final static QName _Columnlayout_QNAME = new QName("", "columnlayout");
    private final static QName _BorderlayoutConstraints_QNAME = new QName("", "borderlayout-constraints");
    private final static QName _GridbagConstraints_QNAME = new QName("", "gridbag-constraints");
    private final static QName _TablelayoutConstraints_QNAME = new QName("", "tablelayout-constraints");
    private final static QName _Tablelayout_QNAME = new QName("", "tablelayout");
    private final static QName _SplitpaneConstraints_QNAME = new QName("", "splitpane-constraints");
    private final static QName _TabbedpaneConstraints_QNAME = new QName("", "tabbedpane-constraints");
    private final static QName _EmptyBorder_QNAME = new QName("", "empty-border");
    private final static QName _EtchedBorder_QNAME = new QName("", "etched-border");
    private final static QName _BevelBorder_QNAME = new QName("", "bevel-border");
    private final static QName _LineBorder_QNAME = new QName("", "line-border");
    private final static QName _TitledBorder_QNAME = new QName("", "titled-border");
    private final static QName _PropertySize_QNAME = new QName("", "property-size");
    private final static QName _PropertyColor_QNAME = new QName("", "property-color");
    private final static QName _PropertyFont_QNAME = new QName("", "property-font");
    private final static QName _PropertyTranslations_QNAME = new QName("", "property-translations");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.nuclos.schema.layout.layoutml
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Layoutml }
     * 
     */
    public Layoutml createLayoutml() {
        return new Layoutml();
    }

    /**
     * Create an instance of {@link Definitions }
     * 
     */
    public Definitions createDefinitions() {
        return new Definitions();
    }

    /**
     * Create an instance of {@link CollectableField }
     * 
     */
    public CollectableField createCollectableField() {
        return new CollectableField();
    }

    /**
     * Create an instance of {@link Layout }
     * 
     */
    public Layout createLayout() {
        return new Layout();
    }

    /**
     * Create an instance of {@link InitialFocusComponent }
     * 
     */
    public InitialFocusComponent createInitialFocusComponent() {
        return new InitialFocusComponent();
    }

    /**
     * Create an instance of {@link Panel }
     * 
     */
    public Panel createPanel() {
        return new Panel();
    }

    /**
     * Create an instance of {@link Splitpane }
     * 
     */
    public Splitpane createSplitpane() {
        return new Splitpane();
    }

    /**
     * Create an instance of {@link Dependencies }
     * 
     */
    public Dependencies createDependencies() {
        return new Dependencies();
    }

    /**
     * Create an instance of {@link Dependency }
     * 
     */
    public Dependency createDependency() {
        return new Dependency();
    }

    /**
     * Create an instance of {@link Rules }
     * 
     */
    public Rules createRules() {
        return new Rules();
    }

    /**
     * Create an instance of {@link Rule }
     * 
     */
    public Rule createRule() {
        return new Rule();
    }

    /**
     * Create an instance of {@link Event }
     * 
     */
    public Event createEvent() {
        return new Event();
    }

    /**
     * Create an instance of {@link Condition }
     * 
     */
    public Condition createCondition() {
        return new Condition();
    }

    /**
     * Create an instance of {@link Actions }
     * 
     */
    public Actions createActions() {
        return new Actions();
    }

    /**
     * Create an instance of {@link TransferLookedupValue }
     * 
     */
    public TransferLookedupValue createTransferLookedupValue() {
        return new TransferLookedupValue();
    }

    /**
     * Create an instance of {@link Clear }
     * 
     */
    public Clear createClear() {
        return new Clear();
    }

    /**
     * Create an instance of {@link Enable }
     * 
     */
    public Enable createEnable() {
        return new Enable();
    }

    /**
     * Create an instance of {@link RefreshValuelist }
     * 
     */
    public RefreshValuelist createRefreshValuelist() {
        return new RefreshValuelist();
    }

    /**
     * Create an instance of {@link ReinitSubform }
     * 
     */
    public ReinitSubform createReinitSubform() {
        return new ReinitSubform();
    }

    /**
     * Create an instance of {@link EmptyPanel }
     * 
     */
    public EmptyPanel createEmptyPanel() {
        return new EmptyPanel();
    }

    /**
     * Create an instance of {@link Tabbedpane }
     * 
     */
    public Tabbedpane createTabbedpane() {
        return new Tabbedpane();
    }

    /**
     * Create an instance of {@link Matrix }
     * 
     */
    public Matrix createMatrix() {
        return new Matrix();
    }

    /**
     * Create an instance of {@link MatrixColumn }
     * 
     */
    public MatrixColumn createMatrixColumn() {
        return new MatrixColumn();
    }

    /**
     * Create an instance of {@link Translations }
     * 
     */
    public Translations createTranslations() {
        return new Translations();
    }

    /**
     * Create an instance of {@link Translation }
     * 
     */
    public Translation createTranslation() {
        return new Translation();
    }

    /**
     * Create an instance of {@link ValuelistProvider }
     * 
     */
    public ValuelistProvider createValuelistProvider() {
        return new ValuelistProvider();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link TextModule }
     * 
     */
    public TextModule createTextModule() {
        return new TextModule();
    }

    /**
     * Create an instance of {@link Property }
     * 
     */
    public Property createProperty() {
        return new Property();
    }

    /**
     * Create an instance of {@link Subform }
     * 
     */
    public Subform createSubform() {
        return new Subform();
    }

    /**
     * Create an instance of {@link SubformColumn }
     * 
     */
    public SubformColumn createSubformColumn() {
        return new SubformColumn();
    }

    /**
     * Create an instance of {@link InitialSortingOrder }
     * 
     */
    public InitialSortingOrder createInitialSortingOrder() {
        return new InitialSortingOrder();
    }

    /**
     * Create an instance of {@link NewEnabled }
     * 
     */
    public NewEnabled createNewEnabled() {
        return new NewEnabled();
    }

    /**
     * Create an instance of {@link EditEnabled }
     * 
     */
    public EditEnabled createEditEnabled() {
        return new EditEnabled();
    }

    /**
     * Create an instance of {@link DeleteEnabled }
     * 
     */
    public DeleteEnabled createDeleteEnabled() {
        return new DeleteEnabled();
    }

    /**
     * Create an instance of {@link CloneEnabled }
     * 
     */
    public CloneEnabled createCloneEnabled() {
        return new CloneEnabled();
    }

    /**
     * Create an instance of {@link DynamicRowColor }
     * 
     */
    public DynamicRowColor createDynamicRowColor() {
        return new DynamicRowColor();
    }

    /**
     * Create an instance of {@link PropertyScript }
     * 
     */
    public PropertyScript createPropertyScript() {
        return new PropertyScript();
    }

    /**
     * Create an instance of {@link Chart }
     * 
     */
    public Chart createChart() {
        return new Chart();
    }

    /**
     * Create an instance of {@link Scrollpane }
     * 
     */
    public Scrollpane createScrollpane() {
        return new Scrollpane();
    }

    /**
     * Create an instance of {@link TextColor }
     * 
     */
    public TextColor createTextColor() {
        return new TextColor();
    }

    /**
     * Create an instance of {@link TextFormat }
     * 
     */
    public TextFormat createTextFormat() {
        return new TextFormat();
    }

    /**
     * Create an instance of {@link Label }
     * 
     */
    public Label createLabel() {
        return new Label();
    }

    /**
     * Create an instance of {@link ClearBorder }
     * 
     */
    public ClearBorder createClearBorder() {
        return new ClearBorder();
    }

    /**
     * Create an instance of {@link MinimumSize }
     * 
     */
    public MinimumSize createMinimumSize() {
        return new MinimumSize();
    }

    /**
     * Create an instance of {@link PreferredSize }
     * 
     */
    public PreferredSize createPreferredSize() {
        return new PreferredSize();
    }

    /**
     * Create an instance of {@link StrictSize }
     * 
     */
    public StrictSize createStrictSize() {
        return new StrictSize();
    }

    /**
     * Create an instance of {@link Font }
     * 
     */
    public Font createFont() {
        return new Font();
    }

    /**
     * Create an instance of {@link Textfield }
     * 
     */
    public Textfield createTextfield() {
        return new Textfield();
    }

    /**
     * Create an instance of {@link Image }
     * 
     */
    public Image createImage() {
        return new Image();
    }

    /**
     * Create an instance of {@link Textarea }
     * 
     */
    public Textarea createTextarea() {
        return new Textarea();
    }

    /**
     * Create an instance of {@link Combobox }
     * 
     */
    public Combobox createCombobox() {
        return new Combobox();
    }

    /**
     * Create an instance of {@link Button }
     * 
     */
    public Button createButton() {
        return new Button();
    }

    /**
     * Create an instance of {@link CollectableComponent }
     * 
     */
    public CollectableComponent createCollectableComponent() {
        return new CollectableComponent();
    }

    /**
     * Create an instance of {@link Background }
     * 
     */
    public Background createBackground() {
        return new Background();
    }

    /**
     * Create an instance of {@link Options }
     * 
     */
    public Options createOptions() {
        return new Options();
    }

    /**
     * Create an instance of {@link Option }
     * 
     */
    public Option createOption() {
        return new Option();
    }

    /**
     * Create an instance of {@link Enabled }
     * 
     */
    public Enabled createEnabled() {
        return new Enabled();
    }

    /**
     * Create an instance of {@link Layoutcomponent }
     * 
     */
    public Layoutcomponent createLayoutcomponent() {
        return new Layoutcomponent();
    }

    /**
     * Create an instance of {@link Webaddon }
     * 
     */
    public Webaddon createWebaddon() {
        return new Webaddon();
    }

    /**
     * Create an instance of {@link PropertyValuelistProvider }
     * 
     */
    public PropertyValuelistProvider createPropertyValuelistProvider() {
        return new PropertyValuelistProvider();
    }

    /**
     * Create an instance of {@link Separator }
     * 
     */
    public Separator createSeparator() {
        return new Separator();
    }

    /**
     * Create an instance of {@link TitledSeparator }
     * 
     */
    public TitledSeparator createTitledSeparator() {
        return new TitledSeparator();
    }

    /**
     * Create an instance of {@link Borderlayout }
     * 
     */
    public Borderlayout createBorderlayout() {
        return new Borderlayout();
    }

    /**
     * Create an instance of {@link Flowlayout }
     * 
     */
    public Flowlayout createFlowlayout() {
        return new Flowlayout();
    }

    /**
     * Create an instance of {@link Gridlayout }
     * 
     */
    public Gridlayout createGridlayout() {
        return new Gridlayout();
    }

    /**
     * Create an instance of {@link Gridbaglayout }
     * 
     */
    public Gridbaglayout createGridbaglayout() {
        return new Gridbaglayout();
    }

    /**
     * Create an instance of {@link Boxlayout }
     * 
     */
    public Boxlayout createBoxlayout() {
        return new Boxlayout();
    }

    /**
     * Create an instance of {@link Rowlayout }
     * 
     */
    public Rowlayout createRowlayout() {
        return new Rowlayout();
    }

    /**
     * Create an instance of {@link Columnlayout }
     * 
     */
    public Columnlayout createColumnlayout() {
        return new Columnlayout();
    }

    /**
     * Create an instance of {@link BorderlayoutConstraints }
     * 
     */
    public BorderlayoutConstraints createBorderlayoutConstraints() {
        return new BorderlayoutConstraints();
    }

    /**
     * Create an instance of {@link GridbagConstraints }
     * 
     */
    public GridbagConstraints createGridbagConstraints() {
        return new GridbagConstraints();
    }

    /**
     * Create an instance of {@link TablelayoutConstraints }
     * 
     */
    public TablelayoutConstraints createTablelayoutConstraints() {
        return new TablelayoutConstraints();
    }

    /**
     * Create an instance of {@link Tablelayout }
     * 
     */
    public Tablelayout createTablelayout() {
        return new Tablelayout();
    }

    /**
     * Create an instance of {@link SplitpaneConstraints }
     * 
     */
    public SplitpaneConstraints createSplitpaneConstraints() {
        return new SplitpaneConstraints();
    }

    /**
     * Create an instance of {@link TabbedpaneConstraints }
     * 
     */
    public TabbedpaneConstraints createTabbedpaneConstraints() {
        return new TabbedpaneConstraints();
    }

    /**
     * Create an instance of {@link EmptyBorder }
     * 
     */
    public EmptyBorder createEmptyBorder() {
        return new EmptyBorder();
    }

    /**
     * Create an instance of {@link EtchedBorder }
     * 
     */
    public EtchedBorder createEtchedBorder() {
        return new EtchedBorder();
    }

    /**
     * Create an instance of {@link BevelBorder }
     * 
     */
    public BevelBorder createBevelBorder() {
        return new BevelBorder();
    }

    /**
     * Create an instance of {@link LineBorder }
     * 
     */
    public LineBorder createLineBorder() {
        return new LineBorder();
    }

    /**
     * Create an instance of {@link TitledBorder }
     * 
     */
    public TitledBorder createTitledBorder() {
        return new TitledBorder();
    }

    /**
     * Create an instance of {@link PropertySize }
     * 
     */
    public PropertySize createPropertySize() {
        return new PropertySize();
    }

    /**
     * Create an instance of {@link PropertyColor }
     * 
     */
    public PropertyColor createPropertyColor() {
        return new PropertyColor();
    }

    /**
     * Create an instance of {@link PropertyFont }
     * 
     */
    public PropertyFont createPropertyFont() {
        return new PropertyFont();
    }

    /**
     * Create an instance of {@link PropertyTranslations }
     * 
     */
    public PropertyTranslations createPropertyTranslations() {
        return new PropertyTranslations();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "property-type")
    public JAXBElement<Object> createPropertyType(Object value) {
        return new JAXBElement<Object>(_PropertyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "border")
    public JAXBElement<Object> createBorder(Object value) {
        return new JAXBElement<Object>(_Border_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "layoutmanager")
    public JAXBElement<Object> createLayoutmanager(Object value) {
        return new JAXBElement<Object>(_Layoutmanager_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "layoutconstraints")
    public JAXBElement<Object> createLayoutconstraints(Object value) {
        return new JAXBElement<Object>(_Layoutconstraints_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "container")
    public JAXBElement<Object> createContainer(Object value) {
        return new JAXBElement<Object>(_Container_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Panel }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Panel }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "panel", substitutionHeadNamespace = "", substitutionHeadName = "container")
    public JAXBElement<Panel> createPanel(Panel value) {
        return new JAXBElement<Panel>(_Panel_QNAME, Panel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Splitpane }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Splitpane }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "splitpane", substitutionHeadNamespace = "", substitutionHeadName = "container")
    public JAXBElement<Splitpane> createSplitpane(Splitpane value) {
        return new JAXBElement<Splitpane>(_Splitpane_QNAME, Splitpane.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyPanel }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EmptyPanel }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "empty-panel", substitutionHeadNamespace = "", substitutionHeadName = "container")
    public JAXBElement<EmptyPanel> createEmptyPanel(EmptyPanel value) {
        return new JAXBElement<EmptyPanel>(_EmptyPanel_QNAME, EmptyPanel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tabbedpane }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Tabbedpane }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "tabbedpane", substitutionHeadNamespace = "", substitutionHeadName = "container")
    public JAXBElement<Tabbedpane> createTabbedpane(Tabbedpane value) {
        return new JAXBElement<Tabbedpane>(_Tabbedpane_QNAME, Tabbedpane.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Matrix }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Matrix }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "matrix", substitutionHeadNamespace = "", substitutionHeadName = "container")
    public JAXBElement<Matrix> createMatrix(Matrix value) {
        return new JAXBElement<Matrix>(_Matrix_QNAME, Matrix.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Property }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Property }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "property", substitutionHeadNamespace = "", substitutionHeadName = "property-type")
    public JAXBElement<Property> createProperty(Property value) {
        return new JAXBElement<Property>(_Property_QNAME, Property.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Subform }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Subform }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "subform", substitutionHeadNamespace = "", substitutionHeadName = "container")
    public JAXBElement<Subform> createSubform(Subform value) {
        return new JAXBElement<Subform>(_Subform_QNAME, Subform.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertyScript }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PropertyScript }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "property-script", substitutionHeadNamespace = "", substitutionHeadName = "property-type")
    public JAXBElement<PropertyScript> createPropertyScript(PropertyScript value) {
        return new JAXBElement<PropertyScript>(_PropertyScript_QNAME, PropertyScript.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Chart }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Chart }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "chart", substitutionHeadNamespace = "", substitutionHeadName = "container")
    public JAXBElement<Chart> createChart(Chart value) {
        return new JAXBElement<Chart>(_Chart_QNAME, Chart.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Scrollpane }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Scrollpane }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "scrollpane", substitutionHeadNamespace = "", substitutionHeadName = "container")
    public JAXBElement<Scrollpane> createScrollpane(Scrollpane value) {
        return new JAXBElement<Scrollpane>(_Scrollpane_QNAME, Scrollpane.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "description")
    public JAXBElement<String> createDescription(String value) {
        return new JAXBElement<String>(_Description_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertyValuelistProvider }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PropertyValuelistProvider }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "property-valuelist-provider", substitutionHeadNamespace = "", substitutionHeadName = "property-type")
    public JAXBElement<PropertyValuelistProvider> createPropertyValuelistProvider(PropertyValuelistProvider value) {
        return new JAXBElement<PropertyValuelistProvider>(_PropertyValuelistProvider_QNAME, PropertyValuelistProvider.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Borderlayout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Borderlayout }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "borderlayout", substitutionHeadNamespace = "", substitutionHeadName = "layoutmanager")
    public JAXBElement<Borderlayout> createBorderlayout(Borderlayout value) {
        return new JAXBElement<Borderlayout>(_Borderlayout_QNAME, Borderlayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Flowlayout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Flowlayout }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "flowlayout", substitutionHeadNamespace = "", substitutionHeadName = "layoutmanager")
    public JAXBElement<Flowlayout> createFlowlayout(Flowlayout value) {
        return new JAXBElement<Flowlayout>(_Flowlayout_QNAME, Flowlayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gridlayout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Gridlayout }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "gridlayout", substitutionHeadNamespace = "", substitutionHeadName = "layoutmanager")
    public JAXBElement<Gridlayout> createGridlayout(Gridlayout value) {
        return new JAXBElement<Gridlayout>(_Gridlayout_QNAME, Gridlayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gridbaglayout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Gridbaglayout }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "gridbaglayout", substitutionHeadNamespace = "", substitutionHeadName = "layoutmanager")
    public JAXBElement<Gridbaglayout> createGridbaglayout(Gridbaglayout value) {
        return new JAXBElement<Gridbaglayout>(_Gridbaglayout_QNAME, Gridbaglayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boxlayout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Boxlayout }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "boxlayout", substitutionHeadNamespace = "", substitutionHeadName = "layoutmanager")
    public JAXBElement<Boxlayout> createBoxlayout(Boxlayout value) {
        return new JAXBElement<Boxlayout>(_Boxlayout_QNAME, Boxlayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Rowlayout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Rowlayout }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "rowlayout", substitutionHeadNamespace = "", substitutionHeadName = "layoutmanager")
    public JAXBElement<Rowlayout> createRowlayout(Rowlayout value) {
        return new JAXBElement<Rowlayout>(_Rowlayout_QNAME, Rowlayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Columnlayout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Columnlayout }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "columnlayout", substitutionHeadNamespace = "", substitutionHeadName = "layoutmanager")
    public JAXBElement<Columnlayout> createColumnlayout(Columnlayout value) {
        return new JAXBElement<Columnlayout>(_Columnlayout_QNAME, Columnlayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BorderlayoutConstraints }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BorderlayoutConstraints }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "borderlayout-constraints", substitutionHeadNamespace = "", substitutionHeadName = "layoutconstraints")
    public JAXBElement<BorderlayoutConstraints> createBorderlayoutConstraints(BorderlayoutConstraints value) {
        return new JAXBElement<BorderlayoutConstraints>(_BorderlayoutConstraints_QNAME, BorderlayoutConstraints.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GridbagConstraints }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GridbagConstraints }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "gridbag-constraints", substitutionHeadNamespace = "", substitutionHeadName = "layoutconstraints")
    public JAXBElement<GridbagConstraints> createGridbagConstraints(GridbagConstraints value) {
        return new JAXBElement<GridbagConstraints>(_GridbagConstraints_QNAME, GridbagConstraints.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TablelayoutConstraints }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link TablelayoutConstraints }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "tablelayout-constraints", substitutionHeadNamespace = "", substitutionHeadName = "layoutconstraints")
    public JAXBElement<TablelayoutConstraints> createTablelayoutConstraints(TablelayoutConstraints value) {
        return new JAXBElement<TablelayoutConstraints>(_TablelayoutConstraints_QNAME, TablelayoutConstraints.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tablelayout }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Tablelayout }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "tablelayout", substitutionHeadNamespace = "", substitutionHeadName = "layoutmanager")
    public JAXBElement<Tablelayout> createTablelayout(Tablelayout value) {
        return new JAXBElement<Tablelayout>(_Tablelayout_QNAME, Tablelayout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitpaneConstraints }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SplitpaneConstraints }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "splitpane-constraints", substitutionHeadNamespace = "", substitutionHeadName = "layoutconstraints")
    public JAXBElement<SplitpaneConstraints> createSplitpaneConstraints(SplitpaneConstraints value) {
        return new JAXBElement<SplitpaneConstraints>(_SplitpaneConstraints_QNAME, SplitpaneConstraints.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TabbedpaneConstraints }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link TabbedpaneConstraints }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "tabbedpane-constraints", substitutionHeadNamespace = "", substitutionHeadName = "layoutconstraints")
    public JAXBElement<TabbedpaneConstraints> createTabbedpaneConstraints(TabbedpaneConstraints value) {
        return new JAXBElement<TabbedpaneConstraints>(_TabbedpaneConstraints_QNAME, TabbedpaneConstraints.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyBorder }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EmptyBorder }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "empty-border", substitutionHeadNamespace = "", substitutionHeadName = "border")
    public JAXBElement<EmptyBorder> createEmptyBorder(EmptyBorder value) {
        return new JAXBElement<EmptyBorder>(_EmptyBorder_QNAME, EmptyBorder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EtchedBorder }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EtchedBorder }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "etched-border", substitutionHeadNamespace = "", substitutionHeadName = "border")
    public JAXBElement<EtchedBorder> createEtchedBorder(EtchedBorder value) {
        return new JAXBElement<EtchedBorder>(_EtchedBorder_QNAME, EtchedBorder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BevelBorder }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BevelBorder }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "bevel-border", substitutionHeadNamespace = "", substitutionHeadName = "border")
    public JAXBElement<BevelBorder> createBevelBorder(BevelBorder value) {
        return new JAXBElement<BevelBorder>(_BevelBorder_QNAME, BevelBorder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineBorder }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LineBorder }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "line-border", substitutionHeadNamespace = "", substitutionHeadName = "border")
    public JAXBElement<LineBorder> createLineBorder(LineBorder value) {
        return new JAXBElement<LineBorder>(_LineBorder_QNAME, LineBorder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TitledBorder }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link TitledBorder }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "titled-border", substitutionHeadNamespace = "", substitutionHeadName = "border")
    public JAXBElement<TitledBorder> createTitledBorder(TitledBorder value) {
        return new JAXBElement<TitledBorder>(_TitledBorder_QNAME, TitledBorder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertySize }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PropertySize }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "property-size", substitutionHeadNamespace = "", substitutionHeadName = "property-type")
    public JAXBElement<PropertySize> createPropertySize(PropertySize value) {
        return new JAXBElement<PropertySize>(_PropertySize_QNAME, PropertySize.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertyColor }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PropertyColor }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "property-color", substitutionHeadNamespace = "", substitutionHeadName = "property-type")
    public JAXBElement<PropertyColor> createPropertyColor(PropertyColor value) {
        return new JAXBElement<PropertyColor>(_PropertyColor_QNAME, PropertyColor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertyFont }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PropertyFont }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "property-font", substitutionHeadNamespace = "", substitutionHeadName = "property-type")
    public JAXBElement<PropertyFont> createPropertyFont(PropertyFont value) {
        return new JAXBElement<PropertyFont>(_PropertyFont_QNAME, PropertyFont.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropertyTranslations }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PropertyTranslations }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "property-translations", substitutionHeadNamespace = "", substitutionHeadName = "property-type")
    public JAXBElement<PropertyTranslations> createPropertyTranslations(PropertyTranslations value) {
        return new JAXBElement<PropertyTranslations>(_PropertyTranslations_QNAME, PropertyTranslations.class, null, value);
    }

}
