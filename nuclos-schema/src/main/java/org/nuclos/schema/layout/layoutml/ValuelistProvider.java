
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}parameter" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="type" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="default"/&gt;
 *             &lt;enumeration value="dependant"/&gt;
 *             &lt;enumeration value="named"/&gt;
 *             &lt;enumeration value="ds"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="value" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "parameter"
})
@XmlRootElement(name = "valuelist-provider")
public class ValuelistProvider implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<Parameter> parameter;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "type", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String type;
    @XmlAttribute(name = "value", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String value;

    /**
     * Gets the value of the parameter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Parameter }
     * 
     * 
     */
    public List<Parameter> getParameter() {
        if (parameter == null) {
            parameter = new ArrayList<Parameter>();
        }
        return this.parameter;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Parameter> theParameter;
            theParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            strategy.appendField(locator, this, "parameter", buffer, theParameter);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ValuelistProvider)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ValuelistProvider that = ((ValuelistProvider) object);
        {
            List<Parameter> lhsParameter;
            lhsParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            List<Parameter> rhsParameter;
            rhsParameter = (((that.parameter!= null)&&(!that.parameter.isEmpty()))?that.getParameter():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameter", lhsParameter), LocatorUtils.property(thatLocator, "parameter", rhsParameter), lhsParameter, rhsParameter)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Parameter> theParameter;
            theParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parameter", theParameter), currentHashCode, theParameter);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ValuelistProvider) {
            final ValuelistProvider copy = ((ValuelistProvider) draftCopy);
            if ((this.parameter!= null)&&(!this.parameter.isEmpty())) {
                List<Parameter> sourceParameter;
                sourceParameter = (((this.parameter!= null)&&(!this.parameter.isEmpty()))?this.getParameter():null);
                @SuppressWarnings("unchecked")
                List<Parameter> copyParameter = ((List<Parameter> ) strategy.copy(LocatorUtils.property(locator, "parameter", sourceParameter), sourceParameter));
                copy.parameter = null;
                if (copyParameter!= null) {
                    List<Parameter> uniqueParameterl = copy.getParameter();
                    uniqueParameterl.addAll(copyParameter);
                }
            } else {
                copy.parameter = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ValuelistProvider();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ValuelistProvider.Builder<_B> _other) {
        if (this.parameter == null) {
            _other.parameter = null;
        } else {
            _other.parameter = new ArrayList<Parameter.Builder<ValuelistProvider.Builder<_B>>>();
            for (Parameter _item: this.parameter) {
                _other.parameter.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other.type = this.type;
        _other.value = this.value;
    }

    public<_B >ValuelistProvider.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new ValuelistProvider.Builder<_B>(_parentBuilder, this, true);
    }

    public ValuelistProvider.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static ValuelistProvider.Builder<Void> builder() {
        return new ValuelistProvider.Builder<Void>(null, null, false);
    }

    public static<_B >ValuelistProvider.Builder<_B> copyOf(final ValuelistProvider _other) {
        final ValuelistProvider.Builder<_B> _newBuilder = new ValuelistProvider.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ValuelistProvider.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree parameterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameter"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterPropertyTree!= null):((parameterPropertyTree == null)||(!parameterPropertyTree.isLeaf())))) {
            if (this.parameter == null) {
                _other.parameter = null;
            } else {
                _other.parameter = new ArrayList<Parameter.Builder<ValuelistProvider.Builder<_B>>>();
                for (Parameter _item: this.parameter) {
                    _other.parameter.add(((_item == null)?null:_item.newCopyBuilder(_other, parameterPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
    }

    public<_B >ValuelistProvider.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new ValuelistProvider.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public ValuelistProvider.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >ValuelistProvider.Builder<_B> copyOf(final ValuelistProvider _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final ValuelistProvider.Builder<_B> _newBuilder = new ValuelistProvider.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static ValuelistProvider.Builder<Void> copyExcept(final ValuelistProvider _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static ValuelistProvider.Builder<Void> copyOnly(final ValuelistProvider _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final ValuelistProvider _storedValue;
        private List<Parameter.Builder<ValuelistProvider.Builder<_B>>> parameter;
        private String name;
        private String type;
        private String value;

        public Builder(final _B _parentBuilder, final ValuelistProvider _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.parameter == null) {
                        this.parameter = null;
                    } else {
                        this.parameter = new ArrayList<Parameter.Builder<ValuelistProvider.Builder<_B>>>();
                        for (Parameter _item: _other.parameter) {
                            this.parameter.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this.type = _other.type;
                    this.value = _other.value;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final ValuelistProvider _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree parameterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameter"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterPropertyTree!= null):((parameterPropertyTree == null)||(!parameterPropertyTree.isLeaf())))) {
                        if (_other.parameter == null) {
                            this.parameter = null;
                        } else {
                            this.parameter = new ArrayList<Parameter.Builder<ValuelistProvider.Builder<_B>>>();
                            for (Parameter _item: _other.parameter) {
                                this.parameter.add(((_item == null)?null:_item.newCopyBuilder(this, parameterPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends ValuelistProvider >_P init(final _P _product) {
            if (this.parameter!= null) {
                final List<Parameter> parameter = new ArrayList<Parameter>(this.parameter.size());
                for (Parameter.Builder<ValuelistProvider.Builder<_B>> _item: this.parameter) {
                    parameter.add(_item.build());
                }
                _product.parameter = parameter;
            }
            _product.name = this.name;
            _product.type = this.type;
            _product.value = this.value;
            return _product;
        }

        /**
         * Adds the given items to the value of "parameter"
         * 
         * @param parameter
         *     Items to add to the value of the "parameter" property
         */
        public ValuelistProvider.Builder<_B> addParameter(final Iterable<? extends Parameter> parameter) {
            if (parameter!= null) {
                if (this.parameter == null) {
                    this.parameter = new ArrayList<Parameter.Builder<ValuelistProvider.Builder<_B>>>();
                }
                for (Parameter _item: parameter) {
                    this.parameter.add(new Parameter.Builder<ValuelistProvider.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "parameter" (any previous value will be replaced)
         * 
         * @param parameter
         *     New value of the "parameter" property.
         */
        public ValuelistProvider.Builder<_B> withParameter(final Iterable<? extends Parameter> parameter) {
            if (this.parameter!= null) {
                this.parameter.clear();
            }
            return addParameter(parameter);
        }

        /**
         * Adds the given items to the value of "parameter"
         * 
         * @param parameter
         *     Items to add to the value of the "parameter" property
         */
        public ValuelistProvider.Builder<_B> addParameter(Parameter... parameter) {
            addParameter(Arrays.asList(parameter));
            return this;
        }

        /**
         * Sets the new value of "parameter" (any previous value will be replaced)
         * 
         * @param parameter
         *     New value of the "parameter" property.
         */
        public ValuelistProvider.Builder<_B> withParameter(Parameter... parameter) {
            withParameter(Arrays.asList(parameter));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Parameter" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Parameter.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Parameter" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Parameter.Builder#end()} to return to the current builder.
         */
        public Parameter.Builder<? extends ValuelistProvider.Builder<_B>> addParameter() {
            if (this.parameter == null) {
                this.parameter = new ArrayList<Parameter.Builder<ValuelistProvider.Builder<_B>>>();
            }
            final Parameter.Builder<ValuelistProvider.Builder<_B>> parameter_Builder = new Parameter.Builder<ValuelistProvider.Builder<_B>>(this, null, false);
            this.parameter.add(parameter_Builder);
            return parameter_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public ValuelistProvider.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public ValuelistProvider.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public ValuelistProvider.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        @Override
        public ValuelistProvider build() {
            if (_storedValue == null) {
                return this.init(new ValuelistProvider());
            } else {
                return ((ValuelistProvider) _storedValue);
            }
        }

        public ValuelistProvider.Builder<_B> copyOf(final ValuelistProvider _other) {
            _other.copyTo(this);
            return this;
        }

        public ValuelistProvider.Builder<_B> copyOf(final ValuelistProvider.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends ValuelistProvider.Selector<ValuelistProvider.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static ValuelistProvider.Select _root() {
            return new ValuelistProvider.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Parameter.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>> parameter = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>> value = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.parameter!= null) {
                products.put("parameter", this.parameter.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            return products;
        }

        public Parameter.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>> parameter() {
            return ((this.parameter == null)?this.parameter = new Parameter.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "parameter"):this.parameter);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, ValuelistProvider.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

    }

}
