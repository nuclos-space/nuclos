
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{}container"/&gt;
 *           &lt;element ref="{}textarea"/&gt;
 *           &lt;element ref="{}collectable-component"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="horizontalscrollbar"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="always"/&gt;
 *             &lt;enumeration value="never"/&gt;
 *             &lt;enumeration value="asneeded"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="verticalscrollbar"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="always"/&gt;
 *             &lt;enumeration value="never"/&gt;
 *             &lt;enumeration value="asneeded"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "clearBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "container",
    "textarea",
    "collectableComponent"
})
public class Scrollpane implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElementRef(name = "border", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    @XmlElementRef(name = "container", type = JAXBElement.class, required = false)
    protected JAXBElement<?> container;
    protected Textarea textarea;
    @XmlElement(name = "collectable-component")
    protected CollectableComponent collectableComponent;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "horizontalscrollbar")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String horizontalscrollbar;
    @XmlAttribute(name = "verticalscrollbar")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String verticalscrollbar;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     * 
     * 
     */
    public List<JAXBElement<?>> getBorder() {
        if (border == null) {
            border = new ArrayList<JAXBElement<?>>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the container property.
     * 
     * @return
     *     possible object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public JAXBElement<?> getContainer() {
        return container;
    }

    /**
     * Sets the value of the container property.
     * 
     * @param value
     *     allowed object is
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
* Row removed due to missing sorting of Java source code generation
     *     
     */
    public void setContainer(JAXBElement<?> value) {
        this.container = value;
    }

    /**
     * Gets the value of the textarea property.
     * 
     * @return
     *     possible object is
     *     {@link Textarea }
     *     
     */
    public Textarea getTextarea() {
        return textarea;
    }

    /**
     * Sets the value of the textarea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Textarea }
     *     
     */
    public void setTextarea(Textarea value) {
        this.textarea = value;
    }

    /**
     * Gets the value of the collectableComponent property.
     * 
     * @return
     *     possible object is
     *     {@link CollectableComponent }
     *     
     */
    public CollectableComponent getCollectableComponent() {
        return collectableComponent;
    }

    /**
     * Sets the value of the collectableComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectableComponent }
     *     
     */
    public void setCollectableComponent(CollectableComponent value) {
        this.collectableComponent = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the horizontalscrollbar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHorizontalscrollbar() {
        return horizontalscrollbar;
    }

    /**
     * Sets the value of the horizontalscrollbar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHorizontalscrollbar(String value) {
        this.horizontalscrollbar = value;
    }

    /**
     * Gets the value of the verticalscrollbar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerticalscrollbar() {
        return verticalscrollbar;
    }

    /**
     * Sets the value of the verticalscrollbar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerticalscrollbar(String value) {
        this.verticalscrollbar = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            JAXBElement<?> theContainer;
            theContainer = this.getContainer();
            strategy.appendField(locator, this, "container", buffer, theContainer);
        }
        {
            Textarea theTextarea;
            theTextarea = this.getTextarea();
            strategy.appendField(locator, this, "textarea", buffer, theTextarea);
        }
        {
            CollectableComponent theCollectableComponent;
            theCollectableComponent = this.getCollectableComponent();
            strategy.appendField(locator, this, "collectableComponent", buffer, theCollectableComponent);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theHorizontalscrollbar;
            theHorizontalscrollbar = this.getHorizontalscrollbar();
            strategy.appendField(locator, this, "horizontalscrollbar", buffer, theHorizontalscrollbar);
        }
        {
            String theVerticalscrollbar;
            theVerticalscrollbar = this.getVerticalscrollbar();
            strategy.appendField(locator, this, "verticalscrollbar", buffer, theVerticalscrollbar);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Scrollpane)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Scrollpane that = ((Scrollpane) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<JAXBElement<?>> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            JAXBElement<?> lhsContainer;
            lhsContainer = this.getContainer();
            JAXBElement<?> rhsContainer;
            rhsContainer = that.getContainer();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "container", lhsContainer), LocatorUtils.property(thatLocator, "container", rhsContainer), lhsContainer, rhsContainer)) {
                return false;
            }
        }
        {
            Textarea lhsTextarea;
            lhsTextarea = this.getTextarea();
            Textarea rhsTextarea;
            rhsTextarea = that.getTextarea();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textarea", lhsTextarea), LocatorUtils.property(thatLocator, "textarea", rhsTextarea), lhsTextarea, rhsTextarea)) {
                return false;
            }
        }
        {
            CollectableComponent lhsCollectableComponent;
            lhsCollectableComponent = this.getCollectableComponent();
            CollectableComponent rhsCollectableComponent;
            rhsCollectableComponent = that.getCollectableComponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "collectableComponent", lhsCollectableComponent), LocatorUtils.property(thatLocator, "collectableComponent", rhsCollectableComponent), lhsCollectableComponent, rhsCollectableComponent)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsHorizontalscrollbar;
            lhsHorizontalscrollbar = this.getHorizontalscrollbar();
            String rhsHorizontalscrollbar;
            rhsHorizontalscrollbar = that.getHorizontalscrollbar();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "horizontalscrollbar", lhsHorizontalscrollbar), LocatorUtils.property(thatLocator, "horizontalscrollbar", rhsHorizontalscrollbar), lhsHorizontalscrollbar, rhsHorizontalscrollbar)) {
                return false;
            }
        }
        {
            String lhsVerticalscrollbar;
            lhsVerticalscrollbar = this.getVerticalscrollbar();
            String rhsVerticalscrollbar;
            rhsVerticalscrollbar = that.getVerticalscrollbar();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "verticalscrollbar", lhsVerticalscrollbar), LocatorUtils.property(thatLocator, "verticalscrollbar", rhsVerticalscrollbar), lhsVerticalscrollbar, rhsVerticalscrollbar)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            List<JAXBElement<?>> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            JAXBElement<?> theContainer;
            theContainer = this.getContainer();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "container", theContainer), currentHashCode, theContainer);
        }
        {
            Textarea theTextarea;
            theTextarea = this.getTextarea();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textarea", theTextarea), currentHashCode, theTextarea);
        }
        {
            CollectableComponent theCollectableComponent;
            theCollectableComponent = this.getCollectableComponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "collectableComponent", theCollectableComponent), currentHashCode, theCollectableComponent);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theHorizontalscrollbar;
            theHorizontalscrollbar = this.getHorizontalscrollbar();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "horizontalscrollbar", theHorizontalscrollbar), currentHashCode, theHorizontalscrollbar);
        }
        {
            String theVerticalscrollbar;
            theVerticalscrollbar = this.getVerticalscrollbar();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verticalscrollbar", theVerticalscrollbar), currentHashCode, theVerticalscrollbar);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Scrollpane) {
            final Scrollpane copy = ((Scrollpane) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<JAXBElement<?>> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyBorder = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<JAXBElement<?>> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if (this.container!= null) {
                JAXBElement<?> sourceContainer;
                sourceContainer = this.getContainer();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyContainer = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "container", sourceContainer), sourceContainer));
                copy.setContainer(copyContainer);
            } else {
                copy.container = null;
            }
            if (this.textarea!= null) {
                Textarea sourceTextarea;
                sourceTextarea = this.getTextarea();
                Textarea copyTextarea = ((Textarea) strategy.copy(LocatorUtils.property(locator, "textarea", sourceTextarea), sourceTextarea));
                copy.setTextarea(copyTextarea);
            } else {
                copy.textarea = null;
            }
            if (this.collectableComponent!= null) {
                CollectableComponent sourceCollectableComponent;
                sourceCollectableComponent = this.getCollectableComponent();
                CollectableComponent copyCollectableComponent = ((CollectableComponent) strategy.copy(LocatorUtils.property(locator, "collectableComponent", sourceCollectableComponent), sourceCollectableComponent));
                copy.setCollectableComponent(copyCollectableComponent);
            } else {
                copy.collectableComponent = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.horizontalscrollbar!= null) {
                String sourceHorizontalscrollbar;
                sourceHorizontalscrollbar = this.getHorizontalscrollbar();
                String copyHorizontalscrollbar = ((String) strategy.copy(LocatorUtils.property(locator, "horizontalscrollbar", sourceHorizontalscrollbar), sourceHorizontalscrollbar));
                copy.setHorizontalscrollbar(copyHorizontalscrollbar);
            } else {
                copy.horizontalscrollbar = null;
            }
            if (this.verticalscrollbar!= null) {
                String sourceVerticalscrollbar;
                sourceVerticalscrollbar = this.getVerticalscrollbar();
                String copyVerticalscrollbar = ((String) strategy.copy(LocatorUtils.property(locator, "verticalscrollbar", sourceVerticalscrollbar), sourceVerticalscrollbar));
                copy.setVerticalscrollbar(copyVerticalscrollbar);
            } else {
                copy.verticalscrollbar = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Scrollpane();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Scrollpane.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        _other.container = this.container;
        _other.textarea = ((this.textarea == null)?null:this.textarea.newCopyBuilder(_other));
        _other.collectableComponent = ((this.collectableComponent == null)?null:this.collectableComponent.newCopyBuilder(_other));
        _other.name = this.name;
        _other.horizontalscrollbar = this.horizontalscrollbar;
        _other.verticalscrollbar = this.verticalscrollbar;
    }

    public<_B >Scrollpane.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Scrollpane.Builder<_B>(_parentBuilder, this, true);
    }

    public Scrollpane.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Scrollpane.Builder<Void> builder() {
        return new Scrollpane.Builder<Void>(null, null, false);
    }

    public static<_B >Scrollpane.Builder<_B> copyOf(final Scrollpane _other) {
        final Scrollpane.Builder<_B> _newBuilder = new Scrollpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Scrollpane.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree containerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("container"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(containerPropertyTree!= null):((containerPropertyTree == null)||(!containerPropertyTree.isLeaf())))) {
            _other.container = this.container;
        }
        final PropertyTree textareaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textarea"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textareaPropertyTree!= null):((textareaPropertyTree == null)||(!textareaPropertyTree.isLeaf())))) {
            _other.textarea = ((this.textarea == null)?null:this.textarea.newCopyBuilder(_other, textareaPropertyTree, _propertyTreeUse));
        }
        final PropertyTree collectableComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("collectableComponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(collectableComponentPropertyTree!= null):((collectableComponentPropertyTree == null)||(!collectableComponentPropertyTree.isLeaf())))) {
            _other.collectableComponent = ((this.collectableComponent == null)?null:this.collectableComponent.newCopyBuilder(_other, collectableComponentPropertyTree, _propertyTreeUse));
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree horizontalscrollbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("horizontalscrollbar"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(horizontalscrollbarPropertyTree!= null):((horizontalscrollbarPropertyTree == null)||(!horizontalscrollbarPropertyTree.isLeaf())))) {
            _other.horizontalscrollbar = this.horizontalscrollbar;
        }
        final PropertyTree verticalscrollbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("verticalscrollbar"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(verticalscrollbarPropertyTree!= null):((verticalscrollbarPropertyTree == null)||(!verticalscrollbarPropertyTree.isLeaf())))) {
            _other.verticalscrollbar = this.verticalscrollbar;
        }
    }

    public<_B >Scrollpane.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Scrollpane.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Scrollpane.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Scrollpane.Builder<_B> copyOf(final Scrollpane _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Scrollpane.Builder<_B> _newBuilder = new Scrollpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Scrollpane.Builder<Void> copyExcept(final Scrollpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Scrollpane.Builder<Void> copyOnly(final Scrollpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Scrollpane _storedValue;
        private JAXBElement<?> layoutconstraints;
        private ClearBorder.Builder<Scrollpane.Builder<_B>> clearBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<Scrollpane.Builder<_B>> minimumSize;
        private PreferredSize.Builder<Scrollpane.Builder<_B>> preferredSize;
        private StrictSize.Builder<Scrollpane.Builder<_B>> strictSize;
        private JAXBElement<?> container;
        private Textarea.Builder<Scrollpane.Builder<_B>> textarea;
        private CollectableComponent.Builder<Scrollpane.Builder<_B>> collectableComponent;
        private String name;
        private String horizontalscrollbar;
        private String verticalscrollbar;

        public Builder(final _B _parentBuilder, final Scrollpane _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    this.container = _other.container;
                    this.textarea = ((_other.textarea == null)?null:_other.textarea.newCopyBuilder(this));
                    this.collectableComponent = ((_other.collectableComponent == null)?null:_other.collectableComponent.newCopyBuilder(this));
                    this.name = _other.name;
                    this.horizontalscrollbar = _other.horizontalscrollbar;
                    this.verticalscrollbar = _other.verticalscrollbar;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Scrollpane _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree containerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("container"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(containerPropertyTree!= null):((containerPropertyTree == null)||(!containerPropertyTree.isLeaf())))) {
                        this.container = _other.container;
                    }
                    final PropertyTree textareaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textarea"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textareaPropertyTree!= null):((textareaPropertyTree == null)||(!textareaPropertyTree.isLeaf())))) {
                        this.textarea = ((_other.textarea == null)?null:_other.textarea.newCopyBuilder(this, textareaPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree collectableComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("collectableComponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(collectableComponentPropertyTree!= null):((collectableComponentPropertyTree == null)||(!collectableComponentPropertyTree.isLeaf())))) {
                        this.collectableComponent = ((_other.collectableComponent == null)?null:_other.collectableComponent.newCopyBuilder(this, collectableComponentPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree horizontalscrollbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("horizontalscrollbar"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(horizontalscrollbarPropertyTree!= null):((horizontalscrollbarPropertyTree == null)||(!horizontalscrollbarPropertyTree.isLeaf())))) {
                        this.horizontalscrollbar = _other.horizontalscrollbar;
                    }
                    final PropertyTree verticalscrollbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("verticalscrollbar"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(verticalscrollbarPropertyTree!= null):((verticalscrollbarPropertyTree == null)||(!verticalscrollbarPropertyTree.isLeaf())))) {
                        this.verticalscrollbar = _other.verticalscrollbar;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Scrollpane >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            if (this.border!= null) {
                final List<JAXBElement<?>> border = new ArrayList<JAXBElement<?>>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((JAXBElement<?> ) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            _product.container = this.container;
            _product.textarea = ((this.textarea == null)?null:this.textarea.build());
            _product.collectableComponent = ((this.collectableComponent == null)?null:this.collectableComponent.build());
            _product.name = this.name;
            _product.horizontalscrollbar = this.horizontalscrollbar;
            _product.verticalscrollbar = this.verticalscrollbar;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public Scrollpane.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public Scrollpane.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<Scrollpane.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Scrollpane.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<Scrollpane.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Scrollpane.Builder<_B> addBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Scrollpane.Builder<_B> withBorder(final Iterable<? extends JAXBElement<?>> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Scrollpane.Builder<_B> addBorder(JAXBElement<?> ... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Scrollpane.Builder<_B> withBorder(JAXBElement<?> ... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public Scrollpane.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<Scrollpane.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Scrollpane.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<Scrollpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public Scrollpane.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<Scrollpane.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Scrollpane.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<Scrollpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public Scrollpane.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<Scrollpane.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Scrollpane.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<Scrollpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "container" (any previous value will be replaced)
         * 
         * @param container
         *     New value of the "container" property.
         */
        public Scrollpane.Builder<_B> withContainer(final JAXBElement<?> container) {
            this.container = container;
            return this;
        }

        /**
         * Sets the new value of "textarea" (any previous value will be replaced)
         * 
         * @param textarea
         *     New value of the "textarea" property.
         */
        public Scrollpane.Builder<_B> withTextarea(final Textarea textarea) {
            this.textarea = ((textarea == null)?null:new Textarea.Builder<Scrollpane.Builder<_B>>(this, textarea, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "textarea" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Textarea.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "textarea" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Textarea.Builder#end()} to return to the current builder.
         */
        public Textarea.Builder<? extends Scrollpane.Builder<_B>> withTextarea() {
            if (this.textarea!= null) {
                return this.textarea;
            }
            return this.textarea = new Textarea.Builder<Scrollpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "collectableComponent" (any previous value will be replaced)
         * 
         * @param collectableComponent
         *     New value of the "collectableComponent" property.
         */
        public Scrollpane.Builder<_B> withCollectableComponent(final CollectableComponent collectableComponent) {
            this.collectableComponent = ((collectableComponent == null)?null:new CollectableComponent.Builder<Scrollpane.Builder<_B>>(this, collectableComponent, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "collectableComponent" property.
         * Use {@link org.nuclos.schema.layout.layoutml.CollectableComponent.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "collectableComponent" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.CollectableComponent.Builder#end()} to return to the current builder.
         */
        public CollectableComponent.Builder<? extends Scrollpane.Builder<_B>> withCollectableComponent() {
            if (this.collectableComponent!= null) {
                return this.collectableComponent;
            }
            return this.collectableComponent = new CollectableComponent.Builder<Scrollpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Scrollpane.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "horizontalscrollbar" (any previous value will be replaced)
         * 
         * @param horizontalscrollbar
         *     New value of the "horizontalscrollbar" property.
         */
        public Scrollpane.Builder<_B> withHorizontalscrollbar(final String horizontalscrollbar) {
            this.horizontalscrollbar = horizontalscrollbar;
            return this;
        }

        /**
         * Sets the new value of "verticalscrollbar" (any previous value will be replaced)
         * 
         * @param verticalscrollbar
         *     New value of the "verticalscrollbar" property.
         */
        public Scrollpane.Builder<_B> withVerticalscrollbar(final String verticalscrollbar) {
            this.verticalscrollbar = verticalscrollbar;
            return this;
        }

        @Override
        public Scrollpane build() {
            if (_storedValue == null) {
                return this.init(new Scrollpane());
            } else {
                return ((Scrollpane) _storedValue);
            }
        }

        public Scrollpane.Builder<_B> copyOf(final Scrollpane _other) {
            _other.copyTo(this);
            return this;
        }

        public Scrollpane.Builder<_B> copyOf(final Scrollpane.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Scrollpane.Selector<Scrollpane.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Scrollpane.Select _root() {
            return new Scrollpane.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> layoutconstraints = null;
        private ClearBorder.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> clearBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> strictSize = null;
        private com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> container = null;
        private Textarea.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> textarea = null;
        private CollectableComponent.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> collectableComponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> horizontalscrollbar = null;
        private com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> verticalscrollbar = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.container!= null) {
                products.put("container", this.container.init());
            }
            if (this.textarea!= null) {
                products.put("textarea", this.textarea.init());
            }
            if (this.collectableComponent!= null) {
                products.put("collectableComponent", this.collectableComponent.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.horizontalscrollbar!= null) {
                products.put("horizontalscrollbar", this.horizontalscrollbar.init());
            }
            if (this.verticalscrollbar!= null) {
                products.put("verticalscrollbar", this.verticalscrollbar.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public ClearBorder.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> container() {
            return ((this.container == null)?this.container = new com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "container"):this.container);
        }

        public Textarea.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> textarea() {
            return ((this.textarea == null)?this.textarea = new Textarea.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "textarea"):this.textarea);
        }

        public CollectableComponent.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> collectableComponent() {
            return ((this.collectableComponent == null)?this.collectableComponent = new CollectableComponent.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "collectableComponent"):this.collectableComponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> horizontalscrollbar() {
            return ((this.horizontalscrollbar == null)?this.horizontalscrollbar = new com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "horizontalscrollbar"):this.horizontalscrollbar);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>> verticalscrollbar() {
            return ((this.verticalscrollbar == null)?this.verticalscrollbar = new com.kscs.util.jaxb.Selector<TRoot, Scrollpane.Selector<TRoot, TParent>>(this._root, this, "verticalscrollbar"):this.verticalscrollbar);
        }

    }

}
