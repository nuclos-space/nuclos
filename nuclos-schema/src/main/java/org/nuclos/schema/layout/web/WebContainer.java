
package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A container element which can hold more web-elements.
 * 
 * <p>Java class for web-container complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-container"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.layout.web}web-component"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="grid" type="{urn:org.nuclos.schema.layout.web}web-grid"/&gt;
 *         &lt;element name="table" type="{urn:org.nuclos.schema.layout.web}web-table"/&gt;
 *         &lt;element name="calculated" type="{urn:org.nuclos.schema.layout.web}web-grid-calculated"/&gt;
 *         &lt;sequence&gt;
 *           &lt;element name="components" type="{urn:org.nuclos.schema.layout.web}web-component" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="opaque" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="background-color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-container", propOrder = {
    "grid",
    "table",
    "calculated",
    "components"
})
@XmlSeeAlso({
    WebPanel.class
})
public class WebContainer
    extends WebComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected WebGrid grid;
    protected WebTable table;
    protected WebGridCalculated calculated;
    protected List<WebComponent> components;
    @XmlAttribute(name = "opaque")
    protected Boolean opaque;
    @XmlAttribute(name = "background-color")
    protected String backgroundColor;

    /**
     * Gets the value of the grid property.
     * 
     * @return
     *     possible object is
     *     {@link WebGrid }
     *     
     */
    public WebGrid getGrid() {
        return grid;
    }

    /**
     * Sets the value of the grid property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebGrid }
     *     
     */
    public void setGrid(WebGrid value) {
        this.grid = value;
    }

    /**
     * Gets the value of the table property.
     * 
     * @return
     *     possible object is
     *     {@link WebTable }
     *     
     */
    public WebTable getTable() {
        return table;
    }

    /**
     * Sets the value of the table property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebTable }
     *     
     */
    public void setTable(WebTable value) {
        this.table = value;
    }

    /**
     * Gets the value of the calculated property.
     * 
     * @return
     *     possible object is
     *     {@link WebGridCalculated }
     *     
     */
    public WebGridCalculated getCalculated() {
        return calculated;
    }

    /**
     * Sets the value of the calculated property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebGridCalculated }
     *     
     */
    public void setCalculated(WebGridCalculated value) {
        this.calculated = value;
    }

    /**
     * Gets the value of the components property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the components property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebComponent }
     * 
     * 
     */
    public List<WebComponent> getComponents() {
        if (components == null) {
            components = new ArrayList<WebComponent>();
        }
        return this.components;
    }

    /**
     * Gets the value of the opaque property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOpaque() {
        return opaque;
    }

    /**
     * Sets the value of the opaque property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpaque(Boolean value) {
        this.opaque = value;
    }

    /**
     * Gets the value of the backgroundColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Sets the value of the backgroundColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBackgroundColor(String value) {
        this.backgroundColor = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            WebGrid theGrid;
            theGrid = this.getGrid();
            strategy.appendField(locator, this, "grid", buffer, theGrid);
        }
        {
            WebTable theTable;
            theTable = this.getTable();
            strategy.appendField(locator, this, "table", buffer, theTable);
        }
        {
            WebGridCalculated theCalculated;
            theCalculated = this.getCalculated();
            strategy.appendField(locator, this, "calculated", buffer, theCalculated);
        }
        {
            List<WebComponent> theComponents;
            theComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            strategy.appendField(locator, this, "components", buffer, theComponents);
        }
        {
            Boolean theOpaque;
            theOpaque = this.isOpaque();
            strategy.appendField(locator, this, "opaque", buffer, theOpaque);
        }
        {
            String theBackgroundColor;
            theBackgroundColor = this.getBackgroundColor();
            strategy.appendField(locator, this, "backgroundColor", buffer, theBackgroundColor);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebContainer)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebContainer that = ((WebContainer) object);
        {
            WebGrid lhsGrid;
            lhsGrid = this.getGrid();
            WebGrid rhsGrid;
            rhsGrid = that.getGrid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "grid", lhsGrid), LocatorUtils.property(thatLocator, "grid", rhsGrid), lhsGrid, rhsGrid)) {
                return false;
            }
        }
        {
            WebTable lhsTable;
            lhsTable = this.getTable();
            WebTable rhsTable;
            rhsTable = that.getTable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "table", lhsTable), LocatorUtils.property(thatLocator, "table", rhsTable), lhsTable, rhsTable)) {
                return false;
            }
        }
        {
            WebGridCalculated lhsCalculated;
            lhsCalculated = this.getCalculated();
            WebGridCalculated rhsCalculated;
            rhsCalculated = that.getCalculated();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "calculated", lhsCalculated), LocatorUtils.property(thatLocator, "calculated", rhsCalculated), lhsCalculated, rhsCalculated)) {
                return false;
            }
        }
        {
            List<WebComponent> lhsComponents;
            lhsComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            List<WebComponent> rhsComponents;
            rhsComponents = (((that.components!= null)&&(!that.components.isEmpty()))?that.getComponents():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "components", lhsComponents), LocatorUtils.property(thatLocator, "components", rhsComponents), lhsComponents, rhsComponents)) {
                return false;
            }
        }
        {
            Boolean lhsOpaque;
            lhsOpaque = this.isOpaque();
            Boolean rhsOpaque;
            rhsOpaque = that.isOpaque();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opaque", lhsOpaque), LocatorUtils.property(thatLocator, "opaque", rhsOpaque), lhsOpaque, rhsOpaque)) {
                return false;
            }
        }
        {
            String lhsBackgroundColor;
            lhsBackgroundColor = this.getBackgroundColor();
            String rhsBackgroundColor;
            rhsBackgroundColor = that.getBackgroundColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "backgroundColor", lhsBackgroundColor), LocatorUtils.property(thatLocator, "backgroundColor", rhsBackgroundColor), lhsBackgroundColor, rhsBackgroundColor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            WebGrid theGrid;
            theGrid = this.getGrid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "grid", theGrid), currentHashCode, theGrid);
        }
        {
            WebTable theTable;
            theTable = this.getTable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "table", theTable), currentHashCode, theTable);
        }
        {
            WebGridCalculated theCalculated;
            theCalculated = this.getCalculated();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "calculated", theCalculated), currentHashCode, theCalculated);
        }
        {
            List<WebComponent> theComponents;
            theComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "components", theComponents), currentHashCode, theComponents);
        }
        {
            Boolean theOpaque;
            theOpaque = this.isOpaque();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opaque", theOpaque), currentHashCode, theOpaque);
        }
        {
            String theBackgroundColor;
            theBackgroundColor = this.getBackgroundColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "backgroundColor", theBackgroundColor), currentHashCode, theBackgroundColor);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebContainer) {
            final WebContainer copy = ((WebContainer) draftCopy);
            if (this.grid!= null) {
                WebGrid sourceGrid;
                sourceGrid = this.getGrid();
                WebGrid copyGrid = ((WebGrid) strategy.copy(LocatorUtils.property(locator, "grid", sourceGrid), sourceGrid));
                copy.setGrid(copyGrid);
            } else {
                copy.grid = null;
            }
            if (this.table!= null) {
                WebTable sourceTable;
                sourceTable = this.getTable();
                WebTable copyTable = ((WebTable) strategy.copy(LocatorUtils.property(locator, "table", sourceTable), sourceTable));
                copy.setTable(copyTable);
            } else {
                copy.table = null;
            }
            if (this.calculated!= null) {
                WebGridCalculated sourceCalculated;
                sourceCalculated = this.getCalculated();
                WebGridCalculated copyCalculated = ((WebGridCalculated) strategy.copy(LocatorUtils.property(locator, "calculated", sourceCalculated), sourceCalculated));
                copy.setCalculated(copyCalculated);
            } else {
                copy.calculated = null;
            }
            if ((this.components!= null)&&(!this.components.isEmpty())) {
                List<WebComponent> sourceComponents;
                sourceComponents = (((this.components!= null)&&(!this.components.isEmpty()))?this.getComponents():null);
                @SuppressWarnings("unchecked")
                List<WebComponent> copyComponents = ((List<WebComponent> ) strategy.copy(LocatorUtils.property(locator, "components", sourceComponents), sourceComponents));
                copy.components = null;
                if (copyComponents!= null) {
                    List<WebComponent> uniqueComponentsl = copy.getComponents();
                    uniqueComponentsl.addAll(copyComponents);
                }
            } else {
                copy.components = null;
            }
            if (this.opaque!= null) {
                Boolean sourceOpaque;
                sourceOpaque = this.isOpaque();
                Boolean copyOpaque = ((Boolean) strategy.copy(LocatorUtils.property(locator, "opaque", sourceOpaque), sourceOpaque));
                copy.setOpaque(copyOpaque);
            } else {
                copy.opaque = null;
            }
            if (this.backgroundColor!= null) {
                String sourceBackgroundColor;
                sourceBackgroundColor = this.getBackgroundColor();
                String copyBackgroundColor = ((String) strategy.copy(LocatorUtils.property(locator, "backgroundColor", sourceBackgroundColor), sourceBackgroundColor));
                copy.setBackgroundColor(copyBackgroundColor);
            } else {
                copy.backgroundColor = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebContainer();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebContainer.Builder<_B> _other) {
        super.copyTo(_other);
        _other.grid = ((this.grid == null)?null:this.grid.newCopyBuilder(_other));
        _other.table = ((this.table == null)?null:this.table.newCopyBuilder(_other));
        _other.calculated = ((this.calculated == null)?null:this.calculated.newCopyBuilder(_other));
        if (this.components == null) {
            _other.components = null;
        } else {
            _other.components = new ArrayList<WebComponent.Builder<WebContainer.Builder<_B>>>();
            for (WebComponent _item: this.components) {
                _other.components.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.opaque = this.opaque;
        _other.backgroundColor = this.backgroundColor;
    }

    @Override
    public<_B >WebContainer.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebContainer.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebContainer.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebContainer.Builder<Void> builder() {
        return new WebContainer.Builder<Void>(null, null, false);
    }

    public static<_B >WebContainer.Builder<_B> copyOf(final WebComponent _other) {
        final WebContainer.Builder<_B> _newBuilder = new WebContainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebContainer.Builder<_B> copyOf(final WebContainer _other) {
        final WebContainer.Builder<_B> _newBuilder = new WebContainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebContainer.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree gridPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("grid"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridPropertyTree!= null):((gridPropertyTree == null)||(!gridPropertyTree.isLeaf())))) {
            _other.grid = ((this.grid == null)?null:this.grid.newCopyBuilder(_other, gridPropertyTree, _propertyTreeUse));
        }
        final PropertyTree tablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("table"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tablePropertyTree!= null):((tablePropertyTree == null)||(!tablePropertyTree.isLeaf())))) {
            _other.table = ((this.table == null)?null:this.table.newCopyBuilder(_other, tablePropertyTree, _propertyTreeUse));
        }
        final PropertyTree calculatedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("calculated"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(calculatedPropertyTree!= null):((calculatedPropertyTree == null)||(!calculatedPropertyTree.isLeaf())))) {
            _other.calculated = ((this.calculated == null)?null:this.calculated.newCopyBuilder(_other, calculatedPropertyTree, _propertyTreeUse));
        }
        final PropertyTree componentsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("components"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentsPropertyTree!= null):((componentsPropertyTree == null)||(!componentsPropertyTree.isLeaf())))) {
            if (this.components == null) {
                _other.components = null;
            } else {
                _other.components = new ArrayList<WebComponent.Builder<WebContainer.Builder<_B>>>();
                for (WebComponent _item: this.components) {
                    _other.components.add(((_item == null)?null:_item.newCopyBuilder(_other, componentsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
            _other.opaque = this.opaque;
        }
        final PropertyTree backgroundColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("backgroundColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundColorPropertyTree!= null):((backgroundColorPropertyTree == null)||(!backgroundColorPropertyTree.isLeaf())))) {
            _other.backgroundColor = this.backgroundColor;
        }
    }

    @Override
    public<_B >WebContainer.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebContainer.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebContainer.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebContainer.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebContainer.Builder<_B> _newBuilder = new WebContainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebContainer.Builder<_B> copyOf(final WebContainer _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebContainer.Builder<_B> _newBuilder = new WebContainer.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebContainer.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebContainer.Builder<Void> copyExcept(final WebContainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebContainer.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebContainer.Builder<Void> copyOnly(final WebContainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebComponent.Builder<_B>
        implements Buildable
    {

        private WebGrid.Builder<WebContainer.Builder<_B>> grid;
        private WebTable.Builder<WebContainer.Builder<_B>> table;
        private WebGridCalculated.Builder<WebContainer.Builder<_B>> calculated;
        private List<WebComponent.Builder<WebContainer.Builder<_B>>> components;
        private Boolean opaque;
        private String backgroundColor;

        public Builder(final _B _parentBuilder, final WebContainer _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.grid = ((_other.grid == null)?null:_other.grid.newCopyBuilder(this));
                this.table = ((_other.table == null)?null:_other.table.newCopyBuilder(this));
                this.calculated = ((_other.calculated == null)?null:_other.calculated.newCopyBuilder(this));
                if (_other.components == null) {
                    this.components = null;
                } else {
                    this.components = new ArrayList<WebComponent.Builder<WebContainer.Builder<_B>>>();
                    for (WebComponent _item: _other.components) {
                        this.components.add(((_item == null)?null:_item.newCopyBuilder(this)));
                    }
                }
                this.opaque = _other.opaque;
                this.backgroundColor = _other.backgroundColor;
            }
        }

        public Builder(final _B _parentBuilder, final WebContainer _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree gridPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("grid"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(gridPropertyTree!= null):((gridPropertyTree == null)||(!gridPropertyTree.isLeaf())))) {
                    this.grid = ((_other.grid == null)?null:_other.grid.newCopyBuilder(this, gridPropertyTree, _propertyTreeUse));
                }
                final PropertyTree tablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("table"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tablePropertyTree!= null):((tablePropertyTree == null)||(!tablePropertyTree.isLeaf())))) {
                    this.table = ((_other.table == null)?null:_other.table.newCopyBuilder(this, tablePropertyTree, _propertyTreeUse));
                }
                final PropertyTree calculatedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("calculated"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(calculatedPropertyTree!= null):((calculatedPropertyTree == null)||(!calculatedPropertyTree.isLeaf())))) {
                    this.calculated = ((_other.calculated == null)?null:_other.calculated.newCopyBuilder(this, calculatedPropertyTree, _propertyTreeUse));
                }
                final PropertyTree componentsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("components"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentsPropertyTree!= null):((componentsPropertyTree == null)||(!componentsPropertyTree.isLeaf())))) {
                    if (_other.components == null) {
                        this.components = null;
                    } else {
                        this.components = new ArrayList<WebComponent.Builder<WebContainer.Builder<_B>>>();
                        for (WebComponent _item: _other.components) {
                            this.components.add(((_item == null)?null:_item.newCopyBuilder(this, componentsPropertyTree, _propertyTreeUse)));
                        }
                    }
                }
                final PropertyTree opaquePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("opaque"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(opaquePropertyTree!= null):((opaquePropertyTree == null)||(!opaquePropertyTree.isLeaf())))) {
                    this.opaque = _other.opaque;
                }
                final PropertyTree backgroundColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("backgroundColor"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundColorPropertyTree!= null):((backgroundColorPropertyTree == null)||(!backgroundColorPropertyTree.isLeaf())))) {
                    this.backgroundColor = _other.backgroundColor;
                }
            }
        }

        protected<_P extends WebContainer >_P init(final _P _product) {
            _product.grid = ((this.grid == null)?null:this.grid.build());
            _product.table = ((this.table == null)?null:this.table.build());
            _product.calculated = ((this.calculated == null)?null:this.calculated.build());
            if (this.components!= null) {
                final List<WebComponent> components = new ArrayList<WebComponent>(this.components.size());
                for (WebComponent.Builder<WebContainer.Builder<_B>> _item: this.components) {
                    components.add(_item.build());
                }
                _product.components = components;
            }
            _product.opaque = this.opaque;
            _product.backgroundColor = this.backgroundColor;
            return super.init(_product);
        }

        /**
         * Sets the new value of "grid" (any previous value will be replaced)
         * 
         * @param grid
         *     New value of the "grid" property.
         */
        public WebContainer.Builder<_B> withGrid(final WebGrid grid) {
            this.grid = ((grid == null)?null:new WebGrid.Builder<WebContainer.Builder<_B>>(this, grid, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "grid" property.
         * Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "grid" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         */
        public WebGrid.Builder<? extends WebContainer.Builder<_B>> withGrid() {
            if (this.grid!= null) {
                return this.grid;
            }
            return this.grid = new WebGrid.Builder<WebContainer.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "table" (any previous value will be replaced)
         * 
         * @param table
         *     New value of the "table" property.
         */
        public WebContainer.Builder<_B> withTable(final WebTable table) {
            this.table = ((table == null)?null:new WebTable.Builder<WebContainer.Builder<_B>>(this, table, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "table" property.
         * Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "table" property.
         *     Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         */
        public WebTable.Builder<? extends WebContainer.Builder<_B>> withTable() {
            if (this.table!= null) {
                return this.table;
            }
            return this.table = new WebTable.Builder<WebContainer.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "calculated" (any previous value will be replaced)
         * 
         * @param calculated
         *     New value of the "calculated" property.
         */
        public WebContainer.Builder<_B> withCalculated(final WebGridCalculated calculated) {
            this.calculated = ((calculated == null)?null:new WebGridCalculated.Builder<WebContainer.Builder<_B>>(this, calculated, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "calculated" property.
         * Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "calculated" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         */
        public WebGridCalculated.Builder<? extends WebContainer.Builder<_B>> withCalculated() {
            if (this.calculated!= null) {
                return this.calculated;
            }
            return this.calculated = new WebGridCalculated.Builder<WebContainer.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        public WebContainer.Builder<_B> addComponents(final Iterable<? extends WebComponent> components) {
            if (components!= null) {
                if (this.components == null) {
                    this.components = new ArrayList<WebComponent.Builder<WebContainer.Builder<_B>>>();
                }
                for (WebComponent _item: components) {
                    this.components.add(new WebComponent.Builder<WebContainer.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        public WebContainer.Builder<_B> withComponents(final Iterable<? extends WebComponent> components) {
            if (this.components!= null) {
                this.components.clear();
            }
            return addComponents(components);
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        public WebContainer.Builder<_B> addComponents(WebComponent... components) {
            addComponents(Arrays.asList(components));
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        public WebContainer.Builder<_B> withComponents(WebComponent... components) {
            withComponents(Arrays.asList(components));
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        public WebContainer.Builder<_B> withOpaque(final Boolean opaque) {
            this.opaque = opaque;
            return this;
        }

        /**
         * Sets the new value of "backgroundColor" (any previous value will be replaced)
         * 
         * @param backgroundColor
         *     New value of the "backgroundColor" property.
         */
        public WebContainer.Builder<_B> withBackgroundColor(final String backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebContainer.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebContainer.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebContainer.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebContainer.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebContainer.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebContainer.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebContainer.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebContainer.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebContainer.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebContainer.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebContainer.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebContainer.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebContainer.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebContainer.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebContainer.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebContainer.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebContainer.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebContainer.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebContainer.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebContainer.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        @Override
        public WebContainer build() {
            if (_storedValue == null) {
                return this.init(new WebContainer());
            } else {
                return ((WebContainer) _storedValue);
            }
        }

        public WebContainer.Builder<_B> copyOf(final WebContainer _other) {
            _other.copyTo(this);
            return this;
        }

        public WebContainer.Builder<_B> copyOf(final WebContainer.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebContainer.Selector<WebContainer.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebContainer.Select _root() {
            return new WebContainer.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebComponent.Selector<TRoot, TParent>
    {

        private WebGrid.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> grid = null;
        private WebTable.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> table = null;
        private WebGridCalculated.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> calculated = null;
        private WebComponent.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> components = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> opaque = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> backgroundColor = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.grid!= null) {
                products.put("grid", this.grid.init());
            }
            if (this.table!= null) {
                products.put("table", this.table.init());
            }
            if (this.calculated!= null) {
                products.put("calculated", this.calculated.init());
            }
            if (this.components!= null) {
                products.put("components", this.components.init());
            }
            if (this.opaque!= null) {
                products.put("opaque", this.opaque.init());
            }
            if (this.backgroundColor!= null) {
                products.put("backgroundColor", this.backgroundColor.init());
            }
            return products;
        }

        public WebGrid.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> grid() {
            return ((this.grid == null)?this.grid = new WebGrid.Selector<TRoot, WebContainer.Selector<TRoot, TParent>>(this._root, this, "grid"):this.grid);
        }

        public WebTable.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> table() {
            return ((this.table == null)?this.table = new WebTable.Selector<TRoot, WebContainer.Selector<TRoot, TParent>>(this._root, this, "table"):this.table);
        }

        public WebGridCalculated.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> calculated() {
            return ((this.calculated == null)?this.calculated = new WebGridCalculated.Selector<TRoot, WebContainer.Selector<TRoot, TParent>>(this._root, this, "calculated"):this.calculated);
        }

        public WebComponent.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> components() {
            return ((this.components == null)?this.components = new WebComponent.Selector<TRoot, WebContainer.Selector<TRoot, TParent>>(this._root, this, "components"):this.components);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> opaque() {
            return ((this.opaque == null)?this.opaque = new com.kscs.util.jaxb.Selector<TRoot, WebContainer.Selector<TRoot, TParent>>(this._root, this, "opaque"):this.opaque);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebContainer.Selector<TRoot, TParent>> backgroundColor() {
            return ((this.backgroundColor == null)?this.backgroundColor = new com.kscs.util.jaxb.Selector<TRoot, WebContainer.Selector<TRoot, TParent>>(this._root, this, "backgroundColor"):this.backgroundColor);
        }

    }

}
