
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="col1" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="col2" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="row1" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="row2" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="hAlign" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="vAlign" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class TablelayoutConstraints implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "col1", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String col1;
    @XmlAttribute(name = "col2", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String col2;
    @XmlAttribute(name = "row1", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String row1;
    @XmlAttribute(name = "row2", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String row2;
    @XmlAttribute(name = "hAlign", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String hAlign;
    @XmlAttribute(name = "vAlign", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String vAlign;

    /**
     * Gets the value of the col1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCol1() {
        return col1;
    }

    /**
     * Sets the value of the col1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCol1(String value) {
        this.col1 = value;
    }

    /**
     * Gets the value of the col2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCol2() {
        return col2;
    }

    /**
     * Sets the value of the col2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCol2(String value) {
        this.col2 = value;
    }

    /**
     * Gets the value of the row1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRow1() {
        return row1;
    }

    /**
     * Sets the value of the row1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRow1(String value) {
        this.row1 = value;
    }

    /**
     * Gets the value of the row2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRow2() {
        return row2;
    }

    /**
     * Sets the value of the row2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRow2(String value) {
        this.row2 = value;
    }

    /**
     * Gets the value of the hAlign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHAlign() {
        return hAlign;
    }

    /**
     * Sets the value of the hAlign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHAlign(String value) {
        this.hAlign = value;
    }

    /**
     * Gets the value of the vAlign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVAlign() {
        return vAlign;
    }

    /**
     * Sets the value of the vAlign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVAlign(String value) {
        this.vAlign = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theCol1;
            theCol1 = this.getCol1();
            strategy.appendField(locator, this, "col1", buffer, theCol1);
        }
        {
            String theCol2;
            theCol2 = this.getCol2();
            strategy.appendField(locator, this, "col2", buffer, theCol2);
        }
        {
            String theRow1;
            theRow1 = this.getRow1();
            strategy.appendField(locator, this, "row1", buffer, theRow1);
        }
        {
            String theRow2;
            theRow2 = this.getRow2();
            strategy.appendField(locator, this, "row2", buffer, theRow2);
        }
        {
            String theHAlign;
            theHAlign = this.getHAlign();
            strategy.appendField(locator, this, "hAlign", buffer, theHAlign);
        }
        {
            String theVAlign;
            theVAlign = this.getVAlign();
            strategy.appendField(locator, this, "vAlign", buffer, theVAlign);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof TablelayoutConstraints)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final TablelayoutConstraints that = ((TablelayoutConstraints) object);
        {
            String lhsCol1;
            lhsCol1 = this.getCol1();
            String rhsCol1;
            rhsCol1 = that.getCol1();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "col1", lhsCol1), LocatorUtils.property(thatLocator, "col1", rhsCol1), lhsCol1, rhsCol1)) {
                return false;
            }
        }
        {
            String lhsCol2;
            lhsCol2 = this.getCol2();
            String rhsCol2;
            rhsCol2 = that.getCol2();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "col2", lhsCol2), LocatorUtils.property(thatLocator, "col2", rhsCol2), lhsCol2, rhsCol2)) {
                return false;
            }
        }
        {
            String lhsRow1;
            lhsRow1 = this.getRow1();
            String rhsRow1;
            rhsRow1 = that.getRow1();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "row1", lhsRow1), LocatorUtils.property(thatLocator, "row1", rhsRow1), lhsRow1, rhsRow1)) {
                return false;
            }
        }
        {
            String lhsRow2;
            lhsRow2 = this.getRow2();
            String rhsRow2;
            rhsRow2 = that.getRow2();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "row2", lhsRow2), LocatorUtils.property(thatLocator, "row2", rhsRow2), lhsRow2, rhsRow2)) {
                return false;
            }
        }
        {
            String lhsHAlign;
            lhsHAlign = this.getHAlign();
            String rhsHAlign;
            rhsHAlign = that.getHAlign();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hAlign", lhsHAlign), LocatorUtils.property(thatLocator, "hAlign", rhsHAlign), lhsHAlign, rhsHAlign)) {
                return false;
            }
        }
        {
            String lhsVAlign;
            lhsVAlign = this.getVAlign();
            String rhsVAlign;
            rhsVAlign = that.getVAlign();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vAlign", lhsVAlign), LocatorUtils.property(thatLocator, "vAlign", rhsVAlign), lhsVAlign, rhsVAlign)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theCol1;
            theCol1 = this.getCol1();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "col1", theCol1), currentHashCode, theCol1);
        }
        {
            String theCol2;
            theCol2 = this.getCol2();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "col2", theCol2), currentHashCode, theCol2);
        }
        {
            String theRow1;
            theRow1 = this.getRow1();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "row1", theRow1), currentHashCode, theRow1);
        }
        {
            String theRow2;
            theRow2 = this.getRow2();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "row2", theRow2), currentHashCode, theRow2);
        }
        {
            String theHAlign;
            theHAlign = this.getHAlign();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hAlign", theHAlign), currentHashCode, theHAlign);
        }
        {
            String theVAlign;
            theVAlign = this.getVAlign();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vAlign", theVAlign), currentHashCode, theVAlign);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof TablelayoutConstraints) {
            final TablelayoutConstraints copy = ((TablelayoutConstraints) draftCopy);
            if (this.col1 != null) {
                String sourceCol1;
                sourceCol1 = this.getCol1();
                String copyCol1 = ((String) strategy.copy(LocatorUtils.property(locator, "col1", sourceCol1), sourceCol1));
                copy.setCol1(copyCol1);
            } else {
                copy.col1 = null;
            }
            if (this.col2 != null) {
                String sourceCol2;
                sourceCol2 = this.getCol2();
                String copyCol2 = ((String) strategy.copy(LocatorUtils.property(locator, "col2", sourceCol2), sourceCol2));
                copy.setCol2(copyCol2);
            } else {
                copy.col2 = null;
            }
            if (this.row1 != null) {
                String sourceRow1;
                sourceRow1 = this.getRow1();
                String copyRow1 = ((String) strategy.copy(LocatorUtils.property(locator, "row1", sourceRow1), sourceRow1));
                copy.setRow1(copyRow1);
            } else {
                copy.row1 = null;
            }
            if (this.row2 != null) {
                String sourceRow2;
                sourceRow2 = this.getRow2();
                String copyRow2 = ((String) strategy.copy(LocatorUtils.property(locator, "row2", sourceRow2), sourceRow2));
                copy.setRow2(copyRow2);
            } else {
                copy.row2 = null;
            }
            if (this.hAlign!= null) {
                String sourceHAlign;
                sourceHAlign = this.getHAlign();
                String copyHAlign = ((String) strategy.copy(LocatorUtils.property(locator, "hAlign", sourceHAlign), sourceHAlign));
                copy.setHAlign(copyHAlign);
            } else {
                copy.hAlign = null;
            }
            if (this.vAlign!= null) {
                String sourceVAlign;
                sourceVAlign = this.getVAlign();
                String copyVAlign = ((String) strategy.copy(LocatorUtils.property(locator, "vAlign", sourceVAlign), sourceVAlign));
                copy.setVAlign(copyVAlign);
            } else {
                copy.vAlign = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new TablelayoutConstraints();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TablelayoutConstraints.Builder<_B> _other) {
        _other.col1 = this.col1;
        _other.col2 = this.col2;
        _other.row1 = this.row1;
        _other.row2 = this.row2;
        _other.hAlign = this.hAlign;
        _other.vAlign = this.vAlign;
    }

    public<_B >TablelayoutConstraints.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new TablelayoutConstraints.Builder<_B>(_parentBuilder, this, true);
    }

    public TablelayoutConstraints.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static TablelayoutConstraints.Builder<Void> builder() {
        return new TablelayoutConstraints.Builder<Void>(null, null, false);
    }

    public static<_B >TablelayoutConstraints.Builder<_B> copyOf(final TablelayoutConstraints _other) {
        final TablelayoutConstraints.Builder<_B> _newBuilder = new TablelayoutConstraints.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final TablelayoutConstraints.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree col1PropertyTree = ((_propertyTree == null)?null:_propertyTree.get("col1"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(col1PropertyTree!= null):((col1PropertyTree == null)||(!col1PropertyTree.isLeaf())))) {
            _other.col1 = this.col1;
        }
        final PropertyTree col2PropertyTree = ((_propertyTree == null)?null:_propertyTree.get("col2"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(col2PropertyTree!= null):((col2PropertyTree == null)||(!col2PropertyTree.isLeaf())))) {
            _other.col2 = this.col2;
        }
        final PropertyTree row1PropertyTree = ((_propertyTree == null)?null:_propertyTree.get("row1"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(row1PropertyTree!= null):((row1PropertyTree == null)||(!row1PropertyTree.isLeaf())))) {
            _other.row1 = this.row1;
        }
        final PropertyTree row2PropertyTree = ((_propertyTree == null)?null:_propertyTree.get("row2"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(row2PropertyTree!= null):((row2PropertyTree == null)||(!row2PropertyTree.isLeaf())))) {
            _other.row2 = this.row2;
        }
        final PropertyTree hAlignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("hAlign"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hAlignPropertyTree!= null):((hAlignPropertyTree == null)||(!hAlignPropertyTree.isLeaf())))) {
            _other.hAlign = this.hAlign;
        }
        final PropertyTree vAlignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vAlign"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vAlignPropertyTree!= null):((vAlignPropertyTree == null)||(!vAlignPropertyTree.isLeaf())))) {
            _other.vAlign = this.vAlign;
        }
    }

    public<_B >TablelayoutConstraints.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new TablelayoutConstraints.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public TablelayoutConstraints.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >TablelayoutConstraints.Builder<_B> copyOf(final TablelayoutConstraints _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final TablelayoutConstraints.Builder<_B> _newBuilder = new TablelayoutConstraints.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static TablelayoutConstraints.Builder<Void> copyExcept(final TablelayoutConstraints _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static TablelayoutConstraints.Builder<Void> copyOnly(final TablelayoutConstraints _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final TablelayoutConstraints _storedValue;
        private String col1;
        private String col2;
        private String row1;
        private String row2;
        private String hAlign;
        private String vAlign;

        public Builder(final _B _parentBuilder, final TablelayoutConstraints _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.col1 = _other.col1;
                    this.col2 = _other.col2;
                    this.row1 = _other.row1;
                    this.row2 = _other.row2;
                    this.hAlign = _other.hAlign;
                    this.vAlign = _other.vAlign;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final TablelayoutConstraints _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree col1PropertyTree = ((_propertyTree == null)?null:_propertyTree.get("col1"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(col1PropertyTree!= null):((col1PropertyTree == null)||(!col1PropertyTree.isLeaf())))) {
                        this.col1 = _other.col1;
                    }
                    final PropertyTree col2PropertyTree = ((_propertyTree == null)?null:_propertyTree.get("col2"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(col2PropertyTree!= null):((col2PropertyTree == null)||(!col2PropertyTree.isLeaf())))) {
                        this.col2 = _other.col2;
                    }
                    final PropertyTree row1PropertyTree = ((_propertyTree == null)?null:_propertyTree.get("row1"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(row1PropertyTree!= null):((row1PropertyTree == null)||(!row1PropertyTree.isLeaf())))) {
                        this.row1 = _other.row1;
                    }
                    final PropertyTree row2PropertyTree = ((_propertyTree == null)?null:_propertyTree.get("row2"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(row2PropertyTree!= null):((row2PropertyTree == null)||(!row2PropertyTree.isLeaf())))) {
                        this.row2 = _other.row2;
                    }
                    final PropertyTree hAlignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("hAlign"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hAlignPropertyTree!= null):((hAlignPropertyTree == null)||(!hAlignPropertyTree.isLeaf())))) {
                        this.hAlign = _other.hAlign;
                    }
                    final PropertyTree vAlignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vAlign"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vAlignPropertyTree!= null):((vAlignPropertyTree == null)||(!vAlignPropertyTree.isLeaf())))) {
                        this.vAlign = _other.vAlign;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends TablelayoutConstraints >_P init(final _P _product) {
            _product.col1 = this.col1;
            _product.col2 = this.col2;
            _product.row1 = this.row1;
            _product.row2 = this.row2;
            _product.hAlign = this.hAlign;
            _product.vAlign = this.vAlign;
            return _product;
        }

        /**
         * Sets the new value of "col1" (any previous value will be replaced)
         * 
         * @param col1
         *     New value of the "col1" property.
         */
        public TablelayoutConstraints.Builder<_B> withCol1(final String col1) {
            this.col1 = col1;
            return this;
        }

        /**
         * Sets the new value of "col2" (any previous value will be replaced)
         * 
         * @param col2
         *     New value of the "col2" property.
         */
        public TablelayoutConstraints.Builder<_B> withCol2(final String col2) {
            this.col2 = col2;
            return this;
        }

        /**
         * Sets the new value of "row1" (any previous value will be replaced)
         * 
         * @param row1
         *     New value of the "row1" property.
         */
        public TablelayoutConstraints.Builder<_B> withRow1(final String row1) {
            this.row1 = row1;
            return this;
        }

        /**
         * Sets the new value of "row2" (any previous value will be replaced)
         * 
         * @param row2
         *     New value of the "row2" property.
         */
        public TablelayoutConstraints.Builder<_B> withRow2(final String row2) {
            this.row2 = row2;
            return this;
        }

        /**
         * Sets the new value of "hAlign" (any previous value will be replaced)
         * 
         * @param hAlign
         *     New value of the "hAlign" property.
         */
        public TablelayoutConstraints.Builder<_B> withHAlign(final String hAlign) {
            this.hAlign = hAlign;
            return this;
        }

        /**
         * Sets the new value of "vAlign" (any previous value will be replaced)
         * 
         * @param vAlign
         *     New value of the "vAlign" property.
         */
        public TablelayoutConstraints.Builder<_B> withVAlign(final String vAlign) {
            this.vAlign = vAlign;
            return this;
        }

        @Override
        public TablelayoutConstraints build() {
            if (_storedValue == null) {
                return this.init(new TablelayoutConstraints());
            } else {
                return ((TablelayoutConstraints) _storedValue);
            }
        }

        public TablelayoutConstraints.Builder<_B> copyOf(final TablelayoutConstraints _other) {
            _other.copyTo(this);
            return this;
        }

        public TablelayoutConstraints.Builder<_B> copyOf(final TablelayoutConstraints.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends TablelayoutConstraints.Selector<TablelayoutConstraints.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static TablelayoutConstraints.Select _root() {
            return new TablelayoutConstraints.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> col1 = null;
        private com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> col2 = null;
        private com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> row1 = null;
        private com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> row2 = null;
        private com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> hAlign = null;
        private com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> vAlign = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.col1 != null) {
                products.put("col1", this.col1 .init());
            }
            if (this.col2 != null) {
                products.put("col2", this.col2 .init());
            }
            if (this.row1 != null) {
                products.put("row1", this.row1 .init());
            }
            if (this.row2 != null) {
                products.put("row2", this.row2 .init());
            }
            if (this.hAlign!= null) {
                products.put("hAlign", this.hAlign.init());
            }
            if (this.vAlign!= null) {
                products.put("vAlign", this.vAlign.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> col1() {
            return ((this.col1 == null)?this.col1 = new com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>>(this._root, this, "col1"):this.col1);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> col2() {
            return ((this.col2 == null)?this.col2 = new com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>>(this._root, this, "col2"):this.col2);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> row1() {
            return ((this.row1 == null)?this.row1 = new com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>>(this._root, this, "row1"):this.row1);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> row2() {
            return ((this.row2 == null)?this.row2 = new com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>>(this._root, this, "row2"):this.row2);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> hAlign() {
            return ((this.hAlign == null)?this.hAlign = new com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>>(this._root, this, "hAlign"):this.hAlign);
        }

        public com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>> vAlign() {
            return ((this.vAlign == null)?this.vAlign = new com.kscs.util.jaxb.Selector<TRoot, TablelayoutConstraints.Selector<TRoot, TParent>>(this._root, this, "vAlign"):this.vAlign);
        }

    }

}
