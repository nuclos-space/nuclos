
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="top" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="left" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="bottom" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="right" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class EmptyBorder implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "top")
    @XmlSchemaType(name = "anySimpleType")
    protected String top;
    @XmlAttribute(name = "left")
    @XmlSchemaType(name = "anySimpleType")
    protected String left;
    @XmlAttribute(name = "bottom")
    @XmlSchemaType(name = "anySimpleType")
    protected String bottom;
    @XmlAttribute(name = "right")
    @XmlSchemaType(name = "anySimpleType")
    protected String right;

    /**
     * Gets the value of the top property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTop() {
        return top;
    }

    /**
     * Sets the value of the top property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTop(String value) {
        this.top = value;
    }

    /**
     * Gets the value of the left property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeft() {
        return left;
    }

    /**
     * Sets the value of the left property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeft(String value) {
        this.left = value;
    }

    /**
     * Gets the value of the bottom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBottom() {
        return bottom;
    }

    /**
     * Sets the value of the bottom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBottom(String value) {
        this.bottom = value;
    }

    /**
     * Gets the value of the right property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRight() {
        return right;
    }

    /**
     * Sets the value of the right property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRight(String value) {
        this.right = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theTop;
            theTop = this.getTop();
            strategy.appendField(locator, this, "top", buffer, theTop);
        }
        {
            String theLeft;
            theLeft = this.getLeft();
            strategy.appendField(locator, this, "left", buffer, theLeft);
        }
        {
            String theBottom;
            theBottom = this.getBottom();
            strategy.appendField(locator, this, "bottom", buffer, theBottom);
        }
        {
            String theRight;
            theRight = this.getRight();
            strategy.appendField(locator, this, "right", buffer, theRight);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EmptyBorder)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EmptyBorder that = ((EmptyBorder) object);
        {
            String lhsTop;
            lhsTop = this.getTop();
            String rhsTop;
            rhsTop = that.getTop();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "top", lhsTop), LocatorUtils.property(thatLocator, "top", rhsTop), lhsTop, rhsTop)) {
                return false;
            }
        }
        {
            String lhsLeft;
            lhsLeft = this.getLeft();
            String rhsLeft;
            rhsLeft = that.getLeft();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "left", lhsLeft), LocatorUtils.property(thatLocator, "left", rhsLeft), lhsLeft, rhsLeft)) {
                return false;
            }
        }
        {
            String lhsBottom;
            lhsBottom = this.getBottom();
            String rhsBottom;
            rhsBottom = that.getBottom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bottom", lhsBottom), LocatorUtils.property(thatLocator, "bottom", rhsBottom), lhsBottom, rhsBottom)) {
                return false;
            }
        }
        {
            String lhsRight;
            lhsRight = this.getRight();
            String rhsRight;
            rhsRight = that.getRight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "right", lhsRight), LocatorUtils.property(thatLocator, "right", rhsRight), lhsRight, rhsRight)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theTop;
            theTop = this.getTop();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "top", theTop), currentHashCode, theTop);
        }
        {
            String theLeft;
            theLeft = this.getLeft();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "left", theLeft), currentHashCode, theLeft);
        }
        {
            String theBottom;
            theBottom = this.getBottom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bottom", theBottom), currentHashCode, theBottom);
        }
        {
            String theRight;
            theRight = this.getRight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "right", theRight), currentHashCode, theRight);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EmptyBorder) {
            final EmptyBorder copy = ((EmptyBorder) draftCopy);
            if (this.top!= null) {
                String sourceTop;
                sourceTop = this.getTop();
                String copyTop = ((String) strategy.copy(LocatorUtils.property(locator, "top", sourceTop), sourceTop));
                copy.setTop(copyTop);
            } else {
                copy.top = null;
            }
            if (this.left!= null) {
                String sourceLeft;
                sourceLeft = this.getLeft();
                String copyLeft = ((String) strategy.copy(LocatorUtils.property(locator, "left", sourceLeft), sourceLeft));
                copy.setLeft(copyLeft);
            } else {
                copy.left = null;
            }
            if (this.bottom!= null) {
                String sourceBottom;
                sourceBottom = this.getBottom();
                String copyBottom = ((String) strategy.copy(LocatorUtils.property(locator, "bottom", sourceBottom), sourceBottom));
                copy.setBottom(copyBottom);
            } else {
                copy.bottom = null;
            }
            if (this.right!= null) {
                String sourceRight;
                sourceRight = this.getRight();
                String copyRight = ((String) strategy.copy(LocatorUtils.property(locator, "right", sourceRight), sourceRight));
                copy.setRight(copyRight);
            } else {
                copy.right = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EmptyBorder();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EmptyBorder.Builder<_B> _other) {
        _other.top = this.top;
        _other.left = this.left;
        _other.bottom = this.bottom;
        _other.right = this.right;
    }

    public<_B >EmptyBorder.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EmptyBorder.Builder<_B>(_parentBuilder, this, true);
    }

    public EmptyBorder.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EmptyBorder.Builder<Void> builder() {
        return new EmptyBorder.Builder<Void>(null, null, false);
    }

    public static<_B >EmptyBorder.Builder<_B> copyOf(final EmptyBorder _other) {
        final EmptyBorder.Builder<_B> _newBuilder = new EmptyBorder.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EmptyBorder.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree topPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("top"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(topPropertyTree!= null):((topPropertyTree == null)||(!topPropertyTree.isLeaf())))) {
            _other.top = this.top;
        }
        final PropertyTree leftPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("left"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(leftPropertyTree!= null):((leftPropertyTree == null)||(!leftPropertyTree.isLeaf())))) {
            _other.left = this.left;
        }
        final PropertyTree bottomPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bottom"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bottomPropertyTree!= null):((bottomPropertyTree == null)||(!bottomPropertyTree.isLeaf())))) {
            _other.bottom = this.bottom;
        }
        final PropertyTree rightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("right"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rightPropertyTree!= null):((rightPropertyTree == null)||(!rightPropertyTree.isLeaf())))) {
            _other.right = this.right;
        }
    }

    public<_B >EmptyBorder.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EmptyBorder.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EmptyBorder.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EmptyBorder.Builder<_B> copyOf(final EmptyBorder _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EmptyBorder.Builder<_B> _newBuilder = new EmptyBorder.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EmptyBorder.Builder<Void> copyExcept(final EmptyBorder _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EmptyBorder.Builder<Void> copyOnly(final EmptyBorder _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EmptyBorder _storedValue;
        private String top;
        private String left;
        private String bottom;
        private String right;

        public Builder(final _B _parentBuilder, final EmptyBorder _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.top = _other.top;
                    this.left = _other.left;
                    this.bottom = _other.bottom;
                    this.right = _other.right;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EmptyBorder _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree topPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("top"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(topPropertyTree!= null):((topPropertyTree == null)||(!topPropertyTree.isLeaf())))) {
                        this.top = _other.top;
                    }
                    final PropertyTree leftPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("left"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(leftPropertyTree!= null):((leftPropertyTree == null)||(!leftPropertyTree.isLeaf())))) {
                        this.left = _other.left;
                    }
                    final PropertyTree bottomPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bottom"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bottomPropertyTree!= null):((bottomPropertyTree == null)||(!bottomPropertyTree.isLeaf())))) {
                        this.bottom = _other.bottom;
                    }
                    final PropertyTree rightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("right"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rightPropertyTree!= null):((rightPropertyTree == null)||(!rightPropertyTree.isLeaf())))) {
                        this.right = _other.right;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EmptyBorder >_P init(final _P _product) {
            _product.top = this.top;
            _product.left = this.left;
            _product.bottom = this.bottom;
            _product.right = this.right;
            return _product;
        }

        /**
         * Sets the new value of "top" (any previous value will be replaced)
         * 
         * @param top
         *     New value of the "top" property.
         */
        public EmptyBorder.Builder<_B> withTop(final String top) {
            this.top = top;
            return this;
        }

        /**
         * Sets the new value of "left" (any previous value will be replaced)
         * 
         * @param left
         *     New value of the "left" property.
         */
        public EmptyBorder.Builder<_B> withLeft(final String left) {
            this.left = left;
            return this;
        }

        /**
         * Sets the new value of "bottom" (any previous value will be replaced)
         * 
         * @param bottom
         *     New value of the "bottom" property.
         */
        public EmptyBorder.Builder<_B> withBottom(final String bottom) {
            this.bottom = bottom;
            return this;
        }

        /**
         * Sets the new value of "right" (any previous value will be replaced)
         * 
         * @param right
         *     New value of the "right" property.
         */
        public EmptyBorder.Builder<_B> withRight(final String right) {
            this.right = right;
            return this;
        }

        @Override
        public EmptyBorder build() {
            if (_storedValue == null) {
                return this.init(new EmptyBorder());
            } else {
                return ((EmptyBorder) _storedValue);
            }
        }

        public EmptyBorder.Builder<_B> copyOf(final EmptyBorder _other) {
            _other.copyTo(this);
            return this;
        }

        public EmptyBorder.Builder<_B> copyOf(final EmptyBorder.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EmptyBorder.Selector<EmptyBorder.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EmptyBorder.Select _root() {
            return new EmptyBorder.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>> top = null;
        private com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>> left = null;
        private com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>> bottom = null;
        private com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>> right = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.top!= null) {
                products.put("top", this.top.init());
            }
            if (this.left!= null) {
                products.put("left", this.left.init());
            }
            if (this.bottom!= null) {
                products.put("bottom", this.bottom.init());
            }
            if (this.right!= null) {
                products.put("right", this.right.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>> top() {
            return ((this.top == null)?this.top = new com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>>(this._root, this, "top"):this.top);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>> left() {
            return ((this.left == null)?this.left = new com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>>(this._root, this, "left"):this.left);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>> bottom() {
            return ((this.bottom == null)?this.bottom = new com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>>(this._root, this, "bottom"):this.bottom);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>> right() {
            return ((this.right == null)?this.right = new com.kscs.util.jaxb.Selector<TRoot, EmptyBorder.Selector<TRoot, TParent>>(this._root, this, "right"):this.right);
        }

    }

}
