
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="columns" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="rows" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Tablelayout implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "columns", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String columns;
    @XmlAttribute(name = "rows", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String rows;

    /**
     * Gets the value of the columns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumns() {
        return columns;
    }

    /**
     * Sets the value of the columns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumns(String value) {
        this.columns = value;
    }

    /**
     * Gets the value of the rows property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRows() {
        return rows;
    }

    /**
     * Sets the value of the rows property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRows(String value) {
        this.rows = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theColumns;
            theColumns = this.getColumns();
            strategy.appendField(locator, this, "columns", buffer, theColumns);
        }
        {
            String theRows;
            theRows = this.getRows();
            strategy.appendField(locator, this, "rows", buffer, theRows);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Tablelayout)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Tablelayout that = ((Tablelayout) object);
        {
            String lhsColumns;
            lhsColumns = this.getColumns();
            String rhsColumns;
            rhsColumns = that.getColumns();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "columns", lhsColumns), LocatorUtils.property(thatLocator, "columns", rhsColumns), lhsColumns, rhsColumns)) {
                return false;
            }
        }
        {
            String lhsRows;
            lhsRows = this.getRows();
            String rhsRows;
            rhsRows = that.getRows();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rows", lhsRows), LocatorUtils.property(thatLocator, "rows", rhsRows), lhsRows, rhsRows)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theColumns;
            theColumns = this.getColumns();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "columns", theColumns), currentHashCode, theColumns);
        }
        {
            String theRows;
            theRows = this.getRows();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rows", theRows), currentHashCode, theRows);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Tablelayout) {
            final Tablelayout copy = ((Tablelayout) draftCopy);
            if (this.columns!= null) {
                String sourceColumns;
                sourceColumns = this.getColumns();
                String copyColumns = ((String) strategy.copy(LocatorUtils.property(locator, "columns", sourceColumns), sourceColumns));
                copy.setColumns(copyColumns);
            } else {
                copy.columns = null;
            }
            if (this.rows!= null) {
                String sourceRows;
                sourceRows = this.getRows();
                String copyRows = ((String) strategy.copy(LocatorUtils.property(locator, "rows", sourceRows), sourceRows));
                copy.setRows(copyRows);
            } else {
                copy.rows = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Tablelayout();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Tablelayout.Builder<_B> _other) {
        _other.columns = this.columns;
        _other.rows = this.rows;
    }

    public<_B >Tablelayout.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Tablelayout.Builder<_B>(_parentBuilder, this, true);
    }

    public Tablelayout.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Tablelayout.Builder<Void> builder() {
        return new Tablelayout.Builder<Void>(null, null, false);
    }

    public static<_B >Tablelayout.Builder<_B> copyOf(final Tablelayout _other) {
        final Tablelayout.Builder<_B> _newBuilder = new Tablelayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Tablelayout.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
            _other.columns = this.columns;
        }
        final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
            _other.rows = this.rows;
        }
    }

    public<_B >Tablelayout.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Tablelayout.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Tablelayout.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Tablelayout.Builder<_B> copyOf(final Tablelayout _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Tablelayout.Builder<_B> _newBuilder = new Tablelayout.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Tablelayout.Builder<Void> copyExcept(final Tablelayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Tablelayout.Builder<Void> copyOnly(final Tablelayout _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Tablelayout _storedValue;
        private String columns;
        private String rows;

        public Builder(final _B _parentBuilder, final Tablelayout _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.columns = _other.columns;
                    this.rows = _other.rows;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Tablelayout _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree columnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("columns"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnsPropertyTree!= null):((columnsPropertyTree == null)||(!columnsPropertyTree.isLeaf())))) {
                        this.columns = _other.columns;
                    }
                    final PropertyTree rowsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rows"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowsPropertyTree!= null):((rowsPropertyTree == null)||(!rowsPropertyTree.isLeaf())))) {
                        this.rows = _other.rows;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Tablelayout >_P init(final _P _product) {
            _product.columns = this.columns;
            _product.rows = this.rows;
            return _product;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        public Tablelayout.Builder<_B> withColumns(final String columns) {
            this.columns = columns;
            return this;
        }

        /**
         * Sets the new value of "rows" (any previous value will be replaced)
         * 
         * @param rows
         *     New value of the "rows" property.
         */
        public Tablelayout.Builder<_B> withRows(final String rows) {
            this.rows = rows;
            return this;
        }

        @Override
        public Tablelayout build() {
            if (_storedValue == null) {
                return this.init(new Tablelayout());
            } else {
                return ((Tablelayout) _storedValue);
            }
        }

        public Tablelayout.Builder<_B> copyOf(final Tablelayout _other) {
            _other.copyTo(this);
            return this;
        }

        public Tablelayout.Builder<_B> copyOf(final Tablelayout.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Tablelayout.Selector<Tablelayout.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Tablelayout.Select _root() {
            return new Tablelayout.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Tablelayout.Selector<TRoot, TParent>> columns = null;
        private com.kscs.util.jaxb.Selector<TRoot, Tablelayout.Selector<TRoot, TParent>> rows = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.columns!= null) {
                products.put("columns", this.columns.init());
            }
            if (this.rows!= null) {
                products.put("rows", this.rows.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Tablelayout.Selector<TRoot, TParent>> columns() {
            return ((this.columns == null)?this.columns = new com.kscs.util.jaxb.Selector<TRoot, Tablelayout.Selector<TRoot, TParent>>(this._root, this, "columns"):this.columns);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Tablelayout.Selector<TRoot, TParent>> rows() {
            return ((this.rows == null)?this.rows = new com.kscs.util.jaxb.Selector<TRoot, Tablelayout.Selector<TRoot, TParent>>(this._root, this, "rows"):this.rows);
        }

    }

}
