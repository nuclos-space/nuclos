
package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}option" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="default" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="orientation"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="horizontal"/&gt;
 *             &lt;enumeration value="vertical"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "option"
})
@XmlRootElement(name = "options")
public class Options implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<Option> option;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "default", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String _default;
    @XmlAttribute(name = "orientation")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String orientation;

    /**
     * Gets the value of the option property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the option property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Option }
     * 
     * 
     */
    public List<Option> getOption() {
        if (option == null) {
            option = new ArrayList<Option>();
        }
        return this.option;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the default property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefault() {
        return _default;
    }

    /**
     * Sets the value of the default property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefault(String value) {
        this._default = value;
    }

    /**
     * Gets the value of the orientation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrientation() {
        return orientation;
    }

    /**
     * Sets the value of the orientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrientation(String value) {
        this.orientation = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Option> theOption;
            theOption = (((this.option!= null)&&(!this.option.isEmpty()))?this.getOption():null);
            strategy.appendField(locator, this, "option", buffer, theOption);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theDefault;
            theDefault = this.getDefault();
            strategy.appendField(locator, this, "_default", buffer, theDefault);
        }
        {
            String theOrientation;
            theOrientation = this.getOrientation();
            strategy.appendField(locator, this, "orientation", buffer, theOrientation);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Options)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Options that = ((Options) object);
        {
            List<Option> lhsOption;
            lhsOption = (((this.option!= null)&&(!this.option.isEmpty()))?this.getOption():null);
            List<Option> rhsOption;
            rhsOption = (((that.option!= null)&&(!that.option.isEmpty()))?that.getOption():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "option", lhsOption), LocatorUtils.property(thatLocator, "option", rhsOption), lhsOption, rhsOption)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsDefault;
            lhsDefault = this.getDefault();
            String rhsDefault;
            rhsDefault = that.getDefault();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "_default", lhsDefault), LocatorUtils.property(thatLocator, "_default", rhsDefault), lhsDefault, rhsDefault)) {
                return false;
            }
        }
        {
            String lhsOrientation;
            lhsOrientation = this.getOrientation();
            String rhsOrientation;
            rhsOrientation = that.getOrientation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orientation", lhsOrientation), LocatorUtils.property(thatLocator, "orientation", rhsOrientation), lhsOrientation, rhsOrientation)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Option> theOption;
            theOption = (((this.option!= null)&&(!this.option.isEmpty()))?this.getOption():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "option", theOption), currentHashCode, theOption);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theDefault;
            theDefault = this.getDefault();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "_default", theDefault), currentHashCode, theDefault);
        }
        {
            String theOrientation;
            theOrientation = this.getOrientation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orientation", theOrientation), currentHashCode, theOrientation);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Options) {
            final Options copy = ((Options) draftCopy);
            if ((this.option!= null)&&(!this.option.isEmpty())) {
                List<Option> sourceOption;
                sourceOption = (((this.option!= null)&&(!this.option.isEmpty()))?this.getOption():null);
                @SuppressWarnings("unchecked")
                List<Option> copyOption = ((List<Option> ) strategy.copy(LocatorUtils.property(locator, "option", sourceOption), sourceOption));
                copy.option = null;
                if (copyOption!= null) {
                    List<Option> uniqueOptionl = copy.getOption();
                    uniqueOptionl.addAll(copyOption);
                }
            } else {
                copy.option = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this._default!= null) {
                String sourceDefault;
                sourceDefault = this.getDefault();
                String copyDefault = ((String) strategy.copy(LocatorUtils.property(locator, "_default", sourceDefault), sourceDefault));
                copy.setDefault(copyDefault);
            } else {
                copy._default = null;
            }
            if (this.orientation!= null) {
                String sourceOrientation;
                sourceOrientation = this.getOrientation();
                String copyOrientation = ((String) strategy.copy(LocatorUtils.property(locator, "orientation", sourceOrientation), sourceOrientation));
                copy.setOrientation(copyOrientation);
            } else {
                copy.orientation = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Options();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Options.Builder<_B> _other) {
        if (this.option == null) {
            _other.option = null;
        } else {
            _other.option = new ArrayList<Option.Builder<Options.Builder<_B>>>();
            for (Option _item: this.option) {
                _other.option.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other._default = this._default;
        _other.orientation = this.orientation;
    }

    public<_B >Options.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Options.Builder<_B>(_parentBuilder, this, true);
    }

    public Options.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Options.Builder<Void> builder() {
        return new Options.Builder<Void>(null, null, false);
    }

    public static<_B >Options.Builder<_B> copyOf(final Options _other) {
        final Options.Builder<_B> _newBuilder = new Options.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Options.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree optionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("option"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionPropertyTree!= null):((optionPropertyTree == null)||(!optionPropertyTree.isLeaf())))) {
            if (this.option == null) {
                _other.option = null;
            } else {
                _other.option = new ArrayList<Option.Builder<Options.Builder<_B>>>();
                for (Option _item: this.option) {
                    _other.option.add(((_item == null)?null:_item.newCopyBuilder(_other, optionPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree _defaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("_default"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(_defaultPropertyTree!= null):((_defaultPropertyTree == null)||(!_defaultPropertyTree.isLeaf())))) {
            _other._default = this._default;
        }
        final PropertyTree orientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orientation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orientationPropertyTree!= null):((orientationPropertyTree == null)||(!orientationPropertyTree.isLeaf())))) {
            _other.orientation = this.orientation;
        }
    }

    public<_B >Options.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Options.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Options.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Options.Builder<_B> copyOf(final Options _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Options.Builder<_B> _newBuilder = new Options.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Options.Builder<Void> copyExcept(final Options _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Options.Builder<Void> copyOnly(final Options _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Options _storedValue;
        private List<Option.Builder<Options.Builder<_B>>> option;
        private String name;
        private String _default;
        private String orientation;

        public Builder(final _B _parentBuilder, final Options _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.option == null) {
                        this.option = null;
                    } else {
                        this.option = new ArrayList<Option.Builder<Options.Builder<_B>>>();
                        for (Option _item: _other.option) {
                            this.option.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this._default = _other._default;
                    this.orientation = _other.orientation;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Options _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree optionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("option"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionPropertyTree!= null):((optionPropertyTree == null)||(!optionPropertyTree.isLeaf())))) {
                        if (_other.option == null) {
                            this.option = null;
                        } else {
                            this.option = new ArrayList<Option.Builder<Options.Builder<_B>>>();
                            for (Option _item: _other.option) {
                                this.option.add(((_item == null)?null:_item.newCopyBuilder(this, optionPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree _defaultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("_default"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(_defaultPropertyTree!= null):((_defaultPropertyTree == null)||(!_defaultPropertyTree.isLeaf())))) {
                        this._default = _other._default;
                    }
                    final PropertyTree orientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orientation"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orientationPropertyTree!= null):((orientationPropertyTree == null)||(!orientationPropertyTree.isLeaf())))) {
                        this.orientation = _other.orientation;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Options >_P init(final _P _product) {
            if (this.option!= null) {
                final List<Option> option = new ArrayList<Option>(this.option.size());
                for (Option.Builder<Options.Builder<_B>> _item: this.option) {
                    option.add(_item.build());
                }
                _product.option = option;
            }
            _product.name = this.name;
            _product._default = this._default;
            _product.orientation = this.orientation;
            return _product;
        }

        /**
         * Adds the given items to the value of "option"
         * 
         * @param option
         *     Items to add to the value of the "option" property
         */
        public Options.Builder<_B> addOption(final Iterable<? extends Option> option) {
            if (option!= null) {
                if (this.option == null) {
                    this.option = new ArrayList<Option.Builder<Options.Builder<_B>>>();
                }
                for (Option _item: option) {
                    this.option.add(new Option.Builder<Options.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "option" (any previous value will be replaced)
         * 
         * @param option
         *     New value of the "option" property.
         */
        public Options.Builder<_B> withOption(final Iterable<? extends Option> option) {
            if (this.option!= null) {
                this.option.clear();
            }
            return addOption(option);
        }

        /**
         * Adds the given items to the value of "option"
         * 
         * @param option
         *     Items to add to the value of the "option" property
         */
        public Options.Builder<_B> addOption(Option... option) {
            addOption(Arrays.asList(option));
            return this;
        }

        /**
         * Sets the new value of "option" (any previous value will be replaced)
         * 
         * @param option
         *     New value of the "option" property.
         */
        public Options.Builder<_B> withOption(Option... option) {
            withOption(Arrays.asList(option));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Option" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Option.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Option" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Option.Builder#end()} to return to the current builder.
         */
        public Option.Builder<? extends Options.Builder<_B>> addOption() {
            if (this.option == null) {
                this.option = new ArrayList<Option.Builder<Options.Builder<_B>>>();
            }
            final Option.Builder<Options.Builder<_B>> option_Builder = new Option.Builder<Options.Builder<_B>>(this, null, false);
            this.option.add(option_Builder);
            return option_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Options.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "_default" (any previous value will be replaced)
         * 
         * @param _default
         *     New value of the "_default" property.
         */
        public Options.Builder<_B> withDefault(final String _default) {
            this._default = _default;
            return this;
        }

        /**
         * Sets the new value of "orientation" (any previous value will be replaced)
         * 
         * @param orientation
         *     New value of the "orientation" property.
         */
        public Options.Builder<_B> withOrientation(final String orientation) {
            this.orientation = orientation;
            return this;
        }

        @Override
        public Options build() {
            if (_storedValue == null) {
                return this.init(new Options());
            } else {
                return ((Options) _storedValue);
            }
        }

        public Options.Builder<_B> copyOf(final Options _other) {
            _other.copyTo(this);
            return this;
        }

        public Options.Builder<_B> copyOf(final Options.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Options.Selector<Options.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Options.Select _root() {
            return new Options.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Option.Selector<TRoot, Options.Selector<TRoot, TParent>> option = null;
        private com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>> _default = null;
        private com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>> orientation = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.option!= null) {
                products.put("option", this.option.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this._default!= null) {
                products.put("_default", this._default.init());
            }
            if (this.orientation!= null) {
                products.put("orientation", this.orientation.init());
            }
            return products;
        }

        public Option.Selector<TRoot, Options.Selector<TRoot, TParent>> option() {
            return ((this.option == null)?this.option = new Option.Selector<TRoot, Options.Selector<TRoot, TParent>>(this._root, this, "option"):this.option);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>> _default() {
            return ((this._default == null)?this._default = new com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>>(this._root, this, "_default"):this._default);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>> orientation() {
            return ((this.orientation == null)?this.orientation = new com.kscs.util.jaxb.Selector<TRoot, Options.Selector<TRoot, TParent>>(this._root, this, "orientation"):this.orientation);
        }

    }

}
