
package org.nuclos.schema.launcher;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for launcher-launch-message complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="launcher-launch-message"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.launcher}launcher-message"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parameters" type="{urn:org.nuclos.schema.launcher}parameter" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "launcher-launch-message", propOrder = {
    "parameters"
})
public class LauncherLaunchMessage
    extends LauncherMessage
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<Parameter> parameters;

    /**
     * Gets the value of the parameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Parameter }
     * 
     * 
     */
    public List<Parameter> getParameters() {
        if (parameters == null) {
            parameters = new ArrayList<Parameter>();
        }
        return this.parameters;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            List<Parameter> theParameters;
            theParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            strategy.appendField(locator, this, "parameters", buffer, theParameters);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LauncherLaunchMessage)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final LauncherLaunchMessage that = ((LauncherLaunchMessage) object);
        {
            List<Parameter> lhsParameters;
            lhsParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            List<Parameter> rhsParameters;
            rhsParameters = (((that.parameters!= null)&&(!that.parameters.isEmpty()))?that.getParameters():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameters", lhsParameters), LocatorUtils.property(thatLocator, "parameters", rhsParameters), lhsParameters, rhsParameters)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            List<Parameter> theParameters;
            theParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parameters", theParameters), currentHashCode, theParameters);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof LauncherLaunchMessage) {
            final LauncherLaunchMessage copy = ((LauncherLaunchMessage) draftCopy);
            if ((this.parameters!= null)&&(!this.parameters.isEmpty())) {
                List<Parameter> sourceParameters;
                sourceParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
                @SuppressWarnings("unchecked")
                List<Parameter> copyParameters = ((List<Parameter> ) strategy.copy(LocatorUtils.property(locator, "parameters", sourceParameters), sourceParameters));
                copy.parameters = null;
                if (copyParameters!= null) {
                    List<Parameter> uniqueParametersl = copy.getParameters();
                    uniqueParametersl.addAll(copyParameters);
                }
            } else {
                copy.parameters = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LauncherLaunchMessage();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LauncherLaunchMessage.Builder<_B> _other) {
        super.copyTo(_other);
        if (this.parameters == null) {
            _other.parameters = null;
        } else {
            _other.parameters = new ArrayList<Parameter.Builder<LauncherLaunchMessage.Builder<_B>>>();
            for (Parameter _item: this.parameters) {
                _other.parameters.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    @Override
    public<_B >LauncherLaunchMessage.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LauncherLaunchMessage.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public LauncherLaunchMessage.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LauncherLaunchMessage.Builder<Void> builder() {
        return new LauncherLaunchMessage.Builder<Void>(null, null, false);
    }

    public static<_B >LauncherLaunchMessage.Builder<_B> copyOf(final LauncherMessage _other) {
        final LauncherLaunchMessage.Builder<_B> _newBuilder = new LauncherLaunchMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >LauncherLaunchMessage.Builder<_B> copyOf(final LauncherLaunchMessage _other) {
        final LauncherLaunchMessage.Builder<_B> _newBuilder = new LauncherLaunchMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LauncherLaunchMessage.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree parametersPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameters"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parametersPropertyTree!= null):((parametersPropertyTree == null)||(!parametersPropertyTree.isLeaf())))) {
            if (this.parameters == null) {
                _other.parameters = null;
            } else {
                _other.parameters = new ArrayList<Parameter.Builder<LauncherLaunchMessage.Builder<_B>>>();
                for (Parameter _item: this.parameters) {
                    _other.parameters.add(((_item == null)?null:_item.newCopyBuilder(_other, parametersPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    @Override
    public<_B >LauncherLaunchMessage.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LauncherLaunchMessage.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public LauncherLaunchMessage.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LauncherLaunchMessage.Builder<_B> copyOf(final LauncherMessage _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LauncherLaunchMessage.Builder<_B> _newBuilder = new LauncherLaunchMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >LauncherLaunchMessage.Builder<_B> copyOf(final LauncherLaunchMessage _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LauncherLaunchMessage.Builder<_B> _newBuilder = new LauncherLaunchMessage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LauncherLaunchMessage.Builder<Void> copyExcept(final LauncherMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LauncherLaunchMessage.Builder<Void> copyExcept(final LauncherLaunchMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LauncherLaunchMessage.Builder<Void> copyOnly(final LauncherMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static LauncherLaunchMessage.Builder<Void> copyOnly(final LauncherLaunchMessage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends LauncherMessage.Builder<_B>
        implements Buildable
    {

        private List<Parameter.Builder<LauncherLaunchMessage.Builder<_B>>> parameters;

        public Builder(final _B _parentBuilder, final LauncherLaunchMessage _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                if (_other.parameters == null) {
                    this.parameters = null;
                } else {
                    this.parameters = new ArrayList<Parameter.Builder<LauncherLaunchMessage.Builder<_B>>>();
                    for (Parameter _item: _other.parameters) {
                        this.parameters.add(((_item == null)?null:_item.newCopyBuilder(this)));
                    }
                }
            }
        }

        public Builder(final _B _parentBuilder, final LauncherLaunchMessage _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree parametersPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameters"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parametersPropertyTree!= null):((parametersPropertyTree == null)||(!parametersPropertyTree.isLeaf())))) {
                    if (_other.parameters == null) {
                        this.parameters = null;
                    } else {
                        this.parameters = new ArrayList<Parameter.Builder<LauncherLaunchMessage.Builder<_B>>>();
                        for (Parameter _item: _other.parameters) {
                            this.parameters.add(((_item == null)?null:_item.newCopyBuilder(this, parametersPropertyTree, _propertyTreeUse)));
                        }
                    }
                }
            }
        }

        protected<_P extends LauncherLaunchMessage >_P init(final _P _product) {
            if (this.parameters!= null) {
                final List<Parameter> parameters = new ArrayList<Parameter>(this.parameters.size());
                for (Parameter.Builder<LauncherLaunchMessage.Builder<_B>> _item: this.parameters) {
                    parameters.add(_item.build());
                }
                _product.parameters = parameters;
            }
            return super.init(_product);
        }

        /**
         * Adds the given items to the value of "parameters"
         * 
         * @param parameters
         *     Items to add to the value of the "parameters" property
         */
        public LauncherLaunchMessage.Builder<_B> addParameters(final Iterable<? extends Parameter> parameters) {
            if (parameters!= null) {
                if (this.parameters == null) {
                    this.parameters = new ArrayList<Parameter.Builder<LauncherLaunchMessage.Builder<_B>>>();
                }
                for (Parameter _item: parameters) {
                    this.parameters.add(new Parameter.Builder<LauncherLaunchMessage.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "parameters" (any previous value will be replaced)
         * 
         * @param parameters
         *     New value of the "parameters" property.
         */
        public LauncherLaunchMessage.Builder<_B> withParameters(final Iterable<? extends Parameter> parameters) {
            if (this.parameters!= null) {
                this.parameters.clear();
            }
            return addParameters(parameters);
        }

        /**
         * Adds the given items to the value of "parameters"
         * 
         * @param parameters
         *     Items to add to the value of the "parameters" property
         */
        public LauncherLaunchMessage.Builder<_B> addParameters(Parameter... parameters) {
            addParameters(Arrays.asList(parameters));
            return this;
        }

        /**
         * Sets the new value of "parameters" (any previous value will be replaced)
         * 
         * @param parameters
         *     New value of the "parameters" property.
         */
        public LauncherLaunchMessage.Builder<_B> withParameters(Parameter... parameters) {
            withParameters(Arrays.asList(parameters));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Parameters" property.
         * Use {@link org.nuclos.schema.launcher.Parameter.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Parameters" property.
         *     Use {@link org.nuclos.schema.launcher.Parameter.Builder#end()} to return to the current builder.
         */
        public Parameter.Builder<? extends LauncherLaunchMessage.Builder<_B>> addParameters() {
            if (this.parameters == null) {
                this.parameters = new ArrayList<Parameter.Builder<LauncherLaunchMessage.Builder<_B>>>();
            }
            final Parameter.Builder<LauncherLaunchMessage.Builder<_B>> parameters_Builder = new Parameter.Builder<LauncherLaunchMessage.Builder<_B>>(this, null, false);
            this.parameters.add(parameters_Builder);
            return parameters_Builder;
        }

        /**
         * Sets the new value of "clientId" (any previous value will be replaced)
         * 
         * @param clientId
         *     New value of the "clientId" property.
         */
        @Override
        public LauncherLaunchMessage.Builder<_B> withClientId(final BigInteger clientId) {
            super.withClientId(clientId);
            return this;
        }

        @Override
        public LauncherLaunchMessage build() {
            if (_storedValue == null) {
                return this.init(new LauncherLaunchMessage());
            } else {
                return ((LauncherLaunchMessage) _storedValue);
            }
        }

        public LauncherLaunchMessage.Builder<_B> copyOf(final LauncherLaunchMessage _other) {
            _other.copyTo(this);
            return this;
        }

        public LauncherLaunchMessage.Builder<_B> copyOf(final LauncherLaunchMessage.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LauncherLaunchMessage.Selector<LauncherLaunchMessage.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LauncherLaunchMessage.Select _root() {
            return new LauncherLaunchMessage.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends LauncherMessage.Selector<TRoot, TParent>
    {

        private Parameter.Selector<TRoot, LauncherLaunchMessage.Selector<TRoot, TParent>> parameters = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.parameters!= null) {
                products.put("parameters", this.parameters.init());
            }
            return products;
        }

        public Parameter.Selector<TRoot, LauncherLaunchMessage.Selector<TRoot, TParent>> parameters() {
            return ((this.parameters == null)?this.parameters = new Parameter.Selector<TRoot, LauncherLaunchMessage.Selector<TRoot, TParent>>(this._root, this, "parameters"):this.parameters);
        }

    }

}
